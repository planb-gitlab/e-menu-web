-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 19, 2019 at 12:45 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `e_menu`
--

-- --------------------------------------------------------

--
-- Table structure for table `business_plan`
--

CREATE TABLE `business_plan` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `business_plan`
--

INSERT INTO `business_plan` (`id`, `name`, `description`, `price`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Trial Version', '1 Month of use,\r\nFree tutorial,\r\nWork like premium plan,', 0, 1, '2018-06-27 07:11:57', NULL),
(2, 'Lite Version', '$ 49/mo billed every 6 months\r\nor $59 billed monthly\r\n1 Store  \r\n1 Register  \r\n10 Users /store \r\nIntuitive Point of Sale \r\nReal-time Inventory Management  \r\nMon-Fri (8:00-5:00) Phone & Online Support  \r\nSmall Business Reporting', 59, 1, '2018-06-27 07:34:30', '2018-10-01 01:49:30'),
(3, 'Pro Version', '$ 69/mo billed every 6 months\r\nor $79 billed monthly\r\n1+ Stores \r\n1+ Register \r\n15 Users /store \r\nIntuitive Point of Sale \r\nReal-time Inventory Management  \r\nMon-Fri (8:00-5:00) Phone & Online Support  \r\nAdvanced Reporting & Analytics  \r\nMonthly Maintenance Check', 79, 1, '2018-06-27 07:36:07', '2018-10-29 08:52:53');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_apicustom`
--

CREATE TABLE `cms_apicustom` (
  `id` int(10) UNSIGNED NOT NULL,
  `permalink` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tabel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aksi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kolom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderby` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_query_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sql_where` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `method_type` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` longtext COLLATE utf8mb4_unicode_ci,
  `responses` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_apikey`
--

CREATE TABLE `cms_apikey` (
  `id` int(10) UNSIGNED NOT NULL,
  `screetkey` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hit` int(11) DEFAULT NULL,
  `status` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_dashboard`
--

CREATE TABLE `cms_dashboard` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_email_queues`
--

CREATE TABLE `cms_email_queues` (
  `id` int(10) UNSIGNED NOT NULL,
  `send_at` datetime DEFAULT NULL,
  `email_recipient` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_cc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_content` text COLLATE utf8mb4_unicode_ci,
  `email_attachments` text COLLATE utf8mb4_unicode_ci,
  `is_sent` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_email_templates`
--

CREATE TABLE `cms_email_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_email_templates`
--

INSERT INTO `cms_email_templates` (`id`, `name`, `slug`, `subject`, `content`, `description`, `from_name`, `from_email`, `cc_email`, `created_at`, `updated_at`) VALUES
(1, 'Email Template Forgot Password Backend', 'forgot_password_backend', NULL, '<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>', '[password]', 'System', 'system@crudbooster.com', NULL, '2018-05-09 00:05:44', NULL),
(2, 'Email Template Forgot Password Backend', 'forgot_password_backend', NULL, '<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>', '[password]', 'System', 'system@crudbooster.com', NULL, '2018-06-03 19:27:25', NULL),
(3, 'Email Template Forgot Password Backend', 'forgot_password_backend', NULL, '<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>', '[password]', 'System', 'system@crudbooster.com', NULL, '2018-06-03 19:28:32', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_logs`
--

CREATE TABLE `cms_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `ipaddress` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `useragent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `id_cms_users` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_logs`
--

INSERT INTO `cms_logs` (`id`, `ipaddress`, `useragent`, `url`, `description`, `details`, `id_cms_users`, `created_at`, `updated_at`) VALUES
(1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', 'http://localhost/e-menu/public/admin/login', 'admin@system.com login with IP Address ::1', '', 1, '2019-03-12 03:28:50', NULL),
(2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', 'http://localhost/e-menu/public/admin/logout', 'admin@system.com logout', '', 1, '2019-03-12 03:48:53', NULL),
(3, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', 'http://localhost/e-menu-client/public/admin/login', 'admin@system.com login with IP Address ::1', '', 1, '2019-03-12 04:54:52', NULL),
(4, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', 'http://localhost/e-menu-client/public/admin/logout', 'admin@system.com logout', '', 1, '2019-03-12 04:54:56', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_menus`
--

CREATE TABLE `cms_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'url',
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_dashboard` tinyint(1) NOT NULL DEFAULT '0',
  `id_cms_privileges` int(11) DEFAULT NULL,
  `super_admin` tinyint(1) DEFAULT NULL,
  `is_only_menu` int(11) DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_menus`
--

INSERT INTO `cms_menus` (`id`, `name`, `type`, `path`, `color`, `icon`, `parent_id`, `is_active`, `is_dashboard`, `id_cms_privileges`, `super_admin`, `is_only_menu`, `sorting`, `created_at`, `updated_at`) VALUES
(1, 'Role', 'Route', 'AdminRolesControllerGetIndex', 'normal', 'fa fa-user', 44, 1, 0, 1, NULL, NULL, 2, '2018-05-09 20:23:37', '2018-10-26 08:41:31'),
(2, 'User', 'Module', 'cms_users', 'normal', 'fa fa-users', 44, 1, 0, 1, NULL, NULL, 1, '2018-05-09 23:40:25', '2018-09-14 01:18:55'),
(3, 'Category Menu', 'Route', 'AdminCategoriesControllerGetIndex', 'normal', 'fa fa-tags', 43, 1, 0, 1, NULL, NULL, 1, '2018-05-10 02:35:08', '2018-09-20 09:41:41'),
(4, 'Menu', 'Route', 'AdminMenusControllerGetIndex', 'normal', 'fa fa-th-large', 43, 1, 0, 1, NULL, NULL, 3, '2018-05-10 21:27:11', '2018-09-14 01:19:51'),
(7, 'Discount', 'Route', 'AdminDiscountsControllerGetIndex', 'normal', 'fa fa-percent', 0, 1, 0, 1, NULL, NULL, 3, '2018-05-11 02:20:34', '2018-12-28 08:40:08'),
(9, 'Currency', 'Route', 'AdminCurrenciesControllerGetIndex', 'normal', 'fa fa-money', 40, 1, 0, 1, NULL, NULL, 5, '2018-05-11 02:39:32', '2018-09-14 01:24:12'),
(10, 'Exchange Rate', 'Module', 'exchange', 'normal', 'fa fa-money', 40, 1, 0, 1, NULL, NULL, 6, '2018-05-11 02:42:26', '2018-09-14 01:23:50'),
(11, 'UOM', 'Module', 'uom', 'normal', 'fa fa-refresh', 40, 1, 0, 1, NULL, NULL, 4, '2018-05-17 19:05:54', '2018-09-22 07:18:40'),
(12, 'Promotion Pack', 'Route', 'AdminPromotionPackControllerGetIndex', 'normal', 'fa fa-bullhorn', 0, 1, 0, 1, NULL, NULL, 4, '2018-05-18 02:01:41', '2018-09-14 01:20:39'),
(14, 'Table Management', 'Route', 'AdminTableNumbersControllerGetIndex', 'normal', 'fa fa-table', 0, 1, 0, 1, NULL, NULL, 5, '2018-05-22 19:42:51', '2018-09-14 01:20:50'),
(17, 'Report', 'Module', 'report_dashboard', 'normal', 'fa fa-bar-chart', 0, 1, 0, 1, NULL, 1, 7, '2018-05-23 19:11:23', '2018-10-01 02:28:50'),
(18, 'Outlet Setting', 'Route', 'AdminSettingControllerGetIndex', 'normal', 'fa fa-cog', 40, 1, 0, 1, NULL, NULL, 1, '2018-05-23 19:16:01', '2018-09-14 01:23:32'),
(21, 'Sale by item', 'Module', 'sale_by_item', 'normal', 'fa fa-cutlery', 17, 1, 0, 1, NULL, NULL, 3, '2018-05-24 00:00:34', '2018-09-13 11:59:31'),
(22, 'Sale by customer', 'Route', 'AdminSaleByCustomerControllerGetIndex', 'normal', 'fa fa-users', 17, 0, 0, 1, NULL, NULL, 2, '2018-05-24 03:31:53', '2018-08-24 07:54:44'),
(23, 'Sale by Cashier', 'Route', 'AdminSaleByCashierControllerGetIndex', 'normal', 'fa fa-user', 17, 1, 0, 1, NULL, NULL, 4, '2018-05-24 03:34:52', '2018-10-17 08:26:15'),
(24, 'Sale by promotion pack', 'Route', 'AdminSaleByPromotionPackControllerGetIndex', 'normal', 'fa fa-shopping-bag', 17, 1, 0, 1, NULL, NULL, 5, '2018-05-24 03:36:22', '2018-09-13 11:59:48'),
(25, 'Void Payment', 'Route', 'AdminVoidPaymentControllerGetIndex', 'normal', 'fa fa-times', 17, 1, 0, 1, NULL, NULL, 6, '2018-05-24 03:39:35', '2018-09-13 11:59:54'),
(26, 'Credit memo report', 'Route', 'AdminCreditMemoReportControllerGetIndex', 'normal', 'fa fa-sticky-note-o', 17, 0, 0, 1, NULL, NULL, 7, '2018-05-24 03:41:38', '2019-01-07 21:28:33'),
(27, 'Discount Summary', 'Route', 'AdminDiscountSummaryControllerGetIndex', 'normal', 'fa fa-percent', 17, 1, 0, 1, NULL, NULL, 7, '2018-05-24 03:43:08', '2019-01-16 16:17:09'),
(29, 'Sale in invoice', 'Route', 'AdminSaleByInvoiceControllerGetIndex', 'normal', 'fa fa-file-o', 17, 1, 0, 1, NULL, NULL, 2, '2018-05-28 20:41:28', '2018-09-13 11:59:23'),
(30, 'User Activity', 'Route', 'AdminUserActivitiesControllerGetIndex', 'normal', 'fa fa-info-circle', 40, 1, 0, 1, NULL, NULL, 7, '2018-05-31 03:09:11', '2018-09-14 01:24:01'),
(33, 'Sub Category', 'Route', 'AdminSubcategoryControllerGetIndex', 'normal', 'fa fa-glass', 43, 1, 0, 1, NULL, NULL, 2, '2018-06-13 01:55:36', '2018-09-14 01:19:36'),
(35, 'Dashboard', 'Route', 'AdminDashboardControllerGetIndex', 'normal', 'fa fa-dashboard', 0, 1, 1, 1, NULL, NULL, 8, '2018-06-22 04:51:29', '2018-10-01 02:29:15'),
(36, 'Outlet User', 'Route', 'AdminStoreUserControllerGetIndex', 'normal', 'fa fa-users', 64, 1, 0, 1, 1, NULL, 1, '2018-06-27 06:37:02', '2019-01-23 05:54:12'),
(39, 'Addon', 'Route', 'AdminMenuMaterialsControllerGetIndex', 'normal', 'fa fa-inbox', 43, 1, 0, 1, NULL, NULL, 4, '2018-06-29 04:08:10', '2018-12-17 01:44:27'),
(40, 'Setting', 'Module', 'setting', 'normal', 'fa fa-cogs', 0, 1, 0, 1, NULL, NULL, 11, '2018-08-11 04:54:43', '2018-09-13 12:01:29'),
(41, 'Screen Request Order', 'Route', 'AdminScreenRequestOrderControllerGetIndex', 'normal', 'fa fa-glass', 0, 0, 0, 1, NULL, NULL, 2, '2018-08-15 06:06:08', '2018-08-24 01:29:53'),
(42, 'Dine In', 'Route', 'CashierTableManagementControllerGetIndex', 'normal', 'fa fa-cutlery', 56, 1, 0, 1, NULL, NULL, 1, '2018-08-16 01:24:31', '2018-09-13 12:00:48'),
(43, 'Menu Management', 'Module', 'menus', 'normal', 'fa fa-cutlery', 0, 1, 0, 1, NULL, 1, 2, '2018-08-22 01:21:29', '2018-09-14 01:19:20'),
(44, 'User Management', 'Module', 'cms_users', 'normal', 'fa fa-users', 0, 1, 0, 1, NULL, 1, 6, '2018-08-24 07:56:14', '2018-09-14 01:18:46'),
(49, 'Take Out', 'Route', 'AdminOrderControllerGetIndex', 'normal', 'fa fa-truck', 56, 1, 0, 1, NULL, NULL, 2, '2018-09-03 03:50:04', '2018-09-13 12:01:00'),
(51, 'Cash Management', 'URL', '#', 'normal', 'fa fa-glass', 0, 1, 0, 1, NULL, 1, 10, '2018-09-04 08:20:30', '2018-09-14 01:17:50'),
(56, 'Order', 'URL', '#', 'normal', 'fa fa-pencil', 0, 1, 0, 1, NULL, 1, 1, '2018-09-11 03:39:28', '2018-09-13 12:00:33'),
(57, 'Void', 'Controller & Method', 'AdminVoidController@getIndex', 'normal', 'fa fa-times', 51, 1, 0, 1, NULL, NULL, 1, '2018-09-14 07:40:42', '2018-10-24 09:19:14'),
(59, 'invoice MG', 'Module', 'invoice_mg', 'normal', 'fa fa-envelope-o', 56, 1, 0, 1, NULL, NULL, 3, '2018-09-25 07:00:16', '2018-10-24 09:18:49'),
(60, 'Report Dashboard', 'Module', 'report_dashboard', 'normal', 'fa fa-dashboard', 17, 1, 0, 1, NULL, NULL, 1, '2018-10-01 01:55:02', '2018-10-01 03:16:48'),
(61, 'Membership', 'Route', 'AdminMembershipsControllerGetIndex', 'normal', 'fa fa-users', 40, 1, 0, 1, NULL, NULL, 3, '2018-11-19 09:12:26', '2018-11-19 09:28:33'),
(62, 'Order Setting', 'Route', 'AdminOrderSettingControllerGetIndex', 'normal', 'fa fa-cog', 40, 1, 0, 1, NULL, NULL, 2, '2018-11-27 04:30:30', '2018-11-27 06:44:48'),
(63, 'Outlet', 'Route', 'AdminOutletControllerGetIndex', 'normal', 'fa fa-home', 64, 0, 0, 1, NULL, NULL, 2, '2019-01-23 05:59:59', '2019-03-09 02:58:52'),
(64, 'Outlet Management', 'URL', '#', 'normal', 'fa fa-users', 0, 1, 0, 1, NULL, NULL, 9, '2019-01-23 06:04:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_menus_privileges`
--

CREATE TABLE `cms_menus_privileges` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_menus` int(11) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_menus_privileges`
--

INSERT INTO `cms_menus_privileges` (`id`, `id_cms_menus`, `id_cms_privileges`) VALUES
(6, 5, 1),
(8, 8, 1),
(13, 13, 1),
(15, 15, 1),
(16, 16, 1),
(19, 19, 1),
(21, 20, 1),
(30, 28, 1),
(40, 31, 1),
(41, 32, 2),
(43, 34, 1),
(51, 38, 1),
(445, 41, 1),
(446, 22, 2),
(447, NULL, 2),
(452, 45, 2),
(453, 46, 2),
(454, 47, 2),
(455, 48, 2),
(457, 50, 1),
(477, 37, 1),
(484, 54, 1),
(642, 29, 3),
(643, 29, 2),
(644, 21, 3),
(645, 21, 2),
(648, 24, 3),
(649, 24, 2),
(650, 25, 3),
(651, 25, 2),
(656, 56, 3),
(657, 56, 2),
(658, 56, 5),
(659, 56, 4),
(660, 42, 3),
(661, 42, 2),
(662, 42, 5),
(663, 42, 4),
(664, 49, 3),
(665, 49, 2),
(666, 49, 5),
(667, 49, 4),
(668, 40, 3),
(669, 40, 2),
(671, 40, 1),
(688, 51, 3),
(689, 51, 2),
(690, 51, 4),
(697, 44, 3),
(698, 44, 2),
(699, 44, 4),
(700, 2, 3),
(701, 2, 2),
(702, 2, 4),
(706, 43, 3),
(707, 43, 2),
(708, 43, 4),
(712, 33, 3),
(713, 33, 2),
(714, 33, 4),
(715, 4, 3),
(716, 4, 2),
(717, 4, 4),
(724, 12, 3),
(725, 12, 2),
(726, 12, 4),
(727, 14, 3),
(728, 14, 2),
(729, 14, 4),
(730, 18, 3),
(731, 18, 2),
(732, 18, 4),
(733, 10, 3),
(734, 10, 2),
(735, 10, 4),
(736, 30, 3),
(737, 30, 2),
(738, 30, 4),
(739, 9, 3),
(740, 9, 2),
(741, 9, 4),
(742, 9, 1),
(744, 53, 3),
(745, 53, 2),
(746, 53, 4),
(750, 3, 3),
(751, 3, 2),
(752, 3, 4),
(759, 52, 3),
(760, 52, 2),
(761, 52, 4),
(765, 11, 3),
(766, 11, 2),
(767, 11, 4),
(768, 58, 1),
(779, 17, 3),
(780, 17, 2),
(781, 35, 3),
(782, 35, 2),
(783, 35, 4),
(784, 35, 1),
(785, 60, 3),
(786, 60, 2),
(787, 23, 3),
(788, 23, 2),
(789, 59, 3),
(790, 59, 2),
(791, 59, 5),
(792, 59, 4),
(793, 57, 3),
(794, 57, 2),
(795, 57, 4),
(984, 1, 3),
(985, 1, 2),
(986, 1, 4),
(1087, 1, 7),
(1088, 44, 7),
(1089, 3, 7),
(1090, 43, 7),
(1092, 52, 7),
(1093, 51, 7),
(1094, 3, 8),
(1095, 43, 8),
(1096, 4, 8),
(1097, 43, 8),
(1099, 43, 8),
(1100, 33, 8),
(1101, 43, 8),
(1102, 42, 8),
(1103, 56, 8),
(1104, 59, 8),
(1105, 56, 8),
(1106, 49, 8),
(1107, 56, 8),
(1108, 12, 8),
(1109, NULL, 8),
(1295, 42, 11),
(1296, 49, 11),
(1297, 56, 11),
(1298, 59, 11),
(1299, 1, 13),
(1300, 2, 13),
(1301, 3, 13),
(1302, 4, 13),
(1304, 9, 13),
(1305, 10, 13),
(1306, 11, 13),
(1307, 12, 13),
(1308, 14, 13),
(1309, 17, 13),
(1310, 18, 13),
(1311, 21, 13),
(1312, 23, 13),
(1313, 24, 13),
(1314, 25, 13),
(1317, 29, 13),
(1318, 30, 13),
(1319, 33, 13),
(1320, 35, 13),
(1322, 40, 13),
(1323, 42, 13),
(1324, 43, 13),
(1325, 44, 13),
(1326, 49, 13),
(1327, 51, 13),
(1328, 52, 13),
(1329, 53, 13),
(1330, 56, 13),
(1331, 57, 13),
(1332, 59, 13),
(1333, 60, 13),
(1334, 1, 14),
(1335, 2, 14),
(1336, 3, 14),
(1337, 4, 14),
(1338, 9, 14),
(1339, 10, 14),
(1340, 11, 14),
(1341, 12, 14),
(1342, 14, 14),
(1343, 18, 14),
(1344, 30, 14),
(1345, 33, 14),
(1346, 35, 14),
(1348, 42, 14),
(1349, 43, 14),
(1350, 44, 14),
(1351, 49, 14),
(1352, 51, 14),
(1353, 52, 14),
(1354, 53, 14),
(1355, 56, 14),
(1356, 57, 14),
(1357, 59, 14),
(1358, 42, 15),
(1359, 49, 15),
(1360, 56, 15),
(1361, 59, 15),
(1362, 52, 17),
(1363, 51, 17),
(1364, 57, 17),
(1365, 51, 17),
(1366, 35, 17),
(1367, NULL, 17),
(1369, NULL, 17),
(1370, 3, 17),
(1371, 43, 17),
(1372, 4, 17),
(1373, 43, 17),
(1375, 43, 17),
(1376, 33, 17),
(1377, 43, 17),
(1378, 42, 17),
(1379, 56, 17),
(1380, 59, 17),
(1381, 56, 17),
(1382, 49, 17),
(1383, 56, 17),
(1384, 12, 17),
(1385, NULL, 17),
(1387, 17, 17),
(1389, 17, 17),
(1390, 60, 17),
(1391, 17, 17),
(1392, 23, 17),
(1393, 17, 17),
(1394, 21, 17),
(1395, 17, 17),
(1396, 24, 17),
(1397, 17, 17),
(1398, 29, 17),
(1399, 17, 17),
(1400, 25, 17),
(1401, 17, 17),
(1402, 40, 17),
(1403, NULL, 17),
(1405, 40, 17),
(1406, 9, 17),
(1407, 40, 17),
(1408, 10, 17),
(1409, 40, 17),
(1410, 18, 17),
(1411, 40, 17),
(1412, 11, 17),
(1413, 40, 17),
(1414, 30, 17),
(1415, 40, 17),
(1416, 14, 17),
(1417, NULL, 17),
(1418, 1, 17),
(1419, 44, 17),
(1420, 2, 17),
(1421, 44, 17),
(1422, 52, 18),
(1423, 51, 18),
(1424, 57, 18),
(1425, 51, 18),
(1426, 35, 18),
(1427, NULL, 18),
(1429, NULL, 18),
(1430, 3, 18),
(1431, 43, 18),
(1432, 4, 18),
(1433, 43, 18),
(1435, 43, 18),
(1436, 33, 18),
(1437, 43, 18),
(1438, 42, 18),
(1439, 56, 18),
(1440, 59, 18),
(1441, 56, 18),
(1442, 49, 18),
(1443, 56, 18),
(1444, 12, 18),
(1445, NULL, 18),
(1447, 17, 18),
(1448, 60, 18),
(1449, 17, 18),
(1450, 23, 18),
(1451, 17, 18),
(1452, 21, 18),
(1453, 17, 18),
(1454, 24, 18),
(1455, 17, 18),
(1456, 29, 18),
(1457, 17, 18),
(1458, 25, 18),
(1459, 17, 18),
(1460, 40, 18),
(1461, NULL, 18),
(1463, 40, 18),
(1464, 9, 18),
(1465, 40, 18),
(1466, 10, 18),
(1467, 40, 18),
(1468, 18, 18),
(1469, 40, 18),
(1470, 11, 18),
(1471, 40, 18),
(1472, 30, 18),
(1473, 40, 18),
(1474, 14, 18),
(1475, NULL, 18),
(1476, 1, 18),
(1477, 44, 18),
(1478, 2, 18),
(1479, 44, 18),
(1480, 52, 19),
(1481, 51, 19),
(1482, 57, 19),
(1483, 51, 19),
(1484, 35, 19),
(1485, NULL, 19),
(1487, NULL, 19),
(1488, 3, 19),
(1489, 43, 19),
(1490, 4, 19),
(1491, 43, 19),
(1493, 43, 19),
(1494, 33, 19),
(1495, 43, 19),
(1496, 42, 19),
(1497, 56, 19),
(1498, 59, 19),
(1499, 56, 19),
(1500, 49, 19),
(1501, 56, 19),
(1502, 12, 19),
(1503, NULL, 19),
(1505, 17, 19),
(1507, 17, 19),
(1508, 60, 19),
(1509, 17, 19),
(1510, 23, 19),
(1511, 17, 19),
(1512, 21, 19),
(1513, 17, 19),
(1514, 24, 19),
(1515, 17, 19),
(1516, 29, 19),
(1517, 17, 19),
(1518, 25, 19),
(1519, 17, 19),
(1520, 40, 19),
(1521, NULL, 19),
(1523, 40, 19),
(1524, 9, 19),
(1525, 40, 19),
(1526, 10, 19),
(1527, 40, 19),
(1528, 18, 19),
(1529, 40, 19),
(1530, 11, 19),
(1531, 40, 19),
(1532, 30, 19),
(1533, 40, 19),
(1534, 14, 19),
(1535, NULL, 19),
(1536, 1, 19),
(1537, 44, 19),
(1538, 2, 19),
(1539, 44, 19),
(1542, 61, 2),
(1545, 62, 3),
(1547, 62, 13),
(1548, 62, 2),
(1549, 62, 4),
(1551, 62, 14),
(1553, 39, 2),
(1624, 7, 2),
(1627, 26, 2),
(1628, 55, 2),
(1630, 27, 2),
(1631, 1, 9),
(1632, 44, 9),
(1633, 2, 9),
(1634, 44, 9),
(1635, 3, 9),
(1636, 43, 9),
(1637, 4, 9),
(1638, 43, 9),
(1639, 7, 9),
(1640, 9, 9),
(1641, 40, 9),
(1642, 10, 9),
(1643, 40, 9),
(1644, 11, 9),
(1645, 40, 9),
(1646, 12, 9),
(1647, 14, 9),
(1648, 18, 9),
(1649, 40, 9),
(1650, 21, 9),
(1651, 17, 9),
(1652, 23, 9),
(1653, 17, 9),
(1654, 24, 9),
(1655, 17, 9),
(1656, 25, 9),
(1657, 17, 9),
(1658, 27, 9),
(1659, 17, 9),
(1660, 30, 9),
(1661, 40, 9),
(1662, 33, 9),
(1663, 43, 9),
(1664, 35, 9),
(1665, 39, 9),
(1666, 43, 9),
(1667, 42, 9),
(1668, 56, 9),
(1669, 49, 9),
(1670, 56, 9),
(1671, 57, 9),
(1672, 51, 9),
(1673, 59, 9),
(1674, 56, 9),
(1675, 60, 9),
(1676, 17, 9),
(1677, 61, 9),
(1678, 40, 9),
(1679, 62, 9),
(1680, 40, 9),
(1681, NULL, 9),
(1682, 1, 10),
(1683, 44, 10),
(1684, 2, 10),
(1685, 44, 10),
(1686, 3, 10),
(1687, 43, 10),
(1688, 4, 10),
(1689, 43, 10),
(1690, 7, 10),
(1691, 9, 10),
(1692, 40, 10),
(1693, 10, 10),
(1694, 40, 10),
(1695, 11, 10),
(1696, 40, 10),
(1697, 12, 10),
(1698, 14, 10),
(1699, 18, 10),
(1700, 40, 10),
(1701, 30, 10),
(1702, 40, 10),
(1703, 33, 10),
(1704, 43, 10),
(1705, 35, 10),
(1706, 39, 10),
(1707, 43, 10),
(1708, 42, 10),
(1709, 56, 10),
(1710, 49, 10),
(1711, 56, 10),
(1712, 57, 10),
(1713, 51, 10),
(1714, 59, 10),
(1715, 56, 10),
(1716, 61, 10),
(1717, 40, 10),
(1718, 62, 10),
(1719, 40, 10),
(1720, 36, 1),
(1723, 64, 1),
(1724, 57, 13),
(1725, 51, 13),
(1726, 35, 13),
(1727, NULL, 13),
(1728, 7, 13),
(1729, NULL, 13),
(1730, 39, 13),
(1731, 43, 13),
(1732, 3, 13),
(1733, 43, 13),
(1734, 4, 13),
(1735, 43, 13),
(1736, 33, 13),
(1737, 43, 13),
(1738, 42, 13),
(1739, 56, 13),
(1740, 59, 13),
(1741, 56, 13),
(1742, 49, 13),
(1743, 56, 13),
(1744, 64, 13),
(1745, NULL, 13),
(1747, 64, 13),
(1748, 12, 13),
(1749, NULL, 13),
(1750, 27, 13),
(1751, 17, 13),
(1752, 60, 13),
(1753, 17, 13),
(1754, 23, 13),
(1755, 17, 13),
(1756, 21, 13),
(1757, 17, 13),
(1758, 24, 13),
(1759, 17, 13),
(1760, 29, 13),
(1761, 17, 13),
(1762, 25, 13),
(1763, 17, 13),
(1764, 40, 13),
(1765, NULL, 13),
(1766, 9, 13),
(1767, 40, 13),
(1768, 10, 13),
(1769, 40, 13),
(1770, 61, 13),
(1771, 40, 13),
(1772, 62, 13),
(1773, 40, 13),
(1774, 18, 13),
(1775, 40, 13),
(1776, 11, 13),
(1777, 40, 13),
(1778, 30, 13),
(1779, 40, 13),
(1780, 14, 13),
(1781, NULL, 13),
(1782, 1, 13),
(1783, 44, 13),
(1784, 2, 13),
(1785, 44, 13),
(1786, 63, 1),
(1787, 63, 13);

-- --------------------------------------------------------

--
-- Table structure for table `cms_moduls`
--

CREATE TABLE `cms_moduls` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `super_admin` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_moduls`
--

INSERT INTO `cms_moduls` (`id`, `name`, `icon`, `path`, `table_name`, `controller`, `is_protected`, `is_active`, `super_admin`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Notifications', 'fa fa-cog', 'notifications', 'cms_notifications', 'NotificationsController', 1, 1, 1, '2018-05-09 00:05:43', NULL, NULL),
(2, 'Privileges', 'fa fa-cog', 'privileges', 'cms_privileges', 'PrivilegesController', 1, 1, 1, '2018-05-09 00:05:43', NULL, NULL),
(3, 'Privileges Roles', 'fa fa-cog', 'privileges_roles', 'cms_privileges_roles', 'PrivilegesRolesController', 1, 1, 1, '2018-05-09 00:05:43', NULL, NULL),
(4, 'Users Management', 'fa fa-users', 'users', 'cms_users', 'AdminCmsUsersController', 0, 1, NULL, '2018-05-09 00:05:43', NULL, NULL),
(5, 'Settings', 'fa fa-cog', 'settings', 'cms_settings', 'SettingsController', 1, 1, 1, '2018-05-09 00:05:43', NULL, NULL),
(6, 'Module Generator', 'fa fa-database', 'module_generator', 'cms_moduls', 'ModulsController', 1, 1, 1, '2018-05-09 00:05:43', NULL, NULL),
(7, 'Menu Management', 'fa fa-bars', 'menu_management', 'cms_menus', 'MenusController', 1, 1, 1, '2018-05-09 00:05:43', NULL, NULL),
(8, 'Email Templates', 'fa fa-envelope-o', 'email_templates', 'cms_email_templates', 'EmailTemplatesController', 1, 1, 1, '2018-05-09 00:05:43', NULL, NULL),
(9, 'Statistic Builder', 'fa fa-dashboard', 'statistic_builder', 'cms_statistics', 'StatisticBuilderController', 1, 1, 1, '2018-05-09 00:05:43', NULL, NULL),
(10, 'API Generator', 'fa fa-cloud-download', 'api_generator', '', 'ApiCustomController', 1, 1, 1, '2018-05-09 00:05:43', NULL, NULL),
(11, 'Log User Access', 'fa fa-flag-o', 'logs', 'cms_logs', 'LogsController', 1, 1, NULL, '2018-05-09 00:05:43', NULL, NULL),
(13, 'Role', 'fa fa-user', 'roles', 'roles', 'AdminRolesController', 0, 0, NULL, '2018-05-09 20:23:37', NULL, NULL),
(14, 'User', 'fa fa-users', 'cms_users', 'cms_users', 'AdminCmsUsers1Controller', 0, 0, NULL, '2018-05-09 23:40:25', NULL, NULL),
(15, 'Category Menu', 'fa fa-tags', 'categories', 'categories', 'AdminCategoriesController', 0, 0, NULL, '2018-05-10 02:35:08', NULL, NULL),
(16, 'Menu', 'fa fa-th-large', 'menus', 'menus', 'AdminMenusController', 0, 0, NULL, '2018-05-10 21:27:11', NULL, NULL),
(18, 'Discount', 'fa fa-percent', 'discounts', 'discounts', 'AdminDiscountsController', 0, 0, NULL, '2018-05-11 02:20:34', NULL, NULL),
(20, 'Currency', 'fa fa-money', 'currencies', 'currencies', 'AdminCurrenciesController', 0, 0, NULL, '2018-05-11 02:39:31', NULL, NULL),
(21, 'Exchange Rate', 'fa fa-cloud-download', 'exchange', 'exchange', 'AdminExchangeController', 0, 0, NULL, '2018-05-11 02:42:26', NULL, NULL),
(22, 'UOM', 'fa fa-refresh', 'uom', 'uom', 'AdminUomController', 0, 0, NULL, '2018-05-17 19:05:53', NULL, NULL),
(23, 'Promotion Pack', 'fa fa-bullhorn', 'promotion_packs', 'promotion_packs', 'AdminPromotionPackController', 0, 0, NULL, '2018-05-18 02:01:41', NULL, NULL),
(25, 'Table Management', 'fa fa-table', 'table_numbers', 'table_numbers', 'AdminTableNumbersController', 0, 0, NULL, '2018-05-22 19:42:50', NULL, NULL),
(27, 'Outlet Setting', 'fa fa-cog', 'setting', 'settings', 'AdminSettingController', 0, 0, NULL, '2018-05-23 19:16:01', NULL, NULL),
(30, 'Sale by item', 'fa fa-dashboard', 'sale_by_item', 'orders', 'AdminSaleByItemController', 0, 0, NULL, '2018-05-24 00:00:34', NULL, NULL),
(31, 'Sale by customer', 'fa fa-users', 'sale_by_customer', 'orders', 'AdminSaleByCustomerController', 0, 0, NULL, '2018-05-24 03:31:53', NULL, NULL),
(32, 'Sale by Cashier', 'fa fa-glass', 'sale_by_cashier', 'orders', 'AdminSaleByCashierController', 0, 0, NULL, '2018-05-24 03:34:52', NULL, NULL),
(33, 'Sale by promotion pack', 'fa fa-glass', 'sale_by_promotion_pack', 'orders', 'AdminSaleByPromotionPackController', 0, 0, NULL, '2018-05-24 03:36:21', NULL, NULL),
(34, 'Void Payment', 'fa fa-glass', 'void_payment', 'orders', 'AdminVoidPaymentController', 0, 0, NULL, '2018-05-24 03:39:35', NULL, NULL),
(35, 'Credit memo report', 'fa fa-glass', 'credit_memo_report', 'orders', 'AdminCreditMemoReportController', 0, 0, NULL, '2018-05-24 03:41:37', NULL, NULL),
(36, 'Discount Summary', 'fa fa-percent', 'discount_summary', NULL, 'AdminDiscountSummaryController', 0, 0, NULL, '2018-05-24 03:43:08', NULL, NULL),
(38, 'Sale by invoice', 'fa fa-newspaper-o', 'sale_by_invoice', 'invoices', 'AdminSaleByInvoiceController', 0, 0, NULL, '2018-05-28 20:41:27', NULL, NULL),
(39, 'User Activity', 'fa fa-info-circle', 'user_activities', 'log_activities', 'AdminUserActivitiesController', 0, 0, NULL, '2018-05-31 03:09:11', NULL, NULL),
(42, 'Sub Category', 'fa fa-glass', 'subcategories', 'subcategories', 'AdminSubcategoryController', 0, 0, NULL, '2018-06-13 01:55:36', NULL, NULL),
(44, 'Dashboard', 'fa fa-dashboard', 'dashboard', 'menus', 'AdminDashboardController', 0, 0, NULL, '2018-06-22 04:51:29', NULL, NULL),
(45, 'Outlet User', 'fa fa-users', 'outlet_user', 'cms_users', 'AdminStoreUserController', 0, 0, 1, '2018-06-27 06:37:02', NULL, NULL),
(46, 'Business Plan', 'fa fa-tasks', 'business_plan', 'business_plan', 'AdminBusinessPlanController', 0, 0, 1, '2018-06-27 07:02:21', NULL, NULL),
(48, 'Addon', 'fa fa-inbox', 'addon', 'menu_materials', 'AdminMenuMaterialsController', 0, 0, NULL, '2018-06-29 04:08:10', NULL, NULL),
(49, 'Screen Request Order', 'fa fa-glass', 'Screen Request Order', 'orders_detail', 'AdminScreenRequestOrderController', 0, 0, 1, '2018-08-15 06:06:08', NULL, NULL),
(51, 'Dine In', 'fa fa-cutlery', 'dine_in', 'table_numbers', 'CashierTableManagementController', 0, 0, NULL, '2018-08-16 01:24:31', NULL, NULL),
(52, 'Revenue', 'fa fa-bar-chart-o', 'revenue', 'orders', 'AdminRevenueController', 0, 0, 1, '2018-08-31 06:56:38', NULL, NULL),
(53, 'Sale Count', 'fa fa-shopping-cart', 'sale_count', 'orders_detail', 'AdminSaleCountController', 0, 0, 1, '2018-08-31 08:09:26', NULL, NULL),
(54, 'Customer Count', 'fa fa-glass', 'customer_count', 'invoices', 'AdminCustomerCountController', 0, 0, 1, '2018-09-03 03:42:30', NULL, NULL),
(55, 'Sale By Store', 'fa fa-inbox', 'sale_by_store', 'invoices', 'AdminSaleByStoreController', 0, 0, 1, '2018-09-03 03:46:32', NULL, NULL),
(56, 'Take Out', 'fa fa-taxi', 'takeout', 'menus', 'AdminOrderController', 0, 0, NULL, '2018-09-03 03:50:03', NULL, NULL),
(58, 'Branch', 'fa fa-glass', 'branch', 'settings', 'AdminBranchController', 0, 0, NULL, '2018-09-06 02:46:55', NULL, NULL),
(59, 'invoice MG', 'fa fa-envelope-o', 'invoice_mg', 'invoices', 'AdminInvoiceMGController', 0, 0, NULL, '2018-09-25 06:58:58', NULL, NULL),
(60, 'Report Dashboard', 'fa fa-dashboard', 'report_dashboard', 'invoices', 'AdminReportDashboardController', 0, 0, NULL, '2018-10-01 01:55:02', NULL, NULL),
(61, 'Refund', 'fa fa-refresh', 'refund', 'invoices', 'AdminRefundController', 0, 1, NULL, '2018-10-28 17:00:00', NULL, NULL),
(62, 'Void', 'fa fa-times', 'void', 'invoices', 'AdminVoidController', 0, 1, NULL, '2018-10-28 17:00:00', NULL, NULL),
(63, 'Membership', 'fa fa-users', 'memberships', 'memberships', 'AdminMembershipsController', 0, 0, NULL, '2018-11-19 09:12:25', NULL, NULL),
(64, 'Order Setting', 'fa fa-cog', 'order_setting', 'settings', 'AdminOrderSettingController', 0, 0, NULL, '2018-11-27 04:30:30', NULL, NULL),
(65, 'Outlet', 'fa fa-home', 'outlet', 'settings', 'AdminOutletController', 0, 0, NULL, '2019-01-23 05:59:59', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_notifications`
--

CREATE TABLE `cms_notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_users` int(11) DEFAULT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_privileges`
--

CREATE TABLE `cms_privileges` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_superadmin` tinyint(1) DEFAULT NULL,
  `is_owner` int(11) DEFAULT '0',
  `theme_color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_privileges`
--

INSERT INTO `cms_privileges` (`id`, `name`, `is_superadmin`, `is_owner`, `theme_color`, `company_id`, `created_at`, `updated_at`) VALUES
(1, 'Super Administrator', 1, 1, 'skin-green', NULL, '2018-05-09 00:05:43', NULL),
(2, 'Admin,Legal', 0, 1, 'skin-green', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_privileges_roles`
--

CREATE TABLE `cms_privileges_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_visible` tinyint(1) DEFAULT NULL,
  `is_create` tinyint(1) DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `is_edit` tinyint(1) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `id_cms_moduls` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_privileges_roles`
--

INSERT INTO `cms_privileges_roles` (`id`, `is_visible`, `is_create`, `is_read`, `is_edit`, `is_delete`, `id_cms_privileges`, `id_cms_moduls`, `created_at`, `updated_at`) VALUES
(178, 1, 1, 1, 1, 1, 1, 46, NULL, NULL),
(179, 1, 1, 1, 1, 1, 1, 15, NULL, NULL),
(180, 1, 1, 1, 1, 1, 1, 35, NULL, NULL),
(181, 1, 1, 1, 1, 1, 1, 20, NULL, NULL),
(182, 1, 1, 1, 1, 1, 1, 44, NULL, NULL),
(183, 1, 1, 1, 1, 1, 1, 18, NULL, NULL),
(184, 1, 1, 1, 1, 1, 1, 36, NULL, NULL),
(185, 1, 1, 1, 1, 1, 1, 21, NULL, NULL),
(186, 1, 1, 1, 1, 1, 1, 16, NULL, NULL),
(187, 1, 1, 1, 1, 1, 1, 48, NULL, NULL),
(188, 1, 1, 1, 1, 1, 1, 23, NULL, NULL),
(189, 1, 1, 1, 1, 1, 1, 13, NULL, NULL),
(190, 1, 1, 1, 1, 1, 1, 31, NULL, NULL),
(191, 1, 1, 1, 1, 1, 1, 38, NULL, NULL),
(192, 1, 1, 1, 1, 1, 1, 30, NULL, NULL),
(193, 1, 1, 1, 1, 1, 1, 33, NULL, NULL),
(194, 1, 1, 1, 1, 1, 1, 32, NULL, NULL),
(195, 1, 1, 1, 1, 1, 1, 27, NULL, NULL),
(196, 1, 1, 1, 1, 1, 1, 45, NULL, NULL),
(197, 1, 1, 1, 1, 1, 1, 42, NULL, NULL),
(198, 1, 1, 1, 1, 1, 1, 25, NULL, NULL),
(199, 1, 1, 1, 1, 1, 1, 22, NULL, NULL),
(200, 1, 1, 1, 1, 1, 1, 14, NULL, NULL),
(201, 1, 1, 1, 1, 1, 1, 39, NULL, NULL),
(202, 1, 1, 1, 1, 1, 1, 4, NULL, NULL),
(203, 1, 1, 1, 1, 1, 1, 34, NULL, NULL),
(229, 1, 1, 1, 1, 1, 1, 49, NULL, NULL),
(230, 1, 1, 1, 1, 1, 1, 50, NULL, NULL),
(231, 1, 1, 1, 1, 1, 1, 51, NULL, NULL),
(255, 1, 1, 1, 1, 1, 1, 52, NULL, NULL),
(280, 1, 1, 1, 1, 1, 1, 53, NULL, NULL),
(306, 1, 1, 1, 1, 1, 1, 54, NULL, NULL),
(333, 1, 1, 1, 1, 1, 1, 55, NULL, NULL),
(361, 1, 1, 1, 1, 1, 1, 56, NULL, NULL),
(390, 1, 1, 1, 1, 1, 1, 57, NULL, NULL),
(391, 1, 1, 1, 1, 1, 1, 58, NULL, NULL),
(545, 1, 1, 1, 1, 1, 1, 59, NULL, NULL),
(727, 1, 1, 1, 1, 1, 1, 60, NULL, NULL),
(1113, 1, 1, 1, 1, 1, 1, 63, NULL, NULL),
(1148, 1, 1, 1, 1, 1, 1, 64, NULL, NULL),
(1149, 1, 1, 1, 1, 1, 2, 58, NULL, NULL),
(1150, 1, 1, 1, 1, 1, 2, 15, NULL, NULL),
(1151, 1, 1, 1, 1, 1, 2, 35, NULL, NULL),
(1152, 1, 0, 0, 0, 0, 2, 20, NULL, NULL),
(1153, 1, 0, 0, 0, 0, 2, 54, NULL, NULL),
(1154, 1, 1, 1, 1, 1, 2, 44, NULL, NULL),
(1155, 1, 1, 1, 1, 1, 2, 51, NULL, NULL),
(1156, 1, 1, 1, 1, 1, 2, 18, NULL, NULL),
(1157, 1, 1, 1, 1, 1, 2, 36, NULL, NULL),
(1158, 1, 1, 1, 1, 1, 2, 21, NULL, NULL),
(1159, 1, 1, 1, 1, 1, 2, 59, NULL, NULL),
(1160, 1, 1, 1, 1, 1, 2, 63, NULL, NULL),
(1161, 1, 1, 1, 1, 1, 2, 16, NULL, NULL),
(1162, 1, 1, 1, 1, 1, 2, 48, NULL, NULL),
(1163, 1, 1, 1, 1, 1, 2, 64, NULL, NULL),
(1164, 1, 1, 1, 1, 1, 2, 45, NULL, NULL),
(1165, 0, 0, 1, 1, 0, 2, 27, NULL, NULL),
(1166, 1, 1, 1, 1, 1, 2, 23, NULL, NULL),
(1167, 1, 1, 1, 1, 1, 2, 60, NULL, NULL),
(1168, 1, 0, 0, 0, 0, 2, 52, NULL, NULL),
(1169, 1, 1, 1, 1, 1, 2, 13, NULL, NULL),
(1170, 1, 1, 1, 1, 1, 2, 32, NULL, NULL),
(1171, 1, 1, 1, 1, 1, 2, 38, NULL, NULL),
(1172, 1, 1, 1, 1, 1, 2, 30, NULL, NULL),
(1173, 1, 1, 1, 1, 1, 2, 33, NULL, NULL),
(1174, 1, 0, 0, 0, 0, 2, 55, NULL, NULL),
(1175, 1, 0, 0, 0, 0, 2, 53, NULL, NULL),
(1176, 1, 1, 1, 1, 1, 2, 42, NULL, NULL),
(1177, 1, 1, 1, 1, 1, 2, 25, NULL, NULL),
(1178, 1, 0, 0, 0, 0, 2, 56, NULL, NULL),
(1179, 1, 1, 1, 1, 1, 2, 22, NULL, NULL),
(1180, 1, 1, 1, 1, 1, 2, 14, NULL, NULL),
(1181, 1, 1, 1, 1, 1, 2, 39, NULL, NULL),
(1182, 1, 1, 1, 1, 1, 2, 4, NULL, NULL),
(1183, 1, 1, 1, 1, 1, 2, 34, NULL, NULL),
(1253, 1, 1, 1, 1, 1, 1, 65, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_settings`
--

CREATE TABLE `cms_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `content_input_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dataenum` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `helper` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `group_setting` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_settings`
--

INSERT INTO `cms_settings` (`id`, `name`, `content`, `content_input_type`, `dataenum`, `helper`, `created_at`, `updated_at`, `group_setting`, `label`) VALUES
(1, 'login_background_color', NULL, 'text', NULL, 'Input hexacode', '2018-05-09 00:05:43', NULL, 'Login Register Style', 'Login Background Color'),
(2, 'login_font_color', NULL, 'text', NULL, 'Input hexacode', '2018-05-09 00:05:43', NULL, 'Login Register Style', 'Login Font Color'),
(3, 'login_background_image', 'uploads/2019-01/69e85336ad90b02787cf4adb7968e6d3.jpg', 'upload_image', NULL, NULL, '2018-05-09 00:05:43', NULL, 'Login Register Style', 'Login Background Image'),
(4, 'email_sender', 'channvuthy.plan.b@gmail.com', 'text', NULL, NULL, '2018-05-09 00:05:43', NULL, 'Email Setting', 'Email Sender'),
(5, 'smtp_driver', 'smtp', 'select', 'smtp,mail,sendmail', NULL, '2018-05-09 00:05:43', NULL, 'Email Setting', 'Mail Driver'),
(6, 'smtp_host', 'smtp.gmail.com', 'text', NULL, NULL, '2018-05-09 00:05:43', NULL, 'Email Setting', 'SMTP Host'),
(7, 'smtp_port', '587', 'text', NULL, 'default 25', '2018-05-09 00:05:43', NULL, 'Email Setting', 'SMTP Port'),
(8, 'smtp_username', 'channvuthy.plan.b@gmail.com', 'text', NULL, NULL, '2018-05-09 00:05:43', NULL, 'Email Setting', 'SMTP Username'),
(9, 'smtp_password', 'ab06sia==1b!', 'text', NULL, NULL, '2018-05-09 00:05:43', NULL, 'Email Setting', 'SMTP Password'),
(10, 'appname', 'E-Menu', 'text', NULL, NULL, '2018-05-09 00:05:43', NULL, 'Application Setting', 'Application Name'),
(11, 'default_paper_size', 'Legal', 'text', NULL, 'Paper size, ex : A4, Legal, etc', '2018-05-09 00:05:43', NULL, 'Application Setting', 'Default Paper Print Size'),
(12, 'logo', 'uploads/2018-09/20be0d953dd0a6f17e3f17dfcfbdc406.png', 'upload_image', NULL, NULL, '2018-05-09 00:05:43', NULL, 'Application Setting', 'Logo'),
(13, 'favicon', NULL, 'upload_image', NULL, NULL, '2018-05-09 00:05:43', NULL, 'Application Setting', 'Favicon'),
(14, 'api_debug_mode', 'true', 'select', 'true,false', NULL, '2018-05-09 00:05:43', NULL, 'Application Setting', 'API Debug Mode'),
(15, 'google_api_key', NULL, 'text', NULL, NULL, '2018-05-09 00:05:43', NULL, 'Application Setting', 'Google API Key'),
(16, 'google_fcm_key', NULL, 'text', NULL, NULL, '2018-05-09 00:05:43', NULL, 'Application Setting', 'Google FCM Key');

-- --------------------------------------------------------

--
-- Table structure for table `cms_statistics`
--

CREATE TABLE `cms_statistics` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_statistic_components`
--

CREATE TABLE `cms_statistic_components` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_statistics` int(11) DEFAULT NULL,
  `componentID` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `component_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area_name` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `config` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_statistic_components`
--

INSERT INTO `cms_statistic_components` (`id`, `id_cms_statistics`, `componentID`, `component_name`, `area_name`, `sorting`, `name`, `config`, `created_at`, `updated_at`) VALUES
(2, 1, '5c03a13b73dc23cc908e5006665d0a69', 'chartarea', NULL, 0, 'Untitled', NULL, '2018-10-01 05:06:54', NULL),
(3, 1, 'eaf0b3338d4af24b643e7f7591e00d1d', 'chartarea', NULL, 0, 'Untitled', NULL, '2018-10-01 05:07:04', NULL),
(4, 1, '6a9b882ca26900b73b4b3b7260ea357b', 'smallbox', 'area1', 0, 'Untitled', NULL, '2018-11-13 03:45:19', NULL),
(5, 1, 'f332068065d6f0c62b96dd70ac6306fd', 'smallbox', 'area3', 0, 'Untitled', NULL, '2018-11-13 03:45:21', NULL),
(6, 1, '46ab75a7c4c3914e278d13d54541e48f', 'smallbox', NULL, 0, 'Untitled', NULL, '2018-11-13 03:45:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_users`
--

CREATE TABLE `cms_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_pin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_pin` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(23) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_type` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'end_user',
  `created_by` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT '1',
  `discount_privilege` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_users`
--

INSERT INTO `cms_users` (`id`, `name`, `photo`, `email`, `phone`, `password`, `user_pin`, `company_pin`, `role`, `user_type`, `created_by`, `company_id`, `id_cms_privileges`, `discount_privilege`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Super Admin', 'uploads/1/2019-01/plan_b_logo.png', 'admin@system.com', '', '$2y$10$NG1.FkEMcKGDvpZqJ25ybOY6X4Gq.F7MQmU7g.nc83kUgZ7SUR4F2', NULL, '', '1', 'user_system', 0, NULL, 1, 0, '2018-05-09 00:05:43', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `country_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_timezone` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `country_name`, `country_timezone`) VALUES
(1, 'Cambodia', NULL),
(2, 'Thailand', NULL),
(3, 'Japan', NULL),
(4, 'China', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_symbol` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_letter` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `name`, `currency_symbol`, `currency_letter`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Dollar', '$', '$', 1, '2018-08-14 10:17:28', '2018-08-20 03:43:52'),
(2, 'Riel', '៛', 'R', 1, '2018-10-29 10:59:52', NULL),
(3, 'Baht', '฿', 'B', 1, '2018-08-15 08:50:31', '2018-10-29 11:06:44');

-- --------------------------------------------------------

--
-- Table structure for table `dashboard_settings`
--

CREATE TABLE `dashboard_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_owner` tinyint(4) DEFAULT '0',
  `income` tinyint(4) DEFAULT '0',
  `user_management` tinyint(1) DEFAULT '0',
  `menu` tinyint(1) DEFAULT '0',
  `promotion_pack` tinyint(1) DEFAULT '0',
  `table_management` tinyint(1) DEFAULT '0',
  `store_setting` tinyint(1) DEFAULT '0',
  `table_sold` tinyint(4) DEFAULT '0',
  `sold_total` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `dashboard_settings`
--

INSERT INTO `dashboard_settings` (`id`, `user_id`, `store_owner`, `income`, `user_management`, `menu`, `promotion_pack`, `table_management`, `store_setting`, `table_sold`, `sold_total`) VALUES
(1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE `discounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `discount_type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` double NOT NULL,
  `created_by` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `exchange`
--

CREATE TABLE `exchange` (
  `id` int(10) UNSIGNED NOT NULL,
  `currency_from` int(11) NOT NULL,
  `currency_from_text` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_to` int(11) NOT NULL,
  `currency_to_text` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price_from` double(6,2) NOT NULL,
  `price_to` double(10,6) NOT NULL,
  `created_by` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `invoice_id` int(10) UNSIGNED NOT NULL,
  `invoice_date` date NOT NULL,
  `table_id` int(11) DEFAULT NULL,
  `order_code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `invoice_number` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `promotion_pack_id` int(11) DEFAULT NULL,
  `currency` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `void` tinyint(1) NOT NULL DEFAULT '0',
  `grand_total` double(8,2) DEFAULT NULL,
  `grand_dollar_exchange` double(8,2) DEFAULT NULL,
  `grand_riel_exchange` double(10,2) DEFAULT NULL,
  `grand_baht_exchange` double(8,2) DEFAULT NULL,
  `tax_dollar` double(6,2) DEFAULT NULL,
  `discount_type` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount_total_dollar` double(8,2) DEFAULT NULL,
  `grand_total_after_refund` double(8,2) DEFAULT NULL,
  `refund_amount` double(6,2) DEFAULT NULL,
  `is_refund` tinyint(1) DEFAULT '0',
  `has_paid` int(11) DEFAULT NULL,
  `paid_total` double(8,2) DEFAULT NULL,
  `paid_dollar` double(8,2) DEFAULT NULL,
  `paid_riel` double(8,2) DEFAULT NULL,
  `paid_baht` double(8,2) DEFAULT NULL,
  `return_amount` double(8,2) DEFAULT NULL,
  `return_dollar` double(10,2) DEFAULT NULL,
  `return_riel` double(10,2) DEFAULT NULL,
  `return_baht` double(10,2) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_orders`
--

CREATE TABLE `invoice_orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `item_size` int(11) DEFAULT NULL,
  `addon_id` int(11) DEFAULT '0',
  `qty` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `company_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `language_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `language_code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `language_name`, `language_code`) VALUES
(1, 'English', 'EN'),
(2, 'Khmer', 'KH');

-- --------------------------------------------------------

--
-- Table structure for table `memberships`
--

CREATE TABLE `memberships` (
  `id` int(10) UNSIGNED NOT NULL,
  `f_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `l_name` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `membership_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `expire_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_code` int(100) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double(6,2) DEFAULT NULL,
  `currency` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `subcategory_id` int(11) DEFAULT NULL,
  `uom_size` int(11) DEFAULT '0',
  `material_size` int(11) DEFAULT '0',
  `sale_amount` int(11) DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menu_detail`
--

CREATE TABLE `menu_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menu_materials`
--

CREATE TABLE `menu_materials` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double NOT NULL,
  `currency` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `menu_uom_price`
--

CREATE TABLE `menu_uom_price` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(11) NOT NULL,
  `uom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uom_price` double NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2016_08_07_145904_add_table_cms_apicustom', 1),
(2, '2016_08_07_150834_add_table_cms_dashboard', 1),
(3, '2016_08_07_151210_add_table_cms_logs', 1),
(4, '2016_08_07_151211_add_details_cms_logs', 1),
(5, '2016_08_07_152014_add_table_cms_privileges', 1),
(6, '2016_08_07_152214_add_table_cms_privileges_roles', 1),
(7, '2016_08_07_152320_add_table_cms_settings', 1),
(8, '2016_08_07_152421_add_table_cms_users', 1),
(9, '2016_08_07_154624_add_table_cms_menus_privileges', 1),
(10, '2016_08_07_154624_add_table_cms_moduls', 1),
(11, '2016_08_17_225409_add_status_cms_users', 1),
(12, '2016_08_20_125418_add_table_cms_notifications', 1),
(13, '2016_09_04_033706_add_table_cms_email_queues', 1),
(14, '2016_09_16_035347_add_group_setting', 1),
(15, '2016_09_16_045425_add_label_setting', 1),
(16, '2016_09_17_104728_create_nullable_cms_apicustom', 1),
(17, '2016_10_01_141740_add_method_type_apicustom', 1),
(18, '2016_10_01_141846_add_parameters_apicustom', 1),
(19, '2016_10_01_141934_add_responses_apicustom', 1),
(20, '2016_10_01_144826_add_table_apikey', 1),
(21, '2016_11_14_141657_create_cms_menus', 1),
(22, '2016_11_15_132350_create_cms_email_templates', 1),
(23, '2016_11_15_190410_create_cms_statistics', 1),
(24, '2016_11_17_102740_create_cms_statistic_components', 1),
(25, '2017_06_06_164501_add_deleted_at_cms_moduls', 1),
(26, '2018_05_10_032119_create_roles_table', 2),
(27, '2018_05_10_093226_create_table_category', 3),
(28, '2018_05_11_041626_create_table_menus', 4),
(29, '2018_05_11_060800_create_table_setting', 5),
(30, '2018_05_11_062513_create_table_table_numbers', 6),
(31, '2018_05_11_062637_create_table_discount', 6),
(32, '2018_05_11_062730_create_table_exange', 6),
(33, '2018_05_11_093758_create_table_currencies', 7),
(34, '2018_05_17_074008_create_promotion_pack', 8),
(35, '2018_05_17_074447_create_promotion_packs', 9),
(36, '2018_05_18_020144_create_table_uom', 9),
(37, '2018_05_18_084700_create_table_promotion_packs', 10),
(38, '2018_05_22_040958_create_promotion_type', 11),
(39, '2018_05_22_075915_create_promotion_detail', 12),
(40, '2018_05_24_025449_create_log_activity_table', 13),
(41, '2018_05_24_040842_create_log_activity_table1', 14),
(42, '2018_05_24_055239_create_table_order', 15),
(43, '2018_05_24_061846_create_table_invoice', 16),
(44, '2018_05_25_015525_create_order_detail', 17),
(45, '2018_06_04_025447_create_activity_log_table', 18),
(46, '2017_11_04_103444_create_laravel_logger_activity_table', 19),
(47, '2018_06_06_083040_create_tmp_order_table', 20),
(48, '2018_06_13_085235_create_subcategory_table', 21),
(49, '2018_06_27_135705_create_business_table', 22),
(50, '2018_06_29_110344_create_menu_meterial_table', 23),
(51, '2018_06_29_124508_create_menu_detail_table', 24),
(52, '2018_07_16_110012_create_dashboard_setting', 25),
(53, '2018_08_08_161413_create_country_table', 26),
(54, '2018_08_10_090511_create_language_table', 27),
(55, '2018_08_10_174544_create_defualt_currency_table', 28),
(56, '2018_08_11_124211_create_tax_type_table', 29),
(57, '2018_08_29_092355_create_uom_price_table', 30),
(58, '2018_09_06_093341_create_branches_table', 31),
(59, '2018_10_05_103855_create_reports_table', 32),
(60, '2018_11_19_153645_create_membership_table', 33);

-- --------------------------------------------------------

--
-- Table structure for table `printer`
--

CREATE TABLE `printer` (
  `id` int(11) NOT NULL,
  `printer_name` text,
  `printer_type` varchar(100) NOT NULL,
  `company_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `printer`
--

INSERT INTO `printer` (`id`, `printer_name`, `printer_type`, `company_id`, `status`) VALUES
(1, NULL, 'invoice', 1, 1),
(2, NULL, 'ticket', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `promotion_detail`
--

CREATE TABLE `promotion_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `promotion_id` int(11) NOT NULL,
  `menus_id` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `promotion_packs`
--

CREATE TABLE `promotion_packs` (
  `id` int(10) UNSIGNED NOT NULL,
  `promotion_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `promotion_type_id` int(11) NOT NULL,
  `promotion` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount_type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount_amount` float DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `promotion_price` double DEFAULT NULL,
  `promotion_menu_id` int(11) DEFAULT NULL,
  `promotion_qty` int(11) DEFAULT NULL,
  `promotion_event` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `promotion_type`
--

CREATE TABLE `promotion_type` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `promotion_type`
--

INSERT INTO `promotion_type` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Promotion by member', 0, '2018-07-01 17:00:00', '2018-07-01 17:00:00'),
(2, 'Promotion by price', 1, '2018-07-01 17:00:00', NULL),
(3, 'Promotion by menu', 0, '2018-07-01 17:00:00', NULL),
(4, 'Promotion by event', 1, '2018-07-01 17:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
  `id` int(10) UNSIGNED NOT NULL,
  `report_date` date DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sold_amount` double(8,2) NOT NULL DEFAULT '0.00',
  `table_sold` int(11) NOT NULL DEFAULT '0',
  `customer_count` int(11) NOT NULL DEFAULT '0',
  `discount` double(8,2) NOT NULL DEFAULT '0.00',
  `sub_total` double(8,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_info` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `company_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_policy` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_lang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_currency` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `company_old_currency` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `include_tax` tinyint(4) DEFAULT NULL,
  `company_sale_tax` double DEFAULT NULL,
  `company_tax_type_id` int(11) DEFAULT NULL,
  `company_pin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `print_setting` int(11) NOT NULL DEFAULT '3',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `subscription_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE `subcategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `table_numbers`
--

CREATE TABLE `table_numbers` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `size` int(11) DEFAULT NULL,
  `floor` int(11) DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `merge_with` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_merge` int(11) DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tax_types`
--

CREATE TABLE `tax_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `tax_information` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tax_types`
--

INSERT INTO `tax_types` (`id`, `tax_information`) VALUES
(1, 'Menu prices already include taxes'),
(2, 'Apply tax on top of my menu price');

-- --------------------------------------------------------

--
-- Table structure for table `uom`
--

CREATE TABLE `uom` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duplicate` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `business_plan`
--
ALTER TABLE `business_plan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_apicustom`
--
ALTER TABLE `cms_apicustom`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_apikey`
--
ALTER TABLE `cms_apikey`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_dashboard`
--
ALTER TABLE `cms_dashboard`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_email_queues`
--
ALTER TABLE `cms_email_queues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_email_templates`
--
ALTER TABLE `cms_email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_logs`
--
ALTER TABLE `cms_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_menus`
--
ALTER TABLE `cms_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_menus_privileges`
--
ALTER TABLE `cms_menus_privileges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_moduls`
--
ALTER TABLE `cms_moduls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_notifications`
--
ALTER TABLE `cms_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_privileges`
--
ALTER TABLE `cms_privileges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_privileges_roles`
--
ALTER TABLE `cms_privileges_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_settings`
--
ALTER TABLE `cms_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_statistics`
--
ALTER TABLE `cms_statistics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_statistic_components`
--
ALTER TABLE `cms_statistic_components`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_users`
--
ALTER TABLE `cms_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dashboard_settings`
--
ALTER TABLE `dashboard_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discounts`
--
ALTER TABLE `discounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exchange`
--
ALTER TABLE `exchange`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`invoice_id`);

--
-- Indexes for table `invoice_orders`
--
ALTER TABLE `invoice_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `memberships`
--
ALTER TABLE `memberships`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_detail`
--
ALTER TABLE `menu_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_materials`
--
ALTER TABLE `menu_materials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_uom_price`
--
ALTER TABLE `menu_uom_price`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `printer`
--
ALTER TABLE `printer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promotion_detail`
--
ALTER TABLE `promotion_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promotion_packs`
--
ALTER TABLE `promotion_packs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promotion_type`
--
ALTER TABLE `promotion_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `table_numbers`
--
ALTER TABLE `table_numbers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tax_types`
--
ALTER TABLE `tax_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uom`
--
ALTER TABLE `uom`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `business_plan`
--
ALTER TABLE `business_plan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_apicustom`
--
ALTER TABLE `cms_apicustom`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_apikey`
--
ALTER TABLE `cms_apikey`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_dashboard`
--
ALTER TABLE `cms_dashboard`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_email_queues`
--
ALTER TABLE `cms_email_queues`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_email_templates`
--
ALTER TABLE `cms_email_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cms_logs`
--
ALTER TABLE `cms_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cms_menus`
--
ALTER TABLE `cms_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `cms_menus_privileges`
--
ALTER TABLE `cms_menus_privileges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1788;

--
-- AUTO_INCREMENT for table `cms_moduls`
--
ALTER TABLE `cms_moduls`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `cms_notifications`
--
ALTER TABLE `cms_notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_privileges`
--
ALTER TABLE `cms_privileges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cms_privileges_roles`
--
ALTER TABLE `cms_privileges_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1254;

--
-- AUTO_INCREMENT for table `cms_settings`
--
ALTER TABLE `cms_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `cms_statistics`
--
ALTER TABLE `cms_statistics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_statistic_components`
--
ALTER TABLE `cms_statistic_components`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `cms_users`
--
ALTER TABLE `cms_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `dashboard_settings`
--
ALTER TABLE `dashboard_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `discounts`
--
ALTER TABLE `discounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `exchange`
--
ALTER TABLE `exchange`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `invoice_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_orders`
--
ALTER TABLE `invoice_orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `memberships`
--
ALTER TABLE `memberships`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menu_detail`
--
ALTER TABLE `menu_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menu_materials`
--
ALTER TABLE `menu_materials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menu_uom_price`
--
ALTER TABLE `menu_uom_price`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `printer`
--
ALTER TABLE `printer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `promotion_detail`
--
ALTER TABLE `promotion_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `promotion_packs`
--
ALTER TABLE `promotion_packs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `promotion_type`
--
ALTER TABLE `promotion_type`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subcategories`
--
ALTER TABLE `subcategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_numbers`
--
ALTER TABLE `table_numbers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tax_types`
--
ALTER TABLE `tax_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `uom`
--
ALTER TABLE `uom`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
