@extends("crudbooster::admin_template")

@section("content")

	<form action="{{ route('sender') }}" method="POST" class="form-horizontal">
	    <input type="hidden" name="_method" value="POST">
	    <input type="hidden" name="_token" value="{{ csrf_token() }}">
	    <input type="text" name="name" class="form-control">
	    <button type="submit" class="btn btn-primary">Submit</button>
	</form>


@endsection