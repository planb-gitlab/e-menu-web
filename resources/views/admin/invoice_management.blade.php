@extends('crudbooster::admin_template')
@section('content')
<!-- css Link -->
<link rel="stylesheet" href="{{ asset('css/sale_by_invoice.css') }}">
<link rel="stylesheet" href="{{ asset('css/loading.css') }}">
<link   rel="stylesheet" href="{{ asset('css/template.css') }}">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
	<script>

		$(window).load(function() {
			$(".se-pre-con-detail").fadeOut("slow");;
		});
		var url="{{URL::to('/')}}";
	</script>

<body id="invoice">

	<!-- Option for choosing filter -->
	<div class="container-fluid">
	  <div class="row filter">

	  	<!-- Time Filter -->
	    <div class="col-md-3">
	      <div class="row">
	        <div class="form-group">
	            <label for="sel1">{{trans('order.Select Sort (This week,month,year):')}}</label>
	            <select class="form-control btnsort" id="by" name='by'>
	              <option>Please Choose... </option>
	              <option>Weekly</option>              
	              <option>Monthly</option>
	              <option>Yearly</option>
	            </select>
	        </div>
	      </div>
	    </div>
	    <!-- End Time Filter -->

	    <!-- Date Filter -->
	    <div class="col-md-4">
	      <div class="form-group">
	        <input type="text" class="hidden" autofocus>
	        <div class="form-group timepicker">
	          <label>{{trans('order.From Date')}}</label>
	          <input type="text" readonly="" name="daterange" class="form-control btn_date" placeholder="Please choose day" />
	           <span style="pointer-events: none;" class="input-group-addon">
	            <span  class="glyphicon glyphicon-calendar"></span>
	        </span>
	        </div>
	      </div>
	    </div>
	    <div class="col-lg-3">
	    	<div class="form-group">
	    		<div class="search_by_id">
	    			<label>{{trans('order.Search Invoice By ID')}}</label>
	    			<input  type="text" class="input_id form-control" placeholder="Please Input invoice id" />
	    			<div class="icon_search">
	    				<button class="icon_search btn btn-default fa fa-search"></button> 
	    			</div>
	    		</div>
	    	</div>
	    </div>
	    <!-- End Date Filter -->
	    
	  </div>
	</div>

	 <div class="modal fade" id="invoice_modal" role="dialog" style="padding-right: 0 !important;" tabindex='-1'>
            <div class="modal-dialog">
            
              <!-- Modal content-->
              <div class="modal-content" id="modal_invoice">
              </div>

              <div class="modal-footer">
                 
                </div>

              
            </div>
    </div>
	<!-- End of Option for choosing filter -->


	<div class="col-md-12">
	  <div class="row">
	    <div class="bg_report">
	     

	      <div class="tab-content">
	        {{-- menu 1  --}}
	        <div id="home" class="tab-pane fade in active">
	          <table class="table table-striped table-bordered table-hover">
	            <thead class="dir_table_thead">
	              <tr>
	                <th class="col-md-1">{{trans('order.No.')}}</th>
	                <th class="col-md-2">{{trans('order.Invoice ID')}}</th>
	                <th>{{trans('order.Invoice Date')}}</th>
	                <th>{{trans('order.Total Price')}}</th>
	                <th>{{trans('order.Action')}}</th>
	              </tr>
	            </thead>
	            <tbody class="dir_table">
	              <tr>
	                <td colspan="5" class="text-center"><h4>{{trans('order.No Data!')}}</h4></td>
	              </tr>
	            </tbody>
	          </table>
	          
	        </div>
	       
	        </div>
	      </div>
	    </div>
	  </div>

	<div class="se-pre-con-detail"></div>



	<!-- Popup view for view and print -->
	<div id="view_invoice_blade">
	</div>	

 <div class="modal fade" id="invoice_modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content modal_content">
        <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">{{trans('order.Modal Header')}}</h4>
        </div>
        <div class="modal-body">
          <p>{{trans('order.Some text in the modal.')}}</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('order.Close')}}</button>
        </div>
      </div>
      
    </div>
  </div>

 </body>
@endsection

<!-- Include Required Prerequisites -->
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
 
<!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link   rel="stylesheet" href="{{ asset('vendor/crudbooster/assets/adminlte/bootstrap/css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

<script type="text/javascript">
	$(function(){
		$('body').on('change', '.btnsort', function(e) {
		      var by = $('.btnsort').val();
		      alert_timer();
			  clear_filter('','custom_date','invoice_date');

		      jQuery.ajax({
		          url: "{{ route('sort_invoice_invoicemanagement') }}",
		          type: 'POST',
		          data: { by: by , _token:"{{Session::token()}}"},
		          dataType: "html",
		          success: function (data) {
		              
		              $('.dir_table').html(data)
		            
		          }
		      })
		    });

		$('body').on('focus','.btn_date',function(){
		      $('.btnsort').prop('checked', false);
		      $('input[name="daterange"]').daterangepicker({
		        format: 'YYYY-MM-DD'
		        });
		    });

		$('body').on('click', '.applyBtn', function(event) {
		        var date = $('.btn_date').val();
		        alert_timer();
				clear_filter('','custom_date','invoice_date');
		        
		        jQuery.ajax({
		            url: "{{ route('sort_invoice_invoicemanagement') }}",
		            type: 'POST',
		            data: { date: date, _token:"{{Session::token()}}"},
		            dataType: "html",
		            success: function (data) {
		               
		                $('.dir_table').html(data)
		              
		            }
		        
		    });
		  });
		
		$(document).on('click','.icon_search',function(){
			var id 	= 	$('.input_id').val();
			alert_timer();
			clear_filter('filter_date','custom_date','');
			jQuery.ajax({
				url:"{{ route('invoicemanagement_search_invoice_byid') }}",
				type:'POST',
				data:{id:id, _token:"{{ Session::token()}}"},
				dataType:"html",
				success:function(data){
					 $('.dir_table').html(data)
				}
			});
		});
		
		
    function PrintInvoice(elem)
    {   
        var siteUrl     = "{{ asset('css/template.css') }}";
        var contents = $("#"+elem).html();
        var frame1      = $('<iframe />');
        frame1[0].name  = "frame1";
        frame1.css({ "position": "absolute", "top": "-1000000px" });
        $("body").append(frame1);
        var frameDoc    = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html><head><title>DIV Contents</title>');
        //Append the external CSS file.
        frameDoc.document.write('<link href="'+siteUrl+'" rel="stylesheet" type="text/css" />');
        //Append the DIV contents.
        frameDoc.document.write('</head><body>');
        
        frameDoc.document.write(contents);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);
        // window.onfocus = function () 
        // { setTimeout(function () 
        //     { window.close(); }, 100); 
        // }
    }
    $(document).on('click','.button_print',function(){
		PrintInvoice("container");
	});
		
	});

	
</script>


<!-- =============== START OF FUNCTION ================ -->
<script type="text/javascript">
	function alert_timer(){
		swal({
			  title: 'Searching in progress',
			  text: 'Please wait patiently',
			  timer: 500,
			  showCancelButton: false,
			  showConfirmButton: false
			});
	}
</script>

<script type="text/javascript">
  function clear_filter(filter0,filter1,filter2){
      // Filter data
      if(filter0 == "filter_date"){
        $('.btnsort').prop('checked', false);
        $('.btnsort').val();
        $('.btnsort option:eq(0)').prop('selected', true);
      }

      // Filter Custom Date
      if(filter1 == "custom_date"){
          $('.btn_date').val("");
      }

      // Filter By Invoice Code
      if(filter2 == "invoice_code"){
      	$('.input_id').val("");
      }



  }
</script>
<!-- =============== END OF FUNCTION ================ -->



<style>

.refund_button {
    bottom: 75px;
    position: relative;
    right: 41px;
    float: right;
}
.cross{
	margin-left: 8px;
}
.modal-title{
	display: inline-block;
}

.modal-dialog{
	margin-top: 10%;
}
.input_id{
	float: left;
}
.icon_search {
    top: 12px;
    right: -11px;
    position: absolute;
    padding:  10px;
    height: 35px;
}
a.invoicemg_button_show_invoice
{
    background-color: #3c5a99;
    border-color: #3c5a99;
    border-bottom: 3px solid #2a4377;
    padding: 3px 15px;
    color:#fff;
}

a.invoicemg_button_show_invoice:hover
{
    background-color: #3c5a99;
    opacity:0.8;
}

a.invoicemg_button_show_invoice:active:focus
{
    background-color: #2a4377;
    opacity:0.8;
}

a.invoicemg_button_show_invoice:active:hover
{
    background-color: #3c5a99;
    opacity:0.8;
    border-bottom: 2px solid #2a4377;
}



a.invoicemg_button_show_invoicebyid
{
    background-color: #3c5a99;
    border-color: #3c5a99;
    border-bottom: 3px solid #2a4377;
    padding: 3px 15px;
    color:#fff;
}

a.invoicemg_button_show_invoicebyid:hover
{
    background-color: #3c5a99;
    opacity:0.8;
}

a.invoicemg_button_show_invoicebyid:active:focus
{
    background-color: #2a4377;
    opacity:0.8;
}

a.invoicemg_button_show_invoicebyid:active:hover
{
    background-color: #3c5a99;
    opacity:0.8;
    border-bottom: 2px solid #2a4377;
}
.choosing_refund_type {
    width: 400px;
    height: 236px;
    margin-left: 42px;
    margin-bottom: 38px;
    background-color: #e5e9f1;
    float: left;
    padding: 12px;
    text-align: center;
}
.header_amount {
    border-bottom: 1px solid #d2c8c8;
    padding-bottom: 11px;
}
.body_refund_type{
	padding-top: 10px;
}
.input_refund_amount{
	font-size: 16px;
	margin-top: 11px;
}
.footer_refund_type{
	margin-top: 17px;
}
.input_refund_amount{
	border-radius: 5px;
}
#txt_refund_amount{
	border-radius: 5px;
}
.button_print {
    position: absolute;
    bottom: 34px;
    left: 40px;
}
.modal-header{
	color: #fff;
    background-color: #4d6cae;
}


</style>















