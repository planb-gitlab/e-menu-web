@extends('crudbooster::admin_template')
@section('content')
<!-- css Link -->
<link rel="stylesheet" href="{{ asset('css/sale_by_invoice.css') }}">
<link rel="stylesheet" href="{{ asset('css/loading.css') }}">
<link   rel="stylesheet" href="{{ asset('css/template.css') }}">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
	<script>

		$(window).load(function() {
			$(".se-pre-con-detail").fadeOut("slow");;
		});
		var url="{{URL::to('/')}}";
	</script>

<body id="invoice">

	<!-- Option for choosing filter -->
	<div class="col-md-12">
	  <div class="row">

	  	<!-- Time Filter -->
	    <div class="col-md-3">
	      <div class="row">
	        <div class="form-group">
	            <label for="sel1">Select Sort (This week,month,year):</label>
	            <select class="form-control btnsort" id="by" name='by'>
	              <option>Please Choose... </option>
	              <option>Weekly</option>              
	              <option>Monthly</option>
	              <option>Yearly</option>
	            </select>
	        </div>
	      </div>
	    </div>
	    <!-- End Time Filter -->

	    <!-- Date Filter -->
	    <div class="col-md-4">
	      <div class="form-group">
	        <input type="text" class="hidden" autofocus>
	        <div class="form-group timepicker">
	          <label>From Date</label>
	          <input type="text" readonly="" name="daterange" class="form-control btn_date" placeholder="Please choose day" />
	           <span style="pointer-events: none;" class="input-group-addon">
	            <span  class="glyphicon glyphicon-calendar"></span>
	        </span>
	        </div>
	      </div>
	    </div>
	    <div class="col-lg-3">
	    	<div class="form-group">
	    		<div class="search_by_id">
	    			<label>Search Invoice By ID</label>
	    			<input  type="text" class="input_id form-control">
	    			<div class="icon_search">
	    				<button class="icon_search btn btn-default fa fa-search"></button> 
	    			</div>
	    		</div>
	    	</div>
	    </div>
	    <!-- End Date Filter -->
	    
	  </div>
	</div>

	 <div class="modal fade" id="invoice_modal" role="dialog" style="padding-right: 0 !important;" tabindex='-1'>
            <div class="modal-dialog">
            
              <!-- Modal content-->
              <div class="modal-content" id="modal_invoice">
              </div>

              <div class="modal-footer">
                 
                </div>

              
            </div>
    </div>
	<!-- End of Option for choosing filter -->


	<div class="col-md-12">
	  <div class="row">
	    <div class="bg_report">
	     

	      <div class="tab-content">
	        {{-- menu 1  --}}
	        <div id="home" class="tab-pane fade in active">
	          <table class="table table-striped table-bordered table-hover">
	            <thead class="dir_table_thead">
	              <tr>
	                <th class="col-md-1">No.</th>
	                <th class="col-md-2">Invoice ID</th>
	                <th>Invoice Date</th>
	                <th>Total Price</th>
	                <th>Quantity</th>
	                <th>Action</th>
	              </tr>
	            </thead>
	            <tbody class="dir_table">
	              <tr>
	                <td colspan="5" class="text-center"><h4>No Data!</h4></td>
	              </tr>
	            </tbody>
	          </table>
	          
	        </div>
	       
	        </div>
	      </div>
	    </div>
	  </div>

	<div class="se-pre-con-detail"></div>



	<!-- Popup view for view and print -->
	<div id="view_invoice_blade">
	</div>	

 <div class="modal fade" id="invoice_modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content modal_content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

 </body>
@endsection

<!-- Include Required Prerequisites -->
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
 
<!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link   rel="stylesheet" href="{{ asset('vendor/crudbooster/assets/adminlte/bootstrap/css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

<script type="text/javascript">
	$(function(){
		$('body').on('change', '.btnsort', function(e) {
		      var by = $('.btnsort').val();
		       $('.btn_date').val("");

		      jQuery.ajax({
		          url: "{{ route('merge_sort_invoice') }}",
		          type: 'GET',
		          data: { by: by , _token:"{{Session::token()}}"},
		          dataType: "html",
		          success: function (data) {
		              
		              $('.dir_table').html(data)
		            
		          }
		      })
		    });

		$('body').on('focus','.btn_date',function(){
		      $('.btnsort').prop('checked', false);
		      $('input[name="daterange"]').daterangepicker({
		        format: 'YYYY-MM-DD'
		        });
		    });

		$('body').on('click', '.applyBtn', function(event) {
		        var date = $('.btn_date').val();
		        $('.btnsort').val();
		        $('select>option:eq(0)').prop('selected', true);
		        jQuery.ajax({
		            url: "{{ route('merge_sort_invoice') }}",
		            type: 'GET',
		            data: { date: date, _token:"{{Session::token()}}"},
		            dataType: "html",
		            success: function (data) {
		               
		                $('.dir_table').html(data)
		              
		            }
		        
		    });
		  });

		$(document).on('click','.icon_search',function(){
			var id=$('.input_id').val();
			jQuery.ajax({
				url:"{{ route('merge_search_invoice_byid') }}",
				type:'GET',
				data:{id:id, _token:"{{ Session::token()}}"},
				dataType:"html",
				success:function(data){
					 $('.dir_table').html(data)
				}
			});
		});

		
		
	});

	
</script>

<style>
.void_button{
	bottom: 64px;
    position: relative;
    left: 41px;
}
.refund_button {
    bottom: 64px;
    position: relative;
    left: 41px;
}
.cross{
	margin-left: 8px;
}
.modal-title{
	display: inline-block;
}

.modal-dialog{
	margin-top: 10%;
}
.input_id{
	float: left;
}
.icon_search {
    top: 12px;
    right: -11px;
    position: absolute;
    padding:  10px;
    height: 35px;
}
a.merge_button_show_invoice
{
    background-color: #3c5a99;
    border-color: #3c5a99;
    border-bottom: 3px solid #2a4377;
    padding: 3px 15px;
    color:#fff;
}

a.merge_button_show_invoice:hover
{
    background-color: #3c5a99;
    opacity:0.8;
}

a.merge_button_show_invoice:active:focus
{
    background-color: #2a4377;
    opacity:0.8;
}

a.merge_button_show_invoice:active:hover
{
    background-color: #3c5a99;
    opacity:0.8;
    border-bottom: 2px solid #2a4377;
}



a.merge_button_show_invoicebyid
{
    background-color: #3c5a99;
    border-color: #3c5a99;
    border-bottom: 3px solid #2a4377;
    padding: 3px 15px;
    color:#fff;
}

a.merge_button_show_invoicebyid:hover
{
    background-color: #3c5a99;
    opacity:0.8;
}

a.merge_button_show_invoicebyid:active:focus
{
    background-color: #2a4377;
    opacity:0.8;
}

a.merge_button_show_invoicebyid:active:hover
{
    background-color: #3c5a99;
    opacity:0.8;
    border-bottom: 2px solid #2a4377;
}

</style>















