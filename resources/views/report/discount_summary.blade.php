@extends('crudbooster::admin_template')
@section('content')

<body id="invoice">
<form class='form' method='get' id="form" enctype="multipart/form-data" action="{{ route('get_sort_discount_summary') }}">    
<div class="container-fluid">
  <div class="filter">

      <div class="row">

        <!-- =================== Time Filter =================== -->
        <div class="col-md-4 filter_date">
          <div class="form-group">
            <label for="sel1">{{trans('report.Select Sort (This week,month,year):')}}</label>
            <select class="form-control btnsort" id="by" name='by'>
              <option value=''    @if($_GET['by'] == '')      selected @endif >Please Choose... </option>
              <option value="weekly"  @if($_GET['by'] == 'weekly')    selected @endif >Weekly</option>              
              <option value="monthly" @if($_GET['by'] == 'monthly')   selected @endif >Monthly</option>
              <option value="yearly"  @if($_GET['by'] == 'yearly')    selected @endif >Yearly</option>
            </select>
          </div>
        </div>

        <!-- =================== Date Filter =================== -->
        <div class="col-md-4 filter_custom_date">
          <div class="form-group">
            <input type="text" class="hidden" autofocus>
            <div class="form-group timepicker">
              <label>{{trans('report.From Date')}}</label>
              <input type="text" readonly="" name="daterange" class="form-control btn_date" placeholder="Please choose day" value="{{$_GET['daterange']}}"/>
              <span style="pointer-events: none;" class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
              </span>
            </div>
          </div>
        </div>

        <!-- =================== Invoice ID =================== -->
        <div class="col-md-4">
          <div class="form-group">
            <div class="search_by_id">
              <label>{{trans('report.Search Invoice By Invoice ID')}}</label>
              <input  type="text" name="invoice_id" class="input_id form-control" placeholder="Please Input Invoice ID" value="{{$_GET['invoice_id']}}" autocomplete="off" />
            </div>
          </div>
        </div>

      </div>

      <div class="row search" style="display: none;">
        <div class="col-md-12">
          <button type="submit" class='btn btn-success btnsearch'>
            <i class="fa fa-search"></i>
          </button>
        </div>
      </div>
  </div>
  

<div class="col-md-12">
  <div class="row">
    <div class="bg_report_product">
     

      <div class="tab-content">
        <div id="home" class="tab-pane fade in active">
          <table class="table table-striped table-bordered table-hover">
            <thead class="dir_table_thead">
              <tr>
                <th class="col-md-1">{{trans('report.No.')}}</th>
                <th class="col-md-2">{{trans('report.Invoice ID.')}}</th>
                <th class="col-md-2">{{trans('report.Invoice Date')}}</th>
                <th class="col-md-1">{{trans('report.Sold Amount')}}</th>
                <th class="col-md-2">{{trans('report.Discount')}}</th>
              </tr>
            </thead>
            <tbody class="dir_table" id="table_info">
                @if(count($invoices) >= 1)
                  @foreach($invoices as $key => $invoice)
                  <tr>
                    <td class="col-md-1">{{ ++$key }}</td>
                    <td class="col-md-2">{{ $invoice->invoice_number }}</td>
                    <td class="col-md-2">{{ $invoice->invoice_date }}</td>
                    <td class="col-md-1">{{$invoice->currency}}{{ $invoice->grand_total }}</td> 
                    <td class="col-md-2">{{$invoice->currency}}{{ $invoice->discount_total_dollar }}</td>

                  </tr>
                  @endforeach
                  
                @else
                  <tr>
                    <td colspan="5" class="text-center"><h4>{{trans('report.No Data!')}}</h4></td>
                  </tr>
                @endif
              </tbody>
            </table>

            {!! $invoices->render() !!}
          
        </div>
       
        </div>
      </div>
    </div>
  </div>

<div class="se-pre-con-detail"></div>

</div>
</form>
</body>
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/sale_summary.css') }}">
<link rel="stylesheet" href="{{ asset('css/loading.css') }}">
@endsection

@section('script')
<script>
  $(function(){
    $(".se-pre-con-detail").fadeOut("slow");;
  });
</script>

<script type="text/javascript">

$('body').on('change', '.btnsort', function(e) {
      clear_filter(['custom_date','input_id']);
      $('.btnsearch').click();
    });

$('body').on('focus','.btn_date',function(){
      var data = $('.detail').attr('type','');
      $('input[name="daterange"]').daterangepicker({
        format: 'YYYY-MM-DD'
        });
    });

$('body').on('click', '.applyBtn', function(event) {
      clear_filter(['filter_date','input_id']);
      $('.btnsearch').click();
});
  
$('body').on('focus','.input_id',function(){
      clear_filter(['filter_date','custom_date']);
  });

</script>


<!-- ================== START OF FUNCTION =================== -->
<script type="text/javascript">
  function clear_filter(filter){
      $.each(filter,function(i,v){
          // Filter data
      if(filter[i] == "filter_date"){
          $('.btnsort').prop('checked', false);
          $('.btnsort').val();
          $('.btnsort option:eq(0)').prop('selected', true);
        
        }

        // Filter Custom Date
        if(filter[i] == "custom_date"){
            $('.btn_date').val("");

        }

        // Item Code
        if(filter[i] == "input_id"){
            $('.input_id').val("");
           
        }
      });
}
</script>

<!-- ================== END OF FUNCTION =================== -->


@endsection