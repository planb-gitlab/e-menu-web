@extends('crudbooster::admin_template')
@section('content')

<body id="invoice">
<form class='form' method='get' id="form" enctype="multipart/form-data" action="{{ route('get_sort_promotion_pack') }}">
<div class="container-fluid">
		<div class="filter">

			<div class="row">

				<!-- =================== Time Filter =================== -->
				<div class="col-md-4 filter_date">
					<div class="form-group">
						<label for="sel1">{{trans('report.Select Sort (This week,month,year):')}}</label>
						<select class="form-control btnsort" id="by" name='by'>
							<option value='' 		@if($_GET['by'] == '') 		 	selected @endif >Please Choose... </option>
							<option value="weekly"  @if($_GET['by'] == 'weekly')  	selected @endif >Weekly</option>              
							<option value="monthly" @if($_GET['by'] == 'monthly') 	selected @endif >Monthly</option>
							<option value="yearly"  @if($_GET['by'] == 'yearly')  	selected @endif >Yearly</option>
						</select>
					</div>
				</div>

				<!-- =================== Date Filter =================== -->
				<div class="col-md-4 filter_custom_date">
					<div class="form-group">
						<input type="text" class="hidden" autofocus>
						<div class="form-group timepicker">
							<label>{{trans('report.From Date')}}</label>
							<input type="text" readonly="" name="daterange" class="form-control btn_date" placeholder="Please choose day" value="{{$_GET['daterange']}}"/>
							<span style="pointer-events: none;" class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
				</div>

				<!-- =================== Promotion Name =================== -->
				<div class="col-md-3">
		        <div class="form-group">
		            <label for="sel1">{{trans('report.Filter by Promotion Name:')}}</label>
		            <select class="form-control" id="promotionName" name='promotion_name'>
		              <option value="">Please Choose... </option>
		              <?php foreach ($get_promotion_name as $key => $get_promotion_names): ?>
		              	 <option value="<?php echo $get_promotion_names->id; ?>"><?php echo $get_promotion_names->promotion_name; ?></option>  
		              <?php endforeach ?>        
		            </select>
		        </div>
		      	</div>

			</div>

			<div class="row">
			   <div class="col-md-4">
				        <div class="form-group">
				            <label for="sel1">{{trans('report.Filter By Promotion Type:')}}</label>
				            <select class="form-control" id="prmotionType" name='promotionType'>
				              <option value="">Please Choose... </option>
				              <?php foreach ($get_promotion_type_info as $key => $get_promotion_type_infos): ?>
				              	<option value="<?php echo $get_promotion_type_infos->id; ?>"><?php echo $get_promotion_type_infos->name; ?></option>
				              <?php endforeach ?>
				                            
				            </select>
				        </div>
				</div>
			    
			</div>

			<div class="row search" style="display: none;">
				<div class="col-md-12">
					<button type="submit" class='btn btn-success btnsearch'>
						<i class="fa fa-search"></i>
					</button>
				</div>
			</div>

			

		</div>	



	<div class="col-md-12">
		<div class="row">
	    <div class="bg_report">
	     

	      <div class="tab-content">
	        <div id="home" class="tab-pane fade in active">
	          <table class="table table-striped table-bordered table-hover">
	            <thead class="dir_table_thead">
	              <tr>
	                <th class="col-md-1">{{trans('report.No.')}}</th>
	                <th class="col-md-2">{{trans('report.Promotion Name')}}</th>
	                <th class="col-md-2">{{trans('report.Promotion Type')}}</th>
	                <th>{{trans('report.Sold Amount')}} ($)</th>
	                <th>{{trans('report.Customer Count')}}</th>
	                <th>{{trans('report.Discount')}} ($)</th>
	                <th>{{trans('report.Sub Total')}} ($)</th>
	                <th>{{trans('report.Location')}}</th>
	              </tr>
	            </thead>
	            <tbody class="dir_table">
	              <tr>
	                <td colspan="5" class="text-center"><h4>{{trans('report.No Data!')}}</h4></td>
	              </tr>
	            </tbody>
	          </table>
	          
	        </div>
	       
	        </div>
	    </div>
		</div>
	</div>

	<div class="se-pre-con-detail"></div>

</div>
</form>
</body>

@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/sale_by_invoice.css') }}">
<link rel="stylesheet" href="{{ asset('css/loading.css') }}">
	
@endsection

@section('script')
<script>
$(function() {
  $(".se-pre-con-detail").fadeOut("slow");;
});

$(function(){
	$('#promotionName').select2();
	$('#prmotionType').select2();
});
</script>

<script>
$(function(){

	$('body').on('change', '.btnsort', function(e) {
      clear_filter('filter_date','','','');
      $('.btnsearch').click();
    });

	$('body').on('focus','.btn_date',function(){
      $('.btnsort').prop('checked', false);
      $('input[name="daterange"]').daterangepicker({
        	format: 'YYYY-MM-DD'
        });
    });

	$('body').on('click', '.applyBtn', function(event) {
        clear_filter('','custom_date','','');
        $('.btnsearch').click();
  	});

	
	$('body').on('change','#promotionName',function(){
		clear_filter('','','promotion_name','');
		$('.btnsearch').click();
	});
	$('body').on('change','#prmotionType',function(){
		clear_filter('','','','promotion_type');
		$('.btnsearch').click();
	});

});
</script>


<script type="text/javascript">
	function clear_filter(filter0,filter1,filter2,filter3)
	{
		if(filter0=="filter_date")
		{
			 $('.btn_date').val("");
			 $('#prmotionType').prop('checked', false);
        	 $('#prmotionType').val("");
             $('#prmotionType option:eq(0)').prop('selected', true).trigger('change.select2');
             $('#promotionName').prop('checked', false);
        	 $('#promotionName').val("");
             $('#promotionName option:eq(0)').prop('selected', true).trigger('change.select2');
		}
		if(filter1=="custom_date")
		{
			 $('.btnsort').prop('checked', false);
		     $('.btnsort').val();
		     $('.btnsort option:eq(0)').prop('selected', true);
			 $('#prmotionType').prop('checked', false);
        	 $('#prmotionType').val("");
             $('#prmotionType option:eq(0)').prop('selected', true).trigger('change.select2');
             $('#promotionName').prop('checked', false);
        	 $('#promotionName').val("");
             $('#promotionName option:eq(0)').prop('selected', true).trigger('change.select2');
		}
		if(filter2=="promotion_name")
		{	 
			 $('.btn_date').val("");
			 $('.btnsort').prop('checked', false);
		     $('.btnsort').val();
		     $('.btnsort option:eq(0)').prop('selected', true);
			 $('#prmotionType').prop('checked', false);
        	 $('#prmotionType').val("");
             $('#prmotionType option:eq(0)').prop('selected', true).trigger('change.select2');
		}
		if(filter3=="promotion_type") 
		{
			 $('.btnsort').prop('checked', false);
		     $('.btnsort').val();
		     $('.btnsort option:eq(0)').prop('selected', true);
			 $('.btn_date').val("");
			 $('#promotionName').prop('checked', false);
        	 $('#promotionName').val("");
             $('#promotionName option:eq(0)').prop('selected', true).trigger('change.select2');

		}
		
	}
</script>

@endsection



