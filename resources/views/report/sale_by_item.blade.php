@extends('crudbooster::admin_template')
@section('content')


<body id="item">

<form class='form' method='get' id="form" enctype="multipart/form-data" action="{{ route('get_sort_item') }}">
<div class="container-fluid">  
  <div class="filter">
      
      <div class="row">

          <!-- =================== Time Filter =================== -->
          <div class="col-md-3 filter_date">
            <div class="form-group">
              <label for="sel1">{{trans('report.Select Sort (This week,month,year):')}}</label>
              <select class="form-control btnsort" id="by" name='by'>
                <option value=''    @if($_GET['by'] == '')              selected @endif >Please Choose... </option>
                <option value="weekly"  @if($_GET['by'] == 'weekly')    selected @endif >Weekly</option>              
                <option value="monthly" @if($_GET['by'] == 'monthly')   selected @endif >Monthly</option>
                <option value="yearly"  @if($_GET['by'] == 'yearly')    selected @endif >Yearly</option>
              </select>
            </div>
          </div>

          <!-- =================== Date Filter =================== -->
          <div class="col-md-4 filter_custom_date">
            <div class="form-group">
              <input type="text" class="hidden" autofocus>
              <div class="form-group timepicker">
                <label>{{trans('report.From Date')}}</label>
                <input type="text" readonly="" name="daterange" class="form-control btn_date" placeholder="Please choose day" value="{{$_GET['daterange']}}"/>
                <span style="pointer-events: none;" class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
            </div>
          </div>

          <!-- =================== Item code =================== -->
          <div class="col-md-4 ">
                 <label>{{trans('report.Filter By Itemcode')}}</label>
                 <select class="form-control" id="itemCode" name="itemCode">
                 <option value="">**Please select items code</option>
                     <?php foreach ($get_item_info as $key => $itemcode): ?>
                         <?php if($itemcode->menu_code != null): ?>
                         <option value="{!! $itemcode->id; !!}">{!!$itemcode->menu_code;!!}</option>
                        <?php endif;?>
                     <?php endforeach ?>
                 </select>
          </div>

      </div>

    
      <div class="row">
                  <div class="col-md-3">
                        <label>{{trans('report.Filter By ItemName')}}</label>
                        <select class="form-control" id="itemName" name="itemName">
                          <option value="">**Please Select items name</option>
                          <?php foreach ($get_item_info as $key => $itemname): ?>
                                <option value="{!! $itemname->id; !!}" @if($_GET['itemName'] == $itemname->id) selected @endif>{!! $itemname->name!!}</option>
                          <?php endforeach ?>

                        </select>
                  </div>
                  <div class="col-md-3" style="border-right: 1px solid #ccc">
                        <label>{{trans('report.Filter By UOM')}}</label>
                        <select class="form-control" id="itemUom" name="itemUom" disabled="">
                          @if($_GET['itemUom'])
                          <option value="">**Please Select item's uom</option>
                          <?php foreach ($get_item_uom as $key => $itemname): ?>
                                <option value="{!! $itemname->id; !!}" >{!! $itemname->name;!!}</option>
                          <?php endforeach ?>
                          @endif

                        </select>
                  </div>
                   <div class="col-md-3">
                        <label>{{trans('report.Filter By Subcategory')}}</label>
                        <select class="form-control" id="itemSubcategory" name="itemSubcategory">
                          <option value="">**Please Select item's subcategory</option>
                          <?php foreach ($get_item_subcat as $key => $itemname): ?>
                                <option value="{!! $itemname->id; !!}"  @if($_GET['itemSubcategory'] == $itemname->id) selected @endif >{!! $itemname->name;!!}</option>
                          <?php endforeach ?>

                        </select>
                  </div>
                   <div class="col-md-3">
                        <label>{{trans('report.Filter By Category')}}</label>
                        <select class="form-control" id="itemCategory" name="itemCategory">
                          <option value="">**Please Select item's category</option>
                          <?php foreach ($get_item_cat as $key => $itemname): ?>
                                <option value="{!! $itemname->id; !!}"  @if($_GET['itemCategory'] == $itemname->id) selected @endif >{!! $itemname->name;!!}</option>
                          <?php endforeach ?>

                        </select>
                  </div>

      </div>

      <div class="row search" style="display: none;">
          <div class="col-md-12">
            <button type="submit" class='btn btn-success btnsearch'>
              <i class="fa fa-search"></i>
            </button>
          </div>
      </div>

</div>


<div class="row">
  <div class="col-md-12">
    <div class="bg_report_product">
     

      <div class="tab-content">
        <div id="home" class="tab-pane fade in active">
          <table class="table table-striped table-bordered table-hover">
            <thead class="dir_table_thead">
              <tr>
                <th class="col-md-1">{{trans('report.No.')}}</th>
                <!-- <th>{{trans('report.Item code')}}</th> -->
                <th>{{trans('report.Item Name')}}</th>
                <th>{{trans('report.UOM')}}</th>
                <th>{{trans('report.Quantity')}}</th>
                <th>{{trans('report.Subcategory')}}</th>
                <th>{{trans('report.Category')}}</th>
              </tr>
            </thead>
            <tbody class="dir_table" id="table_info">
                  @if(count($invoices) >= 1)
                    @foreach($invoices as $key => $invoice)
                    <tr>
                      <td class="col-md-1">{{ ++$key }}</td>
                      <td>{{ $invoice->name }}</td>
                      <td>@if($invoice->uom_name != null){{ $invoice->uom_name }} @else X @endif</td>
                      <td>{{ $invoice->count_qty }} </td>
                      <td>@if($invoice->sub_name != null){{ $invoice->sub_name }} @else X @endif</td> 
                      <td>{{ $invoice->cat_name}}</td>

                    </tr>
                    @endforeach
                    
                  @else
                    <tr>
                      <td colspan="5" class="text-center"><h4>{{trans('report.No Data!')}}</h4></td>
                    </tr>
                  @endif
                </tbody>
              </table>

              {!! $invoices->render() !!}
          </table>
          
        </div>
       
        </div>
      </div>
    </div>
  </div>

<div class="se-pre-con-detail"></div>


</div>
</form>
</body>
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/sale_summary.css') }}">
<link rel="stylesheet" href="{{ asset('css/loading.css') }}">
<style type="text/css">
    .other_search{
      margin-top: 25px;
    }
    #btn_other{
      height: 34px;
    }
</style>
@endsection


@section('script')
<script>
    $(function(){
      $(".se-pre-con-detail").fadeOut("slow");;
    });

    $(function(){
                $('#itemCode').select2();
                $('#itemName').select2();
                $('#itemUom').select2();
                $('#itemSubcategory').select2();
                $('#itemCategory').select2();
    });


    //===================== find ======================//
    $(function(){
      
      $('body').on('change', '.btnsort', function(e) {
        clear_filter('','custom_date','item_code','item_name','item_uom','item_subcategory','item_category');
        $('.btnsearch').click();
      });

      $('body').on('focus','.btn_date',function(){
        $('.btnsort').prop('checked', false);
        $('input[name="daterange"]').daterangepicker({
          format: 'YYYY-MM-DD'
        });
      });

      $('body').on('click', '.applyBtn', function(event) {
        clear_filter('filter_date','','item_code','item_name','item_uom','item_subcategory','item_category');
        $('.btnsearch').click();

      });

      $('body').on('change', '#itemCode', function(event) {
        clear_filter('filter_date','custom_date','','item_name','item_uom','item_subcategory','item_category');
        $('.btnsearch').click();

      });

      $('body').on('change', '#itemName', function(event) {
        clear_filter('filter_date','custom_date','item_code','','item_uom','item_subcategory','item_category');
        $('.btnsearch').click();

      });

      $('body').on('change', '#itemUom', function(event) {
        clear_filter('filter_date','custom_date','item_code','','','item_subcategory','item_category');
        $('.btnsearch').click();

      });

      $('body').on('change', '#itemSubcategory', function(event) {
        clear_filter('filter_date','custom_date','item_code','item_name','item_uom','','item_category');
        $('.btnsearch').click();

      });

      $('body').on('change', '#itemCategory', function(event) {
        clear_filter('filter_date','custom_date','item_code','item_name','item_uom','item_subcategory','');
        $('.btnsearch').click();

      });

    });

</script>

<script>
  $(function(){
     var item_name = "{{$_GET['itemName']}}";
     var item_uom = "{{$_GET['itemUom']}}";
     if(item_name != ''){
      jQuery.ajax({
            url     : "{{ route('get_uom_menu') }}",
            method  : "POST",
            data    : {item_name : item_name, item_uom:item_uom},
            success : function(data){
              $('#itemUom').html(data);
              $('#itemUom').prop('disabled', false);
            },
            error   : function(data){
              swal("Warning", "There must be a problem with the request ! Please check again", "warning");
            }
        });
        
     }else{
        

        $('#itemUom').prop('disabled', true);
        
     }
  });
</script>

<!-- ================== START OF FUNCTION =================== -->
<script type="text/javascript">
  function alert_timer(){
    swal({
        title: 'Searching in progress',
        text: 'Please wait patiently',
        timer: 500,
        showCancelButton: false,
        showConfirmButton: false
      });
  }
</script>

<script type="text/javascript">
  function clear_filter(filter0,filter1,filter2,filter3,filter4,filter5,filter6){
      // Filter data
      if(filter0 == "filter_date"){
        $('.btnsort').prop('checked', false);
        $('.btnsort').val();
        $('.btnsort option:eq(0)').prop('selected', true);
      }

      // Filter Custom Date
      if(filter1 == "custom_date"){
          $('.btn_date').val("");
      }

      // Item Code
      if(filter2 == "item_code"){
          $('#itemCode').prop('checked', false);
          $('#itemCode').val();
          $('#itemCode option:eq(0)').prop('selected', true).trigger('change.select2');
      }

      // Item Name
      if(filter3 == "item_name"){
          $('#itemName').prop('checked', false);
          $('#itemName').val();
          $('#itemName option:eq(0)').prop('selected', true).trigger('change.select2');
      }

      // Item Uom
      if(filter4 == "item_uom"){
          $('#itemUom').prop('checked', false);
          $('#itemUom').val();
          $('#itemUom option:eq(0)').prop('selected', true).trigger('change.select2');
      }

      // Item Subcategory
      if(filter5 == "item_subcategory"){
        $('#itemSubcategory').prop('checked', false);
        $('#itemSubcategory').val();
        $('#itemSubcategory option:eq(0)').prop('selected', true).trigger('change.select2');
      }

      // Item Subcategory
      if(filter6 == "item_category"){
        $('#itemCategory').prop('checked', false);
        $('#itemCategory').val();
        $('#itemCategory option:eq(0)').prop('selected', true).trigger('change.select2');
      }
  }
</script>
<!-- ================== END OF FUNCTION =================== -->
@endsection

