@extends('crudbooster::admin_template')
@section('content')


<body id="invoice">

	<form class='form' method='get' id="form" enctype="multipart/form-data" action="{{ route('get_sort_invoice') }}">
	<div class="container-fluid">	
		
		<div class="filter">
			<div class="row">

				<!-- =================== Time Filter =================== -->
				<div class="col-md-4 filter_date">
					<div class="form-group">
						<label for="sel1">{{trans('report.Select Sort (This week,month,year):')}}</label>
						<select class="form-control btnsort" id="by" name='by'>
							<option value='' @if($_GET['by'] == '') selected @endif >Please Choose... </option>
							<option value="weekly" @if($_GET['by'] == 'weekly') selected @endif >Weekly</option>
							<option value="monthly" @if($_GET['by'] == 'monthly') selected @endif >Monthly</option>
							<option value="yearly" @if($_GET['by'] == 'yearly') selected @endif >Yearly</option>
						</select>
					</div>
				</div>

				<!-- =================== Date Filter =================== -->
				<div class="col-md-4 filter_custom_date">
					<div class="form-group">
						<input type="text" class="hidden" autofocus>
						<div class="form-group timepicker">
							<label>{{trans('report.From Date')}}</label>
							<input type="text" readonly="" name="daterange" class="form-control btn_date" placeholder="Please choose day" value="{{$_GET['daterange']}}"/>
							<span style="pointer-events: none;" class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
				</div>

				<!-- =================== Invoice ID =================== -->
				<div class="col-md-4">
					<div class="form-group">
						<div class="search_by_id">
							<label>{{trans('report.Search Invoice By Invoice ID')}}</label>
							<input  type="text" name="invoice_id" class="input_id form-control" placeholder="Please Input Invoice ID" value="{{$_GET['invoice_id']}}" autocomplete="off" />
						</div>
					</div>
				</div>

			</div>

			<div class="row search" style="display: none;">
				<div class="col-md-12">
					<button type="submit" class='btn btn-success btnsearch'>
						<i class="fa fa-search"></i>
					</button>
				</div>
			</div>
		</div>

		<div class="col-md-12">
			<div class="row">
				<div class="bg_report">

					<div class="tab-content">
						<div id="home" class="tab-pane fade in active">
							<table class="table table-striped table-bordered table-hover">
								<thead class="dir_table_thead">
									<tr>
										<th class="col-md-1">{{trans('report.No.')}}</th>
										<th class="col-md-2">{{trans('report.Order Date')}}</th>
										<th class="col-md-2">{{trans('report.Invoice No')}}</th>
										<th class="col-md-2">{{trans('report.orderType')}}</th>
										<th class="col-md-1">{{trans('report.Sub Total')}}</th>
										<th class="col-md-2">{{trans('report.Location')}}</th>
										<th class="col-md-1">{{trans('report.Action')}}</th>
									</tr>
								</thead>

								<tbody class="dir_table" id="table_info">
									@if(count($invoices) >= 1)
										@foreach($invoices as $key => $invoice)
										<tr>
											<td class="col-md-1">{{ ++$key }}</td>
											<td class="col-md-2">{{ $invoice->invoice_date }}</td>
											<td class="col-md-2">{{ $invoice->invoice_number }}</td>
											<td class="col-md-2">{{ strtoupper($invoice->order_type) }}</td>
											<td class="col-md-1">{{ $invoice->currency }}{{ number_format($invoice->grand_total, 2) }}</td> 
											<td class="col-md-2">{{ $invoice->company_name }}</td>
											<td class="col-md-1">
												<a class="invoice_show"  data-val="{{$invoice->invoice_id}}">
													<i class="fa fa-eye"></i>
												</a> 
												<input type="hidden" id="invoiceId" value="{{ $invoice->invoice_id }}">
											</td>

										</tr>
										@endforeach
										
									@else
										<tr>
											<td colspan="5" class="text-center"><h4>{{trans('report.No Data!')}}</h4></td>
										</tr>
									@endif
								</tbody>
							</table>

							{!! $invoices->render() !!}
						</div>
					</div>
					
				</div>
			</div>

		</div>

		<div class="se-pre-con-detail"></div>


	</div>
	</form>

</body>
@endsection


@section('modal')
<div class="modal fade" id="view_modal" role="dialog" style="padding-right: 0 !important;" tabindex='-1'>
<div class="modal-dialog">

	<div class="modal-content">
	    <div class="modal-header">
	        <h4 class="modal-title">{{trans('report.Invoice')}}</h4>
	        <button type="button" class="close close_popup" data-dismiss="modal">&times;</button>
	    </div>

	    <div class="modal-body"></div>

	</div>

</div>
</div>
@endsection


@section('css')
<!-- css Link -->
<link rel="stylesheet" href="{{ asset('css/sale_by_invoice.css') }}">
<link rel="stylesheet" href="{{ asset('css/loading.css') }}">

<style>
	
	.numData{
		padding: 10px 10px 10px 3px; 	
	}

	.container_refund_info{
		width: 287px;
		height: 82px;
		float: right;
		text-align: left;
		box-shadow: 0px 0px 5px grey;
		padding: 15px;
		font-weight: bold;
		font-size: 16px;
		margin-top: 20px;
	}
	.main_invoice{
		position:relative;
	}
	#bg_text{
		position: absolute;
		color: lightgrey;
		/*color: #eeeeee;*/
		font-size: 120px;
		transform: rotate(408deg);
		-webkit-transform: rotate(387deg);
		height: 500px;
		top: 259px;
		z-index: 0;
	}
	.void_button{
		bottom: 64px;
		position: relative;
		left: 41px;
	}
	.refund_button {
		bottom: 64px;
		position: relative;
		left: 41px;
	}
	.cross{
		margin-left: 8px;
	}
	.modal-title{
		display: inline-block;
	}

	.modal-dialog{
		margin-top: 10%;
	}
	.input_id{
		float: left;
	}
	.icon_search {
		top: 12px;
		right: -11px;
		position: absolute;
		padding:  10px;
		height: 35px;
	}
	a.invoice_show
	{
		background-color: #3c5a99;
		border-color: #3c5a99;
		border-bottom: 3px solid #2a4377;
		padding: 3px 15px;
		color:#fff;
		font-size: 12px;
		cursor: pointer;
	}

	a.invoice_show:hover
	{
		background-color: #3c5a99;
		opacity:0.8;
	}

	a.invoice_show:active:focus
	{
		background-color: #2a4377;
		opacity:0.8;
	}

	a.invoice_show:active:hover
	{
		background-color: #3c5a99;
		opacity:0.8;
		border-bottom: 2px solid #2a4377;
	}
	.header_modal{
		background: #5974ad;
		color: #fff;
	}
	a.invoicebyid
	{
		background-color: #3c5a99;
		border-color: #3c5a99;
		border-bottom: 3px solid #2a4377;
		padding: 3px 15px;
		color:#fff;
	}

	a.invoicebyid:hover
	{
		background-color: #3c5a99;
		opacity:0.8;
	}

	a.invoicebyid:active:focus
	{
		background-color: #2a4377;
		opacity:0.8;
	}

	a.invoicebyid:active:hover
	{
		background-color: #3c5a99;
		opacity:0.8;
		border-bottom: 2px solid #2a4377;
	}
	.button_print {
		position: absolute;
		bottom: 33px;
		left: 40px;
	}


	.invoice_info, .invoice_order{
		margin-bottom: 10px;
	}

	.invoice_order{
		border-bottom:1px dashed #ccc;
	}

	.invoice_order .header{
		border-top:1px dashed #ccc;
		border-bottom:1px dashed #ccc;
	}

	.modal-header {
	    border-bottom-color: #f4f4f4;
	    background: #3C5A99;
	    color: #fff;
	}

</style>

@endsection


@section('script')

<script>
	$(function(){
		$(".se-pre-con-detail").fadeOut("slow");
	});
</script>

<script type="text/javascript">

	//===================== find ======================//
	$(function(){
		
		$('body').on('change', '.btnsort', function(e) {
			clear_filter(['custom_date','input_id']);
		    $('.btnsearch').click();
		});

		$('body').on('focus','.btn_date',function(){
			$('.btnsort').prop('checked', false);
			$('input[name="daterange"]').daterangepicker({
				format: 'YYYY-MM-DD'
			});
		});

		$('body').on('click', '.applyBtn', function(event) {
			clear_filter(['filter_date','input_id']);
		    $('.btnsearch').click();

		});

		$('body').on('focus','.input_id ',function(){
			clear_filter(['filter_date','custom_date']);
		});

		$('body').on('click','.invoice_show',function(){
			var invoice_no = $(this).attr('data-val');
			jQuery.ajax({
				url 	: "{{ route('report.view_invoice') }}",
				method	: "POST",
				data 	: {invoice_no : invoice_no},
				success : function(data){
					$('#view_modal .modal-body').html(data);
					$('#view_modal').modal('show');
				},
				error 	: function(data){
					swal("Warning", "There must be a problem with the request ! Please check again", "warning");
				}

			});
			
		});


});


</script>

<!-- =============== START OF FUNCTION ================ -->

<script type="text/javascript">
  function clear_filter(filter){
      $.each(filter,function(i,v){
          // Filter data
      if(filter[i] == "filter_date"){
          $('.btnsort').prop('checked', false);
          $('.btnsort').val();
          $('.btnsort option:eq(0)').prop('selected', true);
        
        }

        // Filter Custom Date
        if(filter[i] == "custom_date"){
            $('.btn_date').val("");

        }

        // Item Code
        if(filter[i] == "input_id"){
            $('.input_id').val("");
           
        }
      });
}
</script>
<!-- =============== END OF FUNCTION ================ -->
@endsection

@push('bottom')
	<script>
		$(document).on('click', '.print', function() {
			var invoiceId = $(this).data('val')
			jQuery.ajax({
				url 	: "{{ route('report.printInvoice') }}",
				method	: "GET",
				data 	: {invoiceId : invoiceId},
				success : function(data){
					console.log(data)
				},
				error 	: function(data){
					swal("Warning", "There must be a problem with the request ! Please check again", "warning");
				}

			});

		});

	</script>
@endpush

