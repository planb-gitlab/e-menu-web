@extends('crudbooster::admin_template')
@section('content')

	
	<!-- css Link -->
	<link rel="stylesheet" href="{{ asset('css/sale_by_invoice.css') }}">
	<link rel="stylesheet" href="{{ asset('css/loading.css') }}">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
	  <script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
	  <script>

	    $(window).load(function() {
	      $(".se-pre-con-detail").fadeOut("slow");;
	    });
	  </script>
	<!-- Option for choosing filter -->
	<div class="container-fluid">
	  <div class="row filter">

	  	<!-- Time Filter -->
	    <div class="col-md-3 filter_date">

	        <div class="form-group">
	            <label for="sel1">{{trans('report.Select Sort (This week,month,year):')}}</label>
	            <select class="form-control btnsort" id="by" name='by'>
	              <option>Please Choose... </option>
	              <option>Weekly</option>              
	              <option>Monthly</option>
	              <option>Yearly</option>
	            </select>
	        </div>
	    </div>
	    <!-- End Time Filter -->

	    <!-- Date Filter -->
	    <div class="col-md-4">
	      <div class="form-group">
	        <input type="text" class="hidden" autofocus>
	        <div class="form-group timepicker">
	          <label>{{trans('report.From Date')}}</label>
	          <input type="text" readonly="" name="daterange" class="form-control btn_date" placeholder="Please choose day" />
	           <span style="pointer-events: none;" class="input-group-addon">
	            <span  class="glyphicon glyphicon-calendar"></span>
	        </span>
	        </div>
	      </div>
	    </div>
	    <!-- End Date Filter -->
	    
	  </div>
	
	<!-- End of Option for choosing filter -->


	<div class="col-md-12">
	  <div class="row">
	    <div class="bg_report">
	     

	      <div class="tab-content">

	        <div id="home" class="tab-pane fade in active">
	          <table class="table table-striped table-bordered table-hover">
	            <thead class="dir_table_thead">
	              <tr>
	                <th class="col-md-1">{{trans('report.No.')}}</th>
	                <th class="col-md-1">{{trans('report.Order Date')}}</th>
	                <th class="col-md-1">{{trans('report.Credit Memo No')}}</th>
	                <th class="col-md-1">{{trans('report.Sold Amount')}}</th>
	                <th class="col-md-1">{{trans('report.Discount')}}</th>
	                <th class="col-md-1">{{trans('report.Sub Total')}}</th>
	                <th class="col-md-1">{{trans('report.VAT')}}</th>
	                <th class="col-md-1">{{trans('report.Location')}}</th>
	              </tr>
	            </thead>
	            <tbody class="dir_table">
	              <tr>
	                <td colspan="5" class="text-center"><h4>{{trans('report.No Data!')}}</h4></td>
	              </tr>
	            </tbody>
	          </table>
	          
	        </div>
	       
	        </div>
	      </div>
	    </div>
	  </div>

		<div class="se-pre-con-detail"></div>


</div>






<!-- Include Required Prerequisites -->
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
 
<!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

<!-- Sweet Alert -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.12/dist/sweetalert2.all.min.js"></script>
<link   rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.12/dist/sweetalert2.min.css">

<script type="text/javascript">
	$('body').on('change', '.btnsort', function(e) {
      var by = $('.btnsort').val();
      clear_filter('','custom_date');
      jQuery.ajax({
          url: "{{ route('get_sort_credit_memo') }}",
          type: 'POST',
          data: { result: by , _token:"{{Session::token()}}"},
          dataType: "html",
          success: function (data) {
              alert_timer();
              $('.dir_table').html(data)
            
          }
      })
    });

$('body').on('focus','.btn_date',function(){
      $('.btnsort').prop('checked', false);
      $('input[name="daterange"]').daterangepicker({
        format: 'YYYY-MM-DD'
        });
    });

$('body').on('click', '.applyBtn', function(event) {
        var date = $('.btn_date').val();
      	clear_filter('filter_date','');
        
        jQuery.ajax({
            url: "{{ route('get_sort_credit_memo') }}",
            type: 'POST',
            data: { date: date, _token:"{{Session::token()}}"},
            dataType: "html",
            success: function (data) {
                alert_timer();
                $('.dir_table').html(data)
              
            }
        
    });
  });

</script>


@endsection


<!-- =============== START OF FUNCTION ================ -->
<script type="text/javascript">
	function alert_timer(){
		swal({
			  title: 'Searching in progress',
			  text: 'Please wait patiently',
			  timer: 500,
			  showCancelButton: false,
			  showConfirmButton: false
			});
	}
</script>

<script type="text/javascript">
  function clear_filter(filter0,filter1){
      // Filter data
      if(filter0 == "filter_date"){
        $('.btnsort').prop('checked', false);
        $('.btnsort').val();
        $('.btnsort option:eq(0)').prop('selected', true);
      }

      // Filter Custom Date
      if(filter1 == "custom_date"){
          $('.btn_date').val("");
      }



  }
</script>
<!-- =============== END OF FUNCTION ================ -->


