@extends('crudbooster::admin_template')
@section('content')

<!-- Report Dashbord -->
<input type="hidden" id="company_currency" data-symbol="{{ $company_currency->currency_symbol }}">

<div class="container-fluid">

	<!-- Filter -->
	<div class="row filter">
		<!-- Filter by date (daily, weekly, monthly, yearly) -->

			<div class="col-md-4 filter_date">
					<span class="filter_title">{{trans('report.Filter by date')}} : </span>
					<select name="filter_option" class="form-control filter_option" id="filter_option">
						<option value="daily"	>Daily</option>
						<option value="weekly"	>Weekly</option>
						<option value="monthly" >Monthly</option>
						<option value="yearly" 	>Yearly</option>
					</select>
			</div>

			<!-- Filter by custom data -->
			<div class="col-md-8 filter_custom">
					<input type="text" class="hidden" autofocus>
					<span class="filter_title">{{trans('report.Filter by custom date')}} : </span>
					<button class="date_range_button date_reverse"><i class="fa fa-angle-left"></i></button>
					<input type="text" readonly="" class="form-control date_range" placeholder="Please Choose From and To Date" />
			        <button class="date_range_button date_forward"><i class="fa fa-angle-right"></i></button>

			</div>

	</div>

	<div class="row report_dashboard">
	<!-- Sold Amount -->
		<div class="col-md-6 sold_amount" id="sold_amount">
			<div class="chart_report">
			<div class="chart_title">{{trans('report.Sold Amount')}}</div>
			<span class="chart_currency">$</span>
			<span class="chart_result">0</span>
			<div class="chart_compare">{{trans('report.No Change From Previous Day')}}</div>
				<!-- <div class="chart_result_pre">
					<span class="chart_result_pre_title">Previous Total</span>
					<span class="chart_result_pre_data">0$</span>
				</div> -->
			<canvas id="sold_amount_chart" class="chart_report_canvas" style="width: 100%; height: auto;"></canvas>
			</div>
		</div>

	<!-- Table Sold -->
		<div class="col-md-6 table_sold" id="table_sold">
			<div class="chart_report">
			<div class="chart_title">{{trans('report.Table Sold')}}</div>
			<span class="chart_result">0</span>
			<div class="chart_compare">{{trans('report.No Change From Previous Day')}}</div>
				<!-- <div class="chart_result_pre">
					<span class="chart_result_pre_title">Previous Total</span>
					<span class="chart_result_pre_data">0$</span>
				</div> -->
			<canvas id="table_sold_chart" class="chart_report_canvas" style="width: 100%; height: auto;"></canvas>
			</div>
		</div>
	</div>

	<div class="row report_dashboard">

	<!-- Customer Count -->
		<div class="col-md-4 customer_count" id="customer_count">
			<div class="chart_report">
			<div class="chart_title">{{trans('report.Customer Count')}}</div>
			<span class="chart_result">0</span>
			<div class="chart_compare">{{trans('report.No Change From Previous Day')}}</div>
				<!-- <div class="chart_result_pre">
					<span class="chart_result_pre_title">Previous Total</span>
					<span class="chart_result_pre_data">0$</span>
				</div> -->
			<canvas id="customer_count_chart" class="chart_report_canvas" style="width: 100%; height: auto;"></canvas>
			</div>
		</div>

	<!-- Discount -->
		<div class="col-md-4 discount" id="discount">
			<div class="chart_report">
			<div class="chart_title">{{trans('report.Discount')}}</div>
			<span class="chart_currency">$</span>
			<span class="chart_result">0</span>
			<div class="chart_compare">{{trans('report.No Change From Previous Day')}}</div>
				<!-- <div class="chart_result_pre">
					<span class="chart_result_pre_title">Previous Total</span>
					<span class="chart_result_pre_data">0$</span>
				</div> -->
			<canvas id="discount_chart" class="chart_report_canvas" style="width: 100%; height: auto;"></canvas>
			</div>
		</div>


	<!-- Sub Total (Sold Amount - Discount) -->
		<div class="col-md-4 subtotal" id="subtotal">
			<div class="chart_report">
			<div class="chart_title">{{trans('report.Sub Total')}}</div>
			<span class="chart_currency">$</span>
			<span class="chart_result">0</span>
			<div class="chart_compare">{{trans('report.No Change From Previous Day')}}</div>
				<!-- <div class="chart_result_pre">
					<span class="chart_result_pre_title">Previous Total</span>
					<span class="chart_result_pre_data">0$</span>
				</div> -->
			<canvas id="subtotal_chart" class="chart_report_canvas" style="width: 100%; height: auto;"></canvas>
			</div>
		</div>

	</div>

	<!-- Revenue table -->
	<div class="row report_dashboard">
		<div class="col-md-12 revenue" id="revenue">
				<div class="bg_report">
	     
					<h3 class="data_title">{{trans('report.Revenue')}}</h3>

					      <div class="tab-content">
					        
					        <div id="home" class="tab-pane fade in active">
					          <table class="table table-striped table-bordered table-hover">
					            <thead class="dir_table_thead">
					              <tr>
					                <th>{{trans('report.Sold Amount')}}</th>
					                <th>{{trans('report.Table Sold')}}</th>
					                <th>{{trans('report.Customer Count')}}</th>
					                <th>{{trans('report.Discount')}}</th>
					                <th>{{trans('report.Sub Total')}}</th>
					                <!-- <th>{{trans('report.VAT')}}</th> -->
					              </tr>
					            </thead>
					            <tbody class="dir_table" id="revenue_data">
					              <tr>
					                <td colspan="6" class="text-center"><h4>{{trans('report.No Data!')}}</h4></td>
					              </tr>
					            </tbody>
					          </table>
					          
					        </div>
					       
					        </div>
	      		</div>

		</div>

	</div>


	<!-- Top Sale Cashier Table (limit 10) -->
	<div class="row report_dashboard">
		<div class="col-md-12 top_sale" id="top_sale">

			<div class="bg_report">
	     			<h3 class="data_title">{{trans('report.Top Sale Cashier')}}</h3>

					      <div class="tab-content">
					        
					        <div id="home" class="tab-pane fade in active">
					          <table class="table table-striped table-bordered table-hover">
					            <thead class="dir_table_thead">
					              <tr>
					                <th>{{trans('report.Cashier Name')}}</th>
					                <th>{{trans('report.Month')}}</th>
					                <th>{{trans('report.Sold Amount')}}</th>
					                <th>{{trans('report.Table Sold')}}</th>
					                <th>{{trans('report.Customer Count')}}</th>
					                <th>{{trans('report.Sub Total')}}</th>
					              </tr>
					            </thead>
					            <tbody class="dir_table" id="top_sale_data">
					              <tr>
					                <td colspan="6" class="text-center"><h4>{{trans('report.No Data!')}}</h4></td>
					              </tr>
					            </tbody>
					          </table>
					          
					        </div>
					       
					        </div>
	      		</div>



		</div>

	</div>

</div>

@endsection

@section('css')
<link 	rel="stylesheet" href="{{ asset('css/report_dashboard.css') }}">
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.min.js"></script>
<!-- Get Graph data for dashboard on load -->
<script type="text/javascript">
var date_interval_sub = 1;
var date_interval_add = 1;

$(function(){

	var filter 		= 'daily';
	var input_date 	= moment().startOf('day').format('YYYY-MM-DD');
	onload_dashboard(filter,input_date);

	$('body').on('change','#filter_option',function(){
		date_interval_sub = 1;
		date_interval_add = 1;

	});
});
</script>



<!-- Custom Date Arrow left -->
<script type="text/javascript">
	$(function(){
		$('body').on('click','.date_reverse',function(){
			if(date_interval_add >1){
				date_interval_add = 1;
			}

			var date 		= $('#filter_option').val();
			var input_date	= $('.date_range').val();
			custom_date_reverse(date,input_date);

		});

	});
</script>



<!-- Custom Date Arrow right -->
<script type="text/javascript">
	$(function(){
		$('body').on('click','.date_forward',function(){
			if(date_interval_sub > 1){
				date_interval_sub = 1;
			}

			var date 		= $('#filter_option').val();
			var input_date	= $('.date_range').val();
			custom_date_forward(date,input_date);


		});
	});
</script>


<!-- Change filter drop down -->
<script type="text/javascript">
	$(function(){
		$('body').on('change','#filter_option',function(){
			var date 		= $('#filter_option').val();
			filter_type_change(date);

		});
	});
</script>




<!-- ========================== START OF FUNCTION ========================== -->

<!-- Function Get Sold Amount -->
<script type="text/javascript">
	var linechart_sold_amount 	= null;
	function get_sold_amount(filter,input_date){
		var report_date = [];
		var sold_amount = [];
		var sold_amount_last = 0;
		var currency 	= [];
		var ctx = document.getElementById("sold_amount_chart").getContext('2d');
		
		
		
		jQuery.ajax({
				url : "{{ route('get_sold_amount') }}",
				type: "POST",
				data: {filter:filter, input_date:input_date},
				dataType: "JSON",
				success: function(data){
					

					for(var i = 1; i< data.length; i++){
						report_date.push(data[i].report_date);
						sold_amount.push(data[i].sold_amount.toFixed(2));
						currency = data[1].currency;
						sold_amount_last = data[1].sold_amount;

					}

					$('#sold_amount .chart_currency').html(currency);
					$('#sold_amount .chart_result').html(sold_amount_last.toFixed(2));

				},
				complete: function(data){
					

					//load Sold Amount data
					if(linechart_sold_amount!=null){
				        linechart_sold_amount.destroy();
				    }
					

					
					
					linechart_sold_amount = new Chart(ctx, {
						   type: 'line',
						   data: {
						      labels: report_date.reverse(),
						      datasets: [
										      {
										         label: "Sold Amount",
										         borderColor: 'green',
										         fill: true,
										         data: sold_amount.reverse(),
										      },
						      			]
						   },
						   options: {
								        elements: {
								            line: {
								                tension: 0, // disables bezier curves
								            }
								        }
								    }
					});




						


				},
				error : function(data){

				}
		});


	}
</script>


<!-- Function Get Table Sold -->
<script type="text/javascript">
	var linechart_table_sold	= null;
	function get_table_sold(filter,input_date){
		var report_date = [];
		var table_sold 	= [];
		var table_sold_last = 0;
		jQuery.ajax({
				url : "{{ route('get_table_sold') }}",
				type: "POST",
				data: {filter:filter, input_date:input_date},
				dataType: "JSON",
				success: function(data){
					

					for(var i = 1; i< data.length; i++){
						report_date.push(data[i].report_date);
						table_sold.push(data[i].table_sold);
						table_sold_last = data[1].table_sold;

					}

					$('#table_sold .chart_result').html(table_sold_last);

				},
				complete: function(data){
					//load Sold Amount data
					var ctx = document.getElementById("table_sold_chart").getContext('2d');

					if(linechart_table_sold!=null){
				        linechart_table_sold.destroy();
				    }
					linechart_table_sold = new Chart(ctx, {
						   type: 'line',
						   data: {
						      labels: report_date.reverse(),
						      datasets: [
										      {
										         label: "Table Sold",
										         borderColor: 'green',
										         fill: true,
										         data: table_sold.reverse(),
										      },
						      			]
						   },
						   options: {
								        elements: {
								            line: {
								                tension: 0, // disables bezier curves
								            }
								        }
								    }
					});
				},
				error : function(data){

				}
		});
	}
</script>


<!-- Function Get Customer Count -->
<script type="text/javascript">
	var linechart_customer_count = null;
	function get_customer_count(filter,input_date){
		var report_date		 	= [];
		var customer_count 		= [];
		var customer_count_last = 0;
		jQuery.ajax({
				url : "{{ route('get_customer_count') }}",
				type: "POST",
				data: {filter:filter, input_date:input_date},
				dataType: "JSON",
				success: function(data){
					

					for(var i = 1; i< data.length; i++){
						report_date.push(data[i].report_date);
						customer_count.push(data[i].customer_count);
						customer_count_last = data[1].customer_count;

					}

					$('#customer_count .chart_result').html(customer_count_last);

				},
				complete: function(data){
					//load Sold Amount data
					var ctx = document.getElementById("customer_count_chart").getContext('2d');

					if(linechart_customer_count!=null){
				        linechart_customer_count.destroy();
				    }

					linechart_customer_count = new Chart(ctx, {
						   type: 'line',
						   data: {
						      labels: report_date.reverse(),
						      datasets: [
										      {
										         label: "Customer Count",
										         borderColor: 'green',
										         fill: true,
										         data: customer_count.reverse(),
										      },
						      			]
						   },
						   options: {
								        elements: {
								            line: {
								                tension: 0, // disables bezier curves
								            }
								        }
								    }
					});
				},
				error : function(data){

				}
		});
	}
</script>


<!-- Function Get Discount -->
<script type="text/javascript">
	var linechart_discount = null;
	function get_discount(filter,input_date){
		var report_date		 	= [];
		var discount 			= [];
		var discount_last 		= 0;
		var currency 			= [];
		jQuery.ajax({
				url : "{{ route('get_discount') }}",
				type: "POST",
				data: {filter:filter, input_date:input_date},
				dataType: "JSON",
				success: function(data){
					

					for(var i = 1; i< data.length; i++){
						report_date.push(data[i].report_date);
						discount.push(data[i].discount.toFixed(2));
						discount_last = data[1].discount;
						currency = data[1].currency;

					}

					$('#discount .chart_currency').html(currency);
					$('#discount .chart_result').html(discount_last.toFixed(2));
					

				},
				complete: function(data){
					//load Sold Amount data
					var ctx = document.getElementById("discount_chart").getContext('2d');

					if(linechart_discount!=null){
				        linechart_discount.destroy();
				    }

					linechart_discount = new Chart(ctx, {
						   type: 'line',
						   data: {
						      labels: report_date.reverse(),
						      datasets: [
										      {
										         label: "Discount",
										         borderColor: 'green',
										         fill: true,
										         data: discount.reverse(),
										      },
						      			]
						   },
						   options: {
								        elements: {
								            line: {
								                tension: 0, // disables bezier curves
								            }
								        }
								    }
					});
				},
				error : function(data){

				}
		});
	}
</script>


<!-- Function Get SubTotal -->
<script type="text/javascript">
	var linechart_subtotal = null;
	function get_subtotal(filter,input_date){
		var report_date		 	= [];
		var sub_total 			= [];
		var sub_total_last 		= 0;
		var currency 			= [];
		jQuery.ajax({
				url : "{{ route('get_subtotal') }}",
				type: "POST",
				data: {filter:filter, input_date:input_date},
				dataType: "JSON",
				success: function(data){
					

					for(var i = 1; i< data.length; i++){
						report_date.push(data[i].report_date);
						sub_total.push(data[i].sub_total.toFixed(2));
						sub_total_last = data[1].sub_total;
						currency = data[1].currency;

					}

					$('#subtotal .chart_currency').html(currency);
					$('#subtotal .chart_result').html(sub_total_last.toFixed(2));

				},
				complete: function(data){
					//load Sold Amount data
					var ctx = document.getElementById("subtotal_chart").getContext('2d');

					if(linechart_subtotal!=null){
				        linechart_subtotal.destroy();
				    }

					linechart_subtotal = new Chart(ctx, {
						   type: 'line',
						   data: {
						      labels: report_date.reverse(),
						      datasets: [
										      {
										         label: "Sub Total",
										         borderColor: 'green',
										         fill: true,
										         data: sub_total.reverse(),
										      },
						      			]
						   },
						   options: {
								        elements: {
								            line: {
								                tension: 0, // disables bezier curves
								            }
								        }
								    }
					});
				},
				error : function(data){

				}
		});
	}
</script>






<!-- Onload Function -->
<script type="text/javascript">
	function onload_dashboard(filter,input_date){

	var date = moment().startOf('day').format('YYYY-MM-DD');
	$(".date_range").val(date);

	var input_date	= $('.date_range').val();

	if(filter == 'daily'){
		if(input_date == moment().startOf('day').format('YYYY-MM-DD')){
			$('.date_forward').css('display','none');
		}
	}else if(filter == 'weekly'){
		console.log('week');
		var current_date = moment().startOf('week').format('YYYY-MM-DD');
		var week_expload = input_date.split(' - ');
		if(week_expload[0] == current_date){
			$('.date_forward').css('display','none');
		}
	}else if(filter == 'monthly'){
		var current_date = moment().startOf('month').format('YYYY-MM-DD');
		var month_expload = input_date.split(' - ');
		if(month_expload[0] == current_date){
			$('.date_forward').css('display','none');
		}
	}else if(filter == 'yearly'){
		var current_date = moment().startOf('year').format('YYYY');
		if(input_date == current_date){
			$('.date_forward').css('display','none');
		}
	}


	jQuery.ajax({
			url : "{{ route('insert_report') }}",
			type: "POST",
			data: {filter:filter, input_date:input_date},
			dataType: "JSON",
	});

	// On load (Get today data)
	get_sold_amount(filter,input_date);
	get_table_sold(filter,input_date);
	get_customer_count(filter,input_date);
	get_discount(filter,input_date);
	get_subtotal(filter,input_date);
	get_list_data(filter,input_date,"{{ route('get_revenue') }}","#revenue_data");
	get_list_data(filter,input_date,"{{ route('get_top_sale_cashier') }}","#top_sale_data");


	
	}
</script>


<!-- Update Report data when change Filter type -->
<script type="text/javascript">
	function filter_type_change(date){
		if(date == "daily"){
				var day = moment().startOf('day').format('YYYY-MM-DD');
				$(".date_range").val(day);
				var input_date	= $('.date_range').val();

			}else if(date == "weekly"){
				var weekly_start = moment().startOf('isoWeek').format('YYYY-MM-DD');
				var weekly_end   = moment().endOf('isoWeek').format('YYYY-MM-DD');
				$(".date_range").val(weekly_start + " - " + weekly_end);
				var input_date	= $('.date_range').val();

			}else if(date == "monthly"){
				var monthly_start = moment().startOf('month').format('YYYY-MM-DD');
				var monthly_end   = moment().endOf('month').format('YYYY-MM-DD');
				$(".date_range").val(monthly_start + " - " + monthly_end);
				var input_date	= $('.date_range').val();

			}else if(date == "yearly"){
				var yearly = moment().startOf('year').format('YYYY');
				$('.date_range').val(yearly);
				var input_date	= $('.date_range').val();
			}

			jQuery.ajax({
					url : "{{ route('insert_report') }}",
					type: "POST",
					data: {filter:date, input_date:input_date},
					dataType: "JSON",
					success: function(data){
						sold_amount(data);

					}
			});



			var input	= $('.date_range').val();

			//get sold amout for today, this week, this month, this year
			get_sold_amount(date,input);
			get_table_sold(date,input);
			get_customer_count(date,input);
			get_discount(date,input);
			get_subtotal(date,input);
			get_list_data(date,input,"{{ route('get_revenue') }}","#revenue_data");
			get_list_data(date,input,"{{ route('get_top_sale_cashier') }}","#top_sale_data");


	}
</script>


<!-- Custom Date Reverse Function -->
<script type="text/javascript">
	function custom_date_reverse(date,input_date){
		
			if(date == "daily"){
				var current_date = moment().startOf('day').format('YYYY-MM-DD');
				if(input_date == current_date){
					date_interval_sub = 1;
					var day = moment().subtract(date_interval_sub, 'days').startOf('day').format('YYYY-MM-DD');
				}else{
					date_interval_sub = 1;
					var day = moment(input_date, "YYYY/MM/DD").subtract(date_interval_sub, 'days').startOf('day').format('YYYY-MM-DD');
				}
				$(".date_range").val(day);
				date_interval_sub++;

			}else if(date == "weekly"){
				var current_date = moment().startOf('week').format('YYYY-MM-DD');
				var week_expload = input_date.split(' - ');
				if(week_expload[0] == current_date){
					date_interval_sub = 1;
					var weekly_start = moment().subtract(date_interval_sub, 'weeks').startOf('isoWeek').format('YYYY-MM-DD');
					var weekly_end   = moment().subtract(date_interval_sub, 'weeks').endOf('isoWeek').format('YYYY-MM-DD');
				}else{
					date_interval_sub = 1;
					var weekly_start = moment(week_expload[0], "YYYY/MM/DD").subtract(date_interval_sub, 'weeks').startOf('isoWeek').format('YYYY-MM-DD');
					var weekly_end 	 = moment(week_expload[1], "YYYY/MM/DD").subtract(date_interval_sub, 'weeks').endOf('isoWeek').format('YYYY-MM-DD');
				}

				$('.date_range').val(weekly_start + ' - ' + weekly_end);
				date_interval_sub++;
			}else if(date == "monthly"){
				var current_date = moment().startOf('month').format('YYYY-MM-DD');
				var month_expload = input_date.split(' - ');
				if(month_expload[0] == current_date){
					date_interval_sub = 1;
					var monthly_start = moment().subtract(date_interval_sub, 'months').startOf('month').format('YYYY-MM-DD');
					var monthly_end   = moment().subtract(date_interval_sub, 'months').endOf('month').format('YYYY-MM-DD');
				}else{
					date_interval_sub = 1;
					var monthly_start = moment(month_expload[0],"YYYY/MM/DD").subtract(date_interval_sub, 'months').startOf('month').format('YYYY-MM-DD');
					var monthly_end   = moment(month_expload[1],"YYYY/MM/DD").subtract(date_interval_sub, 'months').endOf('month').format('YYYY-MM-DD');
				}

				$(".date_range").val(monthly_start + ' - ' + monthly_end);
				date_interval_sub++;

			}else if(date == "yearly"){

				var yearly = moment(input_date,"YYYY").subtract(date_interval_sub, 'years').startOf('year').format('YYYY');
				$('.date_range').val(yearly);
				
			}

			


			var input	= $('.date_range').val();
			jQuery.ajax({
					url : "{{ route('insert_report') }}",
					type: "POST",
					data: {filter:date, input_date:input},
					dataType: "JSON",
			});

			if(date == 'daily'){
				if(input != moment().startOf('day').format('YYYY-MM-DD')){
					$('.date_forward').css('display','inline-block');
				}else{
					$('.date_forward').css('display','none');
				}
			}else if(date == 'weekly'){
				var week_expload = input.split(' - ');
				if(week_expload[0] != moment().startOf('isoWeek').format('YYYY-MM-DD')){
					$('.date_forward').css('display','inline-block');
				}else{
					$('.date_forward').css('display','none');
				}
			}else if(date == 'monthly'){
				var month_expload = input.split(' - ');
				if(month_expload[0] != moment().startOf('month').format('YYYY-MM-DD')){
					$('.date_forward').css('display','inline-block');
				}else{
					$('.date_forward').css('display','none');
				}
			}else if(date == 'yearly'){

				if(input != moment().startOf('year').format('YYYY')){
					$('.date_forward').css('display','inline-block');
				}else{
					$('.date_forward').css('display','none');
				}
			}



		





			//get sold amout for today, this week, this month, this year
			get_sold_amount(date,input);
			get_table_sold(date,input);
			get_customer_count(date,input);
			get_discount(date,input);
			get_subtotal(date,input);
			get_list_data(date,input,"{{ route('get_revenue') }}","#revenue_data");
			get_list_data(date,input,"{{ route('get_top_sale_cashier') }}","#top_sale_data");

	}
</script>


<!-- Custom Date forward Function -->
<script type="text/javascript">
	function custom_date_forward(date,input_date){
		if(date == "daily"){
				var current_date = moment().startOf('day').format('YYYY-MM-DD');
				if(input_date == current_date){
					date_interval_add = 1;
					var day = moment().add(date_interval_add, 'days').startOf('day').format('YYYY-MM-DD');
				}else{
					date_interval_add = 1;
					var day = moment(input_date, "YYYY/MM/DD").add(date_interval_add, 'days').startOf('day').format('YYYY-MM-DD');
				}
				$(".date_range").val(day);
				date_interval_add++;

			}else if(date == "weekly"){
				var current_date = moment().startOf('week').format('YYYY-MM-DD');
				var week_expload = input_date.split(' - ');
				if(week_expload[0] == current_date){
					date_interval_add = 1;
					var weekly_start = moment().add(date_interval_add, 'weeks').startOf('isoWeek').format('YYYY-MM-DD');
					var weekly_end   = moment().add(date_interval_add, 'weeks').endOf('isoWeek').format('YYYY-MM-DD');
				}else{
					date_interval_add = 1;
					var weekly_start = moment(week_expload[0], "YYYY/MM/DD").add(date_interval_add, 'weeks').startOf('isoWeek').format('YYYY-MM-DD');
					var weekly_end 	 = moment(week_expload[1], "YYYY/MM/DD").add(date_interval_add, 'weeks').endOf('isoWeek').format('YYYY-MM-DD');
				}

				$('.date_range').val(weekly_start + ' - ' + weekly_end);
				date_interval_add++;
			}else if(date == "monthly"){
				var current_date = moment().startOf('month').format('YYYY-MM-DD');
				var month_expload = input_date.split(' - ');
				if(month_expload[0] == current_date){
					date_interval_add = 1;
					var monthly_start = moment().add(date_interval_add, 'months').startOf('month').format('YYYY-MM-DD');
					var monthly_end   = moment().add(date_interval_add, 'months').endOf('month').format('YYYY-MM-DD');
				}else{
					date_interval_add = 1;
					var monthly_start = moment(month_expload[0],"YYYY/MM/DD").add(date_interval_add, 'months').startOf('month').format('YYYY-MM-DD');
					var monthly_end   = moment(month_expload[1],"YYYY/MM/DD").add(date_interval_add, 'months').endOf('month').format('YYYY-MM-DD');

				}

				$(".date_range").val(monthly_start + ' - ' + monthly_end);
				date_interval_add++;

			}else if(date == "yearly"){
				var yearly = moment(input_date,"YYYY").add(date_interval_add, 'years').startOf('year').format('YYYY');
				$('.date_range').val(yearly);
				
			}

			

			var input	= $('.date_range').val();
			
			jQuery.ajax({
					url : "{{ route('insert_report') }}",
					type: "POST",
					data: {filter:date, input_date:input},
					dataType: "JSON",
			});

			if(date == 'daily'){
				if(input == moment().startOf('day').format('YYYY-MM-DD')){
					$('.date_forward').css('display','none');
				}
			}else if(date == 'weekly'){

				var week_expload = input.split(' - ');
				if(week_expload[0] == moment().startOf('isoWeek').format('YYYY-MM-DD')){
					$('.date_forward').css('display','none');
				}
			}else if(date == 'monthly'){
				var month_expload = input.split(' - ');
				if(month_expload[0] == moment().startOf('month').format('YYYY-MM-DD')){
					$('.date_forward').css('display','none');
				}
			}else if(date == 'yearly'){
				if(input == moment().startOf('year').format('YYYY')){
					$('.date_forward').css('display','none');
				}
			}

			//get sold amout for today, this week, this month, this year
			get_sold_amount(date,input);
			get_table_sold(date,input);
			get_customer_count(date,input);
			get_discount(date,input);
			get_subtotal(date,input);
			get_list_data(date,input,"{{ route('get_revenue') }}","#revenue_data");
			get_list_data(date,input,"{{ route('get_top_sale_cashier') }}","#top_sale_data");



	}
</script>


<!-- get revenue data function-->
<script type="text/javascript">
	function get_list_data(filter,input_date,route_location,$list_id){

		jQuery.ajax({
				url : route_location,
				type: "POST",
				data: {filter:filter, input_date:input_date},
				dataType: "html",
				success: function(data){
					$($list_id).html(data);
				},
				complete: function(data){

				},
				error: function(data){

				}
		});

	}
</script>
<!-- ========================== END OF FUNCTION ========================== -->
@endsection




