@extends('crudbooster::admin_template')
@section('content')
<link 	rel="stylesheet" href="{{ asset('css/report/report.css') }}">
<!-- Calculation Process -->
<div class="amount_calculation" style="display: none;">
	@foreach($overall_ravenue as $revenue)
		<?php $r += $revenue->total_with_tax_discount ?>
	@endforeach
	
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-3 revenue">
			<div class="revenue_title">Overall Revenue</div>
			<div class="revenue_date">{{ $overall_ravenue_first->order_date }} <span class="seperator">-</span> {{ $overall_ravenue_last->order_date }}</div>
			<div class="revenue_icon"><i class="fa fa-bar-chart text-normal"></i></div>
			<div class="revenue_amount">{{ $setting->currency_symbol }} <?php echo $r; ?></div>
		</div>
	</div>

</div>





<!-- Loading -->
<div class="se-pre-con-detail"></div>
@endsection
<script src="{{ asset('js/jquery_animation/jquery.min.js') }}"></script>
<script src="{{ asset('js/jquery_animation/modernizr.js') }}"></script>
<link 	rel="stylesheet" href="{{ asset('css/loading.css') }}">

<script>
	$(window).load(function() {
		$(".se-pre-con-detail").fadeOut("slow");;
	});
	var url="{{URL::to('/')}}";
</script>