@extends('crudbooster::admin_template')
@section('content')

	
<body id="invoice">
<form class='form' method='get' id="form" enctype="multipart/form-data" action="{{ route('get_sort_void_payment') }}">	
<div class="container-fluid">
	  	<div class="filter">

			<div class="row">

				<!-- =================== Time Filter =================== -->
				<div class="col-md-4 filter_date">
					<div class="form-group">
						<label for="sel1">{{trans('report.Select Sort (This week,month,year):')}}</label>
						<select class="form-control btnsort" id="by" name='by'>
							<option value='' 		@if($_GET['by'] == '') 		 	selected @endif >Please Choose... </option>
							<option value="weekly"  @if($_GET['by'] == 'weekly')  	selected @endif >Weekly</option>              
							<option value="monthly" @if($_GET['by'] == 'monthly') 	selected @endif >Monthly</option>
							<option value="yearly"  @if($_GET['by'] == 'yearly')  	selected @endif >Yearly</option>
						</select>
					</div>
				</div>

				<!-- =================== Date Filter =================== -->
				<div class="col-md-4 filter_custom_date">
					<div class="form-group">
						<input type="text" class="hidden" autofocus>
						<div class="form-group timepicker">
							<label>{{trans('report.From Date')}}</label>
							<input type="text" readonly="" name="daterange" class="form-control btn_date" placeholder="Please choose day" value="{{$_GET['daterange']}}"/>
							<span style="pointer-events: none;" class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
				</div>

			</div>


			<div class="row search" style="display: none;">
				<div class="col-md-12">
					<button type="submit" class='btn btn-success btnsearch'>
						<i class="fa fa-search"></i>
					</button>
				</div>
			</div>

		</div>	
	<!-- End of Option for choosing filter -->


	<div class="col-md-12">
	  <div class="row">
	    <div class="bg_report">
	     

	      <div class="tab-content">

	        <div id="home" class="tab-pane fade in active">
	          <table class="table table-striped table-bordered table-hover">
	            <thead class="dir_table_thead">
						<tr>
							<th class="col-md-1">{{trans('report.No.')}}</th>
							<th class="col-md-2">{{trans('report.Order Date')}}</th>
							<th class="col-md-2">{{trans('report.Invoice No')}}</th>
							<th class="col-md-1">{{trans('report.Sub Total')}}</th>
							<th class="col-md-2">{{trans('report.Location')}}</th>
							<th class="col-md-1">{{trans('report.Action')}}</th>
						</tr>
					</thead>

					<tbody class="dir_table" id="table_info">
						@if(count($invoices) >= 1)
							@foreach($invoices as $key => $invoice)
							<tr>
								<td class="col-md-1">{{ ++$key }}</td>
								<td class="col-md-2">{{ $invoice->invoice_date }}</td>
								<td class="col-md-2">{{ $invoice->invoice_number }}</td>
								<td class="col-md-1">{{$invoice->currency}}{{ $invoice->grand_total }}</td> 
								<td class="col-md-2">{{ $invoice->company_name }}</td>
								<td class="col-md-1">
									<a class="invoice_show"  data-val="{{$invoice->invoice_number}}">
										<i class="fa fa-eye"></i>
									</a> 
									<input type="hidden" id="invoiceId" value="{{ $invoice->invoice_number }}">
								</td>

							</tr>
							@endforeach
							
						@else
							<tr>
								<td colspan="5" class="text-center"><h4>{{trans('report.No Data!')}}</h4></td>
							</tr>
						@endif
					</tbody>
				</table>

				{!! $invoices->render() !!}
	          
	        </div>
	       
	        </div>
	      </div>
	    </div>
	  </div>
	

	<div class="se-pre-con-detail"></div>


</div>
</form>
</body>

@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/sale_by_invoice.css') }}">
<link rel="stylesheet" href="{{ asset('css/loading.css') }}">
@endsection

@section('script')
<script>
$(function() {
  $(".se-pre-con-detail").fadeOut("slow");;
});
</script>

<script type="text/javascript">
$(function(){
	$('body').on('change', '.btnsort', function(e) {
      	clear_filter('','custom_date');
      	$('.btnsearch').click();
    });

	$('body').on('focus','.btn_date',function(){
      	$('.btnsort').prop('checked', false);
      	$('input[name="daterange"]').daterangepicker({
        format: 'YYYY-MM-DD'
        });
	});

	$('body').on('click', '.applyBtn', function(event) {
    	clear_filter('filter_date','');
    	$('.btnsearch').click();
	});

	$('body').on('click','.invoice_show',function(){
			var invoice_no = $(this).attr('data-val');
			jQuery.ajax({
				url 	: "{{ route('report.view_invoice') }}",
				method	: "POST",
				data 	: {invoice_no : invoice_no},
				success : function(data){
					$('#view_modal .modal-body').html(data);
					$('#view_modal').modal('show');
				},
				error 	: function(data){
					swal("Warning", "There must be a problem with the request ! Please check again", "warning");
				}

			});
			
		});

});

</script>




<!-- =============== START OF FUNCTION ================ -->

<script type="text/javascript">
  function clear_filter(filter0,filter1){
      // Filter data
      if(filter0 == "filter_date"){
        $('.btnsort').prop('checked', false);
        $('.btnsort').val();
        $('.btnsort option:eq(0)').prop('selected', true);
      }

      // Filter Custom Date
      if(filter1 == "custom_date"){
          $('.btn_date').val("");
      }



  }
</script>
<!-- =============== END OF FUNCTION ================ -->
<!-- =============== copy data =======================-->
<!-- <script type="text/javascript">
	$(function(){
	$('body').on('click','.voidCopy',function(){
			var invoice_no = $(this).attr('data-val');
			jQuery.ajax({
				url 	: "{{ route('cash_management.void_copy_invoice') }}",
				method	: "POST",
				data 	: {invoice_no : invoice_no},
				success : function(data){
                    swal({ 
					  title: "Complete",
					   text: "Successfully void this invoice",
					    type: "success" 
					  },
					  function(){
					    location.reload();
					});
				},
				error 	: function(data){
					swal("Warning", "There must be a problem with the request ! Please check again", "warning");
				}

			});
			
	});
});
</script> -->
<!-- =============== end copy data ===================-->

@endsection






