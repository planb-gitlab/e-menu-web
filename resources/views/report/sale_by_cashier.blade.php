@extends('crudbooster::admin_template')
@section('content')

<body id="item">
<form class='form' method='get' id="form" enctype="multipart/form-data" action="{{ route('get_sort_cashier') }}">	
<div class="container-fluid">
	  
	  <div class="filter">

	  	<div class="row">

          	<!-- =================== Time Filter =================== -->
	        <div class="col-md-3 filter_date">
	            <div class="form-group">
	              <label for="sel1">{{trans('report.Select Sort (This week,month,year):')}}</label>
	              <select class="form-control btnsort" id="by" name='by'>
	                <option value=''    @if($_GET['by'] == '')              selected @endif >Please Choose... </option>
	                <option value="weekly"  @if($_GET['by'] == 'weekly')    selected @endif >Weekly</option>              
	                <option value="monthly" @if($_GET['by'] == 'monthly')   selected @endif >Monthly</option>
	                <option value="yearly"  @if($_GET['by'] == 'yearly')    selected @endif >Yearly</option>
	              </select>
	            </div>
	        </div>

          	<!-- =================== Date Filter =================== -->
	        <div class="col-md-4 filter_custom_date">
	            <div class="form-group">
	              <input type="text" class="hidden" autofocus>
	              <div class="form-group timepicker">
	                <label>{{trans('report.From Date')}}</label>
	                <input type="text" readonly="" name="daterange" class="form-control btn_date" placeholder="Please choose day" value="{{$_GET['daterange']}}"/>
	                <span style="pointer-events: none;" class="input-group-addon">
	                  <span class="glyphicon glyphicon-calendar"></span>
	                </span>
	              </div>
	            </div>
	        </div>

          	<!-- =================== Item code =================== -->
          	<div class="col-md-3">
		        <div class="form-group">
		            <label for="sel1">{{trans('report.Select User To Sort')}} :</label>
		            <select class="form-control" id="search_by_user" name='cashier_name' style="width: 300px; min-height: 40px;">
		              <option value="">**Please Choose... </option>
		              <?php foreach ($get_user as $key => $get_users): ?>
		              		<option value="<?php echo $get_users->id; ?>" @if($_GET['cashier_name'] == $get_users->id) selected @endif><?php echo $get_users->name; ?></option> 
		              <?php endforeach ?>
		                           
		            </select>
		        </div>
		    </div>

      </div>

     	<div class="row search" style="display: none;">
				<div class="col-md-12">
					<button type="submit" class='btn btn-success btnsearch'>
						<i class="fa fa-search"></i>
					</button>
				</div>
		</div>
	    
	</div>


	<div class="col-md-12">
	  <div class="row">
	    <div class="bg_report">
	     

	      <div class="tab-content">
	        <div id="home" class="tab-pane fade in active">
	          <table class="table table-striped table-bordered table-hover">
	            <thead class="dir_table_thead">
	              <tr>
	              	<th class="col-md-1">{{trans('report.No.')}}</th>
	                <th class="col-md-3">{{trans('report.Cashier Name')}}</th>
	                <th class="col-md-2">{{trans('report.Sold Amount')}}</th>
	                <th>{{trans('report.Table Sold')}}</th>
	                <th>{{trans('report.Customer Count')}}</th>
	              </tr>
	            </thead>
	            <tbody class="dir_table" id="table_info">
                 @if(count($invoices) >= 1)
                    @foreach($invoices as $key => $invoice)
                    <tr>
                      <td class="col-md-1">{{ ++$key }}</td>
                      <td class="col-md-3">{{ $invoice->name }}</td>
                      <td class="col-md-2">{{ $invoice->currency }}{{ $invoice->grand_total }}</td>
                      <td>{{$invoice->table_sold}}</td>
                      <td>{{$invoice->table_sold}}</td>

                    </tr>
                    @endforeach
                    
                  @else
                    <tr>
                      <td colspan="5" class="text-center"><h4>{{trans('report.No Data!')}}</h4></td>
                    </tr>
                  @endif
                </tbody>
              </table>

              {!! $invoices->render() !!}
	          </table>
	          
	        </div>
	       
	        </div>
	      </div>
	    </div>
	  </div>

	<div class="se-pre-con-detail"></div>

</div>
</form>
</body>

@endsection


@section('css')
<link rel="stylesheet" href="{{ asset('css/sale_by_invoice.css') }}">
<link rel="stylesheet" href="{{ asset('css/loading.css') }}">
@endsection


@section('script')
<script>
	    $(function() {
	      $(".se-pre-con-detail").fadeOut("slow");;
	    });
</script>

<script>
$(function(){
	$('#search_by_user').select2();
});

$(function(){
	$('body').on('change', '.btnsort', function(e) {
      clear_filter('','custom_date','search_by_user');
      $('.btnsearch').click();
    });

$('body').on('focus','.btn_date',function(){
      $('.btnsort').prop('checked', false);
      $('input[name="daterange"]').daterangepicker({
        format: 'YYYY-MM-DD'
        });
    });

$('body').on('click', '.applyBtn', function(event) {
      	clear_filter('filter_date','','search_by_user');
      	$('.btnsearch').click();
    });
});

$('body').on('change','#search_by_user',function(){
    clear_filter('filter_date','custom_date','');
    $('.btnsearch').click();
});
</script>



<!-- ================== START OF FUNCTION =================== -->
<script type="text/javascript">
  function alert_timer(){
    swal({
        title: 'Searching in progress',
        text: 'Please wait patiently',
        timer: 500,
        showCancelButton: false,
        showConfirmButton: false
      });
  }
</script>


<script type="text/javascript">
  function clear_filter(filter0,filter1,filter2){
      // Filter data
      if(filter0 == "filter_date"){
        $('.btnsort').prop('checked', false);
        $('.btnsort').val();
        $('.btnsort option:eq(0)').prop('selected', true);
      }

      // Filter Custom Date
      if(filter1 == "custom_date"){
          $('.btn_date').val("");
      }

      // Item Code
      if(filter2 == "search_by_user"){
          $('#search_by_user').prop('checked', false);
          $('#search_by_user').val();
          $('#search_by_user option:eq(0)').prop('selected', true).trigger('change.select2');
      }


  }
</script>

<!-- ================== END OF FUNCTION =================== -->
@endsection











