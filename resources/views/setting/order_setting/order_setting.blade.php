@extends('crudbooster::admin_template')
@section('content')

<div class="container-fluid">
	<div class="row">
		<a href="#order_layout" data-toggle="tab" id="order_layout_btn" class="nav_btn">
			<span class="hint"></span>
			<div class="col-md-2 col-xs-5 tabs" class="active">
				<i class="fa fa-file-o"></i> <br/>
				{{ trans('setting.order_layout') }}
			</div>
		</a>

		<a href="#discount_privilege" data-toggle="tab" id="discount_privilege_btn" class="nav_btn">
			<span class="hint"></span>
			<div class="col-md-2 col-xs-3 tabs">
				<i class="fa fa-percent"></i> <br/>
				{{ trans('setting.discount_privilege') }}
			</div>
		</a>

		<a href="#print_setting" data-toggle="tab" id="print_setting_btn" class="nav_btn">
			<span class="hint"></span>
			<div class="col-md-2 col-xs-3 tabs" id="printSetting">
				<i class="fa fa-print"></i> <br/>
				{{ trans('setting.print_setting') }}
			</div>
		</a>

		<a href="#invoice_layout" data-toggle="tab" id="invoice_layout_btn" class="nav_btn">
			<span class="hint"></span>
			<div class="col-md-2 col-xs-3 tabs">
				<i class="fa fa-list-alt"></i> <br/>
				{{ trans('setting.invoice_layout') }}
			</div>
		</a>



	</div>

	<div class="row setting_info">

		<div class="tab-content">
			<div class="tab-pane active box_info in_progress" id="order_layout">
				<div class="row">
				<div class="col-md-12">
						<div class="box-body">

							<div class="row">
								<div class="col-md-6 order_layout border-right" data-value="1">
									<img src="{{asset('uploads/defualt_image/setting/order_layout/01.order.jpg')}}" />
								</div>
							</div>

						</div>
				</div>
				</div>
			</div>


			<div class="tab-pane box_info in_progress" id="discount_privilege">

					<input type="hidden" class="form-control" name="company_id"  value="">

					<div class="row">
						<div class="col-md-12">
							<div class="box-body">

								<div class="row">
									<div class="col-md-6">
										<table class="table table-striped table-bordered table-hover">
											<thead class="dir_table_thead no_permission">
												<tr>
													<td colspan="3">{{trans('setting.No Permission')}}</td>
												</tr>

												<tr>
													<td>{{trans('setting.No')}}</td>
													<td>{{trans('setting.Name')}}</td>
													<td>{{trans('setting.Action')}}</td>
												</tr>
											</thead>

											<tbody class="dir_table">
												@foreach($users_no_permit as $key => $user)
												<tr>
													<td>{{++$key}}</td>
													<td>{{$user->name}}</td>
													<td><i class="fa fa-plus add_permission" data-val="{{$user->id}}" title="Make this user able to make discount"></i></td>


												</tr>
												@endforeach
											</tbody>
										</table>
									</div>

									<div class="col-md-6">
										<table class="table table-striped table-bordered table-hover">
											<thead class="dir_table_thead full_permission">
												<tr>
													<td colspan="3">{{trans('setting.Full Permission')}}</td>
												</tr>

												<tr>
													<td>{{trans('setting.No')}}</td>
													<td>{{trans('setting.Name')}}</td>
													<td>{{trans('setting.Action')}}</td>
												</tr>
											</thead>

											<tbody class="dir_table">
												@foreach($users_with_permit as $key => $user)
												<tr>
													<td>{{++$key}}</td>
													<td>{{$user->name}}</td>
													<td><i class="fa fa-times remove_permission" data-val="{{$user->id}}" title="Make this user not able to make discount"></i></td>


												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>

							</div>
						</div>
					</div>

				</div>

				<div class="tab-pane box_info in_progress" id="print_setting" data-print="{{ $setting->print_setting }}">
					

					<div class="row">
						<div class="col-md-12">
							<div class="box-body">
								<div class="row">
									<div class="col-md-4">
												<table class="table table-striped table-bordered table-hover">
													<tr>
														<td colspan="4" class="headPrintText">{{trans('setting.Choose print setting')}}</td>
													</tr>


													<tr>
														<td>{{trans('setting.Print Ticket')}}</td>
														<td><input type="checkbox" id="ticketPrint"></td>
														<td><img class="printer_setting" data-printer="ticket" style="width:25px;" src="{{asset('uploads/defualt_image/printer_setting.png')}}"/></td>
													</tr>

													<tr>
														<td>{{trans('setting.Print Invoice')}}</td>
														<td><input type="checkbox" id="invPrint"></td>
														<td><img class="printer_setting" data-printer="invoice" style="width:25px;" src="{{asset('uploads/defualt_image/printer_setting.png')}}"/></td>
													</tr>

												</table>
												<div style="text-align: right;">
													<input type="submit" name="submit" value="Save" class="btn btn-success submit_print">
												</div>

									</div>

									<div class="col-md-8">
										<img class="preview" 
										@if($setting->print_setting == 1)
											src="{{asset('uploads/defualt_image/setting/01.For-order-setting.png')}}" 
										@elseif($setting->print_setting == 2)
											src="{{asset('uploads/defualt_image/setting/02.For-order-setting.png')}}"
										@elseif($setting->print_setting == 3)
											src="{{asset('uploads/defualt_image/setting/03.For-order-setting.png')}}"
										@elseif($setting->print_setting == 4)
											src="{{asset('uploads/defualt_image/setting/04.For-order-setting.png')}}"
										@endif
										/>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="tab-pane box_info in_progress" id="invoice_layout">
					<div class="row">
					<div class="col-md-12">
							<div class="box-body">

								<div class="row">
									<div class="col-md-3 order_layout border-right" data-value="1">
										<img src="{{asset('uploads/defualt_image/setting/invoice_layout/01.invoice.jpg')}}" />
									</div>
								</div>

							</div>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	@endsection

	@section('css')
	<style type="text/css">
		.invPrint,.ticketPrint{
			right: 70%;
		}
		.printSetting{
			overflow: hidden;
			text-align: left;
			box-shadow: 0 1px 2px grey;

		}
		.headPrintText{
			background: #3c5a99;
			text-align: center;
			color: #fff;
			
		}
		.choosing{
			overflow: hidden;
			padding: 15px;
		}
		.choosing label{
			width: 30%;
			font-size: 15px;
		}

		img.preview {
		    width: 100%;
		    box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
		    background: #ecf0f5;
		}

		#saveBtn{
			right: 0px;
		}

		.printer_setting{
			cursor: pointer;
		}

		.printer_setting:hover{
			opacity: 0.8;
		}
	</style>
	<link rel="stylesheet" type="text/css" href="{{asset('css/setting/order_setting.css')}}">
	@endsection

	
	@section('script')
	<script>
		$(function(){
			var print_setting = $('#print_setting').attr('data-print');
			if(print_setting == 1){
				$('#print_setting #ticketPrint').prop('checked',false)
				$('#print_setting #invPrint').prop('checked',true);
			}else if(print_setting == 2){
				$('#print_setting #ticketPrint').prop('checked',true)
				$('#print_setting #invPrint').prop('checked',false);
			}else if(print_setting == 3){
				$('#print_setting #ticketPrint').prop('checked',true)
				$('#print_setting #invPrint').prop('checked',true);
			}else if(print_setting == 4){
				$('#print_setting #ticketPrint').prop('checked',false)
				$('#print_setting #invPrint').prop('checked',false);
			}
		});
	</script>



	<script>
		$(function(){
			var number="";
			$('#print_setting input[type="checkbox"]').click(function(){
				var ticketAttr = $('#print_setting #ticketPrint').prop('checked');
				var invAttr = $('#print_setting #invPrint').prop('checked');
				
				if(invAttr == true && ticketAttr == false){
					number = 1;
					$('#print_setting .preview').attr('src','{{ asset("uploads/defualt_image/setting/01.For-order-setting.png") }}');
				}
				else if(invAttr == false && ticketAttr == true){
					number = 2;
					$('#print_setting .preview').attr('src','{{ asset("uploads/defualt_image/setting/02.For-order-setting.png") }}');


				}else if(invAttr == true && ticketAttr == true){
					number = 3;
					$('#print_setting .preview').attr('src','{{ asset("uploads/defualt_image/setting/03.For-order-setting.png") }}');


				}else if(invAttr == false && ticketAttr == false){
					number = 4;
					$('#print_setting .preview').attr('src','{{ asset("uploads/defualt_image/setting/04.For-order-setting.png") }}');
				}
				


			});
			$('body').on('click','.submit_print',function(){
				var invAttr = $('#print_setting #invPrint').prop('checked');
				var ticketAttr = $('#print_setting #ticketPrint').prop('checked');
				if(invAttr == true && ticketAttr == false){
					number = 1;
				}
				else if(invAttr == false && ticketAttr == true){
					number = 2;
				}else if(invAttr == true && ticketAttr == true){
					number = 3;
				}else if(invAttr == false && ticketAttr == false){
					number = 4;
				}
				jQuery.ajax({
					url 	: " {{ route('print_setting')}}",
					type 	: 'POST',
					data 	: {print_setting:number},
					success : function(data){
						swal('Order Complete','Your request has been send','success');

					},
					error	: function(data){
						swal("Warning", "There must be a mistake, Please Check again!", "warning");
					}
				});
			});
		});
		$(function(){


			$('body').on('click','.add_permission',function(){
				var user_id = $(this).attr('data-val');
				swal({
					title: "Are you sure?",
					text: "This user will have FULL permission to use discount when order",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: '#DD6B55',
					confirmButtonText: 'Yes',
					cancelButtonText: "No",
					closeOnConfirm: false,
					closeOnCancel: false,
					reverseButtons : true
				},
				function(isConfirm){
					if (isConfirm){
						jQuery.ajax({
							url 	: "{{ route('update_discount_permission') }}",
							method 	: "POST",
							data 	: {user_id: user_id,permit : 1},
							success : function(data){
								if(data){
									window.location.reload();
								}

							},
							error 	: function(data){
								swal("Warning", "There must be a mistake, Please Check again!", "warning");
							}
						});
					}else{
						swal.close();
					}
				});

			});
		});


		$(function(){
			$('body').on('click','.remove_permission',function(){
				var user_id = $(this).attr('data-val');
				swal({
					title: "Are you sure?",
					text: "This user will have NO permission to use discount when order",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: '#DD6B55',
					confirmButtonText: 'Yes',
					cancelButtonText: "No",
					closeOnConfirm: false,
					closeOnCancel: false,
					reverseButtons : true
				},
				function(isConfirm){
					if (isConfirm){
						jQuery.ajax({
							url 	: "{{ route('update_discount_permission') }}",
							method 	: "POST",
							data 	: {user_id: user_id, permit : 0},
							success : function(data){
								if(data){
									window.location.reload();
								}

							},
							error 	: function(data){
								swal("Warning", "There must be a mistake, Please Check again!", "warning");
							}
						});
					}else{
						swal.close();
					}
				});

			});
		});
	</script>

	<!-- Hint -->
	<script>
		$(function(){
			$('#order_layout_btn .hint').on('click',function(){
				swal("{{trans('setting.order_layout')}}", "{{trans('setting.order_layout_info')}}", "info");
			});

			$('#discount_privilege_btn .hint').on('click',function(){
				swal("{{trans('setting.discount_privilege')}}", "{{trans('setting.discount_privilege_info')}}", "info");
			});

			$('#print_setting_btn .hint').on('click',function(){
				swal("{{trans('setting.print_setting')}}", "{{trans('setting.print_setting_info')}}", "info");
			});

			$('#invoice_layout_btn .hint').on('click',function(){
				swal("{{trans('setting.invoice_layout')}}", "{{trans('setting.invoice_layout_info')}}", "info");
			});
		});
	</script>


	<!-- ====================== Printer Configuration (Open Modal) ======================= -->
	<script>
		$(function(){
			$('.printer_setting').on('click',function(){
				var printer_type = $(this).data('printer');
				$('#printer_modal .save_printer').attr('data-printer',printer_type);
				$('#printer_modal .printer_configuration_name').val('');
				jQuery.ajax({
							url 	: "{{ route('find_printer_name') }}",
							method 	: "POST",
							data 	: {printer_type : printer_type},
							beforeSend : function(){},
							success : function(data){
									$('#printer_modal .printer_configuration_name').val(data);
									$('#printer_modal').modal('show');
							},
							error 	: function(data){}
				});
				
			});
		});
	</script>

	<!-- ====================== Printer Configuration (Open Modal) ======================= -->
	<script>
		$(function(){
			$('#printer_modal .save_printer').on('click',function(){
				var printer_name = $('#printer_modal .printer_configuration_name').val();
				var printer_type = $(this).data('printer');
				jQuery.ajax({
							url 	: "{{ route('update.printer_name') }}",
							method 	: "POST",
							data 	: {printer_name : printer_name, printer_type : printer_type},
							beforeSend : function(){},
							success : function(data){
								swal({ title: 'Printer Configuration Complete', text: 'This printer has been setup ... .',showConfirmButton: false,timer:1500 },function(){
                                               window.location.reload();
                                            });
								

							},
							error 	: function(data){}
				});
			});
		});
	</script>

	@endsection

	@section('modal')
		<div class="modal fade printer_modal" id="printer_modal" role="dialog" tabindex='-1'>
		    <div class="modal-dialog">
		        <div class="modal-content">

		            <div class="modal-header">
		                <button type="button" class="close close_popup" data-dismiss="modal">&times;</button>
		                <h4 class="modal-title">Printer Configuration</h4>
		            </div>

		            <div class="modal-body">
		              <input type="text" name="printer_name" class="form-control printer_configuration_name" placeholder="Please input printer name">
		          	</div>

		          	<div class="modal-footer">
			            <button type="button" class="btn btn-default save_printer" >{{trans('order.Save')}}</button>
			            <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('order.Close')}}</button>
			        </div>

		      </div>
		  </div>
		</div>
	@endsection