@extends('crudbooster::admin_template')
@section('content')

	
	<!-- css Link -->
	<link rel="stylesheet" href="{{ asset('css/sale_by_invoice.css') }}">
    <script src="{{ asset('js/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/status_remove.js') }}" type="text/javascript"></script>

    <link rel="stylesheet" href="{{ asset('css/loading.css') }}">

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
<script>

    $(window).load(function() {
        $(".se-pre-con-detail").fadeOut("slow");;
    });
</script>

	<div style="font-size: 30px; margin-bottom: 20px;">
	 <strong >{!! $page_name or "User Activity Detail" !!}</strong>
	</div>
	<!-- go back to previous page -->
    @if(CRUDBooster::getCurrentMethod() != 'getProfile' && $button_cancel)
            @if(g('return_url'))
                <p><a title='Return' href='{{g("return_url")}}'><i class='fa fa-chevron-circle-left '></i>
                        &nbsp; {{trans("crudbooster.form_back_to_list",['module'=>CRUDBooster::getCurrentModule()->name])}} "Sale by Invoice Report"</a></p>
            @else
                <p><a title='Main Module' href='{{CRUDBooster::mainpath()}}'><i class='fa fa-chevron-circle-left '></i>
                        &nbsp; {{trans("crudbooster.form_back_to_list",['module'=>CRUDBooster::getCurrentModule()->name])}} "Sale Invoice Report"</a></p>
            @endif
    @endif
    
    <!-- End of "go back to previous page" -->


	 <div class="panel panel-default">
            <!-- Page Title -->
            <div class="panel-heading">
                <strong>{!! $page_title or "Page Title" !!}</strong>
            </div>
            <!-- End of page Title -->

            <div class="panel-body" style="padding: 20px 0 0 0">
               
                <div class="box-body" id="parent-form-area">

                    <div class="table-responsive">
                        <table id="table-detail" class="table table-striped-main" style="margin-bottom: 20px;">
                            <tbody >
                                <tr>
                                    <td>Created Date</td>
                                    <td>{{ $logs->created_at }}</td>
                                    

                                </tr>

                                <tr>
                                    <td>IP Address</td>
                                    <td>{{ $logs->ipaddress }}</td>
                                    
                                </tr>

                                <tr >
                                    <td>Device (Browser)</td>
                                    <td id="useragent">{{ $logs->useragent }}</td>
                                    
                                </tr>

                                 <tr >
                                    <td>URL</td>
                                    <td>{{ $logs->url }}</td>
                                    
                                </tr>

                                 <tr >
                                    <td>Activity</td>
                                    <td>{{ $logs->description }}</td>
                                    
                                </tr>
                                
                            </tbody>
                        </table>
                        
						 
                        <?php echo "$logs->details"; ?>     
                          
 
                    </div>                                            

                </div>

                    

            </div>



     </div>

     <div class="se-pre-con-detail"></div>

@endsection







