@extends('crudbooster::admin_template')
@section('content')

<link rel="stylesheet" href="{{ asset('css/logs.css') }}">

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
<script>

	$(window).load(function() {
		$(".se-pre-con-detail").fadeOut("slow");;
	});

  var url="{{URL::to('/')}}";
</script>


<div class="panel panel-default">
            <!-- Page Title -->
            <div class="panel-heading">
                <strong><i class='{{CRUDBooster::getCurrentModule()->icon}}'></i> {!! "Filter" !!}</strong>
                <!-- Date Filter -->
			    
			      <div class="form-group">
			        <input type="text" class="hidden" autofocus>
			        <div class="form-group timepicker">
			          <input type="text" readonly="" name="daterange" class="form-control btn_date" placeholder="Please choose day" />
			           <span style="pointer-events: none;" class="input-group-addon">
			            <span  class="glyphicon glyphicon-calendar"></span>
			        </span>
			        </div>
			      </div>

			      <div class="form" style="display:inline-block;width: 260px;">
                    <div class="input-group">
                        <input type="text" id="search" name="search" value="" class="form-control input-sm pull-right search" placeholder="Search">
                        
                        <div class="input-group-btn">
                            <button id="submit" type="submit" class="btn btn-sm btn-default btnsearch"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                   </div>

                    <!-- <div class="form" style="display:inline-block;width: 56px;">
                     <div class="input-group">
                        <select name="limit" style="width: 56px;" class="form-control input-sm record">
                            <option value="5">5</option>
                            <option selected="" value="10">10</option>
                            <option value="20">20</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                            <option value="200">200</option>
                        </select>
                    </div>
                	</div> -->


			    
			    <!-- End Date Filter -->

            </div>


            <!-- End of page Title -->


      <div class="panel-body" style="padding: 20px 0 0 0">
				<div class="container">
					<table class="table table-bordered log_table">
					<thead class="dir_table_thead">
						<tr class="title">
							<th class="col-md-1">{{trans('setting.No')}}</th>
							<th>{{trans('setting.Activity')}}</th>
							<th>Date{{trans('setting.Created')}}</th>
							<th>{{trans('setting.Action')}}</th>
						</tr>
					</thead>

					
		           
					<tbody class="dir_table">
                @if($logs->count() >= 1)
                  @foreach ($logs as $key => $log)

                    <tr class="title">
                      <td class="col-md-1">{{++$key}}</td>
                      <td>{{$log->description}}</td>
                      <td>{{$log->created_at}}</td>
                      <td>
                        <a class="view_button unselectable" data-id="{{$log->id}}">{{trans('setting.view detail')}}</a>
                      </td>
                    </tr>

                  @endforeach

                  
                  @else
			             <tr>
			                <td colspan="4" class="text-center"><h4>{{trans('setting.No Data!')}}</h4></td>
			             </tr>
                @endif
		              	
		      </tbody>   

              

		            
					

					</table>

          <div class="page">
            @if($logs->count() >= 1)
                  {{ $logs->links() }}
            @endif
          </div>

          <div class="se-pre-con-detail"></div>



					

				</div>
			</div>
</div>


<!-- View log detail modal-->
<div class="modal fade" id="log_modal" role="dialog" style="padding-right: 0 !important; display: none;" tabindex='-1'>
            <div class="modal-dialog modal-lg">
            
              <!-- Modal content-->
              <div class="modal-content">

                <div class="modal-header" style="background-color: rgb(60, 90, 153); color: rgb(255, 255, 255);">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">{{trans('setting.View Detail')}}</h4>
                </div>


                <div class="modal-body">
              </div>

              
            </div>
    </div>



@endsection

<!-- Include Required Prerequisites -->
<script src="{{ asset('js/jquery.min.js') }}"   >   </script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
 
<!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

<script src="{{ asset('js/sweetalert2/sweetalert2.all.min.js') }}"></script>
<link   rel="stylesheet" href="{{ asset('css/sweetalert2/sweetalert2.min.css') }}" />

<!-- Search button press enter to search -->
<script type="text/javascript">
  $(function(){
      var input = document.getElementById("search");

      input.addEventListener("keyup", function(event) {
        event.preventDefault();
        
            if (event.keyCode === 13) {
                document.getElementById("submit").click();
            }

      });

  });

</script>

<!-- Search -->
<script type="text/javascript">
  $(function(){

      $('body').on('focus','.btn_date',function(){
        $('.btnsort').prop('checked', false);
        $('input[name="daterange"]').daterangepicker({
          format: 'YYYY-MM-DD'
          });
      });

      $('body').on('click','#submit',function(){
        var search  = $('#search').val();
        var date    = $('.btn_date').val();
        var loading = "{{ asset('vendor/crudbooster/assets/lightbox/dist/images/loading.gif') }}";

        jQuery.ajax({
            url     : "{{ route('get_search_log_activity') }}",
            type    : "POST",
            data    : { search:search, date:date, _token:"{{Session::token()}}"},
            before  : function(data){
            },
            success : function(data){
              $('.dir_table').html(data);
              $('.page').css('display','none');
              swal({
                title   : "Good Job!",
                text    : "Your search ( " + search + " ) is success!",
                timer   : 1000,
                type    : 'success',
              });

            },
            error   : function(data){
              swal("Warning", "There must be a mistake, Please Check again!", "warning");
            }

        });

      });
      
  });

</script>
  






<!-- View Detail -->
<script type="text/javascript">
$(function(){
  $('body').on('click','.view_button',function(){
    var log_id = $(this).attr('data-id');

    jQuery.ajax({
        url     : "{{ route('view_log_detail') }}",
        type    : "POST",
        data    : { log_id: log_id, _token:"{{Session::token()}}" },
        success : function(data){
          $('#log_modal .modal-body').html(data);
          $('#log_modal').modal('show');

        },
        error   : function(data){
          swal("Warning", "There must be a mistake, Please Check again!", "warning");
        }
    });

    
  });


});
</script>





<style>
.hidden{
  display: hidden;
}
</style>


