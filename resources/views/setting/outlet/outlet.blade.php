@extends('crudbooster::admin_template')
@section('content')
<?php session_start(); ?>
<body id="store_setting_update"> 

	<div class="container-fluid nav-tabs">
		<div class="row">
			<a href="#company_info" data-toggle="tab">
				<div class="col-md-2 col-xs-5 tabs" class="active">
					<i class="fa fa-info-circle"></i> <br/>
					{{ trans('setting.general_information') }}
				</div>
			</a>

			<a href="#tax" data-toggle="tab">
				<div class="col-md-2 col-xs-3 tabs" class="active">
					<i class="fa fa-percent"></i> <br/>
					{{ trans('setting.tax') }}
				</div>
			</a>

			<a href="#currency" data-toggle="tab">
				<div class="col-md-2 col-xs-3 tabs" class="active">
					<i class="fa fa-money"></i> <br/>
					{{ trans('setting.currency') }}
				</div>
			</a>
		</div>

		<div class="row setting_info">
			<div class="tab-content">
			<div class="tab-pane active box_info" id="company_info">
				
				<form class="form-horizontal" method='post' id="form" enctype="multipart/form-data" action="{{route('update.company_information')}}">

					<input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type='hidden' name='return_url' value='{{ @$return_url }}'/>
                    <input type='hidden' name='ref_mainpath' value='{{ CRUDBooster::mainpath() }}'/>
                    <input type='hidden' name='ref_parameter' value='{{urldecode(http_build_query(@$_GET))}}'/>
                    @if($hide_form)
                        <input type="hidden" name="hide_form" value='{!! serialize($hide_form) !!}'>
                    @endif

                    <input type="hidden" class="form-control" name="company_id"  value="{{ $settings->id }}">


					<div class="row">
                    <div class="col-md-12">
                    <div class="box-body">

                        <input type="hidden" name="outlet_session" value="company_info" />
                        
                        <div class="form-group header-group-0" id="form-group-company_name">
                            <label class="control-label col-sm-2">{{ trans('setting.company_name') }}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-md-8">
                               <input type="text" name="company_name" class="form-control company_name" placeholder="Company Name" id="company_name" value="{{$settings->company_name}}">
                            </div>
                        </div>

                        <div class="form-group header-group-0" id="form-group-company_address">
                            <label class="control-label col-sm-2">{{ trans('setting.company_address') }}
                            <span class="text-danger" title="{{trans('setting.This field is required')}}">*</span></label>
                            <div class="col-md-8">
                               <input type="text" name="company_address" class="form-control company_address" placeholder="Company Address" id="company_address" value="{{$settings->company_address}}">
                            </div>
                        </div>


                        <div class="form-group header-group-0" id="form-group-company_country">
                            <label class="control-label col-sm-2">{{ trans('setting.company_country') }}
                            <span class="text-danger" title="{{trans('setting.This field is required')}}">*</span></label>
                            <div class="col-md-8">
                               <select name="company_country" class="form-control company_country" required="" id='company_country'>
                                	
                                    @foreach($countries as $country)
                                    <option value="{{$country->id}}" @if($settings->company_country == $country->id) selected="selected" @endif>{{$country->country_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="form-group header-group-0" id="form-group-company_logo">
                            <label class="control-label col-sm-2">{{ trans('setting.company_logo') }}
                            <span class="text-danger" title="{{trans('setting.This field is required')}}">*</span></label>
                            <div class="col-sm-6">
                               @if(empty($settings->company_logo))
                               <input type="file" class="form-control" name="company_logo" id="company_logo" accept="image/*" required> 
                                <div style="color:#908e8e; margin-top: 5px;"> {{ trans('setting.file_image_require') }}</div>
                               @else
                                <p>
                                    <a data-lightbox="roadtrip" href="{{$url}}/{{$settings->company_logo}}"><img style="max-width:160px" title="{{$settings->company_logo}}" src="{{$url}}/{{$settings->company_logo}}"></a></p>
                                
                                <input type="hidden" name="menu_photo" value="{{$settings->company_logo}}">                   
                                    <p><a class="btn btn-danger btn-delete btn-sm" onclick="if(!confirm('Are you sure ?')) return false" href="{{$url}}/admin/setting/delete-image?image={{$settings->company_logo}}&amp;id={{$settings->id}}&amp;column=company_logo"><i class="fa fa-ban"></i> {{ trans('setting.text_delete') }} </a></p>
                                    <p class="text-muted"><em>{{ trans('setting.remove_image_first') }}</em></p>
                                 <div class="text-danger"></div>


                               @endif
                            </div>

                        </div>

                        <div class="form-group header-group-0" id="form-group-company_info">
                            <label class="control-label col-sm-2">{{ trans('setting.company_info') }}</label>
                            <div class="col-md-8">
                               <textarea name="company_info" class="form-control company_info" placeholder="Company Information" id="company_info">{{$settings->company_info}}</textarea>
                            </div>
                        </div>

                        <div class="form-group header-group-0" id="form-group-company_email">
                            <label class="control-label col-sm-2">{{ trans('setting.company_email') }}
                            <span class="text-danger" title="{{trans('setting.This field is required')}}">*</span></label>
                            <div class="input-group col-md-8">
					            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
					            <input type="email" title="{{ trans('setting.company_email') }}" required="" maxlength="255" class="form-control company_email" name="company_email" id="company_email" value="{{$settings->company_email}}">
					        </div>
                        </div>

                        <div class="form-group header-group-0" id="form-group-company_phone">
                            <label class="control-label col-sm-2">{{ trans('setting.company_phone') }}
                            <span class="text-danger" title="{{trans('setting.This field is required')}}">*</span></label>
                            <div class="col-md-8">
                               <input type="number" name="company_phone" class="form-control company_phone" placeholder="Company Phone" id="company_phone" value="{{$settings->company_phone}}">
                            </div>
                        </div>

                        <div class="form-group header-group-0" id="form-group-company_contact">
                            <label class="control-label col-sm-2">{{ trans('setting.company_contact') }}</label>
                            <div class="col-md-8">
                               <input type="text" name="company_contact" class="form-control company_contact" placeholder="Company Contact" id="company_contact" value="{{$settings->company_contact}}">
                            </div>
                        </div>


                        

                        <div class="form-group header-group-0" id="form-group-company_website">
                            <label class="control-label col-sm-2">{{ trans('setting.company_website') }}
                            <span class="text-danger" title="{{trans('setting.This field is required')}}">*</span></label>
                            <div class="col-md-8">
                               <input type="text" name="company_website" class="form-control company_website" placeholder="Company Website" id="company_website" value="{{$settings->company_website}}">
                            </div>
                        </div>

                        <div class="form-group header-group-0" id="form-group-company_language">
                            <label class="control-label col-sm-2">{{ trans('setting.company_language') }}
                            <span class="text-danger" title="{{trans('setting.This field is required')}}">*</span></label>
                            <div class="col-md-8">
                               <select name="company_language" class="form-control company_language" required="" id='company_language'>
                                	
                                    @foreach($languages as $language)
                                    <option value="{{$language->language_code}}" @if($settings->company_lang == $language->language_code) selected="selected" @endif>({{$language->language_code}}) {{$language->language_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                       

                       


                        <div class="box-footer">

	                        <div class="form-group">
	                            <label class="control-label col-sm-2"></label>
	                            <div class="col-sm-10">
	                                @if(CRUDBooster::isCreate() || CRUDBooster::isUpdate())

	                                    @if(CRUDBooster::isCreate() && $button_addmore==TRUE && $command == 'add')
	                                        <input type="submit" name="submit" value='{{trans("setting.button_save_more")}}' class='btn btn-success'>
	                                    @endif

	                                    @if($button_save && $command != 'detail')
	                                        <input type="submit" name="submit" value='{{trans("setting.button_save")}}' class='btn btn-success'>
	                                    @endif

	                                @endif
	                            </div>
	                        </div>


                    	</div><!-- /.box-footer !-->



                    </div>
                	</div>
                	</div>

				</form>
			</div> <!-- GENERAL INFORMATION -->

			<div class="tab-pane box_info" id="tax">
			 	<form class="form-horizontal" method='post' id="form" enctype="multipart/form-data" action="{{route('update.company_tax')}}">

					<input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type='hidden' name='return_url' value='{{ @$return_url }}'/>
                    <input type='hidden' name='ref_mainpath' value='{{ CRUDBooster::mainpath() }}'/>
                    <input type='hidden' name='ref_parameter' value='{{urldecode(http_build_query(@$_GET))}}'/>
                    @if($hide_form)
                        <input type="hidden" name="hide_form" value='{!! serialize($hide_form) !!}'>
                    @endif

                    <input type="hidden" class="form-control" name="company_id"  value="{{ $settings->id }}">


					<div class="row">
                    <div class="col-md-12">
                    <div class="box-body">

                        <input type="hidden" name="outlet_session" value="tax" />

                        <div class="form-group header-group-0" id="form-group-company_have_tax">
                            <label class="control-label col-sm-2">{{trans('setting.Include Tax')}}
                            <span class="text-danger" title="{{trans('setting.This field is required')}}">*</span></label>
                            <div class="col-md-8 have_tax_container">
                              <input type="checkbox" id="have_tax" name="have_tax" class="have_tax"
                               @if($settings->include_tax == 1) checked="checked" @endif
                              />

                            </div>
                        </div>
                        

                    	<div class="form-group header-group-0" id="form-group-company_sale_tax" style="display: none;">
                            <label class="control-label col-sm-2">{{ trans('setting.company_sale_tax') }}
                            <span class="text-danger" title="{{trans('setting.This field is required')}}">*</span></label>
                            <div class="col-md-8">
                               <input type="number" name="company_sale_tax" class="form-control company_sale_tax" placeholder="Company Sale Tax" id="company_sale_tax" value="{{$settings->company_sale_tax}}">
                            </div>
                        </div>
                        
                        <div class="form-group header-group-0" id="form-group-tax_type" style="display: none;">
                            <label class="control-label col-sm-2">{{ trans('setting.tax type') }}
                            <span class="text-danger" title="{{trans('setting.This field is required')}}">*</span></label>
                            <div class="col-md-8">
                            <select class="form-control include_tax_type" name="Tax_type">
                                <option value="1" @if($settings->company_tax_type_id == 1) selected @endif>Tax Invoice</option>
                                <option value="2" @if($settings->company_tax_type_id == 2) selected @endif>Commercial Invoice</option>
                           </select>
                            </div>
                        </div>
                                   


                        <div class="box-footer" >

	                        <div class="form-group">
	                            <label class="control-label col-sm-2"></label>
	                            <div class="col-sm-10">
	                                @if(CRUDBooster::isCreate() || CRUDBooster::isUpdate())

	                                    @if(CRUDBooster::isCreate() && $button_addmore==TRUE && $command == 'add')
	                                        <input type="submit" name="submit" value='{{trans("setting.button_save_more")}}' class='btn btn-success'>
	                                    @endif

	                                    @if($button_save && $command != 'detail')
	                                        <input type="submit" name="submit" value='{{trans("setting.button_save")}}' class='btn btn-success save'>
	                                    @endif

	                                @endif
	                            </div>
	                        </div>


                    	</div><!-- /.box-footer !-->



                    </div>
                	</div>
                	</div>

				</form>
			 </div> <!-- TAX -->

			 
             <div class="tab-pane box_info" id="currency">
        		
                <form class="form-horizontal" method='post' id="form" enctype="multipart/form-data" action="{{route('update.company_currency')}}">

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type='hidden' name='return_url' value='{{ @$return_url }}'/>
                    <input type='hidden' name='ref_mainpath' value='{{ CRUDBooster::mainpath() }}'/>
                    <input type='hidden' name='ref_parameter' value='{{urldecode(http_build_query(@$_GET))}}'/>
                    @if($hide_form)
                        <input type="hidden" name="hide_form" value='{!! serialize($hide_form) !!}'>
                    @endif

                    <input type="hidden" class="form-control" name="company_id"  value="{{ $settings->id }}">


                    <div class="row">
                    <div class="col-md-12">
                    <div class="box-body">
                        <input type="hidden" name="outlet_session" value="currency" />

                        <div class="form-group header-group-0" id="form-group-company_currency">
                            <p class="col-md-12">{!! trans('setting.company_currency_information') !!} <br/><br/> {!! trans('setting.company_currency_information_01') !!}<a href="{{URL::to('/')}}/admin/exchange">{!! trans('setting.Exchange Rate') !!}</a></p>
                        </div>
                        <div class="form-group header-group-0" id="form-group-company_currency">
                            <label class="control-label col-sm-2">{{ trans('setting.company_currency') }}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-md-8">
                               <select name="company_currency" class="form-control company_currency" required="" id='company_currency'>
                                    @foreach($currencies as $currency)
                                    <option value="{{$currency->id}}" @if($settings->company_currency == $currency->id) selected="selected" @endif >{{$currency->name}}</option>

                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="box-footer" >

                                    <div class="form-group">
                                        <label class="control-label col-sm-2"></label>
                                        <div class="col-sm-10">
                                            @if(CRUDBooster::isCreate() || CRUDBooster::isUpdate())

                                                @if(CRUDBooster::isCreate() && $button_addmore==TRUE && $command == 'add')
                                                    <input type="submit" name="submit" value='{{trans("setting.button_save_more")}}' class='btn btn-success'>
                                                @endif

                                                @if($button_save && $command != 'detail')
                                                    <input type="submit" name="submit" value='{{trans("setting.button_save")}}' class='btn btn-success save'>
                                                @endif

                                            @endif
                                        </div>
                                    </div>


                        </div><!-- /.box-footer !-->


                    </div>
                    </div>
                    </div>
                    </form>


			 </div>
			 <!-- CURRENCY -->



			</div><!-- tab-content -->
		</div><!-- setting_info -->


	</div><!-- Container-fluid -->


    <!-- modal confirm delete -->
    <div class="modal fade" id="confirm_delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                ...
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('setting.Cancel')}}</button>
                <a class="btn btn-danger btn-ok">{{trans('setting.Delete')}}</a>
            </div>
        </div>
    </div>
    </div>

	

</body>
@endsection

<script src="{{ asset('js/jquery.min.js') }}" type="text/javascript"></script>


<!-- Select2 for company language and company currency -->
<script type="text/javascript">
    $(document).ready(function() {
    $('#company_language').select2();
    // $('#company_currency').select2();
});
</script>



<script type="text/javascript">
  $(function(){
        var include_tax_on_load = $('#have_tax');
        
        if(include_tax_on_load.prop('checked')){
            $('#form-group-company_sale_tax').css('display','block');
            $('#form-group-tax_type').css('display','block');
        }else{
            $('#form-group-company_sale_tax').css('display','none');
            $('#form-group-tax_type').css('display','none');
        }



    $('body').on('change','#have_tax',function(){
        var include_tax = $('#have_tax');
        
        if(include_tax.prop('checked')){
            $('#form-group-company_sale_tax').css('display','block');
            $('#form-group-tax_type').css('display','block');
        }else{
            $('#form-group-company_sale_tax').css('display','none');
            $('#form-group-tax_type').css('display','none');
        }

    });

  });
</script>

<script type="text/javascript">
    $(function(){
        if("{{$outlet_session}}"){
            if("{{$outlet_session}}" != null){
                 $('.nav-tabs a[href="#{{$outlet_session}}"]').tab('show').fade();
            }
        }
    });
</script>



<style type="text/css">
	.tabs{
		background: #fff;
		padding: 20px;
		box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
		margin: 0 10px 0 0;
		text-align: center;
		font-size: 16px;
	}

    a:last-child .tabs{
        margin-right: 0;
    }

	.box_info{
		background: #fff;
		padding: 20px 20px 0 20px;
		box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);

	}

	.setting_info{
		margin-top: 20px;
	}

	.input-group[class*=col-]{
		padding-left: 15px !important;
		padding-right: 15px !important;
	}

    .have_tax_container {
        padding: 7px 15px;
    }

    .taxType{
        text-align: right;
    }
</style>




