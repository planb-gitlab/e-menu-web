@extends("layouts.create_update")
@section("content")
<?php
	$action = (@$row) ? CRUDBooster::mainpath("edit-save/$row->id") : CRUDBooster::mainpath("add-save");
	$return_url = ($return_url) ?: g('return_url');
?>
<body id="store_setting_update"> 

	<div class="container-fluid ">
			<form class="form-horizontal" method='post' id="form" enctype="multipart/form-data" action="{{ $action }}">

					<input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type='hidden' name='return_url' value='{{ @$return_url }}'/>
                    <input type='hidden' name='ref_mainpath' value='{{ CRUDBooster::mainpath() }}'/>
                    <input type='hidden' name='ref_parameter' value='{{urldecode(http_build_query(@$_GET))}}'/>
                    @if($hide_form)
                        <input type="hidden" name="hide_form" value='{!! serialize($hide_form) !!}'>
                    @endif

                    <!-- ==================================== General Infomation =========================================== -->
					<div class="row general_info">
                    <div class="col-md-12">
                    <div class="box-body">

                    	<div class="form-group header-group-0" id="form-group-company_outlet_user">
                            <label class="control-label col-sm-2">{{ trans('setting.Outlet User') }}
                            <span class="text-danger" title="{{trans('setting.This field is required')}}">*</span></label>
                            <div class="col-md-8">
                               <select name="company_outlet_user" class="form-control company_outlet_user" required id='company_outlet_user'>
                               		<option value="0">**No outlet user selected</option>
                                    @foreach($users as $user)
                                    <option value="{{$user->id}}" @if($settings->created_by == $user->id) selected="selected" @endif>({{$user->name}})</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                    	<div class="form-group header-group-0" id="form-group-company_pin">
                            <label class="control-label col-sm-2">{{ trans('crudbooster.company_pin') }}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-md-8">
                                
                               <input type="text" name="company_pin" class="form-control company_pin" id="company_pin" value="" placeholder="Please input your company pin (Note* : 4 numbers digit only)" autocomplete="off" maxlength="4" @if($command =='add') required @endif>
                               
                               <div style="color:#908e8e; margin-top: 5px;"> {{ trans('crudbooster.pin_info') }}</div>
                            </div>
                            <div class="col-md-2">
                            	<button type="button" class="btn generate_pin">Generate Pin</button>
                            </div>
                        </div>
                        
                        <div class="form-group header-group-0" id="form-group-company_name">
                            <label class="control-label col-sm-2">{{ trans('setting.company_name') }}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-md-8">
                               <input type="text" name="company_name" class="form-control company_name" placeholder="Company Name" id="company_name" value="{{$settings->company_name}}" required>
                            </div>
                        </div>

                        <div class="form-group header-group-0" id="form-group-company_address">
                            <label class="control-label col-sm-2">{{ trans('setting.company_address') }}
                            <span class="text-danger" title="{{trans('setting.This field is required')}}">*</span></label>
                            <div class="col-md-8">
                               <input type="text" name="company_address" class="form-control company_address" placeholder="Company Address" id="company_address" value="{{$settings->company_address}}" required>
                            </div>
                        </div>


                        <div class="form-group header-group-0" id="form-group-company_country">
                            <label class="control-label col-sm-2">{{ trans('setting.company_country') }}
                            <span class="text-danger" title="{{trans('setting.This field is required')}}">*</span></label>
                            <div class="col-md-8">
                               <select name="company_country" class="form-control company_country" required id='company_country'>
                                    @foreach($countries as $country)
                                    <option value="{{$country->id}}" @if($settings->company_country == $country->id) selected="selected" @endif>{{$country->country_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="form-group header-group-0" id="form-group-company_logo">
                            <label class="control-label col-sm-2">{{ trans('setting.company_logo') }}
                            <span class="text-danger" title="{{trans('setting.This field is required')}}">*</span></label>
                            <div class="col-sm-6">
                               @if(empty($settings->company_logo))
                               <input type="file" class="form-control" name="company_logo" id="company_logo" accept="image/*" required> 
                                <div style="color:#908e8e; margin-top: 5px;"> {{ trans('setting.file_image_require') }}</div>
                               @else
                                <p>
                                    <a data-lightbox="roadtrip" href="{{$url}}/{{$settings->company_logo}}"><img style="max-width:160px" title="{{$settings->company_logo}}" src="{{$url}}/{{$settings->company_logo}}"></a></p>
                                
                                <input type="hidden" name="company_logo" value="{{$settings->company_logo}}">                   
                                    <p><a class="btn btn-danger btn-delete btn-sm" onclick="if(!confirm('Are you sure ?')) return false" href="{{$url}}/admin/setting/delete-image?image={{$settings->company_logo}}&amp;id={{$settings->id}}&amp;column=company_logo"><i class="fa fa-ban"></i> {{ trans('setting.text_delete') }} </a></p>
                                    <p class="text-muted"><em>{{ trans('setting.remove_image_first') }}</em></p>
                                 <div class="text-danger"></div>


                               @endif
                            </div>

                        </div>

                        <div class="form-group header-group-0" id="form-group-company_info">
                            <label class="control-label col-sm-2">{{ trans('setting.company_info') }}</label>
                            <div class="col-md-8">
                               <textarea name="company_info" class="form-control company_info" placeholder="Company Information" id="company_info">{{$settings->company_info}}</textarea>
                            </div>
                        </div>

                        <div class="form-group header-group-0" id="form-group-company_email">
                            <label class="control-label col-sm-2">{{ trans('setting.company_email') }}
                            <span class="text-danger" title="{{trans('setting.This field is required')}}">*</span></label>
                            <div class="input-group col-md-8">
					            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
					            <input type="email" title="{{ trans('setting.company_email') }}" required maxlength="255" class="form-control company_email" name="company_email" placeholder="Company Email" id="company_email" value="{{$settings->company_email}}">
					        </div>
                        </div>

                        <div class="form-group header-group-0" id="form-group-company_phone">
                            <label class="control-label col-sm-2">{{ trans('setting.company_phone') }}
                            <span class="text-danger" title="{{trans('setting.This field is required')}}">*</span></label>
                            <div class="col-md-8">
                               <input type="tel" name="company_phone" class="form-control company_phone" placeholder="Company Phone" id="company_phone" value="{{$settings->company_phone}}" required>
                            </div>
                        </div>

                        <div class="form-group header-group-0" id="form-group-company_contact">
                            <label class="control-label col-sm-2">{{ trans('setting.company_contact') }}</label>
                            <div class="col-md-8">
                               <input type="text" name="company_contact" class="form-control company_contact" placeholder="Company Contact" id="company_contact" value="{{$settings->company_contact}}">
                            </div>
                        </div>


                        

                        <div class="form-group header-group-0" id="form-group-company_website">
                            <label class="control-label col-sm-2">{{ trans('setting.company_website') }}</label>
                            <div class="col-md-8">
                               <input type="text" name="company_website" class="form-control company_website" placeholder="Company Website" id="company_website" value="{{$settings->company_website}}">
                            </div>
                        </div>

                        <div class="form-group header-group-0" id="form-group-company_language">
                            <label class="control-label col-sm-2">{{ trans('setting.company_language') }}
                            <span class="text-danger" title="{{trans('setting.This field is required')}}">*</span></label>
                            <div class="col-md-8">
                               <select name="company_language" class="form-control company_language" required id='company_language'>
                                    @foreach($languages as $language)
                                    <option value="{{$language->language_code}}" @if($settings->company_lang == $language->language_code) selected="selected" @endif>({{$language->language_code}}) {{$language->language_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                       
                    </div>
                	</div>
                	</div>

                	<!-- ==================================== Tax Infomation =========================================== -->

                	<div class="row tax_info">
                    <div class="col-md-12">
                    <div class="box-body">

                    	<div class="form-group header-group-0" id="form-group-company_have_tax">
                            <label class="control-label col-sm-2" style="padding-top: 0px;">{{trans('setting.Include Tax')}}</label>
                            <div class="col-md-8 have_tax_container">
                              <input type="checkbox" id="have_tax" name="include_tax" class="have_tax"
                               @if($settings->include_tax == 1) checked="checked" @endif
                              />

                            </div>
                        </div>
                        

                    	<div class="form-group header-group-0" id="form-group-company_sale_tax" style="display: none;">
                            <label class="control-label col-sm-2">{{ trans('setting.company_sale_tax') }}
                            <span class="text-danger" title="{{trans('setting.This field is required')}}">*</span></label>
                            <div class="col-md-8">
                               <input type="number" name="company_sale_tax" class="form-control company_sale_tax" placeholder="Company Sale Tax" id="company_sale_tax" value="{{$settings->company_sale_tax}}">
                            </div>
                        </div>
                        
                        <div class="form-group header-group-0" id="form-group-tax_type" style="display: none;">
                            <label class="control-label col-sm-2">{{ trans('setting.tax type') }}
                            <span class="text-danger" title="{{trans('setting.This field is required')}}">*</span></label>
                            <div class="col-md-8">
                            <select class="form-control include_tax_type" name="company_tax_type_id">
                            	<option value="">** Please select tax type</option>
                                <option value="1" @if($settings->company_tax_type_id == 1) selected @endif>Tax Invoice</option>
                                <option value="2" @if($settings->company_tax_type_id == 2) selected @endif>Commercial Invoice</option>
                           </select>
                            </div>
                        </div>

                       
                    </div>
                	</div>
                	</div>


                	<!-- ==================================== Currency Infomation =========================================== -->

                	<div class="row currency_info">
                    <div class="col-md-12">
                    <div class="box-body">

                    	<div class="form-group header-group-0" id="form-group-company_currency">
                            <label class="control-label col-sm-2">{{ trans('setting.company_currency') }}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-md-8">
                               	<select name="company_currency" class="form-control company_currency" required="" id='company_currency'>
                                    @foreach($currencies as $currency)
                                    <option value="{{$currency->id}}" @if($settings->company_currency == $currency->id) selected="selected" @endif >{{$currency->name}}</option>

                                    @endforeach
                                </select>
                            </div>
                        </div>

                       
                    </div>
                	</div>
                	</div>





                	<div class="box-footer" style="background: #F5F5F5">

                                    <div class="form-group">
                                        <label class="control-label col-sm-2"></label>
                                        <div class="col-sm-10">
                                            @if($button_cancel && CRUDBooster::getCurrentMethod() != 'getDetail')
                                                @if(g('return_url'))
                                                    <a href='{{g("return_url")}}' class='btn btn-default'><i
                                                                class='fa fa-chevron-circle-left'></i> {{trans("crudbooster.button_back")}}</a>
                                                @else
                                                    <a href='{{CRUDBooster::mainpath("?".http_build_query(@$_GET)) }}' class='btn btn-default'><i
                                                                class='fa fa-chevron-circle-left'></i> {{trans("crudbooster.button_back")}}</a>
                                                @endif
                                            @endif
                                            @if(CRUDBooster::isCreate() || CRUDBooster::isUpdate())

                                                @if(CRUDBooster::isCreate() && $command == 'add')
                                                    <input type="submit" name="submit" value='{{trans("crudbooster.button_save_more")}}' class='btn btn-success'>
                                                @endif

                                                @if($button_save && $command != 'detail')
                                                    <input type="submit" name="submit" value='{{trans("crudbooster.button_save")}}' class='btn btn-success save'>
                                                @endif

                                            @endif
                                        </div>
                                    </div>
                    </div>


				</form>
	</div>

</body>
@endsection

@section("css")
<style type="text/css">
	.tabs{
		background: #fff;
		padding: 20px;
		box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
		margin: 0 10px 0 0;
		text-align: center;
		font-size: 16px;
	}

    a:last-child .tabs{
        margin-right: 0;
    }

	.box_info{
		background: #fff;
		padding: 20px 20px 0 20px;
		box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);

	}

	.setting_info{
		margin-top: 20px;
	}

	.input-group[class*=col-]{
		padding-left: 15px !important;
		padding-right: 15px !important;
	}

    .taxType{
        text-align: right;
    }

    .general_info,.tax_info {
	    border-bottom: 1px dashed #ccc;
	    margin-bottom: 10px;
	}

</style>
@endsection

@section("script")
<script>
	$('#company_country').select2();
	$('#company_language').select2();
	$('#company_currency').select2();
</script>

<script>

	var include_tax_on_load = $('#have_tax');
	if(include_tax_on_load.prop('checked')){
        	$('.company_sale_tax').prop('required',true);
        	$('.include_tax_type').prop('required',true);
            $('#form-group-company_sale_tax').show();
            $('#form-group-tax_type').show();
    }else{
        	$('.company_sale_tax').prop('required',false);
        	$('.include_tax_type').prop('required',false);
            $('#form-group-company_sale_tax').hide();
            $('#form-group-tax_type').hide();
    }


    $('body').on('change','#have_tax',function(){
        var include_tax = $('#have_tax');
        if(include_tax.prop('checked')){
        	$('.company_sale_tax').prop('required',true);
        	$('.include_tax_type').prop('required',true);
            $('#form-group-company_sale_tax').show();
            $('#form-group-tax_type').show();
        }else{
        	$('.company_sale_tax').prop('required',false);
        	$('.include_tax_type').prop('required',false);
            $('#form-group-company_sale_tax').hide();
            $('#form-group-tax_type').hide();
        }

    });
</script>

<script type="text/javascript">
    $('body').on('click','.generate_pin',function(){
	    var pin = Math.floor(1000 + Math.random() * 9000);
	    $('#company_pin').val(pin);
	    var company_pin = $('#company_pin').val();

        jQuery.ajax({
            url     : "{{ route('pin_validation') }}",
            method  : "POST",
            data    : {company_pin : company_pin},
            success : function(data){
              if(data == 1){
                 	$('#company_pin').css({"background":"#40a669","color":"#fff","border-color":"#3d9d64"}); 
              }else{
                 	$('#company_pin').css({"background":"#a64040","color":"#fff","border-color":"#9d3d3d"});
                  	swal("Pin in used", "This pin already in use, please generate another pin", "warning");
              }
            },
            error : function(data){
            	swal("Warning", "There must be a problem with the request ! Please check again", "warning");
            }
        });


    });

    $('body').on('keyup','#company_pin',function(){
        var company_pin = $(this).val();
        if(company_pin.length == 4){
          jQuery.ajax({
            url     : "{{ route('pin_validation') }}",
            method  : "POST",
            data    : {company_pin : company_pin},
            beforeSend : function(){

            },
            success : function(data){
              	if(data == 1){
                 	$('#company_pin').css({"background":"#40a669","color":"#fff","border-color":"#3d9d64"});
                  
              	}else{
                 	$('#company_pin').css({"background":"#a64040","color":"#fff","border-color":"#9d3d3d"});
                  	swal("Pin in used", "This pin already in use, please generate another pin", "warning");
              	}
            },
            error : function(data){
            	swal("Warning", "There must be a problem with the request ! Please check again", "warning");
            }
        });
        }else{
          $('#company_pin').css({"background":"#a64040","color":"#fff","border-color":"#9d3d3d"});
        } 
        

    });

</script>
@endsection