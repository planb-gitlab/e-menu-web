<style type="text/css">
	@font-face {
    font-family: Fake Receipt;
    src: url('{{ asset('fonts/fakereceipt.ttf') }}');
	}


	* {
	  box-sizing: border-box;
	}

	@page {
        margin: 5px 20px;
        size: 78mm 30cm;
    }

	body{
		font-size: 10px;
		color:#000;
		font-family: 'Fake Receipt';
	}

	.logo{
		display: block;
		text-align: center;
		margin-bottom: 10px;
	}

	.logo img{
		width: 20%;
    	height: auto;

	}

	#company_name{
		font-size: 12px;
	}

	.info{
		text-align: center;

	}

	#invoice_container {
	  width: 100%;
	  position: relative;
	  display: block;
	  margin-bottom: 20px;
	}

	.invoice_data{
		display: block;
	}

	.left_label{
		width: 40%;
		display: inline-block;
		line-height: 0;
	}

	.left_paid{
		/*width: 60%;*/
		display: inline-block;
		line-height: 0px;
	}
	.total_amount {
		font-size: 14px;
		margin-top: 10px;
		line-height: 0
	}
	.total_value {
		padding: 0 !important;
		margin: 0 !important;
		font-size: 14.5px;
		line-height: 0;

	}

	table{
		width: 100%;
		margin-bottom: 10px;
		border-collapse: collapse;

	}

	table tr.header{
		border-top:3px dashed #000;
	}

	.calculate_data{
		margin-bottom: 10px;
		display: block;
		text-align: right;
	}


</style>

<body id="print_layout">
			

			<div class="info" style="margin-top: 0;">
				<div><img src="{{asset($outlet->company_logo)}}"/></div>
				<div id="company_name">{{$outlet->company_name}} </div>
				<div>{{$outlet->company_address}}</div>
				<div>{{$outlet->company_phone}}</div>
				@if($outlet->company_contact) 
					<div> {{$outlet->company_contact}} </div> 
				@endif 
				<div style="margin:10px 0; border:1px solid #000"></div>
			</div>
			
			<div id="invoice_container">
				<div class="invoice_data">
					<span class="left_label" style="">Invoice No</span>
					<span class="invoice_number">{{ $invoice->invoice_number }}</span>
				</div>
				
				@if ($invoice->order_type == "dinein" && $invoice->table_code)
				<div class="invoice_data">
					<span class="left_label" style="">Table No</span>
					<span class="table_number">{{ $invoice->table_code }}</span>
				</div>	
				@endif

				
				<div class="invoice_data">
					<span class="left_label">Date</span>
					<span class="invoice_date">{{ $now }}</span>
				</div>

				<div class="invoice_data">
					<span class="left_label">Seller</span>
					<span class="invoice_date">{{ CRUDBooster::myName() }}</span>
				</div>
			</div>

			<table style="border-bottom: 1px dashed #000;" width="100%">
				<tr class="header" >
					<td style="border-bottom: 1px dashed #000; border-top: 1px dashed #000;">ID</td>
					<td style="border-bottom: 1px dashed #000; border-top: 1px dashed #000;">Name</td>
					<td style="border-bottom: 1px dashed #000; border-top: 1px dashed #000;">Qty</td>
					<td style="border-bottom: 1px dashed #000; border-top: 1px dashed #000;">Price</td>
					<td style="border-bottom: 1px dashed #000; border-top: 1px dashed #000;">Total</td>
				</tr>

				@foreach($invoice_detail as $key => $invoices)
				<tr>
					<td width="10">{{++$key}}.</td>
					<td width="90">
						{{ $invoices['item_name'] }} 
						@if(!empty($invoices['item_size']))
							<small style="font-size: 6px !important">( {{ getItemSize($invoices['item_size']) }} )</small>
						@else 
							<small style="font-size:  6px !important">( Normal )</small>
						@endif
					</td>
					<td width="15">{{ $invoices['item_qty'] }}</td>
					<td>{{ $outlet->currency_symbol }}{{ $invoices['item_amount'] }}</td>
					<td>{{ $outlet->currency_symbol }}{{ number_format($invoices['item_qty'] * $invoices['item_amount'], 2) }}</td>
				</tr>
					@foreach($invoices['addons'] as $addon)
					<tr>
						<td></td>
						<td>> {{ $addon['addon_name'] }}</td>
						<td>{{$addon['addon_qty'] }}</td>
						<td>{{ $outlet->currency_symbol }}{{ $addon['addon_amount'] }}</td>
						<td>{{ $outlet->currency_symbol }}{{ number_format($addon['addon_qty'] * $addon['addon_amount'], 2) }}</td>
					</tr>
					@endforeach
				@endforeach
			</table>

			<div class="calculate_data">

				@if($outlet->include_tax == 1)
				<div class="paid_info">
					<span class="left_paid">Sub Total</span>
					<span class="">{{ $outlet->currency_symbol }}{{ number_format($invoice->grand_total, 2) }}</span>
				</div>
				@if(!empty($invoice->discount))
					<div class="paid_info">
						<span class="left_paid">Discount</span>
						<span class="">
							{{ ($invoice->discount_value == "%") ? $invoice->discount_value . '%' : '' }}
							{{ $outlet->currency_symbol }}{{ number_format($invoice->discount, 2) }}</span>
					</div>
				@endif
				<div class="paid_info">
					<span class="left_paid">Tax</span>
					<span class="">({{ $invoice->tax }}%) ${{ number_format($invoice->tax_amount, 2) }}</span>
				</div>
				@endif
				
				<div>
					<div class="paid_info">
						<span class="left_paid total_amount">Total($)</span>
						<span class="total_value">
							@if(!empty($invoice->discount))
								<?php $totalAmount = ($invoice->grand_total - $invoice->discount) + $invoice->tax_amount; ?>
							@else
								<?php $totalAmount = ($invoice->grand_total + $invoice->tax_amount); ?>
							@endif
							{{ $outlet->currency_symbol }}{{ number_format($totalAmount, 2) }}
					</div>

					@if($invoice->grand_riel_exchange != 0.00)
					<div class="paid_info">
						<span class="left_paid total_amount">Total(R)</span>
						<span class="total_value">
							R{{ number_format($invoice->grand_riel_exchange, 0) }}</span>
					</div>
					@endif
					
					@if($invoice->grand_baht_exchange != 0.00)
					<div class="paid_info">
						<span class="left_paid total_amount">Total(B)</span>
						<span class="total_value">
							B{{ number_format($invoice->grand_baht_exchange, 2) }}</span>
					</div>
					@endif
				</div>

				<div style="border-bottom: 1px dashed #000; margin:10px 0; padding: 10px 0"></div>

				@if($invoice->paid_total != 0.00)
					
					@if($invoice->paid_dollar != 0.00)
					<div class="paid_info">
						<span class="left_paid">Paid Amount($) :</span>
						<span class="">${{ number_format($invoice->paid_dollar, 2) }}</span>
					</div>
					@endif
					
					@if($invoice->paid_riel != 0.00)
					<div class="paid_info">
						<span class="left_paid">Paid Amount(R) :</span>
						<span class="">R{{ number_format($invoice->paid_riel, 2) }}</span>
					</div>
					@endif
					
					@if($invoice->paid_baht != 0.00)
					<div class="paid_info">
						<span class="left_paid">Paid Amount(B) :</span>
						<span class="">B{{ number_format($invoice->paid_riel, 2) }}</span>
					</div>
					@endif


					@if($invoice->return_dollar != 0.00)
					<div class="paid_info">
						<span class="left_paid">Return Amount($) :</span>
						<span class="">${{ number_format($invoice->return_dollar, 2) }}</span>
					</div>
					@endif
					
					@if($invoice->return_riel != 0.00)
					<div class="paid_info">
						<span class="left_paid">Return Amount(R) :</span>
						<span class="">R{{ number_format($invoice->return_riel, 2) }}</span>
					</div>
					@endif
					
					@if($invoice->return_baht != 0.00)
					<div class="paid_info">
						<span class="left_paid">Return Amount(B) :</span>
						<span class="">B{{ number_format($invoice->return_baht, 2) }}</span>
					</div>
					@endif
				@endif
			</div>

			<div class="Last_sentence" style="text-align: center; display: block;">
				<span>Thank you ! Please have a great day.</span>
			</div>


</body>


