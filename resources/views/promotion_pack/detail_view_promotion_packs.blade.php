@extends('crudbooster::admin_template')
@section('content')

    <link rel="stylesheet" href="{{ asset('css/promotion_pack.css') }}">
    <script src="{{ asset('js/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
    <!-- go back to previous page -->
    @if(CRUDBooster::getCurrentMethod() != 'getProfile' && $button_cancel)
            @if(g('return_url'))
                <p><a title='Return' href='{{g("return_url")}}'><i class='fa fa-chevron-circle-left '></i>
                        &nbsp; {{trans("crudbooster.form_back_to_list",['module'=>CRUDBooster::getCurrentModule()->name])}}</a></p>
            @else
                <p><a title='Main Module' href='{{CRUDBooster::mainpath()}}'><i class='fa fa-chevron-circle-left '></i>
                        &nbsp; {{trans("crudbooster.form_back_to_list",['module'=>CRUDBooster::getCurrentModule()->name])}}</a></p>
            @endif
    @endif
    <!-- End of "go back to previous page" -->

    <div class="panel panel-default">
            <!-- Page Title -->
            <div class="panel-heading">
                <strong><i class='{{CRUDBooster::getCurrentModule()->icon}}'></i> {!! $page_title or "Page Title" !!}</strong>
            </div>
            <!-- End of page Title -->

            <div class="panel-body" style="padding: 20px 0 0 0">
               
                <div class="box-body" id="parent-form-area">

                    <div class="table-responsive">
                        <table id="table-detail" class="table table-striped">
                            <tbody>
                                <tr>
                                    <td>Promotion Type</td>
                                    <td>{{ $promotion_pack->name }}</td>
                                </tr>

                                <tr>
                                    <td>Promotion name</td>
                                    <td>{{ $promotion_pack->promotion_name }}</td>
                                </tr>

                                <tr>
                                    <td>Start Date</td>
                                    <td>{{ $promotion_pack->start_date }}</td>
                                </tr>
                                @if($promotion_pack->end_date == null)
                                <tr>
                                    <td>End Date</td>
                                    <td>none</td>
                                </tr>
                                @else
                                 <tr>
                                    <td>End Date</td>
                                    <td>{{ $promotion_pack->end_date }}</td>
                                </tr>
                                @endif
                                @if($promotion_pack->promotion == "free")
                                <tr style="background: #333; color:#fff;">
                                    <th>Free Item</th>
                                    <th>Amount</th>
                                </tr>
                                @else
                                 <tr>
                                    <td>Discount Amount</td>
                                    <td>{{ $promotion_pack->discount_amount }}{{$promotion_pack->discount_type}}</td>
                                </tr>
                                @endif
                                @foreach($promotion_detail as $pro_detail)
                                <tr>
                                    <th>{{$pro_detail->name}}</th>
                                    <th>{{$pro_detail->amount}}</th>
                                </tr>
                                @endforeach


                            </tbody>
                        </table>
                    </div>                                            

                </div>

                    

            </div>


    </div>




  



    
   
   

    





@endsection

