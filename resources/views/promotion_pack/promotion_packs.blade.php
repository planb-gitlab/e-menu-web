@extends('crudbooster::admin_template')
@section('content')
<!-- go back to previous page -->
    @if(CRUDBooster::getCurrentMethod() != 'getProfile' && $button_cancel)
            @if(g('return_url'))
                <p><a title='Return' href='{{g("return_url")}}'><i class='fa fa-chevron-circle-left '></i>
                        &nbsp; {{trans("crudbooster.form_back_to_list",['module'=>CRUDBooster::getCurrentModule()->name])}}</a></p>
            @else
                <p><a title='Main Module' href='{{CRUDBooster::mainpath()}}'><i class='fa fa-chevron-circle-left '></i>
                        &nbsp; {{trans("crudbooster.form_back_to_list",['module'=>CRUDBooster::getCurrentModule()->name])}}</a></p>
            @endif
    @endif
    <!-- End of "go back to previous page" -->

    <div class="panel panel-default">
            <!-- Page Title -->
            <div class="panel-heading">
                <strong><i class='{{CRUDBooster::getCurrentModule()->icon}}'></i> {!! $page_title or "Page Title" !!}</strong>
            </div>
            <!-- End of page Title -->

            <div class="panel-body" style="padding: 20px 0 0 0">
                <?php
                $action = (@$row) ? CRUDBooster::mainpath("edit-save/$row->id") : CRUDBooster::mainpath("add-save");
                $return_url = ($return_url) ?: g('return_url');
                ?>


                    <form class='form-horizontal' method='post' id="form" enctype="multipart/form-data" action="{{ (@$promotion_pack->id)?
                        route('update_promotion_pack'):
                        route('save_promotion_pack') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type='hidden' name='return_url' value='{{ @$return_url }}'/>
                    <input type='hidden' name='ref_mainpath' value='{{ CRUDBooster::mainpath() }}'/>
                    <input type='hidden' name='ref_parameter' value='{{urldecode(http_build_query(@$_GET))}}'/>
                    @if($hide_form)
                        <input type="hidden" name="hide_form" value='{!! serialize($hide_form) !!}'>
                    @endif

                    <input type="hidden" class="form-control" name="promotion_id"  value="{{ $promotion_pack->id }}">
                   

                    
                    <div class="box-body">
                        
                        <div class="form-group header-group-0 ">
                            <label class="control-label col-sm-2">{{trans('promotionpack.Promotion Name')}}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-sm-4">
                                <input type="text" title="{{trans('promotionpack.Promotion Name')}}" required class="form-control promotion_name" name="promotion_name" value="{{ $promotion_pack->promotion_name }}" value="{{$promotion_packs->name}}" placeholder="Input promotion pack name" />
                            </div>
                        </div>


                        
                        <div class="form-group header-group-0" id="form-group-promotion_type">
                                <label class="control-label col-sm-2">{{trans('promotionpack.Promotion Type')}}
                                    <span class="text-danger" title="This field is required">*</span>
                                </label>
                                <div class="col-sm-4">
                                   <select name="promotion_type" class="form-control promotion_type" required id='promotion_type'>
                                        <option value=''>** Please select promotion type</option>
                                        @foreach($promotion_types as $promotion_type)
                                            <option value="{{$promotion_type->id}}" @if($promotion_pack->promotion_type_id == $promotion_type->id) selected="selected" @endif >{{$promotion_type->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                        </div>

                    <!-- 2.Promotion by cost -->

                        
                        <div class="form-group header-group-0" id="form-group-promotion_price" style="display: none;">
                                <label class="control-label col-sm-2">{{trans('promotionpack.Price Over')}} ($)</label>
                                <div class="col-sm-4">
                                   <input type="number" required name="promotion_price" class="form-control" disabled="disabled" placeholder="promotion_price" id="promotion_price" value="{{$promotion_pack->promotion_price}}">
                                </div>
                        </div>

                    

                    

                        <!-- 3.Promotion by menu -->
                       
                        <div class="form-group header-group-0" id="form-group-promotion_menu" style="display: none;">
                                <label class="control-label col-sm-2">{{trans('promotionpack.Menu')}}</label>
                                <div class="col-sm-4">
                                   <select name="promotion_menu" class="form-control promotion_menu" id='promotion_menu' style="margin-bottom: 5px;">
                                    <option value="0">Please select menu</option>
                                    @foreach($menu as $menus)
                                        <option value="{{ $menus->id }}" @if($promotion_pack->promotion_menu_id == $menus->id) selected="selected" @endif>{{$menus->name}}</option>
                                    @endforeach
                                    </select>
                                    <input type="number" name="promotion_qty" class="form-control" placeholder="Quantity" id="promotion_qty" value="{{$promotion_pack->promotion_qty}}">
                                </div>
                        </div>

                        <!-- 4.Promotion by event -->
                       
                        <div class="form-group header-group-0" id="form-group-promotion_event" style="display: none;">
                                <label class="control-label col-sm-2">{{trans('promotionpack.Event')}}</label>
                                <div class="col-sm-4">                                   
                                    <input type="text" name="promotion_event" required disabled="disabled" class="form-control" placeholder="Event" id="promotion_event" value="{{$promotion_pack->promotion_event}}">
                                </div>
                        </div>

                        <!-- Start Date -->
                        <div class="form-group header-group-0">
                            <label class="control-label col-sm-2">{{trans('promotionpack.Start Date')}}
                            <span class="text-danger" title="{{trans('promotionpack.This field is required')}}">*</span></label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                <span class="input-group-addon icon_calender"><i class="fa fa-calendar"></i></span>
                                <input type="text" title="{{trans('promotionpack.Start Date')}}" placeholder="Please choose start date" required class="form-control notfocus start_date datepicker" id="start_time" name="start_date" value="{{$promotion_pack->start_date}}" autocomplete="off" />
                                </div>
                            </div>

                            

                        </div>


                        <div class="form-group header-group-0 end_date_main">
                            <label class="control-label col-sm-2">{{trans('promotionpack.End Date')}}
                            <span class="text-danger" title="{{trans('promotionpack.This field is required')}}">*</span></label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                <span class="input-group-addon icon_calender"><i class="fa fa-calendar"></i></span>
                                <input type="text" title="{{trans('promotionpack.End Date')}}" required placeholder="Please choose end date" class="form-control notfocus end_date datepicker" name="end_date" id="end_date" value="{{$promotion_pack->end_date}}" autocomplete="off" />
                                </div>
                            </div>

                            <label class="end_date_checkbox_web">
                                <input type="checkbox"  name="end_date_checkbox" id="end_date_checkbox" value="1" @if ($promotion_pack) @if($promotion_pack->end_date == null) checked @else "" @endif @endif
                                />
                                {{ trans('promotionpack.no_end_date') }}
                            </label>
                            
                           
                            
                        </div>

                    </div>

                    <!-- Free Discount ($,%) + Free Specific Item -->
                    <div class="row free_stuff">
                        @if($command == "add")
                        <input type="hidden" name="promotion" id="promotion" value="discount">
                        @else
                        <input type="hidden" name="promotion" id="promotion" value="{{$promotion_pack->promotion}}">
                        @endif
                        <!-- discount -->
                        <div class="col-md-3 discount">
                                <div class="main active">
                                    <div class="title">{{ trans('promotionpack.discount') }}</div>                                    
                                   
                                </div>
                        </div>

                        <!-- Free Specific Item -->
                        <div class="col-md-3 free_item">
                                <div class="main">
                                    <div class="title">{{ trans('promotionpack.Free Item') }}</div>                                    
                                    
                                </div>
                        </div>                        

                        
                    </div><!-- Free Discount ($,%) + Free Specific Item END -->


                    <!-- discount block -->
                    <div class="row discount_block">
                        
                        <div class="col-md-1 percent">
                            <div class="main active">
                                <div class="title">%</div>
                            </div>
                        </div>

                        <div class="col-md-1 money">
                            <div class="main">
                                <div class="title">$</div>
                            </div>
                        </div>

                        <div class="col-md-8">
                            @if($command == "add")
                            <input type="hidden" name="discount_type" id="discount_type" value="%">
                            @else
                            <input type="hidden" name="discount_type" id="discount_type" value="{{ $promotion_pack->discount_type }}">
                            @endif
                            <input type="number" required name="discount_amount" id="discount_amount" placeholder="discount amount" class="form-control" value="{{ $promotion_pack->discount_amount }}">
                        </div>


                    </div>
                    <!-- discount block END -->




                    
                    <!-- Add promotion -->
                        <div class="menu_add" style="display: none;">

                            <div class="mainboxSet">
                                <label>{{trans('promotionpack.Free Item')}} <span class="text-danger">*</span></label>
                                <span class="set_minus" data-val="1"><i class="glyphicon glyphicon-minus"></i></span>
                                <input type="number" name="" oninput="setAddmore();" value="1" class="setmenuMore" placeholder="1" min="1">
                                <span class="set_plus" data-val="1"><i class="glyphicon glyphicon-plus"></i></span>
                                <button type="button" class="add-field btn btn-default" limit-date="0" val-num="1" onclick="addmenu_more();">{{-- <i class="glyphicon glyphicon-plus-sign"></i> --}} {{trans('promotionpack.Add')}} </button>
                            </div>
                            
                        </div>

                   
                        <div class="Add-menu5">
                            @if($promotion_detail)    
                            @foreach($promotion_detail as $pro_detail)
                            <div class="wrap_menuadd">

                                
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-4 menu">
                                          <label>{{trans('promotionpack.Free Item')}} <span class="text-danger">*</span></label>
                                          <select name="menu[]" class="form-control menu">
                                          
                                            @foreach($menu as $menus)
                                            <option value="{{$menus->id}}" @if($menus->id == $pro_detail->menus_id) selected="selected" @endif> {{$menus->name}}</option>
                                            @endforeach

                                
                                          </select>
                                        </div>
                                        <div class="col-sm-4 menu_amount">
                                            <label>{{trans('promotionpack.Amount')}} <span class="text-danger">*</span></label>

                                            <input type="number" class="form-control menu-amount" name="menu_amount[]" id="menu_amount" required="required" value="{{$pro_detail->amount}}" min="1">

                                        </div>

                                        <div class="col-sm-2 clear_padding">
                                          <span class="remove-this" data-id="" title="{{trans('promotionpack.Remove')}}"><i class="glyphicon glyphicon-remove"></i></span>
                                        </div>
                                    </div>
                                </div>
                                


                            </div>
                            @endforeach
                            @endif
                        </div>
                        
                    
                    <!-- Add Promotion END -->


                   

                    <div class="box-footer" style="background: #F5F5F5">
                        <div class="form-group">
                            <label class="control-label col-sm-2"></label>
                            <div class="col-sm-10">
                                @if($button_cancel && CRUDBooster::getCurrentMethod() != 'getDetail')
                                    @if(g('return_url'))
                                        <a href='{{g("return_url")}}' class='btn btn-default'><i
                                                    class='fa fa-chevron-circle-left'></i> {{trans("promotionpack.Back")}}</a>
                                    @else
                                        <a href='{{CRUDBooster::mainpath("?".http_build_query(@$_GET)) }}' class='btn btn-default'><i
                                                    class='fa fa-chevron-circle-left'></i> {{trans("promotionpack.Back")}}</a>
                                    @endif
                                @endif
                                @if(CRUDBooster::isCreate() || CRUDBooster::isUpdate())

                                    @if(CRUDBooster::isCreate() && $button_addmore==TRUE && $command == 'add')
                                        @if($check_edit!='check_edit')
                                            <input type="submit" name="submit" value='{{trans("promotionpack.button_save_more")}}' class='btn btn-success save_add_more save_add_more_iscreated' style="display: none;">
                                        @endif
                                    @endif

                                    @if($button_save && $command != 'detail')
                                        <input type="submit" name="submit" value='{{trans("promotionpack.Save")}}' class='btn btn-success save_add_iscreated' id="btn_save_add">
                                    @endif

                                @endif
                            </div>
                        </div>


                    </div><!-- /.box-footer-->


                </form>

            </div>


    </div>


<div class="se-pre-con-detail"></div>

@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/switch.css') }}">
<link rel="stylesheet" href="{{ asset('css/loading.css') }}">
<link rel="stylesheet" href="{{ asset('css/promotion_pack.css') }}">
@endsection


@section('script')
<!-- ===================== Loading GIF ========================== -->
<script>
    $(function() {
        $(".se-pre-con-detail").fadeOut("slow");;
    });
</script>

<!-- ===================== Plus Button ========================== -->
<script>
    function setAddmore(){
        var inputData = $('.setmenuMore').val();
        $('.add-field').attr("val-num",inputData);
    }

    var startIndex=1;
    function addmenu_more() {
        for (i = 0; i < $('.add-field').attr("val-num"); i++) {
            $('.Add-menu5').append('<div class="wrap_menuadd">'

                +'<div class="col-sm-12">'
                    +'<div class="row">'
                        +'<div class="col-sm-4 menu">'
                          +'<label>Free Item ' + startIndex + ' <span class="text-danger">*</span></label>'
                          +'<select name="menu[]">'
                          @foreach ($menu as $menus)
                              +'<option value="{{ $menus->id }}">{{ $menus->name }}</option>'
                          @endforeach
                          +'</select>'
                        +'</div>'
                        +'<div class="col-sm-4 menu_amount">'
                            +'<label>Amount <span class="text-danger">*</span></label>'
                            +'<input type="number" class="form-control menu-amount" name="menu_amount[]" required="required" value="1" min="1">'
                        +'</div>'

                        +'<div class="col-sm-2 clear_padding">'
                          +'<span class="remove-this" data-id="" title="Remove"><i class="glyphicon glyphicon-remove"></i></span>'
                        +'</div>'
                    +'</div>'
                +'</div>'
            +'</div>'
            );
          startIndex++;
          var limitdata=$('.add-field').attr("limit-date");
          var limitdatas=parseInt(limitdata) + 1;
          $('.add-field').attr("limit-date",limitdatas);
        }
    }
   
</script>

<script type="text/javascript">
    $(document).on('click','.set_minus',function(){
        var setMinus=$(this).attr('data-val');
        var setmenuMore=$('.setmenuMore').val();
        var sumMinus=parseInt(setmenuMore) - parseInt(setMinus);
        if (sumMinus == 0) {
            $('.setmenuMore').val('1');
            $('.add-field').attr('val-num','1');
        }
        if (sumMinus >= 1) {
            $('.setmenuMore').val(sumMinus);
            $('.add-field').attr('val-num',sumMinus);
        }
    })

    $(document).on('click','.set_plus',function(){
        var setMinus=$(this).attr('data-val');
        var setmenuMore=$('.setmenuMore').val();
        var sumMinus=parseInt(setmenuMore) + parseInt(setMinus);
        if (sumMinus == 0) {
            $('.setmenuMore').val(1);
            $('.add-field').attr('val-num','1');
        }
        if (sumMinus >= 1) {
            $('.setmenuMore').val(sumMinus);
            $('.add-field').attr('val-num',sumMinus);
        }
    });
    $(document).on('click','.remove-this',function(){
    var id=$(this).attr('data-id');
        if (id >= '1') {
            var conf=confirm("Do you want to remove this menu?");
            if(conf==true){
              var kk=$(this).parents('.wrap_menuadd').remove();
              jQuery.ajax({
                data:{id:id},
                url:"",
                // type:"GET",
                success:function(data){
                    window.location.reload(true);
                }
              });
            }
        }else{
            var kk=$(this).parents('.wrap_menuadd').remove();
        }
    });
</script>


<!-- =========================== Change Promotion Type ================================== -->
<script type="text/javascript">
      $(function(){
         $('body').on('change','#promotion_type',function(){
            var pro_type = $('#promotion_type').val();
            $('#form-group-promotion_menu').css('display','none');
            $('#form-group-promotion_price').css('display','none');
            $('#form-group-promotion_event').css('display','none');

            // membership
            if(pro_type == 1){
                
            }
            // cost
            else if(pro_type == 2){
                $('#form-group-promotion_price').css('display','block');
                $('#promotion_price').removeAttr('disabled');
                $('#promotion_event').attr('disabled','disabled');
            }
            // menu
            else if(pro_type == 3){
                 $('#form-group-promotion_menu').css('display','block');
            }
            //event
            else if(pro_type == 4){
                $('#form-group-promotion_event').css('display','block');
                $('#promotion_event').removeAttr('disabled');
                $('#promotion_price').attr('disabled','disabled');
            }
            else{
                $('#form-group-promotion_menu').css('display','none');
                $('#form-group-promotion_price').css('display','none');
                $('#form-group-promotion_event').css('display','none');
            }

         });
      });

</script>

<script type="text/javascript">
    $(function(){
            var pro_type = $('#promotion_type').val();
            $('#form-group-promotion_menu').css('display','none');
            $('#form-group-promotion_price').css('display','none');
            $('#form-group-promotion_event').css('display','none');

            // membership
            if(pro_type == 1){
                
            }
            // cost
            else if(pro_type == 2){
                $('#form-group-promotion_price').css('display','block');
                $('#promotion_price').removeAttr('disabled');
                $('#promotion_event').attr('disabled','disabled');
            }
            // menu
            else if(pro_type == 3){
                 $('#form-group-promotion_menu').css('display','block');
            }
            //event
            else if(pro_type == 4){
                $('#form-group-promotion_event').css('display','block');
                $('#promotion_event').removeAttr('disabled');
                $('#promotion_price').attr('disabled','disabled');
            }
            else{
                $('#form-group-promotion_menu').css('display','none');
                $('#form-group-promotion_price').css('display','none');
                $('#form-group-promotion_event').css('display','none');
            }

         });
</script>


<!-- discount active -->
<script type="text/javascript">
      $(function(){
        $('body').on('click','.free_stuff .discount',function(){
            $('.free_stuff .discount .main').addClass('active');
            $('.free_stuff .free_item .main').removeClass('active');

            $('.discount_block .percent .main').addClass('active');
            $('#discount_type').val('%')

            $('.discount_block').css('display','flex');
            $('.menu_add').css('display','none');
            $('.Add-menu5').css('display','none');

            $('#promotion').val('discount');
            $('#discount_amount').removeAttr('disabled');
        });

        $('body').on('click','.free_stuff .free_item',function(){
            $('.free_stuff .free_item .main').addClass('active');
            $('.free_stuff .discount .main').removeClass('active');

            $('.discount_block').css('display','none');

            $('#promotion').val('free');

            $('.discount_block').css('display','none');
            $('.menu_add').css('display','block');
            $('.Add-menu5').css('display','block');

            $('.percent .main').removeClass('active');
            $('#discount_type').val('')
            $('#discount_amount').val('');

            $('#discount_amount').attr('disabled','disabled');
        });

      });
  </script>


@if($command != "add")
@if($promotion_pack->promotion == "discount")
<script type="text/javascript">
      $(function(){
            $('.free_stuff .discount .main').addClass('active');
            $('.free_stuff .free_item .main').removeClass('active');

            $('.discount_block .percent .main').addClass('active');
            $('#discount_type').val('%')

            $('.discount_block').css('display','flex');
            $('.menu_add').css('display','none');
            $('.Add-menu5').css('display','none');

            $('#promotion').val('discount');
            $('#discount_amount').removeAttr('disabled');
        });

</script>
@else
<script type="text/javascript">
      $(function(){
            $('.free_stuff .free_item .main').addClass('active');
            $('.free_stuff .discount .main').removeClass('active');

            $('.discount_block').css('display','none');

            $('#promotion').val('free');

            $('.discount_block').css('display','none');
            $('.menu_add').css('display','block');
            $('.Add-menu5').css('display','block');

            $('.percent .main').removeClass('active');
            $('#discount_type').val('')
            $('#discount_amount').val('');

            $('#discount_amount').attr('disabled','disabled');
        });

</script>
@endif
@endif


  <!-- discount % or $ show and active -->
  <script type="text/javascript">
      $(function(){
        $('body').on('click','.discount_block .percent',function(){
            $('#discount_type').val('%');
            $('#discount_amount').prop("disabled", false); 

            $('.discount_block .percent .main').addClass('active');
            $('.discount_block .money .main').removeClass('active');


        });

        $('body').on('click','.discount_block .money',function(){
            $('#discount_type').val('$');            
            $('#discount_amount').prop("disabled", false); 

            $('.discount_block .money .main').addClass('active');
            $('.discount_block .percent .main').removeClass('active');            


      });

    });
      // 
      $(document).on('click','.icon_calender',function(){
        var dat=$(this).next().attr('id');
        if (dat=='start_time') {
            $('#start_time').click();
        }
        if (dat=='end_date') {
            $('#end_date').click();
        }
      });
  </script>

  <script>
      $(function(){
        if($('.discount_block').is(":visible") || $('.menu_add').is(":visible")){
            $('.free_stuff').show();
        }else{
            $('.free_stuff').hide();
        }
      });
  </script>

@endsection