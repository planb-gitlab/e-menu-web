@extends("crudbooster::admin_template")
@section("content")

<body id="branch">
	@if(CRUDBooster::getCurrentMethod() != 'getProfile' && $button_cancel)
            @if(g('return_url'))
                <p><a title='Return' href='{{g("return_url")}}'><i class='fa fa-chevron-circle-left '></i>
                        &nbsp; {{trans("crudbooster.form_back_to_list",['module'=>CRUDBooster::getCurrentModule()->name])}}</a></p>
            @else
                <p><a title='Main Module' href='{{CRUDBooster::mainpath()}}'><i class='fa fa-chevron-circle-left '></i>
                        &nbsp; {{trans("crudbooster.form_back_to_list",['module'=>CRUDBooster::getCurrentModule()->name])}}</a></p>
            @endif
    @endif

    <div class="panel panel-default branch_info">
            <!-- Page Title -->
            <div class="panel-heading">
                <strong><i class='{{CRUDBooster::getCurrentModule()->icon}}'></i> {!! $page_title_branch or "Add Branch" !!}</strong>
            </div>
            <!-- End of page Title -->

            <div class="panel-body" style="padding: 20px 0 0px 0">
                <?php
                $action = (@$row) ? CRUDBooster::mainpath("edit-save/$row->id") : CRUDBooster::mainpath("add-save");
                $return_url = ($return_url) ?: g('return_url');
                ?>


                    <form class='form-horizontal' method='post' id="form" enctype="multipart/form-data" 
                    action="{{ (@$branch->id)?
                        		route('update_branch'):
                        		route('save_branch') }}">

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type='hidden' name='return_url' value='{{ @$return_url }}'/>
                    <input type='hidden' name='ref_mainpath' value='{{ CRUDBooster::mainpath() }}'/>
                    <input type='hidden' name='ref_parameter' value='{{urldecode(http_build_query(@$_GET))}}'/>
                    @if($hide_form)
                        <input type="hidden" name="hide_form" value='{!! serialize($hide_form) !!}'>
                    @endif

                    <input type="hidden" class="form-control" name="id"  value="{{$branch->id}}">


                    <div class="row">
                    <div class="col-md-12">
                    <div class="box-body">

                    	<div class="form-group header-group-0" id="form-group-branch_name">
                            <label class="control-label col-sm-2">{{ trans('setting.branch_name') }}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-md-8">
                               <input type="text" required="" name="branch_name" class="form-control branch_name" placeholder="Branch Name" id="branch_name" value="{{$branch->company_name}}">
                            </div>
                        </div>

                        <div class="form-group header-group-0" id="form-group-branch_address">
                            <label class="control-label col-sm-2">{{ trans('setting.branch_address') }}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-md-8">
                               <input type="text" required="" name="branch_address" class="form-control branch_address" placeholder="Branch Address" id="branch_address" value="{{$branch->company_address}}">
                            </div>
                        </div>

                        <div class="form-group header-group-0" id="form-group-branch_country">
                            <label class="control-label col-sm-2">{{ trans('setting.branch_country') }}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-md-8">
                               <select name="branch_country" class="form-control branch_country" required="" id='branch_country'>
                                	
                                    @foreach($countries as $country)
                                    <option value="{{$country->id}}" @if($branch->company_country == $country->id) selected="selected" @endif>{{$country->country_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group header-group-0" id="form-group-branch_info">
                            <label class="control-label col-sm-2">{{ trans('setting.branch_info') }}</label>
                            <div class="col-md-8">
                               <textarea name="branch_info" class="form-control branch_info" placeholder="Branch Information" id="branch_info">{{$branch->company_info}}</textarea>
                            </div>
                        </div>

                        <div class="form-group header-group-0" id="form-group-branch_email">
                            <label class="control-label col-sm-2">{{ trans('setting.branch_email') }}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="input-group col-md-8">
					            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
					            <input type="email" title="Branch Email" required="" placeholder="Branch Email" maxlength="255" class="form-control branch_email" name="branch_email" id="branch_email" value="{{$branch->company_email}}">
					        </div>
                        </div>

                        <div class="form-group header-group-0" id="form-group-branch_phone">
                            <label class="control-label col-sm-2">{{ trans('setting.branch_phone') }}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-md-8">
                               <input type="number" required="" name="branch_phone" class="form-control branch_phone" placeholder="Branch Phone" id="branch_phone" value="{{$branch->company_phone}}">
                            </div>
                        </div>

                        <div class="form-group header-group-0" id="form-group-branch_contact">
                            <label class="control-label col-sm-2">{{ trans('setting.branch_contact') }}</label>
                            <div class="col-md-8">
                               <input type="text" name="branch_contact" class="form-control branch_contact" placeholder="Branch Contact" id="branch_contact" value="{{$branch->company_contact}}">
                            </div>
                        </div>

                        <div class="form-group header-group-0" id="form-group-branch_policy">
                            <label class="control-label col-sm-2">{{ trans('setting.branch_policy') }}</label>
                            <div class="col-md-8">
                               <input type="text" name="branch_policy" class="form-control branch_policy" placeholder="Branch Policy (Also Shown in invoice)" id="branch_policy" value="{{$branch->company_policy}}">
                            </div>
                        </div>

                        <div class="form-group header-group-0" id="form-group-branch_language">
                            <label class="control-label col-sm-2">{{ trans('setting.branch_language') }}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-md-8">
                               <select name="branch_language" class="form-control branch_language" required="" id='branch_language'>
                                	
                                    @foreach($languages as $language)
                                    <option value="{{$language->language_code}}" @if($branch->company_language == $language->language_code) selected="selected" @endif>({{$language->language_code}}) {{$language->language_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        @if($currencies)
                        <div class="form-group header-group-0" id="form-group-branch_currency">
                            <label class="control-label col-sm-2">{{ trans('setting.branch_currency') }}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-md-8">
                               <select name="branch_currency" class="form-control branch_currency" required="" id='branch_currency'>
                                    @foreach($currencies as $currency)
                                    <option value="{{$currency->id}}" @if($branch->company_currency == $currency->id) selected="selected" @endif> {{$currency->name}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @endif

                        <div class="form-group header-group-0" id="form-group-branch_pin">
                            <label class="control-label col-sm-2">{{ trans('setting.branch_pin') }}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-md-8">
                               	
                               <input type="branch_pin" name="branch_pin" required="" class="form-control branch_pin" id="branch_pin" value="" maxlength="4">
                               <button type="button" class="btn generate_pin branch_generate_pin">Generate Pin</button>
                               <div style="color:#908e8e; margin-top: 5px;"> {{ trans('setting.pin_info') }}</div>
                            </div>
                        </div>


                        <div class="box-footer" >

                                    <div class="form-group">
                                        <label class="control-label col-sm-2"></label>
                                        <div class="col-sm-10">
                                            @if($button_cancel && CRUDBooster::getCurrentMethod() != 'getDetail')
                                                @if(g('return_url'))
                                                    <a href='{{g("return_url")}}' class='btn btn-default'><i
                                                                class='fa fa-chevron-circle-left'></i> {{trans("setting.button_back")}}</a>
                                                @else
                                                    <a href='{{CRUDBooster::mainpath("?".http_build_query(@$_GET)) }}' class='btn btn-default'><i
                                                                class='fa fa-chevron-circle-left'></i> {{trans("setting.button_back")}}</a>
                                                @endif
                                            @endif
                                            @if(CRUDBooster::isCreate() || CRUDBooster::isUpdate())

                                                @if(CRUDBooster::isCreate() && $button_addmore==TRUE && $command == 'add')
                                                    <input type="submit" name="submit" value='{{trans("setting.button_save_more")}}' class='btn btn-success'>
                                                @endif

                                                @if($button_save && $command != 'detail')
                                                    <input type="submit" name="submit" value='{{trans("setting.button_save")}}' class='btn btn-success save'>
                                                @endif

                                            @endif
                                        </div>
                                    </div>


                        </div><!-- /.box-footer !-->


                    </div>
                	</div>
            		</div>


    				</form>
    		</div>
    </div>



    @if($branch->id)
    <div class="panel panel-default branch_admin">
            <!-- Page Title -->
            <div class="panel-heading">
                <strong><i class='fa fa-user'></i> {!! $page_title_user or "Add Admin Branch" !!}</strong>
            </div>
            <!-- End of page Title -->

            <div class="panel-body" style="padding: 20px 0 0px 0">
                <?php
                $action = (@$row) ? CRUDBooster::mainpath("edit-save/$row->id") : CRUDBooster::mainpath("add-save");
                $return_url = ($return_url) ?: g('return_url');
                ?>


                    <form class='form-horizontal' method='post' id="form_admin" enctype="multipart/form-data" 
                    action="{{ (@$branch_admin->id)?
                        		route('update_branch_admin'):
                        		route('save_branch_admin') }}">

                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type='hidden' name='return_url' value='{{ @$return_url }}'/>
                    <input type='hidden' name='ref_mainpath' value='{{ CRUDBooster::mainpath() }}'/>
                    <input type='hidden' name='ref_parameter' value='{{urldecode(http_build_query(@$_GET))}}'/>
                    @if($hide_form)
                        <input type="hidden" name="hide_form" value='{!! serialize($hide_form) !!}'>
                    @endif

                    <input type="hidden" class="form-control" name="admin_id"  value="{{$branch_admin->id}}" />

                    <div class="row">
                    <div class="col-md-12">
                    <div class="box-body">

                    	<div class="form-group header-group-0" id="form-group-admin_name">
                            <label class="control-label col-sm-2">{{ trans('setting.admin_name') }}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-md-8">
                               <input type="text" name="admin_name" class="form-control admin_name" placeholder="Enter Admin Name" id="admin_name" value="{{$branch_admin->name}}">
                            </div>
                        </div>

                        <div class="form-group header-group-0" id="form-group-admin_photo">
                            <label class="control-label col-sm-2">{{ trans('setting.admin_photo') }}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-sm-6">
                               @if(empty($branch_admin->admin_photo))
                               <input type="file" class="form-control" name="admin_photo" id="admin_photo" accept="image/*" required> 
                                <div style="color:#908e8e; margin-top: 5px;"> {{ trans('setting.file_image_require') }}</div>
                               @else
                                <p>
                                    <a data-lightbox="roadtrip" href="{{$url}}/{{$branch_admin->photo}}"><img style="max-width:160px" title="{{$branch_admin->photo}}" src="{{$url}}/{{$branch_admin->photo}}"></a></p>
                                
                                <input type="hidden" name="menu_photo" value="{{$branch_admin->photo}}">                   
                                    <p><a class="btn btn-danger btn-delete btn-sm" onclick="if(!confirm('Are you sure ?')) return false" href="{{$url}}/admin/branch/delete-image?image={{$branch_admin->photo}}&amp;id={{$branch_admin->id}}&amp;column=photo"><i class="fa fa-ban"></i> {{ trans('setting.text_delete') }} </a></p>
                                    <p class="text-muted"><em>{{ trans('setting.remove_image_first') }}</em></p>
                                 <div class="text-danger"></div>


                               @endif
                            </div>

                        </div>

                        <div class="form-group header-group-0" id="form-group-admin_email">
                            <label class="control-label col-sm-2">{{ trans('setting.admin_email') }}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="input-group col-md-8">
					            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
					            <input type="email" title="Branch Email" required="" placeholder="Enter Admin Email" maxlength="255" class="form-control admin_email" name="admin_email" id="admin_email" value="{{$branch_admin->email}}">
					        </div>
                        </div>

                        <div class="form-group header-group-0" id="form-group-admin_phone">
                            <label class="control-label col-sm-2">{{ trans('setting.admin_phone') }}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-md-8">
                               <input type="number" name="admin_phone" class="form-control admin_phone" placeholder="Enter Admin Phone" id="admin_phone" value="{{$branch_admin->phone}}">
                            </div>
                        </div>

                        <div class="form-group header-group-0" id="form-group-admin_password">
                            <label class="control-label col-sm-2">{{ trans('setting.admin_password') }}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-md-8">
                               <input type="password" name="admin_password" class="form-control admin_password" id="admin_password" value="">
                               <div style="color:#908e8e; margin-top: 5px;"> {{ trans('setting.password_info') }}</div>
                            </div>
                        </div>

                        <div class="form-group header-group-0" id="form-group-admin_pin">
                            <label class="control-label col-sm-2">{{ trans('setting.admin_pin') }}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-md-8">
                               	
                               <input type="admin_pin" name="admin_pin" class="form-control admin_pin" id="admin_pin" value="" maxlength="4">
                               <button type="button" class="btn generate_pin admin_generate_pin">{{ trans('setting.Generate Pin') }}</button>
                               <div style="color:#908e8e; margin-top: 5px;"> {{ trans('setting.pin_info') }}</div>
                            </div>
                        </div>


                        <div class="box-footer" >

                                    <div class="form-group">
                                        <label class="control-label col-sm-2"></label>
                                        <div class="col-sm-10">
                                            @if($button_cancel && CRUDBooster::getCurrentMethod() != 'getDetail')
                                                @if(g('return_url'))
                                                    <a href='{{g("return_url")}}' class='btn btn-default'><i
                                                                class='fa fa-chevron-circle-left'></i> {{trans("setting.button_back")}}</a>
                                                @else
                                                    <a href='{{CRUDBooster::mainpath("?".http_build_query(@$_GET)) }}' class='btn btn-default'><i
                                                                class='fa fa-chevron-circle-left'></i> {{trans("setting.button_back")}}</a>
                                                @endif
                                            @endif
                                            @if(CRUDBooster::isCreate() || CRUDBooster::isUpdate())

                                                @if(CRUDBooster::isCreate() && $button_addmore==TRUE && $command == 'add')
                                                    <input type="submit" name="submit" value='{{trans("setting.button_save_more")}}' class='btn btn-success'>
                                                @endif

                                                @if($button_save && $command != 'detail')
                                                    <input type="submit" name="submit" value='{{trans("setting.button_save")}}' class='btn btn-success save'>
                                                @endif

                                            @endif
                                        </div>
                                    </div>


                        </div><!-- /.box-footer !-->



                    </div>
                	</div>
                	</div>

    				</form>
    		</div>
    </div>
    @endif

</body>
@endsection

<style type="text/css">
	.input-group[class*=col-]{
		padding-left: 15px !important;
		padding-right: 15px !important;
	}

	.select2-container .select2-selection--single {
				    border: 1px solid #ccc !important;
				    height: 36px !important;
				    border-radius: 0 !important;
	}

	input#admin_pin,input#branch_pin {
    	width: 84.66%;
    	display: inline-block;
 	}

 	button.btn.generate_pin {
    display: inline;
    margin-top: -3px;
	}

	button.btn.generate_pin:hover{
		opacity: 0.9;
	}

	button.btn.generate_pin:active:focus{
	color: #333;
    background-color: #d4d4d4;
    border-color: #8c8c8c;
	}
</style>

<script src="{{ asset('js/jquery.min.js') }}" type="text/javascript"></script>


<!-- Select2 for company language and company currency -->
<script type="text/javascript">
    $(document).ready(function() {
   	$('#branch_country').select2();
    $('#branch_language').select2();
    // $('#company_currency').select2();
});
</script>


<!-- Generate Pin for branch and check is there no duplication : Branch -->
<script type="text/javascript">
	$(function(){
		$('body').on('click','.branch_generate_pin',function(){
			var pin = Math.floor(1000 + Math.random() * 9000);
			$('#branch_pin').val(pin);

            jQuery.ajax({
                    url     : "{{ route('pin_validation') }}",
                    type    : "POST",
                    data    : {company_pin : pin},
                    success : function(data){
                        if(data == 1){
                              $('#branch_pin').css({'background':'green','color':"#fff"});
                        }else{
                              $('#branch_pin').css({'background':'#e94f4f','color':"#fff"});
                        }
                    },
                    error : function(data){

                    }
                });


		});


        $('body').on('keyup','#branch_pin',function(){
            $('#branch_pin').css({'background':'#e94f4f','color':"#fff"});
            var pin = $(this).val();
            if(pin == ''){
                $('#branch_pin').css({'background':'none','color':"#333"});
            }

            if(pin.length == 4){
                    jQuery.ajax({
                        url     : "{{ route('pin_validation') }}",
                        type  : "POST",
                        data    : {company_pin : pin},
                        success : function(data){
                          if(data == 1){
                              $('#branch_pin').css({'background':'green','color':"#fff"});
                          }else{
                              $('#branch_pin').css({'background':'#e94f4f','color':"#fff"});
                          }
                        },
                        error : function(data){

                        }
                    });
            }


        });




	});
</script>

<!-- Generate Pin for user_admin and check is there no duplication : User Admin -->
<script type="text/javascript">
    $(function(){
        $('body').on('click','.admin_generate_pin',function(){
            var pin = Math.floor(1000 + Math.random() * 9000);
            $('#admin_pin').val(pin);
        });
    });

</script>