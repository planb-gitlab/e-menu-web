<style type="text/css">
	@font-face {
    font-family: Fake Receipt;
    src: url('{{ asset('fonts/fakereceipt.ttf') }}');
	}


	* {
	  box-sizing: border-box;
	}

	@page {
        margin: 5px 20px;
        size: 78mm 30cm;
    }

	body{
		font-size: 10px;
		color:#000;
		font-family: 'Fake Receipt';
	}

	.logo{
		display: block;
		text-align: center;
		margin-bottom: 10px;
	}

	.logo img{
		width: 20%;
    	height: auto;

	}

	#company_name{
		font-size: 12px;
	}

	.info{
		text-align: center;

	}

	#invoice_container {
	  width: 100%;
	  position: relative;
	  display: block;
	  margin-bottom: 20px;
	}

	.invoice_data{
		display: block;
	}

	.left_label{
		width: 40%;
		display: inline-block;
		line-height: 0;
	}

	.left_paid{
		/*width: 60%;*/
		display: inline-block;
		line-height: 0px;
	}
	.total_amount {
		font-size: 14px;
		margin-top: 10px;
		line-height: 0
	}
	.total_value {
		padding: 0 !important;
		margin: 0 !important;
		font-size: 14.5px;
		line-height: 0;

	}

	table{
		width: 100%;
		margin-bottom: 10px;
		border-collapse: collapse;

	}

	table tr.header{
		border-top:3px dashed #000;
	}

	.calculate_data{
		margin-bottom: 10px;
		display: block;
		text-align: right;
	}


</style>
	
<body id="print_layout">

	<div class="info" style="margin-top: 0;">
		<div><img src="{{asset($outlet->company_logo)}}"/></div>
		<div id="company_name">{{$outlet->company_name}} </div>
		<div>{{$outlet->company_address}}</div>
		<div>{{$outlet->company_phone}}</div>
		@if($outlet->company_contact) 
			<div> {{$outlet->company_contact}} </div> 
		@endif 
		<div style="margin:10px 0; border:1px solid #000"></div>
	</div>
	
	<div id="invoice_container">
		<div class="invoice_data">
			<span class="left_label" style="">Invoice No</span>
			<span class="invoice_number">{{ $invoice->invoice_number }}</span>
		</div>
		
		@if ($invoice->order_type == "dinein")
		<div class="invoice_data">
			<span class="left_label" style="">Table No</span>
			<span class="table_number">{{ showTableCode($invoice->table_id) }}</span>
		</div>	
		@endif
		
		<div class="invoice_data">
			<span class="left_label">Date</span>
			<span class="invoice_date">{{ $now }}</span>
		</div>

		<div class="invoice_data">
			<span class="left_label">Seller</span>
			<span class="invoice_date">{{ $invoice->name }}</span>
		</div>
	</div>
	
	<?php $total = 0; ?>
	<table style="border-bottom: 1px dashed #000;">
		<tr class="header" >
			<td style="border-bottom: 1px dashed #000; border-top: 1px dashed #000;">ID</td>
			<td style="border-bottom: 1px dashed #000; border-top: 1px dashed #000;">Name</td>
			<td style="border-bottom: 1px dashed #000; border-top: 1px dashed #000;">Qty</td>
			<td style="border-bottom: 1px dashed #000; border-top: 1px dashed #000;">Price</td>
			<td style="border-bottom: 1px dashed #000; border-top: 1px dashed #000;">Total</td>
		</tr>
		@php $total = 0 @endphp

		@foreach($items as $key => $item)

		<tr>
			<td>{{ ++$key }}.</td>
			<td>
				{{ $item['itemName'] }}
				@if(!empty($item['item_size']))
					<small style="font-size: 8px !important;">( {{ getItemSize($item['item_size']) }} )</small>
				@else 
					<small style="font-size:  8px !important;">( Normal )</small>
				@endif
			</td>
			<td>{{ $item['itemQty'] }}</td>
			<td>{{ $outlet->currency_symbol }}{{ number_format($item['price'], 2) }}</td>
			<td>{{ $outlet->currency_symbol }}{{ number_format($item['itemQty'] * $item['price'], 2) }}</td>
		</tr>
		@php $total += $item['itemQty'] * $item['price'] @endphp
		@if($item['addOn'] != null)
			@foreach($item['addOn'] as $skey => $addon)
				<tr>
					<td width="1"></td>
					<td>>{{ $skey+1 }}.{{ $addon->addOnName }} </td>
					<td>{{$addon->addOnQty }}</td>
					<td>
						{{ $outlet->currency_symbol }}{{ number_format($addon->addOnPrice, 2) }}
					</td>
					<td>
						{{ $outlet->currency_symbol }}
						{{ number_format($addon->addOnQty * $addon->addOnPrice, 2) }}
					</td>
				</tr>
				{{ $total += $addon->addOnQty * $addon->addOnPrice }}
			@endforeach
		@endif
		@endforeach
	</table>

	<div class="calculate_data">

		@if($outlet->include_tax == 1)
			<div class="paid_info">
				<span class="left_paid">Sub Total</span>
				<span class="">{{ $outlet->currency_symbol }}{{ number_format($total, 2) }}</span>
			</div>

			<div class="paid_info">
				<span class="left_paid">Tax</span>
				<span class="">({{ $outlet->company_sale_tax }}%) 
					@php $tax = ($total * $outlet->company_sale_tax)/100 @endphp
					${{ number_format($tax, 2) }}</span>
			</div>
		@endif
		
		@if(!empty($discount_id))
			<div class="paid_info">
				<span class="left_paid">Discount</span>
				<span class="">
					({{ checkDiscount($discount_id)->discount }}{{ checkDiscount($discount_id)->discount_type }})
				</span>
			</div>
		@endif

		<div>
			<div class="paid_info">
				<span class="left_paid total_amount">Total ({{ $outlet->currency_symbol }})</span>
				<span class="total_value">
					{{ $outlet->currency_symbol }}{{ number_format($total + $tax, 2) }}
				</span>
			</div>

			{{-- @fo<div class="paid_info">
				<span class="left_paid total_amount">Total ({{ $exchange->currency_letter }})</span>
				<span class="total_value">{{ $exchange->currency_letter }}{{ number_format(($total + (($total * $outlet->company_sale_tax)/100)) * $exchange->price_to, 2) }}</span>
			</div>reach($exchanges as $exchange)
			
			@endforeach --}}
		</div>
		
		<div style="border-bottom: 1px dashed #000; margin:10px 0"></div>
		
		@if($invoice->paid_total != '0')

			@if($invoice->paid_dollar != 0.0)
			<div class="paid_info">
				<span class="left_paid">Paid Amount($) :</span>
				<span class="">${{ number_format($invoice->paid_dollar, 2) }}</span>
			</div>
			@endif
			
			@if($invoice->paid_riel != 0.0)
			<div class="paid_info">
				<span class="left_paid">Paid Amount(R) :</span>
				<span class="">R{{ number_format($invoice->paid_riel, 2) }}</span>
			</div>
			@endif
			
			@if($invoice->paid_baht != 0.0)
			<div class="paid_info">
				<span class="left_paid">Paid Amount(B) :</span>
				<span class="">B{{ $invoice->paid_paht }}</span>
			</div>
			@endif			

		@if($invoice->return_dollar != 0.0)
			<div class="paid_info">
				<span class="left_paid">Return Amount($) :</span>
				<span class="">${{ number_format($invoice->return_dollar, 2) }}</span>
			</div>
			@endif
			
			@if($invoice->return_riel != 0.0)
			<div class="paid_info">
				<span class="left_paid">Return Amount(R) :</span>
				<span class="">R{{ number_format($invoice->return_riel, 2) }}</span>
			</div>
			@endif
			
			@if($invoice->return_baht != 0.0)
			<div class="paid_info">
				<span class="left_paid">Return Amount(B) :</span>
				<span class="">B{{ $invoice->return_baht }}</span>
			</div>
			@endif
		@endif
	</div>
	<div class="Last_sentence" style="text-align: center; display: block;">
		<span>Thank you ! Please have a great day.</span>
	</div>
</body>


