<style type="text/css">
	@font-face {
    font-family: Fake Receipt;
    src: url('{{ asset('fonts/fakereceipt.ttf') }}');
	}


	* {
	  box-sizing: border-box;
	}

	@page {
        margin: 5px 20px;
        size: 78mm 30cm;
    }

	body{
		font-size: 10px;
		color:#000;
		font-family: 'Fake Receipt';
	}

	#ticket_container {
	  width: 100%;
	  position: relative;
	  display: block;
	  margin-bottom: 20px;
	}

	.ticket_data{
		display: block;
	}

	.left_label{
		width: 40%;
		display: inline-block;
		line-height: 0;
	}


	.left_paid{
		width: 70%;
		display: inline-block;
		line-height: 0;
	}



</style>

<body id="print_layout">
	<div id="ticket_container">
		<div class="ticket_data">
			<span class="left_label" style="">Time</span>
			<span class="" style="font-size: 10px;">{{ $now }}</span>
		</div>
		<?php if ($order_type =="dinein"): ?>
			<div class="ticket_data">
			<span class="left_label">Table</span>
			<span class="" style="font-size: 14px;">{{ $table }}</span>
		</div>
		<?php endif ?>
		<?php if ($order_type =="takeout"): ?>
			<div class="ticket_data">
			<span class="left_label">Order No</span>
			<span class="" style="font-size: 14px;">{{ $orderCode }}</span>
		</div>
		<?php endif ?>

		<div class="ticket_data">
			<span class="left_label">Type:</span>
			<span class="" style="font-size: 14px;">{{ $order_type }}</span>
		</div>
	</div>

	<div id="ticket_container">
		<div class="ticket_data" style="text-align: center; border-top:1px dashed #000; border-bottom: 1px dashed #000; padding:10px 0;">
			Full Item
		</div>

		<br/>

		<table style="width: 100%; font-size: 12px">
		
		@foreach($items as $key => $item)
			<tr>
				<td style="width: 80%;">
					{{ $key + 1 }}.
					{{ $item['item_name'] }} 

					@if(!empty($item['item_size']))
						<br/>
						<div style="text-indent:50px; font-size: 8px !important">({{ getItemSize($item['item_size']) }})</div>
					@endif
				</td>
				<td style="text-align: right;">{{ $item['addmore']}}</td>
			</tr>
			@foreach($item['addons'] as $addon)
			<tr style="text-indent: 30px;">
				<td style="width: 80%;"> > {{ $addon['addon_name'] }}</td>
				<td style="text-align: right;">{{$addon['addon_qty'] }}</td>
			</tr>
			@endforeach
		@endforeach
		</table>
	</div>



</body>


