<div class="form-group header-group-0" id="form-group-membership_no" >
            <div class="col-md-12">   
            	      	
               <input maxlength="6" name="membership_no" placeholder="Membership No" class="form-control" required id="membership_no" style="width: 84.66%; display: inline-block;"  />
               <button type="button" class="btn generate_pin" style="display: inline; margin-top: -3px;">{{trans('setting.Generate')}}</button>
            </div>
</div>

<script src="{{ asset('js/jquery.min.js') }}" type="text/javascript"></script>
<script>
	$(function(){
		$('body').on('click','.generate_pin',function(){
			var pin = Math.floor(100000 + Math.random() * 900000);
			$('#membership_no').val(pin);

				jQuery.ajax({
		            url     : "{{ route('validation_for_membership') }}",
		            type  	: "POST",
		            data    : {pin : pin},
		            success : function(data){
		              if(data == null){
		                  $('#membership_no').css({'background':'#e94f4f','color':"#fff"});
		              }else{
		                  $('#membership_no').css({'background':'green','color':"#fff"});
		              }
		            },
		            error : function(data){

		            }
		        });

		});


		$('body').on('keyup','#membership_no',function(){
			$('#membership_no').css({'background':'#e94f4f','color':"#fff"});
			var pin = $(this).val();
			if(pin == ''){
				$('#membership_no').css({'background':'none','color':"#333"});
			}

			if(pin.length == 6){
					jQuery.ajax({
			            url     : "{{ route('validation_for_membership') }}",
			            type  	: "POST",
			            data    : {pin : pin},
			            success : function(data){
			              if(data == ''){
			                  $('#membership_no').css({'background':'#e94f4f','color':"#fff"});
			              }else{
			                  $('#membership_no').css({'background':'green','color':"#fff"});
			              }
			            },
			            error : function(data){

			            }
			        });
			}


		});

		var url = window.location.pathname;
		if($.isNumeric(url.split('/').reverse()[0])){
				$('#membership_no').removeAttr('required');
		}


	});

</script>
