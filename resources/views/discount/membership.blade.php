@extends("layouts.create_update")
@section("content")
		<form class='form-horizontal' method='post' id="form" enctype="multipart/form-data" action="{{route('update.membership_discount')}}"> 
		<input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <input type='hidden' name='return_url' value='{{ @$return_url }}'/>
        <input type='hidden' name='ref_mainpath' value='{{ CRUDBooster::mainpath() }}'/>
        <input type='hidden' name='ref_parameter' value='{{urldecode(http_build_query(@$_GET))}}'/>
        @if($hide_form)
            <input type="hidden" name="hide_form" value='{!! serialize($hide_form) !!}'>
        @endif
            <input type="hidden" name="id" value="{{$membership->id}}" />


           <div class="row">
           <div class="col-md-12">
           <div class="box-body">

                <!-- NAME -->
           		<div class="form-group header-group-0" id="form-group_discount_type">
                            <label class="control-label col-sm-2">{{trans('discount.Discount Type')}}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-sm-6">
                              	<select name="discount_type" required class="form-control">
                              		<option value="">**Please select discount type</option>
                              		<option value="%" @if($membership->discount_type == "%") selected @endif>%</option>
                              		<option value="$" @if($membership->discount_type == "$") selected @endif>$</option>
                              	</select>
                            </div>
                </div>

                <!-- NAME -->
           		<div class="form-group header-group-0" id="form-group_discount_amount">
                            <label class="control-label col-sm-2">{{trans('discount.Discount')}}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-sm-6">
                              	<input type="number" name="discount" value="{{$membership->discount}}" class="form-control" placeholder="Please input discount amount in number">
                            </div>
                </div>

                

           </div>
           </div>
           </div>

           <div class="box-footer" style="background: #F5F5F5">

                                    <div class="form-group">
                                        <label class="control-label col-sm-2"></label>
                                        <div class="col-sm-10">
                                            @if($button_cancel && CRUDBooster::getCurrentMethod() != 'getDetail')
                                                @if(g('return_url'))
                                                    <a href='{{g("return_url")}}' class='btn btn-default'><i
                                                                class='fa fa-chevron-circle-left'></i> {{trans("crudbooster.button_back")}}</a>
                                                @else
                                                    <a href='{{CRUDBooster::mainpath("?".http_build_query(@$_GET)) }}' class='btn btn-default'><i
                                                                class='fa fa-chevron-circle-left'></i> {{trans("crudbooster.button_back")}}</a>
                                                @endif
                                            @endif
                                            

                                                @if(CRUDBooster::isCreate() && $command == 'add')
                                                    <input type="submit" name="submit" value='{{trans("crudbooster.button_save_more")}}' class='btn btn-success'>
                                                @endif

                                                    <input type="submit" name="submit" value='{{trans("crudbooster.button_save")}}' class='btn btn-success save'>

                                        </div>
                                    </div>

            </div>

	</form>
@endsection