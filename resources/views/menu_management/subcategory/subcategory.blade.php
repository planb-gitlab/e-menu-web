@extends("layouts.create_update")
@section("content")
	<form class='form-horizontal' method='post' id="form" enctype="multipart/form-data" 
					action="{{ (@!$subcategory->id)?
                                route('save.subcategory'):
                                route('update.subcategory')
                            }}">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type='hidden' name='return_url' value='{{ @$return_url }}'/>
        <input type='hidden' name='ref_mainpath' value='{{ CRUDBooster::mainpath() }}'/>
        <input type='hidden' name='ref_parameter' value='{{urldecode(http_build_query(@$_GET))}}'/>
        @if($hide_form)
            <input type="hidden" name="hide_form" value='{!! serialize($hide_form) !!}'>
        @endif

        <input type="hidden" class="form-control" name="id"  value="{{ $id }}">


        <div class="row">
        <div class="col-md-12">
        <div class="box-body">

            <div class="form-group header-group-0" id="form-group-category_name">
                <label class="control-label col-sm-2">{{trans('menumanagement.Category')}}
                <span class="text-danger" title="This field is required">*</span></label>
                <div class="col-sm-6">
                    <select name="category" class="form-control category" required id='category'>
                    <option value="">Please select category menu</option>
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}" @if($subcategory->category_id == $category->id) selected="selected" @endif>{{$category->name}}</option>
                    @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group header-group-0" id="form-group-category_name">
                <label class="control-label col-sm-2">{{trans('menumanagement.Name')}}
                <span class="text-danger" title="This field is required">*</span></label>
                <div class="col-sm-6">
                   <input type="text" name="name" class="form-control" required placeholder="Input category name" id="name" value="{{ $subcategory->name }}" autocomplete="off">
                </div>
            </div>


            <div class="form-group header-group-0" id="form-group-category_photo">
                <label class="control-label col-sm-2">{{trans('menumanagement.Photo')}}</label>
                <div class="col-sm-6">
                   @if(empty($subcategory->photo))
                   <input type="file"   class="form-control" name="photo" id="photo" accept="image/*"> 
                    <div style="color:#908e8e; margin-top: 5px;">{{trans('menumanagement.File types support : JPG, JPEG, PNG, GIF, BMP')}}</div>
                   @else
                    <p>
                        <a data-lightbox="roadtrip" href="{{$url}}/{{$subcategory->photo}}">
                        	<img style="max-width:160px" title="{{$subcategory->photo}}" src="{{$url}}/{{$subcategory->photo}}">
                        </a>
                    </p>
                    
                    <input type="hidden" name="photo" value="{{$subcategory->photo}}">                   
                        <p><a class="btn btn-danger btn-delete btn-sm" onclick="if(!confirm('Are you sure ?')) return false" href="{{$url}}/admin/subcategories/delete-image?image={{$subcategory->photo}}&amp;id={{$subcategory->id}}&amp;column=photo"><i class="fa fa-ban"></i> {{trans('menumanagement.Delete')}} </a></p>
                        <p class="text-muted"><em>{{trans('menumanagement.* If you want to upload other file, please first delete the file.')}}</em></p>
                     <div class="text-danger"></div>


                   @endif
                </div>

            </div>

                        



        </div>
        </div>
        </div>


        <div class="box-footer" style="background: #F5F5F5">

                        <div class="form-group">
                            <label class="control-label col-sm-2"></label>
                            <div class="col-sm-10">
                                @if($button_cancel && CRUDBooster::getCurrentMethod() != 'getDetail')
                                    @if(g('return_url'))
                                        <a href='{{g("return_url")}}' class='btn btn-default'><i
                                                    class='fa fa-chevron-circle-left'></i> {{trans("menumanagement.Back")}}</a>
                                    @else
                                        <a href='{{CRUDBooster::mainpath("?".http_build_query(@$_GET)) }}' class='btn btn-default'><i
                                                    class='fa fa-chevron-circle-left'></i> {{trans("menumanagement.Back")}}</a>
                                    @endif
                                @endif
                                @if(CRUDBooster::isCreate() || CRUDBooster::isUpdate())

                                    @if(CRUDBooster::isCreate() && $button_addmore==TRUE && $command == 'add')
                                        <input type="submit" name="submit" value='{{trans("crudbooster.button_save_more")}}' class='btn btn-success'>
                                    @endif

                                    @if($button_save && $command != 'detail')
                                        <input type="submit" name="submit" value='{{trans("menumanagement.Save")}}' class='btn btn-success save'>
                                    @endif

                                @endif
                            </div>
                        </div>


        </div><!-- /.box-footer !-->
    </form>


@endsection