@extends("layouts.create_update")
@section('content')
	<form class='form-horizontal' method='post' id="form" enctype="multipart/form-data" 
					action="{{ route('add.menu_addon') }}">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type='hidden' name='return_url' value='{{ @$return_url }}'/>
        <input type='hidden' name='ref_mainpath' value='{{ CRUDBooster::mainpath() }}'/>
        <input type='hidden' name='ref_parameter' value='{{urldecode(http_build_query(@$_GET))}}'/>
        @if($hide_form)
            <input type="hidden" name="hide_form" value='{!! serialize($hide_form) !!}'>
        @endif

        <input type="hidden" class="form-control" name="id"  value="{{ $id }}">


        <div class="container-fluid">
        <div class="row">
        <div class="col-md-12">
        <div class="box-body">

            <div class="form-group header-group-0" id="form-group-name">
                <label class="control-label col-sm-2">Menu</label>
                <div class="col-sm-6 control-label" style="text-align: left;">{{$menu->name}}</div>
            </div>

            @foreach($addons as $a)
                <div class="form-group header-group-0" id="form-group-addon">
                    <label class="col-sm-2 control-label">{{$a->name}}</label>
                    <div class="col-sm-6 control-label" style="text-align: left;">
                        <?php $addon = DB::table('menu_detail')->where('menu_id',$id)->where('material_id',$a->id)->first(); ?>
                        <input type="checkbox" name="addon[]" value="{{$a->id}}" @if($addon->material_id == $a->id) checked @endif />
                    </div>
                </div>
            @endforeach

        </div>
        </div>
        </div>
        </div>


        <div class="box-footer" style="background: #F5F5F5">
                        <div class="form-group">
                            <label class="control-label col-sm-2"></label>
                            <div class="col-sm-10">
                                @if($button_cancel && CRUDBooster::getCurrentMethod() != 'getDetail')
                                    @if(g('return_url'))
                                        <a href='{{g("return_url")}}' class='btn btn-default'><i
                                                    class='fa fa-chevron-circle-left'></i> {{trans("menumanagement.Back")}}</a>
                                    @else
                                        <a href='{{CRUDBooster::mainpath("?".http_build_query(@$_GET)) }}' class='btn btn-default'><i
                                                    class='fa fa-chevron-circle-left'></i> {{trans("menumanagement.Back")}}</a>
                                    @endif
                                @endif
                                @if(CRUDBooster::isCreate() || CRUDBooster::isUpdate())

                                    @if(CRUDBooster::isCreate() && $button_addmore==TRUE && $command == 'add')
                                        <input type="submit" name="submit" value='{{trans("crudbooster.button_save_more")}}' class='btn btn-success'>
                                    @endif

                                    @if($button_save && $command != 'detail')
                                        <input type="submit" name="submit" value='{{trans("menumanagement.Save")}}' class='btn btn-success save'>
                                    @endif

                                @endif
                            </div>
                        </div>


        </div><!-- /.box-footer !-->
    </form>
@endsection

@section('css')
@endsection

@section('script')
@endsection

@section('modal')
@endsection