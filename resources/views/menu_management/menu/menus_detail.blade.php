@extends('crudbooster::admin_template')
@section('content')
    @if(CRUDBooster::getCurrentMethod() != 'getProfile' && $button_cancel)
            @if(g('return_url'))
                <p><a title='Return' href='{{g("return_url")}}'><i class='fa fa-chevron-circle-left '></i>
                        &nbsp; {{trans("crudbooster.form_back_to_list",['module'=>CRUDBooster::getCurrentModule()->name])}}</a></p>
            @else
                <p><a title='Main Module' href='{{CRUDBooster::mainpath()}}'><i class='fa fa-chevron-circle-left '></i>
                        &nbsp; {{trans("crudbooster.form_back_to_list",['module'=>CRUDBooster::getCurrentModule()->name])}}</a></p>
            @endif
    @endif

    <div class="panel panel-default">
            <div class="panel-heading">
                <strong><i class='{{CRUDBooster::getCurrentModule()->icon}}'></i> {!! $page_title or "Menus" !!}</strong>
            </div>

            <div class="panel-body" style="padding: 20px 0 0 0">
               
                <div class="box-body" id="parent-form-area">

                    <div class="table-responsive">
                        <table id="table-detail" class="table table-striped">
                            <tbody>
                                <tr>
                                    <td>{{trans('menumanagement.Category')}}</td>
                                    <td>{{$menu->category_name}}</td>
                                </tr>

                                @if($menu->subcategory_id)
                                <tr>
                                    <td>{{trans('menumanagement.Sub Category Name')}}</td>
                                    <td>{{$menu->subcategory_name}}</td>
                                </tr>
                                @endif

                                <tr>
                                    <td>{{trans('menumanagement.Menu Name')}}</td>
                                    <td>{{$menu->name}}</td>
                                </tr>

                                @if($menu->description)
                                <tr>
                                    <td>{{trans('menumanagement.Description')}}</td>
                                    <td>{{$menu->description}}</td>
                                </tr>
                                @endif
                                
                                <tr>
                                    <td>{{trans('menumanagement.Menu Photo')}}</td>
                                    <td><img src="{{ asset($menu->photo) }}" /></td>
                                </tr>

                                <tr>
                                    <td>{{trans('menumanagement.Price')}} ({{$company_currency->currency_symbol}})</td>
                                    <td>{{$menu->price}}</td>
                                </tr>

                            </tbody>
                        </table>

                        @if($menu->uom_size != null)
                        <div class="col-md-6 addon">
	                        <table class="table table-striped table-bordered table-hover">
	                        	<tr style="background: #3C5A99; color:#fff;">
	                                	<td>No</td>
	                                    <td>{{trans('menumanagement.UOM')}}</td>
	                                    <td>{{trans('menumanagement.Price')}}</td>
	                            </tr>

			                    @foreach($uoms as $key => $uom )
	                            	<tr>
	                            		<td>{{++$key}}</td>
	                                    <td>{{$uom->uom}}</td>
	                                    <td>{{$uom->uom_price}}</td>
	                                </tr>
	                            @endforeach
	                        </table>
                        </div>
						@endif
                          
                        @if($menu->material_size != null)   
                        <div class="col-md-6 uom">
	                        <table class="table table-striped table-bordered table-hover">   
                                <tr style="background: #3C5A99; color:#fff;">
                                	<td>No</td>
                                    <td>{{trans('menumanagement.Addon')}}</td>
                                </tr>

                                @foreach($addons as $key => $addon )
	                            	<tr>
	                            		<td>{{++$key}}</td>
	                                    <td>{{$addon->name}}</td>
	                                </tr>
	                            @endforeach
                        	</table>
                        </div>
                        @endif

                    </div>                                            

                </div>

                    

            </div>


    </div>

@endsection

<style>
	.addon,.uom{
		padding-left: 2px !important;
		padding-right: 2px !important;
	}

	.addon table,.uom table{
		box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
	}
</style>
