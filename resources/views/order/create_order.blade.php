@extends("layouts.order")
@section("content")
<body class="cashier_create_order">

    <!-- =======================  Outlet's Info ============================= -->
    <input type="hidden" id="company_id" value="{{ $setting->id }}">
    <input type="hidden" id="discount_type_id" value="">

    <input type="hidden" id="company_currency" value="{{ $company_currency->company_currency }}" data-symbol='{{ $company_currency->currency_symbol }}'>
    @foreach($other_exchange as $exchange)
    <input type="hidden" id='{{ $exchange->currency_to }}' data-from="{{$exchange->price_from}}" data-to="{{$exchange->price_to}}">
    @endforeach
    <input type="hidden" id='grand_total_complete_data' value='' />
    <input type="hidden" id="paid_amount" class="paid_amount_company_currency" value="0" data-val="{{ $company_currency->company_currency }}" data-name="{{ $company_currency->currency_symbol }}" data-amount="0">
    @foreach($currencies as $currency)
    <input type="hidden" id="paid_amount_{{$currency->id}}" class="paid_amount" value="0" data-val="{{$currency->id}}" data-name="{{$currency->currency_symbol}}" data-amount="0">
    @endforeach
    <input type="hidden" id="paid_total"    value="0" disabled="">
    <input type="hidden" id="return_amount" value="0.00">
    <input type='hidden' id="takeout"       value="{{$_GET['order_id']}}">
    @if($_GET['order_data'])
    <input type='hidden' id="old_order_table"     value="{{$_GET['table_name']}}">
    @endif

    <div class="container-fluid header clearfix">
    <div class="row">
            <div class="col-md-5 col-sm-5 col-xs-12 rsp_center module">
                <strong class="title"><i class='{{CRUDBooster::getCurrentModule()->icon}}'></i> {{ $page_title }}</strong>
            </div>

            <div class="col-md-7 col-sm-7 col-xs-12 rsp_center orderData">
                <table class="table_information_top" style="color:#fff; float: right; font-size: 2.5vh; position: relative;
                                    top: 9px; ">

                    <tr>
                        <td style="padding-right: 20px;" >
                            <span class="cashier_title">{{trans('order.Outlet')}} :</span>
                            <span class="cashier_data">{{$setting->company_name}}</span>
                        </td>
                        <td rowspan="2" style="padding: 0 20px; border-left: 1px solid #fff;">
                            <span class="cashier_title">{{trans('order.Cashier Name')}} : </span>
                            <span class="cashier_id" style="display:none;">{{$user->id}}</span>
                            <span class="cashier_data">{{$user->name}}</span>
                        </td>
                    </tr>

                    <span class="company_tax" style="display: none;">{{$tax->company_sale_tax}}</span>

                    <tr>
                        <td style="padding-right: 20px;" >
                               @if($_GET['table_id'] || $_GET['table_id'] == 0)
                               <style>
                                   .table_information_top{
                                        position: relative;
                                        top: 0px !important;
                                   }
                               </style>
                               <div class="table_id" style="display:none;">{{$_GET['table_id']}}</div>
                               <div class="table_code">
                                <span class="table_title">{{trans('order.Table')}} : </span>
                                <span class="table_data">{{$_GET['table_name']}}</span>
                            </div>
                            @endif
                        </td>
                    </tr>
                </table>
            </div>

    </div>
    </div>

<div class="container-fluid body clearfix">
<div class="row">
<div class="col-md-5 col-sm-5 col-xs-12 customer_order">
        <table id="table_order">
            <thead>
                <tr>
                    <th>{{trans('order.Description')}}</th>
                    <th>{{trans('order.Qty')}}</th>
                    <th>{{trans('order.Price')}}({{$company_currency->currency_symbol}})</th>
                    <th>{{trans('order.Subtotal')}} ({{$company_currency->currency_symbol}})</th>
                    <th class="note_title" style="text-align: center;">
                        <span style="background-size: 10px; display:none">{{trans('order.Note Added')}}</span>
                    </th>
                </tr>
            </thead>
            <tbody id="table_order_data" data-new='0'></tbody>
        </table>

        <div class="total_amount">
            <div class="row">
                <div class="col-md-12 col-sm-12 old_order">
                    <input type="hidden" value="0" class="order_total_tax"/>
                </div>

                @if($exist_order == 'true')
                <div class="col-md-5 col-sm-5 col-xs-5 discount_data" style="text-align: left;">
                    <span style="margin-right: 5px;" class="discount_button discount_container">
                        <img title="discount" src="{{asset('uploads/defualt_image/percentage-discount.png')}}" />
                    </span>

                    <span style="margin-right: 5px;" class="discount_button membership_container">
                        <img title="membership" src="{{asset('uploads/defualt_image/user-discount.png')}}" />
                    </span>
                    
                    <!-- <span style="margin-right: 5px;" class="discount_button promotion_pack_container">
                        <img title="promotion pack" src="{{asset('uploads/defualt_image/promotion-discount.png')}}" />
                    </span> -->

                    <span id="remove_discount" style="display:none; color:#f00;">Remove Discount</span>
                </div>
                <div class="col-md-7 col-sm-7 col-xs-7 discount_total total">
                @else
                <div class="col-md-12 col-sm-12 col-xs-12 discount_total total">
                @endif

                    <input type="hidden" class="order_total_tax" value="0.00">
                    <span class="total_title">{{trans('order.Total')}} : <span class="discount_result" style="display: none;"></span></span>
                        <span id="order_total">
                            <span>{{$company_currency->currency_symbol}}</span>
                            <span id="order_total_data_no_discount" style="display: none;"></span>
                            <span id="order_total_data">
                                @if($order_menu)
                                {{  number_format($order_menu_payment->total, 2) }}
                                @else
                                0.00
                                @endif
                            </span>
                        </span>
                </div>
            </div>
        </div>

        <div id="operation_button">
                
                <div class="button save_data" @if($exist_order == 'true') style="display: none;" @endif>
                    <div class="item"><img class="button_image" src="{{asset('uploads/defualt_image/save.png')}}" /></div>
                    <p>{{trans('order.Save')}}</p>
                </div>
                

                @if($exist_order == 'true')
                

                <div class="button paidPrint">
                    <div class="item"><img class="button_image" src="{{asset('uploads/defualt_image/order_paid.png')}}" /></div>
                    <p>{{trans('order.Paid & Print')}}</p>
                </div>

                <div class="button print_invoice" data-val="order and print" data-invoice="{{$invoice_id->invoice_number}}">
                    <div class="item"><img class="button_image" src="{{asset('uploads/defualt_image/print.png')}}" /></div>
                    <p>{{trans('order.Print_invoice')}}</p>
                </div>

                <div class="button split_payment" data-invoice="{{$invoice_id->invoice_number}}">
                    <div class="item"><img class="button_image" src="{{asset('uploads/defualt_image/split.png')}}" /></div>
                    <p>{{trans('order.Split Payment')}}</p>
                </div>
                @endif

                <div class="button exit">
                    <div class="item"><img class="button_image" src="{{asset('uploads/defualt_image/exit.png')}}" /></div>
                    <p>{{trans('order.Exit')}}</p>
                </div>
        </div>

</div>

    <div class="col-md-1 col-sm-1 col-xs-1 menuRotate">
        <a id="category">
            <div class="rotate category" is-have="1">
                {{trans('order.Category')}}
                <i class="fa fa-search"></i>
            </div>
        </a>    

        @if(count($subcategories) > 0)
            <a id="subcategory" class="js-ripple">
                <div class="rotate subcategory" is-have="1" >{{trans('order.Subcategory')}}</div>
            </a>
        @endif

            <a id="menu" class="js-ripple">
                <div class="rotate menu" is-have="1">
                    {{trans('order.Menu')}}
                    <i class="fa fa-search"></i>
                </div>
            </a>
    </div>

        <div class="row">        
        <div class="food">
            <div class="category">
                <div class="category_data">
                    <a class="category_code" data-val="0">
                        <div class="category_item item-0">{{ trans('order.most_sold') }}</div>
                    </a>

                    <!-- <a class="category_code" data-val="99999">
                        <div class="category_item item-99999">{{ trans('order.addon') }}</div>
                    </a> -->
                    @foreach($categories as $category)
                    <a class="category_code" data-val="{{$category->id}}">
                        <div class="category_item item-{{$category->id}}">
                            {{$category->name}}   
                        </div>
                    </a>
                    @endforeach
                </div>

            </div>
        </div>

        @if(count($subcategories) > 0)
            <div class="food">
            <div class="subcategory">
                <div class="subcategory_data">
                    @foreach($subcategories as $subcategory)
                    <a class="subcategory_code" data-val="{{$subcategory->id}}">
                        <div class="subcategory_item item-{{$subcategory->id}}" >

                            {{$subcategory->name}}

                        </div>
                    </a>
                    @endforeach
                </div>
            </div>
            </div>
        @endif

            <div class="food">
            <div class="menu">
                <div class="menu_data">
                    @foreach($menus as $menu)
                    <?php $uom      = DB::table('menu_uom_price')->where('menu_id',$menu->id)->get(); ?>
                    <?php $addon    = DB::table('menu_detail')->where('menu_id',$menu->id)->get(); ?>

                    @if($menu->menu_id != null)
                    <div class="menu_item">
                        <a class="menu_code" data-val="{{$menu->id}}" data-name="{{$menu->name}}" data-amount="{{$menu->price}}" data-size="{{ count($uom) }}" data-material="{{ count($addon) }}" >
                            <img class="menu_image" src="{{asset($menu->photo)}}"/>
                            <div class="menu_name">{{mb_substr($menu->name,0,14,'utf-8')}}</div>
                        </a>
                    </div>
                    @else
                    <div class="menu_item">
                        
                        @if(count($uom) > 0)
                            <span class="have_size"></span>
                        @endif

                        @if(count($addon) > 0)
                        <span class="have_material"></span>
                        @endif
                        <a class="menu_code" data-val="{{$menu->id}}" data-name="{{$menu->name}}" data-amount="{{$menu->price}}"
                            data-size="{{ count($uom) }}" data-material="{{ count($addon) }}">
                            <img class="menu_image" src="{{asset($menu->photo)}}"/>
                            <div class="menu_code_number"> {{ !empty($menu->menu_code) ? '#' . $menu->menu_code : $menu->menu_code }}</div>
                            <div class="menu_name">{{mb_substr($menu->name,0,14,'utf-8')}}</div>
                        </a>
                    </div>
                    @endif
                    @endforeach
                </div>
            </div>
            </div>
        </div>


</div>
</div> 
</body>
@endsection

@section('css')
<link   rel="stylesheet" href="{{ asset('css/create_order.css') }}">
<link   rel="stylesheet" href="{{ asset('css/template.css') }}">
<link   rel="stylesheet" href="{{ asset('css/print/print_ticket.css') }}" media="print">
<link   rel="stylesheet" href="{{ asset('css/jquery.toast.css') }}">
<link   rel="stylesheet" href="{{ asset('vendor/crudbooster/assets/adminlte/font-awesome/css/font-awesome.min.css') }}"/>
<style>
    @font-face {
        font-family: Roboto Regular;
        src: url('{{ asset('fonts/Roboto-Regular.ttf') }}');
    }

    body{
        font-family: 'Roboto Regular','khmer regular';
    }
</style>
@endsection

@section('script')
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{ asset('js/jquery.toast.js') }}"></script>
<script src="{{ asset('js/TableToJson.min.js') }}"></script>
<script src="{{ asset('js/jquery.mousewheel.min.js')}}"></script>
<script src="https://js.pusher.com/4.3/pusher.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/push.js/1.0.5/push.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.12/dist/sweetalert2.all.min.js"></script>

<!-- ======================== Global Variable ========================-->
<script>
    var include_discount = 0;   
</script>

<!-- ======================== Make Category and Subcategroy scroll horizontal ==================== -->
<script>
    $(document).ready(function() {
        $('.category_data, .subcategory_data').mousewheel(function(e, delta) {
            this.scrollLeft -= (delta * 40);
            e.preventDefault();
        });
    });
</script>

<!-- ======================== Show existing order before paid ==================== -->
<script>
    $(function(){
        var existing    = "{{$_GET['order_data']}}";
        if(existing != ''){
            var table_id    = "{{$_GET['table_name']}}";
            var invoiceId   = "{{$_GET['invoice_id']}}";
            var total       = 0;
            var order_type  = "{{$order_type}}";
            
            // return false
                jQuery.ajax({
                    url         : "{{ route('get_order') }}",
                    method      : "POST",
                    data        : {tableid : table_id, invoiceId : invoiceId, orderData : existing, order_type:order_type},
                    beforeSend  : function(data){   $('#table_order_data').html("Loading...");    },
                    success     : function(data){   $('#table_order_data').html(data);
                        $('#table_order_data tr').each(function(){
                            total += parseFloat($(this).find('td.m_subamount').text());
                        });
                        $('#order_total_data').html(total.toFixed(2));
                        
                        
                        var full_total_popup = parseFloat($('#order_total_data').text());
                        var tax_popup        = "{{$tax->company_sale_tax}}";
                        var sale_tax_popup   = full_total_popup * (tax_popup/100);
                        var total_popup      = full_total_popup + sale_tax_popup; // with discount and tax
                        $('.order_total_tax').val(total_popup);
                    },
                    error       : function(data){
                        swal("Warning", "There must be a problem with the request ! Please check again", "warning");
                    }
                });
            // }
        }
    });
</script>

<!-- ======================== Change height of menu when there is no subcategory ==================== -->
<script type="text/javascript">
    $(function(){
        var subcategory_menu        =   $('.rotate.subcategory').attr('is-have');
        if(subcategory_menu != 1){      $('.rotate.menu').css('height','67.8vh');
        $('.food .menu .menu_data').css('height','67.8vh');
    }               
});
</script>

<!-- ======================== Promotion Pack Popup trigger ==================== -->
<script>
    $(function(){
        $('.membership_container').on('click',function(){
            $('#membership_modal').modal('show');
            $('#membership_modal').css('z-index','1053');
        });
        $('.discount_container').on('click',function(){
            $('#discount_modal').modal('show');
            $('#discount_modal').css('z-index','1053');
        });
        $('.promotion_pack_container').on('click',function(){
            $('#promotion_pack_modal').modal('show');
            $('#promotion_pack_modal').css('z-index','1053');
        });
    });
</script>

<!-- ======================== Swal Popup (Information) on Menu that have size or have addon ==================== -->
<script>
    $(function(){
        $('body').on('click','.have_size',function(){
            swal('Have Size','This item have size!','info');
        });

        $('body').on('click','.have_material',function(){
            swal('Have Material','This item have material!','info');
        });
    });
</script>


<!-- ======================== Auto input cursor into search field ==================== -->
<script>
    $(function(){
        $('body').on('shown.bs.modal','.search_item_modal', function () {
            $(this).find('.search_data').focus();
        });

        $('body').on('hidden.bs.modal','.search_item_modal', function () {
            $(this).find('.search_data').val('');
        });
    });
</script>



<!-- ======================== Search Category + Click on Category Function ==================== -->
<script type="text/javascript">
    $(function(){
        var category_has_click      = 0;
        var subcategory_has_click   = 0;
        var menu_has_click          = 0;
        $('body').on('click','#category',function(e){
            if(category_has_click == 0){
                $('#category_search_modal').modal('show');
            }else{
                jQuery.ajax({
                    url         : "{{ route('show_category') }}",
                    method      : "POST",
                    data        : {search_data : null},
                    dataType    : "html",
                    beforeSend  : function(){
                        $('#category .category').html('Category');
                        $('#category .category').removeClass('remove_filter');
                    },
                    success     : function(data){
                        $('.category_data').html(data);
                        category_has_click = 0;
                    },
                    error       : function(data){
                        swal("Warning", "There must be a problem with the request ! Please check again", "warning");
                    }
                });

                jQuery.ajax({
                    url         : "{{ route('show_subcategory') }}",
                    method      : "POST",
                    data        : {search_data : null},
                    dataType    : "html",
                    beforeSend  : function(){
                        $('#subcategory .subcategory').html('Subcategory');
                        $('#subcategory .subcategory').removeClass('remove_filter');
                    },
                    success     : function(data){
                        if(data != null && data != ""){
                            $(".subcategory_data").hide().html(data).fadeIn('100');
                            $(".subcategory_data").css({'text-align': 'left','padding':'0'}); 
                        }else{   
                            $(".subcategory_data").html("No data");
                            $(".subcategory_data").css({'text-align': 'center','padding':'7vh'}); 
                        }
                        subcategory_has_click = 0;
                    },
                    error       : function(data){
                        swal("Warning", "There must be a problem with the request ! Please check again", "warning");
                    }
                });

                jQuery.ajax({
                    url         : "{{ route('show_menu') }}",
                    method      : "POST",
                    data        : {search_data : null},
                    dataType    : "html",
                    beforeSend  : function(){
                        $('#menu .menu').html('Menu');
                        $('#menu .menu').removeClass('remove_filter');
                    },
                    success     : function(data){
                        if(data != null && data != ""){
                            $(".menu_data").hide().html(data).fadeIn('100');
                            $(".menu_data").css({'text-align': 'left','padding':'1vh'});
                        }else{ 
                            $(".menu_data").html("No data");
                            $(".menu_data").css({'text-align': 'center','padding':'21vh',});
                        }
                        menu_has_click = 0;
                    },
                    error       : function(data){
                        swal("Warning", "There must be a problem with the request ! Please check again", "warning");
                    }
                });
            }
        });

        $('#category_search_modal .search').on('click',function(){
            var search_data = $('#category_search_modal .search_data').val();
            jQuery.ajax({
                url         : "{{ route('show_category') }}",
                method      : "POST",
                data        : {search_data : search_data},
                dataType    : "html",
                beforeSend  : function(){
                    $('#category .category').html('Remove Filter');
                    $('#category .category').addClass('remove_filter');
                },
                success     : function(data){
                    $('.category_data').html(data);
                    category_has_click = 1;
                },
                error       : function(data){
                    swal("Warning", "There must be a problem with the request ! Please check again", "warning");
                }
            });
        });

        $('body').on('click','.category_code',function(){
            var category_id = $(this).attr('data-val');
            jQuery.ajax({
                url         : "{{ route('show_subcategory_by_category') }}",
                method      : "POST",
                data        : {category_id},
                dataType    : "html",
                beforeSend  : function(){
                    $(".category_item").removeClass("active");
                    $(".category_item.item-" + category_id).addClass("active");

                    $("#category .category").html("Remove Filter");
                    $("#category .category").addClass("remove_filter");

                    $("#subcategory .subcategory").html("Subcategory");
                    $("#subcategory .subcategory").removeClass("remove_filter");
                },
                success     : function(data){
                    if(data != null && data != ""){
                        $(".subcategory_data").hide().html(data).fadeIn('100');
                        $(".subcategory_data").css({'text-align': 'left','padding':'0'}); 
                    }else{   
                        $(".subcategory_data").html("No data");
                        $(".subcategory_data").css({'text-align': 'center','padding':'7vh'}); 
                    }
                },
                error       : function(data){
                    swal("Warning", "There must be a problem with the request ! Please check again", "warning");
                }
            });


            jQuery.ajax({
                url         : "{{ route('show_menu_by_category_subcategory') }}",
                type        : "POST",
                data        : { category_id:category_id },
                dataType    : "html",
                success     : function(data){
                    if(data != null && data != ""){
                        $(".menu_data").hide().html(data).fadeIn('100');
                        $(".menu_data").css({'text-align': 'left','padding':'1vh'});
                    }else{ 
                        $(".menu_data").html("No data");
                        $(".menu_data").css({'text-align': 'center','padding':'21vh',});
                    }
                }
            });
            category_has_click = 1;
        });
    });
</script>

<!-- ======================== Search Subcategory + Click on Subcategory Function ==================== -->
<script>
    $(function(){
        var subcategory_has_click = 0;
        $('body').on('click','#subcategory',function(e){
            if(subcategory_has_click == 0){
                $('#subcategory_search_modal').modal('show');
            }else{
                jQuery.ajax({
                    url         : "{{ route('show_subcategory') }}",
                    method      : "POST",
                    data        : {search_data : null},
                    dataType    : "html",
                    beforeSend  : function(){
                        $('#subcategory .subcategory').html('Subcategory');
                        $('#subcategory .subcategory').removeClass('remove_filter');
                    },
                    success     : function(data){
                        if(data != null && data != ""){
                            $('.subcategory_data').html(data).fadeIn('100');
                            $(".subcategory_data").css({'text-align': 'left','padding':'0'}); 
                        }else{ 
                            $(".subcategory_data").html("No data");
                            $(".subcategory_data").css({'text-align': 'center'});
                        }
                        subcategory_has_click = 0;
                    },
                    error       : function(data){
                        swal("Warning", "There must be a problem with the request ! Please check again", "warning");
                    }
                });

                jQuery.ajax({
                    url         : "{{ route('show_menu') }}",
                    method      : "POST",
                    data        : {search_data : null},
                    dataType    : "html",
                    beforeSend  : function(){
                        $('#menu .menu').html('Menu');
                        $('#menu .menu').removeClass('remove_filter');
                    },
                    success     : function(data){
                        if(data != null && data != ""){
                            $(".menu_data").hide().html(data).fadeIn('100');
                            $(".menu_data").css({'text-align': 'left','padding':'1vh'});
                        }else{ 
                            $(".menu_data").html("No data");
                            $(".menu_data").css({'text-align': 'center','padding':'21vh',});
                        }
                        menu_has_click = 0;
                    },
                    error       : function(data){
                        swal("Warning", "There must be a problem with the request ! Please check again", "warning");
                    }
                });
            }
        });

        $('#subcategory_search_modal .search').on('click',function(){

            var search_data = $('#subcategory_search_modal .search_data').val();
            jQuery.ajax({
                url         : "{{ route('show_subcategory') }}",
                method      : "POST",
                data        : {search_data : search_data},
                dataType    : "html",
                beforeSend  : function(){
                    $('#subcategory .subcategory').html('Remove Filter');
                    $('#subcategory .subcategory').addClass('remove_filter');
                },
                success     : function(data){
                    if(data != null && data != ""){
                        $('.subcategory_data').html(data).fadeIn('100');
                        $(".subcategory_data").css({'text-align': 'left','padding':'0'}); 
                    }else{ 
                        $(".subcategory_data").html("No data");
                        $(".subcategory_data").css({'text-align': 'center','padding':'3vw'});
                    }
                    subcategory_has_click = 1;
                },
                error       : function(data){
                    swal("Warning", "There must be a problem with the request ! Please check again", "warning");
                }
            });
        });

        $('body').on('click','.subcategory_code',function(){
            var subcategory_id = $(this).attr('data-val');
            jQuery.ajax({
                url         : "{{ route('show_menu_by_category_subcategory') }}",
                type        : "POST",
                data        : { subcategory_id:subcategory_id },
                dataType    : "html",
                beforeSend  : function(data){
                    $(".subcategory_item ").removeClass("active");
                    $(".subcategory_item.item-" + subcategory_id).addClass("active");

                    $('#subcategory .subcategory').html('Remove Filter');
                    $('#subcategory .subcategory').addClass('remove_filter');
                },
                success     : function(data){
                    if(data != null && data != ""){
                        $(".menu_data").hide().html(data).fadeIn('100');
                        $(".menu_data").css({'text-align': 'left','padding':'1vh'});
                    }else{ 
                        $(".menu_data").html("No data");
                        $(".menu_data").css({'text-align': 'center','padding':'21vh',});
                    }
                },
                error       : function(data){
                    swal("Warning", "There must be a problem with the request ! Please check again", "warning");
                }
            });
            subcategory_has_click = 1;
        });
    });
</script>


<!-- ======================== Search Menu Function ==================== -->
<script type="text/javascript">
    $(function(){
        var menu_has_click = 0;
        $('body').on('click','#menu',function(e){
            if(menu_has_click == 0){
                $('#menu_search_modal').modal('show');
            }else{
                jQuery.ajax({
                    url         : "{{ route('show_menu') }}",
                    method      : "POST",
                    data        : {search_data : null},
                    dataType    : "html",
                    beforeSend  : function(){
                        $('#menu .menu').html('Menu');
                        $('#menu .menu').removeClass('remove_filter');
                    },
                    success     : function(data){
                        if(data != null && data != ""){
                            $(".menu_data").hide().html(data).fadeIn('100');
                            $(".menu_data").css({'text-align': 'left','padding':'1vh'});
                        }else{ 
                            $(".menu_data").html("No data");
                            $(".menu_data").css({'text-align': 'center','padding':'21vh'});
                        }
                        menu_has_click = 0;
                    },
                    error       : function(data){
                        swal("Warning", "There must be a problem with the request ! Please check again", "warning");
                    }

                });
            }

        });

        $('#menu_search_modal .search').on('click',function(){

            var search_data = $('#menu_search_modal .search_data').val();
            jQuery.ajax({
                url         : "{{ route('show_menu') }}",
                method      : "POST",
                data        : {search_data : search_data},
                dataType    : "html",
                beforeSend  : function(){
                    $('#menu .menu').html('Remove Filter');
                    $('#menu .menu').addClass('remove_filter');
                },
                success     : function(data){
                    if(data != null && data != ""){
                        $(".menu_data").hide().html(data).fadeIn('100');
                        $(".menu_data").css({'text-align': 'left','padding':'1vh'});
                    }else{ 
                        $(".menu_data").html("No data");
                        $(".menu_data").css({'text-align': 'center','padding':'21vh'});
                    }
                    menu_has_click = 1;
                },
                error       : function(data){
                    swal("Warning", "There must be a problem with the request ! Please check again", "warning");
                }

            });
        });
    });
</script>

<!-- ======================== Add Menu to list (No size or Addon) ==================== -->
<script type="text/javascript">
        var order_total = 0;
        $(function() {
            var addit = true;
            $('body').on('click','.menu_code',function(e){

            if(include_discount == 1){
                var real_total = $('#order_total_data_no_discount').text();
                $('#order_total_data').text(real_total);
                include_discount = 0;
                $('.discount_result').text('').hide();
            }
            var menu_id         =   $(this).attr('data-val');
            var menu_name       =   $(this).attr('data-name');
            var menu_amount     =   $(this).attr('data-amount');
            var menu_size       =   $(this).attr('data-size');
            var menu_material   =   $(this).attr('data-material');
            $('#table_order_data').attr('data-new','1');
            enable_and_disable_create_new();
            
            if(menu_size >= 1){
                jQuery.ajax({
                   url      : "{{ route('menu_data') }}",
                   method   : "POST",
                   data     : { menu_id : menu_id , _token:"{{Session::token()}}"},
                   dataType : "html",
                   beforeSend :function(data){
                        $('#menu_data_modal').modal('show');
                        $('#menu_data_modal').css({'top':'0vh'});
                        $('#menu_data_modal .modal-header').css({'background':'#3c5a99','color':'#fff'});
                        $('#menu_data_modal .modal-header').html(menu_name);
                        $('#menu_data_modal .modal-header').attr('data-id',menu_id);
                        $('#menu_data_modal .modal-header').attr('data-amount',menu_amount);
                        $('#menu_data_modal .modal-header').attr('data-size',0);
                        $('#menu_data_modal .modal-header').attr('data-addon',0);
                        $('#menu_data_modal .modal-body').html("Loading...");
                        $('#menu_data_modal .modal-header').attr('data-size', 1);
                    },
                    success  : function(data){
                        $('#menu_data_modal .modal-body').html(data);
                    },
                    error    : function(data){
                        swal("Warning", "There must be a problem with the request ! Please check again", "warning");
                    }
                });
            }else if(menu_material >=1){
                jQuery.ajax({
                    url      : "{{ route('menu_data_with_material') }}",
                    method   : "POST",
                    data     : { menu_id : menu_id , _token:"{{Session::token()}}"},
                    dataType : "html",
                    beforeSend :function(data){
                        $('#menu_data_modal').modal('show');
                        $('#menu_data_modal').css({'top':'0vh'});
                        $('#menu_data_modal .modal-header').css({'background':'#3c5a99','color':'#fff'});
                        $('#menu_data_modal .modal-header').html(menu_name);
                        $('#menu_data_modal .modal-header').attr('data-id',menu_id);
                        $('#menu_data_modal .modal-header').attr('data-amount',menu_amount);
                        $('#menu_data_modal .modal-header').attr('data-size',0);
                        $('#menu_data_modal .modal-header').attr('data-addon',0);
                        $('#menu_data_modal .modal-body').html("Loading...");
                    },
                    success  : function(data){
                        $('#menu_data_modal .modal-body').html(data);
                    },
                    error    : function(data){
                        swal("Warning", "There must be a problem with the request ! Please check again", "warning");
                    }
                });

            }else{
                var menu_id     = $(this).attr('data-val');
                var menu_name   = $(this).attr('data-name');
                var qty         = 1;
                var menu_amount = Number($(this).attr('data-amount'));
                order_total     += qty * menu_amount;
                var hasCheck    = $('#storage').text();
                var mainTotal   = parseFloat($("#order_total_data").text());


                $("#table_order_data").each(function(){  
                    $('.m_id').filter(function(){
                        if($(this).text() === menu_id){
                            addit = false;
                        }
                    });       
                });
                

                if(addit){
                    // add to list

                    $("#table_order_data").append(
                        "<tr class='menu_data_in_table'>"                                                                                  
                        +   "<td class='m_id' data-val="+ menu_id +" style='display:none;'>" + menu_id + "</td>"
                        +   "<td class='m_name' data-val="+ menu_id +" data-name='"+menu_name+"' data-print='0' data-qty='0'>" + menu_name
                        +   "<span class='m_size' style='display:none;'><br/></span>"
                        +     "<span class='m_size_name'></span></td>"

                        +       "<td class='m_qty'> <span class='set_minus' data-val='1'>"
                        +           "<i class='glyphicon glyphicon-minus'></i></span>"                             
                        +           "<span class='m_qty_data'>" + qty + "</span>"                                   
                        +           "<span class='set_plus' data-val='1'>"                                          
                        +           "<i class='glyphicon glyphicon-plus'></i></span></td>"                          

                        +       "<td class='m_amount'>" + menu_amount.toFixed(2) +"</td>"                           
                        +       "<td class='m_subamount'>" + (qty* menu_amount).toFixed(2) + "</td>"                 
                        +       "<td><span class='item_note' style='display:none'></span>"
                        +       "<span class='remove-this' data-id='' title='Remove'><i class='glyphicon glyphicon-remove'></i></span></td>" 
                        +       "</tr>");

                    $('.m_name').append(' ');

                    mainTotal += parseFloat($("[data-val="+menu_id+"]").nextAll(".m_subamount").text());


                    $("#order_total_data").html(mainTotal.toFixed(2));
                    // show total calculation with tax
                    var full_total_popup = mainTotal;
                    var tax_popup        = "{{$tax->company_sale_tax}}";
                    var sale_tax_popup   = full_total_popup * (tax_popup/100);
                    var total_popup      = full_total_popup + sale_tax_popup; // with discount and tax
                    $('.order_total_tax').val(total_popup);

                    // success_toast('Successfully Added',menu_name + ' has been added to the list','success',1000,'top-left','#004040');


                }else{
                    // Duplicate Item -> Update Quantity
                    var qtyData         =  Number($("[data-val="+menu_id+"]").next().find('.m_qty_data').text());
                    if(qtyData > 1){
                        mainTotal       += parseFloat($("[data-val="+menu_id+"]").nextAll(".m_amount").text());
                    }else{
                        mainTotal       += parseFloat($("[data-val="+menu_id+"]").nextAll(".m_subamount").text());
                    }

                    $("#order_total_data").html(mainTotal.toFixed(2));


                    var menuqty         = Number($(".menu_data_in_table [data-val="+menu_id+"]").next().find(".m_qty_data").text()) + parseInt(1);
                    var amount          = parseFloat($(".menu_data_in_table [data-val="+menu_id+"]").nextAll(".m_amount").text());
                    var menusubtotal    = amount * menuqty;

                    $(".menu_data_in_table .m_name[data-val="+menu_id+"]").attr('data-print','0');
                    $(".menu_data_in_table [data-val="+menu_id+"]").next().find('.m_qty_data').text(menuqty);
                    $(".menu_data_in_table [data-val="+menu_id+"]").nextAll(".m_subamount").text(menusubtotal.toFixed(2));

                    var full_total_popup = mainTotal;
                    var tax_popup        = "{{$tax->company_sale_tax}}";
                    var sale_tax_popup   = full_total_popup * (tax_popup/100);
                    var total_popup      = full_total_popup + sale_tax_popup; // with discount and tax
                    $('.order_total_tax').val(total_popup);

                    addit = true;
                    // success_toast('Successfully Added',menu_name + ' has been added 1 more to the list','success',1000,'top-left','#004040');
                }
            }
        });
});

</script>

<!-- ======================== Add Menu to list (With Size) ==================== -->
<script>
    $(function(){
        $('body').on('click','.menu_size',function(){

            $('.menu_size').removeClass('active');
            $(this).addClass('active');
            $(this).parent().parent().parent().prev('.modal-header').attr('data-size','1');
        });     
    });
</script>

<!-- ======================== Add Menu to list (With Material) ==================== -->
<script>
    $(function(){
        var tick = false;
        $('body').on('click','.material_item',function(e){
            $(this).find(".material_tick").toggleClass("hidden");
            var checkBoxes = $(this).find("input[type=checkbox]");
            checkBoxes.prop("checked", !checkBoxes.prop("checked"));
                if(checkBoxes.prop("checked") == true){
                    $(this).attr('data-tick','1');
                }else{
                    $(this).attr('data-tick','0');
                }
            
                var i = 0;
                $('.material_item[data-tick="1"]').each(function(e){
                    i += 1;
                });

                if(i >= 1){
                    $(this).parent().parent().prev('.modal-header').attr('data-addon','1');
                }else{
                    $(this).parent().parent().prev('.modal-header').attr('data-addon','0');
                }
            
        });
    });
</script>


<!-- ======================== Add Menu to list (Have Size + Addon) But did not select both ==================== -->
<script>
    $(function(){
        $('body').on('click','.materialadd',function(){

            var menu_size               = $(this).parent().prev().prev('.modal-header').attr('data-size');
            var menu_addon              = $(this).parent().prev().prev('.modal-header').attr('data-addon');
            var qty                     = 1;
            var mainTotal               = parseFloat($("#order_total_data").text());
            var itemNote                = $('.item_note_data').val(); 
            var m_note_id               = $(this).parent().prev().find('.item_note_data').attr('data-size');

            $('#table_order_data').attr('data-new','1');
            enable_and_disable_create_new();
            // if no size or no addon selected
            if(menu_size == 0 && menu_addon == 0){
                var addit                   = true;
                    //menu data
                    var menu_id                 = $(this).parent().prev().prev('.modal-header').attr('data-id');
                    var menu_name               = $(this).parent().prev().prev('.modal-header').text();
                    var menu_amount             = Number($(this).parent().prev().prev('.modal-header').attr('data-amount'));
                    $("#table_order_data").each(function(){  
                        $('.m_id').filter(function(){ if($(this).text() === menu_id){ addit = false; } }); 
                    });

                    if(addit){
                        $("#table_order_data").append(
                            "<tr class='menu_data_in_table'>"                                                                                  
                            +       "<td class='m_id' data-val="+ menu_id +" style='display:none;' data-main='1'>" + menu_id + "</td>"
                            +       "<td class='m_name' data-val="+ menu_id +" data-name='"+menu_name+"' data-print='0' data-qty='0' >" + menu_name

                            +       "<td class='m_qty'> <span class='set_minus' data-val='1'>"
                            +           "<i class='glyphicon glyphicon-minus'></i></span>"                             
                            +           "<span class='m_qty_data'>" + qty + "</span>"                                   
                            +           "<span class='set_plus' data-val='1'>"                                          
                            +           "<i class='glyphicon glyphicon-plus'></i></span></td>"                          

                            +       "<td class='m_amount'>" + menu_amount.toFixed(2) +"</td>"                           
                            +       "<td class='m_subamount'>" + (qty*menu_amount).toFixed(2) + "</td>"                 
                            +       "<td><span class='item_note' style='display:none'>" + itemNote + "</span>"
                            +       "<span class='remove-this' data-id='' title='Remove'><i class='glyphicon glyphicon-remove'></i></span></td>" 

                            +   "</tr>");

                        mainTotal += parseFloat($(".m_id[data-val="+menu_id+"][data-main=1]").nextAll(".m_subamount").text());
                        $("#order_total_data").html(mainTotal.toFixed(2));
                        // success_toast('Successfully Added',menu_name + ' has been added to the list','success',1000,'top-left','#004040'); 
                    }else{

                        var qtyData         =  Number($(".m_id[data-val="+menu_id+"][data-main=1]").nextAll('.m_qty').children('.m_qty_data').text());                  
                        if(qtyData > 1){
                            mainTotal       += parseFloat($(".m_id[data-val="+menu_id+"][data-main=1]").nextAll(".m_amount").text());
                        }else{
                            mainTotal       += parseFloat($(".m_id[data-val="+menu_id+"][data-main=1]").nextAll(".m_subamount").text());
                        }

                        $("#order_total_data").html(mainTotal.toFixed(2));
                        var menuqty         = Number($(".m_id[data-val="+menu_id+"][data-main=1]").nextAll('.m_qty').children(".m_qty_data").text()) + parseInt(1);
                        var amount          = parseFloat($(".m_id[data-val="+menu_id+"][data-main=1]").nextAll(".m_amount").text());
                        var menusubtotal    = amount * menuqty;

                        $(".m_name[data-val="+menu_id+"][data-main=1]").attr('data-print','0');
                        $(".m_id[data-val="+menu_id+"][data-main=1]").nextAll('.m_qty').children('.m_qty_data').text(menuqty);
                        $(".m_id[data-val="+menu_id+"][data-main=1]").nextAll('.m_subamount').text(menusubtotal.toFixed(2));
                    } 

                }else if(menu_size == 1 && menu_addon == 0){
                    var addit                   = true;
                    var menu_id                 = $(this).parent().prevAll('.modal-body').find('.menu_size.active').find('.menu_item_code').attr('data-val');
                    var menu_name               = $(this).parent().prevAll('.modal-body').find('.menu_size.active').find('.menu_item_code').attr('data-name');
                    var menu_size_id            = $(this).parent().prevAll('.modal-body').find('.menu_size.active').find('.menu_item_code').attr('data-size');
                    var menu_size_name          = $(this).parent().prevAll('.modal-body').find('.menu_size.active').find('.menu_item_code').attr('data-size-name');
                    var menu_amount             = Number($(this).parent().prevAll('.modal-body').find('.menu_size.active').find('.menu_item_code').attr('data-amount'));


                    $("#table_order_data").each(function(){  
                        $('.m_size').filter(function(){ if($(this).text() === menu_size_id){ addit = false;}}); 
                    });


                    if(addit){
                        $("#table_order_data").append(
                            "<tr class='menu_data_in_table'>"                                                                                  
                            +       "<td class='m_id' data-val="+ menu_id +" style='display:none;'>" + menu_id + "</td>"
                            +       "<td class='m_name' data-val="+ menu_id +" data-name='"+menu_name+"' data-print='0' data-qty='0' >" + menu_name
                            +           "<br/> <span class='m_size' data-val="+ menu_size_id +" style='display:none;' data-main='0'>" + menu_size_id + "</span>"
                            +           " <span class='m_size_name' data-size="+menu_size_id+">(" + menu_size_name + ")</span> </td>"

                            +       "<td class='m_qty'> <span class='set_minus' data-val='1'>"
                            +           "<i class='glyphicon glyphicon-minus'></i></span>"                             
                            +           "<span class='m_qty_data'>" + qty + "</span>"                                   
                            +           "<span class='set_plus' data-val='1'>"                                          
                            +           "<i class='glyphicon glyphicon-plus'></i></span></td>"                          

                            +       "<td class='m_amount'>" + menu_amount.toFixed(2) +"</td>"                           
                            +       "<td class='m_subamount'>" + (qty*menu_amount).toFixed(2) + "</td>"                 
                            +       "<td><span class='item_note' style='display:none'>" + itemNote + "</span>"
                            +       "<span class='remove-this' data-id='' title='Remove'><i class='glyphicon glyphicon-remove'></i></span></td>" 

                            +   "</tr>");

                        mainTotal += parseFloat($(".m_size[data-val="+menu_size_id+"][data-main=0]").parent().nextAll(".m_subamount").text());
                        $("#order_total_data").html(mainTotal.toFixed(2));
                        // success_toast('Successfully Added',menu_name + ' has been added to the list','success',1000,'top-left','#004040'); 

                    }else{
                        var qtyData         =  Number($(".m_size[data-val="+menu_size_id+"][data-main=0]").parent().nextAll('.m_qty').children('.m_qty_data').text()); 
                        
                        if(qtyData > 1){
                            mainTotal       += parseFloat($(".m_size[data-val="+menu_size_id+"][data-main=0]").parent().nextAll(".m_amount").text());
                        }else{
                            mainTotal       += parseFloat($(".m_size[data-val="+menu_size_id+"][data-main=0]").parent().nextAll(".m_subamount").text());
                        }

                        $("#order_total_data").html(mainTotal.toFixed(2));
                        var menuqty         = Number($(".m_size[data-val="+menu_size_id+"][data-main=0]").parent().nextAll('.m_qty').children(".m_qty_data").text()) + parseInt(1);
                        var amount          = parseFloat($(".m_size[data-val="+menu_size_id+"][data-main=0]").parent().nextAll(".m_amount").text());
                        var menusubtotal    = amount * menuqty;

                        $(".m_size[data-val="+menu_size_id+"][data-main=0]").parent().attr('data-print','0');
                        $(".m_size[data-val="+menu_size_id+"][data-main=0]").parent().nextAll('.m_qty').children('.m_qty_data').text(menuqty);
                        $(".m_size[data-val="+menu_size_id+"][data-main=0]").parent().nextAll('.m_subamount').text(menusubtotal.toFixed(2));
                    }

                }else if(menu_size == 0 && menu_addon == 1){
                    var addit = true;
                    $(".material_item[data-tick=1]").each(function() {
                        var menu_id                 = $(this).parent().parent().prev('.modal-header').attr('data-id');
                        var menu_name               = $(this).parent().parent().prev('.modal-header').text();
                        var menu_amount             = Number($(this).parent().parent().prev('.modal-header').attr('data-amount'));
                        var m_id           = $(this).find('input[type=checkbox]:checked').attr('menu-id');
                        var material_id    = $(this).find('.material_code').attr('data-val');
                        var material_name  = $(this).find('.material_code').attr('data-name');
                        var menu_size      = $(this).find('input[type=checkbox]:checked').attr('data-size');
                        var material_price = Number($(this).find('.material_code').attr('data-amount'));


                        $("#table_order_data").append(
                            "<tr class='menu_data_in_table'>"                                                                                  
                            +       "<td class='m_id' data-val="+ menu_id +" style='display:none;' data-main='1'>" + menu_id + "</td>"
                            +       "<td class='m_name' data-val="+ menu_id +" data-name='"+menu_name+"' data-print='0' data-qty='0' >" + menu_name

                            +       "<td class='m_qty'> <span class='set_minus' data-val='1'>"
                            +           "<i class='glyphicon glyphicon-minus'></i></span>"                             
                            +           "<span class='m_qty_data'>" + qty + "</span>"                                   
                            +           "<span class='set_plus' data-val='1'>"                                          
                            +           "<i class='glyphicon glyphicon-plus'></i></span></td>"                          

                            +       "<td class='m_amount'>" + menu_amount.toFixed(2) +"</td>"                           
                            +       "<td class='m_subamount'>" + (qty*menu_amount).toFixed(2) + "</td>"                 
                            +       "<td><span class='item_note' style='display:none'>" + itemNote + "</span>"
                            +       "<span class='remove-this' data-id='' title='Remove'><i class='glyphicon glyphicon-remove'></i></span></td>" 

                            +   "</tr>");

                        mainTotal += parseFloat($(".m_id[data-val="+menu_id+"][data-main=1]").nextAll(".m_subamount").text());
                        $("#order_total_data").html(mainTotal.toFixed(2));
                        // success_toast('Successfully Added',menu_name + ' has been added to the list','success',1000,'top-left','#004040'); 

                            if(addit){
                                $("#table_order_data").append(
                                    "<tr class='material_data_add material_with_price' data-parent="+ m_id +" data-size="+ menu_size  +">" 
                                    +   "<td class='menu_id' material-val="+ m_id +" style='display:none;'>" + m_id +"</td>" 
                                    +   "<td class='material_id' material-val="+ material_id +" style='display:none;'>" + material_id +"</td>" 
                                    +   "<td style='width:25vh; color:#00afe1; font-size:1.4vh;' class='material_name' data-val="+ material_id +">" 
                                    + material_name + "</td>" 
                                    +   "<td class='m_qty'> <span class='set_minus' data-val='1'>"
                                    +   "<i class='glyphicon glyphicon-minus'></i></span>"
                                    +   "<span class='m_qty_data'>" + qty + "</span>"
                                    +   "<span class='set_plus' data-val='1'>"
                                    +   "<i class='glyphicon glyphicon-plus'></i></span></td>"

                                    +   "<td class='m_amount'>" + material_price.toFixed(2) +"</td>"
                                    +   "<td class='m_subamount'>" + (qty*material_price).toFixed(2) + "</td>"
                                    +   "<td><span class='item_note' style='display:none'></span>"
                                    +   "<span class='remove-this' data-id='' title='Remove'><i class='glyphicon glyphicon-remove'></i></span></td>" 

                                    +"</tr>");

                                mainTotal += parseFloat($(".menu_id[material-val="+m_id+"]").nextAll(".m_subamount").text());
                                $("#order_total_data").html(mainTotal.toFixed(2));
                                // success_toast('Successfully Added',menu_name + ' has been added to the list','success',1000,'top-left','#004040');
                            }

                        });

                }else if(menu_size == 1 && menu_addon == 1){
                    var addit = true;
                    var menu_id                 = $(this).parent().prevAll('.modal-body').find('.menu_size.active').find('.menu_item_code').attr('data-val');
                    var menu_name               = $(this).parent().prevAll('.modal-body').find('.menu_size.active').find('.menu_item_code').attr('data-name');
                    var menu_size_id            = $(this).parent().prevAll('.modal-body').find('.menu_size.active').find('.menu_item_code').attr('data-size');
                    var menu_size_name          = $(this).parent().prevAll('.modal-body').find('.menu_size.active').find('.menu_item_code').attr('data-size-name');
                    var menu_amount             = Number($(this).parent().prevAll('.modal-body').find('.menu_size.active').find('.menu_item_code').attr('data-amount'));


                    $("#table_order_data").append(
                        "<tr class='menu_data_in_table'>"                                                                                  
                        +       "<td class='m_id' data-val="+ menu_id +" style='display:none;'>" + menu_id + "</td>"
                        +       "<td class='m_name' data-val="+ menu_id +" data-name='"+menu_name+"' data-print='0' data-qty='0' >" + menu_name
                        +           "<br/> <span class='m_size' data-val="+ menu_size_id +" style='display:none;' data-main='0'>" + menu_size_id + "</span>"
                        +           " <span class='m_size_name' data-size="+menu_size_id+">(" + menu_size_name + ")</span> </td>"

                        +       "<td class='m_qty'> <span class='set_minus' data-val='1'>"
                        +           "<i class='glyphicon glyphicon-minus'></i></span>"                             
                        +           "<span class='m_qty_data'>" + qty + "</span>"                                   
                        +           "<span class='set_plus' data-val='1'>"                                          
                        +           "<i class='glyphicon glyphicon-plus'></i></span></td>"                          

                        +       "<td class='m_amount'>" + menu_amount.toFixed(2) +"</td>"                           
                        +       "<td class='m_subamount'>" + (qty*menu_amount).toFixed(2) + "</td>"                 
                        +       "<td><span class='item_note' style='display:none'>" + itemNote + "</span>"
                        +       "<span class='remove-this' data-id='' title='Remove'><i class='glyphicon glyphicon-remove'></i></span></td>" 

                        +   "</tr>");

                    mainTotal += parseFloat($(".m_id[data-val="+menu_id+"]").nextAll(".m_subamount").text());
                    $("#order_total_data").html(mainTotal.toFixed(2));
                    // success_toast('Successfully Added',menu_name + ' has been added to the list','success',1000,'top-left','#004040'); 


                    $(".material_item[data-tick=1]").each(function() {
                        var m_id                    = $(this).find('input[type=checkbox]:checked').attr('menu-id');
                        var material_id             = $(this).find('.material_code').attr('data-val');
                        var material_name           = $(this).find('.material_code').attr('data-name');
                        var menu_size               = $(this).parent().prev('.item_detail').find('.menu_detail').find('.menu_size.active').find('.menu_item_code').attr('data-size');
                        // $(this).parent().prev('.item_detail').find('.menu_detail').find('.menu_size.active').find('menu_item_code').attr('data-size');
                        var material_price          = Number($(this).find('.material_code').attr('data-amount'));
                        if(addit){
                            $("#table_order_data").append(
                                "<tr class='material_data_add material_with_price' data-parent="+ m_id +" data-size="+ menu_size  +">" 
                                +   "<td class='menu_id' material-val="+ m_id +" style='display:none;'>" + m_id +"</td>" 
                                +   "<td class='material_id' material-val="+ material_id +" style='display:none;'>" + material_id +"</td>" 
                                +   "<td style='width:25vh; color:#00afe1; font-size:1.4vh;' class='material_name' data-val="+ material_id +">" 
                                + material_name + "</td>" 
                                +   "<td class='m_qty'> <span class='set_minus' data-val='1'>"
                                +   "<i class='glyphicon glyphicon-minus'></i></span>"
                                +   "<span class='m_qty_data'>" + qty + "</span>"
                                +   "<span class='set_plus' data-val='1'>"
                                +   "<i class='glyphicon glyphicon-plus'></i></span></td>"

                                +   "<td class='m_amount'>" + material_price.toFixed(2) +"</td>"
                                +   "<td class='m_subamount'>" + (qty*material_price).toFixed(2) + "</td>"
                                +   "<td><span class='item_note' style='display:none'></span>"
                                +   "<span class='remove-this' data-id='' title='Remove'><i class='glyphicon glyphicon-remove'></i></span></td>" 

                                +"</tr>");

                            mainTotal += parseFloat($(".menu_id[material-val="+m_id+"]").nextAll(".m_subamount").text());
                            $("#order_total_data").html(mainTotal.toFixed(2));
                            // success_toast('Successfully Added',menu_name + ' has been added to the list','success',1000,'top-left','#004040');
                        }

                    });

                }else{
                    swal("Warning", "There must be a problem with the request ! Please check again", "warning");
                }   

                $(this).parent().prev().prev('.modal-header').attr('data-size','0');
                $(this).parent().prev().prev('.modal-header').attr('data-addon','0');     

                });
}); 
</script>

<script>
    // Show material when there is a material in a menu
    $(function(){
        $('body').on('click','tbody#table_order_data td.m_name',function(e){
            var menu_id     = $(this).attr('data-val');
            var menu_size   = $(this).find('.m_size').attr('data-val');
            var menu_note   = $(this).parent().find('.item_note').text();
            var menu_name   = $(this).text();
            $('#menu_data_modal').modal('show');
            $('#menu_data_modal .materialnoteadd').css('display','inline-block')
            // $("#menu_data_modal .modal-title").html(menu_name);
            jQuery.ajax({
                url     : "{{ route('show_material_for_menu') }}",
                type    : "POST",
                data    : { menu_id:menu_id ,menu_size : menu_size, menu_note: menu_note, _token:"{{Session::token()}}" },
                dataType: "html",
                beforeSend: function(){
                    $("#menu_list_data_modal .modal-body").html("Loading...");
                },

                success : function(data){
                    if(data != null && data != ''){
                        $('#menu_list_data_modal .modal-body').hide().html(data).fadeIn('100');
                    }else{
                                // $('#menu_data_modal .modal-body').html("No Material");
                            }
                        }
                    });



        });
    });




    // Plus
    $(document).on('click','.set_plus',function(){
        var setPlus         = $(this).attr('data-val');
        var setmenuMore     = $(this).prev('.m_qty_data').text();
        var sumPlus         = parseFloat(setmenuMore) + parseFloat(setPlus);
        var mainTotal       = Number($("#order_total_data").text());
        var old_order       = $('#old_order_table').val();


        if (sumPlus == 0) {
            $(this).prev('.m_qty_data').html(1);
        }

        if (sumPlus >= 1) {
            $('#table_order_data').attr('data-new','1');
            enable_and_disable_create_new();
            $(this).parent().prev('.m_name').attr('data-print','0');
            $(this).prev('.m_qty_data').html(sumPlus);
            var amount              = parseFloat($(this).closest('tr').children('.m_amount').text());
            var amount_for_total    = parseFloat($(this).closest('tr').children('.m_amount').text());
            amount                  = sumPlus * amount;
            $(this).closest('tr').children('.m_subamount').html(amount.toFixed(2));
            mainTotal               += amount_for_total;
            $('#order_total_data').html(mainTotal.toFixed(2));
        }
    });
    // End Plus


    // Minus
    $(document).on('click','.set_minus',function(){
        var setMinus=$(this).attr('data-val');
        var setmenuMore=$(this).next('.m_qty_data').text();
        var sumMinus=parseFloat(setmenuMore) - parseFloat(setMinus);
        var mainTotal = Number($("#order_total_data").text());
        if (sumMinus == 0) {
         $(this).next('.m_qty_data').html(1);
     }


     var old_order       = $('#old_order_table').val();
     if(old_order == null){
        if (sumMinus >= 1) {
         $(this).next('.m_qty_data').html(sumMinus);
         var amount           = parseFloat($(this).closest('tr').children('.m_amount').text());
         var amount_for_total = parseFloat($(this).closest('tr').children('.m_amount').text());
         amount               = sumMinus * amount;
         $(this).closest('tr').children('.m_subamount').html(amount.toFixed(2));
         mainTotal            -= amount_for_total;
         $('#order_total_data').html(mainTotal.toFixed(2));
     }
 }else{
    var current_qty = $(this).parent().prev('.m_name').attr('data-qty');
    if(sumMinus >= current_qty && sumMinus >= 1) {
        $(this).next('.m_qty_data').html(sumMinus);
        var amount           = parseFloat($(this).closest('tr').children('.m_amount').text());
        var amount_for_total = parseFloat($(this).closest('tr').children('.m_amount').text());
        amount               = sumMinus * amount;
        $(this).closest('tr').children('.m_subamount').html(amount.toFixed(2));
        mainTotal            -= amount_for_total;
        $('#order_total_data').html(mainTotal.toFixed(2));

        var new_status  = [];
        $("#table_order_data tr").each(function(){  
            var last_qty    = $(this).find('.m_name').data('qty');
            var new_qty     = $(this).find('.m_qty_data').text();
            
            if(new_qty == last_qty){
                new_status.push('0');
            }else{
                new_status.push('1');
            }

        });

        if($.inArray('1', new_status) == -1){

            $('#table_order_data').attr('data-new','0');
            enable_and_disable_create_new();
        }
        
    }

    
    }
})
    // End Minus


    // remove
    $(document).on('click','#table_order_data .remove-this',function(){

        var kk              = $(this).parents('tr').remove();
        var amount          = $(this).parents('tr').children('td:eq(4)').text();
        var total           = $('#order_total_data').text();
        var order_total1    = parseFloat(total) - parseFloat(amount);
        order_total         = parseFloat(total) - parseFloat(amount);
        $('#order_total_data').html(order_total1.toFixed(2));
        $('#table_order_data').attr('data-new','1');
        enable_and_disable_create_new();

    });

    $(document).on('click','#old_order_modal #table_order_data_old .remove-this',function(){
        var kk      =$(this).parents('tr').remove();

        if(isDelete == 1){
            var grand_total_after_delete = 0;
            $('#table_order_data_old .menu_data_in_table').each(function(){
                var price                   = '';
                price                       = $(this).children().next().next().next().next().find('.old_subtotal').text();
                grand_total_after_delete   += parseFloat(price);
            });
            $('#table_order_data .menu_data_in_table').each(function(){
                var price = '';
                price = $(this).children().next().next().next().next().text();
                grand_total_after_delete   += parseFloat(price);
            });
            $('#order_total_data').html(grand_total_after_delete.toFixed(2));
        }
    });

    


    $(document).on('click','.exit',function(){

        window.close();

    });


</script>



<!-- Broken Popup -->
<script type="text/javascript">
    $(function(){
        $('body').on('click','.broken',function(){
            $('#broken_modal').modal('show');
            $("#broken_modal").css("top","150px");
            $("#broken_modal .modal-header").css("background-color", "#3C5A99");
            $("#broken_modal .modal-header").css("color", "#fff");
        });

        $('body').on('click','.brokenadd',function(){
            var broken_info = $('.broken-info').val();
            var qty = 1;
            var broken_price = Number($('.brokencost').val());
            var mainTotal = Number($("#order_total_data").text());
            var total = broken_price + mainTotal;


            $("#table_order_data").append("<tr>" +
                "<td style='display:none'></td>" +
                "<td style='width:30vh;' class='broken_name'>" + broken_info + "</td>" +


                "<td class='m_qty' style='width:15%;'> <span class='set_minus' data-val='1'>" +
                "<i class='glyphicon glyphicon-minus'></i></span>" +
                "<span class='m_qty_data'>" + qty + "</span>" +
                "<span class='set_plus' data-val='1'>" +
                "<i class='glyphicon glyphicon-plus'></i></span></td>" +

                "<td class='m_amount'>" + broken_price.toFixed(2) +"</td>" +
                "<td class='m_subamount'>" + (qty*broken_price).toFixed(2) + "</td>" +
                "<td><span class='item_note' style='display:none'></span>" +
                "<span class='remove-this' data-id='' title='Remove'><i class='glyphicon glyphicon-remove'></i></span></td>" +

                "</tr>");
            $('#order_total').html("<span>$</span>" + "<span id='order_total_data'>" + total.toFixed(2) + "</span>");
            
        });
    });
</script>


<!-- Note Information -->
<script type="text/javascript">
    $(function(){
        $('body').on('click','.note',function(){
            var note_info = $('.note_info').val(); 
            if(note_info != ''){
                $('.noteremove').css('display','inline-block');
            }
            $('#note_modal').modal('show');
            $("#note_modal").css("top","150px");
            $("#note_modal .modal-header").css("background-color", "#3C5A99");
            $("#note_modal .modal-header").css("color", "#fff");
        });

        $('body').on('click','.noteadd',function(){
            var note = $('.note_info').val();
            if(note != ''){
                $('.note_title span').css('display','inline-block');
            }else{
                $('.note_title span').css('display','none');
            }
        });

        $('body').on('click','.noteremove',function(){
            $('.note_info').val('');
            $('.note_title span').css('display','none');
        });
    });
</script>


<!-- Order confirmation -->
<script type="text/javascript">
    var tax_amount_dollar;
    var tax_amount_all;
    var grand_total_dollar;
    var grand_total_all;
    var isDelete = 0;
    $(function(){



        $('#order_confirm_modal').on('shown.bs.modal', function () {
           $('#order_confirm_modal #table_order_data_confirm .m_name').removeClass('m_name').addClass('m_name_new');
           $('#confirm_order_deposit_data').focus();
           $('.discount_amount').html('<option value=0 selected="selected">Please select discount rate</option>');
           $('.discount_amount').attr("disabled",'disabled');
       });

        $('#order_confirm_modal').on('hidden.bs.modal', function () {
            $("#order_confirm_modal #table_order_confirm tbody#table_order_data_confirm").html('');
            $("#order_confirm_modal #discount_amount").html('0');   
            $('.promotion_pack_info').css('display','none');
            $('#order_confirm_modal .main').removeClass('active');
        });
        
        
        $('#old_order_modal').on('shown.bs.modal', function(){
            $('body').on('click','#old_order_modal #table_order_data_old .remove-this',function(){
                isDelete = 1;

            });
        });


        $('body').on('click','.save_data',function(){
            var tbody           = $("tbody#table_order_data");
            var tbody1          = $("tbody#table_order_data").html();
            var old_order       = $('#old_order_id_data').val();


            if (tbody.children().length == 0 && isDelete == 0 && old_order == null) {
                swal("Warning", "Please place your order menu first!", "warning");

            }else{



                $('#order_confirm_modal').modal('show');
                $("#order_confirm_modal").css("top","1vh");
                $("#order_confirm_modal .modal-header").css("background-color", "#3C5A99");
                $("#order_confirm_modal .modal-header").css("color", "#fff");
                

                
                $("#order_confirm_modal #table_order_confirm tbody#table_order_data_confirm").html(tbody1);
                $("#order_confirm_modal #table_order_confirm tbody#table_order_data_confirm .remove-this").parent().remove();
                $("#order_confirm_modal #table_order_confirm tbody#table_order_data_confirm").append('<div id="promotion_pack_free_item_data"></div>')



                // Sub Total in $ / Riel / Baht
                var order_total = parseFloat($('#order_total_data').text());
                $("#confirm_order_total_data").html(order_total);

                $('.paid_amount').each(function(){
                    var currency = $(this).attr('data-val');
                    var company_currency    = $('#company_currency').val();   
                    if(company_currency == currency){
                        $('#confirm_order_total_data_'+currency).html(order_total.toFixed(2));
                    }else{
                        var currency_from_return       = parseFloat($('#'+currency).attr('data-from'));
                        var currency_to_return         = parseFloat($('#'+currency).attr('data-to'));
                        var return_convert = (order_total * currency_to_return) / currency_from_return;
                        $('#confirm_order_total_data').html(order_total.toFixed(2));
                        $('#confirm_order_total_data_'+currency).html(return_convert.toFixed(0));
                    }



                });


                




                // show total calculation with tax
                var full_total_popup = parseFloat($('#confirm_order_total_data').text());
                var tax_popup        = parseFloat($('.tax_amount').text());
                var sale_tax_popup   = full_total_popup * (tax_popup/100);
                var total_popup      = full_total_popup + sale_tax_popup; // with discount and tax
                $('#confirm_order_total_data_complete').text(total_popup.toFixed(2));
                $('#confirm_remain_amount_data').text(total_popup.toFixed(2));
                $('#confirm_order_total_data_complete_hidden').text(total_popup.toFixed(2));


                // Show Tax in money ($, Riel / Baht)
                $('.tax_amount_money').html(sale_tax_popup.toFixed(2));
                $('#paid_amount').attr('data-tax',sale_tax_popup.toFixed(2));

                $('.paid_amount').each(function(){
                    var currency = $(this).attr('data-val');
                    var company_currency    = $('#company_currency').val();   
                    if(company_currency == currency){
                        $('.tax_amount_'+currency).html(sale_tax_popup.toFixed(2));

                        var currency_symbol           = $('#paid_amount_'+currency).attr('data-name');
                        tax_amount_dollar = currency_symbol + sale_tax_popup.toFixed(2); 

                        $(this).attr('data-tax',sale_tax_popup.toFixed(2));

                    }else{
                        var currency_from_return       = parseFloat($('#'+currency).attr('data-from'));
                        var currency_to_return         = parseFloat($('#'+currency).attr('data-to'));
                        var return_convert             = (sale_tax_popup * currency_to_return) / currency_from_return;
                        $('.tax_amount_money').html(tax_amount_dollar);
                        $('.tax_amount_'+currency).html(return_convert.toFixed(0));



                        var currency_symbol            = $('#paid_amount_'+currency).attr('data-name');
                        tax_amount_all                 = currency_symbol + return_convert.toFixed(0); 
                        $(this).attr('data-tax',return_convert);
                    }



                });



                // show Total calculation with tax ($, Riel, Baht)
                $('#paid_amount').attr('data-exchange',total_popup.toFixed(2));
                $('.paid_amount').each(function(){
                    var currency = $(this).attr('data-val');
                    var company_currency    = $('#company_currency').val();   
                    if(company_currency == currency){
                        $('#confirm_order_total_data_complete_hidden_'+currency).html(total_popup.toFixed(2));
                        $('#confirm_order_total_data_complete_'+currency).html(total_popup.toFixed(2));
                        $(this).attr("data-exchange",total_popup.toFixed(2));

                        var currency_symbol           = $('#paid_amount_'+currency).attr('data-name');
                        grand_total_dollar = currency_symbol + sale_tax_popup.toFixed(2); 


                    }else{
                        var currency_from_return       = parseFloat($('#'+currency).attr('data-from'));
                        var currency_to_return         = parseFloat($('#'+currency).attr('data-to'));
                        var return_convert             = (total_popup * currency_to_return) / currency_from_return;
                        $('#confirm_order_total_data_complete_hidden_'+currency).html(return_convert.toFixed(0));
                        $('#confirm_order_total_data_complete').html(grand_total_dollar);
                        $('#confirm_order_total_data_complete_'+currency).html(return_convert.toFixed(0));
                        $(this).attr('data-exchange',return_convert.toFixed(0));

                        var currency_symbol            = $('#paid_amount_'+currency).attr('data-name');
                        grand_total_all                = currency_symbol + return_convert.toFixed(0); 

                    }



                });
                
                
                // Show Remain after paid calculation
                var remain = parseFloat($('#confirm_order_total_data_complete_hidden').text());
                $('#confirm_remain_amount_data').html(remain.toFixed(2));
                $('.paid_amount').each(function(){
                    var currency = $(this).attr('data-val');
                    var company_currency    = $('#company_currency').val();   
                    if(company_currency == currency){
                        $('#confirm_remain_amount_data_'+currency).html(remain.toFixed(2));

                    }else{
                        var currency_from_return       = parseFloat($('#'+currency).attr('data-from'));
                        var currency_to_return         = parseFloat($('#'+currency).attr('data-to'));
                        var return_convert             = (remain * currency_to_return) / currency_from_return;


                    }



                });

                var grand_total = $('#confirm_order_total_data_complete').text();
                $('#grand_total_complete_data').val(grand_total);
                

                var old_order = $('#old_order_id_data').val();
                if(old_order != null){
                    $('.order_confirm').hide();
                    $('#order_confirm_modal .order_print').hide();

                }

                if($('#table_order_data_confirm').children('.menu_data_in_table').length >= 1){
                    $('.order_confirm').show();
                    $('#order_confirm_modal .order_print').show();
                }
                
                




            }


        });





});
</script>


<!-- promotion pack information -->
<script type="text/javascript">
    $(function(){
        $('body').on('change','.promotion_pack_data',function(){
            var promotion_pack_data = $('.promotion_pack_data').val();
            if(promotion_pack_data == 0){
                $('.promotion_pack_info').css('display','none');
                $('#promotion_pack_free_item_data').html('');
                var full_total          = parseFloat($('#confirm_order_total_data_complete_hidden').text());
                $('#confirm_order_total_data_complete').html(full_total.toFixed(2));

                $('#promotion_pack_info .modal-body').children().remove();

                var return_amount = 0;
                var total           = parseFloat($('#confirm_order_total_data_complete').text());
                var return_amount   = total;

                var company_currency    = $('#company_currency').val();
                $('.paid_amount').each(function(){
                    var currency = $(this).attr('data-val');
                    if(company_currency == currency){
                        $('#confirm_order_total_data_complete_'+currency).html(return_amount.toFixed(2));
                    }else{
                        var currency_from_return       = parseFloat($('#'+currency).attr('data-from'));
                        var currency_to_return         = parseFloat($('#'+currency).attr('data-to'));
                        var return_convert = (return_amount * currency_to_return) / currency_from_return;
                        $('#confirm_order_total_data_complete_'+currency).html(return_convert);

                    }
                });

            }else{
                $('.promotion_pack_info').css('display','inline-block');
                jQuery.ajax({
                    url         : "{{ route('get_promotion_pack_data_change') }}",
                    method     : "POST",
                    data        : { promotion_pack_id:promotion_pack_data, _token:"{{Session::token()}}" },
                    data_type   : 'html',
                    beforeSend  : function(data){
                        // $('#table_order_data_confirm').htl('Loading...');
                    },
                    success     : function(data){
                        $('#promotion_pack_free_item_data').hide().html(data).fadeIn('100');
                        
                    },
                    complete    : function(){
                        var promotion_discount_type                     = $('#promotion_pack_free_item_data #promotion_pack_discount_type').val();

                        if(promotion_discount_type == null){
                            var full_total          = parseFloat($('#confirm_order_total_data_complete_hidden').text());
                            $('#confirm_order_total_data_complete_hidden').html(full_total); 
                        }else if(promotion_discount_type == '%'){
                            var discount            = parseFloat($('#promotion_pack_free_item_data #promotion_pack_discount_amount').val());
                            var full_total          = parseFloat($('#confirm_order_total_data_complete_hidden').text());
                            var complete_total      = (full_total * discount)/100;
                            var complete_total_real = full_total - complete_total;
                            $('#confirm_order_total_data_complete').html(complete_total_real.toFixed(2)); 
                        }else if(promotion_discount_type == '$'){


                            var full_total = parseFloat($('#confirm_order_total_data').text());
                            var tax        = parseFloat($('.tax_amount').text());
                            var discount   = parseFloat($('#promotion_pack_free_item_data #promotion_pack_discount_amount').val());



                                            // Calculating the tax
                                            var sale_tax                    = full_total * (tax/100);
                                            var complete_total_all          = full_total + sale_tax;       // with discount and tax

                                            var complete_total = complete_total_all - discount;            // only discount (without TAX)


                                            if(complete_total_all < 0){
                                                $('#confirm_order_total_data_complete').text('0.00');   
                                            }else{
                                                $('#confirm_order_total_data_complete').text(complete_total.toFixed(2));   
                                            }

                                            // check remaining and return money when input discount after paid
                                            var return_amount = 0;
                                            var total           = parseFloat($('#confirm_order_total_data_complete').text());
                                            var return_amount   = total;

                                            var company_currency    = $('#company_currency').val();
                                            $('.paid_amount').each(function(){
                                                var currency = $(this).attr('data-val');
                                                if(company_currency == currency){
                                                    $('#confirm_order_total_data_complete_'+currency).html(return_amount.toFixed(2));
                                                }else{
                                                    var currency_from_return       = parseFloat($('#'+currency).attr('data-from'));
                                                    var currency_to_return         = parseFloat($('#'+currency).attr('data-to'));
                                                    var return_convert = (return_amount * currency_to_return) / currency_from_return;
                                                    $('#confirm_order_total_data_complete'+currency).html(return_amount.toFixed(2));
                                                    $('#confirm_order_total_data_complete_'+currency).html(return_convert);

                                                }
                                            });
                                        }
                                    }

                                });


}


});


$('body').on('click','.promotion_pack_info',function(){
    var promotion_pack_id = $('.promotion_pack_data').val();

    jQuery.ajax({
        url         : "{{ route('get_promotion_pack_data') }}",
        method     : "POST",
        data        : { promotion_pack_id:promotion_pack_id, _token:"{{Session::token()}}" },
        data_type   : 'html',
        beforeSend  : function(data){
            $('#promotion_pack_info .modal-body').html('Loading...');
        },
        success     : function(data){
            $('#promotion_pack_info .modal-body').hide().html(data).fadeIn('100');
        }


    });



    $('#promotion_pack_info').modal('show');
    $('#promotion_pack_info').css({'top':'0vh','left':'-1vh'});
    $('#promotion_pack_info').css('z-index','1052');
    $('.modal-backdrop').css('z-index','1051');

});


$('#promotion_pack_info').on('hidden.bs.modal', function () {
   $('.modal-backdrop').removeAttr('style');


});

});
</script>

<!-- Paid -->
<script type="text/javascript">
    $(function(){
        $('body').on('click','.paidPrint',function(){
            // Show Remain after paid calculation
            var total = $('.order_total_tax').val();

            // var tax   = $('.company_tax').text()*total/100;
            // var grand_total = parseFloat(total) + parseFloat(tax); original one
            // var grand_total = parseFloat(total) + parseFloat(tax);
            
            var grand_total = parseFloat(total);

            $('#grand_total_paid').html(parseFloat(grand_total).toFixed(2));
            $('#confirm_remain_amount_data').html(parseFloat(grand_total).toFixed(2));
            $('.paid_amount').each(function(){
                var currency = $(this).attr('data-val');
                var company_currency    = $('#company_currency').val();   
                if(company_currency == currency){
                    $('#grand_total_paid_'+currency).html(total.toFixed(2));

                }else{
                    var total = $('#order_total_data').text();
                    var currency_from_return       = parseFloat($('#'+currency).attr('data-from'));
                    var currency_to_return         = parseFloat($('#'+currency).attr('data-to'));
                    var return_convert = (grand_total * currency_to_return) / currency_from_return;
                    $('#grand_total_paid').html(grand_total.toFixed(2));
                    $('#grand_total_paid_'+currency).html(return_convert.toFixed(0));
                    $('#confirm_remain_amount_data_'+currency).html(return_convert.toFixed(0));

                }



            });
            $('#paid_info').css('z-index','1052');
            $('.modal-backdrop').css('z-index','1051');
            $('#paid_info #nav_pay_full_amount').show();
            $('#paid_info #nav_split_payment').hide();
            $('#paid_info .box_info').removeClass('active');
            $('#paid_info #pay_full_amount').addClass('active');

            $('#paid_info').modal('show');
        });


        $('#paid_info').on('hidden.bs.modal', function () {
           $('.modal-backdrop').removeAttr('style').fadeIn();
           $('.confirm_order_deposit_data').val('');
           $('.confirm_return').css("display",'none');


       });
    });
</script>


<!-- ====================== Save Order to json file ====================== -->
<script type="text/javascript">
    var test="test";
    $(function(){
        $('body').on('click','.order_confirm', function(){
            var order_type      = "{{$order_type}}";
            var type      = "{{ $type }}";
            var invoiceId      = "{{ $_GET["invoice_id"] }}";
            
            var company_id      = "{{$user->company_id}}";
            var dataStatus      = 0;
            var items           = [];
            var addons          = [];
            //item + size
            $('#table_order_data tr.menu_data_in_table').each(function(){
                var data = {
                    item_id         : $(this).find('td.m_id').text(),
                    item_name       : $(this).find('td.m_name').attr('data-name'),
                    item_qty        : $(this).find('td.m_name').attr('data-qty'),
                    item_amount     : $(this).find('td.m_amount').text(),
                    item_size       : $(this).find('td .m_size').text(),
                    print_status    : $(this).find('td.m_name').attr('data-print'),
                    order_type      : order_type,
                    addmore         : parseInt($(this).find('td .m_qty_data').text()) - parseInt($(this).find('td.m_name').attr('data-qty')),
                    item_size_name  : $(this).find('td .m_size_name').text(),
                    addons          : []
                }
                var menu_id     = $(this).find('td.m_id').text();
                var menu_size   = $(this).find('td .m_size_name').attr('data-size');
                
                
                //Material
                $(this).parent().find("tr.material_data_add[data-parent="+menu_id+"][data-size="+menu_size+"]").each(function(){
                    
                    var data_m = {
                        addon_id     : $(this).find('td.material_id').text(),
                        addon_name   : $(this).find('td.material_name').text(),
                        addon_qty    : $(this).find('td span.m_qty_data').text(),
                        addon_amount : $(this).find('td.m_amount').text()
                    }
                    data.addons.push(data_m);
                    // data.addons = addons;
                });
                items.push(data);

            });
            var table_code = $('.table_data').text();
            if (order_type == 'takeout') {
                table_code = 'takeout-' + Math.floor( Math.random() * 1000 ) + Date.now();
            }
            
            
            var order_request = {
                id : table_code,
                dataStatus : dataStatus,
                order_type : order_type,
                type : type,
                invoiceId : invoiceId,
                items : [],
            }

            order_request.items = items;

            //request change status for this specific table
            jQuery.ajax({
                url         : "{{ route('set_table_status') }}",
                method      : "POST",
                data        : {table_code: table_code, status : 0},
                success     : function(data){},
                error       : function(data){swal("Warning", "There must be a mistake, Please Check again!", "warning");}
            });

            var table_id = "{{$_GET['table_id']}}";

            //add to local storage
            jQuery.ajax({
                url         : "{{ route('request_order') }}",
                method      : "POST",
                data        : JSON.stringify(order_request),
                beforeSend  : function(datas){},
                success     : function(datas){
                    
                    var status = '';
                    //generate invoice
                    jQuery.ajax({
                        url         : "{{ route('printTicket') }}",
                        method      : "POST",
                        data        : {table_code: table_code, comid : company_id, order_type : order_type, type : type, invoiceId : invoiceId},
                        beforeSend  : function(data){},
                        success     : function(data){
                            var status = data.status

                            swal('Order Completed','Your request has been sent','success').then((value) => {
                            
                                if (order_type == 'takeout') {
                            
                                    var query_url = "?invoice_id=" + status.invoice_id + "&order_data=invoice&type=edit_invoice&order_type=takeout"
                                } else {

                                    query_url = "?table_id=" + table_id + "&table_name=" + table_code + "&order_data=old&order_type=dinein"

                                }
                                
                                window.location.href = ('{{route("cashier_update_order")}}' + query_url);
                            });
                        },
                        error       : function(data){
                            swal("Warning", "There must be a mistake, Please Check again!", "warning");
                        }
                    });

                    
                },
                error   : function(data){
                    swal("Warning", "There must be a mistake, Please Check again!", "warning");
                }
            });

        });    
});

</script>


<script>
    $(function(){
        $('body').on('click','.print_invoice',function(){
            var table_code        = $('.table_data').text();
            var discount_id = $("#discount_type_id").val();
            var newOrderTotal = $("#order_total_data").text();
            var invoice_id = {{ !empty(isset($_GET['invoice_id']))? $_GET['invoice_id'] : '0' }};
            var url           = location.href;
            var split_url     = url.split("/");
            var count         = split_url.length;
            var order_type      = "{{$order_type}}";  

            tax_dollar          = "{{$tax->company_sale_tax}}";
            g_total             = $('.order_total_tax').val();

            var items  = [];
            var addons = [];

            //item + size
            $('#table_order_data tr.menu_data_in_table').each(function(){
                var data = {
                    item_id         : $(this).find('td.m_id').text(),
                    item_name       : $(this).find('td.m_name').attr('data-name'),
                    item_qty        : $(this).find('td.m_name').attr('data-qty'),
                    item_amount     : $(this).find('td.m_amount').text(),
                    item_size       : $(this).find('td .m_size').text(),
                    order_type      : order_type,
                    item_size_name  : $(this).find('td .m_size_name').text(),
                    addons          : []
                }
                var menu_id = $(this).find('td.m_id').text();
                
                //Material
                $(this).parent().find("tr.material_data_add[data-parent="+menu_id+"]").each(function(){

                    var data_m = {
                        addon_id     : $(this).find('td.material_id').text(),
                        addon_name   : $(this).find('td.material_name').text(),
                        addon_qty    : $(this).find('td .m_qty_data').text(),
                        addon_amount : $(this).find('td.m_amount').text()
                    }
                    addons.push(data_m);
                    data.addons = addons;
                });
                
                items.push(data);
                
            });
            
            var table_id          = $('.table_id').text();
            var table_code        = $('.table_data').text();            
            var order_request     = {
                id      : table_code,
                items   : [],
            }

            order_request.items = items;

            jQuery.ajax({
                        url         : "{{ route('generate_invoice') }}",
                        method      : "POST",
                        data        :   {
                            table_code      : table_code, 
                            g_total         : newOrderTotal,
                            // g_dollar        : g_dollar,
                            // g_riel          : g_riel,
                            // g_baht          : g_baht,  // end of grand amount
                            table_id        : table_id,
                            invoice_id      : invoice_id,
                            order_type      : order_type,
                            discount_id     : discount_id

                        },
                        beforeSend  : function(data){
                        	var confirm2 = { title: 'Please Wait', text: 'Process to print ... .',showConfirmButton: false };
            				swal(confirm2);

                        },
                        success     : function(data){
                        	swal.close();
                        	swal('Print Completed','You have sucessfully print invoice of this table','success').then((value) => {
                           		
                        });
                },
                error       : function(data){}
            });

            // $(".print_invoice").prop('disabled',true); 
        });

});
</script>


<script>
    $(function(){
        $('body').on('click','.order_print',function(){
            var discount_id = $("#discount_type_id").val();
            var table_code    = $('.table_data').text();
            var newOrderTotal = $('#grand_total_paid').text();
            var invoiceId = "{{ !empty(isset($_GET['invoice_id']))? $_GET['invoice_id'] : '0' }}";
            var dataStatus    = 1;
            var p_total       = 0;
            var p_dollar      = 0;
            var p_riel        = 0;
            var p_baht        = 0;
            var p_all         = 0;

            var g_total       = 0;
            var g_dollar      = 0;
            var g_riel        = 0;
            var g_baht        = 0;
            var g_all         = 0;

            var return_amount;
            var r_dollar      = 0;
            var r_riel        = 0;
            var r_baht        = 0;

            var tax_dollar    = 0;
            var order_type      = "{{$order_type}}";  
            
            if($('.paid_amount').length <= 0){
                var currency_symbol = $('#company_currency').attr('data-symbol');
                tax_dollar          = $('#paid_amount').attr('data-tax');
                g_total             = $('.order_total_tax').val();
                p_total             = $('#paid_amount').val();
                return_amount       = $('#return_amount').val();

                if(currency_symbol == '$'){
                    g_dollar    = $('#grand_total_complete_data').val();
                    p_dollar    = $('#paid_amount').val();
                    r_dollar    = $('#return_amount').val();

                }else if (currency_symbol == '៛'){
                    g_riel      = $('#grand_total_complete_data').val();
                    p_riel      = $('#paid_amount').val();
                    r_riel      = $('#return_amount').val();

                }else if (currency_symbol == '฿'){
                    g_baht      = $('#grand_total_complete_data').val();
                    p_baht      = $('#paid_amount').val();
                    r_baht      = $('#return_amount').val();

                }else{
                    g_all       = $('#grand_total_complete_data').val();
                    p_all       = $('#paid_amount').val();
                    r_all      = $('#return_amount').val();
                }
                
            }else{
              var default_currency = $('.paid_amount_company_currency').attr('data-name');
              if(default_currency == '$'){

                    g_dollar    = $('.paid_amount_company_currency').attr('data-exchange');
                    p_dollar    = $('.paid_amount_company_currency').attr('data-amount');
                    r_dollar    = $('.paid_amount_company_currency').attr('data-return');
                    
                }else if (default_currency == '៛'){

                    g_riel      = $('.paid_amount_company_currency').attr('data-exchange');
                    p_riel      = $('.paid_amount_company_currency').attr('data-amount');
                    r_riel      = $('.paid_amount_company_currency').attr('data-return');

                }else if (default_currency == '฿'){

                    g_baht      = $('.paid_amount_company_currency').attr('data-exchange');
                    p_baht      = $('.paid_amount_company_currency').attr('data-amount');
                    r_baht      = $('.paid_amount_company_currency').attr('data-return');
                }


              $('.paid_amount').each(function(){
                var currency_symbol = $(this).attr('data-name');
                tax_dollar          = $('#paid_amount').attr('data-tax');
                g_total             = $('.order_total_tax').val();
                p_total             = $('#paid_amount').val();
                return_amount       = $('#return_amount').val();

                if(currency_symbol == '$'){

                    g_dollar    = $(this).attr('data-exchange');
                    p_dollar    = $(this).attr('data-amount');
                    r_dollar    = $(this).attr('data-return');
                    
                }else if (currency_symbol == '៛'){

                    g_riel      = $(this).attr('data-exchange');
                    p_riel      = $(this).attr('data-amount');
                    r_riel      = $(this).attr('data-return');

                }else if (currency_symbol == '฿'){

                    g_baht      = $(this).attr('data-exchange');
                    p_baht      = $(this).attr('data-amount');
                    r_baht      = $(this).attr('data-return');

                }else{

                    g_all       = $(this).attr('data-exchange');
                    p_all       = $(this).attr('data-amount');
                    r_all       = $(this).attr('data-return');

                }

            });

          }


          var items  = [];
          var addons = [];

            //item + size
            $('#table_order_data tr.menu_data_in_table').each(function(){
                var data = {
                    item_id         : $(this).find('td.m_id').text(),
                    item_name       : $(this).find('td.m_name').attr('data-name'),
                    item_qty        : $(this).find('td.m_name').attr('data-qty'),
                    item_amount     : $(this).find('td.m_amount').text(),
                    item_size       : $(this).find('td .m_size').text(),
                    item_size_name  : $(this).find('td .m_size_name').text(),
                    addons          : []
                }
                var menu_id = $(this).find('td.m_id').text();
                
                //Material
                $(this).parent().find("tr.material_data_add[data-parent="+menu_id+"]").each(function(){

                    var data_m = {
                        addon_id     : $(this).find('td.material_id').text(),
                        addon_name   : $(this).find('td.material_name').text(),
                        addon_qty    : $(this).find('td .m_qty_data').text(),
                        addon_amount : $(this).find('td.m_amount').text()
                    }
                    addons.push(data_m);
                    data.addons = addons;
                });
                
                items.push(data);
                
            });
            
            var table_id          = $('.table_id').text();
            var table_code        = $('.table_data').text();
            var order_request     = {
                id      : table_code,
                dataStatus : dataStatus,
                items   : [],
            }
            var company_id          = $('#company_id').val();

            order_request.items = items;


            //request change status for this specific table
            jQuery.ajax({
                url         : "{{ route('set_table_status') }}",
                method      : "POST",
                data        : {table_code: table_code, status : 1 ,unmerge_all:'true'},
                success     : function(data){},
                error       : function(data){}
            });
                            
            //add to local storage
            jQuery.ajax({
                url         : "{{ route('paid_order') }}",
                method      : "POST",
                data        :   {
                    table_code      : table_code,
                    g_total         : newOrderTotal,
                    g_dollar        : g_dollar,
                    g_riel          : g_riel,
                                    g_baht          : g_baht,  // end of grand amount

                                    p_total         : p_total,
                                    p_dollar        : p_dollar,
                                    p_riel          : p_riel,
                                    p_baht          : p_baht, // end of paid amount

                                    return_amount   : return_amount,
                                    r_dollar        : r_dollar,
                                    r_riel          : r_riel,
                                    r_baht          : r_baht, // end of return

                                    order_type      : order_type,

                                    table_id        : table_id,
                                    invoiceId       : invoiceId,
                                    discount_id     : discount_id
                                },
                                beforeSend  : function(data){},
                                success     : function(data){
                    //generate invoice
                    jQuery.ajax({
                        url         : "{{ route('printTicket') }}",
                        method      : "POST",
                        data        : {table_code: table_code, comid : company_id, invoiceId : invoiceId,},
                        success     : function(data){},
                        error       : function(data){
                            swal("Warning", "There must be a mistake, Please Check again!", "warning");
                        }
                    });


                    //request unlink table json file
                    jQuery.ajax({
                        url         : "{{ route('generate_paid_invoice') }}",
                        method      : "POST",
                        data        : {table_id:table_id, table_code: table_code, invoiceId : invoiceId},
                        success     : function(data){},
                        error       : function(data){}
                    });


                    swal('Paid Complete','You have sucessfully print invoice of this table','success').then((value) => {
                        window.close();
                    });
                },
                error       : function(data){}
            });

           $(".order_print").prop('disabled',true); 
        });

});
</script>

<!-- Deposit Currency input -->
<script type="text/javascript">
    $(function(){
        $('body').on('keyup','.confirm_order_deposit_data',function(){

            var input_currency      = $(this).attr('data-val'); // cashier input currency
            var input_amount        = parseFloat($(this).val()); // cashier input amount
            var company_currency    = $('#company_currency').val(); 
            // var total_without_tax   = $('#order_total_data').text(); //get total from dine in page
            // var tax                 = $('.company_tax').text()*total_without_tax/100;
            // var total               = parseFloat(total_without_tax); // company currency

            var total_tax           = $('.order_total_tax').val(); //get total from dine in page
            var total               = parseFloat(total_tax); // company currency
            //previous code
            //var total               = parseFloat($('#confirm_order_total_data_complete').text());    // order total amount
            var currency_from = parseFloat($('#'+input_currency).attr('data-from')); // Exchange rate (From) on specific currency
            var currency_to = parseFloat($('#'+input_currency).attr('data-to')); // Exchange rate (To) on specific currency

            var money_convert = 0;                                                            // Converted variable
            var input_total = 0;                                                            // Amount Cashier Input
            var manipulate_total = 0;                                                            // total after calculation

            var paid_total = 0;
            var real_amount = 0;
            
            //if paid input equal to 0 - it value will also be 0
            if($(this).val().length <= 0){
                input_amount = 0.00;
                $('.discount').show(150);
                

            }

            if(company_currency == input_currency){
                real_amount = input_amount;
                input_total = input_amount;
                $('#paid_amount').val(input_total.toFixed(2));
                $('#paid_amount').attr('data-amount',real_amount);

            }else{
                real_amount   =  input_amount;

                money_convert = (input_amount * currency_from) / currency_to;
                input_total = money_convert;
            }

            $('#paid_amount_' + input_currency).val(input_total.toFixed(2));
            $('#paid_amount_' + input_currency).attr('data-amount',real_amount);

            $('.paid_amount').each(function(){
             paid_total     += parseFloat($(this).val()) + parseFloat($('#paid_amount').attr('data-amount'));
         });


            if($('.paid_amount').length <= 0){
                paid_total = parseFloat($('#paid_amount').attr('data-amount'));
            }


            $('#paid_total').val(paid_total.toFixed(2));


            manipulate_total = total - paid_total;
            if(manipulate_total <= 0){
                manipulate_total = 0;
                $('#confirm_remain_amount').css({"background":"#559e55","color":"#fff"});
            }else{
                $('#confirm_remain_amount').css({"background":"#9e5555","color":"#fff"});
            }
            //Real Total
            $('#confirm_remain_amount_data').html(manipulate_total.toFixed(2));

            // Show Remain after paid calculation
            var remain = parseFloat($('#confirm_order_total_data_complete_hidden').text());
            $('#confirm_remain_amount_data').html(manipulate_total.toFixed(2));

            $('.paid_amount').each(function(){
                var currency = $(this).attr('data-val');
                var company_currency    = $('#company_currency').val();   
                if(company_currency == currency){
                    $('#confirm_remain_amount_data_'+currency).html(manipulate_total.toFixed(2));

                }else{
                    var currency_from_return       = parseFloat($('#'+currency).attr('data-from'));
                    var currency_to_return         = parseFloat($('#'+currency).attr('data-to'));
                    var return_convert = (manipulate_total * currency_to_return) / currency_from_return;
                    $('#confirm_remain_amount_data_'+currency).html(return_convert.toFixed(0));

                }



            });

            var get_paid = parseFloat($('#paid_total').val());

            if(total < paid_total){
                var return_money = -(total - paid_total);
                $('#return_amount').val(return_money.toFixed(2));
            }else{
                $('#return_amount').val('0.00');
            }


            var return_amount =  parseFloat($('#return_amount').val());
            $('#confirm_return_amount_data').html(return_amount.toFixed(2));
            if(parseFloat($('#confirm_return_amount_data').text()) > 0 || parseFloat($('#confirm_remain_amount_data').text()) == 0.00){
                $('#confirm_return_amount_data').parent().css({"background":"#9e9855","color":"#fff"});
                $('.confirm_return').css('display','block');
                $('.order_confirm_and_save').css('display','inline-block');
                $('.order_print').css('display','inline-block');

            }else{
                $('#confirm_return_amount_data').parent().css({"background":"#559e55","color":"#fff"});
                $('.confirm_return').css('display','none');
                $('.order_confirm_and_save').css('display','none');
                $('.order_print').css('display','none');

            }


            $('#paid_amount').attr('data-return',return_amount.toFixed(2));
            $('.paid_amount').each(function(){
                var currency = $(this).attr('data-val');
                
                if(company_currency == currency){
                    $('#confirm_return_amount_data_'+currency).html(return_amount.toFixed(2));
                    
                    
                }else{
                    var currency_from_return       = parseFloat($('#'+currency).attr('data-from'));
                    var currency_to_return         = parseFloat($('#'+currency).attr('data-to'));
                    var return_convert = (return_amount * currency_to_return) / currency_from_return;
                    $('#confirm_return_amount_data_'+currency).html(return_convert.toFixed(0));
                    $(this).attr('data-return',return_convert.toFixed(0));
                }

                if(parseFloat($('#confirm_return_amount_data_'+currency).text()) > 0 || parseFloat($('#confirm_remain_amount_data').text()) == 0.00){
                    $('#confirm_return_amount_data_'+currency).parent().css({"background":"#9e9855","color":"#fff"});
                    
                    $('.confirm_return').css('display','block');
                    $('.order_confirm_and_save').css('display','inline-block');
                }else{
                    $('#confirm_return_amount_data_'+currency).parent().css({"background":"#559e55","color":"#fff"});
                    
                    $('.confirm_return').css('display','none');
                    $('.order_confirm_and_save').css('display','none');
                }


            });

            if(parseFloat($('#confirm_remain_amount_data').text()) <= 0){
                $('.remain_paid_complete').css({"background":"#559e55","color":"#fff"});
            }else{
                $('.remain_paid_complete').css({"background":"#9e5555","color":"#fff"});
            }

            $('.confirm_order_deposit_data').each(function(){
                if($(this).val() != ""){
                    $('.remain_paid').css('display','inline-block');
                }
            });


        });

});

</script>





<!-- Add Note to menu -->
<script type="text/javascript">
    $(function(){
        $('body').on('click','.materialnoteadd',function(){
            var itemNote  = $(this).parent().prev().find('.item_note_data').val();
            var m_note_id    = $(this).parent().prev().find('.item_note_data').attr('data-size');
            $("[data-val="+m_note_id+"]").next().next().next().next().find('.item_note').html(itemNote);
            swal("Good job!", "Your note has been added!", "success");
        });
    });
    
</script>



<!-- Input text box number only -->
<!-- Insert this code below , and put onkeypress="return isNumber(event)" to your input -> done     -->
<script type="text/javascript">
 function isNumber(txt)
 {
    if(event.keyCode > 47 && event.keyCode < 58 || event.keyCode == 46)
    {
       var txtbx    = document.getElementById(txt);
       var amount   = document.getElementById(txt).value;
       var present  = 0;
       var count    = 0;

       if(amount.indexOf(".",present)||amount.indexOf(".",present+1));
       {
              // alert('0');
          }

              /*if(amount.length==2)
              {
                if(event.keyCode != 46)
                return false;
        }*/
        do
        {
           present=amount.indexOf(".",present);
           if(present!=-1)
           {
             count++;
             present++;
         }
     }
     while(present!=-1);
     if(present==-1 && amount.length==0 && event.keyCode == 46)
     {
        event.keyCode=0;
                    //alert("Wrong position of decimal point not  allowed !!");
                    return false;
                }

                if(count>=1 && event.keyCode == 46)
                {

                    event.keyCode=0;
                    //alert("Only one decimal point is allowed !!");
                    return false;
                }
                if(count==1)
                {
                    var lastdigits=amount.substring(amount.indexOf(".")+1,amount.length);
                    if(lastdigits.length>=2)
                    {
                              //alert("Two decimal places only allowed");
                              // event.keyCode=0;
                              // return false;
                          }
                      }
                      return true;
                  }
                  else
                  {
                    event.keyCode=0;
                    //alert("Only Numbers with dot allowed !!");
                    return false;
                }

            }

        </script>


        <script type="text/javascript">
            $(document).on('keydown','.confirm_order_deposit_data',function (e) {
                if (e.which === 40) {
                  $(this).parents().next().next().find('.confirm_order_deposit_data').focus();
              }
          });

            $(document).on('keydown','.confirm_order_deposit_data',function (e) {
                if (e.which === 38) {
                  $(this).parents().prev().prev().find('.confirm_order_deposit_data').focus();
              }
          });
      </script>





      <script type="text/javascript">
        $(function(){
            $('.tabs').click(function(){
                $('.tabs.active').removeClass('active');
                /*$('.tab-pane .confirm_order_deposit_data').val('').trigger($.Event("keyup"));*/
                $(this).addClass('active');
            });
        });
    </script>

    <script>
        function success_toast(heading,text,icon,time,position,loader_color){
            $.toast({
                    text: text, // Text that is to be shown in the toast
                    heading: heading, // Optional heading to be shown on the toast
                    icon: icon, // Type of toast icon
                    showHideTransition: 'fade', // fade, slide or plain
                    allowToastClose: true, // Boolean value true or false
                    hideAfter: time, // false to make it sticky or number representing the miliseconds as time after which toast needs to be hidden
                    stack: 1, // false if there should be only one toast at a time or a number representing the maximum number of toasts to be shown at a time
                    position: position, // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values
                    
                    
                    
                    textAlign: 'left',  // Text alignment i.e. left, right or center
                    loader: true,  // Whether to show loader or not. True by default
                    loaderBg: loader_color,  // Background color of the toast loader
                    beforeShow: function () {}, // will be triggered before the toast is shown
                    afterShown: function () {}, // will be triggered after the toat has been shown
                    beforeHide: function () {}, // will be triggered before the toast gets hidden
                    afterHidden: function () {}  // will be triggered after the toast has been hidden
                });
        }
    </script>


    <!-- Clear Discount -->
    <script>
        $(function(){
            $('body').on('click','.clear_container',function(){
                $('.bargain_box').removeClass('active');
                var last_total = $('#confirm_order_total_data_complete_hidden').text();
                $('#grand_total_paid').html(last_total);
                $('#confirm_order_total_data_complete_hidden').html(last_total);
                $('#confirm_order_total_data_complete').html(last_total);
                $('#confirm_remain_amount_data').html(last_total);

                $('.paid_amount').each(function(){
                    var currency = $(this).attr('data-val');
                    var company_currency    = $('#company_currency').val();  
                    if(company_currency == currency){
                        $('#paid_info #grand_total_paid_'+currency).html(last_total.toFixed(2));

                    }else{

                        var currency_from_return        = parseFloat($('#'+currency).attr('data-from'));
                        var currency_to_return          = parseFloat($('#'+currency).attr('data-to'));
                        var return_convert              = (last_total * currency_to_return) / currency_from_return;
                        $('#paid_info #grand_total_paid').html(last_total);
                        $('#paid_info #grand_total_paid_'+currency).html(return_convert.toFixed(0));
                        $('#paid_info #confirm_remain_amount_data_'+currency).html(return_convert);

                    }
                });

                $('.membership_container').unbind('click').click('.membership_container',function(){
                    $('#membership_modal').modal('show');
                    $('#membership_modal').css('z-index','1053');
                });

                $('.discount_container').unbind('click').click('.discount_container',function(){
                    $('#discount_modal').modal('show');
                    $('#discount_modal').css('z-index','1053');
                });

                $('.promotion_pack_container').unbind('click').click('.promotion_pack_container',function(){
                    $('#promotion_pack_modal').modal('show');
                    $('#promotion_pack_modal').css('z-index','1053');
                });

                $(this).hide();
            });
        });

        $(function(){

            $('#membership_modal').on('hidden.bs.modal',function (e){
                $('.membership_no').val("");
                $('.membership_validate_result').html('');
            });

            $('#discount_modal').on('hidden.bs.modal',function(e){
                $(this).find('.main').removeClass('active');
                $(this).find('#discount_amount').val(0);
                $(this).find('#discount_amount').attr('disabled', 'disabled');
            });
        });
    </script>


    <!-- Set Membership -->
    <script>
        $(function(){
            $('body').on('click','.validate_membership',function(){
                var membership_no = $('.membership_no').val();
                jQuery.ajax({
                    url     : "{{ route('find_membership') }}",
                    method  : "POST",
                    data    : {membership_no: membership_no},
                    success : function(data){
                        $('#membership_modal .modal-body .membership_validate_result').html(data);
                    },
                    error   : function(data){},
                });
            });

        });

        $(function(){
            $('body').on('click','.set_membership',function(){
                var current_total = parseFloat($('#order_total_data').text());
                jQuery.ajax({
                    url     : "{{ route('set_membership') }}",
                    method  : "POST",
                    data    : {current_total:current_total},
                    success : function(data){
                        $('#grand_total_paid').html(data);
                        $('#confirm_order_total_data_complete').html(data);
                        $('#confirm_remain_amount_data').html(data);
                        $('.membership_container.bargain_box').addClass('active');
                        var new_total = 0;
                        var current_total = parseFloat($('#order_total_data').text());
                        $('#order_total_data_no_discount').text(current_total);
                        include_discount = 1;
                        
                        var discount_type   = $('.membership_discount_type').val();  
                        var discount_amount = $('.membership_discount_amount').val();
                        $('.discount_result').css('display','inline-block');
                        $('.discount_result').html('( With discount : '+ discount_amount + discount_type +' off )');

                        $('.discount_button').hide();
                        $('#remove_discount').css('display','inline-block');
                        var discount = $('.membership_discount_amount').val();
                        var current_total = parseFloat($('#order_total_data').text());
                        // Check Discount Type
                        if(discount_type == '%'){
                            var discount_amount = (current_total * discount)/100;
                            new_total           = current_total - discount_amount;

                        }else if(discount_type == '$'){
                            new_total           = current_total - discount;
                        }
                        
                        $('#order_total_data').html(new_total.toFixed(2));


                        $('.paid_amount').each(function(){
                            var currency = $(this).attr('data-val');
                            var company_currency    = $('#company_currency').val();  
                            if(company_currency == currency){
                                $('#paid_info #grand_total_paid_'+currency).html(new_total.toFixed(2));
                                $('#order_total_data').html(new_total.toFixed(2));

                            }else{

                                var currency_from_return        = parseFloat($('#'+currency).attr('data-from'));
                                var currency_to_return          = parseFloat($('#'+currency).attr('data-to'));
                                var return_convert              = (new_total * currency_to_return) / currency_from_return;
                                $('#paid_info #grand_total_paid').html(new_total);
                                $('#paid_info #grand_total_paid_'+currency).html(return_convert.toFixed(0));
                                $('#paid_info #confirm_remain_amount_data_'+currency).html(return_convert);

                            }
                        });

                    },
                    complete: function(data){
                     //    $('.bargain_box').off('click').click('.membership_container',function(){
                     //     swal('Completed','You already choosen customer membership, If you want to change please click on cross sign.','info');
                     // });
                     //    $('.clear_container').css('display','inline-block');
                    },
                    error   : function(data){},
                });

            });

        });

    </script>

    <!-- Set Discount -->
    <script type="text/javascript">

        $(function(){
            $('body').on('click','.discount .percent',function(){
                
                $('#discount_type').val('%');

                var discount_type = '%';

                jQuery.ajax({
                    url     : "{{ route('order_discount_rate') }}",
                    method  : "POST",
                    data    : { discount_type:discount_type, _token:"{{Session::token()}}" },
                    dataType: 'html',
                    beforeSend:function(data){
                        $('#discount_amount').prop("disabled", true);
                        $('#discount_amount').html('<option>Loading...</option>');
                        $('.discount .percent .main').addClass('active');
                        $('.discount .money .main').removeClass('active');
                    },
                    success:function(data){
                        $('#discount_amount').prop("disabled", false); 
                        $('#discount_amount').hide().html(data).fadeIn('100');
                    }

                });

            });
        });

        $(function(){
            $('body').on('click','.discount .money',function(){
                
                $('#discount_type').val('$');
                var discount_type = '$';
                jQuery.ajax({
                    url     : "{{ route('order_discount_rate') }}",
                    method  : "POST",
                    data    : { discount_type:discount_type, _token:"{{Session::token()}}" },
                    dataType: 'html',
                    beforeSend:function(data){
                        $('#discount_amount').prop("disabled", true); 
                        $('#discount_amount').html('<option>Loading...</option>');
                        $('.discount .money .main').addClass('active');
                        $('.discount .percent .main').removeClass('active'); 
                    },
                    success:function(data){
                        $('#discount_amount').prop("disabled", false); 
                        $('#discount_amount').hide().html(data).fadeIn('100');
                    }

                });

            });
        });

        $(function(){
            $('body').on('change','#discount_amount',function(){
                var discount = $(this).val();
                
                jQuery.ajax({
                    url     : "{{ route('enable_discount') }}",
                    method  : "POST",
                    data    : {discount : discount},
                    success : function(data){
                        $('#discount_modal .modal-footer').html(data);
                    },
                    error   : function(data){},
                });
            });  
        });


        $(function(){
            
            $('body').on('click','.set_discount',function(){

                var discount_type       = $('#discount_type').val();
                
                var current_total       = parseFloat($('#order_total_data').text());
                var current_total_tax   = parseFloat($('.order_total_tax').val());
                var discount            = parseFloat($('.discount_amount').val());
                var new_total           = 0;
                var new_total_tax       = 0;
                var discountID = $('#discount_amount').find(':selected').data('value');
                var total_order = $('#order_total_data').text();

                $("#order_total_tax").val(total_order);
                // Backup real total
                $('#order_total_data_no_discount').text(current_total);

                $('#discount_type_id').val(discountID);
                // $('.order_total_tax').va
                include_discount = 1;

                // Check Discount Type
                if(discount_type == '%'){
                    var discount_amount     = (current_total * discount)/100;
                    new_total               = current_total - discount_amount;

                    var discount_amount_tax = (current_total_tax * discount)/100;
                    new_total_tax           = current_total_tax - discount_amount;

                }else if(discount_type == '$'){
                    new_total           = current_total - discount;
                    new_total_tax       = current_total_tax - discount_amount;
                }

                // set_discount 
                if(new_total > 0){
                    $('#confirm_order_total_data_complete').text(new_total.toFixed(2));
                    $('#confirm_remain_amount_data').text(new_total.toFixed(2));
                    $('#grand_total_paid').text(new_total.toFixed(2));
                    $('#confirm_remain_amount_data').text(new_total.toFixed(2));
                    $('#order_total_data').text(new_total);
                    $('.order_total_tax').val(new_total_tax);
                }else{
                    $('#confirm_order_total_data_complete').text('0.00');
                    $('#confirm_remain_amount_data').text('0.00');
                    $('#grand_total_paid').text('0.00');
                    $('#confirm_remain_amount_data').text('0.00');
                    $('#order_total_data').text('0.00');
                    $('.order_total_tax').val('0.00');
                }
            
                //set_discount_multi_currency with remaning money
                $('.paid_amount').each(function(){
                    var currency = $(this).attr('data-val');
                    if(company_currency == currency){
                        $('#grand_total_paid_'+currency).html(new_total.toFixed(2));
                    }else{
                        var currency_from_return                    = parseFloat($('#'+currency).attr('data-from'));
                        var currency_to_return                      = parseFloat($('#'+currency).attr('data-to'));
                        var return_convert                          = (new_total * currency_to_return) / currency_from_return;
                        $('#order_total_data').html(new_total.toFixed(2));
                        $('#paid_info #grand_total_paid_'+currency).html(return_convert.toFixed(0));
                        $('#paid_info #confirm_remain_amount_data_'+currency).html(return_convert.toFixed(0));
                    }
                });

                //change background color of Remain when the remaining is 0
                if(parseFloat($('#confirm_remain_amount_data').text()) <= 0){
                    $('.remain_paid_complete').css({"background":"#559e55","color":"#fff"});
                }else{
                    $('.remain_paid_complete').css({"background":"#9e5555","color":"#fff"});
                }

                $('.bargain .discount_container').addClass('active');
                $('.bargain_box ').off('click').click('.discount_container ',function(){
                     swal('Complete','You already choosen customer discount, If you want to change please click on cross sign.','info');
                 });
                $('.clear_container').css('display','inline-block');

                var discount_type   = $('#discount_type').val();  
                var discount_amount = $('.discount_amount').val();
                $('.discount_result').css('display','inline-block');
                $('.discount_result').html('( With discount : '+ discount_amount + discount_type +' off )');

                $('.discount_button').hide();
                $('#remove_discount').css('display','inline-block');


            });
        });

    </script>

    <script type="text/javascript">
        $('body').on('click','#remove_discount',function(){
            if(include_discount == 1){
                var real_total = parseFloat($('#order_total_data_no_discount').text());
                $('#order_total_data').text(real_total.toFixed(2));
                include_discount = 0;
                $('.discount_result').text('').hide();
                $(this).hide();
                $('.discount_button').css('display','inline-block');
                $('#discount_type_id').val('');
            }
        });
    </script>

    <!-- ========================== Set Promotion Pack Automatically  ============================ -->
    <script>
        $(function(){
            $('body').on('click','.paid',function(){
                var m_id    = [];
                $('.m_id').each(function(){
                    m_id.push($(this).text());
                });


                var current_total = parseFloat($('#confirm_order_total_data_complete').text());
                jQuery.ajax({
                    url     : "{{ route('promotion_pack_deal') }}",
                    method  : "POST",
                    data    : {current_total: current_total, m_id:m_id , _token:"{{Session::token()}}" },
                    dataType: "html",
                    beforeSend : function(){
                    	var confirm2 = { title: 'Please Wait', text: 'Process to print ... .',showConfirmButton: false };
                        swal(confirm2);
                    },
                    success : function(data){
                        if(data){
                            $('.promotion_pack_container').show();
                        }
                    },
                    error   : function(data){}
                });
            });

        });

        $(function(){
            $('#promotion_pack_modal').on('shown.bs.modal',function(e){
                var m_id    = [];
                $('.m_id').each(function(){
                    m_id.push($(this).text());
                });

                var current_total = parseFloat($('#confirm_order_total_data_complete').text());
                jQuery.ajax({
                    url         : "{{ route('promotion_pack_deal') }}",
                    method      : "POST",
                    data        : {current_total: current_total, m_id:m_id , _token:"{{Session::token()}}" },
                    dataType    : "html",
                    beforeSend  : function(data){
                        $('#promotion_pack_modal .modal-body .promotion_pack').html(
                            '<option>Loading...</option>'
                            );
                    },
                    success     : function(data){
                        if(data){
                            $('#promotion_pack_modal .modal-body .promotion_pack').prop("disabled", false); 
                            $('#promotion_pack_modal .modal-body .promotion_pack').html(data);
                        }
                    },
                    error       : function(data){}
                });
            });

        });

        $(function(){
            $('body').on('click','.set_promotion',function(){
                var current_total = parseFloat($('#confirm_order_total_data_complete').text());
                var promtion_id = $('#promotion').val();
                jQuery.ajax({
                    url     : "{{ route('set_promotion') }}",
                    method  : "POST",
                    data    : {current_total :current_total ,promotion_id:promotion_id},
                    success : function(data){},
                    error   : function(data){}
                });

            });
        });

    </script>



<!-- ================================ Split Payment ==================================== -->
<script type="text/javascript">

    $(function(){
        $('body').on('click','.split_payment',function(){
            var total           = $('#order_total_data').text();
            var tax             = $('.company_tax').text()*total/100;
            var grand_total     = parseFloat(total) + parseFloat(tax);
            $('#grand_total_paid').html(grand_total.toFixed(2));
            $('#confirm_remain_amount_data').html(grand_total.toFixed(2));
            $('.paid_amount').each(function(){
                var currency            = $(this).attr('data-val');
                var company_currency    = $('#company_currency').val();   
                if(company_currency == currency){
                    $('#grand_total_paid_'+currency).html(grand_total.toFixed(2));

                }else{
                    var currency_from_return       = parseFloat($('#'+currency).attr('data-from'));
                    var currency_to_return         = parseFloat($('#'+currency).attr('data-to'));
                    var return_convert             = (grand_total * currency_to_return) / currency_from_return;
                    $('#grand_total_paid').html(grand_total.toFixed(2));
                    $('#grand_total_paid_'+currency).html(return_convert.toFixed(0));
                    $('#confirm_remain_amount_data_'+currency).html(return_convert);

                }



            });
            $('#paid_info').css('z-index','1052');
            $('.modal-backdrop').css('z-index','1051');
            $('#paid_info #nav_pay_full_amount').hide();
            $('#paid_info #nav_split_payment').show();
            $('#paid_info .box_info').removeClass('active');
            $('#paid_info #split_payment').addClass('active');
            $('#paid_info').modal('show');
        });
    });

    $(function(){
        $('body').on('click','.split_payment .money',function(){
            var choosen_currency = $(this).attr('data-currency');
            $('.split_payment .money .main').removeClass('active');
            $('.split_payment .money[data-currency="'+choosen_currency+'"]').find('.main').addClass('active');
            $('.coverBothInv .currency_symbol').text(choosen_currency);
            $('#split_payment').attr('data-currency',choosen_currency);
            $('.coverBothInv').show();
            $('#firstInv').val('');
            $('#secondInv').val('');

        });
    });


    $(function(){
        $('body').on('keyup','#firstInv',function(){
            var data_currency = $('#split_payment').attr('data-currency');
            if(data_currency == '$'){
                var total_val       =   parseFloat($('#grand_total_paid').text());

            }else if(data_currency == '៛'){
                var total_val       =   parseFloat($('#grand_total_paid_2').text());

            }

            var invoice1_val    =   $('#firstInv').val();
            var total_invoice2  =   total_val-invoice1_val;
            if(invoice1_val == ''){
                $('#secondInv').val('');
                $('#btn_oksplite').hide();
            }
            else if(total_val <= invoice1_val){
                $('#secondInv').val("The split amount must be lower than total amount");
                $('#btn_oksplite').hide();
            }else{
                if(data_currency == '$'){
                    $('#secondInv').val(total_invoice2.toFixed(2));
                }else if(data_currency == '៛'){
                    $('#secondInv').val(total_invoice2);
                }

                $('#btn_oksplite').css('display','inline-block');
            }

        });

        $('body').on('click','#btn_oksplite',function(){
            var table_code          = $('.table_data').text();
            var company_id          = $('#company_id').val();
                        //generate invoice
                        jQuery.ajax({
                            url         : "{{ route('printTicket') }}",
                            method      : "POST",
                            data        : {table_code: table_code, comid : company_id},
                            success     : function(data){

                                var grandTotal  = $('#grand_total_paid').text();
                                var mainSplit   = $('#firstInv').val();
                                var sideSplit   = $('#secondInv').val();
                                jQuery.ajax({
                                    url         : "{{ route('split_invoice') }}",
                                    method      : "POST",
                                    data        : {grandTotal:grandTotal , mainSplit:mainSplit , sideSplit:sideSplit ,table_code:table_code},
                                    beforeSend  : function(data){
                                        swal('Order Completed','Your request has been sent','success').then((value) => {
                                        });
                                    },
                                    success     : function(data){}
                                });



                            },
                            error       : function(data){
                                swal("Warning", "There must be a mistake, Please Check again!", "warning");
                            }
                        });
 

                    });

    });

</script>



<!-- ======================= Search Category + Subcategory + Menu, When user press enter => search auto =======================-->    
<script>
        $(function(){
            $('body').on('keyup','#category_search_modal .search_data',function(e){
                if(e.keyCode == 13){
                    $(this).next('#search').click();
                }
            });

            $('body').on('keyup','#subcategory_search_modal .search_data',function(e){
                if(e.keyCode == 13){
                    $(this).next('#search').click();
                }
            });

            $('body').on('keyup','#menu_search_modal .search_data',function(e){
                if(e.keyCode == 13){
                    $(this).next('#search').click();
                }
            });

            $('#paid_info').on('shown.bs.modal',function(){
                $('#confirm_order_deposit_data').focus();
            });
    });
</script>

<!-- ======================= (OLD ORDER) Check if add more or remove =======================-->
<script>
        function enable_and_disable_create_new(){
            if("{{$exist_order}}" == true){
                var new_order = $('#table_order_data').attr('data-new');

                if(new_order == 1){
                    $('.save_data').show();
                    $('.discount_data').hide();
                    $('.discount_total').removeClass('col-md-7');
                    $('.discount_total').removeClass('col-xs-7');
                    $('.discount_total').addClass('col-md-12');
                    $('.discount_total').addClass('col-xs-12');
                    $('.split_payment').hide();
                    $('.print_invoice').hide();
                    $('.paidPrint').hide();
                }else{
                    $('.save_data').hide();
                    $('.discount_data').show();
                    $('.discount_total').removeClass('col-md-12');
                    $('.discount_total').removeClass('col-xs-12');
                    $('.discount_total').addClass('col-md-7');
                    $('.discount_total').addClass('col-xs-7');
                    $('.print_invoice').show();
                    $('.split_payment').show();
                    $('.paidPrint').show();
                }
            }
           
        }



</script>
@endsection
