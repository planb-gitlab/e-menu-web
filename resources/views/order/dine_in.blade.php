@extends("crudbooster::admin_template")
@section("content")
<body class="table_detail">
   	
<input type="hidden" id="store" value="{{$store}}">

   	<!-- Information About Table -->
<div class="container-fluid">
   		<!-- Green = Table is free -->
   		<div class="row status">
   			
   			<div class="col-md-2 col-xs-4 title available">
   				<span>{{ trans('tablemanagement.available') }}</span>
   			</div>

   			<!-- <div class="col-md-3 occupied">
   				<span>{{ trans('tablemanagement.occupied') }}</span>
   			</div> -->

   			<div class="col-md-2 col-xs-4 title unavailable">
   				<span>{{ trans('tablemanagement.unavailable') }}</span>
   			</div>

   			<div class="col-md-2 col-xs-4 title merge">
   				<span>{{ trans('tablemanagement.merge') }}</span>
   			</div>

   		</div>
</div>

    
<div class="container-fluid">
					
		<div id="main_table_container">
			
			<div id="normal">
				<div id="containment">
					<div class="row table_management">
					@foreach($tables as $key => $table)
						<div class="main_table col-md-3 col-xs-6" id="table-code-{{ $table->table_code}}">
							
							<div class = "col-md-4 col-xs-4 table_info @if($table->is_merge == 0) table_order @endif"
									data-id 	= "{{$table->id}}" 
									data-val	= "{{$table->id}}" 
									data-name	= "{{$table->table_code}}" 
									data-status	= "{{$table->status}}"
							>
								<ul class="sortable-list {{ ($table->status == 1)? 'table_free' : 'table_booked' }} @if($table->is_merge == 1) table_is_merge @endif" data-val="{{$table->table_code}}" id="{{$table->table_code}}"
								>
									<li class="sortable-item" data-image="{{URL::to('/')}}/{{$table->photo}}" style="background:linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url({{URL::to('/')}}/{{$table->photo}}) center no-repeat; background-size: cover;">
										<div class="table_number" data-id="{{$table->id}}" data-val="{{$table->table_code}}" data-name="{{$table->table_code}}">{{$table->table_code}}</div>
									</li>
								</ul>
							</div>

							<div class="col-md-6 col-xs-6 table_data">
								<div class="table_name">{{$table->table_code}}</div>
								<div class="table_capacity"><span>Size : </span>{{$table->size}}</div>	
							</div>

									<a class ="table_detail merge col-md-2 col-xs-2 table_function {{ ($table->merge_with != null) ? 'have-merge' : '' }} @if($table->is_merge == 0) table_merge @endif" 
									title="merge"
									data-id 	="{{$table->id}}" 
									data-val	="{{$table->id}}" 
									data-name	="{{$table->table_code}}" 
									data-status	="{{$table->status}}"
									id="current-table-id-{{ $table->id }}" 
									>
										<img src="{{URL::to('/')}}/uploads/defualt_image/foodorder.png" />
									</a>


						</div>

					@endforeach
						<div class="clearer">&nbsp;</div>

					</div>
				</div>							
			</div><!-- Normal END -->	    

    	</div>


    <!-- Find table fast modal -->
    <div class="modal fade" id="find_table_modal" role="dialog" style="padding-right: 0 !important;">
            <div class="modal-dialog">
            
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Find Table via table code</h4>
                </div>
                <div class="modal-body">
                  <input type="text" name="table_code" maxlength="4" class="form-control table_code" placeholder="Type your table code">
                  <div class="find_table_data" style="display: none;"></div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default find_table" >Find</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
              
            </div>
    </div>

    <!-- Show merge modal -->
    <div class="modal fade" id="merge_modal" role="dialog" style="padding-right: 0 !important;">
            <div class="modal-dialog">
            
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Merge Table</h4>
                </div>

                <div class="modal-body">

                </div>



                <div class="modal-footer">
                  <button type="button" class="btn btn-default merge_and_order" style="display: none;">Merge & Order</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
              
            </div>
    </div>
  	
    	

    


    

</div>
</body>

@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/cashier/table_detail.css')}}">
<link rel="stylesheet" href="{{ asset('css/daterangepicker.css') }}" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.12/dist/sweetalert2.min.css">
<link rel="stylesheet" href="{{ asset('css/multiselect/jquery.dropdown.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/custom_button.css') }}" />
<link rel="stylesheet" href="{{ asset('css/sort.css') }}" />
@endsection

@section('script')
<script src="{{ asset('js/moment.min.js')		}}"></script>
<script src="{{ asset('js/daterangepicker.js') 	}}"></script>
<script src="{{ asset('js/multiselect/jquery.dropdown.min.js') 	}}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.12/dist/sweetalert2.all.min.js"></script>


<!-- =========================== New Order (New Tab) =========================== -->
<script type="text/javascript">
	$(function(){
		$('body').on('click','.table_order',function(){
			var table_id 	= $(this).attr('data-val');
			var table_code	= $(this).attr('data-name');
			var hasOrder	= $(this).attr('data-status');
			var wihe 		= 'width='+screen.availWidth+',height='+screen.availHeight; 


			if(table_id && table_code){
				if(hasOrder == 1){
					window.open('{{route("cashier_create_order")}}?table_id=' + table_id + '&table_name=' + table_code + '&order_type=dinein', '_blank' );
				}else{
					window.open('{{route("cashier_update_order")}}?table_id=' + table_id + '&table_name=' + table_code +'&order_data=old&order_type=dinein');
				}

			}
		});
	});
</script>   




<!-- ======================== Quick Table search (Open Modal) ======================== -->
<script type="text/javascript">
    $(function(){
	    $(document).bind("keyup keydown", function (e) { 
	    	// if (e.shiftKey && e.keyCode == 191) { 
	    	if (e.keyCode == 191) { 
	    			$('#find_table_modal').modal('show');
	    			$('.table_code').val('');	
	    		} 
	    });
	});
    $(function(){
    	$('#find_table_modal').on('shown.modal.bs',function(){
    		$('.table_code').focus();
    		$(this).bind("keydown", function (e) { 
			    	if(e.keyCode == 13){
		    			$('button.find_table').click();
		    		}
			});
    	});
    });	
</script>


<script type="text/javascript">
	
	$(function(){
		$('body').on('click','.find_table',function(){
			var table_code = $('.table_code').val();
			var table_id;
			var table_status;
			jQuery.ajax({
			            url 	: "{{ route('find_table') }}",
			            type 	: 'POST',
			            data 	: { table_code:table_code, _token:"{{Session::token()}}"},
			            dataType: "html",
			            success : function(data){
			            	$('.find_table_data').html(data);
							table_id 		= $('.table_id').text();
							table_status 	= $('.table_status').text();
							if(table_id && table_status == 1){
								window.open('{{route("cashier_create_order")}}?table_id=' + table_id + '&table_name=' + table_code, '_blank');
							}else if(table_id && table_status != 1){
								window.open('{{route("cashier_update_order")}}?table_id=' + table_id + '&table_name=' + table_code + '&order_data=old');
							}else{
								swal({
									  position: 'top-end',
									  type: 'warning',
									  title: 'There is no table',
									  showConfirmButton: false,
									  timer: 1000
									})
							}
						},

					});
		});
	});

</script>



<!-- Merge Table Using button -->
<script type="text/javascript">
	$(function(){
		$('body').on('click','a.table_merge',function(){
			var table_id 	= $(this).attr('data-val');
			
				jQuery.ajax({
					url 	: " {{ route('find_merge_table') }}",
					type 	: "POST",
					data 	: { table_id:table_id, _token:"{{Session::token()}}" },
					success 	: function(data){
						$('#merge_modal .modal-body').html(data);
						$('.side_table_merge').select2({
							placeholder: "Please select table to merge (Can be more than 1)",
    						allowClear: true
						});
					},
					error		: function(data){

					}
				});
				$('#merge_modal').modal('show');
		});


		$('body').on('click','.merge_and_order',function(){

			var table_id 		= $(this).parent().prev().children('.main_table_merge').children('.table_id').val();
			var table_code 		= $(this).parent().prev().children('.main_table_merge').children('.table_code').text();
			var side_table_id 	= $(this).parent().prev().children('.side_table').children('.side_table_merge').val();
			var old_order		= $('#table-code-'+ table_code).find('.table_info').data('status');
			console.log(old_order);

			jQuery.ajax({
					url 	: " {{ route('merge_table') }}",
					type 	: "POST",
					data 	: { table_id:table_id, side_table_id:side_table_id, _token:"{{Session::token()}}" },
					success 	: function(data){
						if(data == "merge_and_order"){
							swal('Complete','You have successfully merge table!','success').then((value) => {
                                //location.reload();
                                if(old_order == 1){
                                	location.reload(); 	
						     		window.open('{{route("cashier_create_order")}}?table_id=' + table_id + '&table_name=' + table_code+"&&"+ side_table_id); 
						     	}else{
						     		location.reload(); 	
						     		window.open('{{route("cashier_update_order")}}?table_id=' + table_id + '&table_name=' + table_code+"&&"+ side_table_id + '&order_data=old');
						     	}

                                });
							 
						}else{
							swal('Complete','You have successfully merge table!','success').then((value) => {
									location.reload(); 	
								});
						}
					},
					error		: function(data){
						swal("Warning", "There must be a problem, Please check your input", "warning");
					}
				});
		});


	});


</script>


<script>
	$(function(){
		$('body').on('change','.side_table_merge',function(){
			if($(this).val() == null || $(this).val() == ''){
				$('.merge_and_order').text('{{trans("tablemanagement.unmerge")}}');
			}else{
				$('.merge_and_order').text('{{trans("tablemanagement.merge and order")}}');
			}
			$('.merge_and_order').css('display','inline-block');
		});
	});
</script>

<?php if (!$sock = @fsockopen('www.google.com', 80, $num, $error, 5)): ?>
<script type="text/javascript">
function fetchdata(){
 jQuery.ajax({
        url     : "{{ route('get_all_table') }}",
        method  : "POST",
        data    : {},
        success : function(data){
            $('#main_table_container').html(data);
        },
        complete: function(data){
        	setTimeout(fetchdata,2000);
        }
    }); 
}

$(document).ready(function(){
 setTimeout(fetchdata,2000);
});


</script>
<?php else: ?>
<script src="https://js.pusher.com/4.4/pusher.min.js"></script>
@push('bottom')

	<script>
    Pusher.logToConsole = false;

    var pusher = new Pusher('a6aeab0c91a31bc5bf47', {
      cluster: 'ap1',
      forceTLS: true
    });


    var channel = pusher.subscribe('my-channel');
    channel.bind('OrderEvent', function(data) {
		jQuery.ajax({
                    url     : "{{ route('get_all_table') }}",
                    method  : "POST",
                    data    : {},
                    success : function(data){
                        $('#main_table_container').html(data);
                    }
                });  
    });

    channel.bind('MergeEvent', function(data) {
      	jQuery.ajax({
                    url     : "{{ route('get_all_table') }}",
                    method  : "POST",
                    data    : {},
                    success : function(data){
                        $('#main_table_container').html(data);
                    }
                });  
	
    });

    channel.bind('UnmergeEvent', function(data) {

    	jQuery.ajax({
                    url     : "{{ route('get_all_table') }}",
                    method  : "POST",
                    data    : {},
                    success : function(data){
                        $('#main_table_container').html(data);
                    }
                });  
    });



  </script>
@endpush
<?php endif; ?>





@endsection





