@extends("crudbooster::admin_template")
@section("content")
<body id="cashier_takeout">

<div class="panel panel-default">
        <div class="row">    
            <div class="col-md-8 panel-heading">
                <strong>{!! $page_title !!}</strong>
            </div>

            <div class="col-md-4" style="text-align: right; padding: 10px 20px;">
                <a href="{{route('save_order.takeout')}}?table_id=0&table_name=0000&order_type=takeout&type=new" target="_blank">  
                    {!! trans('order.Order') !!} <i class="fa fa-shopping-cart icon" aria-hidden="true"></i>
                </a>
            </div>
        </div>

</div>

<!-- menu for takeout order -->
<div class="container-fluid button_order" style="padding-top: 20px">
    	

    <div class="col-md-12" id="view_order_detail">

            <table class="table table-striped table-bordered table-hover">
                <thead class="dir_table_thead">
                    
                <th>{{trans('order.No')}}</th>
                <th>{{trans('order.OrderCode')}}</th>
                <th>{{trans('order.Order Date')}}</th>
                <th>{{trans('order.Order Time')}}</th>
                <th>{{trans('order.Action')}}</th>

                </thead>
            

                <tbody id="view_order_data">
                    @if(count($order) > 0)
                    @foreach($order as $key => $o)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$o->order_code}}</td>
                        <td>{{$o->invoice_date}}</td>
                        <td>{{ Carbon\Carbon::parse($o->created_at)->format('g:i A') }}</td>
                        <td>
                            <a class="view_order" href='{{route("cashier_update_order")}}?invoice_id={{ $o->invoice_id }}&order_data=invoice&type=edit_invoice&order_type=takeout', target="_blank">
                                  <i class="fa fa-eye"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                    @else 
                    <tr>
                        <td colspan="5" style="text-align: center;">No Data</td>
                    </tr>
                    @endif
                </tbody>
            </table> 


    </div>
        

</div>





@endsection

@section('css')
<style type="text/css">
    .panel-default>.panel-heading{
    background-color: #fff !important;
}

.container-fluid i.icon {
    font-size: 50px;  
    color:#333;
}

span.glyphicon {
    font-size: 50px;  
    color:#333;
}

hr{
    border-top-color: #eae5e5 !important;
}

.view_takeout_order{
    margin: 20px;
    box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
}

.view_takeout_order .main{
    height: auto;
    padding-bottom: 30px;
    margin-bottom: 20px
}

.main{
    background: #fff;
    text-align: center;
    height: 17vw;
    box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
    margin-bottom: 30px;
}


.main:hover,.main:hover span,.main:hover i{
    
    color:#5674b3;
    transition: .1s all;
}

.main .title{
    color:#333;
    font-weight: 600;
    padding: 20px 0 0;
}

.main .item{
    color:#333;
    font-size: 22px;
}


.row{
    margin-left: 0 !important;
    margin-right: 0 !important;
}

.rowDetail{
    background: #3C5A99;
}

.rowDetail a:hover .detail{
    background: #5674b3;
    transition: .1s all;
}

.detail{
        padding: 20px 15px;
        border-right: 1px solid #9dabca;
        color:#fff;
}

.no_border{
    border-right: 0;
}

a.view_order{
    background-color: #3c5a99;
    border-color: #3c5a99;
    border-bottom: 3px solid #2a4377;
    padding: 3px 15px;
    color: #fff;
    font-size: 12px;
    cursor: pointer;
}



strong.dashboard_setting {
    position:  absolute;
    right: 30px;
    top: 120px;
    cursor: pointer;
}       

strong.dashboard_setting:hover{
    opacity: 0.8;

}

.modal-open {overflow-y: auto}

thead.dir_table_thead {
    background: #3c5a99;
    color: #fff;
    display: table;
    width: 100%;
    table-layout: fixed;
}

#view_order_data {
    height: 23.3vw;
    overflow-y: auto;
    transition: color 3s ease;
    display: table;
    width: 100%;
    table-layout: fixed;
    background: #fff;
}

#view_order_data tr {
    display: table;
    width: 100%;
    table-layout: fixed;
}

.button_order {
    padding:0 !important;
    background: #fff;
}

table.table.table-striped.table-bordered.table-hover {
    margin: 0;
}

div#view_order_detail {
    /* border: 1px solid #d7e1f5; */
    padding: 10px;
    box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
}


</style>
@endsection

@section('script')
<script src="https://js.pusher.com/4.4/pusher.min.js"></script>
<script>

    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('a6aeab0c91a31bc5bf47', {
      cluster: 'ap1',
      forceTLS: true
    });

    var channel = pusher.subscribe('my-channel');
    channel.bind('OrderEvent', function(data) {
        var action          = data.data.action;
                jQuery.ajax({
                    url     : "{{ route('get_order_data') }}",
                    method  : "POST",
                    data    : {},
                    success : function(data){
                        var url = "{{ url("admin/takeout") }}"
                        // $('#view_order_data').reload(url);
                        window.location.href = url;

                    }
                });  
    });
  </script>
@endsection