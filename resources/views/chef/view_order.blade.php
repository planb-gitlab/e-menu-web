@extends("layouts.order")
@section("content")


<div class="container-fluid header">
    <div class="row">
        <div class="col-md-6 module">
                <strong class="title"><i class='{{CRUDBooster::getCurrentModule()->icon}}'></i> {!! $page_title or "Create Order" !!}</strong>
        </div>

        <div class="col-md-6 orderData">
                <div class="table_id" style="display:none;">{{$_GET['id']}}</div>
                <div class="table_code">
                    <span class="table_title">Table : </span>
                    <span class="table_data">{{$_GET['name']}}</span>
                </div>
                <div>
                    <span class="cashier_title">Chef : </span>
                    <span class="cashier_id" style="display:none;">{{$user->id}}</span>
                    <span class="cashier_data">{{$user->name}}</span>
                </div> 
        </div>



    </div>
</div>

<div class="container-fluid body ">
    <div class="row chef">
       



         <div class="col-md-12 customer_order">
            <table id="table_order" style="border:1px solid #ccc;">
                <thead>
                    <tr>
                        <th>Description</th>
                        <th>Qty</th>
                        <th>Price</th>
                        <th>Action</th>
                    </tr>
                </thead>


                <tbody id="table_order_data">
                   
                </tbody>

            </table>

        </div>

        <div class="col-md-5 customer_order">

            <div id="operation_button">
              <div class="button save_data" data-toggle="modal" data-target="#savemodal">
                <div class="item">
                    <img class="button_image" src="{{asset('uploads/defualt_image/save.png')}}" />
                </div>
                <p>Save</p>
              </div>


              <div class="button exit">
                <div class="item">
                    <img class="button_image" src="{{asset('uploads/defualt_image/exit.png')}}" />
                </div>
                <p>Exit</p>
              </div>

              
            </div>

        </div>


        

        


        <!-- search modal -->
        <div class="modal fade" id="searchmodal" role="dialog">
            <div class="modal-dialog">
            
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                  <input type="text" name="search" placeholder="input category here" class="search_data">
                  <button id="search" type="submit" class="btn btn-default search" data-dismiss="modal">Search</button>
                </div>
                <!-- <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div> -->
              </div>
              
            </div>
          </div>

        <!-- End of search modal -->






        <!-- save modal -->
        <div class="modal fade" id="savemodal" role="dialog">
            <div class="modal-dialog">
            
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                  <h4 class="modal-title">Complete</h4>
                </div>
                <div class="modal-body">
                  <p>Order has successfully been added.</p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                </div>
              </div>
              
            </div>
          </div>

        <!-- End of save modal -->


         <!-- show material modal -->
        <div class="modal fade" id="material_modal" role="dialog" style="padding-right: 0 !important;">
                <div class="modal-dialog">
                
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                      <h4 class="modal-title">Material Information</h4>
                    </div>
                    <div class="modal-body">
                      <p>Loading Material detail...</p>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default materialadd" >Add to list</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                  
                </div>
        </div>

        <!-- End of Material modal -->

         <!-- show broken modal -->
        <div class="modal fade" id="broken_modal" role="dialog" style="padding-right: 0 !important;">
                <div class="modal-dialog">
                
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                      <h4 class="modal-title">Broken Information</h4>
                    </div>
                    <div class="modal-body">
                      <label style="width: 100%;">Broken Information</label>
                      <textarea style="width: 100%;" class="form-control broken-info"></textarea>
                      <label style="width: 100%;">Cost</label>
                      <input style="width: 100%;" type='number' step='any' placeholder="broken cost" class="form-control brokencost"/>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default brokenadd" >Add to list</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                  
                </div>
        </div>

        <!-- End of broken modal -->
  


    </div>
</div> 
@endsection
    


