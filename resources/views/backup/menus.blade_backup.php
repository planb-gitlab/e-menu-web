<!-- Add Menu and Update Menu -->
@extends('crudbooster::admin_template')
@section('content')
<link rel="stylesheet" href="{{ asset('css/menus.css') }}">
    
    <!-- go back to previous page -->
    @if(CRUDBooster::getCurrentMethod() != 'getProfile' && $button_cancel)
            @if(g('return_url'))
                <p><a title='Return' href='{{g("return_url")}}'><i class='fa fa-chevron-circle-left '></i>
                        &nbsp; {{trans("crudbooster.form_back_to_list",['module'=>CRUDBooster::getCurrentModule()->name])}}</a></p>
            @else
                <p><a title='Main Module' href='{{CRUDBooster::mainpath()}}'><i class='fa fa-chevron-circle-left '></i>
                        &nbsp; {{trans("crudbooster.form_back_to_list",['module'=>CRUDBooster::getCurrentModule()->name])}}</a></p>
            @endif
    @endif
    <!-- End of "go back to previous page" -->


    <!-- Company Currency -->
    <input type="hidden" name="company_currency" id="company_currency" value="{{ $company_currency->currency_symbol }}">


    <div class="panel panel-default">
            <!-- Page Title -->
            <div class="panel-heading">
                <strong><i class='{{CRUDBooster::getCurrentModule()->icon}}'></i> {!! $page_title or "Page Title" !!}</strong>
            </div>
            <!-- End of page Title -->

            <div class="panel-body" style="padding: 20px 0 0px 0">
                <?php
                $action = (@$row) ? CRUDBooster::mainpath("edit-save/$row->id") : CRUDBooster::mainpath("add-save");
                $return_url = ($return_url) ?: g('return_url');
                ?>


                    <form class='form-horizontal' method='post' id="form" enctype="multipart/form-data" 
                    action="{{ (@$menus->menu_id)?
                                route('update.menu'):
                                $action }}" >

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type='hidden' name='return_url' value='{{ @$return_url }}'/>
                    <input type='hidden' name='ref_mainpath' value='{{ CRUDBooster::mainpath() }}'/>
                    <input type='hidden' name='ref_parameter' value='{{urldecode(http_build_query(@$_GET))}}'/>
                    @if($hide_form)
                        <input type="hidden" name="hide_form" value='{!! serialize($hide_form) !!}'>
                    @endif

                    <input type="hidden" class="form-control" name="menu_id"  value="{{ $menus->menu_id }}">

                   
                <div class="row">
                    <div class="col-md-8">
                    <div class="box-body">
                        
                        <div class="form-group header-group-0" id="form-group-category">
                            <label class="control-label col-sm-2">{{trans('menumanagement.Category')}}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-sm-6">
                               <select name="category" class="form-control category" required="" id='category'>
                                <option value="">Please select category menu</option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}"@if($menus->category_id == $category->id) selected="selected" @endif>{{$category->name}}</option>
                                @endforeach
                                </select>
                            </div>
                            <div class="add_new">
                                <span class="add_new_category">
                                    <img class="add_new_icon" title="Add new category" src="{{ asset('uploads/defualt_image/category.png') }}">
                                    <span class="add_new_title">{{trans('menumanagement.Category')}}</span>
                                </span>
                            </div>
                        </div>

                        <div class="form-group header-group-0" id="form-group-subcategory">
                            <label class="control-label col-sm-2">{{trans('menumanagement.Sub Category')}}</label>
                            <div class="col-sm-6">
                               <select name="subcategory" class="form-control subcategory" id='subcategory'>
                                <option value="">Please select subcategory menu</option>
                                @foreach($subcategories as $subcategory)
                                    <option value="{{ $subcategory->id }}" @if($menus->subcategory_id == $subcategory->id) selected="selected" @endif>{{ $subcategory->name }}</option>
                                @endforeach
                                </select>
                            </div>
                            <div class="add_new">
                                <span class="add_new_subcategory">
                                    <img class="add_new_icon" title="Add new sub category" src="{{ asset('uploads/defualt_image/subcategory.png') }}">
                                    <span class="add_new_title">{{trans('menumanagement.Sub Category')}}</span>
                                </span>
                            </div>
                        </div>

                        <div class="form-group header-group-0" id="form-group-menu">
                            <label class="control-label col-sm-2">{{trans('menumanagement.Menu Name')}}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-sm-6">
                               <input type="text" name="menu" class="form-control" placeholder="menu name" required id="menu" value="{{ $menus->menu_name }}" autocomplete="off">
                            </div>
                        </div>

                        <div class="form-group header-group-0" id="form-group-menu_description">
                            <label class="control-label col-sm-2">{{trans('menumanagement.Description')}}</label>
                            <div class="col-sm-6">
                               <textarea class="form-control" name="menu_description" id="menu_description" placeholder="Description for the item" autocomplete="off">{{$menus->menu_description}}</textarea>
                            </div>

                        </div>

                        <div class="form-group header-group-0" id="form-group-menu_photo">
                            <label class="control-label col-sm-2">{{trans('menumanagement.Menu Photo')}}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-sm-6">
                               @if(empty($menus->menu_photo))
                               <input type="file" class="form-control" name="menu_photo" id="menu_photo" accept="image/*" required> 
                                <div style="color:#908e8e; margin-top: 5px;">{{trans('menumanagement.File types support : JPG, JPEG, PNG, GIF, BMP')}}</div>
                               @else
                                <p>
                                    <a data-lightbox="roadtrip" href="{{$url}}/{{$menus->menu_photo}}"><img style="max-width:160px" title="{{$menu->menu_name}}" src="{{$url}}/{{$menus->menu_photo}}"></a></p>
                                
                                <input type="hidden" name="menu_photo" value="{{$menus->menu_photo}}">                   
                                    <p><a class="btn btn-danger btn-delete btn-sm" onclick="if(!confirm('Are you sure ?')) return false" href="{{$url}}/admin/menus/delete-image?image={{$menus->menu_photo}}&amp;id={{$menus->menu_id}}&amp;column=photo"><i class="fa fa-ban"></i> Delete </a></p>
                                    <p class="text-muted"><em>{{trans('menumanagement.* If you want to upload other file, please first delete the file.')}}</em></p>
                                 <div class="text-danger"></div>


                               @endif
                            </div>

                        </div>

                        <div class="form-group header-group-0" id="form-group-menu_price">
                            <label class="control-label col-sm-2">{{trans('menumanagement.Price')}} ({{ $company_currency->currency_symbol }})
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-sm-6">
                               <input type="number" step="any" class="form-control" name="menu_price" id="menu_price" required min="0" placeholder="0.00" value="{{$menus->menu_price}}"> 
                            </div>

                        </div>


                        <div class="UOM_initial">
                            
                            <button type="button" class="btn btn-default add_uom col-md-8" val-num="1" >
                                <img src="{{ asset('uploads/defualt_image/add.png') }}" class="add_button_img"/>
                                <span class="add_button_title">{{trans('menumanagement.Select UOM with Price')}}</span>
                            </button>

                            <div class="add_new">
                                <span class="add_new_uom">
                                    <img class="add_new_icon" title="Add new UOM" src="{{ asset('uploads/defualt_image/add_uom.png') }}">
                                    <span class="add_new_title">{{trans('menumanagement.Add New UOM')}}</span>
                                </span>
                            </div>

                        </div>


                        <div class="uom_price">
                        <!-- UOM with Price -->
                        @if($menu_uom_price)
                                @foreach($menu_uom_price as $uom)
                                    <div class="form-group header-group-0" id="form-group-uom">
                                        <label class="control-label col-sm-2">{{trans('menumanagement.UOM')}}
                                        <span class="text-danger" title="This field is required">*</span></label>
                                            <div class="col-sm-6">
                                                <select name="uom[]" class="form-control uom" id="uom">
                                                    <option value="">Please select uom</option>
                                                        @foreach($uoms as $u)
                                                            <option value="{{ $u->name }}" @if($u->name == $uom->uom) selected="selected" @endif>{{$u->name}}</option>
                                                        @endforeach
                                                </select>
                                            </div>
                                            
                                            <div class="col-sm-3">
                                            <input type="number" step="any" class="form-control" name="uom_price[]" id="uom_price" required min="0"
                                            placeholder= "Price* ($)" value="{{ $uom->uom_price }}">
                                            </div>
                                            <span class="remove_uom_price" data-id="" title="{{trans('menumanagement.Remove')}}"><i class="glyphicon glyphicon-remove"></i></span>
                                    </div>
                                @endforeach
                            @endif
                        </div>


                            
                        </div>



                    </div>


                   
                    <div class="col-md-4">
                        <div class="menu_add">
                            <div class="mainboxSet">
                                <button type="button" class="btn btn-default add_material" limit-date="0" val-num="1">{{trans('menumanagement.Add addon to this menu')}}</button>
                                <label> : </label>
                            </div>
                            
                        </div>

                        <div class="Add-menu5">
                            @if($menu_details)
                            @foreach($menu_details as $menu_detail)
                            <div class="wrap_menuadd">

                                
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-10 materials">
                                          <select name="material[]" class="form-control material">
                                          
                                            @foreach($materials as $material)
                                            <option value="{{$material->id}}" @if($menu_detail->material_id == $material->id) selected="selected" @endif> {{$material->name}}</option>
                                            @endforeach

                                
                                          </select>
                                        </div>

                                        <div class="col-sm-2 clear_padding">
                                          <span class="remove-this" data-id="" title="{{trans('menumanagement.Remove')}}"><i class="glyphicon glyphicon-remove"></i></span>
                                        </div>
                                    </div>
                                </div>
                            
                            </div>
                            @endforeach
                            @endif

                        </div>

                    </div>
            </div>

                       

                   

                     <div class="box-footer" style="background: #F5F5F5">

                        <div class="form-group">
                            <label class="control-label col-sm-2"></label>
                            <div class="col-sm-10">
                                @if($button_cancel && CRUDBooster::getCurrentMethod() != 'getDetail')
                                    @if(g('return_url'))
                                        <a href='{{g("return_url")}}' class='btn btn-default'><i
                                                    class='fa fa-chevron-circle-left'></i> {{trans("crudbooster.button_back")}}</a>
                                    @else
                                        <a href='{{CRUDBooster::mainpath("?".http_build_query(@$_GET)) }}' class='btn btn-default'><i
                                                    class='fa fa-chevron-circle-left'></i> {{trans("crudbooster.button_back")}}</a>
                                    @endif
                                @endif
                                @if(CRUDBooster::isCreate() || CRUDBooster::isUpdate())

                                    @if(CRUDBooster::isCreate() && $command == 'add')
                                        <input type="submit" name="submit" value='{{trans("crudbooster.button_save_more")}}' class='btn btn-success submit_menu '>
                                    @endif

                                    @if($button_save && $command != 'detail')
                                        <input type="submit" name="submit" value='{{trans("crudbooster.button_save")}}' class='btn btn-success submit_menu'>
                                    @endif

                                @endif
                            </div>
                        </div>


                    </div><!-- /.box-footer !-->


                </form>

            </div>


    </div>


    <!-- show category modal -->
    <div class="modal fade" id="category_modal" role="dialog" style="padding-right: 0 !important;">
            <div class="modal-dialog modal-lg">
            
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title"><strong>{{trans('menumanagement.Create Category Menu')}}</strong></h4>
                </div>
                
                <div class="modal-body">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong><i class="fa fa-tags"></i>{{trans('menumanagement.Add Category Menu')}}</strong>
                        </div>

                        <div class="panel-body" style="padding:20px 0px 0px 0px">
                                <form class="form-horizontal" method="post" id="form_category">

                                    <div class="box-body" id="parent-form-area">
                                        <div class="form-group header-group-0 " id="form-group-name" style="">
                                        <label class="control-label col-sm-2">{{trans('menumanagement.Name')}}
                                            <span class="text-danger" title="This field is required">*</span>
                                        </label>

                                        <div class="col-sm-10">
                                            <input type="text" title="{{trans('menumanagement.Name')}}" required="" placeholder="You can only enter the letter only" maxlength="70" class="form-control" name="name" id="name" value="">
                                            <div class="text-danger"></div>
                                            <p class="help-block"></p>
                                        </div>
                                        </div>   

                                        <div class="form-group header-group-0 " id="form-group-photo" style="">
                                            <label class="col-sm-2 control-label">{{trans('menumanagement.Photo')}}</label>

                                            <div class="col-sm-10">
                                                <input type="file" id="photo" title="Photo" class="form-control" name="photo">
                                                 <p class="help-block">{{trans('menumanagement.File types support : JPG, JPEG, PNG, GIF, BMP')}}</p>
                                                <div class="text-danger"></div>
                                            </div>

                                        </div>
                                    </div><!-- /.box-body -->

                                <div class="box-footer" style="background: #F5F5F5">

                                    <div class="form-group">
                                        <label class="control-label col-sm-2"></label>
                                        <div class="col-sm-10">
                                           <!--  <a href="http://localhost/e-menu/public/admin/categories" class="btn btn-default"><i class="fa fa-chevron-circle-left"></i> Back</a> -->
                                                <!-- <input type="submit" name="submit" value="Save &amp; Add More" class="btn btn-success"> -->
                                                <input type="submit" name="submit" value="{{trans('menumanagement.Save')}}" class="btn btn-success submit">
                                        </div>
                                    </div>


                                </div><!-- /.box-footer-->

                            </form>

                        </div>
                    </div>
                
                </div>

              

            </div>
              
        </div>
    </div>

        <!-- End of show category -->


    <!-- show sub category modal -->
    <div class="modal fade" id="subcategory_modal" role="dialog" style="padding-right: 0 !important;">
            <div class="modal-dialog modal-lg">
            
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title"><strong>{{trans('menumanagement.Create Sub Category Menu')}}</strong></h4>
                </div>
                
                <div class="modal-body">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong><i class="fa fa-tags"></i>{{trans('menumanagement.Add Category Menu')}}</strong>
                        </div>

                        <div class="panel-body" style="padding:20px 0px 0px 0px">
                                <form class="form-horizontal" method="post" id="form_subcategory">

                                    <div class="box-body" id="parent-form-area">
                                        <div class="form-group header-group-0 " id="form-group-category_id" style="">
                                            <label class="control-label col-sm-2">{{trans('menumanagement.Category')}}
                                                <span class="text-danger" title="This field is required">*</span>
                                                    </label>
                                            <div class="col-sm-10">
                                                <select name="category" class="form-control category" required="" style="width: 100%;">
                                                <option value="">Please select category menu</option>
                                                    @foreach($categories as $category)
                                                        <option value="{{ $category->id }}">{{$category->name}}</option>
                                                    @endforeach 
                                                </select>

                                            </div>
                                        </div>


                                        <div class="form-group header-group-0 " id="form-group-name" style="">
                                            <label class="control-label col-sm-2">{{trans('menumanagement.Name')}}
                                                <span class="text-danger" title="This field is required">*</span>
                                            </label>

                                            <div class="col-sm-10">
                                                <input type="text" title="{{trans('menumanagement.Name')}}" required="" placeholder="You can only enter the letter only" maxlength="70" class="form-control" name="name" id="name" value="">
                                                <div class="text-danger"></div>
                                                <p class="help-block"></p>
                                            </div>
                                        </div>   

                                        <div class="form-group header-group-0 " id="form-group-photo" style="">
                                            <label class="col-sm-2 control-label">{{trans('menumanagement.Photo')}}</label>

                                            <div class="col-sm-10">
                                                <input type="file" id="photo" title="Photo" class="form-control" name="photo">
                                                 <p class="help-block">{{trans('menumanagement.File types support : JPG, JPEG, PNG, GIF, BMP')}}</p>
                                                <div class="text-danger"></div>
                                            </div>

                                        </div>
                                    </div><!-- /.box-body -->

                                <div class="box-footer" style="background: #F5F5F5">

                                    <div class="form-group">
                                        <label class="control-label col-sm-2"></label>
                                        <div class="col-sm-10">
                                                <input type="submit" name="submit" value="Save" class="btn btn-success submit">
                                        </div>
                                    </div>


                                </div><!-- /.box-footer-->

                            </form>

                        </div>
                    </div>
                
                </div>

              

            </div>
              
        </div>
    </div>

    <!-- End of show sub category -->


    <!-- show uom modal -->
    <div class="modal fade" id="uom_modal" role="dialog" style="padding-right: 0 !important;">
            <div class="modal-dialog modal-lg">
            
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title"><strong>{{trans('menumanagement.Create UOM')}}</strong></h4>
                </div>
                

                <div class="modal-body">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                                <strong><i class="fa fa-tags"></i>{{trans('menumanagement.Add Category Menu')}}</strong>
                        </div>
                        
                        <div class="panel-body" style="padding:20px 0px 0px 0px">
                                <form class="form-horizontal" method="post" id="form_uom">
                                    

                                    <div class="box-body" id="parent-form-area">
                                        <div class="form-group header-group-0 " id="form-group-name" style="">
                                        <label class="control-label col-sm-2">{{trans('menumanagement.Name')}}
                                            <span class="text-danger" title="This field is required">*</span>
                                        </label>

                                        <div class="col-sm-10">
                                            <input type="text" title="{{trans('menumanagement.Name')}}" required="" placeholder="You can only enter the letter only" maxlength="70" class="form-control" name="name" id="name" value="">
                                            <div class="text-danger"></div>
                                            <p class="help-block"></p>
                                        </div>
                                        </div>   

                                        
                                    </div><!-- /.box-body -->

                                <div class="box-footer" style="background: #F5F5F5">

                                    <div class="form-group">
                                        <label class="control-label col-sm-2"></label>
                                        <div class="col-sm-10">
                                                <input type="submit" name="submit" value="{{trans('menumanagement.Save')}}" class="btn btn-success submit">
                                        </div>
                                    </div>


                                </div><!-- /.box-footer-->

                            </form>

                        </div>
                    </div>
                
                </div>

              

            </div>
              
        </div>
    </div>

    <!-- End of uom -->

    <div id="uploadimageModal" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">{{trans('menumanagement.Upload & Crop Image')}}</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 text-center">
                          <div id="image_demo" style="width:100%; margin-top:30px"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success crop_image">{{trans('menumanagement.Crop')}}</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('menumanagement.Close')}}</button>
            </div>
        </div>
    </div>
    </div>

@endsection


<script src="{{ asset('js/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.12/dist/sweetalert2.all.min.js"></script>
<link   rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.12/dist/sweetalert2.min.css">
<link rel="stylesheet" href="{{asset('css/croppie.css')}}">


<style type="text/css">
    form#form_uom {
        margin-bottom: 0;
    }
</style>

<!-- on load -->
<script>
    $(function() {
        $( "#datepicker1" ).datepicker();
        $( "#datepicker2" ).datepicker();
    });

    $(function(){
        var uom = $('#form-group-uom').children().length;
        if(uom > 0){
            $('#form-group-menu_price').hide();
        }else{
            $('#form-group-menu_price').show();
        }
    });
    $(function(){
        $('.category').select2();
        $('.category').select2();
        $('.subcategory').select2();
    });
    
</script>

<script>

  $(function(){
    $('body').on('change','#category',function(){
        var category_id = $('#category').val();
        jQuery.ajax({
                url: "{{ route('menu_category') }}",
                type: 'POST',
                data: { category_id: category_id},
                dataType: "html",

                beforeSend: function(){
                 $("#subcategory").attr("disabled", true);
                $("#subcategory").html("<option>Please wait</option>");
               },

                success: function (data) {
                    $("#subcategory").attr("disabled", false);
                    $('#subcategory').html(data);
                }
            
        });
    });
     
  })



 
  </script>

  


  <!-- pop up create category -->
    <script type="text/javascript">
      $(function(){
        $('body').on('click','.add_new_category',function(){
            $('#category_modal').modal('show');
            $("#category_modal .modal-dialog").css('top','100px');
        });
      });

      $(function(){
        $('body').on('submit','#form_category',function(e){
            e.preventDefault();

            jQuery.ajax({
                            url: "{{ route('add_category_menu_via_menu_page') }}",
                            type: "POST",
                            data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                            contentType: false,       // The content type used when sending data to the server.
                            cache: false,             // To unable request pages to be cached
                            processData: false,
                            beforeSend: function(data){
                                
                            },
                            success: function (data) {
                                window.location.reload();
                            }
                        });
        });
      });
    
  </script>


  <!-- pop up create sub category -->
  <script type="text/javascript">
      $(function(){
        $('body').on('click','.add_new_subcategory',function(){
            $('#subcategory_modal').modal('show');
            $("#subcategory_modal .modal-dialog").css('top','100px');
        });
      });

      $(function(){
        $('body').on('submit','#form_subcategory',function(e){
            e.preventDefault();

            jQuery.ajax({
                            url: "{{ route('add_subcategory_menu_via_menu_page') }}",
                            type: "POST",
                            data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                            contentType: false,       // The content type used when sending data to the server.
                            cache: false,             // To unable request pages to be cached
                            processData: false,
                            beforeSend: function(data){
                                
                            },
                            success: function (data) {
                                window.location.reload();
                            }
                        });
        });
      });
    
  </script>

  <!-- pop up create uom -->
  <script type="text/javascript">
      $(function(){
        $('body').on('click','.add_new_uom',function(){
            $('#uom_modal').modal('show');
            $("#uom_modal .modal-dialog").css('top','100px');
        });
      });

      $(function(){
        $('body').on('submit','#form_uom',function(e){
            e.preventDefault();

            jQuery.ajax({
                            url: "{{ route('add_uom_via_menu_page') }}",
                            type: "POST",
                            data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                            contentType: false,       // The content type used when sending data to the server.
                            cache: false,             // To unable request pages to be cached
                            processData: false,
                            beforeSend: function(data){
                                $('#uom_modal').modal('hide');
                            },
                            success: function (data) {
                                if(data){
                                    $('.uom_price').append(data);
                                }else{
                                   swal('Warning!','The uom has exist in the database already!','warning');
                                }
                                
                            }
                        });
        });
      });
    
  </script>

    <!-- pop up create sub category -->
    <script type="text/javascript">
      $(function(){
        $('body').on('click','.add-field',function(){
            $('#material_modal').modal('show');
            $("#material_modal .modal-dialog").css('top','0px');
        });
      });

    
    </script>



    <!-- uom -->
    <script type="text/javascript">
        $(function(){
            $('body').on('click','.add_uom',function(){
                jQuery.ajax({
                    url     : "{{ route('menu.add_uom') }}",
                    type    : "POST",
                    success : function(data){
                        $('.uom_price').append(data);
                    },
                    error   : function(data){

                    }
                });
                
                    
            });

            $('body').on('click','.remove_uom_price',function(){
                $(this).parent().remove();
            });

        });


            

    </script>


    <script>
    // material
    $(function(){
        $('body').on('click','.add_material',function(){
            jQuery.ajax({
                    url     : "{{ route('menu.add_material') }}",
                    type    : "POST",
                    success : function(data){
                        $('.Add-menu5').append(data); 
                    },
                    error   : function(data){

                    }
                });
            
        });

        $('body').on('click','.remove-this',function(){
                $(this).parent().parent().remove();
        });
    });
    </script>


    <!-- Check uom validation -->
    <script type="text/javascript">
        $(function(){
            var arr = [];
            var submit = true;
            $('body').on('click','.submit_menu',function(e){
                $(".uom").each(function(index) {
                    var value = $(this).val();
                        if (arr.indexOf(value) == -1){
                            arr.push(value);
                        }
                        else{
                            $(this).addClass("duplicate");
                            submit = false;
                        }

                        if (submit === false) {
                            e.preventDefault();
                        }
                });

                $(".material").each(function(index) {
                    var value = $(this).val();
                        if (arr.indexOf(value) == -1){
                            arr.push(value);
                        }
                        else{
                            $(this).addClass("duplicate");
                            submit = false;
                        }

                        if (submit === false) {
                            e.preventDefault();
                        }
                });
            });

        });
        
    </script>




