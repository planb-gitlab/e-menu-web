@extends("layouts.order")
@section("content")


    <script src="{{ asset('js/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/create_order.css') }}">
    



<div class="container-fluid header">
    <div class="row">
        <div class="col-md-6 module">
                <strong class="title"><i class='{{CRUDBooster::getCurrentModule()->icon}}'></i> {!! $page_title or "Page Title" !!}</strong>
        </div>

        <div class="col-md-6 orderData">
                <div class="table_id" style="display:none;">{{$_GET['id']}}</div>
                <div class="table_code">
                    <span class="order_title">Table : </span>
                    <span class="order_data">{{$_GET['name']}}</span>
                </div>
                <div>
                    <span class="order_title">User : </span>
                    <span class="order_data">{{$user->name}}</span>
                </div> 
        </div>



    </div>
</div>

<div class="container-fluid body">
    <div class="row">
       

        <div class="col-md-2 menuRotate">
            <div class="rotate category">
                Category
            </div>    

            <div class="rotate subcategory">
                Subcategory
            </div>

            <div class="rotate menu">
                Menu
            </div>



        </div>

        <div class="col-md-6">
            <div class="category">
                

                <div class="category_data">
                    <div class="category_item">
                        Food
                    </div>
                     <div class="category_item">
                        Soft Drink
                    </div>
                     <div class="category_item">
                        Hard Drink
                    </div>
                    <div class="category_item">
                        Coffee
                    </div>
                     <div class="category_item">
                        Coffee
                    </div>
                     <div class="category_item">
                        Coffee
                    </div>
                </div>
            </div>
        </div>

         <div class="col-md-5 customer_order">
            <table>
                <thead>
                    <tr>
                        <th>Description</th>
                        <th>Qty</th>
                        <th>Each</th>
                        <th>Amount</th>
                    </tr>
                </thead>


                <tbody>
                    <tr>   
                        <td>Menu 01</td>
                        <td>1</td>
                        <td>$10.00</td>
                        <td>1</td>
                    </tr>

                    <tr>   
                        <td>Menu 02</td>
                        <td>1</td>
                        <td>$20.00</td>
                        <td>1</td>
                    </tr>
                </tbody>

            </table>

        </div>
    </div>
</div> 
@endsection
<script src="{{ asset('js/jquery.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    
    $(document).ready(function() {
    
    var $item = $('div.category_item'), //Cache your DOM selector
        visible = 3, //Set the number of items that will be visible
        index = 0, //Starting index
        endIndex = ( $item.length / visible ) - 1; //End index
    
    $('div#rightArrow').click(function(){
        if(index < endIndex ){
          index++;
          $item.animate({'left':'-=300px'});
        }
    });
    
    $('div#leftArrow').click(function(){
        if(index > 0){
          index--;            
          $item.animate({'left':'+=300px'});
        }
    });
    
});
</script>
