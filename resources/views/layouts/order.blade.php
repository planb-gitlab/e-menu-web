<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>{{ ($page_title)?Session::get('appname').': '.strip_tags($page_title):"Admin Area" }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <meta name='generator' content='CRUDBooster 5.4.6'/>
    <meta name='robots' content='noindex,nofollow'/>
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="shortcut icon"
          href="{{ CRUDBooster::getSetting('favicon')?asset(CRUDBooster::getSetting('favicon')):asset('vendor/crudbooster/assets/logo_crudbooster.png') }}">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta name="viewport" content="width=device-width">
    <!-- Bootstrap 3.3.2 -->
    <link href="{{ asset("vendor/crudbooster/assets/adminlte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css"/>
    <!-- Font Awesome Icons -->
    <link href="{{asset("vendor/crudbooster/assets/adminlte/font-awesome/css")}}/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <!-- Ionicons -->
    @yield('css')

    @stack('head')
</head>
<body>
@yield('content')
@include('crudbooster::admin_template_plugins')

<!-- search modal: category -->
<div class="modal fade search_item_modal" id="category_search_modal" role="dialog" tabindex='-1'>
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close close_popup" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">{{trans('order.search_category')}}</h4>
            </div>

            <div class="modal-body">
              <input type="text" name="search" placeholder="input category here" class="search_data" autocomplete="false">
              <button id="search" type="submit" class="btn btn-default search" data-dismiss="modal">{{trans('order.Search')}}</button>
          </div>

      </div>
  </div>
</div>

<!-- search modal: subcategory -->
<div class="modal fade search_item_modal" id="subcategory_search_modal" role="dialog" tabindex='-1'>
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close close_popup" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">{{trans('order.search_subcategory')}}</h4>
            </div>

            <div class="modal-body">
              <input type="text" name="search" placeholder="input subcategory here" class="search_data" autocomplete="false">
              <button id="search" type="submit" class="btn btn-default search" data-dismiss="modal">{{trans('order.Search')}}</button>
          </div>

      </div>
  </div>
</div>

<!-- search modal: category -->
<div class="modal fade search_item_modal" id="menu_search_modal" role="dialog" tabindex='-1'>
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close close_popup" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">{{trans('order.search_menu')}}</h4>
            </div>

            <div class="modal-body">
              <input type="text" name="search" placeholder="Menu name, #menu code" class="search_data" autocomplete="false">
              <button id="search" type="submit" class="btn btn-default search" data-dismiss="modal">{{trans('order.Search')}}</button>
          </div>

      </div>
  </div>
</div>


<!-- show addon modal -->
<div class="modal fade" id="material_modal" role="dialog" style="padding-right: 0 !important;" tabindex='-1'>
<div class="modal-dialog">

    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">{{trans('order.Material Information')}}</h4>
        </div>
        
        <div class="modal-body">
            <p>{{trans('order.Loading Material detail...')}}</p>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-default materialadd" >{{trans('order.Add to list')}}</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('order.Close')}}</button>
        </div>
    </div>

</div>
</div>

<!-- show broken modal -->
<div class="modal fade" id="broken_modal" role="dialog" style="padding-right: 0 !important;" tabindex='-1'>
<div class="modal-dialog">

    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">{{trans('order.Broken Information')}}</h4>
        </div>
        
        <div class="modal-body">
          <label style="width: 100%;">{{trans('order.Broken Information')}}</label>
          <textarea style="width: 100%;" class="form-control broken-info"></textarea>
          <label style="width: 100%;">{{trans('order.Cost')}}</label>
          <input style="width: 100%;" type='number' step='any' placeholder="broken cost" class="form-control brokencost"/>
      	</div>

      	<div class="modal-footer">
          <button type="button" class="btn btn-default brokenadd" >{{trans('order.Add to list')}}</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('order.Close')}}</button>
      	</div>
  	</div>

</div>
</div>


<!-- note modal -->
<div class="modal fade" id="note_modal" role="dialog" style="padding-right: 0 !important;" tabindex='-1'>
<div class="modal-dialog">

	<div class="modal-content">
	    <div class="modal-header">
	        <h4 class="modal-title">{{trans('order.Note Information')}}</h4>
	    </div>

	    <div class="modal-body">
	        <label style="width: 100%;">{{trans('order.Note Information')}}</label>
	        <textarea style="width: 100%;" class="form-control note_info"></textarea>
	    </div>

	    <div class="modal-footer">
	        <button type="button" class="btn btn-default noteadd"     data-dismiss="modal"  >{{trans('order.Add note')}}</button>
	        <button type="button" class="btn btn-default noteremove"  data-dismiss="modal"  style="display: none;">{{trans('order.Remove Note')}}</button>
	        <button type="button" class="btn btn-default"             data-dismiss="modal"  >{{trans('order.Close')}}</button>
	    </div>
	</div>

</div>
</div>


<!-- order confirmation modal -->
<div class="modal fade" id="order_confirm_modal" role="dialog" style="padding-right: 0 !important;" tabindex='-1'>
<div class="modal-dialog">

  	<div class="modal-content">
	    <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">{{trans('order.Order Confirmation')}}</h4>
	    </div>

	    <div class="modal-body">
	        <div class="main_invoice">
	            <div id="container">
	                
	                <div class="clearfix"></div>

	                    <section id="items">

	                        <table cellpadding="0" cellspacing="0" id="table_order_confirm" style="text-align: center;">
	                            <thead>
	                                <tr>
	                                    <th style="display: none;">{{trans('order.itemId')}}</th>
	                                    <th style="width: 11.5vw !important;">{{trans('order.Description')}}</th>
	                                    <th style="width: 8.5vw !important; text-align: center;">{{trans('order.Quantity')}}</th>
	                                    <th>{{trans('order.Price')}}</th>
	                                    <th>{{trans('order.Subtotal')}}</th>
	                                </tr>
	                            </thead>

	                            <tbody id="table_order_data_confirm"></tbody>
	                            <tbody id="table_order_data_confirm_remove" style="display: none"></tbody>

	                        </table>
	                    </section>

	                    <section id="sums">


	                        <!-- Sub Total Data -->
	                        <div class="border_container">
	                            <div class="confirm_total">
	                                <label class="confirm_total_label length">{{trans('order.Subtotal')}} ({{ $company_currency->currency_symbol }}) : </label>
	                                <span id="confirm_order_total" class="input_length" style="margin-bottom: 0px;">

	                                    <span id="confirm_order_total_data">0.00</span>
	                                    <span>{{ $company_currency->currency_symbol }}</span>

	                                </span>
	                            </div>
	                        </div>



	                        <div class="border_container"> 
	                            <!-- Tax amount and percentage -->
	                            <div class="tax">
	                                <label class="tax_label length">{{trans('order.Tax :')}} </label>
	                                <span class="input_length" @if($currencies->count() <= 0) style="margin-bottom: 0px;" @endif >
	                                    <span class="tax_amount">{{ $tax->company_sale_tax }}</span>
	                                    <span>%</span>
	                                </span>
	                            </div>


	                            <div class="tax_in_money">
	                                <label class="tax_label length">{{trans('order.Tax')}} ({{ $company_currency->currency_symbol }}) : </label>
	                                <span class="input_length" style="margin-bottom: 0">
	                                    <span class="tax_amount_money"></span>
	                                    <span>{{ $company_currency->currency_symbol }}</span>
	                                </span>

	                            </div>
	                        </div>                         


	                        <div class="row">

	                            <!-- Grand Total -->
	                            <!-- Grant total in default currency -->
	                            <div class="border_container">
	                                <div class="confirm_total_complete">
	                                    <label class="confirm_total_label_complete length">{{trans('order.Grand Total')}} ({{$company_currency->currency_symbol}}): </label>
	                                    <span id="confirm_order_total_complete" class="input_length" style="margin-bottom: 0">
	                                        <input type="hidden" id="confirm_order_total_data_complete_hidden" val="0">
	                                        <span id="confirm_order_total_data_complete" >0.00</span>
	                                        <span>{{$company_currency->currency_symbol}}</span>
	                                    </span>
	                                </div>
	                            </div>
	                        </div>

	                    </section>
	            </div>
	        </div>

	    </div>

		<div class="modal-footer">
		  <button type="button" class="btn btn-default order_confirm" href="sender" data-val="order">{{trans('order.Order')}}</button>
		</div>

	</div>

</div>
</div>


<!-- Promotion Pack Info -->
<div class="modal fade" id="promotion_pack_info" role="dialog" style="padding-right: 0 !important;" tabindex='-1'>
<div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	        <div class="modal-header">
	            <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
	            <h4 class="modal-title">{{trans('order.Promotion Pack Information')}}</h4>
	        </div>

	        <div class="modal-body"></div>

	        <div class="modal-footer">
	          <button type="button" class="btn btn-default" data-dismiss="modal" >{{trans('order.Close')}}</button>
		     </div>
		</div>

</div>
</div>


<!-- Print Ticket Info -->
<div class="modal fade" id="print_ticket_modal" role="dialog" style="padding-right: 0 !important;" tabindex='-1'>
<div class="modal-dialog">
      	<!-- Modal content-->
	    <div class="modal-content">
	        <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal">&times;</button>
	            <h4 class="modal-title">{{trans('order.Print Ticket for chef')}}</h4>
	        </div>
	        <div class="modal-body" id="modal-body_printing_ticket"></div>
	        <div class="modal-footer">
	          <button type="button" class="btn btn-default print_ticket_data"             data-dismiss="modal"  >{{trans('order.print')}}</button>
	      	</div>
	  </div>

</div>
</div>
<!-- End of Print Ticket info -->

<!-- menu size Info -->
<div class="modal fade" id="menu_data_modal" role="dialog" style="padding-right: 0 !important;" tabindex='-1'>
<div class="modal-dialog">

	    <div class="modal-content">
	        <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal">&times;</button>
	            <h4 class="modal-title">{{trans('order.Detail Item Order')}}</h4>
	        </div>

	        <div class="modal-body" id="modal-body_menu_size"></div>

	        <div class="modal-footer">
	          <button type="button" class="btn btn-default materialadd" data-dismiss="modal" >{{trans('order.Add to list')}}</button>
	          <button type="button" class="btn btn-default materialnoteadd" style="display:none" data-dismiss="modal" >{{trans('order.Add Note')}}</button>
	          <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('order.Close')}}</button>

	      	</div>
  		</div>

</div>
</div>
<!-- End of menu size Info -->

<!-- menu size Info -->
<div class="modal fade" id="menu_list_data_modal" role="dialog" style="padding-right: 0 !important;" tabindex='-1'>
<div class="modal-dialog">

	    <div class="modal-content">
	        <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal">&times;</button>
	            <h4 class="modal-title">{{trans('order.Detail Item Order')}}</h4>
	        </div>

	        <div class="modal-body" id="modal-body_menu_size"></div>

	        <div class="modal-footer">
	          <button type="button" class="btn btn-default materialnoteadd" style="display:none" data-dismiss="modal" >{{trans('order.Add Note')}}</button>
	          <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('order.Close')}}</button>

	      	</div>
  		</div>

</div>
</div>
<!-- End of menu size Info -->


<!-- paid data -->
<div class="modal fade" id="paid_info" role="dialog" style="padding-right: 0 !important;" tabindex='-1'>
<div class="modal-dialog">

    <div class="modal-content">
        <div class="modal-header" style="background-color: rgb(60, 90, 153); color: rgb(255, 255, 255);">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">{{trans('order.Paid')}}</h4>
        </div>

        <div class="modal-body">
            <div class="row">
                <div class="col-md-12 col-sm-12" style="padding: 0;">

                    <!-- Membership, Discount, Promotion Pack -->
                    <!-- <div class="bargain">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="member_bargain">
                                    {{trans('order.Discount:')}}
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="membership_container bargain_box">
                                    <img src="{{asset('uploads/defualt_image/paid/membership.png')}}" title='{{trans("order.Membership")}}' />
                                </div>
                            </div>


                            @if($user->discount_privilege == 1)
                            <div class="col-md-2">
                                <div class="discount_container bargain_box">
                                   <img src="{{asset('uploads/defualt_image/paid/discount.png')}}" title='{{trans("order.Discount")}}'/>
                               </div>
                           	</div>
                          	@endif


                           	<div class="col-md-2">
	                            <div class="promotion_pack_container bargain_box" style="display: none;">
	                                <img src="{{asset('uploads/defualt_image/paid/promotion.png')}}" title='{{trans("order.Promotion Pack")}}'/>
	                            </div>
                        	</div>
                    	</div>

                    	<div class="clear_container" style="display: none;">
                        	<img src="{{asset('uploads/defualt_image/paid/clear.png')}}" title='{{trans("order.Clear")}}' />
                    	</div>
                	</div> -->

	                <!-- Total Amount -->
	                <div class="border_container">
	                    @if($currencies->count() >= 1)
	                    <div class="grand_total_paid_container">
							
	                        <label class="length_paid">{{trans('order.Grand Total')}} ({{$company_currency->currency_symbol}}): </label>
	                        <span  class="input_length_paid">
	                            <span id="grand_total_paid">0.00</span>
	                            <span>{{$company_currency->currency_symbol}}</span>

	                            <!-- Remain -->
	                            <span id="confirm_remain_amount" class="input_length confirm_remain remain_paid_complete">
	                                <span>( </span>
	                                <span id="confirm_remain_amount_data">0.00</span>
	                                <span>{{ $company_currency->currency_symbol }}</span>
	                                <span> )</span>
	                            </span>
	                        </span>
	                    </div>
	                    @foreach($currencies as $currency)
	                    <div class="grand_total_paid_container">
	                        <label class="length_paid">Grand Total ({{ $currency->currency_symbol }}): </label>
	                        <span  class="input_length_paid" disabled>
	                            <span id="grand_total_paid_{{$currency->id}}">0.00
	                            </span>
	                            <span>{{ $currency->currency_symbol }}</span>


	                            <span id="confirm_remain_amount" class="input_length remain_paid remain_paid_complete" disabled>
	                                <span>( </span>
	                                <span id="confirm_remain_amount_data_{{$currency->id}}">0.00</span>
	                                <span>{{ $currency->currency_symbol }}</span>
	                                <span> )</span>
	                            </span>
	                        </span>
	                    </div>
	                    @endforeach
	                    @else

	                    <div class="grand_total_paid_container">
	                        <label class="length_paid">Grand Total ({{$company_currency->currency_symbol}}): </label>
	                        <span  class="input_length_paid" disabled>
	                            <span id="grand_total_paid">0.00</span>
	                            <span>{{$company_currency->currency_symbol}}</span>

	                            <!-- Remain -->
	                            <span id="confirm_remain_amount" class="input_length confirm_remain">
	                                <span>( </span>
	                                <span id="confirm_remain_amount_data">0.00</span>
	                                <span>{{ $company_currency->currency_symbol }}</span>
	                                <span> )</span>
	                            </span>
	                        </span>
	                    </div>

	                    @endif
	                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 col-sm-12 paid_amount_container" style="padding: 0;">

                <div class="col-md-12 top_navbar" style="padding: 0;" >
                    <!-- <a href="#pay_full_amount" data-toggle="tab" id='nav_pay_full_amount'>
                        <span class="col-md-12 col-xs-12 tabs" >{{ trans('order.Pay Full Amount') }}</span>
                    </a>


                    <a href="#split_payment" data-toggle="tab" id='nav_split_payment'>
                        <span class="col-md-12 col-xs-12 tabs">{{ trans('order.Split Invoice') }}</span>
                    </a> -->


                    <!-- <a href="#merge_payment" data-toggle="tab" id='nav_merge_payment'>
                        <span class="col-md-4 col-xs-3 tabs">{{ trans('order.merge_payment') }}</span>
                    </a> -->
                </div>

                <div class="tab-content border_container">
                	<!-- FULL Payment -->
                    <div class="tab-pane fade in active box_info" id="pay_full_amount">
                        <div class="border_bottom">
                            <div class="row">
                                <div class="confirm_deposit">
                                    @if($currencies->count() >= 1)
                                    	<label class="confirm_deposit_label length" id="confirm_deposit_label">Paid 
                                        <span class="currency_symbol">({{ $company_currency->currency_symbol }})</span>: </label>
                                        <span id="confirm_order_deposit">
                                            <input type="text" name="deposit" id="confirm_order_deposit_data" class="form-control input_length confirm_order_deposit_data" value="" data-val="{{ $company_currency->company_currency }}" placeholder="0.00" onkeypress="return isNumber('confirm_order_deposit_data')">
                                        </span>
                                        @foreach($currencies as $currency)
                                        <label class="confirm_deposit_label length" id="confirm_deposit_label_{{$currency->name}}">Paid 
                                            <span class="currency_symbol">({{ $currency->currency_symbol }})</span>: </label>
                                            <span id="confirm_order_deposit">
                                                <input type="text" name="deposit" id="confirm_order_deposit_data" class="form-control input_length confirm_order_deposit_data" value="" data-val="{{$currency->id}}" placeholder="0.00" onkeypress="return isNumber('confirm_order_deposit_data')">
                                            </span>
                                        @endforeach
                                    @else
                                    <label class="confirm_deposit_label length" id="confirm_deposit_label">{{trans('order.Paid')}} 
                                    <span class="currency_symbol">({{ $company_currency->currency_symbol }})</span>: </label>
                                    <span id="confirm_order_deposit">
                                        <input type="text" name="deposit" id="confirm_order_deposit_data" class="form-control input_length confirm_order_deposit_data" value="" data-val="{{ $company_currency->company_currency }}" placeholder="0.00" onkeypress="return isNumber('confirm_order_deposit_data')" />
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>


                                    
                        @if($currencies->count() >= 1)
	                        <!-- Return -->
	                        <div class="confirm_return" style="display: none;">
	                            <label class="confirm_return_label length">{{trans('order.Return')}}  
	                            <span class="currency_symbol">({{ $company_currency->currency_symbol }})</span>: 
	                        	</label>
	                            <span id="confirm_return_amount" class="form-control input_length" disabled>
	                                <span id="confirm_return_amount_data">0.00</span>
	                                <span>{{ $company_currency->currency_symbol }}</span>
	                            </span>
	                        </div>
	                        @foreach($currencies as $currency)
	                        <div class="confirm_return" style="display: none;">
	                            <label class="confirm_return_label length">{{trans('order.Return')}} 
	                            <span class="currency_symbol">({{ $currency->currency_symbol }})</span>: 
	                        	</label>
	                            <span id="confirm_return_amount" class="form-control input_length" disabled>
	                                <span id="confirm_return_amount_data_{{$currency->id}}">0.00</span>
	                                <span>{{ $currency->currency_symbol }}</span>
	                            </span>
	                        </div>
	                        @endforeach
                        @else
	                        <div class="confirm_return" style="display: none;">
	                            <label class="confirm_return_label length">{{trans('order.Return')}} <span class="currency_symbol">({{ $company_currency->currency_symbol }})</span>: </label>
	                            <span id="confirm_return_amount" class="form-control input_length" disabled>

	                                <span id="confirm_return_amount_data">0.00</span>
	                                <span>{{ $currency->currency_symbol }}</span>
	                            </span>
	                        </div>
                        @endif
						<div class="modal-footer">
							<span type="button" class="btn btn-primary order_print"  data-functionality="p_o_pt_pi" style="display: none"  data-val="order and print">{{trans('order.Paid')}}</span>
							<!-- <button type="button" class="btn btn-danger" data-dismiss="modal"  >{{trans('order.Cancel')}}</button> -->
						</div>
                    </div>

                    <!-- split_payment -->
                    <div class="tab-pane fade in box_info split_payment" id="split_payment">

                    	<div class="row currency_label">
		                    <label class="length" style="padding: 6px 0px;">{{trans('order.Choose your currency')}} :</label>

		                    @if(!$currencies)
			                    <div class="col-md-1 col-sm-1 money money-{{$currency->currency_symbol}}" data-currency='{{$currency->currency_symbol}}'>
			                        <div class="main">
			                            <div class="title">{{$company_currency->currency_symbol}}</div>
			                        </div>
			                    </div>

		                    @else
			                    <div class="col-md-1 col-sm-1 money money-{{$company_currency->currency_symbol}}" data-currency='{{$company_currency->currency_symbol}}'>
			                        <div class="main">
			                            <div class="title">{{$company_currency->currency_symbol}}</div>
			                        </div>
			                    </div>

			                    @foreach($currencies as $currency)
			                    <div class="col-md-1 col-sm-1 money money-{{$currency->currency_symbol}}" data-currency='{{$currency->currency_symbol}}'>
			                        <div class="main">
			                            <div class="title">{{$currency->currency_symbol}}</div>
			                        </div>
			                    </div>
			                    @endforeach

		                    @endif

		           		</div>


		           		<div class="row">

	                        <div class="text-center coverBothInv" style="display: none;">
	                           	<label class="confirm_label">{{trans('order.Invoice No 1 :')}} (<span class='currency_symbol'></span>) </label>
	                           	<span><input type="number" step='any' name="first_receipt" class="form-control confirm_length" id="firstInv" placeholder="0.00"></span>
	                        	<label class="confirm_label">{{trans('order.Invoice No 2 :')}} (<span class='currency_symbol'></span>)</label>
	                        	<span><input type="number" id="secondInv" name="first_receipt" readonly="" class="form-control confirm_length" placeholder="0.00"></span>
	                    	</div>

                    	</div>


                    	
							<div class="modal-footer">
								<button type="button" class="btn btn-default" id="btn_oksplite" name="" value="Split and Paid" style="display: none;">{{trans('order.Split Invoice')}}</button>
							</div>
					</div>


                    <!-- merge_payment -->
                    <!-- <div class="tab-pane fade in box_info" id="merge_payment">
                        <span style="text-align: center; display: block;">In Progress</span>
                    </div> -->
                   

                </div>

            </div>
        </div>
        </div>

    </div>

</div>
</div>

<!-- start paid and print in dinein -->
<div class="modal fade" id="paid_information" role="dialog" style="padding-right: 0 !important;" tabindex='-1'>
<div class="modal-dialog">

    <div class="modal-content">
        <div class="modal-header" style="background-color: rgb(60, 90, 153); color: rgb(255, 255, 255);">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">{{trans('order.Paid')}}</h4>
        </div>

        <div class="modal-body">
            <div class="row">
                <div class="col-md-12 col-sm-12">

                    <!-- Membership, Discount, Promotion Pack -->
                    <div class="bargain">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="member_bargain">
                                    {{trans('order.Discount:')}}
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="membership_container bargain_box">
                                    <img src="{{asset('uploads/defualt_image/paid/membership.png')}}" title='{{trans("order.Membership")}}' />
                                </div>
                            </div>


                            @if($user->discount_privilege == 1)
                            <div class="col-md-2">
                                <div class="discount_container bargain_box">
                                   <img src="{{asset('uploads/defualt_image/paid/discount.png')}}" title='{{trans("order.Discount")}}'/>
                               </div>
                           	</div>
                          	@endif


                           	<div class="col-md-2">
	                            <div class="promotion_pack_container bargain_box" style="display: none;">
	                                <img src="{{asset('uploads/defualt_image/paid/promotion.png')}}" title='{{trans("order.Promotion Pack")}}'/>
	                            </div>
                        	</div>
                    	</div>

                    	<div class="clear_container" style="display: none;">
                        	<img src="{{asset('uploads/defualt_image/paid/clear.png')}}" title='{{trans("order.Clear")}}' />
                    	</div>
                	</div>

	                <!-- Total Amount -->
	                <div class="border_container">
	                    @if($currencies->count() >= 1)
	                    <div class="grand_total_paid_container">

	                        <label class="length_paid">{{trans('order.Grand Total')}} ({{$company_currency->currency_symbol}}): </label>
	                        <span  class="input_length_paid">
	                            <span id="grand_total_paid">0.00</span>
	                            <span>{{$company_currency->currency_symbol}}</span>

	                            <!-- Remain -->
	                            <span id="confirm_remain_amount" class="input_length confirm_remain remain_paid_complete">
	                                <span>( </span>
	                                <span id="confirm_remain_amount_data">0.00</span>
	                                <span>{{ $company_currency->currency_symbol }}</span>
	                                <span> )</span>
	                            </span>
	                        </span>
	                    </div>
	                    @foreach($currencies as $currency)
	                    <div class="grand_total_paid_container">
	                        <label class="length_paid">Grand Total ({{ $currency->currency_symbol }}): </label>
	                        <span  class="input_length_paid" disabled>
	                            <span id="grand_total_paid_{{$currency->id}}">0.00
	                            </span>
	                            <span>{{ $currency->currency_symbol }}</span>


	                            <span id="confirm_remain_amount" class="input_length remain_paid remain_paid_complete" disabled>
	                                <span>( </span>
	                                <span id="confirm_remain_amount_data_{{$currency->id}}">0.00</span>
	                                <span>{{ $currency->currency_symbol }}</span>
	                                <span> )</span>
	                            </span>
	                        </span>
	                    </div>
	                    @endforeach
	                    @else

	                    <div class="grand_total_paid_container">
	                        <label class="length_paid">Grand Total ({{$company_currency->currency_symbol}}): </label>
	                        <span  class="input_length_paid" disabled>
	                            <span id="grand_total_paid">0.00</span>
	                            <span>{{$company_currency->currency_symbol}}</span>

	                            <!-- Remain -->
	                            <span id="confirm_remain_amount" class="input_length confirm_remain">
	                                <span>( </span>
	                                <span id="confirm_remain_amount_data">0.00</span>
	                                <span>{{ $company_currency->currency_symbol }}</span>
	                                <span> )</span>
	                            </span>
	                        </span>
	                    </div>

	                    @endif
	                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 col-sm-12 paid_amount_container">

                <div class="col-md-12 top_navbar">
                    <a href="#pay_full_amount" data-toggle="tab" id='nav_pay_full_amount'>
                        <span class="col-md-6 col-xs-4 tabs active" >{{ trans('order.Pay Full Amount') }}</span>
                    </a>


                    <a href="#split_payment" data-toggle="tab" id='nav_split_payment'>
                        <span class="col-md-6 col-xs-4 tabs">{{ trans('order.Split Invoice') }}</span>
                    </a>


                    <!-- <a href="#merge_payment" data-toggle="tab" id='nav_merge_payment'>
                        <span class="col-md-4 col-xs-3 tabs">{{ trans('order.merge_payment') }}</span>
                    </a> -->
                </div>

                <div class="tab-content border_container">
                	<!-- FULL Payment -->
                    <div class="tab-pane fade in active box_info" id="pay_full_amount">
                        <div class="border_bottom">
                            <div class="row">
                                <div class="confirm_deposit">
                                    @if($currencies->count() >= 1)
                                    	<label class="confirm_deposit_label length" id="confirm_deposit_label">Paid 
                                        <span class="currency_symbol">({{ $company_currency->currency_symbol }})</span>: </label>
                                        <span id="confirm_order_deposit">
                                            <input type="text" name="deposit" id="confirm_order_deposit_data" class="form-control input_length confirm_order_deposit_data" value="" data-val="{{ $company_currency->company_currency }}" placeholder="0.00" onkeypress="return isNumber('confirm_order_deposit_data')">
                                        </span>
                                        @foreach($currencies as $currency)
                                        <label class="confirm_deposit_label length" id="confirm_deposit_label_{{$currency->name}}">Paid 
                                            <span class="currency_symbol">({{ $currency->currency_symbol }})</span>: </label>
                                            <span id="confirm_order_deposit">
                                                <input type="text" name="deposit" id="confirm_order_deposit_data" class="form-control input_length confirm_order_deposit_data" value="" data-val="{{$currency->id}}" placeholder="0.00" onkeypress="return isNumber('confirm_order_deposit_data')">
                                            </span>
                                        @endforeach
                                    @else
                                    <label class="confirm_deposit_label length" id="confirm_deposit_label">{{trans('order.Paid')}} 
                                    <span class="currency_symbol">({{ $company_currency->currency_symbol }})</span>: </label>
                                    <span id="confirm_order_deposit">
                                        <input type="text" name="deposit" id="confirm_order_deposit_data" class="form-control input_length confirm_order_deposit_data" value="" data-val="{{ $company_currency->company_currency }}" placeholder="0.00" onkeypress="return isNumber('confirm_order_deposit_data')" />
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>


                                    
                        @if($currencies->count() >= 1)
	                        <!-- Return -->

	                        <div class="confirm_return" style="display: none;">
	                            <label class="confirm_return_label length">{{trans('order.Return')}}  
	                            <span class="currency_symbol">({{ $company_currency->currency_symbol }})</span>: 
	                        	</label>
	                            <span id="confirm_return_amount" class="form-control input_length" disabled>
	                                <span id="confirm_return_amount_data">0.00</span>
	                                <span>{{ $company_currency->currency_symbol }}</span>
	                            </span>
	                        </div>
	                        @foreach($currencies as $currency)
	                        <div class="confirm_return" style="display: none;">
	                            <label class="confirm_return_label length">{{trans('order.Return')}} 
	                            <span class="currency_symbol">({{ $currency->currency_symbol }})</span>: 
	                        	</label>
	                            <span id="confirm_return_amount" class="form-control input_length" disabled>
	                                <span id="confirm_return_amount_data_{{$currency->id}}">0.00</span>
	                                <span>{{ $currency->currency_symbol }}</span>
	                            </span>
	                        </div>
	                        @endforeach
                        @else
	                        <div class="confirm_return" style="display: none;">
	                            <label class="confirm_return_label length">{{trans('order.Return')}} <span class="currency_symbol">({{ $company_currency->currency_symbol }})</span>: </label>
	                            <span id="confirm_return_amount" class="form-control input_length" disabled>

	                                <span id="confirm_return_amount_data">0.00</span>
	                                <span>{{ $currency->currency_symbol }}</span>
	                            </span>
	                        </div>
                        @endif
						<div class="modal-footer">
							<button type="button" class="btn btn-primary order_print"  data-functionality="p_o_pt_pi" style="display: none" data-val="order and print" disabled="">{{trans('order.Paid')}}</button>
							<button type="button" class="btn btn-success print_invoice" data-val="order and print" data-invoice="{{$invoice_id->invoice_number}}" disabled="">{{trans('order.Print_invoice')}}</button>
							<button type="button" class="btn btn-danger" data-dismiss="modal"  >{{trans('order.Close')}}</button>
						</div>
                    </div>

                    <!-- split_payment -->
                    <div class="tab-pane fade in box_info split_payment" id="split_payment">

                    	<div class="row currency_label">
		                    <label class="length" style="padding: 6px 0px;">{{trans('order.Choose your currency')}} :</label>

		                    @if(!$currencies)
			                    <div class="col-md-1 col-sm-1 money money-{{$currency->currency_symbol}}" data-currency='{{$currency->currency_symbol}}'>
			                        <div class="main">
			                            <div class="title">{{$company_currency->currency_symbol}}</div>
			                        </div>
			                    </div>

		                    @else
			                    <div class="col-md-1 col-sm-1 money money-{{$company_currency->currency_symbol}}" data-currency='{{$company_currency->currency_symbol}}'>
			                        <div class="main">
			                            <div class="title">{{$company_currency->currency_symbol}}</div>
			                        </div>
			                    </div>

			                    @foreach($currencies as $currency)
			                    <div class="col-md-1 col-sm-1 money money-{{$currency->currency_symbol}}" data-currency='{{$currency->currency_symbol}}'>
			                        <div class="main">
			                            <div class="title">{{$currency->currency_symbol}}</div>
			                        </div>
			                    </div>
			                    @endforeach

		                    @endif

		           		</div>


		           		<div class="row">

	                        <div class="text-center coverBothInv" style="display: none;">
	                           	<label class="confirm_label">{{trans('order.Invoice No 1 :')}} (<span class='currency_symbol'></span>) </label>
	                           	<span><input type="number" step='any' name="first_receipt" class="form-control confirm_length" id="firstInv" placeholder="0.00"></span>
	                        	<label class="confirm_label">{{trans('order.Invoice No 2 :')}} (<span class='currency_symbol'></span>)</label>
	                        	<span><input type="number" id="secondInv" name="first_receipt" readonly="" class="form-control confirm_length" placeholder="0.00"></span>
	                    	</div>

                    	</div>


                    	
							<div class="modal-footer">
								<button type="button" class="btn btn-default" id="btn_oksplite" name="" value="Split and Paid" style="display: none;">{{trans('order.Split Invoice')}}</button>
								<button type="button" class="btn btn-default"             data-dismiss="modal"  >{{trans('order.Close')}}</button>
							</div>
					</div>


                    <!-- merge_payment -->
                    <!-- <div class="tab-pane fade in box_info" id="merge_payment">
                        <span style="text-align: center; display: block;">In Progress</span>
                    </div> -->
                   

                </div>

            </div>
        </div>
        </div>

    </div>

</div>
</div>

<!-- end paid and print in dinein -->

<!-- membership -->
<div class="modal fade" id="membership_modal" role="dialog" tabindex='-1'>
<div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" id="modal_item_discount">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">{{trans('order.Membership')}}</h4>
        </div>

        <div class="modal-body" id="modal-body_item_discount_percentage">
            <input type="text" name="membership_no" class='form-control membership_no' placeholder="Please input customer's membership number" class="form-control" autocomplete="off" />
            <button type="button" class="btn btn-default validate_membership">{{trans("order.Validation")}}</button>
            <span class="membership_validate_result"></span>
        </div>
    </div>

</div>
</div>


@if($user->discount_privilege == 1)
<!-- discount -->
<div class="modal fade" id="discount_modal" role="dialog" tabindex='-1'>
<div class="modal-dialog">

    <div class="modal-content" id="modal_item_discount">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">{{trans('order.Discount')}}</h4>
        </div>

        <div class="modal-body" id="modal-body_item_discount_percentage">
             <!-- Discount -->
            <div class="row discount">
                    <label class="length" style="padding: 6px 0px;">{{trans('order.Discount:')}} </label>

                    <div class="col-md-1 col-sm-1 percent">
                        <div class="main">
                            <div class="title">%</div>
                        </div>
                    </div>

                    <div class="col-md-1 col-sm-1 money">
                        <div class="main">
                            <div class="title">{{$company_currency->currency_symbol}}</div>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-6 amount" >
                        <input type="hidden" name="discount_type" id="discount_type">
                        <select name="discount_amount" id="discount_amount" class="form-control input_length discount_amount" disabled>
                            <option value="0" selected="selected">Please select discount amount</option>
                        </select>
                    </div>
            </div>

        </div>

        <div class="modal-footer"></div>

  	</div>

</div>
</div>
@endif


<!-- Promotion Pack -->
<div class="modal fade" id="promotion_pack_modal" role="dialog" tabindex='-1'>
<div class="modal-dialog">

    <div class="modal-content" id="modal_item_discount">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">{{trans('order.Promotion Pack')}}</h4>
        </div>


        <div class="modal-body">
            <label class="length" style="padding: 6px 0px;">{{trans('order.Discount:')}} </label>
            <select name="promotion_pack" id="promotion_pack" class="form-control promotion_pack" disabled>
            </select>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default set_promotion" data-dismiss="modal">{{trans('order.Set Promotion Pack')}}</button>
        </div>


  	</div>

</div>
</div>

@yield('script')
</body>
