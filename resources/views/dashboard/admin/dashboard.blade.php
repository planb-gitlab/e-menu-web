@extends('crudbooster::admin_template')
@section('content')


<div class="panel panel-default">
            <div class="panel-heading">
                <strong class="dashboard_title"><i class='{{CRUDBooster::getCurrentModule()->icon}}'></i>{!! $page_title !!}</strong>
                <strong class="dashboard_setting"><i class='fa fa-cog'></i></strong>
                <!-- @if($created_by == 1)
                <button type="button" class="btn btn-default switch_branch" data-id="<?php echo $get_user_id->id; ?>">
                	{{ trans("dashboard.Change Branch") }}
                </button>
                @endif -->
                <span class="clear"></span>
            </div>
</div>


<div class="container-fluid" style="padding-top: 20px">
            	<div class="row">
            		<div class="col-md-2 dine_in">
            			<a href="{{URL::to('/')}}/admin/dine_in">
	            			<div class="main">
		            			<div class="title">{{ trans("dashboard.Dine In") }}</div>
		            			<hr>
		            			<i class="fa fa-cutlery icon" aria-hidden="true"></i>
		            		</div>
		            	</a>

            		</div>


            		@if($dashboard_settings->menu == 1)
	            	<div class="col-md-2 menu">
		            			
		            			<a href="{{URL::to('/')}}/admin/menus">
			            			<div class="main">
				            			<div class="title">{{ trans("dashboard.Menu") }}</div>
				            			<hr>
				            			
				            			<i class="fa fa-th-large icon" aria-hidden="true"></i>
				            			<br><br>
				            			<span class="item">
				            				@if ($menus)
					            				@if ($menus->count() >= 1)
					            					{{$menus->count()}} {{ trans("dashboard.Items") }}
					            				@elseif ($menus->count() ==1)
					            					{{$menus->count()}} {{ trans("dashboard.Item") }}
					            				@else
					            					0 {{ trans("dashboard.Item") }}
					            				@endif
					            			@endif
				            			</span>
				            		</div>
		            			</a>
		            		
	            		
	            	</div>
					@endif


            		@if(!empty($users))
            		@if($dashboard_settings->user_management == 1)
            		<div class="col-md-2 user">
            			

								<a href="{{URL::to('/')}}/admin/cms_users">     
									<div class="main">
									    
					            			<div class="title">{{ trans("dashboard.User_Management") }}</div>
					            			<hr>
					            			
					            			<i class="fa fa-users icon" aria-hidden="true"></i>
					            			<br><br>
					            			<span class="item">
					            				@if ($users)
						            				@if ($users->count() >= 1)
						            					{{$users->count()}} {{ trans("dashboard.Users") }}
						            				@elseif ($users->count() ==1)
						            					{{$users->count()}} {{ trans("dashboard.Users") }}
						            				@else
						            					0 {{ trans("dashboard.User") }}
						            				@endif
						            			@endif

					            			</span>
				            			
				            		</div>
			            		</a>

		            			
	            		

	            	</div>
	            	@endif

	            	@endif



	            	@if($dashboard_settings->promotion_pack == 1)
	            	<div class="col-md-2 promotion_pack">
	            		<a href="{{URL::to('/')}}/admin/promotion_packs">
		            		<div class="main">
		            			<div class="title">{{ trans("dashboard.Promotion Pack") }}</div>
		            			<hr>
		            			<i class="fa fa-bullhorn icon" aria-hidden="true"></i>
		            			<br><br>
		            			<span class="item">
		            			@if ($promotion_packs)
			            			@if ($promotion_packs->count() >= 1)
					            		{{$promotion_packs->count()}} {{ trans("dashboard.Promotion Packs") }}
					            	@elseif ($promotion_packs->count() == 1)
					            		{{$promotion_packs->count()}} {{ trans("dashboard.Promotion Pack") }}
					            	@else
					            		0 {{ trans("dashboard.Promotion Pack") }}
					            	@endif
					            @endif
				            	</span>
		            			
		            		</div>
	            		</a>
	            	</div>
	            	@endif

	            	@if($dashboard_settings->table_management == 1)
	            	<div class="col-md-2 table_number">
	            		<a href="{{URL::to('/')}}/admin/table_numbers">
		            		<div class="main">
		            			<div class="title">{{ trans("dashboard.Table") }}</div>
		            			<hr>
		            			<i class="fa fa-table icon" aria-hidden="true"></i>
		            			<br><br>
		            			<span class="item">
		            			@if ($tables)
			            			@if ($tables->count() >= 1)
					            		{{$tables->count()}} {{ trans("dashboard.Tables") }}
					            	@elseif ($tables->count() == 1)
					            		{{$tables->count()}} {{ trans("dashboard.Table") }}
					            	@else
					            		0 {{ trans("dashboard.Table") }}
					            	@endif
					            @endif
				            	</span>
		            		</div>
	            		</a>
	            	</div>
	            	@endif

            	
	            	@if(!empty($settings))
            		
	            		@if($dashboard_settings->store_setting == 1)
	            		<div class="col-md-2 setting">
		            		<a href="{{URL::to('/')}}/admin/setting">
			            		<div class="main">
			            			<div class="title">{{ trans("dashboard.Setting") }}</div>
			            			<hr>
			            			<i class="fa fa-cog icon" aria-hidden="true"></i>
			            			
			            			
			            		</div>
		            		</a>
		            	</div>

		            	@endif

	            	@endif

	            	
            	</div>
    </div>


    <!-- setting modal -->
    <div class="modal fade" id="settingmodal" role="dialog">
            <div class="modal-dialog">
            
              	<div class="modal-content">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal">&times;</button>
	                  	<h4 class="modal-title">{{ trans('dashboard.dashboard_setting') }}</h4>
	                </div>
                
                <form id="dashboard_setting" method="post" action="{{ route('insert_dashboard_setting') }}">
                <div class="modal-body menu">
                	
	                	<label>
	                	 	<input type="checkbox" id="user_management" name="user_management"
	                	 	@if($dashboard_settings->user_management == 1) checked="checked" @else @endif/>
	                	 	{{ trans('dashboard.user_management') }}
	                	</label>

	                	<br>

	                	<label>
	                	 	<input type="checkbox" id="menu_management" name="menu_management"
	                	 	@if($dashboard_settings->menu == 1) checked="checked" @else @endif />
	                	 	{{ trans('dashboard.menu_management') }}
	                	</label>

	                	<br>

	                	<label>
	                	 	<input type="checkbox" id="promotion_pack" name="promotion_pack"
	                	 	@if($dashboard_settings->promotion_pack == 1) checked="checked" @else @endif />
	                	 	{{ trans('dashboard.promotion_pack') }}
	                	</label>

	                	<br>

	                	<label>
	                	 	<input type="checkbox" id="table_management" name="table_management"
	                	 	@if($dashboard_settings->table_management == 1) checked="checked" @else @endif />
	                	 	{{ trans('dashboard.table_management') }}
	                	</label>

	                	<br>

	                	<label>
	                	 	<input type="checkbox" id="store_setting" name="store_setting"
	                	 	@if($dashboard_settings->store_setting == 1) checked="checked" @else @endif />
	                	 	{{ trans('dashboard.store_setting') }}
	                	</label>
                	           
                </div>

                <div class="modal-footer">
                  	<button type="submit" class="btn btn-default dashboard_setting_save">Save</button>
                </div>

                </form> 
              </div>
              
            </div>
    </div>

        <!-- End of search modal -->
        <!-- Start change branch model -->
         <div class="modal fade" id="changebranch" role="dialog">
            <div class="modal-dialog">
            
              <!-- Modal content-->
              <div class="modal-content">
              <div class="modal-header header_model_change_branch">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{{ trans("dashboard.Change Branch") }}</h4>
              </div>
              <div class="modal-body all_branch">
              </div>
              
	    	
              </div>
              
            </div>
          </div>



        <!-- End change branch model -->



@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/switch.css') }}">
<link rel="stylesheet" href="{{ asset('css/dashboard/dashboard.css') }}">

<style type="text/css">
.panel-heading{
	overflow:hidden;
}

.stat-value{
	text-decoration: underline;
}

.switch_branch 
{
    position: relative;
    float: right;
}

.clear { clear: left; display: block; }
.header_model_change_branch{
	background: #3C5A99;
	color: #fff;
}
.all_branch{
	overflow: hidden;
}
.box_branch{
	padding: 15px;
	height: auto;
	background: #f6f4f4;
	margin-bottom: 10px;
	box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
}
.box_branch:hover{
	 background: #eee;
}
.branch_name{
	font-size: 20px;
	padding: 15px;
	text-align: center;
	font-weight: 600;
}

.box_branch.main_branch {
    /*background: #83df9e;*/
}

strong.dashboard_title {
    padding: 4px 0;
    display: inline-block;
}


</style>
@endsection


@section('script')
<script type="text/javascript">
	//===================== Cog Icon : Use for opening dashboard customize modal ======================//
	$(function(){
		$('body').on('click','.dashboard_setting',function(){
			$('#settingmodal').modal('show');
            $(".modal-dialog").css("top","170px");
            $("#settingmodal .modal-content").css("box-shadow","0 1px 6px rgba(0, 0, 0, 0.2)");
            $("#settingmodal .modal-content").css("border-radius","0px");
            $("#settingmodal .modal-content").css("border","0px");
            $("#settingmodal .modal-header").css({"background-color":"#3C5A99", "color":"#fff"});
        });
	});


	//===================== Switch Branch Button : Use for opening current branch modal ======================//
	$(function(){
		$('body').on('click','.switch_branch',function(){
				var main_branch_id=$(this).attr("data-id");
				jQuery.ajax({
                        url     : "{{ route('show_box_branch') }}",
                        method  : "POST",
                        data    : {main_branch_id:main_branch_id},
                        dataType: "html",
                        success:function(data){
                            
                            $('#changebranch .modal-body').html(data);
                  			$('#changebranch').modal('show');
                        },
                        error:function(data){
                        	alert('Error request!');
                        }
            });
		});
	});
</script>

@endsection

