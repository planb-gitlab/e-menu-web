@extends('crudbooster::admin_template')
@section('content')
<link rel="stylesheet" href="{{ asset('css/switch.css') }}">

<div class="panel panel-default">
            <!-- Page Title -->
            <div class="panel-heading">
                <strong><i class='{{CRUDBooster::getCurrentModule()->icon}}'></i> {!! $page_title or "Dashboard" !!}</strong>
                <strong class="dashboard_setting"><i class='fa fa-cog'></i></strong>
            </div>
            <!-- End of page Title -->

            
</div>

<div class="container-fluid" style="padding-top: 20px">
            	<div class="row">
            		@if($dashboard_settings->store_owner == 1)
            		<div class="col-md-2 user">


		            				<a href="{{URL::to('/')}}/admin/outlet">     
											<div class="main">
											    
							            			<div class="title">{{ trans('dashboard.Outlet') }}</div>
							            			<hr>
							            			
							            			<i class="fa fa-users icon" aria-hidden="true"></i>
							            			<br><br>
							            			<span class="item">
							            				@if ($outlets->count() >= 1)
							            					{{$outlets->count()}} {{ trans('dashboard.Outlet') }}
							            				@elseif ($outlets->count() ==1)
							            					{{$outlets->count()}} {{ trans('dashboard.Outlet') }}
							            				@else
							            					0 {{ trans('dashboard.Outlet') }}
							            				@endif

							            			</span>
						            			
						            		</div>
					            	</a>
            		

            			
            		</div> <!-- End store Owner -->
            		@endif


		</div>
</div>


 <!-- setting modal -->
        <div class="modal fade" id="settingmodal" role="dialog">
            <div class="modal-dialog">
            
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body menu">
                	<form id="dashboard_setting" method="post">
                	 <label><input type="checkbox" id="store_owner" @if($dashboard_settings->store_owner == 1) checked="checked" @endif
                	 	/>{{ trans('crudbooster.Outlet') }}
                	 </label>

                	 <br>
				                 
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default dashboard_setting_save">Save</button>
                </div>
              </div>
              
            </div>
          </div>

        <!-- End of search modal -->

@endsection

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/dashboard/dashboard.css') }}">

@endsection

@section('script')
<script type="text/javascript">
	$(function(){
		$('body').on('click','.dashboard_setting',function(){
			$('#settingmodal').modal('show');
            
             $(".modal-dialog").css("top","170px");
                    $("#settingmodal .modal-content").css("box-shadow","0 1px 6px rgba(0, 0, 0, 0.2)");
                    $("#settingmodal .modal-content").css("border-radius","0px");
                    $("#settingmodal .modal-content").css("border","0px");
                    $("#settingmodal .modal-header").css({"background-color":"#3C5A99", "color":"#fff"});
                    $("#settingmodal .modal-title").html("Dashboard Setting");
        });
	});
</script>


<script type="text/javascript">
	$(function(){
		$('body').on('click','.dashboard_setting_save',function(){
			var store_owner = $('#store_owner');

			jQuery.ajax({
                        url     : "{{ route('insert_dashboard_setting') }}",
                        method  : "POST",
                        data    : {store_owner:store_owner.prop('checked')},
                        dataType: "html",
                        success:function(data){
                        	location.reload();	
                           
                            
                        }
            });
		});
	});
</script>

@endsection