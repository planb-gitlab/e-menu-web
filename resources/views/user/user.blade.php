@extends("layouts.create_update")
@section("content")
	<form class='form-horizontal' method='post' id="form" enctype="multipart/form-data" action="{{(@!$row->id)?
                                route('save.user'):
                                route('update.user')
                            }}"> 
		<input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <input type='hidden' name='return_url' value='{{ @$return_url }}'/>
        <input type='hidden' name='ref_mainpath' value='{{ CRUDBooster::mainpath() }}'/>
        <input type='hidden' name='ref_parameter' value='{{urldecode(http_build_query(@$_GET))}}'/>
        @if($hide_form)
            <input type="hidden" name="hide_form" value='{!! serialize($hide_form) !!}'>
        @endif
            <input type="hidden" name="id" value="{{$row->id}}" />


           <div class="row">
           <div class="col-md-12">
           <div class="box-body">

                <!-- NAME -->
           		<div class="form-group header-group-0" id="form-group_user_name">
                            <label class="control-label col-sm-2">{{trans('user.Name')}}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-sm-6">
                               <input type="text" name="name" class="form-control" required value="{{ $user->name }}" placeholder="Please enter your user's name" autocomplete="off">
                            </div>
                </div>

                <!-- PHOTO -->
                <div class="form-group header-group-0" id="form-group_user_photo">
                            <label class="control-label col-sm-2">{{trans('user.Photo')}}
                            <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <div class="col-sm-6">
                                 @if(empty($user->photo))
                                    <input type="file" class="form-control" required name="photo" id="photo" accept="image">         
                                    <div style="color:#908e8e; margin-top: 5px;"> {{ trans('setting.file_image_require') }}</div>
                                @else
                                    <p><a data-lightbox="roadtrip" href="{{$url}}/{{$user->photo}}">
                                        <img style="max-width:160px" title="{{$user->photo}}" src="{{$url}}/{{$user->photo}}">
                                    </a></p>
                                    
                                    <input type="hidden" name="photo" value="{{$table->photo}}">                   
                                        <p><a class="btn btn-danger btn-delete btn-sm" onclick="if(!confirm('Are you sure ?')) return false" href="{{$url}}/admin/cms_users/delete-image?image={{$user->photo}}&amp;id={{$user->id}}&amp;column=photo"><i class="fa fa-ban"></i> {{ trans('setting.text_delete') }} </a></p>
                                        <p class="text-muted"><em>{{ trans('setting.remove_image_first') }}</em></p>
                                     <div class="text-danger"></div>
                                @endif
                            </div>
                </div>

                <!-- PHONE NUMBER -->
                <div class="form-group header-group-0" id="form-group_user_phone">
                            <label class="control-label col-sm-2">{{trans('user.Phone Number')}}</label>
                            <div class="col-sm-6">
                               <input type="number" name="phone" class="form-control" value="{{ $user->phone }}" placeholder="Please enter user's phone number" autocomplete="off">
                            </div>
                </div>

                <!-- ROLES -->
                <div class="form-group header-group-0" id="form-group_user_role">
                            <label class="control-label col-sm-2">{{trans('user.Role')}}
                            <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <div class="col-sm-6">
                               <select name="role" class="form-control" required>
                                   <option value="">**Please select user's role</option>
                                   @foreach($roles as $role)
                                        <option value="{{$role->id}}" @if($user->id_cms_privileges == $role->id) selected @endif>{{$role->name}}</option>
                                   @endforeach
                               </select>
                            </div>
                </div>

                <!-- PIN -->
                <div class="form-group header-group-0" id="form-group_user_pin">
                            <label class="control-label col-sm-2">{{trans('user.PIN')}}
                            <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <div class="col-sm-6">
                                <input type="number" name="user_pin" class="form-control" id="user_pin" value="{{ $user->user_pin }}" placeholder="Please enter user's pin number" autocomplete="off" required>
                                <div style="color:#908e8e; margin-top: 5px;"> {!! trans('user.pin_info') !!}</div>
                            </div>
                            <div class="col-md-4">
                                <button type="button" class="btn generate_pin admin_generate_pin" style="display: inline;">{{trans('user.Generate Pin')}}</button>
                            </div>
                </div>

           </div>
           </div>
           </div>

           <div class="box-footer" style="background: #F5F5F5">

                                    <div class="form-group">
                                        <label class="control-label col-sm-2"></label>
                                        <div class="col-sm-10">
                                            @if($button_cancel && CRUDBooster::getCurrentMethod() != 'getDetail')
                                                @if(g('return_url'))
                                                    <a href='{{g("return_url")}}' class='btn btn-default'><i
                                                                class='fa fa-chevron-circle-left'></i> {{trans("crudbooster.button_back")}}</a>
                                                @else
                                                    <a href='{{CRUDBooster::mainpath("?".http_build_query(@$_GET)) }}' class='btn btn-default'><i
                                                                class='fa fa-chevron-circle-left'></i> {{trans("crudbooster.button_back")}}</a>
                                                @endif
                                            @endif
                                            @if(CRUDBooster::isCreate() || CRUDBooster::isUpdate())

                                                @if(CRUDBooster::isCreate() && $command == 'add')
                                                    <input type="submit" name="submit" value='{{trans("crudbooster.button_save_more")}}' class='btn btn-success'>
                                                @endif

                                                @if($button_save && $command != 'detail')
                                                    <input type="submit" name="submit" value='{{trans("crudbooster.button_save")}}' class='btn btn-success save'>
                                                @endif

                                            @endif
                                        </div>
                                    </div>

            </div>

	</form>
@endsection

@section('script')
<script type="text/javascript">
    $(function(){
        $('body').on('click','.admin_generate_pin',function(){
            var pin = Math.floor(1000 + Math.random() * 9000);
            $('#user_pin').val(pin);
            var pin_type    = 'user';

                jQuery.ajax({
                    url     : "{{ route('pin_validation') }}",
                    type  : "POST",
                    data    : {pin : pin,pin_type:pin_type},
                    success : function(data){
                        if(data.status == "good"){
                            $('#user_pin').css({'background':'green','color':"#fff"});    
                        }else{
                            $('#user_pin').css({'background':'#e94f4f','color':"#fff"});
                        }
                    },
                    error : function(data){
                        swal("Warning", "There must be a problem with the request ! Please check again", "warning");
                    }
                });

        });

        $('body').on('keyup','#user_pin',function(){
            $('#user_pin').css({'background':'#e94f4f','color':"#fff"});
            var pin         = $(this).val();
            var pin_type    = 'user';
            if(pin == ''){
                $('#user_pin').css({'background':'none','color':"#333"});
            }

            if(pin.length == 4){
                    jQuery.ajax({
                        url     : "{{ route('pin_validation') }}",
                        type  : "POST",
                        data    : {pin : pin,pin_type:pin_type},
                        success : function(data){
                            if(data.status == "good"){
                                $('#user_pin').css({'background':'green','color':"#fff"});
                            }else{
                                $('#user_pin').css({'background':'#e94f4f','color':"#fff"});
                            }
                        },
                        error : function(data){
                            swal("Warning", "There must be a problem with the request ! Please check again", "warning");
                        }
                    });
            }


        });

    });

</script>
@endsection