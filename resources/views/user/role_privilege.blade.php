@extends('crudbooster::admin_template')
@section('content')

    <!-- ===================== go back to previous page ===================== -->
    @if(CRUDBooster::getCurrentMethod() != 'getProfile' && $button_cancel)
            @if(g('return_url'))
                <p><a title='Return' href='{{g("return_url")}}'><i class='fa fa-chevron-circle-left '></i>
                        &nbsp; {{trans("crudbooster.form_back_to_list",['module'=>CRUDBooster::getCurrentModule()->name])}}</a></p>
            @else
                <p><a title='Main Module' href='{{CRUDBooster::mainpath()}}'><i class='fa fa-chevron-circle-left '></i>
                        &nbsp; {{trans("crudbooster.form_back_to_list",['module'=>CRUDBooster::getCurrentModule()->name])}}</a></p>
            @endif
    @endif

    <div class="panel panel-default">
            <!-- ===================== Page Title ===================== -->
            <div class="panel-heading">
                <strong><i class='{{CRUDBooster::getCurrentModule()->icon}}'></i> {!! $page_title or "Page Title" !!}</strong>
            </div>

            <div class="panel-body" style="padding: 20px 0 0 0">

                <form class='form-horizontal' method='post' id="form" enctype="multipart/form-data" action="{{ (@$row->id)?
                    route('update_role'):
                    route('save_role') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type='hidden' name='return_url' value='{{ @$return_url }}'/>
                <input type='hidden' name='ref_mainpath' value='{{ CRUDBooster::mainpath() }}'/>
                <input type='hidden' name='ref_parameter' value='{{urldecode(http_build_query(@$_GET))}}'/>
                @if($hide_form)
                    <input type="hidden" name="hide_form" value='{!! serialize($hide_form) !!}'>
                @endif

                <input type="hidden" class="form-control" name="role_id"  value="{{ $row->id }}">
    
                <div class="box-body" style="width:1000px;margin:0 auto">

                    <!-- ===================== Role Name ===================== -->
                    <div class="form-group header-group-0 ">
                        <label class="control-label col-sm-2" style="text-align: left; padding:0; padding-top: 7px;">{{trans('usermanagement.Role Name')}}
                        <span class="text-danger" title="{{trans('usermanagement.This field is required')}}">*</span></label>
                        <div class="col-sm-10">
                            <input type="text" title="{{trans('usermanagement.Role Name')}}" required class="form-control" id="role" name="role" value="{{$roles->name}}" placeholder="You can only enter the letter only">
                        </div>
                    </div>


                    <div id="privileges_configuration" class="form-group">
                    <label style="text-align: center; font-size: 20px; margin-top: 20px; margin-bottom: 30px;">
                        {{trans('usermanagement.Privileges Configuration')}}
                    </label>

                    
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr class="active">
                                <th width='3%'>{{trans('role.privileges_module_list_no')}}</th>
                                <th width='50%'>{{trans('role.privileges_module_main_list')}}</th>
                                <th width='47%'>{{trans('role.privileges_module_sub_list')}}</th>
                                <th class="center">{{trans('role.privileges_module_list_view')}}</th>
                            </tr>
                            <tr class="info">
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                                <th class="center"><input title="{{trans('usermanagement.Check all vertical')}}" type="checkbox" id="is_visible"></th>
                            </tr>
                        </thead>
                         <tbody>

                           @foreach($main_moduls as $key => $main)
                           <?php 
                           $roles_main = DB::table('cms_privileges_roles')->where('id_cms_moduls', $main->modul_id)->where('id_cms_privileges', $row->id)->first(); 
                           ?>
                           <tr class="main_moduls">
                            <td class="center">{{$key+1}}</td>
                            <td>{{$main->name}}</td>
                            <td>&nbsp;</td>
                            @if(!$main->is_only_menu)
                                <td class="active center"> <input type="checkbox" class="is_visible"   
                                    name="privileges[{{$main->id}}][is_visible]"  <?=@$roles_main->is_visible ? "checked" : ""?>
                                    value="1"></td>
                            @else
                            <td>&nbsp;</td>

                            @endif
                            <?php
                                $moduls = DB::table('cms_menus')
                                                ->leftjoin('cms_moduls','cms_moduls.name','cms_menus.name')
                                                ->select('cms_moduls.*','cms_moduls.id as modul_id','cms_menus.*','cms_menus.id as menu_id')
                                                ->where('cms_menus.is_active','1')->where('cms_menus.super_admin',null)
                                                ->where('parent_id',$main->menu_id)->orderby('cms_menus.name','ASC')->get();

                            ?>   

                            @foreach($moduls as $modul)
                            <?php 
                            $roles = DB::table('cms_privileges_roles')->where('id_cms_moduls', $modul->modul_id)->where('id_cms_privileges', $row->id)->first(); ?>

                            <tr id="{{$modul->name}}">
                                
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>{{$modul->name}}</td>
                                <td class="active center">  <input type="checkbox" class="is_visible"   
                                    name="privileges[{{$modul->id}}][is_visible]"  <?=@$roles->is_visible ? "checked" : ""?>
                                    value="1"></td>

                            </tr>
                            @endforeach
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
                </div>

                <div class="box-footer" style="background: #F5F5F5">
                    <div class="form-group">
                        <label class="control-label col-sm-2"></label>
                        <div class="col-sm-10">
                            @if($button_cancel && CRUDBooster::getCurrentMethod() != 'getDetail')
                                @if(g('return_url'))
                                    <a href='{{g("return_url")}}' class='btn btn-default'><i
                                                class='fa fa-chevron-circle-left'></i> {{trans("crudbooster.button_back")}}</a>
                                @else
                                    <a href='{{CRUDBooster::mainpath("?".http_build_query(@$_GET)) }}' class='btn btn-default'><i
                                                class='fa fa-chevron-circle-left'></i> {{trans("crudbooster.button_back")}}</a>
                                @endif
                            @endif
                            @if(CRUDBooster::isCreate() || CRUDBooster::isUpdate())

                                @if(CRUDBooster::isCreate() && $button_addmore==TRUE && $command == 'add')
                                    <input type="submit" name="submit" value='{{trans("crudbooster.button_save_more")}}' class='btn btn-success'>
                                @endif

                                @if($button_save && $command != 'detail')
                                    <input type="submit" name="submit" value='{{trans("crudbooster.button_save")}}' class='btn btn-success'>
                                @endif

                            @endif
                        </div>
                    </div>
                </div>


            </form>

        </div>
    </div>
<div class="se-pre-con-detail"></div>
@endsection

@section('css')
<style type="text/css">
    .table-bordered{
        box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
    }

    .main_moduls{
        background: #c1d0f0 !important;
    }

    .center {
        text-align:  center;
    }
</style>

<link rel="stylesheet" href="{{ asset('css/switch.css') }}">
<link rel="stylesheet" href="{{ asset('css/loading.css') }}">

@endsection

@section('script')
<!-- ======================= Column Tick ============================== -->
<script src="{{ asset('js/role_privilege.js') }}" type="text/javascript"></script>

<!-- ======================= Loading ============================== -->
<script>
    $(function() {
         $(".se-pre-con-detail").fadeOut("slow");;
    });
</script>

<!-- ======================= Column Tick ============================== -->
<script>
    $(function () {
        $("#is_visible").click(function () {
            var is_check = $(this).prop('checked');
            $(".is_visible").prop("checked", is_check);
        })
})

</script>
@endsection



