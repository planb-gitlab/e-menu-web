@extends('layouts.configuration')
@section('content')
<body id="configuration_page">
    <!-- ================ TOP TITLE ================= -->
    <div class="container" id="configuration_setup">
        <div class="row page_data">    
                <div class="page_title col-md-9 col-xs-9">
                    {{ trans('dashboard_configure.configuration_title') }}
                </div>
                <div class="col-md-3 col-xs-3 logout">
                    <i class="fa fa-sign-out"></i> <br/>
                    {{ trans('dashboard_configure.button_logout') }}
                </div>
        </div>   
    </div>

    
    <!-- ================ Form Input ================= -->
    <div class="container">
        @if(@$alerts)
                @foreach(@$alerts as $alert)
                    <div class='callout callout-{{$alert["type"]}}'>
                        {!! $alert['message'] !!}
                    </div>
                @endforeach
        @endif


        @if (Session::get('message')!='')
                <div class='alert alert-{{ Session::get("message_type") }}'>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-info"></i> {{ trans("crudbooster.alert_".Session::get("message_type")) }}</h4>
                    {!!Session::get('message')!!}
                </div>
        @endif


        <form class="form-horizontal" method='post' id="store_configuration" 
            enctype="multipart/form-data" action="{{ route('store_configuration') }}">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type='hidden' name='return_url' value='{{ @$return_url }}'/>
        <input type='hidden' name='ref_mainpath' value='{{ CRUDBooster::mainpath() }}'/>
        <input type='hidden' name='ref_parameter' value='{{urldecode(http_build_query(@$_GET))}}'/>
        @if($hide_form)
            <input type="hidden" name="hide_form" value='{!! serialize($hide_form) !!}'>
        @endif

        <input type="hidden" class="form-control" name="company_id"  value="{{ $settings->id }}">

        <div class="col-md-12 col-xs-12 configuration_block">
    	    <div class="row setting_info">
            <div id="company_info" class="container-fluid box_info">
                <!-- ================ General Information ================= -->
                <div class="row">
                    <div class="col-md-12 col-xs-12 block_title">
                        <div class="col-md-10">
                            <i class="fa fa-info-circle icon_title"></i>
                            {{ trans('dashboard_configure.general_information') }}
                        </div>
                        <div class="col-md-2" style="float:right; text-align: right;">
                            1
                        </div>
                    </div>
                <div class="col-md-12">
                <div class="box-body">
                        
                        <div class="form-group header-group-0" id="form-group-company_name">
                            <label class="control-label col-sm-3">{{ trans('dashboard_configure.company_name') }}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-md-9">
                               <input type="text" name="company_name" class="form-control company_name" placeholder="Company Name" id="company_name" value="{{$settings->company_name}}" data-valid="0" required autocomplete="off">
                            </div>
                        </div>

                        <div class="form-group header-group-0" id="form-group-company_address">
                            <label class="control-label col-sm-3">{{ trans('dashboard_configure.company_address') }}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-md-9">
                               <input type="text" name="company_address" class="form-control company_address" required placeholder="Company Address (Location, Street, District, Commune, City)" id="company_address" value="{{$settings->company_address}}" data-valid="0" autocomplete="off">
                            </div>
                        </div>


                        <div class="form-group header-group-0" id="form-group-company_country">
                            <label class="control-label col-sm-3">{{ trans('dashboard_configure.company_country') }}
                                <span class="text-danger" title="This field is required">*</span>
                            </label>
                            <div class="col-md-9">

                               <select name="company_country" class="form-control company_country" required id='company_country' data-valid="0">
                                    <option value=''>{{ trans('dashboard_configure.Please select your country') }}</option>
                                    @foreach($countries as $country)
                                    <option value="{{$country->id}}" @if($settings->company_country == $country->id) selected="selected" @endif>{{$country->country_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group header-group-0" id="form-group-company_logo">
                            <label class="control-label col-sm-3">{{ trans('dashboard_configure.company_logo') }}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-sm-6">
                               @if(empty($settings->company_logo))
                               <input type="file" class="form-control" name="company_logo" id="company_logo" accept="image/*" required data-valid="0" > 
                                <div style="color:#908e8e; margin-top: 5px;"> {{ trans('setting.file_image_require') }}</div>
                               @else
                                <p>
                                    <a data-lightbox="roadtrip" href="{{$url}}/{{$settings->company_logo}}"><img style="max-width:160px" title="{{$settings->company_logo}}" src="{{$url}}/{{$settings->company_logo}}"></a></p>
                                
                                <input type="hidden" name="menu_photo" value="{{$settings->company_logo}}">                   
                                    <p><a class="btn btn-danger btn-delete btn-sm" onclick="if(!confirm('Are you sure ?')) return false" href="{{$url}}/admin/setting/delete-image?image={{$settings->company_logo}}&amp;id={{$settings->id}}&amp;column=company_logo"><i class="fa fa-ban"></i> {{ trans('crudbooster.text_delete') }} </a></p>
                                    <p class="text-muted"><em>{{ trans('crudbooster.remove_image_first') }}</em></p>
                                 <div class="text-danger"></div>
                               @endif
                            </div>

                        </div>

                        <div class="form-group header-group-0" id="form-group-company_info">
                            <label class="control-label col-sm-3">{{ trans('dashboard_configure.company_info') }}</label>
                            <div class="col-md-9">
                               <input type="text" name="company_info" class="form-control company_information" placeholder="Company Information" id="company_info" value="{{$settings->company_info}}" data-valid="0" autocomplete="off">
                            </div>
                        </div>

                        <div class="form-group header-group-0" id="form-group-company_email">
                            <label class="control-label col-sm-3">{{ trans('dashboard_configure.company_email') }}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="input-group col-md-9">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <input type="email" title="Company Email" required maxlength="255" class="form-control company_email" name="company_email" id="company_email" value="{{$settings->company_email}}" placeholder="Please input your company email" data-valid="0" autocomplete="off">
                            </div>
                        </div>

                        <div class="form-group header-group-0" id="form-group-company_phone">
                            <label class="control-label col-sm-3">{{ trans('dashboard_configure.company_phone') }}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-md-9">
                               <input type="number" name="company_phone" class="form-control company_phone" required placeholder="Company Phone" id="company_phone" value="{{$settings->company_phone}}" data-valid="0" autocomplete="off">
                            </div>
                        </div>

                        <div class="form-group header-group-0" id="form-group-company_contact">
                            <label class="control-label col-sm-3">{{ trans('dashboard_configure.company_contact') }}</label>
                            <div class="col-md-9">
                               <input type="text" name="company_contact" class="form-control company_contact" placeholder="Company Contact" id="company_contact" value="{{$settings->company_contact}}" data-valid="0" autocomplete="off">
                            </div>
                        </div>

                        <div class="form-group header-group-0" id="form-group-company_website">
                            <label class="control-label col-sm-3">{{ trans('dashboard_configure.company_website') }}
                            </label>
                            <div class="col-md-9">
                               <input type="text" name="company_website" class="form-control company_website" placeholder="Company Website" id="company_website" value="{{$settings->company_website}}" data-valid="0" autocomplete="off">
                            </div>
                        </div>

                        <div class="form-group header-group-0" id="form-group-company_language">
                            <label class="control-label col-sm-3">{{ trans('dashboard_configure.company_language') }}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-md-9">
                               <select name="company_language" class="form-control company_language" required="" id='company_language' data-valid="0">
                                    <option value="">{{ trans('dashboard_configure.Please select your language') }}</option>
                                    @foreach($languages as $language)
                                    <option value="{{$language->language_code}}" @if($settings->company_lang == $language->language_code) selected="selected" @endif>({{$language->language_code}}) {{$language->language_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group header-group-0" id="form-group-company_pin">
                            <label class="control-label col-sm-3">{{ trans('dashboard_configure.company_pin') }}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-md-9">
                                
                               <input type="company_pin" name="company_pin" class="form-control company_pin" id="company_pin" value="" maxlength="4" placeholder="Please input your company pin (Note* : 4 numbers digit only)" required autocomplete="off">
                               <button type="button" class="btn generate_pin">Generate Pin</button>
                               <div style="color:#908e8e; margin-top: 5px;"> {{ trans('dashboard_configure.pin_info') }}</div>
                            </div>
                        </div>

                    </div>
                    </div>
                    </div>

            </div> <!-- GENERAL INFORMATION -->
            
            
            <!-- ================= TAX Information ================= -->
            <div id="tax" class="container-fluid box_info">
                <div class="row">
                  <div class="col-md-12 col-xs-12 block_title">
                        <div class="col-md-10">
                            <i class="fa fa-percent icon_title"></i>
                            {{ trans('dashboard_configure.tax') }}
                        </div>
                        <div class="col-md-2" style="float:right; text-align: right;">
                            2
                        </div>
                  </div>
                <div class="col-md-12">
                <div class="box-body">
                    <div class="form-group header-group-0" id="form-group-company_have_tax">
                        <label class="control-label col-sm-3">{{ trans('dashboard_configure.Include Tax') }}
                        <span class="text-danger" title="This field is required">*</span></label>
                        <div class="col-md-9 have_tax_container">
                          <input type="checkbox" id="have_tax" name="have_tax" class="have_tax" style="position: relative;   top: 7px;"/>

                        </div>
                    </div>


                    <div class="form-group header-group-0" id="form-group-company_sale_tax" style="display: none;">
                        <label class="control-label col-sm-3">{{ trans('dashboard_configure.company_sale_tax') }}
                        <span class="text-danger" title="This field is required">*</span></label>
                        <div class="col-md-9">
                           <input type="text" name="company_sale_tax" class="form-control company_sale_tax" placeholder="Company Sale Tax" id="company_sale_tax" value="{{$settings->company_sale_tax}}" data-valid="0">
                        </div>
                    </div>

                    <div class="form-group header-group-0" id="form-group-tax_type" style="display: none;">
                        <label class="control-label col-sm-3">{{ trans('dashboard_configure.tax type') }}
                        <span class="text-danger" title="{{trans('setting.This field is required')}}">*</span></label>
                        <div class="col-md-9">
                        <select class="form-control include_tax_type" name="Tax_type">
                            <option value="1" @if($settings->company_tax_type_id == 1) selected @endif>{{ trans('dashboard_configure.Tax Invoice') }}</option>
                            <option value="2" @if($settings->company_tax_type_id == 2) selected @endif>{{ trans('dashboard_configure.Commercial Invoice') }}</option>
                       </select>
                        </div>
                    </div>        


                </div>
                </div>
                </div>

            </div> <!-- TAX -->

            <!-- ==================== Currency Information ============================ -->
            <div id="currency" class="container-fluid box_info">
                    <div class="row">
                            <div class="col-md-12 col-xs-12 block_title">
                                <div class="col-md-10">
                                    <i class="fa fa-money icon_title"></i>
                                    {{ trans('dashboard.currency') }}
                                </div>
                                <div class="col-md-2" style="float:right; text-align: right;">
                                    3
                                </div>
                            </div>
                        <div class="col-md-12">
                        <div class="box-body">
                            <div class="form-group header-group-0" id="form-group-company_currency">
                                <label class="control-label col-sm-3">{{ trans('dashboard_configure.company_currency') }}
                                <span class="text-danger" title="This field is required">*</span>
                                </label>
                                <div class="col-md-9">
                                   <select name="company_currency" class="form-control company_currency" required="" id='company_currency' data-valid="0">
                                        <option value="">{{ trans('dashboard_configure.Please select default currency') }}</option>
                                        @foreach($currencies as $currency)
                                        <option value="{{$currency->id}}" @if($settings->company_currency == $currency->id) selected="selected" @endif >{{$currency->name}}</option>

                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
            </div><!-- CURRENCY -->

            <!-- ==================== Subscription Information ============================ -->
            <div id="subscription" class="container-fluid box_info">
                    <div class="row">
                        <div class="col-md-12 col-xs-12 block_title">
                            <div class="col-md-10">
                                <i class="fa fa-newspaper-o icon_title"></i>
                                {{ trans('dashboard_configure.subscription') }}
                            </div>
                            <div class="col-md-2" style="float:right; text-align: right;">
                                4
                            </div>
                        </div>
                    <div class="col-md-12">
                    <div class="box-body">
                            <div class="form-group header-group-0" id="form-group-subscription_id">
                                <label class="control-label col-sm-3">{{ trans('dashboard_configure.subscription') }}
                                <span class="text-danger" title="This field is required">*</span></label>
                                <div class="col-md-9">
                                    <select name="subscription_id" class="form-control subscription_id" required="" id='subscription_id' data-valid="0">
                                        <option value="">{{ trans("dashboard_configure.Please select client's subscription") }}</option>
                                        @foreach($subscription as $s)
                                        <option value="{{$s->id}}">{{$s->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="box-footer" >
                                        <div class="form-group">
                                            <label class="control-label col-sm-3"></label>
                                            <div class="col-sm-12 save_button">           
                                                        <input type="submit" name="submit" value='{{trans("crudbooster.button_save")}}' class='btn btn-success save'>
                                            </div>
                                        </div>
                            </div><!-- /.box-footer !-->
                    </div>
                    </div>
                    </div>
            </div><!-- Subscription -->


        </div><!-- setting_info -->
        </div>
        </form>

</div>
</body>

@endsection

@section('css')
<style type="text/css">
    .tabs{
        background: #fff;
        padding: 20px;
        box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
        margin: 0 10px 0 0;
        text-align: center;
        font-size: 16px;
    }

    .left_navbar {
        margin-right: 2vw;
        float:  left;
        padding-left: 0 !important;
        padding-right: 0px !important;
    }

    a .tabs{
        margin-bottom: 1vw;
    }

    .box_info{
        background: #fff;
        padding: 0px 20px 0 20px;
        box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
        margin-bottom: 20px;

    }


    .input-group[class*=col-]{
        padding-left: 15px !important;
        padding-right: 15px !important;
    }

    .page_title {
        background: #fff;
        padding: 20px;
        box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
        margin-bottom: 20px;
        text-align:  center;
        font-size: 20px;
    }


    body#configuration_page {
        /*background:url('{{ asset('../storage/app/uploads/defualt_image/configuration_background.png')  }}');*/
        background-size: 23%;
    }

    .box-footer {
        text-align: right;
        padding-right: 0 !important;
    }

    .disabled{
        color: #808080;
        background: #ecf0f5;
    }

    input#company_pin {
      width: 75.66%;
      display: inline-block;
    }

    button.btn.generate_pin {
      display: inline;
      margin-top: -3px;
    }




</style>
@endsection

@section('script')


<!-- =============================== Logout Account on page store configuration =============================== -->
<script>
    $(function(){
        $('body').on('click','.logout',function(){
            jQuery.ajax({
                url     : "{{route('logout')}}",
                method  : 'POST',
                success : function(data){
                    location.reload();
                }
            });
        });
    });
</script>



<!-- =============================== Tax Display ===============================-->
<script>
    $(function(){
        $('body').on('change','#tax',function(){
        var tax_check = $('#tax');

        if(tax_check.prop('checked') == true){
            $('.tax_info').css('display','block');
        }else{
            $('.tax_info').css('display','none');
            $('#sale_tax').val('');
        }

        });
       
    });
</script>



<!-- =============================== Check PIN (If exist from PLANB server) =============================== -->
<script>
  $(function(){

    $('body').on('click','.generate_pin',function(){
        var pin         = Math.floor(1000 + Math.random() * 9000);
        $('#company_pin').val(pin);
        var pin         = $('#company_pin').val();
        var pin_type    = 'outlet';
        
        jQuery.ajax({
            url         : "{{ route('pin_validation') }}",
            method      : "POST",
            data        :   {    
                            pin : pin ,
                            pin_type : pin_type 
                        },
            beforeSend  : function(){},
            success     : function(data){
                if(data.status == "good"){
                    $('#company_pin').css({"background":"#40a669","color":"#fff","border-color":"#3d9d64"});
                    $('.next_tax').removeAttr('disabled','disabled');     
                }else{
                    $('#company_pin').css({"background":"#a64040","color":"#fff","border-color":"#9d3d3d"});
                    $('.next_tax').attr('disabled','disabled');
                }
            },
            error : function(data){}
        });
    });
  });
</script>

<script>
$(function(){
    $('body').on('keyup','#company_pin',function(){
        var pin         = $(this).val();
        var pin_type    = 'outlet';
        if(company_pin.length == 4){
            jQuery.ajax({
                url     : "{{ route('pin_validation') }}",
                method  : "POST",
                data    : {pin : pin, pin_type:pin_type},
                beforeSend : function(){},
                success : function(data){
                  if(data.status == "good"){
                     $('#company_pin').css({"background":"#40a669","color":"#fff","border-color":"#3d9d64"});
                     $('.next_tax').removeAttr('disabled','disabled');  
                  }else{
                     $('#company_pin').css({"background":"#a64040","color":"#fff","border-color":"#9d3d3d"});
                     $('.next_tax').attr('disabled','disabled');
                  }
                },
                error : function(data){}
            });
        }else{
          $('#company_pin').css({"background":"#a64040","color":"#fff","border-color":"#9d3d3d"});
        } 
        
    });
});
</script>


<script>
  $(function(){
    $('body').on('change','#have_tax',function(){
        var include_tax = $('#have_tax');
        
        if(include_tax.prop('checked')){
            $('#form-group-company_sale_tax').css('display','block');
            $('#form-group-tax_type').css('display','block');
            
        }else{
            $('#form-group-company_sale_tax').css('display','none');
            $('#form-group-tax_type').css('display','none');
        }

    });

  });
</script>
@endsection