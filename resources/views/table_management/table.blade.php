@extends("layouts.create_update")
@section("content")
	<form class='form-horizontal' method='post' id="form" enctype="multipart/form-data" action="{{(@!$row->id)?
                                route('save.table'):
                                route('update.table')
                            }}"> 
		<input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <input type='hidden' name='return_url' value='{{ @$return_url }}'/>
        <input type='hidden' name='ref_mainpath' value='{{ CRUDBooster::mainpath() }}'/>
        <input type='hidden' name='ref_parameter' value='{{urldecode(http_build_query(@$_GET))}}'/>
        @if($hide_form)
            <input type="hidden" name="hide_form" value='{!! serialize($hide_form) !!}'>
        @endif
            <input type="hidden" name="id" value="{{$row->id}}" />


           <div class="row">
           <div class="col-md-12">
           <div class="box-body">

                <!-- Table Code -->
           		<div class="form-group header-group-0" id="form-group_table_code">
                            <label class="control-label col-sm-2">{{trans('tablemanagement.table_code')}}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-sm-6">
                               <input type="text" name="table_code" class="form-control" required value="{{ $table->table_code }}" placeholder="Input table code (Note : 4 digit only)" autocomplete="off" maxlength="4" min="0" max="9999" step="1">
                            </div>
                </div>

                <!-- Table Type -->
                <!-- <div class="form-group header-group-0" id="form-group_table_type">
                            <label class="control-label col-sm-2">{{trans('tablemanagement.table_type')}}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-sm-6">
                            	<select name="table_type" class="form-control type" required id='type'>
	                                <option value="" 		@if($table->type == "") 		selected @endif >** Please select table type</option>
	                                <option value="Normal" 	@if($table->type == "Normal") 	selected @endif >Normal</option>
	                                <option value="Vip" 	@if($table->type == "Vip") 		selected @endif >VIP</option>
                                </select>
                            </div>
                </div> -->

                <!-- Table Size -->
                <div class="form-group header-group-0" id="form-group_table_size">
                            <label class="control-label col-sm-2">{{trans('tablemanagement.table_size')}}
                            <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-sm-6">
                               <input type="number" name="table_size" class="form-control" required placeholder="Input number of sit for this table" value="{{ $table->size }}" autocomplete="off">
                            </div>
                </div>

                <!-- Table Photo -->
                <!-- <div class="form-group header-group-0" id="form-group_table_photo">
                            <label class="control-label col-sm-2">{{trans('tablemanagement.table_photo')}}</label>
                            <div class="col-sm-6">
                              	 @if(empty($table->photo))
                               		<input type="file" class="form-control" name="photo" id="photo" accept="image"> 		
                                	<div style="color:#908e8e; margin-top: 5px;"> {{ trans('setting.file_image_require') }}</div>
                               	@else
	                                <p><a data-lightbox="roadtrip" href="{{$url}}/{{$table->photo}}">
	                                	<img style="max-width:160px" title="{{$table->photo}}" src="{{$url}}/{{$table->photo}}">
	                                </a></p>
	                                
	                                <input type="hidden" name="photo" value="{{$table->photo}}">                   
	                                    <p><a class="btn btn-danger btn-delete btn-sm" onclick="if(!confirm('Are you sure ?')) return false" href="{{$url}}/admin/table_numbers/delete-image?image={{$table->photo}}&amp;id={{$table->id}}&amp;column=photo"><i class="fa fa-ban"></i> {{ trans('setting.text_delete') }} </a></p>
	                                    <p class="text-muted"><em>{{ trans('setting.remove_image_first') }}</em></p>
	                                 <div class="text-danger"></div>
                               	@endif
                            </div>
                </div> -->

           </div>
           </div>
           </div>

           <div class="box-footer" style="background: #F5F5F5">

                                    <div class="form-group">
                                        <label class="control-label col-sm-2"></label>
                                        <div class="col-sm-10">
                                            @if($button_cancel && CRUDBooster::getCurrentMethod() != 'getDetail')
                                                @if(g('return_url'))
                                                    <a href='{{g("return_url")}}' class='btn btn-default'><i
                                                                class='fa fa-chevron-circle-left'></i> {{trans("crudbooster.button_back")}}</a>
                                                @else
                                                    <a href='{{CRUDBooster::mainpath("?".http_build_query(@$_GET)) }}' class='btn btn-default'><i
                                                                class='fa fa-chevron-circle-left'></i> {{trans("crudbooster.button_back")}}</a>
                                                @endif
                                            @endif
                                            @if(CRUDBooster::isCreate() || CRUDBooster::isUpdate())

                                                @if(CRUDBooster::isCreate() && $command == 'add')
                                                    <input type="submit" name="submit" value='{{trans("crudbooster.button_save_more")}}' class='btn btn-success'>
                                                @endif

                                                @if($button_save && $command != 'detail')
                                                    <input type="submit" name="submit" value='{{trans("crudbooster.button_save")}}' class='btn btn-success save'>
                                                @endif

                                            @endif
                                        </div>
                                    </div>

            </div>

	</form>
@endsection