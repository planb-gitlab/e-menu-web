<?php
return[
    //Super Admin Dashboard

    // ======================= A ========================== //
    'Admin'             => 'Admin',

    // ======================= B ========================== //
    // ======================= C ========================== //
    'Change Branch'     => 'Change Branch',
    'Cashier'           => 'Cashier',
    'Category'          => 'Category',

    // ======================= D ========================== //  
    'Dashboard'         => 'Dashboard',
    'Dine In'           => 'Dine In',
    'dashboard_setting' => 'Dashboard Setting',

    // ======================= E ========================== //  
    // ======================= F ========================== //
    // ======================= G ========================== //  
    // ======================= H ========================== //  
    // ======================= I ========================== //  
    'Item'              => 'Item',
    'Items'             => 'Items',

    // ======================= J ========================== //  
    // ======================= K ========================== //  
    // ======================= L ========================== //  
    // ======================= M ========================== //
    'Manager'           => 'Manager',
    'Menu'              => 'Menu',
    'menu_management'   => 'Menu Management',

    // ======================= N ========================== //
    // ======================= O ========================== //
    'Outlet'            => 'Outlet',  
    // ======================= P ========================== //
    'Promotion Pack'    => 'Promotion Pack',
    'Promotion Packs'   => 'Promotion Packs',
    'promotion_pack'    => 'Promotion Pack',

    // ======================= Q ========================== //
    // ======================= R ========================== //  
    // ======================= S ========================== //
    'super_admin_dashboard' => 'Dashboard',
    'SubCategory'       => 'Sub Category',
    'Setting'           => 'Setting',   
    'store_setting'     => 'Store Setting',

    // ======================= T ========================== //
    'Table'             => 'Table',
    'Tables'            => 'Tables',
    'table_management'  => 'Table Management',

    // ======================= U ========================== //  
    'User_Management'   => 'User Management',
    'User'              => 'User',
    'Users'             => 'Users',
    'user_management'   => 'User Management',
    // ======================= V ========================== //  
    // ======================= W ========================== //  
    'Waiter'            => 'Waiter',

    // ======================= X ========================== //  
    // ======================= Y ========================== //  
    // ======================= Z ========================== //  

];

?>