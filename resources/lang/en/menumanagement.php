<?php
return[
	// ======================= A ========================== //
	'Add Data' 							=> 'Add Data',
	'Action'							=> 'Action',
	'Add Menu' 							=> 'Add Menu',
	'Add New UOM' 						=> 'Add New UOM',
	'Addon'								=> 'Addon',
	'Add addon to this menu' 			=> 'Add addon to this menu',
	'Add Category Menu' 				=> 'Add Category Menu',

	// ======================= B ========================== //
	'Back To List Data Category Menu'	=> 'Back To List Data Category Menu',
	'Back' 								=> 'Back',

	// ======================= C ========================== //
	'Category Menu' 					=> 'Category Menu',
	'Category' 							=> 'Category',
    'Create Category Menu' 				=> 'Create Category Menu',
	'Create Sub Category Menu' 			=> 'Create Sub Category Menu',
	'Create UOM' 						=> 'Create UOM',
	'Crop' 								=> 'Crop',
	'Close' 							=> 'Close',
	// ======================= D ========================== //	
	'Description' 						=> 'Description',
	'Delete' 							=> 'Delete',

	// ======================= E ========================== //	
	// ======================= F ========================== //
	'File types support : JPG, JPEG, PNG, GIF, BMP' 	=> 'File types support : JPG, JPEG, PNG, GIF, BMP',

	// ======================= G ========================== //	
	// ======================= H ========================== //	
	// ======================= I ========================== //	
	'* If you want to upload other file, please first delete the file.' => '* If you want to upload other file, please first delete the file.',

	// ======================= J ========================== //	
	// ======================= K ========================== //	
	// ======================= L ========================== //	
	// ======================= M ========================== //
	'Menu' 								=> 'Menu',
	'Menu Name' 						=> 'Menu Name',
	'Menu Photo' 						=> 'Menu Photo',
	'Material'							=> 'Material',

	// ======================= N ========================== //
	'Name'								=> 'Name',

	// ======================= O ========================== //	
	// ======================= P ========================== //
	'Photo' 							=> 'Photo',
	'Price' 							=> 'Price',

	// ======================= Q ========================== //
	// ====================== R ========================== //	
	'Remove' 							=> 'Remove',

	// ======================= S ========================== //
	'Status'							=> 'Status',
	'Save'							    => 'Save',
	'Sub Category' 						=> 'Sub Category',
	'Sub Category Menu' 				=> 'Sub Category Menu',
	'Sub Category Name'					=> 'Sub Category Name',
	'Sub Category' 						=> 'Sub Category',
	'Select UOM with Price' 			=> 'Select UOM with Price',


	// ======================= T ========================== //
	'Total rows'						=> 'Total rows',

	// ======================= U ========================== //	
	'UOM' 								=> 'UOM',
	'Upload & Crop Image' 				=> 'Upload & Crop Image',

	// ======================= V ========================== //	
	// ======================= W ========================== //	
	// ======================= X ========================== //	
	// ======================= Y ========================== //	
	// ======================= Z ========================== //	
		
];
?>