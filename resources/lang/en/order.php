<?php 
return[	
	// ======================= A ========================== //
		'addon'								  	=> 'Add On',
		"Add Note" 								=> "Add Note",
		'Add to list' 				          	=> 'Add to list',
		'Action' 							  	=> 'Action',
	// ======================= B ========================== //

	// ======================= C ========================== //
		'Cancel'								=> 'Cancel',
		'Capacity'				          		=> 'Capacity',
		'Category'					          	=> 'Category',
		'Cashier Name'					  		=> 'Cashier Name',
		'Choose your currency'				  	=> 'Choose your currency',
		'Create Order'				          	=> 'Create Order',
		'Close' 					          	=> 'Close',
		'Cost'						          	=> 'Cost',
		'Clear' 							  	=> 'Clear',
	// ======================= D ========================== //	
		'Description'				          	=> 'Description',
		'Discount:' 				          	=> 'Discount:',
		'Discount' 							  	=> 'Discount',
		'Detail Item Order'			          	=> 'Detail Item Order',
	// ======================= E ========================== //	
		'Exit' 						          	=> 'Exit',

	// ======================= F ========================== //
		'From Date' 						  	=> 'From Date',
	// ======================= G ========================== //	
		'Grand Total' 			              	=> 'Grand Total',
	// ======================= H ========================== //	
	// ======================= I ========================== //	
		'Invoice No 1 :' 			          	=> 'Invoice No 1 :',
		'Invoice No 2 :' 			          	=> 'Invoice No 2 :',
		'itemId' 							  	=> 'itemId',
		'Invoice Management' 				  	=> 'Invoice Management',
		'Invoice ID' 						  	=> 'Invoice ID',
		'Invoice Date' 						  	=> 'Invoice Date',
	// ======================= J ========================== //	
	// ======================= K ========================== //	
	// ======================= L ========================== //	
		'Loading Material detail...' 		  	=> 'Loading Material detail...',
	// ======================= M ========================== //
		'Menu'						          	=> 'Menu',
		'Merge Payment' 			          	=> 'Merge Payment',
		'Membership' 						  	=> 'Membership',
		'Material / More Option :'            	=> 'Material / More Option :',
		'most_sold'							  	=> 'Most Sold',
		'Material Information' 				  	=> 'Material Information',
		'Modal Header' 						  	=> 'Modal Header',
	// ======================= N ========================== //
		'Note' 						          	=> 'Note',
		'Note Added'						  	=> 'Note Added',
		'No data'					          	=> 'No data',
		'Note Information'			          	=> 'Note Information',
		'No'								  	=> 'No',
		'No.' 						 		  	=> 'No.',
		'No Data!' 							  	=> 'No Data!',
	// ======================= O ========================== //	
		'Outlet'							  	=> 'Outlet',
		'Order Confirmation' 		          	=> 'Order Confirmation',
		'Order' 					          	=> 'Order',
		'OK' 						          	=> 'OK',
		'Old order Information' 			  	=> 'Old order Information',
		'Order & Print' 					  	=> 'Order & Print',
		'Order' 							  	=> 'Order',
		'OrderCode'							=> 'Order Code',
		'Order Date' 						  	=> 'Order Date',
		'Order Time'						  	=> 'Order Time',
	// ======================= P ========================== //
		'Price'					         	  	=> 'Price',	
		'Price' 					          	=> 'Price',
		'Paid & Print' 				          	=> 'Paid & Print',
		'Print_invoice' 					  	=> 'Print Invoice',
		'Paid' 						          	=> 'Paid',
		'Pay Full Amount' 			          	=> 'Pay Full Amount',
		'Promotion Pack Information' 		  	=> 'Promotion Pack Information',
		'Print Ticket for chef' 			  	=> 'Print Ticket for chef',
		'print' 							  	=> 'print',
		'Promotion Pack' 					  	=> 'Promotion Pack',
	// ======================= Q ========================== //
		'Qty'						          	=> 'Qty',	
		'Quantity' 					          	=> 'Quantity',

	// ======================= R ========================== //	
		'Remove Filter' 			          	=> 'Remove Filter',
		'Remove Note' 						  	=> 'Remove Note',
		'Remove' 							  	=> 'Remove',
		'Return' 							  	=> 'Return',
	// ======================= S ========================== //
		'Save' 						          	=> 'Save',
		'Subcategory'			          	  	=> 'Subcategory',
		'Subtotal'					          	=> 'Sub total',
		'Split Payment' 			          	=> 'Split Payment',
		'Size of menu :'			          	=> 'Size of menu :',
		'Search menu'				          	=> 'Search menu',
		'Search'					          	=> 'Search',
		'Search subcategory'		          	=> 'Search subcategory',
		'Search category' 			          	=> 'Search category',
		'Set Promotion Pack'				  	=> 'Set Promotion Pack',
		'Select Sort (This week,month,year):' 	=> 'Select Sort (This week,month,year):',
		'Search Invoice By ID' 				  	=> 'Search Invoice By ID',
		'Some text in the modal.' 			  	=> 'Some text in the modal.',
		'search_category'					  	=> 'Search Category',
		'search_subcategory'				  	=> 'Search Subcategory',
		'search_menu'						  	=> 'Search Menu',
		'Split Invoice'				  		  	=> 'Split Invoice',
		'Split Invoice and Paid'			  	=> 'Split Invoice and Paid',	
	// ======================= T ========================== //
		'Total'					          		=> 'Total',
		'Table'									=> 'Table',
		'Tax :' 					          	=> 'Tax :',
		'Tax' 				                  	=> 'Tax',
		'Take Out Order' 					  	=> 'Take Out Order',
		'takeout_order'							=> 'Take Out Order',
		'Total' 							  	=> 'Total',
		'Total Price' 						  	=> 'Total Price',
	// ======================= U ========================== //	
	// ======================= V ========================== //	
		'Validate'							  	=> 'Validate',
		'View Order' 						  	=> 'View Order',
		'Validation'						  	=> 'Validation',
	// ======================= W ========================== //	
	// ======================= X ========================== //	
	// ======================= Y ========================== //	
	// ======================= Z ========================== //	


];
?>