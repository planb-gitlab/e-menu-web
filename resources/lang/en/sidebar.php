<?php 
return[	
	// ======================= A ========================== //
	'Addon'					=> 'Addon',
	// ======================= B ========================== //
	// ======================= C ========================== //
	'Category Menu'			=> 'Category Menu',
	'Cash Management'		=> 'Cash Management',
	'Currency'				=> 'Currency',
	// ======================= D ========================== //
	'Dashboard'				=> 'Dashboard',
	'Dine In'				=> 'Dine In',
	'Discount'				=> 'Discount',
	'Discount Summary'		=> 'Discount Summary',
	// ======================= E ========================== //
	'Exchange Rate'			=> 'Exchange Rate',	
	// ======================= F ========================== //
	// ======================= G ========================== //	
	// ======================= H ========================== //	
	// ======================= I ========================== //
	'invoice MG'			=> 'Invoice MG',		
	// ======================= J ========================== //	
	// ======================= K ========================== //	
	// ======================= L ========================== //	
	// ======================= M ========================== //
	'Membership'			=> 'Membership',
	'Menu Management'		=> 'Menu Management',
	'Menu'					=> 'Menu',
	// ======================= N ========================== //
	// ======================= O ========================== //
	'Outlet Management'		=> 'Outlet Management',
	'Outlet Setting'		=> 'Outlet Setting',
	'Outlet User'			=> 'Outlet User',
	'Order'					=> 'Order',	
	'Order Setting'			=> 'Order Setting',
	// ======================= P ========================== //
	'Promotion Pack'		=> 'Promotion Pack',
	// ======================= Q ========================== //
	// ======================= R ========================== //
	'Role'					=> 'Role',
	'Report'				=> 'Report',
	'Report Dashboard'		=> 'Report Dashboard',	
	// ======================= S ========================== //
	'Sale by Cashier'		=> 'Sale by cashier',
	'Sale by item'			=> 'Sale by item',
	'Sale in invoice'		=> 'Sale in invoice',
	'Sale by promotion pack'=> 'Sale by promotion pack',
	'Setting'				=> 'Setting',
	'Sub Category'			=> 'Sub Category Menu',
	// ======================= T ========================== //
	'Table Management'		=> 'Table Management',
	'Take Out'				=> 'Take Out',	
	// ======================= U ========================== //
	'UOM'					=> 'UOM',
	'User Activity'			=> 'User Activity',
	'User Management'		=> 'User Management',
	'User'					=> "User",
	// ======================= V ========================== //
	'Void'					=> 'Void',
	'Void Payment'			=> 'Void Payment',	
	// ======================= W ========================== //	
	// ======================= X ========================== //	
	// ======================= Y ========================== //	
	// ======================= Z ========================== //	
	
];
?>