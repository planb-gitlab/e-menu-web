<?php 
return[	
	// ======================= A ========================== //
	// ======================= B ========================== //
	"button_logout"				=> "Logout",
	// ======================= C ========================== //
	"company_address"			=> "Company Address",
	"company_contact"			=> "Company Contact",
	"company_country"			=> "Company Country",
	"company_currency"			=> "Company Currency",
	"company_email"				=> "Company Email",
	"company_info"				=> "Company Info",
	"company_language"			=> "Company Language",
	"company_logo"				=> "Company Logo",
	"company_name"				=> "Company Name",
	"company_phone"				=> "Company Phone",
	"company_pin"				=> "Company Pin",
	"company_sale_tax"			=> "Company Sale Tax (%)",
	"company_website"			=> "Company Website",
	"Commercial Invoice"		=> "Commercial Invoice (Added Tax to over each menu)",
	"configuration_title"		=> "Create Restaurant",
	"currency"					=> "Currency",
	// ======================= D ========================== //	
	// ======================= E ========================== //	
	// ======================= F ========================== //
	// ======================= G ========================== //
	"general_information"		=> "General Information",	
	// ======================= H ========================== //
	// ======================= I ========================== //
	"Include Tax"				=> "Include Tax",	
	// ======================= J ========================== //	
	// ======================= K ========================== //	
	// ======================= L ========================== //	
	// ======================= M ========================== //
	// ======================= N ========================== //
	// ======================= O ========================== //	
	// ======================= P ========================== //
	"pin_info"								=> "Note : Input 4 digit number only",
	"pin_already_in_use"					=> "Pin is already in use. Please change it",
	"Please select client's subscription" 	=> "Please select client's subscription",
	"Please select default currency"		=> "Please select default currency",
	"Please select your language"			=> "Please select your language",
	"Please select your country"			=> "Please select your country",
	// ======================= Q ========================== //
	// ======================= R ========================== //	
	// ======================= S ========================== //
	"subscription"				=> "Subscription",
	// ======================= T ========================== //
	"tax"						=> "Tax",
	"Tax Invoice"				=> "Tax Invoice (Include overall tax in invoice)",
	"tax type"					=> "tax type",
	// ======================= U ========================== //	
	// ======================= V ========================== //	
	// ======================= W ========================== //	
	// ======================= X ========================== //	
	// ======================= Y ========================== //	
	// ======================= Z ========================== //	
	
];
?>