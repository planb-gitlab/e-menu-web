<?php 
return[	
	
	// ======================= A ========================== //
	'Action' 								=> 'Action',
	// ======================= B ========================== //
	// ======================= C ========================== //
	'Credit Memo No' 						=> 'Credit Memo No',
	'Customer Count' 						=> 'Customer Count',
	'Cashier Name'							=> 'Cashier Name',
	'Close' 								=> 'Close',
	'Category'								=> 'Category',
	// ======================= D ========================== //	
	'Discount' 								=> 'Discount',
	// ======================= E ========================== //	
	// ======================= F ========================== //
	'Filter by date'  						=> 'Filter by date',
	'Filter by custom date' 				=> 'Filter by custom date',
	'Filter By Itemcode' 					=> 'Filter By Itemcode',
	'Filter By ItemName' 					=> 'Filter By ItemName',
	'Filter By UOM' 						=> 'Filter By UOM',
	'Filter By Subcategory' 				=> 'Filter By Subcategory',
	'Filter By Category' 					=> 'Filter By Category',
	'Filter by Promotion Name:' 			=> 'Filter by Promotion Name:',
	'Filter By Promotion Type:' 			=> 'Filter By Promotion Type:',
	'From Date' 							=> 'From Date',
	// ======================= G ========================== //	
	// ======================= H ========================== //	
	// ======================= I ========================== //	
	'Invoice No' 							=> 'Invoice No',
	'orderType'								=> 'Order Type',
	'Item Name' 							=> 'Item Name',
	'Item code' 							=> 'Item code',
	'Invoice'								=> "Invoice",
	'Invoices' 								=> 'Invoices',
	'Invoice ID.' 							=> 'Invoice ID.',
	'Invoice ID' 							=> 'Invoice ID',
	'Invoice Date' 							=> 'Invoice Date',
	// ======================= J ========================== //	
	// ======================= K ========================== //	
	// ======================= L ========================== //	
	'Location' 								=> 'Location',
	// ======================= M ========================== //
	'Modal Header' 							=> 'Modal Header',
	'Month' 								=> 'Month',
	// ======================= N ========================== //
	'Next' 									=> 'Next',
	'No.' 									=> 'No.',
	'No Change From Previous Day' 			=> 'No Change From Previous Day',
	'No Data!' 								=> 'No Data!',
	// ======================= O ========================== //	
	'of' 									=> 'of',
	'Order Date' 							=> 'Order Date',
	// ======================= P ========================== //
	'Previous' 								=> 'Previous',
	'Print'									=> "Print",
	'Promotion Name' 						=> 'Promotion Name',
	'Promotion Type' 						=> 'Promotion Type',
	// ======================= Q ========================== //
	'Quantity' 								=> 'Quantity',
	// ======================= R ========================== //	
	'Revenue' 								=> 'Revenue',
	// ======================= S ========================== //
	'Sale by Invoice Report'				=> 'Sale by Invoice Report',
	'Sale by Item Report'					=> 'Sale by Item Report',
	'Sale by Item Report Search'			=> 'Sale by Item Report',
	'Sale by Promotion Pack Report'			=> 'Sale by Promotion Pack Report',
	'Search Invoice By ID' 					=> 'Search Invoice By ID',
	'Search Invoice By Invoice ID' 			=> 'Search Invoice By Invoice ID',
	'Select Sort (This week,month,year):' 	=> 'Select Sort (This week,month,year):',
	'Select User To Sort' 					=> 'Select User To Sort',
	'Show' 									=> 'Show',
	'Sold Amount' 							=> 'Sold Amount',
	'Sub Total' 							=> 'Sub Total',
	'Sold Amount' 							=> 'Sold Amount',
	'Showing' 								=> 'Showing',
	'Some text in the modal.' 				=> 'Some text in the modal.',
	'Subcategory' 							=> 'Subcategory',
	// ======================= T ========================== //
	'Table Sold' 							=> 'Table Sold',
	'to' 									=> 'to',
	'Top Sale Cashier' 						=> 'Top Sale Cashier',
	'Total Price' 							=> 'Total Price',
	// ======================= U ========================== //	
	'UOM' 									=> 'UOM',
	// ======================= V ========================== //	
	'VAT'									=> 'VAT',
	'Void'									=> 'Void',
	'Void & Copy'							=> 'Void & Copy',
	'Void Payment No' 						=> 'Void Payment No',
	// ======================= W ========================== //	
	// ======================= X ========================== //	
	// ======================= Y ========================== //	
	// ======================= Z ========================== //	

];
?>