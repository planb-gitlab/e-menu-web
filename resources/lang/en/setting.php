<?php 
return[

     // ======================= A ========================== //
	    'Action' 					=> 'Action',
	    'Address' 					=> 'Address',
	    'add_branch'        		=> 'Add Branch',
        'admin_name'        		=> 'Admin Name',
        'admin_photo'       		=> 'Admin Photo',
        'admin_email'       		=> 'Admin Email',
        'admin_phone'       		=> 'Admin Phone',
        'admin_pin'         		=> 'Admin Pin',
        'Activity' 					=> 'Activity',
        'Action' 					=> 'Action',
        'admin_password'    		=> 'Admin Password',

	// ======================= B ========================== //
        'branch_name'       		=> 'Branch Name',
        'branch_address'    		=> 'Branch Address',
        'branch_country'    		=> 'Branch Country',
        'branch_info'       		=> 'Branch Info',
        'branch_email'      		=> 'Branch Email',
        'branch_contact'    		=> 'Branch Contact',
        'branch_phone'      		=> 'Branch Phone',
        'branch_policy'     		=> 'Branch Policy',
        'branch_language'   		=> 'Branch Language',
        'branch_currency'   		=> 'Branch Currency',
        'branch_pin'        		=> 'Branch Pin',
        'Branch Name'       		=> 'Branch Name',
        'Branch Address'    		=> 'Branch Address',
        'Branch Email'      		=> 'Branch Email',
        'Branch Contact'    		=> 'Branch Contact',
        'Branch Phone'      		=> 'Branch Phone',
	    'button_back' 				=> 'Back',
	    'button_save_more' 			=> 'Save & Add More',
	    'button_save' 				=> 'Save',
	// ======================= C ========================== //
		'Cancel' 					=> 'Cancel',
	    'currency'            		=> 'Currency',
	    'company_name'              => 'Company Name',
	    'company_address'           => 'Address',
	    'company_country'           => 'Country',
	    'company_logo'              => 'Logo',
	    'company_info'              => 'Company Information',
	    'company_email'             => 'Email',
	    'company_contact'           => 'More Contact Number',
	    'company_phone'             => 'Phone',
	    'company_website'           => 'Company Website',
	    'company_language'          => 'Use Language',
	    'company_currency'          => 'Company Currency',
	    'company_pin'               => 'Company Pin',
	    'company_sale_tax'          => 'Company Sale Tax (%)',
	    'company_time_zone'         => 'Company Time Zone',
	    'company_currency_information'  => '<b>Note</b><span class="text-danger" title="This field is required">*</span> : If you to change your company\'s currency, the price of <br/>[ 1. Menu ] [2. Addon] [3.Discount] [4.Promotion Pack] <br/>will also automatically coverted and change to your new currency. Please be aware.',
	    'company_currency_information_01'  => '<b>Note</b><span class="text-danger" title="This field is required">*</span> : Please Set Your <b>ALL Exchange Rate</b> first before take any action. <br/>You can find it here : ',
	    'Choose print setting' 		=> 'Choose print setting',
        'Currency To'				=> 'Currency To',
        'Created' 					=> 'Created',
        'Currency From' 			=> 'Currency From',
	// ======================= D ========================== //	
		'Delete'					=> 'Delete',
	    'discount_privilege'        => 'Discount Privilege',
	    'discount_privilege_info'   => 'Use for : set discount permission to specific user. Who will be able to use discount functionality.',
	    'Date Of Birth' 			=> 'Date Of Birth',
	// ======================= E ========================== //	
	    'Exchange Rate'				=> 'Exchange Rate',

	// ======================= F ========================== //
	    'file_image_require'        => 'File types support : JPG, JPEG, PNG, GIF, BMP',
	    'First Name' 				=> 'First Name',
        'file_image_require'        => 'File types support : JPG, JPEG, PNG, GIF, BMP',
	    'Full Permission' 			=> 'Full Permission',

	// ======================= G ========================== //	
	    'general_information' 		=> 'General Information',
	    'Generate' 					=> 'Generate',
        'Generate Pin'      		=> 'Generate Pin',
	// ======================= H ========================== //	
	// ======================= I ========================== //	
		'Include Tax' 				=> 'Include Tax',
		'invoice_layout'            => 'Invoice Layout',
		'invoice_layout_info'       => 'Use for : set default invoice layout.',
		'In Progress'    			=> 'In Progress',
	// ======================= J ========================== //	
	// ======================= K ========================== //	
	// ======================= L ========================== //	
	    'Last Name' 				=> 'Last Name',
	// ======================= M ========================== //
		'Membership No' 			=> 'Membership No',

	// ======================= N ========================== //
		'No Permission' 			=> 'No Permission',
		'No' 						=> 'No',
		'Name' 						=> 'Name',
   	 	'No' 						=> 'No',
    	'No Data!' 					=> 'No Data!',
	// ======================= O ========================== //	
		'order_layout'              => 'Order Layout',
		'order_layout_info'			=> 'Use for : set default ordering layout.',
    	'Outlet User'				=> 'Outlet User',
	// ======================= P ========================== //
	    'print_setting'             => 'Print Setting',
	    'print_setting_info'        => 'Use for : enable or disable printing ticket or invoice.',
	    'Print Invoice' 			=> 'Print Invoice',
	    'Print Ticket' 				=> 'Print Ticket',
	    'Phone Number' 				=> 'Phone Number',
        'password_info'     		=> 'Minimum 5 characters. Please leave empty if you do not want to change the password.',
        'pin_info'          		=> 'Minimum 4 characters, Maximum 4 characters. Please leave empty if you do not want to change the pin.',
        'Price To' 					=> 'Price To',
        'Price From' 				=> 'Price From',
	// ======================= Q ========================== //
	// ======================= R ========================== //	
	    'remove_image_first'        => '* If you want to upload other file, please first delete the file.',
	    'Right part' 				=> 'Right part',
	// ======================= S ========================== //
	// ======================= T ========================== //
		'This field is required' 	=> 'This field is required',
		'tax type' 			  		=> 'Tax Type',
		'tax_type'                  => 'Tax Type',
		'text_delete' 				=> 'Delete',
	    'tax'                 		=> 'TAX / VAT',
        'The company information has been updated !' => 'The company information has been updated !',
	// ======================= U ========================== //	
	    'uom'                 		=> 'Unit of Measurement',
        'update_branch'     		=> 'Update Branch',
	// ======================= V ========================== //	
        'view detail' 				=> 'view detail',

	// ======================= W ========================== //	
	// ======================= X ========================== //	
	// ======================= Y ========================== //	
	// ======================= Z ========================== //	
];
?>