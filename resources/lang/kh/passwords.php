<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    // ======================= A ========================== //
    // ======================= B ========================== //
    // ======================= C ========================== //
    // ======================= D ========================== //  
    // ======================= E ========================== //  
    // ======================= F ========================== //
    // ======================= G ========================== //  
    // ======================= H ========================== //  
    // ======================= I ========================== //  
    // ======================= J ========================== //  
    // ======================= K ========================== //  
    // ======================= L ========================== //  
    // ======================= M ========================== //
    // ======================= N ========================== //
    // ======================= O ========================== //  
    // ======================= P ========================== //
    'password' => 'ពាក្យសម្ងាត់ត្រូវតែយ៉ាងហោចណាស់ប្រាំមួយតួហើយត្រូវនឹងការបញ្ជាក់',
    // ======================= Q ========================== //
    // ======================= R ========================== // 
    'reset' => 'ពាក្យសម្ងាត់របស់អ្នកត្រូវបានកំណត់ឡើងវិញ!',
    // ======================= S ========================== //
    'sent' => 'យើងបានផ្ញើរលីងដើម្បីប្តូលេខសង្ថាត់តាមអ៊ីមែលរបស់អ្នក!',
    // ======================= T ========================== //
    'token' => 'លេខសម្ងាត់ដែលបង្កើតថ្មីនេះមិនត្រឹមត្រូវទេ.',
    // ======================= U ========================== //  
    'user' => "យើងមិនអាចរកឃើញអ្នកប្រើប្រាស់ដែលមានអាស័យដ្ឋានអ៊ីមែលនោះទេ.",
    // ======================= V ========================== //  
    // ======================= W ========================== //  
    // ======================= X ========================== //  
    // ======================= Y ========================== //  
    // ======================= Z ========================== //  

];
