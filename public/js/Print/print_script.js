function doit(){
    if (!window.print){
        alert("You need NS4.x to use this print button!")
        return
    }

    var content = document.getElementById('view_invoice_blade').innerHTML;
    var mywindow = window.open('', 'Print','height=600,width=800');

    mywindow.document.write('<html><head><title>Print</title>');
    mywindow.document.write('<style>'+
    						'table{border:1px solid #333;}' +
    						'table.invoice_data	  {	margin-bottom: 20px;	width: 100%;	padding: 20px;	font-size: 16px;	} '+
    						'table.invoice_data td{	border-bottom: 1px solid #ccc;	}'+
    						'table.order_data     {	width: 100%; padding: 20px	}'+
    						'table.order_data th  {	border-bottom: 1px solid #333;	}'+
    						'table td{ border-bottom: 1px solid #ccc;	}'+
    						'footer.main-footer { display:  none; border:  none;  } a{display: none;}'+
    						'span{display:none;}' + 
    						'button{display:none}' +
    						'</style></head><body >');
    mywindow.document.write(content);
    mywindow.document.write('</body></html>');

    mywindow.document.close();
    mywindow.focus()
    mywindow.print();
    mywindow.close();

    window.print()
}
    // doit();
    // window.onfocus=function(){ 
    // window.close();