$(function(){

	var invoice_id="";
	$('body').on('click','.button_show_invoice',function(){
		 invoice_id= $(this).attr("data-val");
		console.log(url);
		jQuery.ajax({
			url: url + "/get_invoice",
			type:'POST',
			data:{invoice_id:invoice_id},
			dataType:"html",
			success:function(data){
				  $('#invoice_modal .modal-content').html(data);
                  $('#invoice_modal').modal('show');
                                
			}
		});
	});

	$('body').on('click','.button_show_invoicebyid',function(){
		invoice_id= $(this).attr("data-val");
		console.log(url);
		jQuery.ajax({
			url: url + "/get_invoice",
			type:'POST',
			data:{invoice_id:invoice_id},
			dataType:"html",
			success:function(data){
				  $('#invoice_modal .modal-content').html(data);
                  $('#invoice_modal').modal('show');
                                
			}
		});
	});
    var is_refund=[];
	$(document).on('click','.cross',function(){
			var exchange=$(this).attr('data-changecurrency');

			$(this).parent().parent().attr('data-refund',1);
			//var refund=$(this).attr('data-refund');
			
				var menusId=$(this).parent().parent().attr('data-id');
				var refund = $(this).parent().parent().attr('data-refund');
				$('#show_order').each(function(){
				
				if(refund == '1'){
					is_refund.push(menusId);
					}

					console.log(is_refund);


				});
            var amount = $(this).parents('tr').children('td:eq(4)').text();
            var subTotal=$('#subtotal').text();
            var order_total1 = parseFloat(subTotal) - parseFloat(amount);
            $('#subtotal').html(order_total1.toFixed(2));
            var last_subtotal=$('#subtotal').text();
            var tax= $('#txt_tax_amount').val();
            var total=parseFloat(last_subtotal) * parseFloat(tax) / 100 + parseFloat(last_subtotal);
            $('#order_total_data_dollar').html(total.toFixed(2));
            var total_riel=parseFloat(total) * parseFloat(exchange);
            $('#order_total_data_riel').html(total_riel.toFixed(2));
            var Total= $('#order_total_data_dollar').text();
            var Paid= $('#paid_dollar').text();
            var Return = parseFloat(Paid) - parseFloat(Total);
            $('#return_dollar').html(Return.toFixed(2));
            var return_riel=parseFloat(Return) * parseFloat(exchange);
            $('#return_riel').html(return_riel.toFixed(2));

            $(this).parents('tr').children('td:eq(3)').html('cancel');
			$(this).parents('tr').children('td:eq(4)').html('cancel');
            
		});
	//============================invoice mg=========================
	$(document).on('click','.invoicemg_button_show_invoice',function(){
		invoice_id= $(this).attr("data-val");
		console.log(url);
		jQuery.ajax({
			url: url + "/invoice_management_view_detail",
			type:'GET',
			data:{invoice_id:invoice_id},
			dataType:"html",
			success:function(data){
				  $('#invoice_modal .modal-content').html(data);
                  $('#invoice_modal').modal('show');
                                
			}
		});
	});
		$(document).on('click','.invoicemg_button_show_invoicebyid',function(){
		invoice_id= $(this).attr("data-val");
		console.log(url);
		jQuery.ajax({
			url: url + "/invoicemanagement_insert_order",
			type:'GET',
			data:{invoice_id:invoice_id},
			dataType:"html",
			success:function(data){
				  $('#invoice_modal .modal-content').html(data);
                  $('#invoice_modal').modal('show');
                                
			}
		});
	});
	

	//============================end invoice mg=====================
	//==============refund================================
	var number_of_button="";
	$(document).on('click','#percentage',function(){
			$('#dollar').removeClass('active');
			$(this).addClass('active');
			$('.input_refund_amount').show();
			$('#txt_refund_amount').show();
			$('.span_refund_amount').show();
			$('.input_refund_amount').val("");
			 number_of_button=$(this).attr("data-id");
			var percen_val=$('.input_refund_amount').val();
			jQuery.ajax({
				url:url+"/",
				type:"GET",
				data:{invoice_id:invoice_id,number_of_button:number_of_button,percen_val:percen_val},
				dataType:"json",
				success:function(data){

				}
			});


		});

		$(document).on('click','#dollar',function(){
			$('#percentage').removeClass('active');
			$(this).addClass('active');
			$('#txt_refund_amount').show();
			$('.input_refund_amount').show();
			$('.span_refund_amount').show();
			$('.input_refund_amount').val("");
			 number_of_button=$(this).attr("data-id");
			
			var dol_val=$('.input_refund_amount').val();
			jQuery.ajax({
				url:url+"/",
				type:"GET",
				data:{invoice_id:invoice_id,number_of_button:number_of_button,dol_val:dol_val},
				dataType:"json",
				success:function(data){
					
				}
			});

		});
		var grand_total="";
		 $('body').on('keyup','.input_refund_amount',function(){
		 	
		 	grand_total=$(this).attr("data-grand_val");
		 	var input=$('.input_refund_amount').val();
		 	if(number_of_button==1){
		 		var total=parseFloat(grand_total-((grand_total*input)/100));
			 	$('#txt_refund_amount').val(total.toFixed(2));
		 	}
		 	else if(number_of_button==2){
		 	 var total=parseFloat(grand_total-input);
		 		$('#txt_refund_amount').val(total.toFixed(2));
		 	}
		 	

		 });
	

		
		//=================end refund=======================
	$(document).on('click','.refund_button',function(){
		
		console.log(url);
		var after_refund_amount=$('#txt_refund_amount').val();
		var after_refund=$('#txt_refund_amount').val();
		 var refund_amount=parseFloat(grand_total-after_refund);
		 	jQuery.ajax({
		 		url:url+"/update_after_refund",
		 		type:"GET",
		 		data:{refund_amount:refund_amount,invoice_id:invoice_id,after_refund_amount:after_refund_amount},
		 		dataType:"json",
		 		success:function(data){

		 		}
		 	});
		 	swal({
				  type: 'success',
				  title: 'Oops...',
				  text: 'Refund Completed!',
				})
		 	 $('#invoice_modal').modal('hide');
		 	 window.location.reload();
			
		
				

	});
	$(document).on('click','.refund_print_button',function(){
		 PrintInvoice('modal_invoice');
		alert();
	});
		/*void invoices*/
	
	$(document).on('click','.void_button_show_invoice',function(){
		 invoice_id= $(this).attr("data-val");
		console.log(url);
			jQuery.ajax({
				url:url + "/void_insert_order",
				type:"GET",
				data:{invoice_id:invoice_id},
				dataType:"html",
				success:function(data){
					$('#invoice_modal .modal-content').html(data);
                    $('#invoice_modal').modal('show');
				}
			});
	});
	$('body').on('click','.void_button_show_invoicebyid',function(){
		invoice_id= $(this).attr("data-val");
		console.log(url);
		jQuery.ajax({
			url: url + "/void_insert_order",
			type:'GET',
			data:{invoice_id:invoice_id},
			dataType:"html",
			success:function(data){
				  $('#invoice_modal .modal-content').html(data);
                  $('#invoice_modal').modal('show');
                                
			}
		});
	});
	$(document).on('click','.void_button',function(){
		jQuery.ajax({
			url:url + "/void_update",
			type:"GET",
			data:{invoice_id:invoice_id},
			dataType:"json",
			success:function(data){
				
			}
		});
		$('#invoice_modal').modal('hide');
		window.location.reload();

				
	});
//=========================== end void=======================
//=========================== start merge=====================
	$(document).on('click','.merge_button_show_invoice',function(){
		 invoice_id= $(this).attr("data-val");
		console.log(url);
			jQuery.ajax({
				url:url + "/merge_insert_order",
				type:"GET",
				data:{invoice_id:invoice_id},
				dataType:"html",
				success:function(data){
					$('#invoice_modal .modal-content').html(data);
                    $('#invoice_modal').modal('show');
				}
			});
	});
	$('body').on('click','.merge_button_show_invoicebyid',function(){
		invoice_id= $(this).attr("data-val");
		console.log(url);
		jQuery.ajax({
			url: url + "/merge_insert_order",
			type:'GET',
			data:{invoice_id:invoice_id},
			dataType:"html",
			success:function(data){
				  $('#invoice_modal .modal-content').html(data);
                  $('#invoice_modal').modal('show');
                                
			}
		});
	});

//===================== end merge===============================
	$('body').on('click','.custom_button',function(){
		var invoiceId = $(this).attr('data-val');
		
		//pop up data for this invoice
		console.log(url);
		
		jQuery.ajax({
	            url: url + "/admin_view_invoice",
	            type: 'POST',
	            data: { invoiceId: invoiceId},
	            dataType: "html",
	            success: function (data) {

	              	$('#view_invoice_blade').html(data);
	              	$('#print_invoice').modal('show');
	            }
	        
	    });

	});


	$('body').on('click','.print_data',function(){
		var invoiceId = $(this).attr('data-val');
		console.log(url);
		//pop up data for this invoice
		
		jQuery.ajax({
	            url: url + "/print_invoice",
	            type: 'GET',
	            data: { invoiceId: invoiceId},
	            dataType: "html",
	            success: function (data) {

	              	$('#view_invoice_blade').html(data);
	              	$('#print_invoice').modal('show');
	            }
	        
	    });

	});
});
