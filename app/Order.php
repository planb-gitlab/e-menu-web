<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = ['user_id', 'order_number', 'table_id', 'created_by', 'status', 'total', 'discount_id'];

    public function hasDetails()
    {
        return $this->hasMany("App\OrderDetail", "orders_id", "id");
    }
}
