<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Material;

class Tmp extends Model
{
    protected $table = "tmp_order";
    protected $fillable = ['user_id', 'menu_id', 'name', 'qty', 'price', 'table_code', 'send_to_kitchen'];

}
