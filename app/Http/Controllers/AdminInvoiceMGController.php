<?php namespace App\Http\Controllers;

	use Session;
	use Illuminate\Http\Request;
	use DB;
	use CRUDBooster;

	class AdminInvoiceMGController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field 			= "id";
			$this->limit 				= "20";
			$this->orderby 				= "invoice_id,desc";
			$this->global_privilege 	= false;
			$this->button_table_action 	= false;
			$this->button_bulk_action 	= false;
			$this->button_action_style 	= "button_icon";
			$this->button_add 			= false;
			$this->button_edit 			= false;
			$this->button_delete 		= false;
			$this->button_detail 		= false;
			$this->button_show 			= false;
			$this->button_filter 		= false;
			$this->button_import 		= false;
			$this->button_export 		= false;
			$this->table 				= "invoices";
			# END CONFIGURATION DO NOT REMOVE THIS LINE
	        
	    }

	    public function getIndex()
	    {
	    	$myID 					= CRUDBooster::myId();
	    	$user					= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
	    	
	    	$data 					= [];
	    	$data['page_title']    	= "Sale by Invoice Report";
	    	$data['invoices']   	= DB::table('invoices')
	    								->leftjoin('settings','settings.id','=','invoices.company_id')
	    								->where('invoices.company_id',$user->company_id)->where('void',0)->orderBy('invoice_id','DESC')->paginate(20);
	    	$data['total_invoice']  = count($data['invoices']); 
	    	$this->cbView('report.sale_by_invoice',$data);
	    	
	    }


}