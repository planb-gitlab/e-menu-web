id  	Module_ID		name 					Modul Usage	
1		13				Role					Setting up role management
2		14				User					CRUD User
3		15				Category Menu			CRUD category
4 		16				Menu 					CRUD Menu
7		18				Discount				CRUD Discount
9		20				Currency				(Super Admin) CRUD Currecy
10		21				Exchange Rate			CRUD Exchange rate
11 		22				UOM						CRUD UOM
12 		23				Promotion Pack			CRUD Promotion Pack
14		25				Table management 		CRUD Table Management
17						Report					Report Container
18 		27				Outlet Setting			CRUD Outlet Setting
21		30				Sale by item			Sale by item report
22 		31				Sale by customer		(not use) Sale by customer report
23		32				Sale by cashier			Sale by cashier report
24		33				Sale by promotion pack 	Sale by promotion pack report
25		34				Void Payment			Void Payment Report
26 		35				Credit Memo				(Not use) Credit Memo report
27		36				Discount Summary 		Discount Summary report
29		38				Sale in invoice 		Sale in invoice report
30		39				User Activity			User Activity
33		42				Sub Category 			CRUD sub category
35 		44				Dashboard				Dashboard for everyone
36 		45				Outlet user 			(Super Admin)
39		48				Addon					CRUD Addon
40 						Setting					Setting container
41		49				Screen Request Order	(Not Use) Chef screen
42 		51				Dine In 				Order Table Management
43 						Menu Management 		Menu Container
44						User management 		User Container
49 		56				Take Out 				Take Out Management
51 						Cash management 		Cash Management container
56 						Order					Order Container
57 		62				Void 					Void Invoice
59 		59				Invoice MG 				Invoice Management Report
60 		60				Report Dashboard 		Report Dashboard
61 		63				Membership 				CRUD Membership
62 		64				Order Setting 			Order Setting
63	 	65				Outlet 					(Super Admin) CRUD Outlet
64						Outlet Management 		(Super Admin) Outlet Container

Note : 
- Left  : Menu_ID
- Right : Module_ID 

Dashboard 				[35],[44]

Order 					[56,42,49,59],[51,56,59]
+ Order					[56]
	- Dine In 			[42],[51]	
	- Take out 			[49],[56]
	- Invoice MG 		[59],[59]

Menu Management 		[43,3,33,4,39],[15,42,16,48]
+ Menu 					[43]
    - Category 			[3],[15]
    - Subcategory   	[33],[42]
    - Menu 				[4],[16]
    - Addon 			[39],[48]

Discount 				[7],[18]

Promotion Pack 			[12],[23]

Table Management 		[14],[25]

User Management 		[44,2,1],[14,13]
+ User Management 		[44]
	- User 				[2],[14]
	- Role 				[1],[13]

Report 					[17,60,29,21,23,24,25,27][60,38,30,32,33,34,36]
+ Report 				[17]
	- Report Dashboard 	[60],[60]
	- Sale in invoice 	[29],[38]
	- Sale by item 		[21],[30]
	- Sale by cashier 	[23],[32]
	- Sale by promotion [24],[33]
	- Void payment 		[25],[34]
	- Discount Summary 	[27],[36]

Cash Management 		[51,57],[62]
+ Cash Management 		[51]
	- Void 				[57],[62]

Setting 				[40,18,62,61,11,9,10,30][27,64,63,22,20,21,39]
+ Setting 				[40]
	- Outlet Setting	[18],[27]
	- Order Setting 	[62],[64]
	- Membership 		[61],[63]
	- UOM  				[11],[22]
	- Currency 			[9],[20]
	- Exchange 			[10],[21]
	- User Activity 	[30],[39]
