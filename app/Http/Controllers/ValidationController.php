<?php namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use DB;
use CRUDBooster;
use URL;
use Response;

class ValidationController extends \crocodicstudio\crudbooster\controllers\CBController
{
	// ===================== Pin Validation ================================
	public function pin_validation(Request $request){
		$myID 		= CRUDBooster::myId();
        $user 	  	= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
		$pin_type 	= $request->pin_type;
		$pin 	  	= $request->pin;
		// ========== Validate pin for outlet ==========
		if($pin_type == "outlet"):
			
	    	if($company_pin):
	    		$db_ext 			= \DB::connection('mysql_external');
	    		$server_company		= $db_ext->table('settings')->select('company_pin')->get();
	    		$all_server 		= $server_company->pluck('company_pin')->toArray();
	    		$all_restaurant_pin = DB::table('settings')->select('company_pin')->get();
	    		$all 				= $all_restaurant_pin->pluck('company_pin')->toArray();

	    		if(!in_array($pin, $all) && !in_array($pin, $all_server)):
	    			return Response::json(["status" => "good"]);
	    		endif;
	    	endif;
		endif;

		// ========== Validate pin for user ==========
		if($pin_type == "user"):
	        if($pin):
	            $all_user_pin 		= DB::table('cms_users')->select('user_pin')->where('company_id',$user->company_id)->where('created_by','<>','1')->get();
	            $all 				= $all_user_pin->pluck('user_pin')->toArray();
	            if(!in_array($pin, $all)):
	                return Response::json(["status" => "good"]);
	            endif;
	        endif;
		endif;

		// ========== Validate pin for membership ==========
		if($pin_type == "membership"):
	        if($pin):
	            $all_member_pin = DB::table('memberships')->select('membership_no')->where('company_id',$user->company_id)->get();
	            $all 			= $all_member_pin->pluck('membership_no')->toArray();
	            if(!in_array($pin, $all)):
	                return Response::json(["status" => "good"]);
	            endif;
	        endif;
		endif;

		// ========== Validate pin for membership ==========
		if($pin_type == "menu"):
	        if($pin):
	            $all_menu_pin 	= DB::table('menus')->select('menu_code')->where('company_id',$user->company_id)->get();
	            $all 			= $all_menu_pin->pluck('menu_code')->toArray();
	            if(!in_array($pin, $all)):
	                return Response::json(["status" => "good"]);
	            endif;
	        endif;
		endif;


	}
}