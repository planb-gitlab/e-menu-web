<?php namespace App\Http\Controllers;

	use Session;
	use Illuminate\Http\Request;
	use Carbon\Carbon;
	use DB;
	use CRUDBooster;
	use Image;
	use File;

	class AdminDashboardController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field 			= "name";
			$this->limit 				= "20";
			$this->orderby 				= "id,desc";
			$this->global_privilege 	= false;
			$this->button_table_action 	= false;
			$this->button_bulk_action 	= false;
			$this->button_action_style 	= "button_icon";
			$this->button_add 			= false;
			$this->button_edit 			= false;
			$this->button_delete 		= false;
			$this->button_detail 		= false;
			$this->button_show 			= false;
			$this->button_filter 		= false;
			$this->button_import 		= false;
			$this->button_export 		= false;
			$this->table 				= "menus";
			# END CONFIGURATION DO NOT REMOVE THIS LINE                
	    }


	    // ========================== Super admin + Admin,Legal + All user Dashboard ================================ 
	    public function getIndex()
	    {	

	    	$myID 		= CRUDBooster::myId();
        	$user 		= DB::table('cms_users')->select('company_id','user_type','created_by','role')->where('id',$myID)->first();
        	$restaurant = DB::table('settings')->select('id')->where('id',$user->company_id)->where('status','1')->first();
	        

	    	// Super Admin Dashboard
	    	if($user->user_type == "user_system"):
	    		$data 						= [];
	    		$data['page_title'] 		= trans("dashboard.super_admin_dashboard");
	    		$data['outlets']			= DB::table('cms_users')->where('role',1)->where('created_by',1)->get();
	    		$data['dashboard_settings'] = DB::table('dashboard_settings')->where('user_id',$myID)->first();

	    		$this->cbView('dashboard.superadmin.dashboard',$data);
	    	endif;
	    	
	    	// First Configuration
	    	if($user->user_type != "user_system" && $restaurant == null):
	    		$data['created_by']		= $user->created_by; 
	    		$data['countries']		= DB::table('countries')->OrderBy('country_name','asc')->get();
	    		$data['languages']		= DB::table('languages')->get();
	    		$data['currencies'] 	= DB::table('currencies')->get();
	           	$data['tax_types']		= DB::table('tax_types')->get();
	           	$data['subscription']	= DB::table('business_plan')->where('status','1')->get();
	      
	    		$this->cbView('first_configuration.store_configuration',$data);
	    	endif;

	    	// Everyone dashboard
	    	if($user->user_type != "user_system" && $user->company_id == $restaurant->id && $restaurant != null):
		    	$data = [];
		    	$data['page_title'] 		= trans("dashboard.Dashboard");
		    	$data['user_role']			= $user->user_role;
	    		$data['created_by']			= $user->created_by; 
		    	$data['users']				= DB::table('cms_users')->where('company_id',$user->company_id)->where('id_cms_privileges','<>','2')->get(); 
		    	$data['menus']				= DB::table('menus')->where('company_id',$user->company_id)->get();
		    	$data['categories']			= DB::table('categories')->where('company_id',$user->company_id)->get();
		    	$data['subcategories']		= DB::table('subcategories')->where('company_id',$user->company_id)->get();
		    	$data['promotion_packs']	= DB::table('promotion_packs')->where('company_id',$user->company_id)->get();
		    	$data['tables']				= DB::table('table_numbers')->where('company_id',$user->company_id)->get();
		    	$data['settings']			= DB::table('settings')->where('id',$user->company_id)->get(); 	
		    	$data['dashboard_settings'] = DB::table('dashboard_settings')->where('user_id',$myID)->first();
		    	$data['get_user_id']		= DB::table('cms_users')->select('id')->where('id',$myID)->first();

		    	$this->cbView('dashboard.admin.dashboard',$data);
	   		endif;
	    }

	    // ========================== Create New Outlet from client side (function) ================================ 
	    public function add_store_configuration(Request $request){

	    	$myID 					= CRUDBooster::myId();
        	$user 					= DB::table('cms_users')->select('user_type')->where('id',$myID)->first();
	    	$now 					= Carbon::now();
	    	$company_pin 			= $request->company_pin;

	    	// Get Company Pin From PLANB's DATABASE & Check if exist
	    	$db_ext 				= \DB::connection('mysql_external');
	    	$main_db_setting 		= $db_ext->table('settings')->select('company_pin')->get();
	    	
	    	foreach($main_db_setting as $m):
	    		if($company_pin == $m->company_pin):
	    			CRUDBooster::redirect($_SERVER['HTTP_REFERER'],trans('crudbooster.pin_already_in_use'),"info"); 
	    		endif;
	    	endforeach;

	    	// Get Company Pin From CLIENT's DATABASE & Check if exist
	    	$client_db 				= DB::table('settings')->select('company_pin')->get();
	    	foreach($client_db as $m):
	    		if($company_pin == $m->company_pin):
	    			CRUDBooster::redirect($_SERVER['HTTP_REFERER'],trans('crudbooster.pin_already_in_use'),"info"); 
	    		endif;
	    	endforeach;

	    	$company_name 			= $request->company_name;
	    	$company_address		= $request->company_address;
	    	$company_country		= $request->company_country;
	    	$photo                  = $request->file('company_logo');
	        $photo_db               = "uploads/client_photo/". time() . '.' . $photo->getClientOriginalExtension();
	        $photo_upload           = time() . '.' . $photo->getClientOriginalExtension();
	        Image::make($photo)->fit(136,131)->save('../storage/app/uploads/client_photo/' . $photo_upload);
	    	$company_info			=	$request->company_info;
	    	$company_email			=	$request->company_email;
	    	$company_phone			=	$request->company_phone;
	    	$company_contact		=	$request->company_contact;
	    	$company_language		=	$request->company_language;
	    	$company_currency		=   $request->company_currency;
	    	$company_website		= 	$request->company_website;
	    	$subscription_id		= 	$request->subscription_id;
	    	$company_include_tax 	=   $request->have_tax;
	    
	    	if($company_include_tax == null):
	    		$include_tax		= 0;
	    		$company_sale_tax	= 0; 
	    	else:
	    		$include_tax		= 1;
	    		$company_sale_tax	= $request->company_sale_tax; 
	    	endif;

			$dataSet = [
		        'company_name'  		=>  $company_name,
		        'company_address'   	=>  $company_address,
		        'company_country'		=>  $company_country,
		        'company_logo'      	=>  $photo_db,
		        'company_info' 			=>  $company_info,
		        'company_email' 		=>  $company_email,
		        'company_contact'		=> 	$company_contact,
		        'company_phone'			=>	$company_phone,
		        'company_lang'			=>  $company_language,
		        'company_currency'		=>  $company_currency,
		        'include_tax'			=>  $include_tax,
		        'company_sale_tax'		=>  $company_sale_tax,
		        'company_website'		=>  $company_website,	
		        'created_by' 			=>  $myID,
		        'company_pin'			=>  $company_pin,
		        'status'				=>  1,
		        'subscription_id'		=>  $subscription_id,
		        'created_by'			=>  $myID,
		        'created_at'			=>  now(),
			]; 

	    	if($dataSet != null):
	    		// Insert outlet into client's DB
	    		$data 				= DB::table('settings')->insert($dataSet);

	    		// Insert outlet into PLANB's DB
	    		$db_ext 			= \DB::connection('mysql_external');
	    		$server_user 		= $db_ext->table('settings')->insert($dataSet);
	    		
	    		// Generate 4 role for client outlet : Admin, Manager, Cashier, Waiter
	    		$last_store_insert  = DB::table('settings')->where('created_by',$myID)->OrderBy('id','DESC')->first();
	    		DB::table('cms_users')->where('id',$myID)->update(['company_id' => $last_store_insert->id]);

	    		// Admin Role
	    		$this->add_cms_menu("Admin", $last_store_insert->company_id , DB::table('cms_privileges')->max('id') + 1, 
	        			[35,56,42,49,59,43,3,33,4,39,7,14,44,2,1,17,60,29,21,23,24,25,27,51,57,40,18,62,61,11,9,10,30],
	        			[44,51,56,59,15,42,16,48,18,25,14,13,60,38,30,32,33,34,36,62,27,64,63,22,20,21,39]		

	        				);

	    		// Manager Role
		        $this->add_cms_menu("Manager", $last_store_insert->company_id , DB::table('cms_privileges')->max('id') + 1, 
		        		[35,56,42,49,59,43,3,33,4,39,7,14,44,2,1,51,57,40,18,62,61,11,9,10,30],
	        			[44,51,56,59,15,42,16,48,18,25,14,13,62,27,64,63,22,20,21,39]
		        				);

		        // Cashier Role
		        $this->add_cms_menu("Cashier", $last_store_insert->company_id , DB::table('cms_privileges')->max('id') + 1, 
		        		[35,56,42,49,59,51,57],
		        		[44,51,56,59,62]			
		        				);

		        // Waiter Role
		        $this->add_cms_menu("Waiter", $last_store_insert->company_id , DB::table('cms_privileges')->max('id') + 1, 
		        		[], 
		        		[]
		        				);

		        // Add company pin to public folder for offline purpose
		        $company_pin_db = "[{'company_pin':".$last_store_insert->company_pin."'}]";
		        File::put('company_pin.json', $company_pin_db);
	    		CRUDBooster::redirect($_SERVER['HTTP_REFERER'],trans('crudbooster.successfully_created_outlet'),"info");
	    	endif;
	    }

	    // ========================== Customize Dashbpoard Setting  (functionality) ================================ 
	    public function insert_dashboard_setting(Request $request){
	    	$myID 			= CRUDBooster::myId();

	    	$store_owner 	= $request->store_owner;
	    	if($store_owner == 'on'):
	    		DB::table('dashboard_settings')->where('user_id',$myID)->update(['store_owner' => 1]);
	    	else:
	    		DB::table('dashboard_settings')->where('user_id',$myID)->update(['store_owner' => 0]);
	    	endif;


	    	$income      	= $request->income;
	    	if($income == 'on'):
	    		DB::table('dashboard_settings')->where('user_id',$myID)->update(['income' => 1]);
	    	else:
	    		DB::table('dashboard_settings')->where('user_id',$myID)->update(['income' => 0]);
	    	endif;


	    	$user_management 	= $request->user_management;
	    	if($user_management == 'on'):
	    		DB::table('dashboard_settings')->where('user_id',$myID)->update(['user_management' => 1]);
	    	else:
	    		DB::table('dashboard_settings')->where('user_id',$myID)->update(['user_management' => 0]);
	    	endif;


			$menu_management 	= $request->menu_management;
			if($menu_management == 'on'):
				DB::table('dashboard_settings')->where('user_id',$myID)->update(['menu' => 1]);
			else:
				DB::table('dashboard_settings')->where('user_id',$myID)->update(['menu' => 0]);
			endif;


			$promotion_pack  	= $request->promotion_pack;
			if($promotion_pack == 'on'):
				DB::table('dashboard_settings')->where('user_id',$myID)->update(['promotion_pack' => 1]);
			else:
				DB::table('dashboard_settings')->where('user_id',$myID)->update(['promotion_pack' => 0]);
			endif;


			$table_management	= $request->table_management;
			if($table_management == 'on'):
				DB::table('dashboard_settings')->where('user_id',$myID)->update(['table_management' => 1]);
			else:
				DB::table('dashboard_settings')->where('user_id',$myID)->update(['table_management' => 0]);
			endif;


			$store_setting		= $request->store_setting;
			if($store_setting == 'on'):
				DB::table('dashboard_settings')->where('user_id',$myID)->update(['store_setting' => 1]);
			else:
				DB::table('dashboard_settings')->where('user_id',$myID)->update(['store_setting' => 0]);
			endif;

			$table_sold			= $request->table_sold;
			if($table_sold == 'on'):
				DB::table('dashboard_settings')->where('user_id',$myID)->update(['table_sold' => 1]);
			else:
				DB::table('dashboard_settings')->where('user_id',$myID)->update(['table_sold' => 0]);
			endif;

			$sold_total		= $request->sold_total;
			if($sold_total == 'on'):
				DB::table('dashboard_settings')->where('user_id',$myID)->update(['sold_total' => 1]);
			else:
				DB::table('dashboard_settings')->where('user_id',$myID)->update(['sold_total' => 0]);
			endif;

			CRUDBooster::redirect($_SERVER['HTTP_REFERER'], trans("crudbooster.alert_update_data_success"), "info");
	    }

	    // ========================== Next Version : Show all branch in thier outlet (view) ================================ 
	    public function show_box_branch(Request $request){
	    	$myID 					= CRUDBooster::myId();
	    	$user 					= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
	    	$current_branch			= DB::table('settings')->select('id','company_name','company_address')
	    							->where('id',$user->company_id)
	    							->first();
	    	$get_branch 			= DB::table('settings')->select('id','company_name','company_address')
	    							->where('created_by',$myID)
	    							->where('id','<>',$user->company_id)
	    							->get();

	    	?>

	    	<?php if($current_branch):?> 
	    		<div class="row">
	    			<div class="col-md-12">
	    				<div class="box_branch main_branch">
		                <div class="information_of_branch">
			                <div class="branch_name">Current Branch : <?php echo $current_branch->company_name; ?></div>
			                <div class="branch_location">
			                	Address : <?php echo $current_branch->company_address; ?></div>
		                </div>
		                </div>
	    			</div>
	    		</div>

	    	<?php endif;?>


	    	<?php if($get_branch!=null): ?>
	    		<div class="row"> 
		    	<?php foreach ($get_branch as $key => $get_branchs): ?>
		    		
			    	<div class="col-md-6 ">
			    		<div class="box_branch branch" data-id="<?php echo $get_branchs->id; ?>">
		                <div class="information_of_branch">
			                <div class="branch_name">Other Branch : <?php echo $get_branchs->company_name; ?></div>
			                <div class="branch_location" ">
			                	Address : <?php echo $get_branchs->company_address; ?></div>
		                </div>
		                </div>
		            </div> 
		            
	            <?php endforeach; ?>
	            </div>
            <?php endif; ?>

	    	<?php 
	    }

	    // ========================== Next Version : Admin,Legal request changing branch (function) ================================ 
	    public function change_admin_branch(Request $request){
	    	$branch_id = $request->branch_id;
	    	$myID      = CRUDBooster::myId();
	    	
	    	DB::table('cms_users')->where('id',$myID)->update(['company_id'=>$branch_id]);
	    }


	    // ==========================  Add default role (include thier menu accessability) (function) ================================ 
	    public function add_cms_menu($p_name, $p_company_id , $last_id, array $menu, array $modul ){
	    	DB::table('cms_privileges')->insert(
	        	[	
	        		'id'				=> $last_id,
	        		'name'				=> $p_name,
	        		'is_superadmin'		=> '0',
	        		'is_owner'			=> '0',
	        		'theme_color'		=> 'skin-green',
	        		'company_id'		=> $p_company_id,
	        		'created_at'		=> Carbon::now(),
	        	]
	        );

	    	if($menu):
		    	foreach($menu as $menu_id):
		        DB::table('cms_menus_privileges')->insert(
		        	[
		        		'id_cms_menus' 		=> $menu_id,
		        		'id_cms_privileges'	=> $last_id,
		        	]
		    	);
		    	endforeach;
	    	endif;

	    	if($modul):
		    	foreach($modul as $modul_id):
		        DB::table('cms_privileges_roles')->insert([
		        		'is_visible'		=> '1',
		        		'is_create'			=> '1',
		        		'is_read'			=> '1',
		        		'is_edit'			=> '1',
		        		'is_delete'			=> '1',
		        		'id_cms_privileges'	=> $last_id,
		        		'id_cms_moduls'		=> $modul_id,
		        ]);
		    	endforeach;
	    	endif;

	    }


}