<?php namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use DB;
use CRUDBooster;
use Carbon\Carbon;

class AdminRolesController extends \crocodicstudio\crudbooster\controllers\CBController
{

    public function cbInit()
    {
            # START CONFIGURATION DO NOT REMOVE THIS LINE
            $this->title_field          = "name";
            $this->limit                = "20";
            $this->orderby              = "id,desc";
            $this->global_privilege     = false;
            $this->button_table_action  = true;
            $this->button_bulk_action   = true;
            $this->button_action_style  = "button_icon";
            $this->button_add           = true;
            $this->button_edit          = true;
            $this->button_delete        = true;
            $this->button_detail        = true;
            $this->button_show          = false;
            $this->button_filter        = false;
            $this->button_import        = false;
            $this->button_export        = false;
            $this->table                = "cms_privileges";
            # END CONFIGURATION DO NOT REMOVE THIS LINE

            # START COLUMNS DO NOT REMOVE THIS LINE
            $this->col = [];
            $this->col[] = ["label"=>"Name","name"=>"name"];
            # END COLUMNS DO NOT REMOVE THIS LINE

            # START FORM DO NOT REMOVE THIS LINE
            $this->form = [];
            // $this->form[] = ['label'=>'Name','name'=>'name','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
            # END FORM DO NOT REMOVE THIS LINE

            # OLD START FORM
            //$this->form = [];
            //$this->form[] = ['label'=>'Name','name'=>'name','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
            # OLD END FORM

            /*
        | ----------------------------------------------------------------------
        | Sub Module
        | ----------------------------------------------------------------------
        | @label          = Label of action
        | @path           = Path of sub module
        | @foreign_key    = foreign key of sub table/module
        | @button_color   = Bootstrap Class (primary,success,warning,danger)
        | @button_icon    = Font Awesome Class
        | @parent_columns = Sparate with comma, e.g : name,created_at
        |
        */
        $this->sub_module = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Action Button / Menu
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
        | @icon        = Font awesome class icon. e.g : fa fa-bars
        | @color       = Default is primary. (primary, warning, succecss, info)
        | @showIf      = If condition when action show. Use field alias. e.g : [id] == 1
        |
        */

        $this->addaction = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Button Selected
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @icon        = Icon from fontawesome
        | @name        = Name of button
        | Then about the action, you should code at actionButtonSelected method
        |
        */
        $this->button_selected = array();


        /*
        | ----------------------------------------------------------------------
        | Add alert message to this module at overheader
        | ----------------------------------------------------------------------
        | @message = Text of message
        | @type    = warning,success,danger,info
        |
        */
        $this->alert = array();


        /*
        | ----------------------------------------------------------------------
        | Add more button to header button
        | ----------------------------------------------------------------------
        | @label = Name of button
        | @url   = URL Target
        | @icon  = Icon from Awesome.
        |
        */
        $this->index_button = array();


        /*
        | ----------------------------------------------------------------------
        | Customize Table Row Color
        | ----------------------------------------------------------------------
        | @condition = If condition. You may use field alias. E.g : [id] == 1
        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
        |
        */
        $this->table_row_color = array();


        /*
        | ----------------------------------------------------------------------
        | You may use this bellow array to add statistic at dashboard
        | ----------------------------------------------------------------------
        | @label, @count, @icon, @color
        |
        */
        $this->index_statistic = array();


        /*
        | ----------------------------------------------------------------------
        | Add javascript at body
        | ----------------------------------------------------------------------
        | javascript code in the variable
        | $this->script_js = "function() { ... }";
        |
        */
        $this->script_js = "";


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code before index table
        | ----------------------------------------------------------------------
        | html code to display it before index table
        | $this->pre_index_html = "<p>test</p>";
        |
        */
        $this->pre_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code after index table
        | ----------------------------------------------------------------------
        | html code to display it after index table
        | $this->post_index_html = "<p>test</p>";
        |
        */
        $this->post_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include Javascript File
        | ----------------------------------------------------------------------
        | URL of your javascript each array
        | $this->load_js[] = asset("myfile.js");
        |
        */
        $this->load_js[] = asset("/js/myjs.js");


        /*
        | ----------------------------------------------------------------------
        | Add css style at body
        | ----------------------------------------------------------------------
        | css code in the variable
        | $this->style_css = ".style{....}";
        |
        */
        $this->style_css = NULL;


        /*
        | ----------------------------------------------------------------------
        | Include css File
        | ----------------------------------------------------------------------
        | URL of your css each array
        | $this->load_css[] = asset("myfile.css");
        |
        */
        $this->load_css = array();
        $this->load_css[] = asset('css/no_data.css');


    }


    /*
    | ----------------------------------------------------------------------
    | Hook for button selected
    | ----------------------------------------------------------------------
    | @id_selected = the id selected
    | @button_name = the name of button
    |
    */
    public function actionButtonSelected($id_selected, $button_name)
    {
        //Your code here

    }


    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate query of index result
    | ----------------------------------------------------------------------
    | @query = current sql query
    |
    */
    public function hook_query_index(&$query)
    {
        $myID = CRUDBooster::myId();
        $user = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $query->where('cms_privileges.company_id', $user->company_id);

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate row of index table html
    | ----------------------------------------------------------------------
    |
    */
    public function hook_row_index($column_index, &$column_value)
    {
        //Your code here
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before add data is execute
    | ----------------------------------------------------------------------
    | @arr
    |
    */
    public function hook_before_add(&$postdata)
    {

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after add public static function called
    | ----------------------------------------------------------------------
    | @id = last insert id
    |
    */
    public function hook_after_add($id)
    {
        //Your code here
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before update data is execute
    | ----------------------------------------------------------------------
    | @postdata = input post data
    | @id       = current id
    |
    */
    public function hook_before_edit(&$postdata, $id)
    {

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after edit public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_edit($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command before delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_before_delete($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_delete($id)
    {
        //Your code here


    }


    //By the way, you can still create your own method in here... :)

    public function getSetStatus($status, $id)
    {

        if ($status == "active") {
            DB::table('roles')->where('id', $id)->update(['status' => 1]);
            CRUDBooster::redirect($_SERVER['HTTP_REFERER'], "Role has been updated!", "info");
        } else if ($status == "pending") {
            DB::table('roles')->where('id', $id)->update(['status' => 0]);
            CRUDBooster::redirect($_SERVER['HTTP_REFERER'], "Role has been updated!", "info");
        }

    }


    public function getAdd(){

        $myID = CRUDBooster::myId();
        $user = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();

        $data                   = [];
        $data['page_title']     = trans("role.role");
        $data['main_moduls']    = DB::table('cms_menus')
                                ->leftjoin('cms_moduls','cms_moduls.name','=','cms_menus.name')
                                ->select('cms_moduls.*','cms_moduls.id as modul_id','cms_menus.*','cms_menus.id as menu_id')
                                ->where('cms_menus.is_active','1')->where('parent_id','==','0')->where('cms_menus.super_admin',null)
                                ->orderby('cms_menus.name','ASC')
                                ->get();
        $data['command']        = 'add';
            
        $this->cbView('user.role_privilege',$data);
    }


    public function getEdit($id){
        $myID = CRUDBooster::myId();
        $user = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();

        $data = [];
        $data['page_title']     = trans("role.role");
        $data['roles']          = DB::table('cms_privileges')->where('id',$id)->first();
        $data['row']            = DB::table('cms_privileges')->where("id", $id)->first();
        $data['main_moduls']    = DB::table('cms_menus')
                                ->leftjoin('cms_moduls','cms_moduls.name','=','cms_menus.name')
                                ->select('cms_moduls.*','cms_moduls.id as modul_id','cms_menus.*','cms_menus.id as menu_id')
                                ->where('cms_menus.is_active','1')->where('parent_id','==','0')->where('cms_menus.super_admin',null)
                                ->orderby('cms_menus.name','ASC')
                                ->get();
        $data['command']        = 'update';
        
            
        $this->cbView('user.role_privilege',$data);
    }

    public function getDelete($id){
        DB::table('cms_privileges')->where('id',$id)->delete();
        DB::table('cms_privileges_roles')->where('id_cms_privileges',$id)->delete();
        DB::table('cms_menus_privileges')->where('id_cms_privileges',$id)->delete();
        

        CRUDBooster::redirect(CRUDBooster::mainpath(), trans("crudbooster.alert_delete_data_success"), 'success');
    }

    public function save_role(Request $request){
        $myID       = CRUDBooster::myId();
        $user       = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $now        = Carbon::now();
        $role       = $request->role;       // privilege name
        $is_mobile  = $request->is_mobile;  // privilege is mobile or not
        $priv       = $request->privileges; // privilege data

        $privilege_dataset = [
            'id'                => DB::table('cms_privileges')->max('id') + 1,
            'name'              => $role,
            'is_superadmin'     => '0',
            'is_owner'          => '2',
            'theme_color'       => 'skin-green',
            'company_id'        => $user->company_id,
            'created_at'        => $now,
        ];

        // insert new privilege
        DB::table('cms_privileges')->insert($privilege_dataset);


        $last_role  = DB::table('cms_privileges')->where('company_id',$user->company_id)->orderby('id','DESC')->first();
        if ($priv):
            foreach ($priv as $id_modul => $data):
                $getMenuName                = DB::table('cms_menus')->select('name')->where('id',$id_modul)->first();
                $getModul                   = DB::table('cms_moduls')->where('name',$getMenuName->name)->select('id')->first();
                $arrs                       = [];
                $arrs['id']                 = DB::table('cms_privileges_roles')->max('id') + 1;
                $arrs['is_visible']         = @$data['is_visible'] ?: 0;
                $arrs['is_create']          = @$data['is_visible'] ?: 0;
                $arrs['is_read']            = @$data['is_visible'] ?: 0;
                $arrs['is_edit']            = @$data['is_visible'] ?: 0;
                $arrs['is_delete']          = @$data['is_visible'] ?: 0;
                $arrs['id_cms_privileges']  = $last_role->id;
                $arrs['id_cms_moduls']      = $getModul->id;
                $arrs['created_at']         = $now;

                DB::table("cms_privileges_roles")->insert($arrs);
                $getMenuFromRole = DB::table('cms_privileges_roles')
                                    ->where('id_cms_privileges',$last_role->id)
                                    ->select('id_cms_moduls','id_cms_privileges')->first();

                $getParentMenu = DB::table('cms_menus')
                                    ->where('parent_id','<>','0')
                                    ->where('id',$id_modul)
                                    ->select('parent_id')->first();

                $menu_privilege = [
                    'id_cms_menus'      => $id_modul,
                    'id_cms_privileges' => $getMenuFromRole->id_cms_privileges
                ];

                $menu_privilege_for_user = [
                    'id_cms_menus'      => $getParentMenu->parent_id,
                    'id_cms_privileges' => $last_role->id
                ];

                // Insert Menu Privilege
                DB::table("cms_menus_privileges")->insert($menu_privilege);
                DB::table("cms_menus_privileges")->insert($menu_privilege_for_user);
            endforeach;
            CRUDBooster::redirect(CRUDBooster::mainpath(), trans("crudbooster.alert_add_data_success"), 'success');
        endif;
    }


    public function update_role(Request $request){
        $myID   = CRUDBooster::myId();
        $user   = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $now    = Carbon::now();
        $id     = $request->role_id;    // privilege id
        $priv   = $request->privileges; // privilege data
        $role   = $request->role;       // privilege name


        DB::table('cms_privileges')->where('id',$id)->update(
            [
                'name'              => $role,
                'is_superadmin'     => '0',
                'is_owner'          => '0',
                'theme_color'       => 'skin-green',
                'company_id'        => $user->company_id,
                'updated_at'        => $now,
            ]
        );

        DB::table("cms_privileges_roles")->where("id_cms_privileges", $id)->delete();
        

            if ($priv):
                foreach ($priv as $id_modul => $data):
                    $getMenuName    = DB::table('cms_menus')->select('name')->where('id',$id_modul)->first();

                    $getModul       = DB::table('cms_moduls')->where('name',$getMenuName->name)->select('id')->first();

                    $currentPermission  = DB::table('cms_privileges_roles')
                                        ->where('id_cms_moduls', $getModul->id)->where('id_cms_privileges', $id)->first();

                    if ($currentPermission):
                        $arrs = [];
                        $arrs['is_visible'] = @$data['is_visible'] ?: 0;
                        $arrs['is_create']  = @$data['is_visible'] ?: 0;
                        $arrs['is_read']    = @$data['is_visible'] ?: 0;
                        $arrs['is_edit']    = @$data['is_visible'] ?: 0;
                        $arrs['is_delete']  = @$data['is_visible'] ?: 0;
 
                        DB::table('cms_privileges_roles')->where('id', $currentPermission->id)->update($arrs);
                    else:

                        $arrs                       = [];
                        $arrs['id']                 = DB::table('cms_privileges_roles')->max('id') + 1;
                        $arrs['is_visible']         = @$data['is_visible'] ?: 0;
                        $arrs['is_create']          = @$data['is_visible'] ?: 0;
                        $arrs['is_read']            = @$data['is_visible'] ?: 0;
                        $arrs['is_edit']            = @$data['is_visible'] ?: 0;
                        $arrs['is_delete']          = @$data['is_visible'] ?: 0;
                        $arrs['id_cms_moduls']      = $getModul->id;
                        $arrs['id_cms_privileges']  = $id;  
                        
                        DB::table("cms_privileges_roles")->insert($arrs);
                         DB::table("cms_menus_privileges")->where("id_cms_privileges", $id)->delete();
                       
                        
                    endif;



                endforeach;


                        $getMenuFromRole = DB::table('cms_privileges_roles')
                                            ->leftjoin('cms_moduls','cms_moduls.id','=','cms_privileges_roles.id_cms_moduls')
                                            ->leftjoin('cms_menus','cms_menus.name','=','cms_moduls.name')
                                            ->where('cms_privileges_roles.id_cms_privileges',$id)->select('cms_menus.id as id','id_cms_moduls','cms_privileges_roles.id_cms_privileges as id_cms_privileges')->get();

                        


                        
                        foreach($getMenuFromRole as $menu):  
                            DB::table("cms_menus_privileges")->insert(
                                    [
                                        'id_cms_menus'      => $menu->id,
                                        'id_cms_privileges' => $id
                                    ]
                                );


                            $getParentMenu = DB::table('cms_menus')
                                            ->where('parent_id','<>','0')
                                            ->where('id',$menu->id)
                                            ->select('parent_id')->first();


                            if($getParentMenu >= 1):
                            DB::table("cms_menus_privileges")->insert(
                                    [
                                        'id_cms_menus'      => $getParentMenu->parent_id,
                                        'id_cms_privileges' => $id
                                    ]
                                );  
                            endif;
                        endforeach;






            else:
                DB::table("cms_menus_privileges")->where("id_cms_privileges", $id)->delete();
            endif;


    
        CRUDBooster::redirect(CRUDBooster::mainpath(), trans("crudbooster.alert_update_data_success", [
            'module' => "Privilege",
            'title' => $row->name,
        ]), 'success');
    }


}