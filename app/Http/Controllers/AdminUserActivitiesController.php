<?php namespace App\Http\Controllers;

	use Session;
	use Illuminate\Http\Request;
	use DB;
	use CRUDBooster;
	use Carbon\Carbon;

	class AdminUserActivitiesController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field 			= "id";
			$this->limit 				= "20";
			$this->orderby 				= "id,desc";
			$this->global_privilege 	= false;
			$this->button_table_action 	= false;
			$this->button_bulk_action 	= false;
			$this->button_action_style 	= "button_icon";
			$this->button_add 			= false;
			$this->button_edit 			= false;
			$this->button_delete 		= false;
			$this->button_detail 		= false;
			$this->button_show 			= false;
			$this->button_filter 		= false;
			$this->button_import 		= false;
			$this->button_export 		= false;
			$this->table 				= "log_activities";
			# END CONFIGURATION DO NOT REMOVE THIS LINE
		}

	    // =========================== Index of user activity (View) ===========================
	    public function getIndex()
	    {	
	    	$myID 				= CRUDBooster::myId();
   	 		$user 				= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
	    	$data 				= [];
	    	$data['page_title'] = trans("useractivity.user_activity");

	    	if($user->user_pin != null):
	    		$data['logs']		= DB::table('cms_logs')
	    							->leftjoin('cms_users','cms_users.id','=','cms_logs.id_cms_users')
	    							->select('cms_logs.id','description','cms_logs.created_at')
	    							->where('company_id',$user->company_id)->where('user_pin','<>',null)->orderBy('cms_logs.id', 'DESC')->paginate(10);
	    	else:
	    		$data['logs']		= DB::table('cms_logs')
	    							->leftjoin('cms_users','cms_users.id','=','cms_logs.id_cms_users')
	    							->select('cms_logs.id','description','cms_logs.created_at')
	    							->where('company_id',$user->company_id)->orderBy('cms_logs.id', 'DESC')->paginate(10);
	    	endif;
	    	
	    	$this->cbView('setting.log_activity.log_activity',$data);
	    }

	    // =========================== View user activity detail (View) ===========================
	    public function view_log_detail(Request $request){
	    	$myID 			= CRUDBooster::myId();
   	 		$user 			= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
   	 		$log_id 		= $request->log_id;

   	 		if($log_id != null):
   	 			$log_data 	= DB::table('cms_logs')->where('id',$log_id)->first();
   	 			$username   = DB::table('cms_users')->select('name')->where('id',$log_data->id_cms_users)->first();
   	 		?>

   	 		<table class="log_detail table">
   	 			<tbody class="dir_table">
   	 			<!-- <tr class="ip">
   	 				<td class="label_lenght">IP Address</td>
   	 				<td><?php echo $log_data->ipaddress; ?></td>
   	 			</tr> -->

   	 			<tr class="user_browser">
   	 				<td class="label_lenght">User Browser</td>
   	 				<td><?php 
   	 						if($log_data->useragent == 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36' || $log_data->useragent == "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36" || $log_data->useragent == "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36"):
   	 						echo "<img src='../uploads/defualt_image/windows.png'/>";
   	 						echo "<img src='../uploads/defualt_image/chrome.png'/>";
   	 						endif;
   	 					?>
   	 				</td>
   	 			</tr>

   	 			<tr class="url">
   	 				<td class="label_lenght">Url</td>
   	 				<td><?php echo $log_data->url; ?></td>
   	 			</tr>

   	 			<tr class="description">
   	 				<td class="label_lenght">Description</td>
   	 				<td><?php echo $log_data->description; ?></td>
   	 			</tr>

   	 			<tr class="username">
   	 				<td class="label_lenght">Username</td>
   	 				<td><?php echo $username->name; ?></td>
   	 			</tr>

   	 			<tr class="created_at">
   	 				<td class="label_lenght">Created At</td>
   	 				<td><?php echo $log_data->created_at; ?></td>
   	 			</tr>
   	 			</tbody>

   	 		</table>

   	 		<?php
   	 		endif;
	    }

	    // =========================== Sort user activity (function) ===========================
	    public function get_sort(Request $request){
	    	   $totalDisplay=0;
	    	   if(!empty($request->total)){
	    	   	$totalDisplay=$request->total;
	    	   }
	    	   $myID=CRUDBooster::myId();	
				$request->date;
				$date = explode(" - ",$request->date);
				$startDate = $date[0]." 00:00:00";
				$endDate = $date[1]." 23:59:59";

				if ($startDate == $endDate) {
					$startDate = $date[0]." 00:00:00";
					$endDate = $date[1]." 23:59:59";
				}

				$record = $request->record;
				
				$get_employee = DB::table('cms_users')->select('id')->where('created_by',$myID)->get();
				$array=[];
				foreach ($get_employee as $key => $value) {
					array_push($array, $value->id);
					array_push($array, $myID);
				}


				$logs=DB::table('cms_logs')
					   ->where('created_at','>=',$startDate)
					   ->where('created_at','<',$endDate)
					   ->whereIn('id_cms_users',$array)  
					   ->orderby('id', 'DESC');
					  if($totalDisplay > 0){
						$logs=$logs->limit(10)->offset($totalDisplay)->get();
					  }else{
 						$logs=$logs->limit($record)->offset(0)->get();
					  }		
				
						
				if(count($logs)>=1){ ?>
			
				<?php foreach ($logs as $key_log => $log) { $totalDisplay++;?>
				<tr>
					<td class="col-md-1"><?php echo $totalDisplay; ?></td>
					<td><?php echo $log->description; ?></td>
					<td><?php echo $log->created_at; ?></td>
					<td>
									
						<div class="button_action" style="text-align:left">

		                <a class="btn btn-xs btn-primary btn-detail" title="Detail Data" href="<?php echo route('log_activity_detail', ['id' => $log->id]);?>"><i class="fa fa-eye"></i></a>
		    			
    
						</div>
					</td>
				</tr>
				


				
				
				
				<?php } 
				
			 }


	    }

	    // =========================== Search user activity (function) ===========================
	    public function get_search(Request $request)
	    {
	    	$myID 			= CRUDBooster::myId();
   	 		$user 			= DB::table('cms_users')->select('company_id','user_pin')->where('id',$myID)->first();
			
			$search 		= $request->search;
			$r_date 		= $request->date;

			$date 			= explode(" - ",$r_date);				
			$startDate 		= $date[0]." 00:00:00";
			$endDate 		= $date[1]." 23:59:59";

			if($r_date != ''):

				if($user->user_pin != null):
		    		$log_data		= DB::table('cms_logs')
		    							->leftjoin('cms_users','cms_users.id','=','cms_logs.id_cms_users')
		    							->select('cms_logs.id','description','cms_logs.created_at')
		    							->where('company_id',$user->company_id)
		    							->where('cms_logs.created_at','>=',$startDate)
						   				->where('cms_logs.created_at','<',$endDate)
		    							->where('description','LIKE','%'.$search.'%')
		    							->where('user_pin','<>',null)->orderBy('cms_logs.id', 'DESC')->get();
		    	else:
		    		$log_data		= DB::table('cms_logs')
		    							->leftjoin('cms_users','cms_users.id','=','cms_logs.id_cms_users')
		    							->select('cms_logs.id','description','cms_logs.created_at')
		    							->where('cms_logs.created_at','>=',$startDate)
						   				->where('cms_logs.created_at','<',$endDate)
		    							->where('description','LIKE','%'.$search.'%')
		    							->where('company_id',$user->company_id)->orderBy('cms_logs.id', 'DESC')->get();
		    	endif;

		    else:

		    	if($user->user_pin != null):
		    		$log_data		= DB::table('cms_logs')
		    							->leftjoin('cms_users','cms_users.id','=','cms_logs.id_cms_users')
		    							->select('cms_logs.id','description','cms_logs.created_at')
		    							->where('company_id',$user->company_id)
		    							->where('description','LIKE','%'.$search.'%')
		    							->where('user_pin','<>',null)->orderBy('cms_logs.id', 'DESC')->get();
		    	else:
		    		$log_data		= DB::table('cms_logs')
		    							->leftjoin('cms_users','cms_users.id','=','cms_logs.id_cms_users')
		    							->select('cms_logs.id','description','cms_logs.created_at')
		    							->where('description','LIKE','%'.$search.'%')
		    							->where('company_id',$user->company_id)->orderBy('cms_logs.id', 'DESC')->get();
		    	endif;

		    endif;


		    if($log_data->count() >= 1):
			    foreach($log_data as $key => $log):
			    ?>

			    <tr class="title">
	                    <td class="col-md-1"><?php echo ++$key; ?></td>
	                    <td><?php echo $log->description; ?></td>
	                    <td><?php echo $log->created_at; ?></td>
	                    <td>
	                       <a class="view_button unselectable" data-id="<?php echo $log->id; ?>">view detail</a>
	                    </td>
	             </tr>
					
			    <?php
				endforeach;
			else:
			?>
			<tr>
			        <td colspan="4" class="text-center"><h4>No Data!</h4></td>
			</tr>
			<?php
			endif;
	    }

	
	}