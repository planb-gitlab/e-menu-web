<?php namespace App\Http\Controllers;

	use Session;
	use Illuminate\Http\Request;
	use DB;
	use CRUDBooster;
	use Carbon\Carbon;

	class AdminBranchController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field 			= "company_name";
			$this->limit 				= "20";
			$this->orderby 				= "id,desc";
			$this->global_privilege 	= false;
			$this->button_table_action 	= true;
			$this->button_bulk_action	= true;
			$this->button_action_style 	= "button_icon";
			$this->button_add 			= true;
			$this->button_edit 			= true;
			$this->button_delete 		= true;
			$this->button_detail 		= true;
			$this->button_show 			= false;
			$this->button_filter 		= false;
			$this->button_import 		= false;
			$this->button_export 		= false;
			$this->table 				= "settings";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Branch Name","name"=>"company_name"];
			$this->col[] = ["label"=>"Branch Address","name"=>"company_address"];
			$this->col[] = ["label"=>"Branch Email","name"=>"company_email"];
			$this->col[] = ["label"=>"Branch Contact","name"=>"company_contact"];
			$this->col[] = ["label"=>"Branch Phone","name"=>"company_phone"];
			# END COLUMNS DO NOT REMOVE THIS LINE
	        
	        # START FORM DO NOT REMOVE THIS LINE
			$this->form = [];

			# END FORM DO NOT REMOVE THIS LINE

			
	    }

	    public function hook_query_index(&$query) {
	        
	        $myID = CRUDBooster::myId();
	        $user 		= DB::table('cms_users')->where('id',$myID)->select('company_id')->first();
	        $query->where('settings.created_by', $myID)->where('settings.id','<>',$user->company_id);

	            
	    }

	    // ============================ Create Branch (View) ============================
	    public function getAdd(){
	    	$myID 							= CRUDBooster::myId();

	    		
	    	$data							= [];
	    	$data['page_title_branch']		= trans('crudbooster.add_branch');
	    	$data['page_title_user']		= trans('crudbooster.admin_branch');
	    	$data['main_branch']    		= DB::table('settings')->where('created_by',$myID)->first();
	    	$data['countries']				= DB::table('countries')->get();
	    	$data['languages']				= DB::table('languages')->orderBy('id','DESC')->get();
	    	$data['tax_types']				= DB::table('tax_types')->get();
	    	$data['currencies']				= DB::table('currencies')->orderBy('id','ASC')->get();

	    	$this->cbView('branch.branch',$data);

	    }

	    // ============================ Update Branch (View) ============================
	    public function getEdit($id){
	    	$myID 							= CRUDBooster::myId();
	    		
	    	$data							= [];
	    	$data['page_title_branch']		= trans('crudbooster.add_branch');
	    	$data['page_title_user']		= trans('crudbooster.admin_branch');
	    	$data['main_branch']   			= DB::table('settings')->where('created_by',$myID)->first();
	    	$data['countries']				= DB::table('countries')->get();
	    	$data['languages']				= DB::table('languages')->orderBy('id','DESC')->get();
	    	$data['tax_types']				= DB::table('tax_types')->get();

	    	$data['branch']					= DB::table('settings')->where('id',$id)->first();
	    	$data['branch_admin']			= DB::table('cms_users')
	    									->where('created_by',$myID)
	    									->where('company_id',$data['branch']->id)
	    									->where('role',3)
	    									->where('user_type','end_user_branch_admin')->first();

	    	$this->cbView('branch.branch',$data);
	    }

	    // ============================ Create Branch (Function) ============================
	    public function save_branch(Request $request){
	    	$myID 		= CRUDBooster::myId();
	    	$get_company_id=DB::table('cms_users')
	    				->select('company_id')
	    				->where('id',$myID);
	    	$name 		= $request->branch_name;
	    	$address 	= $request->branch_address;
	    	$country	= $request->branch_country;
	    	$info 		= $request->branch_info;
	    	$email 		= $request->branch_email;
	    	$contact 	= $request->branch_contact;
	    	$phone 		= $request->branch_phone;
	    	$policy		= $request->branch_policy;
	    	$language   = $request->branch_language;
	    	$currency 	= $request->branch_currency;
	    	$branch_pin = $request->branch_pin;
	    	$created_by = $myID; 
	    	$status     = "1";
	    	$created_at = Carbon::now();

	    	if($name || $address || $countries || $email || $phone || $language || $branch_pin):
	    		DB::table('settings')->insert(
		    		[
		    			'company_name'		=> $name,
		    			'company_address'	=> $address,
		    			'company_country'	=> $country,
		    			'company_info'		=> $info,
		    			'company_email'		=> $email,
		    			'company_contact'	=> $contact,
		    			'company_phone'		=> $phone,
		    			'company_policy'	=> $policy,
		    			'company_lang'		=> $language,
		    			'company_currency'	=> $currency,
		    			'created_by'		=> $created_by,
		    			'status'			=> $status,
		    			'created_at'		=> $created_at,
		    			'company_pin'		=> $branch_pin

		    		]
	    		);
	    		

		    	DB::table('cms_users')->insert(
		    			[
		    				'name' 				=> "Admin",
			    			'phone' 			=> "09876543",
			    			'user_pin'			=> "0001",
			    			'company_pin'		=> $branch_pin,
			    			'created_by'		=> $myID,
			    			'user_type'			=> "end_user",
			    			'company_id'		=> $get_company_id->company_id,
			    			'created_at'		=> $created_at,
			    			'status'			=> 1
		    			]
		    		);
		    	

	    		CRUDBooster::redirect(g("return_url"), trans("crudbooster.alert_add_data_success"), 'success');
	    		CRUDBooster::redirect(Request::server('HTTP_REFERER'), trans("crudbooster.alert_add_data_success"), 'success');
	    	else:
	    		
	    		CRUDBooster::redirect($_SERVER['HTTP_REFERER'],trans('crudbooster.wrong_input'),"warning");
	    	endif;

	    }

	    // ============================ Update Branch (Function) ============================
	    public function update_branch(Request $request){
	    	$myID 		= CRUDBooster::myId();

	    	$id 		= $request->id;
	    	$name 		= $request->branch_name;
	    	$address 	= $request->branch_address;
	    	$country	= $request->branch_country;
	    	$info 		= $request->branch_info;
	    	$email 		= $request->branch_email;
	    	$contact 	= $request->branch_contact;
	    	$phone 		= $request->branch_phone;
	    	$policy		= $request->branch_policy;
	    	$language   = $request->branch_language;
	    	$branch_pin = $request->branch_pin;
	    	$created_by = $myID; 
	    	$status     = "1";
	    	$updated_at = Carbon::now();


	    	if($name || $address || $countries || $email || $phone || $language):
	    		DB::table('settings')->where('id',$id)->update(
		    		[
		    			'company_name'		=> $name,
		    			'company_address'	=> $address,
		    			'company_country'	=> $country,
		    			'company_info'		=> $info,
		    			'company_email'		=> $email,
		    			'company_contact'	=> $contact,
		    			'company_phone'		=> $phone,
		    			'company_policy'	=> $policy,
		    			'company_lang'		=> $language,
		    			'created_by'		=> $created_by,
		    			'status'			=> $status,
		    			'updated_at'		=> $updated_at,
		    		]
	    		);

	    		if($branch_pin != null || $branch_pin != ''):
	    			DB::table('settings')->where('id',$id)->update(
		    			[
		    				'company_pin'		=> $branch_pin
		    			]
		    		);
	    		endif;


	    		CRUDBooster::redirect(g("return_url"), trans("crudbooster.alert_add_data_success"), 'success');
	    		CRUDBooster::redirect(Request::server('HTTP_REFERER'), trans("crudbooster.alert_add_data_success"), 'success');
	    	else:
	    		
	    		CRUDBooster::redirect($_SERVER['HTTP_REFERER'],trans('crudbooster.wrong_input'),"warning");
	    	endif;
	    }

	    // ============================ WHAT IS THIS? (Function) ============================
	    public function save_branch_admin(Request $request){
	    	$myID 				= CRUDBooster::myId();

	    	$name 				= $request->admin_name;
	    	$email 				= $request->admin_email;
	    	$phone 				= $request->admin_phone;
	    	$password 			= $request->admin_password;
	    	$role 				= '3';
	    	$user_pin			= $request->admin_pin;
	    	$photo				= $request->file('admin_photo');
	    	$fileName			= "uploads"."/"."$myID"."/"."$now->year"."-"."$now->month/".$photo->getClientOriginalName();
	    	$photo->move("../storage/app/uploads/"."$myID"."/"."$now->year"."-"."$now->month/",$fileName);

	    	$company 	 		= DB::table('settings')->where('created_by',$myID)->first();
	    	$company_id 		= $company->id;
	    	$status     		= "1";
	    	$id_cms_privileges	= "2";
	    	$user_type			= "end_user_branch_admin";
	    	$created_at 		= Carbon::now();
	    	
	    	if($name || $photo || $phone || $role || $pin):
	    		DB::table('cms_users')->insert(
		    		[
		    			'name' 				=> $name,
		    			'email' 			=> $email,
		    			'phone' 			=> $phone,
		    			'password'			=> bcrypt($password),
		    			'user_pin'			=> $user_pin,
		    			'role'		 		=> $role,
		    			'created_by'		=> $myID,
		    			'company_id'		=> $company_id,
		    			'id_cms_privileges' => $id_cms_privileges,
		    			'user_type'			=> $user_type,
		    			'created_at'		=> $created_at,
		    			'status'			=> $status
		    		]
	    		);
	    	

	    		CRUDBooster::redirect(g("return_url"), trans("crudbooster.alert_add_data_success"), 'success');
	    		CRUDBooster::redirect(Request::server('HTTP_REFERER'), trans("crudbooster.alert_add_data_success"), 'success');
	    	else:
	    		
	    		CRUDBooster::redirect($_SERVER['HTTP_REFERER'],trans('crudbooster.wrong_input'),"warning");
	    	endif;
	    }

	    // ============================ WHAT IS THIS? (Function) ============================
	    public function update_branch_admin(Request $request){
	       	$myID 				= CRUDBooster::myId();

	    	$id 				= $request->admin_id;
	    	$name 				= $request->admin_name;
	    	$email 				= $request->admin_email;
	    	$phone 				= $request->admin_phone;
	    	$password 			= $request->admin_password;
	    	$role 				= '3';
	    	$user_pin			= $request->admin_pin;
	    	$photo				= $request->file('admin_photo');
	    	$fileName			= "uploads"."/"."$myID"."/"."$now->year"."-"."$now->month/".$photo->getClientOriginalName();
	    	$photo->move("../storage/app/uploads/"."$myID"."/"."$now->year"."-"."$now->month/",$fileName);

	    	$company 	 		= DB::table('settings')->where('created_by',$myID)->first();
	    	$company_id 		= $company->id;
	    	$status     		= "1";
	    	$id_cms_privileges	= "2";
	    	$user_type			= "end_user_branch_admin";
	    	$created_at 		= Carbon::now();

	    	if($name || $photo || $phone || $role || $pin):
	    		DB::table('cms_users')->where('id',$id)->update(
		    		[
		    			'name' 				=> $name,
		    			'email' 			=> $email,
		    			'phone' 			=> $phone,
		    			'password'			=> bcrypt($password),
		    			'user_pin'			=> $user_pin,
		    			'role'		 		=> $role,
		    			'created_by'		=> $myID,
		    			'company_id'		=> $company_id,
		    			'id_cms_privileges' => $id_cms_privileges,
		    			'user_type'			=> $user_type,
		    			'created_at'		=> $created_at,
		    			'status'			=> $status
		    		]
	    		);


	    		CRUDBooster::redirect(g("return_url"), trans("crudbooster.alert_add_data_success"), 'success');
	    		CRUDBooster::redirect(Request::server('HTTP_REFERER'), trans("crudbooster.alert_add_data_success"), 'success');
	    	else:
	    		
	    		CRUDBooster::redirect($_SERVER['HTTP_REFERER'],trans('crudbooster.wrong_input'),"warning");
	    	endif;
	    }


	}