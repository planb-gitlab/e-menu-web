<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;

	class AdminExchangeController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

	    	$myID 			= CRUDBooster::myId();
        	$companyId 			= getCompanyId($myID);
        	$company_data	= DB::table('settings')->select('company_currency')->where('id', $companyId)->first();
			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "id";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = false;
			$this->button_show = false;
			$this->button_filter = false;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "exchange";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			//$this->col[] = ["label"=>"Currency From","name"=>"currency_form","join"=>"currencies,name"];
			$this->col[] = ["label"=>"Currency To","name"=>"currency_to","join"=>"currencies,name"];
			//$this->col[] = ["label"=>"Price From","name"=>"price_from","callback_php"=>'number_format([price_from])'];
			$this->col[] = ["label"=>"Price To","name"=>"price_to"];
			$this->col[] = ["label"=>"Created At","name"=>"created_at"];
			$this->col[] = ["label"=>"Status","name"=>"status"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			/*$this->form[] = ['label'=>'Currency From','name'=>'currency_form','type'=>'select','validation'=>'required|integer|min:0','width'=>'col-sm-3','datatable'=>'currencies,name'];*/
			$this->form[] = ['label'=>trans('setting.Currency To'),'name'=>'currency_to','type'=>'select','validation'=>'required|integer|min:0','width'=>'col-sm-4','datatable'=>'currencies,name','datatable_where'=>'currencies.id != ' . $company_data->company_currency];
			/*$this->form[] = ['label'=>'Price From','name'=>'price_from','type'=>'number','validation'=>'required|min:0|numeric','width'=>'col-sm-4','placeholder'=>'You can only enter the number only'];*/
			$this->form[] = ['label'=>trans('setting.Price To'),'name'=>'price_to','type'=>'number','validation'=>'required|min:0','width'=>'col-sm-4','placeholder'=>'You can only enter the number only'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Currency From','name'=>'currency_form','type'=>'select','validation'=>'required|integer|min:0','width'=>'col-sm-3','datatable'=>'currencies,name'];
			//$this->form[] = ['label'=>'Price From','name'=>'price_from','type'=>'number','validation'=>'required|min:0|double','width'=>'col-sm-4', 'placeholder' => 'You can only enter the number only'];
			//$this->form[] = ['label'=>'Currency To','name'=>'currency_to','type'=>'select','validation'=>'required|integer|min:0','width'=>'col-sm-3','datatable'=>'currencies,name'];
			//$this->form[] = ['label'=>'Price To','name'=>'price_to','type'=>'number','validation'=>'required|min:0|double','width'=>'col-sm-4','placeholder' => 'You can only enter the number only'];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = "
	        	$(document).ready(function() {
				    $('#currency_to').select2();
				});
	        ";



            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        // $this->load_js[] = asset("js/same_currency.js");
	        // $this->load_js[] = asset("js/anti_duplicate_currencies.js");
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        $this->load_css[] = asset('css/no_data.css');
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here


	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        
	        $myID = CRUDBooster::myId();
        	$companyId = getCompanyId($myID);
	        $query->where('exchange.company_id', $companyId);
	            
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here

	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {     

	    	$myID = CRUDBooster::myId();
        	$companyId = getCompanyId($myID);
        	$company_data = DB::table('settings')->select('company_currency')->where('settings.id', $companyId)->first();   
	        $currency_from = $company_data->company_currency;
	        $currency_to = $postdata['currency_to'];
	        
			$checkExist	= DB::table('exchange')
			    		->where('currency_from',$currency_from)->where('currency_to',$currency_to)->where('company_id',$companyId)->get();
		        
		        if(count($checkExist) >= 1):
		        	CRUDBooster::redirect($_SERVER['HTTP_REFERER'],trans('crudbooster.exist_data'),"warning");
		        else:
		        	$currency_from_text = DB::table('currencies')->where('id',$currency_from)->first();
		        	$currency_to_text = DB::table('currencies')->where('id',$currency_to)->first();
		        	$postdata['currency_from'] = $currency_from;
		        	$postdata['currency_to'] = $currency_to;
		        	$postdata['price_from'] = '1';
		        	$postdata['currency_from_text']	= $currency_from_text->name;
		        	$postdata['currency_to_text'] = $currency_to_text->name;
		         	$postdata['created_by'] = $myID;
		         	$postdata['company_id'] = $companyId;
		     	endif;

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here
		    

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 
	    	CRUDBooster::redirect($_SERVER['HTTP_REFERER'],trans("setting.The company information has been updated !"),"info");
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }


	    

	    //By the way, you can still create your own method in here... :) 
		
	   


	    

	}