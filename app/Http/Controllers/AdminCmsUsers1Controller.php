<?php namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use DB;
use CRUDBooster;
use File;
use Image;
use Carbon\Carbon;
use URL;
use Response;

class AdminCmsUsers1Controller extends \crocodicstudio\crudbooster\controllers\CBController
{

    public function cbInit()
    {           
            $user = DB::table('cms_users')->select('company_id')->where('id',CRUDBooster::myId())->first();
			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field           = "name";
			$this->limit                 = "20";
			$this->orderby               = "id,desc";
			$this->global_privilege      = false;
			$this->button_table_action   = true;
			$this->button_bulk_action    = false;
			$this->button_action_style   = "button_icon";
			$this->button_add            = true;
			$this->button_edit           = true;
			$this->button_delete         = true;
			$this->button_detail         = false;
			$this->button_show           = false;
			$this->button_filter         = false;
			$this->button_import         = false;
			$this->button_export         = false;
			$this->table                 = "cms_users";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Name","name"=>"name"];
			$this->col[] = ["label"=>"Photo","name"=>"photo","image"=>true];
			$this->col[] = ["label"=>"Role","name"=>"id_cms_privileges","join"=>"cms_privileges,name"];
			$this->col[] = ["label"=>"Phone","name"=>"phone"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>trans('crudbooster.Name'),'name'=>'name','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
			$this->form[] = ['label'=>trans('crudbooster.Photo'),'name'=>'photo','type'=>'upload','width'=>'col-sm-10','validation'=>'required','help'=>trans('usermanagement.File types support : JPG, JPEG, PNG, GIF, BMP')];
			// $this->form[] = ['label'=>'Email','name'=>'email','type'=>'email','validation'=>'min:1|max:255|email|unique:cms_users','width'=>'col-sm-10','placeholder'=>'Please enter a valid email address'];
			$this->form[] = ['label'=>trans('crudbooster.Phone'),'name'=>'phone','type'=>'number','width'=>'col-sm-10','placeholder'=>'Input user phone number'];
			// $this->form[] = ['label'=>'Password','name'=>'password','type'=>'password','validation'=>'min:3|max:32','width'=>'col-sm-10','help'=>'Minimum 5 characters. Please leave empty if you did not change the password.'];

			$this->form[] = ['label'=>trans('crudbooster.Role'),'name'=>'id_cms_privileges','validation'=>'required','type'=>'select','width'=>'col-sm-10','datatable'=>'cms_privileges,name','datatable_where'=>'id != 1 && id != 2'];

			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Name','name'=>'name','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
			//$this->form[] = ['label'=>'Photo','name'=>'photo','type'=>'upload','width'=>'col-sm-10','help'=>'File types support : JPG, JPEG, PNG, GIF, BMP'];
			//$this->form[] = ['label'=>'Email','name'=>'email','type'=>'email','validation'=>'required|min:1|max:255|email|unique:cms_users','width'=>'col-sm-10','placeholder'=>'Please enter a valid email address'];
			//$this->form[] = ['label'=>'Password','name'=>'password','type'=>'password','validation'=>'min:3|max:32','width'=>'col-sm-10','help'=>'Minimum 5 characters. Please leave empty if you did not change the password.'];
			//$this->form[] = ['label'=>'Role','name'=>'role','type'=>'select','width'=>'col-sm-10','datatable'=>'roles,name'];
			# OLD END FORM

			/*
    | ----------------------------------------------------------------------
    | Sub Module
    | ----------------------------------------------------------------------
    | @label          = Label of action
    | @path           = Path of sub module
    | @foreign_key 	  = foreign key of sub table/module
    | @button_color   = Bootstrap Class (primary,success,warning,danger)
    | @button_icon    = Font Awesome Class
    | @parent_columns = Sparate with comma, e.g : name,created_at
    |
    */
        $this->sub_module = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Action Button / Menu
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
        | @icon        = Font awesome class icon. e.g : fa fa-bars
        | @color 	   = Default is primary. (primary, warning, succecss, info)
        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
        |
        */
        $this->addaction = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Button Selected
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @icon 	   = Icon from fontawesome
        | @name 	   = Name of button
        | Then about the action, you should code at actionButtonSelected method
        |
        */
        $this->button_selected = array();


        /*
        | ----------------------------------------------------------------------
        | Add alert message to this module at overheader
        | ----------------------------------------------------------------------
        | @message = Text of message
        | @type    = warning,success,danger,info
        |
        */
        $this->alert = array();


        /*
        | ----------------------------------------------------------------------
        | Add more button to header button
        | ----------------------------------------------------------------------
        | @label = Name of button
        | @url   = URL Target
        | @icon  = Icon from Awesome.
        |
        */
        $this->index_button = array();


        /*
        | ----------------------------------------------------------------------
        | Customize Table Row Color
        | ----------------------------------------------------------------------
        | @condition = If condition. You may use field alias. E.g : [id] == 1
        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
        |
        */
        $this->table_row_color = array();


        /*
        | ----------------------------------------------------------------------
        | You may use this bellow array to add statistic at dashboard
        | ----------------------------------------------------------------------
        | @label, @count, @icon, @color
        |
        */
        $this->index_statistic = array();


        /*
        | ----------------------------------------------------------------------
        | Add javascript at body
        | ----------------------------------------------------------------------
        | javascript code in the variable
        | $this->script_js = "function() { ... }";
        |
        */
        $this->script_js = NULL;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code before index table
        | ----------------------------------------------------------------------
        | html code to display it before index table
        | $this->pre_index_html = "<p>test</p>";
        |
        */
        $this->pre_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code after index table
        | ----------------------------------------------------------------------
        | html code to display it after index table
        | $this->post_index_html = "<p>test</p>";
        |
        */
        $this->post_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include Javascript File
        | ----------------------------------------------------------------------
        | URL of your javascript each array
        | $this->load_js[] = asset("myfile.js");
        |
        */
        $this->load_js = array();


        /*
        | ----------------------------------------------------------------------
        | Add css style at body
        | ----------------------------------------------------------------------
        | css code in the variable
        | $this->style_css = ".style{....}";
        |
        */
        $this->style_css = NULL;



        /*
        | ----------------------------------------------------------------------
        | Include css File
        | ----------------------------------------------------------------------
        | URL of your css each array
        | $this->load_css[] = asset("myfile.css");
        |
        */
        $this->load_css = array();
        $this->load_css[] = asset('css/no_data.css');
        $this->load_css[]   = asset('css/img_display_fit.css');


    }


    /*
    | ----------------------------------------------------------------------
    | Hook for button selected
    | ----------------------------------------------------------------------
    | @id_selected = the id selected
    | @button_name = the name of button
    |
    */
    public function actionButtonSelected($id_selected, $button_name)
    {
        //Your code here

    }


    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate query of index result
    | ----------------------------------------------------------------------
    | @query = current sql query
    |
    */
    public function hook_query_index(&$query)
    {
        $myID = CRUDBooster::myId();
        $user = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $query->where('cms_users.company_id', $user->company_id)->where('cms_users.created_by','<>','1');
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate row of index table html
    | ----------------------------------------------------------------------
    |
    */
    public function hook_row_index($column_index, &$column_value)
    {
        //Your code here
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before add data is execute
    | ----------------------------------------------------------------------
    | @arr
    |
    */
    public function hook_before_add(&$postdata)
    {

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after add public static function called
    | ----------------------------------------------------------------------
    | @id = last insert id
    |
    */
    public function hook_after_add($id)
    {
        DB::table('dashboard_settings')->insert(['user_id' => $id]);
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before update data is execute
    | ----------------------------------------------------------------------
    | @postdata = input post data
    | @id       = current id
    |
    */
    public function hook_before_edit(&$postdata, $id)
    {

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after edit public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_edit($id)
    {

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command before delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_before_delete($id)
    {   
        // ================ Delete User Dashboard ================
        DB::table('dashboard_settings')->where('user_id',$id)->delete();
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_delete($id)
    {

    }

    // =========================== User Add (View) ===========================
    public function getAdd(){
        $myID               = CRUDBooster::myId();
        $user               = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $data               = [];
        $data['page_title'] = trans('crudbooster.add_user');
        $data['roles']      = DB::table('cms_privileges')->where('is_superadmin','<>','1')->where('id','<>','1')->where('id','<>','2')->get();
        $this->cbView('user.user',$data);
    }

    // =========================== User Edit (View) ===========================
    public function getEdit($id){
        $myID               = CRUDBooster::myId();
        $user               = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $data               = [];
        $data['page_title'] = trans('crudbooster.update_user');
        $data['url']        = URL::to("/");
        $data['row']        = DB::table('cms_users')->select('id')->where('id',$id)->first();
        $data['user']       = DB::table('cms_users')->where('id',$id)->first();
        $data['roles']      = DB::table('cms_privileges')->where('is_superadmin','<>','1')->where('id','<>','1')->where('id','<>','2')->get();
        $this->cbView('user.user',$data);
    }

    // =========================== Save User ===========================
    public function save(Request $request){
        $myID       = CRUDBooster::myId();
        $user       = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $company    = DB::table('settings')->select('company_pin')->where('id',$user->company_id)->first();
        $name       = $request->name;
        $photo      = $request->file('photo');
        if($photo):
            $photo_db               = "uploads/client_photo/". time() . '.' . $photo->getClientOriginalExtension();
            $photo_upload           = time() . '.' . $photo->getClientOriginalExtension();
            Image::make($photo)->fit(90,90)->save('../storage/app/uploads/client_photo/' . $photo_upload);
        endif;
        $phone      = $request->phone;
        $role       = $request->role;
        $user_pin   = $request->user_pin;

        $client_db  = DB::table('cms_users')->select('user_pin')->where('company_id',$user->company_id)->get();
        foreach($client_db as $c):
            if($user_pin == $c->user_pin):
                CRUDBooster::redirect($_SERVER['HTTP_REFERER'],trans('crudbooster.pin_already_in_use'),"info"); 
            endif;
        endforeach;

        $dataSet = [
            'name'              => $name,
            'photo'             => $photo_db,
            'phone'             => $phone,
            'role'              => $role,
            'user_pin'          => $user_pin,
            'company_pin'       => $company->company_pin,
            'created_by'        => $myID,
            'company_id'        => $user->company_id,
            'id_cms_privileges' => $role,
            'created_at'        => Carbon::now()
        ];

        if($dataSet != null):
            DB::table('cms_users')->insert($dataSet);
        endif;
        CRUDBooster::redirect(CRUDBooster::mainpath(), trans("crudbooster.alert_add_data_success"), 'success');
    }

    // =========================== Update User ===========================
    public function update(Request $request){
        $myID           = CRUDBooster::myId();
        $user           = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $company        = DB::table('settings')->select('company_pin')->where('id',$user->company_id)->first();
        $id             = $request->id;
        $name           = $request->name;

        $user_update    = DB::table('cms_users')->select('photo')->where('id',$id)->first();
        if($user_update->photo != null):
            $photo_db   = $user_update->photo;
        else:
        $photo          = $request->file('photo');
            if($photo):
                $photo_db               = "uploads/client_photo/". time() . '.' . $photo->getClientOriginalExtension();
                $photo_upload           = time() . '.' . $photo->getClientOriginalExtension();
                Image::make($photo)->fit(90,90)->save('../storage/app/uploads/client_photo/' . $photo_upload);
            endif;
        endif;
        $phone          = $request->phone;
        $role           = $request->role;
        $user_pin       = $request->user_pin;

        $client_db  = DB::table('cms_users')->select('user_pin')->where('company_id',$user->company_id)->get();
        foreach($client_db as $c):
            if($user_pin == $c->user_pin):
                CRUDBooster::redirect($_SERVER['HTTP_REFERER'],trans('crudbooster.pin_already_in_use'),"info"); 
            endif;
        endforeach;


        $dataSet = [
            'name'              => $name,
            'photo'             => $photo_db,
            'phone'             => $phone,
            'role'              => $role,
            'user_pin'          => $user_pin,
            'company_pin'       => $company->company_pin,
            'id_cms_privileges' => $role,
            'updated_at'        => Carbon::now()
        ];

        if($dataSet != null):
            DB::table('cms_users')->where('id',$id)->update($dataSet);
        endif;
        CRUDBooster::redirect(CRUDBooster::mainpath(), trans("crudbooster.alert_add_data_success"), 'success');
    }



}