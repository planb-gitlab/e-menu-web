<?php namespace App\Http\Controllers;

	use Session;
	use Illuminate\Http\Request;
	use DB;
	use CRUDBooster;

	class AdminOrderSettingController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field 			= "company_name";
			$this->limit 				= "20";
			$this->orderby 				= "id,desc";
			$this->global_privilege 	=	 false;
			$this->button_table_action 	= false;
			$this->button_bulk_action 	= false;
			$this->button_action_style 	= "button_icon";
			$this->button_add 			= false;
			$this->button_edit 			= false;
			$this->button_delete 		= true;
			$this->button_detail 		= true;
			$this->button_show 			= false;
			$this->button_filter 		= false;
			$this->button_import 		= false;
			$this->button_export 		= false;
			$this->table 				= "settings";
			# END CONFIGURATION DO NOT REMOVE THIS LINE
		}

		public function getIndex(){
			$myID           			= CRUDBooster::myId();
        	$user           			= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
			$data 						= [];
			$data['users_no_permit'] 	= DB::table('cms_users')->where('company_id',$user->company_id)->where('discount_privilege','0')->get();
			$data['users_with_permit'] 	= DB::table('cms_users')->where('company_id',$user->company_id)->where('discount_privilege','1')->get();
			$data['setting'] 			= DB::table('settings')->select('print_setting')->where('id',$user->company_id)->first();
			$this->cbView('setting.order_setting.order_setting',$data);
		}

		public function set_permit(Request $request){
			$user_id 	= $request->user_id;
			$permit 	= $request->permit;
			if($user_id && $permit == 1):
				DB::table('cms_users')->where('id',$user_id)->update(['discount_privilege'=>1]);
				return "success";
			else:
				DB::table('cms_users')->where('id',$user_id)->update(['discount_privilege'=>0]);
				return "success";
			endif;
		}

		public function print_setting(Request $request){
			$myID           			= CRUDBooster::myId();
        	$user           			= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
			$print_setting 				= $request->print_setting;
			DB::table('settings')->where('id',$user->company_id)->update(['print_setting' => $print_setting]);
		}

		public function update_printer_name(Request $request){
			$myID           			= CRUDBooster::myId();
        	$user           			= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        	$printer_name 				= $request->printer_name;
        	$printer_type 				= $request->printer_type;
        	DB::table('printer')->where('printer_type',$printer_type)->where('company_id',$user->company_id)->update(['printer_name' => $printer_name]);
		}

		public function find_printer_name(Request $request){
			$myID           			= CRUDBooster::myId();
        	$user           			= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        	$printer_type 				= $request->printer_type;
        	$printer_info 				= DB::table('printer')->where('printer_type',$printer_type)->where('company_id',$user->company_id)->first();

        	return $printer_info->printer_name;


		}


	}