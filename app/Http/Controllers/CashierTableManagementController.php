<?php 
	namespace App\Http\Controllers;

	use App\Events\OrderEvent;
	use App\InvoiceModel;
	use App\Events\MergeEvent;
	use App\Events\UnmergeEvent;
	use Session;
	use Illuminate\Http\Request;
	use DB;
	use CRUDBooster;
	use URL;
	use File;

	class CashierTableManagementController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field 			= "id";
			$this->limit 				= "20";
			$this->orderby 				= "id,desc";
			$this->global_privilege 	= false;
			$this->button_table_action 	= false;
			$this->button_bulk_action 	= false;
			$this->button_action_style 	= "button_icon";
			$this->button_add 			= false;
			$this->button_edit 			= false;
			$this->button_delete		= false;
			$this->button_detail 		= false;
			$this->button_show 			= false;
			$this->button_filter 		= false;
			$this->button_import 		= false;
			$this->button_export 		= false;
			$this->table 				= "table_numbers";
			# END CONFIGURATION DO NOT REMOVE THIS LINE
		}


	    // This function worked for display a dine in route
	    public function getIndex(){

		    $myID 					= CRUDBooster::myId();
	   	 	$user 					= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();

	    	$data 					= [];
	    	$data['myID'] 			= CRUDBooster::myId();
   	 		$data['user'] 			= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
	    	$data['page_title'] 	= trans('crudbooster.Dine In');
	    	$data['store'] 			= $myID;
	    	$data['tables'] 		= DB::table('table_numbers')->where('company_id',$user->company_id)->get();

	    	$this->cbView('order.dine_in',$data);
		}


		public function set_table_status(Request $request){
			$myID       	= CRUDBooster::myId();
	        $user       	= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();	
			$table_code   	= $request->table_code;
			$table_status 	= $request->status;
			$unmerge_all 	= $request->unmerge_all;
			if(!$unmerge_all):
				DB::table('table_numbers')->where('table_code',$table_code)->where('company_id',$user->company_id)->update(['status' => $table_status]);
			else:
				DB::table('table_numbers')->where('table_code',$table_code)->where('company_id',$user->company_id)->update(
					[
						'status' 	=> $table_status,
						'is_merge'	=> '0',

					]
				);
			endif;
		}


		public function get_all_table(){

			$myID       = CRUDBooster::myId();
	        $user       = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();	
		    $url 		= URL::to("/");

	    	$tables				= DB::table('table_numbers')->where('company_id',$user->company_id)->get();
	    	?>

			<div id="normal">
				<div id="containment">
					<div class="row table_management">
					<?php foreach($tables as $key => $table): ?>
						<div class="main_table col-md-3" id="table-code-<?php echo $table->table_code; ?>">
							<div class = "col-md-4 table_info <?php if($table->is_merge == 0): echo 'table_order'; endif; ?>"
									data-id 	= "<?php echo $table->id; ?>" 
									data-val	= "<?php echo $table->id; ?>" 
									data-name	= "<?php echo $table->table_code; ?>" 
									data-status	= "<?php echo $table->status; ?>"
							>
								<ul class="sortable-list <?php if($table->status == 1): echo 'table_free'; else: echo 'table_booked'; endif; ?> <?php if($table->is_merge == 1): echo 'table_is_merge'; endif;?>" data-val="<?php echo $table->table_code; ?>" id="<?php echo $table->table_code; ?>"
								>
									<li class="sortable-item" data-image="<?php echo URL::to('/');?>/<?php echo $table->photo;?>" style="background:linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url(<?php echo URL::to('/');?>/<?php echo $table->photo; ?>) center no-repeat; background-size: cover;">
										<div class="table_number" data-id="<?php echo $table->id; ?>" data-val="<?php echo $table->table_code; ?>" data-name="<?php echo $table->table_code; ?>"><?php echo $table->table_code; ?></div>
									</li>
								</ul>
							</div>

							<div class="col-md-6 table_data">
								<div class="table_name"><?php echo $table->table_code; ?></div>
								<div class="table_capacity"><span>Size : </span><?php echo $table->size; ?></div>	
							</div>

									<a class ="table_detail merge col-md-2 table_function <?php if($table->merge_with != null): echo 'have-merge'; else: echo ''; endif;?> <?php if($table->is_merge == 0): echo 'table_merge'; endif; ?>" 
									title="merge"
									data-id 	="<?php echo $table->id; ?>" 
									data-val	="<?php echo $table->id; ?>" 
									data-name	="<?php echo $table->table_code; ?>" 
									data-status	="<?php echo $table->status; ?>"
									id="current-table-id-<?php echo $table->id; ?>" 
									>
										<img src="<?php echo URL::to('/') ?>/uploads/defualt_image/foodorder.png" />
									</a>

						</div>

					<?php endforeach; ?>
						<div class="clearer">&nbsp;</div>

					</div>
				</div>							
			</div><!-- Normal END -->		    
		<?php 
		}



		public function find_table(Request $request){

			$table_code = $request->table_code;
			$myID       = CRUDBooster::myId();
	        $user       = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();

			$table_data = DB::table('table_numbers')->where('table_code',$table_code)->where('company_id',$user->company_id)->first();
			echo "<span class='table_id'>" . $table_data->id . "</span>";
			echo "<span class='table_status'>" . $table_data->status . "</span>";
		}



		// Get all tables to merge function
		// Function uses for select table to merge
		public function find_merge_table(Request $request){
			
			$table_id = $request->table_id;
			$myID = CRUDBooster::myId();
	        $user = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();

	       	$main_table_data = DB::table('table_numbers')
	       		->where('id',$table_id)
	       		->where('company_id',$user->company_id)
	       		// ->whereStatus(1)
	       		->first();

	        $merge_data = explode("," , $main_table_data->merge_with);

	        $showAvailableTable = DB::table('table_numbers')->where('id','<>', $table_id)->where('company_id',$user->company_id)
	       		->whereStatus(1)
	       		->get();

	        ?>
	        <div class="main_table_merge">
	        	<span>Main Table : </span>
	        	<input type="hidden" name="table_id" class="table_id" value="<?php echo $main_table_data->id; ?>" />
	        	<span class="table_code"><?php echo $main_table_data->table_code;?></span>
	    	</div>
	    	
	    	<div class="side_table">

	    		<select class="side_table_merge form-control" id="side_table_merge_original" name="side_table_merge[]" multiple="multiple" placeholder='please choose your side table to merge'>
	    			<?php foreach($showAvailableTable as $side): ?>
						  	<option value="<?php echo $side->id; ?>" 
						  		<?php 
							  		foreach($merge_data as $merge):
							  			if($side->id == $merge): echo 'selected' ; endif; 
							  		endforeach;
							  	?>
						  	>
						  		<?php 
						  			echo $side->table_code;
						  		?>
						  	</option>
	    				
					<?php endforeach; 	?>
	    		</select>
	    	</div>


	        <?php
		}

		// Function for merge any table

		public function merge_table(Request $request) {
			
			$myId = CRUDBooster::myId();
			$req = $request->all(); 

			$user = DB::table('cms_users')->select('company_id')->find($myId);
			// var_dump($user->company_id); die();
			if (!empty($req['side_table_id'])) {
				
				// Get table id that going to merge to the main table
				$tableMergeId = implode(',', array_filter($req['side_table_id']));

				// Before Update new Table merge
				$mergedTable = DB::table('table_numbers')->find($req['table_id']);
				$mergeId = explode(',', $mergedTable->merge_with);
				
				// Update all data to null related to main table
				// Reset is_merge field to 0 
				if ($mergedTable) {
					foreach ($mergeId as $key => $value) {
						DB::table('table_numbers')->whereId($value)->update(['is_merge' => 0, 'created_by' => $myId]);
					}
				}

				// Re-updated is merge and merge_with field
				$merge = DB::table('table_numbers')->whereId($req['table_id'])->whereIsMerge(0)->update(['merge_with' => $tableMergeId, 'created_by' => $myId, 'company_id' => $user->company_id]);

				// Check if have any merge table inside
				if ($merge && !empty($tableMergeId)) {
					if (count($req['side_table_id']) > 0) {
						foreach ($req['side_table_id'] as $key => $value) {

							$isMerged = DB::table('table_numbers')->find($value); 

							// Check if merge with the main table already merge with sub table
							if (!empty($isMerged->merge_with)) {
								$result = $tableMergeId . ',' . $isMerged->merge_with;

								$lastExplode = explode(',', $result);
								DB::table('table_numbers')->whereId($req['table_id'])->update([
									'merge_with' => $result, 'company_id' => $user->company_id
								]);

								foreach ($lastExplode as $key => $value) {
									DB::table('table_numbers')->whereId($value)->update([
										'merge_with' => NULL, 'is_merge' => 1
									]);
								}
								
							}

							// Check if already merged
							if ($isMerged->is_merge == 1) {
								DB::table('table_numbers')->whereId($req['table_id'])->update(['merge_with' => NULL]);
								return "already_merged";
								// var_dump($isMerged->table_code . 'Is already merged');
							}

							DB::table('table_numbers')->whereId($value)->update(['is_merge' => 0, 'created_by' => $myId]);
							$merge = DB::table('table_numbers')->whereId($value)->update(['is_merge' => 1, 'created_by' => $myId, 'company_id' => $user->company_id]);
							
						}
					}

				}

				// Response data json to realtime update
		        $data = [
		        	'oldTableCode' => getTableCode($mergeId),
		            'tableCode' => getTableCode($req['side_table_id']),
		            'currentTable' => $req['table_id'],
		            'mergeWith' => true
		        ];
		        event(new MergeEvent($data));
				return "merge_and_order";
			}

			// Clear table merge code
			else {

				$findTableId = DB::table('table_numbers')->find($req['table_id']);
				// Check if it have any merged data
				if (!empty($findTableId->merge_with)) {
					$explodeId = explode(',', $findTableId->merge_with);

					foreach ($explodeId as $key => $value) {
						$updateIsMerge = DB::table('table_numbers')->whereId($value)
							->update(['is_merge' => 0, 'created_by' => $myId,  'company_id' => $user->company_id]);
					}
					DB::table('table_numbers')->whereId($req['table_id'])
						->update(['merge_with' => NULL,  'company_id' => $user->company_id]);
				}

				// Response data json to realtime update
		        $data = [
		            'oldTableCode' => getTableCode($explodeId),
		            'currentTable' => $req['table_id'],
		            'mergeWith' => false
		        ];
		        event(new UnmergeEvent($data));
				return "reload";

			}
			
		}


		public function merge_table_old(Request $request){

			$myID       	= CRUDBooster::myId();
	        $user       	= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
			$table_id 		= $request->table_id;
			$side_table_id 	= $request->side_table_id;

			if($side_table_id):
				foreach($side_table_id as $side):
					if($table_id == $side): return; endif;
				endforeach;

				$old_merge_data_all 	= DB::table('table_numbers')->where('id', $table_id)->where('company_id',$user->company_id)->first();
				DB::table('table_numbers')->where('id', $table_id)->where('company_id',$user->company_id)->update(['merge_with' => null]);

				foreach ($side_table_id as $side):
					
					
					$merge_data = DB::table('table_numbers')->where('id', $table_id)->where('company_id',$user->company_id)->first();
			        
			        	if($merge_data->merge_with != null):
				        	$full_merge 		= $merge_data->merge_with . ',' . $side;
				        	$merge 				= DB::table('table_numbers')
				        						->where('id', $table_id)->where('company_id',$user->company_id)->update(array('merge_with' => $full_merge));
				        	$merge_status 		= DB::table('table_numbers')
				        						->where('id', $side)->where('company_id',$user->company_id)->update(array('is_merge' => 1));

				        						
				        	
			        	else:
			        		$merge 				= DB::table('table_numbers')
			        							->where('id', $table_id)->where('company_id',$user->company_id)->update(array('merge_with' => $side,'status' => 1));
			        		$merge_status 		= DB::table('table_numbers')
			        							->where('id', $side)->where('company_id',$user->company_id)->update(array('is_merge' => 1));

			        	endif;


			    endforeach;

			    $new_merge_data_all 	= DB::table('table_numbers')->where('id', $table_id)->where('company_id',$user->company_id)->first();
				$new_merge_data_explode = explode("," , $new_merge_data_all->merge_with);
				$old_merge_data_explode = explode("," , $old_merge_data_all->merge_with);

				foreach ($old_merge_data_explode as $old_merge):
					foreach($new_merge_data_explode as $new_merge):
						if($old_merge != $new_merge):
					DB::table('table_numbers')->where('id', $old_merge)->where('company_id',$user->company_id)->update(array('is_merge' => null));
						endif;
					endforeach;
				endforeach;

				return "merge_and_order";


			else:
				$merge_data_all 		= DB::table('table_numbers')->where('id', $table_id)->where('company_id',$user->company_id)->first();
				$merge_data_explode = explode("," , $merge_data_all->merge_with);
				
				foreach ($merge_data_explode as $merge):
					DB::table('table_numbers')->where('id', $merge)->where('company_id',$user->company_id)->update(array('is_merge' => null));
				endforeach;

				DB::table('table_numbers')->where('id', $table_id)->where('company_id',$user->company_id)->update(array(
					'merge_with' => null,
					)
				);

				return "reload";
			endif;


		}

	

}