<?php namespace App\Http\Controllers;

	use Session;
	use Illuminate\Http\Request;
	use DB;
	use CRUDBooster;

	class AdminSaleByPromotionPackController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {
			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field 			= "id";
			$this->limit 				= "20";
			$this->orderby 				= "id,desc";
			$this->global_privilege 	= false;
			$this->button_table_action 	= false;
			$this->button_bulk_action 	= false;
			$this->button_action_style 	= "button_icon";
			$this->button_add 			= false;
			$this->button_edit 			= true;
			$this->button_delete 		= true;
			$this->button_detail 		= true;
			$this->button_show 			= false;
			$this->button_filter 		= true;
			$this->button_import 		= false;
			$this->button_export 		= false;
			$this->table 				= "orders";
			# END CONFIGURATION DO NOT REMOVE THIS LINE        
	    }

	    public function getIndex()
	    {
	    	$myID 								= CRUDBooster::myId();
	    	$user								= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
	    	$data 								= [];
	    	$data['page_title'] 				= trans('report.Sale by Promotion Pack Report');
	    	$data['get_promotion_name'] 		= DB::table('promotion_packs')->select('id','promotion_name')->where('company_id',$user->company_id)->where('status',1)->get();
	    	$data['get_promotion_type_info']	= DB::table('promotion_type')->select('id','name')->where('status',1)->get();
	    	$data['invoices']					= DB::table('cms_users')
			    								->join('invoices','invoices.created_by','=','cms_users.id')
			    								->select('cms_users.name',DB::raw('sum(grand_total) as grand_total'),DB::raw('count("invoice_number") as table_sold'),'invoices.currency')
			    								->where('cms_users.company_id','=',$user->company_id)
												->groupBy('cms_users.id')->groupBy('cms_users.name')->groupBy('invoices.currency')->paginate(20);
	    	$this->cbView('report.sale_by_promotion_pack',$data);
	    }


	    

	}