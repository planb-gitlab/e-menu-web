<?php 
namespace App\Http\Controllers;

	use Session;
	use Illuminate\Http\Request;
	use App\InvoiceModel;
	use DB;
	use File;
	use CRUDBooster;
	use Carbon\Carbon;
	use Response;
	use Dompdf\Dompdf;

	class AdminSaleByInvoiceController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field 			= "id";
			$this->limit 				= "20";
			$this->orderby 				= "id,desc";
			$this->global_privilege 	= false;
			$this->button_table_action 	= false;
			$this->button_bulk_action 	= false;
			$this->button_action_style 	= "button_icon";
			$this->button_add 			= false;
			$this->button_edit 			= true;
			$this->button_delete 		= true;
			$this->button_detail 		= true;
			$this->button_show 			= false;
			$this->button_filter 		= false;
			$this->button_import 		= false;
			$this->button_export 		= false;
			$this->table 				= "Invoices";
			# END CONFIGURATION DO NOT REMOVE THIS LINE	        
	    }

	    public function getIndex()
	    {
	    	$myID 					= CRUDBooster::myId();
	    	$companyId				= getCompanyId($myID);	
	    	$data 					= [];
	    	$data['page_title']    	= trans('report.Sale by Invoice Report');
	    	$data['invoices']   	= DB::table('invoices')
	    								->leftjoin('settings','settings.id', 'invoices.company_id')
	    								->where('invoices.company_id',$companyId)
	    								->where('void', 0)
	    								->whereHasPaid(1)
	    								->orderBy('invoice_id','DESC')
	    								->paginate(20);

	    	$data['total_invoice']  = count($data['invoices']); 
	    	
	    	$this->cbView('report.sale_by_invoice',$data);
	    	
	    }

	    public function get_sort(Request $request)
	    {
	    	$myID 					= CRUDBooster::myId();
	    	$user					= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();

	    	$data 					= [];
	    	$data['page_title']    	= trans('report.Sale by Invoice Report');
	    	

	    	$date 					= $request->by;
	    	$custom_date			= $request->daterange;
	    	$invoice_id 			= $request->invoice_id;

	    	if($date):
	    		if ($request->by == "weekly"):
				    $startDate 	= date("Y-m-d", strtotime('monday this week'))." 00:00:00";
					$endDate 	= date("Y-m-d", strtotime('sunday this week'))." 23:59:59";
				elseif ($request->by == "monthly"): 
					$startDate 	= date('Y-m-01')." 00:00:00";
					$date 		= date('Y-m-d');
					$endDate 	= date("Y-m-t", strtotime($date))." 23:59:59";
				elseif ($request->by == "yearly"):
					$startDate 	= date('Y-01-01')." 00:00:00";
					$date 		= date('Y-m-d');
					$endDate 	= date("Y-12-t", strtotime($date))." 23:59:59";
				elseif ($request->by == "daily" ):
					$day 		= new \DateTime();
					$startDate 	= $day->format('Y-m-d')." 00:00:00";
					$endDate 	= $day->format('Y-m-d')." 23:59:59";
				else:
					$startDate 	= null;
					$endDate 	= null;
				endif;

				$invoices = DB::table('invoices')->leftjoin('settings','settings.id','=','invoices.company_id')
							->where('invoice_date','>=',$startDate)
							->where('invoice_date','<',$endDate)
							->where('void','=','0')
							->where('invoices.status','=',1)
							->where('invoices.company_id',$user->company_id)
							->OrderBy('invoice_id','DESC')
							->paginate(20)->appends('by',$request->by);


	    	elseif($custom_date):
    			$date 		= explode(' - ',$custom_date);
    			$startDate	= $date[0];
    			$endDate 	= $date[1];

    			$invoices = DB::table('invoices')->leftjoin('settings','settings.id','=','invoices.company_id')
						->where('invoice_date','>=',$startDate)
						->where('invoice_date','<=',$endDate)
						->where('void','=','0')
						->where('has_paid', NULL)
						->where('invoices.status','=',1)
						->where('invoices.company_id',$user->company_id)
						->OrderBy('invoice_id','DESC')
						->paginate(20)->appends('daterange',$request->daterange);
	    	elseif($invoice_id):
	    		$invoices = DB::table('invoices')->leftjoin('settings','settings.id','=','invoices.company_id')
						->where('void','=','0')
						->where('invoices.status','=',1)
						->where('invoices.company_id',$user->company_id)
						->where('invoice_number','=',$invoice_id)
						->OrderBy('invoice_id','DESC')
						->paginate(20)->appends('invoice',$request->invoice_id);
	    	else:
	    		$invoices = DB::table('invoices')->leftjoin('settings','settings.id','=','invoices.company_id')
							->where('void','=','0')
							->where('invoices.status','=',1)
							->where('invoices.company_id',$user->company_id)
							->OrderBy('invoice_id','DESC')
							->paginate(20);
	    	endif;

	    	$data['invoices'] = $invoices;
	    	$this->cbView('report.sale_by_invoice',$data);


	    }

	    public function view_invoice(Request $request){
	    	$myID 					= CRUDBooster::myId();
	    	$user					= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
	    	$invoice_no 			= $request->invoice_no;
	    	$invoice 				= DB::table('invoices')
	    							->leftjoin('cms_users','cms_users.id','=','invoices.created_by')
	    							->where('invoices.company_id',$user->company_id)->where('invoices.invoice_id',$invoice_no)->first(); 
	    	$orders 				= DB::table('invoice_orders')
	    							->leftjoin('menus','menus.id','=','invoice_orders.item_id')
	    							->leftjoin('menu_materials','menu_materials.id','=','invoice_orders.addon_id')
	    							->select('invoice_orders.*','menus.*','invoice_orders.price as price','menu_materials.name as material_name')
	    							->where('invoice_id',$invoice_no)->get();?>

	    	<table class="invoice_info">
	    		<tr>
					<td>Invoice No</td>
					<td><?= $invoice->invoice_number; ?></td>
				</tr>
				<tr>
					<td>Date</td>
					<td><?= $invoice->created_at; ?></td>
				</tr>
				<tr>
					<td>Seller</td>
					<td><?= $invoice->name; ?></td>
				</tr>
	    	</table>
	    	<table class="invoice_order">
	    		<tr class="header">
	    			<td class="col-md-1">ID</td>
	    			<td class="col-md-5">Name</td>	
	    			<td>Qty</td>	
	    			<td>Price</td>	
	    			<td>Total</td>	
	    		</tr>
	    		<?php foreach($orders as $key => $order): ?>
	    		<tr>
	    			<?php if($order->addon_id == 0): ?>
						<td class="col-md-1"><?= ++$key; ?></td>
						<td class="col-md-5"><?= $order->name; ?></td>
					<?php else: ?>
						<td class="col-md-1">></td>
						<td class="col-md-5"><?= $order->material_name; ?></td>
					<?php endif; ?>
					<td><?= $order->qty; ?></td>
					<td><?= $invoice->currency . number_format($order->price, 2); ?></td>
					<td><?= $invoice->currency . number_format($order->price* $order->qty, 2); ?></td>
	    		</tr>
	    		<?php endforeach; ?>
	    	</table>
	    	<table class="invoice_transaction" style="text-align: right;">
	    		<tr>
	    			<td></td>	
	    			<td>Total Amount: </td>	
	    			<td><?= $invoice->currency . number_format($invoice->grand_total, 2);?></td>		
	    		</tr>

	    		<tr>
	    			<td></td>	
	    			<td>Paid Amount : </td>	
	    			<td><?= $invoice->currency . number_format($invoice->paid_total, 2);?></td>		
	    		</tr>

	    		<tr>
	    			<td></td>	
	    			<td>Return Amount: </td>	
	    			<td><?= $invoice->currency . number_format($invoice->return_amount, 2);?></td>
	    		</tr>
	    	</table>

	    	<button type="button" class="btn btn-default print" data-val="<?= $invoice->invoice_id; ?>" data-dismiss="modal"  ><?= trans('report.Print'); ?></button>
			
	    	<?php
	    }


	public function printInvoice() {

		$companyId = getCompanyId(CRUDBooster::myId());	
		$invoiceId = request()->invoiceId;
		$data = [];
		$data['outlet'] = DB::table('settings')->where('settings.id',$companyId)
            ->leftjoin('currencies','currencies.id','=','settings.company_currency')
            ->first();
            
		$data['invoice'] = DB::table('invoices')
            ->leftjoin('cms_users','cms_users.id','invoices.created_by')
            ->whereInvoiceId($invoiceId)
            ->first();

		$singleInvoice = DB::table('invoices')->whereInvoiceId($invoiceId)->first();
		$orderData = DB::table('invoice_orders')
			
			->select('invoices.*', 'invoice_orders.*', 'menus.*', 'invoice_orders.price as item_price')
			->join('invoices', 'invoices.invoice_id', 'invoice_orders.invoice_id')
			// ->leftJoin('menu_uom_price as uom_price', 'uom_price.id', 'invoice_orders.addon_id')
			->join('menus', 'menus.id', 'invoice_orders.item_id')
			->whereHasPaid(1)
			->whereVoid(0)
			->whereAddonId(0)
			->where('invoices.invoice_id', $invoiceId)
			->get();
			
			$mainItem = [];
		
			foreach ($orderData as $key => $value) {

				// if ($value->addon_id != 0) {
					$addOn = DB::table('invoice_orders as order')
						->select('menu.name as addOnName', 'order.price as addOnPrice', 'order.qty as addOnQty')
						->join('menu_materials as menu', 'menu.id', 'order.addon_id')
						->where('addon_id', '<>', 0)
						->whereItemId($value->item_id)
						->get()->toArray();
				// }

				$mainItem[] = [
					'itemName' => $value->name,
					'orderType' => $value->order_type,
					'invoiceNumber' => $value->invoice_number,
					'tableCode' => showTableCode($value->table_id),
					'itemQty' => $value->qty,
					'price' => $value->item_price,
					'addOn' => $addOn
				];
			}
			
			$data['now']            = Carbon::now();
			$data['items']			= $mainItem;
			$data['order_type']		= $singleInvoice->order_type;
			$data['table_id']		= $singleInvoice->table_id;
			$data['discount_id']	= $data['invoice']->discount_type;			
			$view                   = view('ticket.print_sale_invoice', $data)->render();
			$dompdf                 = new Dompdf(array('enable_remote' => true));
			$dompdf->loadHtml($view);
			$dompdf->setPaper('A4', 'portrait');
			$dompdf->render();
			$output 				= $dompdf->output();
			File::put($invoiceId.'-ticket.pdf', $output);

			$printer_info      = DB::table('printer')->select('printer_name')->where('company_id',$company_id)->where('printer_type','invoice')->first();

	        //generate vbscript
			$print_vbs = '
			Set oShell = CreateObject ("Wscript.Shell") 
			Dim strArgs
			strArgs = "'.$invoiceId.'.bat"
			oShell.Run strArgs, 0, false';

			$print_batch 		= '"PDFtoPrinter.exe" "' . $invoiceId . '-ticket.pdf" "' . $printer_info->printer_name .'"';

			File::put($invoiceId.'-ticket.vbs', $print_vbs);
			File::put($invoiceId.'.bat', $print_batch);
			
			$print_ticket  		= $invoiceId."-ticket.vbs";
			echo shell_exec($print_ticket);

			sleep(5);
			UnlinkFile($invoiceId . '-ticket.vbs');
			UnlinkFile($invoiceId . '.bat');
			// UnlinkFile($invoiceId . '-ticket.pdf');

	}

	private function getAddonItem($parentId, $key, $value) {
		return $data = [
			$key => $value,
		];
	}

}