<?php

namespace App\Http\Controllers;

use crocodicstudio\crudbooster\helpers\CRUDBooster;
use Illuminate\Http\Request;
use DB;

class StatusController extends Controller
{
    public function getUpdateStatus($slug, $id)
    {

        if($slug == "categories"){
        //category
            $category           = DB::table($slug)->where('id', $id);
            $category_data      = $category->first();
            $category_status    = $category_data->status;
            $subcategory        = DB::table('subcategories')->where('category_id',$id)->get();
            $menu               = DB::table('menus')->where('category_id',$id)->get();
           
            if(count($subcategory) <= 0 && count($menu) <= 0):
                if($category_status == 0): 
                    $status = 1;
                else: 
                    $status = 0;
                endif;
                $category->update(['status' => $status]);
                CRUDBooster::redirect($_SERVER['HTTP_REFERER'], "The data has been updated !", "success");
            else:
                if($category_status == 0):
                    /*status disable*/
                    CRUDBooster::redirect($_SERVER['HTTP_REFERER'], trans("crudbooster.data_is_in_use"), "warning");
                else:
                   /* status enable*/
                    CRUDBooster::redirect($_SERVER['HTTP_REFERER'], trans("crudbooster.data_is_in_use"), "warning");
                endif;
            endif;


        } else if($slug == "subcategories"){
            //subcategory
            $subcategories      =  DB::table('subcategories')->where('id',$id);
            $subcategories_data = $subcategories->first();
            $subcategory_status =  $subcategories_data->status;
            $menu_with_sub      = DB::table('menus')->where('subcategory_id',$id)->get();
            
            if(count($menu_with_sub) <= 0):
                if($subcategory_status == 0):
                    $status = 1;
                else:
                    $status = 0;
                endif;
                DB::table('subcategories')->where('id',$id)->update(['status' => $status]);
                CRUDBooster::redirect($_SERVER['HTTP_REFERER'], "The data has been updated !", "success");
            else:
                CRUDBooster::redirect($_SERVER['HTTP_REFERER'], trans("crudbooster.data_is_in_use"), "warning");
            endif;
        }
/*
        else if($slug == "menus"){
            $menuAll     = DB::table('menus')->where('id', $id);
            $menu_data   = $menuAll->first();
            $menu_status = $menu_data->status;
            $material    = DB::table('menu_detail')->where('menu_id', $id)->get();
            if(count($material) <= 0):
                if($menu_status == 1):
                    $status = 0;
                else:
                    $status = 1;
                endif;
                $menuAll->update(['status' => $status]);
                CRUDBooster::redirect($_SERVER['HTTP_REFERER'], "The data has been updated !", "success");
            else:
               CRUDBooster::redirect($_SERVER['HTTP_REFERER'], trans("crudbooster.data_is_in_use"), "warning");
            endif; 
        }
*/
        else if($slug == "addon"){
            $menu_mat           = DB::table('menu_materials')->where('id', $id);
            $menu_mat_data      = $menu_mat->first();
            $menu_mat_status    = $menu_mat_data->status;
            $menu_detail_data   = DB::table('menu_detail')->where('material_id', $id)->get();
            if(count($menu_detail_data) <= 0):
                if($menu_mat_status == 1):
                    $status = 0;
                else:
                    $status = 1;
                endif;
                $menu_mat->update(['status' => $status]);
                CRUDBooster::redirect($_SERVER['HTTP_REFERER'], "The data has been updated !", "success");
            else:
                CRUDBooster::redirect($_SERVER['HTTP_REFERER'], trans("crudbooster.data_is_in_use"), "warning");
            endif;
        }
       


        try {
            if ($slug == "setting") {
                $slug = "settings";
            }
            $db = DB::table($slug)->where('id', $id);
            $dbStatus = $db->first();
            $status = $dbStatus->status;
            if ($status == 0) {
                $status = 1;
            } else {
                $status = 0;
            }
            $db->update(['status' => $status]);
            return redirect()->back()->withInput()->withErrors(['message' => 'The data has been updated !']);

        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
    }
}
