<?php namespace App\Http\Controllers;

	use Session;
	use App\Http\Requests;
	use Illuminate\Http\Request;
	use DB;
	use CRUDBooster;
	use Carbon\Carbon;
	use Response;
	use App\sold_amount;

	class AdminReportDashboardController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field 			= "id";
			$this->limit 				= "20";
			$this->orderby 				= "invoice_id,desc";
			$this->global_privilege 	= false;
			$this->button_table_action 	= false;
			$this->button_bulk_action 	= false;
			$this->button_action_style 	= "button_icon";
			$this->button_add 			= false;
			$this->button_edit 			= false;
			$this->button_delete 		= false;
			$this->button_detail 		= false;
			$this->button_show 			= false;
			$this->button_filter 		= false;
			$this->button_import 		= false;
			$this->button_export 		= false;
			$this->table 				= "invoices";
			# END CONFIGURATION DO NOT REMOVE THIS LINE  
	    }


	    public function getIndex(){
	    	$myID           = CRUDBooster::myId();
        	$user           = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();

        	$data 						= [];
        	$data['company_currency']	= DB::table('settings')->where('settings.id',$user->company_id)
    									->leftjoin('currencies','currencies.id','=','settings.company_currency')
    									->select('company_currency','currency_symbol')->first();
        	$data['sold_amout']			= DB::table('invoices')->select('grand_total')->where('company_id',$user->company_id)->get();
        	$data['table_sold']			= DB::table('invoices')->select('table_id')->where('table_id','<>',null)->where('company_id',$user->company_id)->get();
        	$data['customer_count']		= DB::table('invoices')->where('paid_total','<>',null)->where('company_id',$user->company_id)->get();
        	$data['discount']			= DB::table('invoices')->where('discount_total_dollar','<>',null)->where('company_id',$user->company_id)->get();
        	$data['revenue']			= DB::table('invoices')->where('company_id',$user->company_id)->get();
        	$data['top_sale_cashier']	= DB::table('invoices')->orderBy('created_by','desc')->where('company_id',$user->company_id)->get();

        	$this->cbView('report.report_dashboard',$data);
	    }


	    public function insert_report(Request $request){
	    	$filter 			= $request->filter;			// Filter Type
        	$date 				= $request->input_date;		// Custom Date in the input box
        	$this->insert_report_data($filter,$date);
	    }


	    public function get_sold_amount(Request $request){
	    	$filter 		= $request->filter;			// Filter Type
        	$date 			= $request->input_date;		// Custom Date in the input box
        	$sold_amount 	= $this->get_report_data($filter,$date,"sold_amount");
        	return $sold_amount;
	    }


	    public function get_table_sold(Request $request){
	    	$filter 		= $request->filter;			// Filter Type
        	$date 			= $request->input_date;		// Custom Date in the input box
        	$table_sold 	= $this->get_report_data($filter,$date,"table_sold");
        	return $table_sold;
	    }

	    public function get_customer_count(Request $request){
	    	$filter 		= $request->filter;			// Filter Type
        	$date 			= $request->input_date;		// Custom Date in the input box
        	$customer_count = $this->get_report_data($filter,$date,"customer_count");
        	return $customer_count;
	    }

	    public function get_discount(Request $request){
	    	$filter 		= $request->filter;			// Filter Type
        	$date 			= $request->input_date;		// Custom Date in the input box
        	$discount 		= $this->get_report_data($filter,$date,"discount");
        	return $discount;
	    }

	    public function get_subtotal(Request $request){
	    	$filter 		= $request->filter;			// Filter Type
        	$date 			= $request->input_date;		// Custom Date in the input box
        	$sub_total 		= $this->get_report_data($filter,$date,"sub_total");
        	return $sub_total;
	    }

	    public function get_revenue(Request $request){
	    	$filter 		= $request->filter;
	    	$date 			= $request->input_date;
	    	$this->get_revenue_list($filter,$date);
	    } 

	    public function get_top_sale_cashier(Request $request){
	    	$filter 		= $request->filter;
	    	$date 			= $request->input_date;
	    	$this->get_top_seller_list($filter,$date);
	    	
	    } 


	    /* ====================== Start of Function =================== */


	    public function insert_report_data($filter, $date){
	    	$myID           	= CRUDBooster::myId();
        	$user           	= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        	$company_currency	= DB::table('settings')->where('settings.id',$user->company_id)
    									->leftjoin('currencies','currencies.id','=','settings.company_currency')
    									->select('company_currency','currency_symbol')->first();


        	if($filter == 'daily'):
        		$invoice_data 	= DB::table('invoices')->where('invoice_date',$date)->where('company_id',$user->company_id)->get();
        		$report_data	= DB::table('reports')->select('report_date')->where('report_date',$date)->where('company_id',$user->company_id)->get();
        		$invoices 		= $invoice_data->pluck('invoice_date')->toArray();
        		$reports 		= $report_data->pluck('report_date')->toArray();


        		if(!in_array($date, $reports)):
        				//Need to insert Data
	        			if(count($invoice_data)>= 1):
	        				foreach($invoice_data as $invoice):
	        					$sold_amount 	+= $invoice->grand_total;
	        					$table_sold  	= count($invoice_data);
	        					$customer_count = count($invoice_data);
	        					$discount 		+= $invoice->discount_total_dollar;
	        					$sub_total 		= $sold_amount - $discount;   					


	        				endforeach;
	        				$dataSet = [
									        'report_date'  		=>  $date,
									        'company_id'   		=>  $user->company_id,
									        'currency'			=>  $company_currency->currency_symbol,
									        'sold_amount'      	=>  $sold_amount,
									        'table_sold' 		=>  $table_sold,
									        'customer_count' 	=>  $customer_count,
									        'discount'			=> 	$discount,
									        'sub_total'			=>	$sub_total,
							    			]; 
	        				DB::table('reports')->insert($dataSet);
	        				
	        			else:	
	        					$dataSet = [
									        'report_date'  		=>  $date,
									        'company_id'   		=>  $user->company_id,
									        'currency'			=>  $company_currency->currency_symbol,
									        'sold_amount'      	=>  0,
									        'table_sold' 		=>  0,
									        'customer_count' 	=>  0,
									        'discount'			=> 	0,
									        'sub_total'			=>	0,
							    			]; 

	        					DB::table('reports')->insert($dataSet);
	        			endif;
        			
        		else:
	        			//Exist data already, Just need to sum other invoice
	        			if(count($invoice_data)>= 1):
		        			foreach($invoice_data as $invoice):
		        					$sold_amount 	+= $invoice->grand_total;
		        					$table_sold  	= count($invoice_data);
		        					$customer_count = count($invoice_data);
		        					$discount 		+= $invoice->discount_total_dollar;
		        					$sub_total 		= $sold_amount - $discount;

		        			endforeach;
		        				$dataSet = [
										        'report_date'  		=>  $date,
										        'company_id'   		=>  $user->company_id,
										        'currency'			=>  $company_currency->currency_symbol,
										        'sold_amount'      	=>  $sold_amount,
										        'table_sold' 		=>  $table_sold,
										        'customer_count' 	=>  $customer_count,
										        'discount'			=> 	$discount,
										        'sub_total'			=>	$sub_total,
								    		]; 
								    	
		        				DB::table('reports')->where('report_date',$date)->where('company_id',$user->company_id)->update($dataSet);
		        		endif;
        		endif;



        	elseif ($filter == 'weekly'):	

        	elseif ($filter == 'monthly'):

        	elseif ($filter == 'yearly'):

        	endif;
	    }//End of insert_report_data



	    public function get_report_data($filter, $date, $get_data){
	    	$myID           = CRUDBooster::myId();
        	$user           = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();

        	if($filter == 'daily'):

				$carbon_date 	= Carbon::parse($date);
				$report_data	= DB::SELECT("SELECT * 
											  FROM reports
											  WHERE company_id = '$user->company_id'
											  AND (report_date = '$date' OR report_date < '$carbon_date')
											  ORDER BY report_date DESC
											  LIMIT 5
											");


				// DB::table('reports')
				// 					->where('report_date','=',$date)
				// 					->orWhere('report_date','<',$carbon_date)
				// 					->where('company_id',$user->company_id)
				// 					->Orderby('report_date','DESC')->limit(5)->get();
				

	    	elseif ($filter == 'weekly'):
	    		$weekly_date	= explode(" - ",$date);				
				$startDate 		= Carbon::parse($weekly_date[0]." 00:00:00");
				$endDate 		= Carbon::parse($weekly_date[1]." 23:59:59");
				$week 			= date("W",strtotime($startDate));
				$report_data	= DB::select("SELECT sum($get_data) as $get_data ,report_normal_data.report_date, report_normal_data.currency FROM reports
												LEFT JOIN 
												(
													select id,week(report_date) as report_date, currency
													FROM reports
												)report_normal_data
												ON report_normal_data.id = reports.id
												WHERE company_id = $user->company_id
												AND report_normal_data.report_date < $week
												group by(report_date)
												Order By report_date DESC
												limit 5
												

											");
	    		

	    	elseif ($filter == 'monthly'):

	    		$monthly_date	= explode(" - ",$date);				
				$startDate 		= Carbon::parse($monthly_date[0]." 00:00:00");
				$endDate 		= Carbon::parse($monthly_date[1]." 23:59:59");

				$month 			= date("m",strtotime($startDate));
				$report_data	= DB::select("SELECT sum($get_data) as $get_data ,report_normal_data.report_date, report_normal_data.currency FROM reports
												LEFT JOIN 
												(
													select id,month(report_date) as report_date, currency
													FROM reports
												)report_normal_data
												ON report_normal_data.id = reports.id
												WHERE company_id = $user->company_id
												AND report_normal_data.report_date <= $month
												group by(report_date)
												Order By report_date DESC
												

											");



	    	elseif ($filter == 'yearly'):
	    		$report_data	= DB::select("SELECT sum($get_data) as $get_data ,report_normal_data.report_date, report_normal_data.currency FROM reports
												LEFT JOIN 
												(
													select id,year(report_date) as report_date, currency
													FROM reports
												)report_normal_data
												ON report_normal_data.id = reports.id
												WHERE company_id = $user->company_id
												AND report_normal_data.report_date <= $date
												group by(report_date)
												Order By report_date DESC
												limit 5
											");
	    	endif;

	    	
	    	$output[] = [];
	    	if($report_data):
	    		
					foreach($report_data as $report):
						$output[] = [
							"currency" 			=> $report->currency,
							$get_data 			=> $report->$get_data,
							"report_date"		=> $report->report_date
						];
					endforeach;

					return Response::json($output,200);
			endif;
	    }//End of get_report_data


	    public function get_revenue_list($filter,$date){
	    	$myID           = CRUDBooster::myId();
        	$user           = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();

        	if($filter == 'daily'):
				$report_data	= DB::select("SELECT sum(grand_total) as sold_amount,
												  sum(discount_total_dollar) as discount,
												  sum(tax_dollar) as tax,
												  count(invoices_2.invoice_id) as table_sold,
												  count(invoices_2.invoice_id) as customer_count,
												  invoices_2.invoice_date,
												  invoices_2.currency
												  FROM invoices
												  LEFT JOIN
												  (
												  	SELECT invoice_id,currency,invoice_date as invoice_date
												  	FROM invoices
												  )invoices_2
												  ON invoices_2.invoice_id = invoices.invoice_id
												  WHERE company_id = $user->company_id
												  AND invoices_2.invoice_date = '$date'
												  GROUP BY (invoice_date)"
										);


        	elseif($filter == 'weekly'):
        		$weekly_date	= explode(" - ",$date);				
				$startDate 		= Carbon::parse($weekly_date[0]." 00:00:00");
				$endDate 		= Carbon::parse($weekly_date[1]." 23:59:59");
				$weekStart 		= date("W",strtotime($startDate)) - 1;
        		$report_data	= DB::select("SELECT sum(grand_total) as sold_amount,
												  sum(discount_total_dollar) as discount,
												  sum(tax_dollar) as tax,
												  count(invoices_2.invoice_id) as table_sold,
												  count(invoices_2.invoice_id) as customer_count,
												  invoices_2.invoice_date,
												  invoices_2.currency
												  FROM invoices
												  LEFT JOIN
												  (
												  	SELECT invoice_id,currency,week(invoice_date) as invoice_date
												  	FROM invoices
												  )invoices_2
												  ON invoices_2.invoice_id = invoices.invoice_id
												  WHERE company_id = $user->company_id
												  AND invoices_2.invoice_date = '$weekStart'
												  GROUP BY (invoice_date)"
										);



        	elseif($filter == 'monthly'):
        		$monthly_date	= explode(" - ",$date);				
				$startDate 		= Carbon::parse($monthly_date[0]." 00:00:00");
				$endDate 		= Carbon::parse($monthly_date[1]." 23:59:59");
				$month 			= date("m",strtotime($startDate));
        		$report_data	= DB::select("SELECT sum(grand_total) as sold_amount,
												  sum(discount_total_dollar) as discount,
												  sum(tax_dollar) as tax,
												  count(invoices_2.invoice_id) as table_sold,
												  count(invoices_2.invoice_id) as customer_count,
												  invoices_2.invoice_date,
												  invoices_2.currency
												  FROM invoices
												  LEFT JOIN
												  (
												  	SELECT invoice_id,currency,month(invoice_date) as invoice_date
												  	FROM invoices
												  )invoices_2
												  ON invoices_2.invoice_id = invoices.invoice_id
												  WHERE company_id = $user->company_id
												  AND invoices_2.invoice_date = '$month'
												  GROUP BY (invoice_date)"
										);

        	elseif($filter == 'yearly'):
        		$report_data	= DB::select("SELECT sum(grand_total) as sold_amount,
												  sum(discount_total_dollar) as discount,
												  sum(tax_dollar) as tax,
												  count(invoices_2.invoice_id) as table_sold,
												  count(invoices_2.invoice_id) as customer_count,
												  invoices_2.invoice_date,
												  invoices_2.currency
												  FROM invoices
												  LEFT JOIN
												  (
												  	SELECT invoice_id,currency,year(invoice_date) as invoice_date
												  	FROM invoices
												  )invoices_2
												  ON invoices_2.invoice_id = invoices.invoice_id
												  WHERE company_id = $user->company_id
												  AND invoices_2.invoice_date = '$date'
												  GROUP BY (invoice_date)"
										);

        	endif;

        	$output[] = [];
	    	if($report_data):
	    			
					foreach($report_data as $report): ?>
					<tr>
						<td><?php echo $report->sold_amount . $report->currency;?></td>
						<td><?php echo $report->table_sold;?></td>
						<td><?php echo $report->customer_count;?></td>
						<?php if ($report->discount != null):?>
							<td><?php echo $report->discount . $report->currency;;?></td>
						<?php else: ?>
							<td><?php echo "0.00"; ?></td>
						<?php endif;?>
						<?php $sub_total = $report->sold_amount - $report->discount; ?>
						<td><?php echo $sub_total . $report->currency;?></td>
						

					</tr>


					<?php endforeach;
			else: ?>

			<tr>
				<td colspan="7" class="text-center"><h4>No Data!</h4></td>
			</tr>

			<?php endif;


	    }//End of get_revenue


	    public function get_top_seller_list($filter,$date){
	    	$myID           = CRUDBooster::myId();
        	$user           = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        	

        	if($filter == 'daily'):
        		$month_name 	= date("m",strtotime($date));
				$report_data	= DB::select("SELECT sum(grand_total) as sold_amount,
												  count(invoices_2.invoice_id) as table_sold,
												  count(invoices_2.invoice_id) as customer_count,
												  invoices_2.currency,
												  cms_users.cashier_name
												  FROM invoices
												  LEFT JOIN
												  (
												  	SELECT invoice_id,created_by,currency,invoice_date as invoice_date
												  	FROM invoices
												  )invoices_2
												  ON invoices_2.invoice_id = invoices.invoice_id
												  LEFT JOIN 
												  (
												  	SELECT id,name as cashier_name
												  	FROM cms_users
												  )cms_users
												  ON cms_users.id = invoices_2.created_by
												  WHERE invoices.company_id = $user->company_id
												  AND invoices_2.invoice_date = '$date'
												  GROUP BY (invoices_2.invoice_date)"
										);


        	elseif($filter == 'weekly'):
        		$weekly_date	= explode(" - ",$date);				
				$startDate 		= Carbon::parse($weekly_date[0]." 00:00:00");
				$endDate 		= Carbon::parse($weekly_date[1]." 23:59:59");
				$weekStart 		= date("W",strtotime($startDate)) - 1;
				$month_name 	= date("m",strtotime($startDate));
        		$report_data	= DB::select("SELECT sum(grand_total) as sold_amount,
												  sum(discount_total_dollar) as discount,
												  sum(tax_dollar) as tax,
												  count(invoices_2.invoice_id) as table_sold,
												  count(invoices_2.invoice_id) as customer_count,
												  invoices_2.currency,
												  cms_users.cashier_name
												  FROM invoices
												  LEFT JOIN
												  (
												  	SELECT invoice_id,created_by,currency,week(invoice_date) as invoice_date
												  	FROM invoices
												  )invoices_2
												  ON invoices_2.invoice_id = invoices.invoice_id
												  LEFT JOIN 
												  (
												  	SELECT id,name as cashier_name
												  	FROM cms_users
												  )cms_users
												  ON cms_users.id = invoices_2.created_by
												  WHERE company_id = $user->company_id
												  AND invoices_2.invoice_date = '$weekStart'
												  GROUP BY (invoices_2.invoice_date)"
										);



        	elseif($filter == 'monthly'):
        		$monthly_date	= explode(" - ",$date);				
				$startDate 		= Carbon::parse($monthly_date[0]." 00:00:00");
				$endDate 		= Carbon::parse($monthly_date[1]." 23:59:59");
				$month 			= date("m",strtotime($startDate));
				$month_name 	= date("m",strtotime($startDate));
        		$report_data	= DB::select("SELECT sum(grand_total) as sold_amount,
												  sum(discount_total_dollar) as discount,
												  sum(tax_dollar) as tax,
												  count(invoices_2.invoice_id) as table_sold,
												  count(invoices_2.invoice_id) as customer_count,
												  invoices_2.currency,
												  cms_users.cashier_name
												  FROM invoices
												  LEFT JOIN
												  (
												  	SELECT invoice_id,created_by,currency,month(invoice_date) as invoice_date
												  	FROM invoices
												  )invoices_2
												  ON invoices_2.invoice_id = invoices.invoice_id
												  LEFT JOIN 
												  (
												  	SELECT id,name as cashier_name
												  	FROM cms_users
												  )cms_users
												  ON cms_users.id = invoices_2.created_by
												  WHERE company_id = $user->company_id
												  AND invoices_2.invoice_date = '$month'
												  GROUP BY (invoices_2.invoice_date)"
										);

        	elseif($filter == 'yearly'):
        		$month_name 	= date("m",strtotime($date));
        		$report_data	= DB::select("SELECT sum(grand_total) as sold_amount,
												  sum(discount_total_dollar) as discount,
												  sum(tax_dollar) as tax,
												  count(invoices_2.invoice_id) as table_sold,
												  count(invoices_2.invoice_id) as customer_count,
												  invoices_2.currency,
												  cms_users.cashier_name
												  FROM invoices
												  LEFT JOIN
												  (
												  	SELECT invoice_id,created_by,currency,year(invoice_date) as invoice_date
												  	FROM invoices
												  )invoices_2
												  ON invoices_2.invoice_id = invoices.invoice_id
												  LEFT JOIN 
												  (
												  	SELECT id,name as cashier_name
												  	FROM cms_users
												  )cms_users
												  ON cms_users.id = invoices_2.created_by
												  WHERE company_id = $user->company_id
												  AND invoices_2.invoice_date = '$date'
												  GROUP BY (invoices_2.invoice_date)"
										);

        	endif;

        	$output[] = [];
	    	if($report_data):
	    			
					foreach($report_data as $report): ?>
					<tr>
						<td><?php echo $report->cashier_name;?></td>
						<td><?php echo $month_name;?></td>
						<td><?php echo $report->sold_amount . $report->currency;?></td>
						<td><?php echo $report->table_sold;?></td>
						<td><?php echo $report->customer_count;?></td>
						<?php $sub_total = $report->sold_amount - $report->discount; ?>
						<td><?php echo $sub_total . $report->currency;?></td>
						
						
					</tr>


					<?php endforeach;
			else: ?>

			<tr>
				<td colspan="6" class="text-center"><h4>No Data!</h4></td>
			</tr>

			<?php endif;


	    }//End of get_top_seller

	    /* ====================== END of Function =================== */








	}

