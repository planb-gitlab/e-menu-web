<?php namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use DB;
use CRUDBooster;
use URL;
use Carbon\Carbon;
use Image;

class AdminTableNumbersController extends \crocodicstudio\crudbooster\controllers\CBController
{

    public function cbInit()
    {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field           = "name";
			$this->limit                 = "20";
			$this->orderby               = "id,desc";
			$this->global_privilege      = false;
			$this->button_table_action   = true;
			$this->button_bulk_action    = true;
			$this->button_action_style   = "button_icon";
			$this->button_add            = true;
			$this->button_edit           = true;
			$this->button_delete         = true;
			$this->button_detail         = false;
			$this->button_show           = false;
			$this->button_filter         = false;
			$this->button_import         = false;
			$this->button_export         = false;
			$this->table                 = "table_numbers";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col   = [];
			$this->col[] = ["label"=>"Table Code","name"=>"table_code"];
			// $this->col[] = ["label"=>"Type","name"=>"type"];
			$this->col[] = ["label"=>"Number of sit","name"=>"size"];
            $this->col[] = ["label"=>"Created At","name"=>"created_at"];
            // $this->col[] = ["label"=>"Table photo","name"=>"photo","image"=>true,"width"=>"100"];
			
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form   = [];
			$this->form[] = ['label'=>'Table Code','name'=>'table_code','type'=>'text','validation'=>'required|min:1|max:4','width'=>'col-sm-9','help'=>'Note* : 4 digits only'];
			// $this->form[] = ['label'=>'Type','name'=>'type','type'=>'select','validation'=>'required','width'=>'col-sm-9','dataenum'=>'Normal;VIP'];
			$this->form[] = ['label'=>'Number of sit','name'=>'size','type'=>'number','validation'=>'required|integer','width'=>'col-sm-9'];
            // $this->form[] = ['label'=>'Table Photo','name'=>'photo','type'=>'upload','validation'=>'image','width'=>'col-sm-9','help'=>'File types support : JPG, JPEG, PNG, GIF, BMP'];
			$this->form[] = ['label'=>'Description','name'=>'description','type'=>'text','width'=>'col-sm-9','placeholder' => 'Thing that describe this specific table , Ex: color...'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			# OLD END FORM

			/*
        | ----------------------------------------------------------------------
        | Sub Module
        | ----------------------------------------------------------------------
        | @label          = Label of action
        | @path           = Path of sub module
        | @foreign_key 	  = foreign key of sub table/module
        | @button_color   = Bootstrap Class (primary,success,warning,danger)
        | @button_icon    = Font Awesome Class
        | @parent_columns = Sparate with comma, e.g : name,created_at
        |
        */
        $this->sub_module = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Action Button / Menu
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
        | @icon        = Font awesome class icon. e.g : fa fa-bars
        | @color 	   = Default is primary. (primary, warning, succecss, info)
        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
        |
        */
        $this->addaction = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Button Selected
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @icon 	   = Icon from fontawesome
        | @name 	   = Name of button
        | Then about the action, you should code at actionButtonSelected method
        |
        */
        $this->button_selected = array();


        /*
        | ----------------------------------------------------------------------
        | Add alert message to this module at overheader
        | ----------------------------------------------------------------------
        | @message = Text of message
        | @type    = warning,success,danger,info
        |
        */
        $this->alert = array();


        /*
        | ----------------------------------------------------------------------
        | Add more button to header button
        | ----------------------------------------------------------------------
        | @label = Name of button
        | @url   = URL Target
        | @icon  = Icon from Awesome.
        |
        */
        $this->index_button = array();


        /*
        | ----------------------------------------------------------------------
        | Customize Table Row Color
        | ----------------------------------------------------------------------
        | @condition = If condition. You may use field alias. E.g : [id] == 1
        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
        |
        */
        $this->table_row_color = array();


        /*
        | ----------------------------------------------------------------------
        | You may use this bellow array to add statistic at dashboard
        | ----------------------------------------------------------------------
        | @label, @count, @icon, @color
        |
        */
        $this->index_statistic = array();


        /*
        | ----------------------------------------------------------------------
        | Add javascript at body
        | ----------------------------------------------------------------------
        | javascript code in the variable
        | $this->script_js = "function() { ... }";
        |
        */
        $this->script_js = NULL;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code before index table
        | ----------------------------------------------------------------------
        | html code to display it before index table
        | $this->pre_index_html = "<p>test</p>";
        |
        */
        $this->pre_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code after index table
        | ----------------------------------------------------------------------
        | html code to display it after index table
        | $this->post_index_html = "<p>test</p>";
        |
        */
        $this->post_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include Javascript File
        | ----------------------------------------------------------------------
        | URL of your javascript each array
        | $this->load_js[] = asset("myfile.js");
        |
        */
        $this->load_js      = array();
        $this->load_js[]    = asset("js/disable_row.js");


        /*
        | ----------------------------------------------------------------------
        | Add css style at body
        | ----------------------------------------------------------------------
        | css code in the variable
        | $this->style_css = ".style{....}";
        |
        */
        $this->style_css = NULL;


        /*
        | ----------------------------------------------------------------------
        | Include css File
        | ----------------------------------------------------------------------
        | URL of your css each array
        | $this->load_css[] = asset("myfile.css");
        |
        */
        $this->load_css     = array();
        $this->load_css[]   = asset('css/no_data.css');
        $this->load_css[]   = asset('css/img_display_fit.css');


    }


    /*
    | ----------------------------------------------------------------------
    | Hook for button selected
    | ----------------------------------------------------------------------
    | @id_selected = the id selected
    | @button_name = the name of button
    |
    */
    public function actionButtonSelected($id_selected, $button_name)
    {
        //Your code here

    }


    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate query of index result
    | ----------------------------------------------------------------------
    | @query = current sql query
    |
    */
    public function hook_query_index(&$query)
    {
        $myID = CRUDBooster::myId();
        $user = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $query->where('table_numbers.company_id', $user->company_id);
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate row of index table html
    | ----------------------------------------------------------------------
    |
    */
    public function hook_row_index($column_index, &$column_value)
    {
        //Your code here
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before add data is execute
    | ----------------------------------------------------------------------
    | @arr
    |
    */
    public function hook_before_add(&$postdata)
    {   

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after add public static function called
    | ----------------------------------------------------------------------
    | @id = last insert id
    |
    */
    public function hook_after_add($id)
    {
        //Your code here


    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before update data is execute
    | ----------------------------------------------------------------------
    | @postdata = input post data
    | @id       = current id
    |
    */
    public function hook_before_edit(&$postdata, $id)
    {   
        
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after edit public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_edit($id)
    {
        //Your code here
        

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command before delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_before_delete($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_delete($id)
    {
        //Your code here

    }
    

    // =============================== Create Table (View) ===============================
    public function getAdd(){
        $myID               = CRUDBooster::myId();
        $user               = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $now                = Carbon::now();
        $data               = [];
        $data['page_title'] = "Table Management";
        $data['command']    = "add";
        $data['now']        = "uploads/"."$user->company_id"."/"."$now->year"."-"."$now->month/";
        $this->cbView('table_management.table',$data);
    }

    // =============================== Update Table (View) ===============================
    public function getEdit($id){
        $myID               = CRUDBooster::myId();
        $user               = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $now                = Carbon::now();
        $data               = [];
        $data['page_title'] = "Table Management";
        $data['command']    = "edit";
        $data['table']      = DB::table('table_numbers')->where('id',$id)->first();
        $data['url']        = URL::to("/");
        $data['row']        = DB::table('table_numbers')->select('id')->where('id',$id)->first();
        $this->cbView('table_management.table',$data);

    }

    // =============================== Create Table (Function) ===============================
    public function AddTable(Request $request){
        $myID           = CRUDBooster::myId();
        $user           = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $now            = Carbon::now();
        $table_code     = $request->table_code;
        $table_type     = $request->table_type;
        $table_size     = $request->table_size;
        $photo          = $request->file('photo');
        $submit         = $request->submit;

        // check if table code exist
        $table          = DB::table('table_numbers')->where('table_code',$table_code)->first();
        if($table):
            CRUDBooster::redirect($_SERVER['HTTP_REFERER'], trans("crudbooster.exist_data"), "warning");
        endif;

        // crop and upload image
        if($photo):
            $photo_db               = "uploads/client_photo/". time() . '.' . $photo->getClientOriginalExtension();
            $photo_upload           = time() . '.' . $photo->getClientOriginalExtension();
            Image::make($photo)->fit(74,56)->save('../storage/app/uploads/client_photo/' . $photo_upload);
        endif;


        $dataSet = [
                    'table_code'    => $table_code,
                    'type'          => $table_type,
                    'status'        => '1',
                    'size'          => $table_size,
                    'floor'         => null,
                    'photo'         => $photo_db,
                    'description'   => null,
                    'is_merge'      => '0',
                    'created_by'    => $myID,
                    'company_id'    => $user->company_id,
                    'created_at'    => $now
                ]; 
        DB::table('table_numbers')->insert($dataSet);

        // Save redirect
        if(strtolower($submit) == strtolower(trans('crudbooster.button_save'))):
            CRUDBooster::redirect(CRUDBooster::mainpath(), trans("crudbooster.alert_add_data_success"), 'success');
        else:
            CRUDBooster::redirect($_SERVER['HTTP_REFERER'], trans("crudbooster.alert_add_data_success"), 'success');
        endif;
    }

    // =============================== Update Table (Function) ===============================
    public function UpdateTable(Request $request){

        $myID           = CRUDBooster::myId();
        $user           = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $now            = Carbon::now();
        $table_code     = $request->table_code;
        $table_type     = $request->table_type;
        $table_size     = $request->table_size;

        // check if table code exist
        $table          = DB::table('table_numbers')->where('table_code',$table_code)->where('table_size',$table_size)->first();
        if($table):
            CRUDBooster::redirect($_SERVER['HTTP_REFERER'], trans("crudbooster.exist_data"), "warning");
        endif;


        $table_update   = DB::table('table_numbers')->select('photo')->where('id',$id)->first();
        if($table_update->photo != "" || $table_update->photo != null):
            $photo_db      = $table_update->photo;
        else:
            if($photo):
                $photo_db               = "uploads/client_photo/". time() . '.' . $photo->getClientOriginalExtension();
                $photo_upload           = time() . '.' . $photo->getClientOriginalExtension();
                Image::make($photo)->fit(90,90)->save('../storage/app/uploads/client_photo/' . $photo_upload);
            endif;
        endif;

        $dataSet = [
                    'table_code'    => $table_code,
                    'type'          => $table_type,
                    'status'        => '1',
                    'size'          => $table_size,
                    'floor'         => null,
                    'photo'         => $photo_db,
                    'description'   => null,
                    'is_merge'      => '0',
                    'created_by'    => $myID,
                    'company_id'    => $user->company_id,
                    'created_at'    => $now
                ]; 
        DB::table('table_numbers')->where('id',$request->id)->update($dataSet);
        CRUDBooster::redirect($_SERVER['HTTP_REFERER'], trans("crudbooster.alert_update_data_success"), "info");
    }
}