<?php 
namespace App\Http\Controllers;

	use Session;
	use Illuminate\Http\Request;
	use DB;
	use CRUDBooster;
	use Carbon\Carbon;

class AdminRefundController extends \crocodicstudio\crudbooster\controllers\CBController
{
	public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "id";
			$this->limit = "20";
			$this->orderby = "invoice_id,desc";
			$this->global_privilege = false;
			$this->button_table_action = false;
			$this->button_bulk_action = false;
			$this->button_action_style = "button_icon";
			$this->button_add = false;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = true;
			$this->button_show = false;
			$this->button_filter = false;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "invoices";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];

			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];

			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        
	        $this->load_js = array();
	        $this->load_js[] = asset('js/print/invoice.js');
	        $this->load_js[] = asset('js/print/print_script.js');
	        
	        
	    }


	    

	    public function getIndex(){
		    	
		    $myID = CRUDBooster::myId();
		    $data = [];
		    $data['page_title'] = "Refund by invoice";
		    $this->cbView('admin.refund',$data);
		}


		public function sort_invoice(Request $request)
		{

		    $myID = CRUDBooster::myId();
		    $user = DB::table('cms_users')->where('id',$myID)->select('company_id')->first();

		    	if ($request->by == "Weekly"):
				    $startDate = date("Y-m-d", strtotime('monday this week'))." 00:00:00";
					$endDate = date("Y-m-d", strtotime('sunday this week'))." 23:59:59";

				elseif ($request->by == "Monthly" ):
					
					$startDate = date('Y-m-01')." 00:00:00";
					$date = date('Y-m-d');
					$endDate = date("Y-m-t", strtotime($date))." 23:59:59";
					
				elseif ($request->by == "Yearly"):
					$startDate = date('Y-01-01')." 00:00:00";
					$date = date('Y-m-d');
					$endDate = date("Y-12-t", strtotime($date))." 23:59:59";

					
				elseif ($request->by == "Daily" ):
					
					$day = new \DateTime();
					$startDate = $day->format('Y-m-d')." 00:00:00";
					$endDate = $day->format('Y-m-d')." 23:59:59";

				else: 
					$request->date;
					$date = explode(" - ",$request->date);
					$startDate = $date[0]." 00:00:00";
					$endDate = $date[1]." 23:59:59";

					if ($startDate == $endDate):
						$startDate = $date[0]." 00:00:00";
						$endDate = $date[1]." 23:59:59";

					endif;
					
				endif;
				

			
				
				
				$invoices = DB::select("
					SELECT * FROM invoices
					WHERE invoice_date >= '$startDate'
					AND invoice_date < '$endDate' 
					AND void = 0 
					AND invoices.is_refund = 0 
					AND status = 1 
					AND company_id = $user->company_id Order By invoice_id DESC");


			

				if(count($invoices)>=1):
					foreach ($invoices as $key => $invoice): 
					$invoice_details = 	DB::select("SELECT amount FROM orders
										LEFT JOIN orders_detail ON orders.id = orders_detail.orders_id
										INNER JOIN invoices ON invoices.orders_id = orders_detail.orders_id
										INNER JOIN menus ON orders_detail.menus_id = menus.id
										WHERE orders.id = '$invoice->orders_id'
										AND invoices.company_id = $user->company_id
										AND invoices.is_refund = 0
										
										");
						
						$quantity = count($invoice_details);
						
						?>	



						<tr>
							<td class="col-md-1"><?php echo ++$key; ?></td>
							<td class="col-md-2"><?php echo $invoice->invoice_number; ?></td>
							<td><?php echo $invoice->invoice_date;?></td>
							<td><?php echo $invoice->currency . $invoice->grand_total; ?></td>
							<td><?php echo $quantity; ?></td>
							<td>
								<a href="#" class="button_show_invoice" data-val="<?php echo $invoice->invoice_number; ?>"><?php echo "Refund" ?></a> 
								<input type="hidden" id="invoiceId" value="<?php echo $invoice->invoice_id; ?>">
								
							</td>
						</tr>


				    <?php endforeach; 
				else: ?>

					<tr>
	                	<td colspan="5" class="text-center"><h4>No Data!</h4></td>
	              	</tr>

				<?php endif;
				

		    }
		    	
		    
		    public function refund_update(Request $request){
		    	$invoiceId 	= $request->invoice_id;
		    	$menus_id 	= $request->is_refund;

		    	foreach($menus_id as $menu){
		    		DB::Update("UPDATE invoices, orders, orders_detail SET invoices.void=1,invoices.is_refund=1,orders.is_refund=1, orders_detail.is_refund=1 WHERE invoices.invoice_number=$invoiceId AND invoices.orders_id=orders.id AND orders.id= orders_detail.orders_id AND orders_detail.menus_id=$menu");
		    	}


		    	
		    }
		    public function search_invoice_byid(Request $request)
		    {
		    	$myID  	= CRUDBooster::myId();
		    	$id 	= $request->id;
		    	$user=DB::table('cms_users')->where('id',$myID)->select('company_id')->first();

		    	$invoices = DB::select("
		    		SELECT * FROM invoices 
		    		WHERE invoice_number LIKE '%$id%' 
		    		AND void = 0 
		    		AND status = 1 
		    		AND invoices.is_refund = 0 
		    		AND company_id = $user->company_id 
		    		Order By invoice_number DESC");

		    	 

		    	if(count($invoices)>=1):
					foreach ($invoices as $key => $invoice): 
					$invoice_details = 	DB::select("SELECT amount FROM orders
										LEFT JOIN orders_detail ON orders.id = orders_detail.orders_id
										INNER JOIN invoices ON invoices.orders_id = orders_detail.orders_id
										INNER JOIN menus ON orders_detail.menus_id = menus.id
										WHERE orders.id = '$invoice->orders_id'
										AND invoices.company_id = $user->company_id
										AND invoices.is_refund = 0
										
										");
						
						$quantity = count($invoice_details);
						
						?>	



						<tr>
							<td class="col-md-1"><?php echo ++$key; ?></td>
							<td class="col-md-2"><?php echo $invoice->invoice_number; ?></td>
							<td><?php echo $invoice->invoice_date;?></td>
							<td><?php echo $invoice->currency . $invoice->grand_total; ?></td>
							<td><?php echo $quantity; ?></td>
							<td>
								<a href="#" class="button_show_invoice" data-val="<?php echo $invoice->invoice_number; ?>"><?php echo "Refund" ?></a> 
								<input type="hidden" id="invoiceId" value="<?php echo $invoice->invoice_id; ?>">
								
							</td>
						</tr>


				    <?php endforeach; 
				else: ?>

					<tr>
	                	<td colspan="5" class="text-center"><h4>No Data!</h4></td>
	              	</tr>

				<?php endif;
		    }



	   //insert menu into table
		public function insert_order(Request $request){
			$invoice_id=$request->invoice_id;
			$myID 			= CRUDBooster::myId();
			$get_company_id=DB::table('cms_users')
		    				->where('id',$myID)
		    				->select('company_id')
		    				->first();

		    $setting = DB::table('settings')
							->leftjoin('countries','countries.id','=','settings.company_country')
							->leftjoin('currencies','currencies.id','=','settings.company_currency')
							->select('settings.*','countries.*','currencies.*')
							->where('settings.id',$get_company_id->company_id)
							->first();
							


		   	$last_insert_invoice = DB::table('invoices')
						->join('orders','orders.id','=','invoices.orders_id')
						->leftjoin('promotion_packs','promotion_packs.id','=','invoices.promotion_pack_id')
						->select('invoices.*','orders.*','promotion_packs.*')
						->where('invoices.company_id',$get_company_id->company_id)
						->where('invoices.invoice_number',$invoice_id)
						->where('invoices.void',0)
						->orderby('invoice_id','DESC')->limit(1)
						->first();


	 		 $printing_orders = DB::table("orders")
						->join('orders_detail','orders_detail.orders_id','=','orders.id')
						->leftjoin('menus','menus.id','=','menus_id')
						->leftjoin('menu_materials','menu_materials.id','=','material_id')
						->leftjoin('menu_uom_price','menu_uom_price.id','=','orders_detail.menus_size')
						->select('menus.*','menu_materials.*','orders.*','orders_detail.*','menus.name as menu_name','orders_detail.price as order_menu_price','menu_materials.name as menu_material_name','menu_uom_price.*')
						->where('orders.company_id',$get_company_id->company_id)
						->where('orders.id',$last_insert_invoice->orders_id)
						->orderby('orders.id','DESC')->get();
			$grandtotal=DB::table('invoices')
						->where('invoice_number',$invoice_id)
						->where('invoices.company_id',$get_company_id->company_id)
						->where('is_refund',0)
						->select('invoices.*')
						->first();
			$exchange_rate_tokh= DB::table('exchange')
						->select('exchange.*')
						->where('currency_from_text','=','Dollar')
						->where('company_id',$get_company_id->company_id)
						->first();
			$exchange_rate_bath= DB::table('exchange')
						->select('price_to')
						->where('currency_from_text','=','Baht')
						->where('currency_to_text','=','Dollar')
						->where('company_id',$get_company_id->company_id)
						->first();
			
					
			
			?>

				
	<div class="main_invoice">
	<div id="container">
	    <section id="memo">
	        <div class="logo">
	          <img data-logo="company_logo" src='<?php echo $setting->company_logo; ?>' />
	        </div>
	        
	        <div class="company-info">
	          <div><?php echo $setting->company_name;?></div>

	          <br/>
	          
	          <span><?php echo $setting->company_address;?></span>
	          <span><?php echo $setting->country_name;?></span>

	          <br/>
	          
	          <span><?php echo $setting->company_contact;?></span>
	          <br/>
	          <span><?php echo $setting->company_phone;?></span>
	          <br/>	
	          <span><?php echo $setting->company_email;?></span>

	        </div>

	      </section>
	       <section id="invoice-title-number">
	      
	        <span id="title">Invoice</span>
	        <span id="number"><?php echo $last_insert_invoice->order_number;?></span>
	        
	      </section>
	      <div class="clearfix"></div>
	      
	      <section id="items">
	        
	        <table cellpadding="0" cellspacing="0">
	        <thead>
			          <tr>
			            <th>No</th>
			            <th>Description</th>
			            <th>Quantity</th>
			            <th>Price</th>
			            <th>Subtotal</th>
			            
			          </tr>
	          </thead>
	          <tbody id="show_order">
	          <?php foreach($printing_orders as $key => $order): ?>
			
		          <tr data-iterate="item" class="invoice" data-id="<?php echo $order->menus_id; ?>" data-refund="0">
		             <!-- Don't remove this column as it's needed for the row commands -->
		            <?php if($order->material_id):?>
		            <td></td>
		            <td class="material"><?php echo $order->menu_material_name; ?></td>
		            <?php else: ?>	
		            <td><?php echo $key+1; ?></td>
		            <td><?php echo $order->menu_name; ?>
		            		<br/>
		  					<?php if($order->menus_size){ ?>
		            		<span class="m_size_name">(<?php echo $order->uom; ?>)</span>
		            <?php }	 ?>
		            <?php endif;?>

		            

		            </td>
		            <td><?php echo $order->amount; ?></td>
		            <td class="price_col"><?php echo $setting->currency_symbol; echo number_format($order->order_menu_price, 2); ?></td>
		            <td class="subtotal_col"><?php echo $setting->currency_symbol; echo number_format($order->subtotal, 2);?> <i data-changecurrency="<?php echo $exchange_rate_tokh->price_to; ?>" style="font-size:20px;color:red"></i></td>
		          </tr>
		         <!--  fa fa-cross cross -->
			
	      	  <?php endforeach; ?>
	      	  </tbody>
	          
	        </table>
	        
	      </section>

	       <section id="sums">
      
	        <table cellpadding="0" cellspacing="0">
	          <tr>
	            <th>Sub Total :</th>
	            <td id="subtotal"><?php echo $setting->currency_symbol; echo $last_insert_invoice->total; ?></td>
	          </tr>
	          
	          <tr data-iterate="tax">
	            <th>Tax :</th>
	            <td><?php echo $last_insert_invoice->tax_amount. "%"; ?></td>
	            <input type="hidden" id="txt_tax_amount" value="<?php echo $last_insert_invoice->tax_amount ?>">
	          </tr>
	           
	           <?php if( $last_insert_invoice->discount_amount > 0):?>
		        <tr data-iterate="discount">
		            <th>discount :</th>
		            <td><?php 
		            		if($last_insert_invoice->discount_amount):
		            		echo $last_insert_invoice->discount_amount; echo $last_insert_invoice->discount_type;
		            		else:
		            		echo $last_insert_invoice->currency . "0";
		            		endif; ?></td>
		        </tr>
		    	<?php elseif($last_insert_invoice->promotion_pack_id != 0 || $last_insert_invoice->promotion_pack_id != null): ?>
		    	<tr data-iterate="discount">
		            <th>discount (Promotion) :</th>
		            <td><?php 
		            	if($last_insert_invoice->promotion = "discount"):
		            		echo $last_insert_invoice->discount_type . $last_insert_invoice->discount_amount; 
		            	endif;
		            	?>	
		            </td> 
		            		
		        </tr>
		        <?php endif; ?>
	         			
	         	<!-- total group -->
		        <tr class="amount-total">
		            <th>Total (<?php echo $setting->currency_symbol; ?>):</th>
		            <td id="order_total_data_dollar"><?php echo $setting->currency_symbol; echo $last_insert_invoice->grand_total;?></td>
		        </tr>



			    <?php if($last_insert_invoice->grand_dollar_exchange != 0){?>
	    		<tr class="amount-total">
		            <th>Total ($):</th>
		            <td id="order_total_data_dollar"><?php echo $setting->currency_symbol; echo $last_insert_invoice->grand_dollar_exchange;?></td>
	          	</tr>
			    <?php }?>
			    	
			    <?php if( $last_insert_invoice->grand_riel_exchange!=0){ ?>
				           <tr class="amount-total">
				            <th>Total (<?php echo '<span>&#6107;</span>';?>) :</th>
				            <td id="order_total_data_riel"><?php echo '<span>&#6107;</span>'; echo $last_insert_invoice->grand_riel_exchange;?></td>
				          </tr>
			        <?php } ?>
		  
	          		<?php if( $last_insert_invoice->grand_baht_exchange!=0){ ?>
				           <tr class="amount-total">
				            <th>Total (<?php echo '<span>&#3647</span>';?>):</th>
				            <td id="order_total_data_bath"><?php echo $last_insert_invoice->grand_baht_exchange;?></td>
				          </tr>
				     <?php } ?>
	          
	          <!-- You can use attribute data-hide-on-quote="true" to hide specific information on quotes.
	               For example Invoicebus doesn't need amount paid and amount due on quotes  -->
	        
		          
				      <tr data-hide-on-quote="true">
				            <th>Paid (<?php echo $setting->currency_symbol;?>):</th>
				            <td id="paid_dollar"><?php echo $setting->currency_symbol; ?><?php echo $last_insert_invoice->paid_total ;?></td>
				     </tr>	
				
		
				<?php if($last_insert_invoice->paid_dollar!=0){ ?>
				          <tr data-hide-on-quote="true">
				            <th>Paid ($):</th>
				            <td id="paid_dollar"><?php echo $last_insert_invoice->paid_dollar ;?></td>
				          </tr>	
				   <?php } ?>
			
				<?php if($last_insert_invoice->paid_riel!=0){ ?>
				          <tr data-hide-on-quote="true">
				            <th>Paid (<?php echo '<span>&#6107;</span>';?>):</th>
				            <td id="paid_riel">៛<?php echo '<span>&#6107;</span>'; echo $last_insert_invoice->paid_riel ;?></td>
				          </tr>
				<?php } ?>
		
			
				<?php if($last_insert_invoice->paid_baht!=0){ ?>
			          <tr data-hide-on-quote="true">
			            <th>Paid (<?php echo '<span>&#3647</span>';?>):</th>
			            <td id="paid_bath"><?php echo '<span>&#3647</span>'; echo $last_insert_invoice->paid_baht;?></td>
			          </tr>
			    <?php } ?>
			
	       
	         <!--  return -->
	       
				     <tr data-hide-on-quote="true">
				            <th>Return(<?php echo $setting->currency_symbol;?>):</th>
				            <td id="return_dollar"><?php echo $setting->currency_symbol; ?><?php echo $last_insert_invoice->return_amount; ?></td>
				     </tr>
		        
	         		<?php if($last_insert_invoice->return_dollar!=0){ ?>
				          <tr data-hide-on-quote="true">
				            <th>Return ($):</th>
				            <td id="return_dollar"><?php echo $last_insert_invoice->return_dollar; ?></td>
				          </tr>
	          	<?php } ?>
	         
				<?php if($last_insert_invoice->return_riel!=0){ ?>
				           <tr data-hide-on-quote="true">
				            <th>Return (<?php echo '<span>&#6107;</span>';?>):</th>
				            <td id="return_riel"><?php echo '<span>&#6107;</span>'; echo $last_insert_invoice->return_riel; ?></td>
				          </tr>
				<?php } ?>
		
			 
			  	<?php if($last_insert_invoice->return_bath!=0){ ?>  
			           <tr data-hide-on-quote="true">
			            <th>Return (<?php echo '<span>&#3647</span>';?>) :</th>
			            <td id="return_bath"><?php echo '<span>&#3647</span>'; echo $last_insert_invoice->return_bath; ?></td>
			          </tr>
			     <?php } ?> 
			
	          
	        </table>

	        <div class="clearfix"></div>
	        
	      </section>

	      <div class="clearfix"></div>

	      <section id="invoice-info">
	        <div>
	          <span>Issued On</span> <span><?php echo $last_insert_invoice->invoice_date; ?></span>
	        </div>
	        <!-- <div>
	          <span>Due On</span> <span><?php echo $last_insert_invoice->invoice_date; ?></span>
	        </div> -->

	        <br />

	       <!--  <div>
	          <span>Currency</span> <span><?php echo $setting->currency_symbol; ?></span>
	        </div> -->
	        <div>

	      </section>
	      
	     <!--  <section id="terms">
	     
	       <div class="notes"><?php echo $setting->company_policy;?></div>
	     
	       <br />
	     
	     
	       
	     </section>
	      -->
	      <div class="clearfix"></div>
			<div class="choosing_refund_type">
			
				<div class="header_amount" id="total"><span>Grant Total:</span>  <?php echo $grandtotal->grand_total; ?>$</div>
				<div class="body_refund_type">
					<div class="group">
						<label for="" class="label_refund">Please Choose the refund type:<?php $total ?></label>
					</div>
					<div class="group">
						<button class="btn btn-primary" data-id="1" id="percentage">%</button>
						<button class="btn btn-primary" data-id="2" id="dollar">$</button>
					</div>
					<input type="text" class="form-control input_refund_amount"  data-grand_val="<?php echo $grandtotal->grand_total; ?>" placeholder="0.00" style="display: none;">
				</div>
				
					<span class="span_refund_amount" style="display: none;">After refund:</span>
					<input type="text" id="txt_refund_amount" class="form-control footer_refund_type" disabled placeholder="0.00" style="display: none;">

				
			</div>
	     <!--  <div class="thank-you">Thank You!</div> -->

	      <div class="clearfix"></div>
	      <button type="button" class="btn btn-default refund_button">Refund</button>

	     
	</div>
	</div>

			<?php
								
		}

	/*public function percentage_cal(Request $request){
		$id=$request->invoice_id;
		$dol=$request->dol_val;
		$per=$request->percen_val;
		$total="";
		$data=DB::table('invoices')
				->where('invoice_id',$id)
				->where('is_refund',0)
				->select('invoices.*')
				->first();
		$number_button=$request->number_of_button;
		if($number_button==1){
				$total=$data->grand_total-($per*$data->grand_total)/100;
		}else{
				$total=$data->grand_total-$dol;
		}
		return view('admin.refund')->withTotal($total);
	}*/
	public function update_after_refund(Request $request){
		$myID 			= CRUDBooster::myId();
		$after_refund_amount=$request->after_refund_amount;
		$refund_amount=$request->refund_amount;
		$id=$request->invoice_id;
		$get_company_id=DB::table('cms_users')
		    				->where('id',$myID)
		    				->select('company_id')
		    				->first();
		DB::table('invoices')
			->where('invoice_number',$id)
			->where('company_id',$get_company_id->company_id)
			->update(['refund_amount'=>$refund_amount,'is_refund'=>1,'grand_total_after_refund'=>$after_refund_amount]);
	}



		    public function show_invoice(Request $request){
		    	$invoiceId 		= $request->invoiceId;
		    	$myID 			= CRUDBooster::myId();
		    	$company		= DB::table('settings')->where('created_by',$myID)->select('company_name')->first();
		    	
		    	if($invoiceId != null):
		    		$invoice 	= DB::table('invoices')
		    					->join('orders','invoices.orders_id','=','orders.id')
		    					->leftjoin('currencies','currencies.name','=','orders.currency')
		    					->where('invoice_id',$invoiceId)->where('invoices.created_by',$myID)->where('currencies.created_by',$myID)->first();

		    		$invoice_details = DB::table('orders_detail')
		    						->select('orders_detail.*'						,
		    								 'orders_detail.price as order_price'	,
		    								 'invoices.*'							,
		    								 'menus.*'								,
		    								 'menu_materials.id as material_id'		,
		    								 'menu_materials.name as material_name' ,
		    								 'currencies.currency_symbol'			
		    								)
			    					->join('invoices','invoices.orders_id','=','orders_detail.orders_id')
			    					->leftjoin('currencies','currencies.name','=','invoices.currency')
			    					->leftjoin('menus','menus.id','=','orders_detail.menus_id')
			    					->leftjoin('menu_materials','menu_materials.id','=','orders_detail.material_id')
			    					->where('invoices.created_by',$myID)->where('invoices.invoice_id',$invoiceId)->where('currencies.created_by',$myID)->get();
		    	endif;

		    	?>

		    	<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="print_invoice">
				  <div class="modal-dialog modal-md">
				    <div class="modal-content">

				      <div class="modal-header">
				        <h4 class="modal-title" id="myLargeModalLabel" style="text-align: center;"><?php echo $company->company_name; ?></h4>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">×</span>
				        </button>
				      </div>
				      <div class="modal-body">
				       	<table class="invoice_data">
				       		<tr>
				       			<td>Invoice Number</td>
				       			<td><?php echo $invoice->invoice_id; ?></td>
				       		</tr>

				       		<tr>
				       			<td>Invoice Order Number</td>
				       			<td><?php echo $invoice->order_number; ?></td>
				       		</tr>

				       		<tr>
				       			<td>Invoice Date</td>
				       			<td><?php echo $invoice->created_at; ?></td>
				       		</tr>
				       		

				       	</table>

				       	<table class="order_data" style="margin-bottom: 10px;">
				       		<thead>
					       		<tr>
					       			<th>Item Name</th>
					       			<th>Item Price</th>
					       			<th>Item Quantity</th>
					       			<th>Sub total</th>
					       		</tr>
				       		</thead>

				       		<?php foreach($invoice_details as $invoice_detail): ?>
				       		
				       		<tbody>
					       		<tr class="order_data_detail">
					       			<?php if($invoice_detail->material_id != null): ?>
						       			<td style="text-indent: 20px;"><?php echo $invoice_detail->material_name; ?></td>
					       			
					       			<?php elseif($invoice_detail->broken_name != ''): ?>
						       			<td><?php echo $invoice_detail->broken_name; ?></td>

					       			<?php else: ?>
						       			<td><?php echo $invoice_detail->name; ?></td>

					       			<?php endif; ?>
					       			
					       			<td><?php echo $invoice_detail->currency_symbol; echo $invoice_detail->order_price; ?></td>
					       			<td><?php echo $invoice_detail->amount; ?></td>
					       			<td><?php echo $invoice_detail->currency_symbol; echo $invoice_detail->subtotal; ?>	</td>
					       		</tr>
				       		</tbody>

				       		<?php endforeach; ?>
				       		

				       	</table>

				       	<!-- total with no tax and no discount -->
				       	<div class="total" style="text-align: right; margin-bottom: 3px">
				       		Total : 
				       		<input type="text" name="" disabled style="text-align: center;" value="<?php echo $invoice->currency_symbol; echo $invoice->total; ?>">
				       	</div>

				       	<!-- discount -->
				       	<?php if($invoice->discount_type != NULL && $invoice->discount_amount > 0): ?>
				       	<div class="discount" style="text-align: right; margin-bottom: 3px">
				       		Discount : 
				       		<input type="text" name="" disabled style="text-align: center;" value="<?php echo $invoice->discount_amount; echo $invoice->discount_type; ?>">
				       		
				       	</div>
				       	<?php endif; ?>

				       	<!-- total with tax and discount -->
				       	<div class="total_with_tax_discount" style="text-align: right; margin-bottom: 3px">
				       		Total (Discount + Tax) : 
				       		<input type="text" name="" disabled style="text-align: center;" value="<?php echo $invoice->currency_symbol; echo $invoice->total_with_tax_discount; ?>">
				       		
				       	</div>



				      </div>


				      <div class="modal-footer">
				      	<button type="button" class="btn btn-primary" onclick="doit()">print</button>
				      </div>


				    </div>
				  </div>
				</div>
		    	
		    	<?php 

		    }

	   

}
