<?php namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use DB;
use CRUDBooster;
use Response;
use URL;
use Carbon\Carbon;
use Dompdf\Dompdf;
use Image;

class AdminCategoriesController extends \crocodicstudio\crudbooster\controllers\CBController
{
    
    public function cbInit()
    {   
        # START CONFIGURATION DO NOT REMOVE THIS LINE
        $this->title_field          = "name";
        $this->limit                = "20";
        $this->orderby              = "id,desc";
        $this->global_privilege     = false;
        $this->button_table_action  = true;
        $this->button_bulk_action   = true;
        $this->button_action_style  = "button_icon";
        $this->button_add           = true;
        $this->button_edit          = true;
        $this->button_delete        = true;
        $this->button_detail        = false;
        $this->button_show          = false;
        $this->button_filter        = false;
        $this->button_import        = false;
        $this->button_export        = false;
        $this->table                = "categories";
        # END CONFIGURATION DO NOT REMOVE THIS LINE

        # START COLUMNS DO NOT REMOVE THIS LINE
        $this->col   = [];
        $this->col[] = ["label" => "Name",  "name" => "name"];
        $this->col[] = ["label" => "Photo", "name"=>"photo","image"=>true,"width"=>"80"];
        $this->col[] = ["label" => "Status", "name" => "status"];
        # END COLUMNS DO NOT REMOVE THIS LINE

        # START FORM DO NOT REMOVE THIS LINE
        $this->form = [];
        $this->form[] = ['label' => 'Name', 'name' => 'name', 'type' => 'text', 'validation' => 'required|string|min:3|max:70', 'width' => 'col-sm-10', 'placeholder' => 'You can only enter the letter only'];
        $this->form[] = ['label' =>'Photo','name'=>'photo','type'=>'upload','validation'=>'image','width'=>'col-sm-10','help'=>'File types support : JPG, JPEG, PNG, GIF, BMP'];
        # END FORM DO NOT REMOVE THIS LINE

       
        $this->load_js = array();
        $this->load_js[] = asset("js/disable_row.js");


        /*
        | ----------------------------------------------------------------------
        | Add css style at body
        | ----------------------------------------------------------------------
        | css code in the variable
        | $this->style_css = ".style{....}";
        |
        */
        $this->style_css = NULL;


        /*
        | ----------------------------------------------------------------------
        | Include css File
        | ----------------------------------------------------------------------
        | URL of your css each array
        | $this->load_css[] = asset("myfile.css");
        |
        */
        $this->load_css = array();
        $this->load_css[] = asset('css/no_data.css');
    }


    /*
    | ----------------------------------------------------------------------
    | Hook for button selected
    | ----------------------------------------------------------------------
    | @id_selected = the id selected
    | @button_name = the name of button
    |
    */
    public function actionButtonSelected($id_selected, $button_name)
    {
        //Your code here

    }


    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate query of index result
    | ----------------------------------------------------------------------
    | @query = current sql query
    |
    */
    public function hook_query_index(&$query)
    {   
        $myID = CRUDBooster::myId();
        $user = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $query->where('categories.company_id', $user->company_id);
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate row of index table html
    | ----------------------------------------------------------------------
    |
    */
    public function hook_row_index($column_index, &$column_value)
    {
        //Your code here
    }
    

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command before delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_before_delete($id)
    {
        $myID           = CRUDBooster::myId();
        $user           = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $subcategory    = DB::table('subcategories')->select('id')->where('category_id',$id)->where('company_id',$user->company_id)->get();
        $menu           = DB::table('menus')->select('id')->where('category_id',$id)->where('company_id',$user->company_id)->get();

        if(count($subcategory) >= 1 || count($menu) >= 1):
            CRUDBooster::redirect($_SERVER['HTTP_REFERER'], trans("crudbooster.data_is_in_use"), "warning");
        endif;
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_delete($id)
    {
        //Your code here

    }

    // ======================== Create Menu (View) ============================
    public function getAdd(){
        $data                   = [];
        $data['page_title']     = "Category Menu";
        $data['command']        = "add";
        $this->cbView('menu_management.category.category',$data);
    }

    // ======================== Update Menu (View) ============================
    public function getEdit($id){
        $data                   = [];
        $data['url']            = URL::to("/");
        $data['page_title']     = "Category Menu";
        $data['id']             = $id;
        $data['category']       = DB::table('categories')->where('id',$id)->first();
        $this->cbView('menu_management.category.category',$data);
    }

    // ======================== Create Menu (Function) ============================
    public function save(Request $request){
        $myID           = CRUDBooster::myId();
        $user           = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $now            = Carbon::now();
        $name           = $request->name;
        $submit         = $request->submit;

        $photo                  = $request->file('photo');
        if(!empty($photo)):
            $photo_db               = "uploads/client_photo/". time() . '.' . $photo->getClientOriginalExtension();
            $photo_upload           = time() . '.' . $photo->getClientOriginalExtension();
            Image::make($photo)->fit(103,95)->save('../storage/app/uploads/client_photo/' . $photo_upload);
        endif;

        if($name): 
            $checkExist     = DB::table('categories')->where('name', $name)->where('company_id', $user->company_id)->get();
            if(count($checkExist) >= 1):
                CRUDBooster::redirect($_SERVER['HTTP_REFERER'], trans("crudbooster.exist_data"), "warning");
            else:
                DB::table('categories')->insert(
                    [
                        'name'          => $name,
                        'photo'         => $photo_db,
                        'company_id'    => $user->company_id,
                        'created_by'    => $myID,
                        'status'        => '1',
                        'created_at'    => $now,

                    ]
                );

                if(strtolower($submit) == strtolower(trans('crudbooster.button_save'))):
                    CRUDBooster::redirect(CRUDBooster::mainpath(), trans("crudbooster.alert_add_data_success"), 'success');
                else:
                    CRUDBooster::redirect($_SERVER['HTTP_REFERER'], trans("crudbooster.alert_add_data_success"), 'success');
                endif;
            endif;
        endif;
    }

    // ======================== Update Menu (Function) ============================
    public function update(Request $request){
        $myID           = CRUDBooster::myId();
        $user           = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $now            = Carbon::now();
        $id             = $request->id;
        $name           = $request->name;
        $image          = DB::table('categories')->where('id',$id)->first();
        $photo          = $request->photo;

        

        if($image->photo != "" || $image->photo != null):
                $menu_image     = DB::table('categories')->select('photo')->where('id',$id)->first();
                $photo_db          = $menu_image->photo;
        else:
                if($request->file('photo')):
                    $photo                  = $request->file('photo');
                    $photo_db               = "uploads/client_photo/". time() . '.' . $photo->getClientOriginalExtension();
                    $photo_upload           = time() . '.' . $photo->getClientOriginalExtension();
                    Image::make($photo)->fit(103,95)->save('../storage/app/uploads/client_photo/' . $photo_upload);
                elseif($photo && !$_FILES['photo']['name']):
                    $photo_db   = $menu_image->photo;    
                else:
                    $photo_db   = null;
                endif;
        endif;

                DB::table('categories')->where('id',$id)->update(
                    [
                        'name'          => $name,
                        'photo'         => $photo_db,
                        'company_id'    => $user->company_id,
                        'created_by'    => $myID,
                        'updated_at'    => $now
                    ]
                );
                CRUDBooster::redirect($_SERVER['HTTP_REFERER'], trans("crudbooster.alert_update_data_success"), "info");

    }


}
