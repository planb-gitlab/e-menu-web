<?php namespace App\Http\Controllers;

	use Session;
	use Illuminate\Http\Request;
	use DB;
	use CRUDBooster;
	use Response;

	class AdminSaleByItemController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field 				= "id";
			$this->limit 					= "20";
			$this->orderby 					= "id,desc";
			$this->global_privilege 		= false;
			$this->button_table_action 		= false;
			$this->button_bulk_action 		= false;
			$this->button_action_style 		= "button_icon";
			$this->button_add 				= false;
			$this->button_edit 				= true;
			$this->button_delete 			= true;
			$this->button_detail 			= true;
			$this->button_show 				= false;
			$this->button_filter 			= false;
			$this->button_import 			= false;
			$this->button_export 			= false;
			$this->table = "orders";
			# END CONFIGURATION DO NOT REMOVE THIS LINE
	    }

	    public function getIndex() {
		   
		   	$myID 					= CRUDBooster::myId();
	    	$user					= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
		   
		   	$data 					= [];
		   	$data['page_title']    	= trans('report.Sale by Item Report');
		   	$data['get_item_info']	= DB::table('menus')->where('menus.company_id',$user->company_id)->where('menus.status',1)->distinct()->get();
		   	$data['get_item_uom']	= DB::table('uom')->select('id','name')->where('company_id',$user->company_id)->where('status',1)->get();
		   	$data['get_item_subcat']= DB::table('subcategories')->select('id','name')->where('company_id',$user->company_id)->where('status',1)->get();
		   	$data['get_item_cat'] 	= DB::table('categories')->select('id','name')->where('company_id',$user->company_id)->where('status',1)->get();

		   	$data['invoices']   	= DB::table('invoices')
		   								->select('item_id','menus.name as name','menu_uom_price.uom as uom_name','categories.name as cat_name','subcategories.name as sub_name',
		   									DB::raw('count(*) as count_qty')
		   								)
		   								->leftjoin('invoice_orders','invoice_orders.invoice_id','=','invoices.invoice_id')
		   								->leftjoin('menus','menus.id','=','invoice_orders.item_id')
		   								->leftjoin('categories','categories.id','=','menus.category_id')
		   								->leftjoin('subcategories','subcategories.id','=','menus.subcategory_id')
		   								->leftjoin('menu_uom_price','menu_uom_price.menu_id','=','invoice_orders.item_id')
	    								->leftjoin('settings','settings.id','=','invoice_orders.company_id')
	    								->groupBy('item_id')->groupBy('menus.name')->groupBy('menu_uom_price.uom')->groupBy('categories.name')->groupBy('subcategories.name')
	    								->OrderBy('menus.name','ASC')
	    								->paginate(20);

		   	$this->cbView('report.sale_by_item',$data);
		}	

		public function get_sort(Request $request){
			$myID 					= CRUDBooster::myId();
	    	$user					= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();


	    	$data 					= [];
	    	$data['page_title']    	= trans('report.Sale by Item Report Search');
	    	$data['page_title']    	= trans('report.Sale by Item Report');
		   	$data['get_item_info']	= DB::table('menus')->where('menus.company_id',$user->company_id)->where('menus.status',1)->distinct()->get();
		   	$data['get_item_uom']	= DB::table('uom')->select('id','name')->where('company_id',$user->company_id)->where('status',1)->get();
		   	$data['get_item_subcat']= DB::table('subcategories')->select('id','name')->where('company_id',$user->company_id)->where('status',1)->get();
		   	$data['get_item_cat'] 	= DB::table('categories')->select('id','name')->where('company_id',$user->company_id)->where('status',1)->get();
	    	

	    	$date 					= $request->by;
	    	$custom_date			= $request->daterange;
	    	$item_code 				= $request->itemCode;
	    	$item_name				= $request->itemName;
	    	$item_uom				= $request->itemUom;
	    	$item_subcategory		= $request->itemSubcategory;
	    	$item_category			= $request->itemCategory;

	    	if($date):
	    		if ($request->by == "weekly"):
				    $startDate 	= date("Y-m-d", strtotime('monday this week'))." 00:00:00";
					$endDate 	= date("Y-m-d", strtotime('sunday this week'))." 23:59:59";
				elseif ($request->by == "monthly"): 
					$startDate 	= date('Y-m-01')." 00:00:00";
					$date 		= date('Y-m-d');
					$endDate 	= date("Y-m-t", strtotime($date))." 23:59:59";
				elseif ($request->by == "yearly"):
					$startDate 	= date('Y-01-01')." 00:00:00";
					$date 		= date('Y-m-d');
					$endDate 	= date("Y-12-t", strtotime($date))." 23:59:59";
				elseif ($request->by == "daily" ):
					$day 		= new \DateTime();
					$startDate 	= $day->format('Y-m-d')." 00:00:00";
					$endDate 	= $day->format('Y-m-d')." 23:59:59";
				else:
					$startDate 	= null;
					$endDate 	= null;
				endif;

				$invoices = DB::table('invoices')
								->select('item_id','menus.name as name','menu_uom_price.uom as uom_name','categories.name as cat_name','subcategories.name as sub_name',
									DB::raw('count(*) as count_qty')
								)
								->leftjoin('invoice_orders','invoice_orders.invoice_id','=','invoices.invoice_id')
								->leftjoin('menus','menus.id','=','invoice_orders.item_id')
								->leftjoin('categories','categories.id','=','menus.category_id')
								->leftjoin('subcategories','subcategories.id','=','menus.subcategory_id')
								->leftjoin('menu_uom_price','menu_uom_price.menu_id','=','invoice_orders.item_id')
								->leftjoin('settings','settings.id','=','invoice_orders.company_id')
							->groupBy('item_id')->groupBy('menus.name')->groupBy('menu_uom_price.uom')->groupBy('categories.name')->groupBy('subcategories.name')
							->OrderBy('menus.name','ASC')
							->where('invoice_date','>=',$startDate)
							->where('invoice_date','<',$endDate)
							->where('void','=','0')
							->where('invoices.status','=',1)
							->where('invoices.company_id',$user->company_id)
							->paginate(20)->appends('by',$request->by);


	    	elseif($custom_date):
    			$date 		= explode(' - ',$custom_date);
    			$startDate	= $date[0];
    			$endDate 	= $date[1];

    			$invoices = DB::table('invoices')
								->select('item_id','menus.name as name','menu_uom_price.uom as uom_name','categories.name as cat_name','subcategories.name as sub_name',
									DB::raw('count(*) as count_qty')
								)
								->leftjoin('invoice_orders','invoice_orders.invoice_id','=','invoices.invoice_id')
								->leftjoin('menus','menus.id','=','invoice_orders.item_id')
								->leftjoin('categories','categories.id','=','menus.category_id')
								->leftjoin('subcategories','subcategories.id','=','menus.subcategory_id')
								->leftjoin('menu_uom_price','menu_uom_price.menu_id','=','invoice_orders.item_id')
								->leftjoin('settings','settings.id','=','invoice_orders.company_id')
							->groupBy('item_id')->groupBy('menus.name')->groupBy('menu_uom_price.uom')->groupBy('categories.name')->groupBy('subcategories.name')
							->OrderBy('menus.name','ASC')
							->where('invoice_date','>=',$startDate)
							->where('invoice_date','<=',$endDate)
							->where('void','=','0')
							->where('invoices.status','=',1)
							->where('invoices.company_id',$user->company_id)
							->paginate(20)->appends('daterange',$request->daterange);


	    	elseif($item_code):
	    		$invoices = DB::table('invoices')
								->select('item_id','menus.name as name','menu_uom_price.uom as uom_name','categories.name as cat_name','subcategories.name as sub_name',
									DB::raw('count(*) as count_qty')
								)
								->leftjoin('invoice_orders','invoice_orders.invoice_id','=','invoices.invoice_id')
								->leftjoin('menus','menus.id','=','invoice_orders.item_id')
								->leftjoin('categories','categories.id','=','menus.category_id')
								->leftjoin('subcategories','subcategories.id','=','menus.subcategory_id')
								->leftjoin('menu_uom_price','menu_uom_price.menu_id','=','invoice_orders.item_id')
								->leftjoin('settings','settings.id','=','invoice_orders.company_id')
							->groupBy('item_id')->groupBy('menus.name')->groupBy('menu_uom_price.uom')->groupBy('categories.name')->groupBy('subcategories.name')
							->OrderBy('menus.name','ASC')
							->where('menus.menu_code','=',$item_code)
							->where('void','=','0')
							->where('invoices.status','=',1)
							->where('invoices.company_id',$user->company_id)
							->paginate(20)->appends('itemCode',$item_code);

			elseif($item_name && $item_uom == ""):
				$invoices = DB::table('invoices')
								->select('item_id','menus.name as name','menu_uom_price.uom as uom_name','categories.name as cat_name','subcategories.name as sub_name',
									DB::raw('count(*) as count_qty')
								)
								->leftjoin('invoice_orders','invoice_orders.invoice_id','=','invoices.invoice_id')
								->leftjoin('menus','menus.id','=','invoice_orders.item_id')
								->leftjoin('categories','categories.id','=','menus.category_id')
								->leftjoin('subcategories','subcategories.id','=','menus.subcategory_id')
								->leftjoin('menu_uom_price','menu_uom_price.menu_id','=','invoice_orders.item_id')
								->leftjoin('settings','settings.id','=','invoice_orders.company_id')
							->groupBy('item_id')->groupBy('menus.name')->groupBy('menu_uom_price.uom')->groupBy('categories.name')->groupBy('subcategories.name')
							->OrderBy('menus.name','ASC')
							->where('invoice_orders.item_id','=',$item_name)
							->where('void','=','0')
							->where('invoices.status','=',1)
							->where('invoices.company_id',$user->company_id)
							->paginate(20)->appends('itemName',$item_name);

			elseif($item_name && $item_uom):
				$invoices = DB::table('invoices')
								->select('item_id','menus.name as name','menu_uom_price.uom as uom_name','categories.name as cat_name','subcategories.name as sub_name',
									DB::raw('count(*) as count_qty')
								)
								->leftjoin('invoice_orders','invoice_orders.invoice_id','=','invoices.invoice_id')
								->leftjoin('menus','menus.id','=','invoice_orders.item_id')
								->leftjoin('categories','categories.id','=','menus.category_id')
								->leftjoin('subcategories','subcategories.id','=','menus.subcategory_id')
								->leftjoin('menu_uom_price','menu_uom_price.menu_id','=','invoice_orders.item_id')
								->leftjoin('settings','settings.id','=','invoice_orders.company_id')
							->groupBy('item_id')->groupBy('menus.name')->groupBy('menu_uom_price.uom')->groupBy('categories.name')->groupBy('subcategories.name')
							->OrderBy('menus.name','ASC')
							->where('invoice_orders.item_id','=',$item_name)
							->where('menu_uom_price.id','=',$item_uom)
							->where('void','=','0')
							->where('invoices.status','=',1)
							->where('invoices.company_id',$user->company_id)
							->paginate(20)->appends('itemName',$item_name)->appends('itemUom',$item_uom);

			elseif($item_subcategory):
				$invoices = DB::table('invoices')
								->select('item_id','menus.name as name','menu_uom_price.uom as uom_name','categories.name as cat_name','subcategories.name as sub_name',
									DB::raw('count(*) as count_qty')
								)
								->leftjoin('invoice_orders','invoice_orders.invoice_id','=','invoices.invoice_id')
								->leftjoin('menus','menus.id','=','invoice_orders.item_id')
								->leftjoin('categories','categories.id','=','menus.category_id')
								->leftjoin('subcategories','subcategories.id','=','menus.subcategory_id')
								->leftjoin('menu_uom_price','menu_uom_price.menu_id','=','invoice_orders.item_id')
								->leftjoin('settings','settings.id','=','invoice_orders.company_id')
							->groupBy('item_id')->groupBy('menus.name')->groupBy('menu_uom_price.uom')->groupBy('categories.name')->groupBy('subcategories.name')
							->OrderBy('menus.name','ASC')
							->where('menus.subcategory_id','=',$item_subcategory)
							->where('void','=','0')
							->where('invoices.status','=',1)
							->where('invoices.company_id',$user->company_id)
							->paginate(20)->appends('itemSubcategory',$item_name);

			elseif($item_category):
				$invoices = DB::table('invoices')
								->select('item_id','menus.name as name','menu_uom_price.uom as uom_name','categories.name as cat_name','subcategories.name as sub_name',
									DB::raw('count(*) as count_qty')
								)
								->leftjoin('invoice_orders','invoice_orders.invoice_id','=','invoices.invoice_id')
								->leftjoin('menus','menus.id','=','invoice_orders.item_id')
								->leftjoin('categories','categories.id','=','menus.category_id')
								->leftjoin('subcategories','subcategories.id','=','menus.subcategory_id')
								->leftjoin('menu_uom_price','menu_uom_price.menu_id','=','invoice_orders.item_id')
								->leftjoin('settings','settings.id','=','invoice_orders.company_id')
							->groupBy('item_id')->groupBy('menus.name')->groupBy('menu_uom_price.uom')->groupBy('categories.name')->groupBy('subcategories.name')
							->OrderBy('menus.name','ASC')
							->where('menus.category_id','=',$item_category)
							->where('void','=','0')
							->where('invoices.status','=',1)
							->where('invoices.company_id',$user->company_id)
							->paginate(20)->appends('itemSubcategory',$item_name);

	    	else:
	    		$invoices = DB::table('invoices')
								->select('item_id','menus.name as name','menu_uom_price.uom as uom_name','categories.name as cat_name','subcategories.name as sub_name',
									DB::raw('count(*) as count_qty')
								)
								->leftjoin('invoice_orders','invoice_orders.invoice_id','=','invoices.invoice_id')
								->leftjoin('menus','menus.id','=','invoice_orders.item_id')
								->leftjoin('categories','categories.id','=','menus.category_id')
								->leftjoin('subcategories','subcategories.id','=','menus.subcategory_id')
								->leftjoin('menu_uom_price','menu_uom_price.menu_id','=','invoice_orders.item_id')
							->leftjoin('settings','settings.id','=','invoice_orders.company_id')
							->groupBy('item_id')->groupBy('menus.name')->groupBy('menu_uom_price.uom')->groupBy('categories.name')->groupBy('subcategories.name')
							->OrderBy('menus.name','ASC')
							->paginate(20);
	    	endif;

	    	$data['invoices'] = $invoices;
	    	$this->cbView('report.sale_by_item',$data);
		}

		public function get_uom_menu(Request $request){
			$myID 					= CRUDBooster::myId();
	    	$user					= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
			$item_id 				= $request->item_name;
			$item_uom 				= $request->item_uom;
			$uom 	 				= DB::table('menus')->rightjoin('menu_uom_price','menu_uom_price.menu_id','=','menus.id')
										->where('menu_id',$item_id)
										->where('company_id',$user->company_id)->get();
			if($item_id != null): ?>

				<option value="">**Please Select item's uom</option>
				<?php foreach($uom as $uoms): ?>
					<option value="<?php echo $uoms->id; ?>" <?php if($item_uom == $uoms->id):  echo 'selected'; endif;?>><?php echo $uoms->uom; ?></option>
				<?php endforeach; ?>
			<?php endif;
		}

}