<?php namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use Carbon\Carbon;
use URL;
use DB;
use CRUDBooster;
use App\Menu;
use Image;


class AdminMenusController extends \crocodicstudio\crudbooster\controllers\CBController
{

    public function cbInit()
    {
            $myID       = CRUDBooster::myId();
            $user       = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field           = "name";
			$this->limit                 = "20";
			$this->orderby               = "id,desc";
			$this->global_privilege      = false;
			$this->button_table_action   = true;
			$this->button_bulk_action    = true;
			$this->button_action_style   = "button_icon";
			$this->button_add            = true;
			$this->button_edit           = true;
			$this->button_delete         = true;
			$this->button_detail         = true;
			$this->button_show           = false;
			$this->button_filter         = false;
			$this->button_import         = false;
			$this->button_export         = false;
			$this->table                 = "menus";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col   = [];
			$this->col[] = ["label"=>"Name","name"=>"name","width"=>"80"];
			$this->col[] = ["label"=>"Photo","name"=>"photo","image"=>true,"width"=>"80"];
			$this->col[] = ["label"=>"Price","name"=>"price","width"=>"80"];
            $this->col[] = ["label"=>"Currency","name"=>"currency","width"=>"80","join" => "currencies,currency_symbol"];
			$this->col[] = ["label"=>"Category Name","name"=>"category_id","join"=>"categories,name","width"=>"120"];
            $this->col[] = ["label"=>"Subcategory Name","name"=>"subcategory_id","join"=>"subcategories,name","width"=>"140"];
			$this->col[] = ["label"=>"Status","name"=>"status","width"=>"80"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
            # Not User
			$this->form     = [];
            $this->form[]   = ['label'=>'Category Name','name'=>'category_id','type'=>'select','validation'=>'required|integer|min:0','width'=>'col-sm-9','datatable'=>'categories,name','datatable_where'=>'company_id =' . $user->company_id];
            $this->form[] = ['label'=>'Sub Category Name','name'=>'subcategory_id','type'=>'select','validation'=>'integer','width'=>'col-sm-9','datatable'=>'subcategories,name','datatable_where'=>'created_by =' . $user->company_id,'parent_select'=>'category_id'];
			$this->form[] = ['label'=>'Menu Name','name'=>'name','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
            $this->form[] = ['label'=>'Description','name'=>'description','type'=>'textarea','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Photo','name'=>'photo','type'=>'upload','validation'=>'image','width'=>'col-sm-10','help'=>'File types support : JPG, JPEG, PNG, GIF, BMP'];
			$this->form[] = ['label'=>'Price','name'=>'price','type'=>'money','validation'=>'required|integer|min:0','width'=>'col-sm-10'];

			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Name','name'=>'name','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
			//$this->form[] = ['label'=>'Photo','name'=>'photo','type'=>'upload','validation'=>'image','width'=>'col-sm-10','help'=>'File types support : JPG, JPEG, PNG, GIF, BMP'];
			//$this->form[] = ['label'=>'Price','name'=>'price','type'=>'money','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Description','name'=>'description','type'=>'textarea','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Category Name','name'=>'category_id','type'=>'select','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'categories,name','datatable_where'=>'created_by ='];
			//$this->form[] = ['label'=>'Sub category','name'=>'subcategory_id','type'=>'select','width'=>'col-sm-10','datatable'=>'subcategories,name','datatable_where'=>'created_by =','parent_select'=>'category_id'];
			//$this->form[] = ['label'=>'uom','name'=>'uom','type'=>'select','validation'=>'required','width'=>'col-sm-9','datatable'=>'uom,name','datatable_where'=>'created_by ='];
			# OLD END FORM

			/*
    | ----------------------------------------------------------------------
    | Sub Module
    | ----------------------------------------------------------------------
    | @label          = Label of action
    | @path           = Path of sub module
    | @foreign_key 	  = foreign key of sub table/module
    | @button_color   = Bootstrap Class (primary,success,warning,danger)
    | @button_icon    = Font Awesome Class
    | @parent_columns = Sparate with comma, e.g : name,created_at
    |
    */
        $this->sub_module = array();

        /*
        | ----------------------------------------------------------------------
        | Add More Action Button / Menu
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
        | @icon        = Font awesome class icon. e.g : fa fa-bars
        | @color 	   = Default is primary. (primary, warning, succecss, info)
        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
        |
        */
        $this->addaction = array();
        $this->addaction[] = ['icon'=>'fa fa-clone','color'=>'success','url'=>CRUDBooster::mainpath('menu_duplicate').'/[id]' ];
        $this->addaction[] = ['label' => 'UOM'  ,'icon'=>'fa fa-plus','color'=>'success','url'=>CRUDBooster::mainpath('menu_uom_index').'/[id]' ];
        $this->addaction[] = ['label' => 'Addon','icon'=>'fa fa-plus','color'=>'success','url'=>CRUDBooster::mainpath('menu_addon_index').'/[id]' ];

        /*
        | ----------------------------------------------------------------------
        | Add More Button Selected
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @icon 	   = Icon from fontawesome
        | @name 	   = Name of button
        | Then about the action, you should code at actionButtonSelected method
        |
        */
        $this->button_selected = array();


        /*
        | ----------------------------------------------------------------------
        | Add alert message to this module at overheader
        | ----------------------------------------------------------------------
        | @message = Text of message
        | @type    = warning,success,danger,info
        |
        */
        $this->alert = array();


        /*
        | ----------------------------------------------------------------------
        | Add more button to header button
        | ----------------------------------------------------------------------
        | @label = Name of button
        | @url   = URL Target
        | @icon  = Icon from Awesome.
        |
        */
        $this->index_button = array();


        /*
        | ----------------------------------------------------------------------
        | Customize Table Row Color
        | ----------------------------------------------------------------------
        | @condition = If condition. You may use field alias. E.g : [id] == 1
        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
        |
        */
        $this->table_row_color = array();


        /*
        | ----------------------------------------------------------------------
        | You may use this bellow array to add statistic at dashboard
        | ----------------------------------------------------------------------
        | @label, @count, @icon, @color
        |
        */
        $this->index_statistic = array();


        /*
        | ----------------------------------------------------------------------
        | Add javascript at body
        | ----------------------------------------------------------------------
        | javascript code in the variable
        | $this->script_js = "function() { ... }";
        |
        */
        $this->script_js = NULL;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code before index table
        | ----------------------------------------------------------------------
        | html code to display it before index table
        | $this->pre_index_html = "<p>test</p>";
        |
        */
        $this->pre_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code after index table
        | ----------------------------------------------------------------------
        | html code to display it after index table
        | $this->post_index_html = "<p>test</p>";
        |
        */
        $this->post_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include Javascript File
        | ----------------------------------------------------------------------
        | URL of your javascript each array
        | $this->load_js[] = asset("myfile.js");
        |
        */
        // $this->load_js[] = asset("js/menus.js");
        $this->load_js[]    = asset("js/disable_row.js");
        /*
        | ----------------------------------------------------------------------
        | Add css style at body
        | ----------------------------------------------------------------------
        | css code in the variable
        | $this->style_css = ".style{....}";
        |
        */
        $this->style_css = null;


        /*
        | ----------------------------------------------------------------------
        | Include css File
        | ----------------------------------------------------------------------
        | URL of your css each array
        | $this->load_css[] = asset("myfile.css");
        |
        */
        $this->load_css     = array();
        $this->load_css[]   = asset('css/no_data.css');
        $this->load_css[]   = asset('css/img_display_fit.css');
    }


    /*
    | ----------------------------------------------------------------------
    | Hook for button selected
    | ----------------------------------------------------------------------
    | @id_selected = the id selected
    | @button_name = the name of button
    |
    */
    public function actionButtonSelected($id_selected, $button_name)
    {
        //Your code here

    }


    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate query of index result
    | ----------------------------------------------------------------------
    | @query = current sql query
    |
    */
    public function hook_query_index(&$query)
    {   
        $myID = CRUDBooster::myId();
        $user = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $query->where('menus.company_id', $user->company_id);
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate row of index table html
    | ----------------------------------------------------------------------
    |
    */
    public function hook_row_index($column_index, &$column_value)
    {
        //Your code here
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before add data is execute
    | ----------------------------------------------------------------------
    | @arr
    |
    */
    public function hook_before_add(&$postdata)
    {   

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after add public static function called
    | ----------------------------------------------------------------------
    | @id = last insert id
    |
    */
    public function hook_after_add($id)
    {   


    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before update data is execute
    | ----------------------------------------------------------------------
    | @postdata = input post data
    | @id       = current id
    |
    */
    public function hook_before_edit(&$postdata, $id)
    {
       $postdata['updated_at'] = $now;
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after edit public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_edit($id)
    {
        //Your code here
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command before delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_before_delete($id)
    {
        //Your code here
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_delete($id)
    {
        DB::table('menu_detail')->where('menu_id',$id)->delete();
        DB::table('menu_uom_price')->where('menu_id',$id)->delete();
    }

    // ======================== Create Menu (View) ============================
    public function getAdd() {

        $myID                       = CRUDBooster::myId();
        $user                       = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
           
        $data                       = [];
        $data['page_title']         = trans('menumanagement.Menu');
        $data['categories']         = DB::table('categories')->where('company_id',$user->company_id)->get();
        $data['subcategories']      = DB::table('subcategories')->where('company_id',$user->company_id)->get();
        $data['uoms']               = DB::table('uom')->where('company_id',$user->company_id)->get();
        $data['materials']          = DB::table('menu_materials')->where('company_id',$user->company_id)->get();
        $data['company_currency']   = DB::table('settings')->leftjoin('currencies','currencies.id','=','settings.company_currency')
                                    ->select('currencies.*','settings.company_currency')->where('settings.id',$user->company_id)->first();
        $data['command']            = "add";
        $this->cbView('menu_management.menu.menus',$data);

    }

    // ======================== Update Menu (View) ============================
    public function getEdit($id) {

        $myID = CRUDBooster::myId();
        $user = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        
        $data                   =   [];
        $data['url']            =   URL::to("/");
        $data['page_title']     =   trans('menumanagement.Menu');
        $data['menus']          =   DB::table('menus')
                                        ->leftjoin('categories','categories.id','=','menus.category_id')
                                        ->leftjoin('currencies','currencies.id','=','menus.currency')
                                        ->select('menus.id              as menu_id',
                                                 'menus.name            as menu_name',
                                                 'menus.menu_code       as menu_code',
                                                 'menus.description     as menu_description',
                                                 'menus.photo           as menu_photo',
                                                 'menus.price           as menu_price',
                                                 'menus.category_id     as category_id',
                                                 'menus.subcategory_id  as subcategory_id',
                                                 'currencies.*'   )
                                        ->where('menus.company_id',$user->company_id)
                                        ->where('menus.id',$id)->first();

        $data['categories']     =   DB::table('categories')->where('company_id',$user->company_id)->get();
        $data['subcategories']  =   DB::table('subcategories')->where('company_id',$user->company_id)->get();
        $data['uoms']           =   DB::table('uom')->where('company_id',$user->company_id)->get();
        $data['materials']      =   DB::table('menu_materials')->where('company_id',$user->company_id)->get();
        $data['menu_details']   =   DB::table('menu_detail')->where('menu_id',$id)->get();
        $data['menu_uom_price'] =   DB::table('menu_uom_price')->where('menu_id',$id)->get();
        $data['company_currency']   = DB::table('settings')->leftjoin('currencies','currencies.id','=','settings.company_currency')
                                    ->select('currencies.*','settings.company_currency')->where('settings.id',$user->company_id)->first();
        $data['command']            = "update";

        $this->cbView('menu_management.menu.menus',$data);
    }

    // ========================= View Menu Detail (View) ============================
    public function getDetail($id){
        $myID           = CRUDBooster::myId();
        $user           = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $data           = [];
        $data['menu']   = DB::table('menus')
                        ->select('menus.*','categories.name as category_name','subcategories.name as subcategory_name')
                        ->leftjoin('categories','categories.id','=','menus.category_id')
                        ->leftjoin('subcategories','subcategories.id','=','menus.subcategory_id')
                        ->where('menus.id',$id)->where('menus.company_id',$user->company_id)->first();
        $data['uoms']   = DB::table('menu_uom_price')->where('menu_id',$id)->get();
        $data['addons'] = DB::table('menu_detail')
                        ->leftjoin('menu_materials','menu_materials.id','=','menu_detail.material_id')
                        ->where('menu_id',$id)
                        ->get();
        $data['company_currency']   = DB::table('settings')->leftjoin('currencies','currencies.id','=','settings.company_currency')
                                    ->select('currencies.*')->where('settings.id',$user->company_id)->first();
                    
        $this->cbView('menu_management.menu.menus_detail',$data);
    }

    // ======================== Create Menu (Function) ============================
    public function AddMenu(Request $request){
        
        $myID                   = CRUDBooster::myId();

        $user                   = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $now                    = Carbon::now();
        $getCompanyCurrency     = DB::table('settings')->where('id',$user->company_id)->select('company_currency')->first();
        $photo                  = $request->file('menu_photo');
        $photo_db               = "uploads/client_photo/". time() . '.' . $photo->getClientOriginalExtension();
        $photo_upload           = time() . '.' . $photo->getClientOriginalExtension();
        Image::make($photo)->fit(136,131)->save('../storage/app/uploads/client_photo/' . $photo_upload);
        
        // Check Existing Menu Code 
        if(!$request->menu_code):
        $checkMenuCode = DB::table('menus')->whereMenuCode($request->menu_code)->first();
        if ($checkMenuCode):
            return CRUDBooster::redirect($_SERVER['HTTP_REFERER'], trans("crudbooster.alert_duplicate_data"), "warning");        
        endif;
        endif;

        $dataSet = [
                    'category_id'           => $request->category,
                    'subcategory_id'        => $request->subcategory,
                    'name'                  => $request->menu,
                    'menu_code'             => $request->menu_code,
                    'description'           => $request->menu_description,
                    'photo'                 => $photo_db,
                    'price'                 => $request->menu_price,
                    'status'                => '1',
                    'currency'              => $getCompanyCurrency->company_currency,
                    'created_by'            => $myID,
                    'company_id'            => $user->company_id
                    ]; 
        DB::table('menus')->insert($dataSet);

        if(strtolower($submit) == strtolower(trans('crudbooster.button_save'))):
            CRUDBooster::redirect(CRUDBooster::mainpath(), trans("crudbooster.alert_add_data_success"), 'success');
        else:
            CRUDBooster::redirect($_SERVER['HTTP_REFERER'], trans("crudbooster.alert_add_data_success"), 'success');
        endif;
    }

    // ======================== Update Menu (Function) ============================
    public function UpdateMenus(Request $request)
    {
        $now                    = Carbon::now();
        $myID                   = CRUDBooster::myId();
        $user                   = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $getCompanyCurrency     = DB::table('settings')->where('id',$user->company_id)->select('company_currency')->first();
        $menu_id                = $request->menu_id;
        $image                   = DB::table('menus')->where('id',$menu_id)->first();
            if($image->photo != "" || $image->photo != null):
                $menu_image = DB::table('menus')->select('photo')->where('id',$menu_id)->first();
                $photo_db = $menu_image->photo;
            else:
                $photo                  = $request->file('menu_photo');
                $photo_db               = "uploads/client_photo/". time() . '.' . $photo->getClientOriginalExtension();
                $photo_upload           = time() . '.' . $photo->getClientOriginalExtension();
                Image::make($photo)->fit(136,131)->save('../storage/app/uploads/client_photo/' . $photo_upload);
            endif;

        if(!$request->menu_code):
        $checkMenuCode = DB::table('menus')->where('id', '<>', $menu_id)->whereMenuCode($request->menu_code)->first();
        if ($checkMenuCode):
            return CRUDBooster::redirect($_SERVER['HTTP_REFERER'], trans("crudbooster.alert_duplicate_data"), "warning");
        endif;
        endif;

        $dataSet = [
                    'category_id'           => $request->category,
                    'subcategory_id'        => $request->subcategory,
                    'name'                  => $request->menu,
                    'menu_code'             => $request->menu_code,
                    'description'           => $request->menu_description,
                    'photo'                 => $photo_db,
                    'price'                 => $request->menu_price,
                    'status'                => '1',
                    'currency'              => $getCompanyCurrency->company_currency,
                    'company_id'            => $user->company_id
                    ]; 
        DB::table('menus')->where('id',$menu_id)->update($dataSet);
        CRUDBooster::redirect($_SERVER['HTTP_REFERER'],trans('crudbooster.alert_update_data_success'),"info");
    }

    // ========================= Get all subcategory (function) ============================
    public function get_subcategory(Request $request){
        $myID           = CRUDBooster::myId();
        $user           = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $category_id    = $request->category_id;;

        if($category_id != null):
            $subcategories = DB::table('subcategories')->select('subcategories.id','subcategories.name')
                            ->where('category_id',$category_id)
                            ->where('company_id',$user->company_id)->get();
        else:
            $subcategories = null;
        endif;
        ?>

        <option value="">Please select subcategory menu</option>
        <?php if($subcategories != null): ?>
        <?php foreach($subcategories as $subcategory): ?>
            <option value="<?php echo $subcategory->id; ?>"><?php echo $subcategory->name; ?></option>    
        <?php endforeach; ?>
        <?php endif;
    }


    // ========================= Create Category (function) ============================
    public function add_category_menu_via_menu_page(Request $request){
        $myID   = CRUDBooster::myId();
        $user   = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();

        $category_name      = $request->name;
        $photo              = $request->file('photo');
        $photo_db           = "uploads/client_photo/". time() . '.' . $photo->getClientOriginalExtension();
        $photo_upload       = time() . '.' . $photo->getClientOriginalExtension();
        Image::make($photo)->fit(136,131)->save('../storage/app/uploads/client_photo/' . $photo_upload);
        
        $dataSet = [
                    'name'          =>  $category_name,
                    'photo'         =>  $photo_db,
                    'created_by'    =>  $myID,
                    'company_id'    =>  $user->company_id,
                    'status'        =>  1,
                    'created_at'    =>  now()
                    ]; 
        $data = DB::table('categories')->insert($dataSet);  
    }

    // ========================= Create Subcategory (function) ============================
    public function add_subcategory_menu_via_menu_page(Request $request){
        $myID   = CRUDBooster::myId();
        $user   = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $category_name          =   $request->category;
        $subcategory_name       =   $request->name;
        $photo                  = $request->file('photo');
        $photo_db               = "uploads/client_photo/". time() . '.' . $photo->getClientOriginalExtension();
        $photo_upload           = time() . '.' . $photo->getClientOriginalExtension();
        Image::make($photo)->fit(136,131)->save('../storage/app/uploads/client_photo/' . $photo_upload);
        
        $dataSet =  [
                        'name'          =>  $subcategory_name,
                        'category_id'   =>  $category_name,
                        'photo'         =>  $photo_db,
                        'created_by'    =>  $myID,
                        'company_id'    =>  $user->company_id,
                        'status'        =>  1,
                        'created_at'    =>  now()
                    ]; 
        
        $data = DB::table('subcategories')->insert($dataSet);
    }

    // ========================= Create UOM (function) ============================
    public function add_uom_via_menu_page(Request $request){
        $myID                   = CRUDBooster::myId();
        $user                   = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $name                   = $request->name;
        $validation             = DB::table('uom')->where('created_by',$myID)->where('name',$name)->get();
        $getCompanyCurrency     = DB::table('settings')->join('currencies','currencies.id','=','settings.company_currency')
                                    ->where('settings.id',$user->company_id)->select('currency_symbol')->first();
        $dataSet    = [
                'name'          =>  $name,
                'created_by'    =>  $myID,
                'company_id'    =>  $user->company_id,
                'status'        =>  1,
                'created_at'    =>  now()
        ]; 
            
        if(count($validation) < 1):
            $data = DB::table('uom')->insert($dataSet); ?> 
                    <div class="form-group header-group-0" id="form-group-uom">  
                            <label class="control-label col-sm-2">UOM
                            <span class="text-danger" title="This field is required">*</span></label>
                                <div class="col-sm-6">
                                   <input type="text" name="uom[]" class="form-control uom" id="uom" placeholder="Unit Of Measurement (UOM)" value="<?php echo $name;?>">
                                </div>
                                
                                <div class="col-sm-3">
                                <input type="number" step="any" class="form-control" name="uom_price[]" id="uom_price" required min="0"
                                placeholder= "Price* (<?php echo $getCompanyCurrency->currency_symbol; ?>)">
                                </div>
                                <span class="remove_uom_price" data-id="" title="Remove"><i class="glyphicon glyphicon-remove"></i></span>
                    </div> 
            <?php
         endif;
            
    }

    // ========================= Duplicate Menu (function) ============================
    public function menu_duplicate($id)
    {   
        $menu           = DB::table('menus')->where('id',$id)->first();
        $menu_materials = DB::table('menu_detail')
                        ->join('menus','menus.id','=','menu_detail.menu_id')
                        ->join('menu_materials','menu_materials.id','=','menu_detail.material_id')->where('menus.id',$id)->get();

        $menu_dataset = [
            'name'              => $menu->name,
            'photo'             => $menu->photo,
            'price'             => $menu->price,
            'description'       => $menu->description,
            'created_by'        => $menu->created_by,
            'company_id'        => $menu->company_id,
            'category_id'       => $menu->category_id,
            'subcategory_id'    => $menu->subcategory_id,
            'status'            => $menu->status,
            'created_at'        => $menu->created_at
        ];
        DB::table('menus')->insert($menu_dataset);

        $last_menu = DB::table('menus')->orderby('id','DESC')->first();
        foreach($menu_materials as $menu_material):
            $menu_detail_dataset = [
                'menu_id'           => $last_menu->id,
                'material_id'       => $menu_material->material_id,
            ];
            DB::table('menu_detail')->insert($menu_detail_dataset);
        endforeach;
        CRUDBooster::redirect($_SERVER['HTTP_REFERER'],trans("crudbooster.data_has_duplicate"),"info");
    }


    // ===================== (View) Add UOM to existing menu ========================
    public function menu_uom_index($id){
        $myID                       = CRUDBooster::myId();
        $user                       = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();

        $data['id']                 = $id;
        $data['menu']               = DB::table('menus')->select('name')->where('id',$id)->first();
        $data['page_title']         = trans('menumanagement.UOM');
        $data['uoms']               = DB::table('uom')->where('company_id',$user->company_id)->get();
        $data['command']            = "add";
        $this->cbView('menu_management.menu.uom',$data);
    }

    // ===================== (View) Add Addon to existing menu ======================
    public function menu_addon_index($id){
        $myID                       = CRUDBooster::myId();
        $user                       = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();

        $data['id']                 = $id;
        $data['menu']               = DB::table('menus')->select('name')->where('id',$id)->first();
        $data['addons']             = DB::table('menu_materials')->where('company_id',$user->company_id)->get();
        $data['page_title']         = trans('menumanagement.Addon');
        $data['command']            = "update";
        $this->cbView('menu_management.menu.addon',$data);
    }

    // ====================== Add Uom to existing menu =======================
    public function add_menu_uom(Request $request){
        $myID                = CRUDBooster::myId();
        $user                = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();

        $id                  = $request->id;
        DB::table('menu_uom_price')->where('menu_id',$id)->delete();


        $uom                 = $request->uom;
        $uom_length          = count($uom);
        for($i=0; $i<$uom_length; $i++):
            $i++;
            if($uom[$i] != 0):
                $menu_uom_dataset = [
                    'menu_id'   => $id,
                    'uom'       => $uom[$i-1],
                    'uom_price' => $uom[$i],
                    'status'    => '0',
                ];
            DB::table('menu_uom_price')->insert($menu_uom_dataset);
            endif;
        
        endfor;
        $check = DB::table('menu_uom_price')->where('menu_id',$id)->first();
        if($check):
            DB::table('menus')->where('id',$id)->update(['uom_size' => 1]);
        else:
            DB::table('menus')->where('id',$id)->update(['uom_size' => 0]);
        endif;
        CRUDBooster::redirect($_SERVER['HTTP_REFERER'],trans('crudbooster.alert_update_data_success'),"info");
    }

    // ====================== Add Addon to existing menu =======================
    public function add_menu_addon(Request $request){
        $myID                = CRUDBooster::myId();
        $user                = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();

        $id                  = $request->id;
        $addon               = $request->addon;
        $now                 = Carbon::now();
        DB::table('menu_detail')->where('menu_id',$id)->delete();

        if($addon != null):
            foreach($addon as $a):
                    $menu_addon_dataset = [
                        'menu_id'           => $id,
                        'material_id'       => $a,
                        'created_at'        => $now,
                        'updated_at'        => $now,
                    ];
                DB::table('menu_detail')->insert($menu_addon_dataset);
            endforeach;
        endif;
        $check = DB::table('menu_detail')->where('menu_id',$id)->first();
        if($check):
            DB::table('menus')->where('id',$id)->update(['material_size' => 1]);
        else:
            DB::table('menus')->where('id',$id)->update(['material_size' => 0]);
        endif;


        CRUDBooster::redirect($_SERVER['HTTP_REFERER'],trans('crudbooster.alert_update_data_success'),"info");
    }

}