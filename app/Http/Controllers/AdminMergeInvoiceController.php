<?php 
namespace App\Http\Controllers;

	use Session;
	use Illuminate\Http\Request;
	use DB;
	use CRUDBooster;
	use Carbon\Carbon;

class AdminMergeInvoiceController extends \crocodicstudio\crudbooster\controllers\CBController
{
	public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "id";
			$this->limit = "20";
			$this->orderby = "invoice_id,desc";
			$this->global_privilege = false;
			$this->button_table_action = false;
			$this->button_bulk_action = false;
			$this->button_action_style = "button_icon";
			$this->button_add = false;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = true;
			$this->button_show = false;
			$this->button_filter = false;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "invoices";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];

			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];

			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        $this->load_js[] = asset('js/print/invoice.js');
	        $this->load_js[] = asset('js/print/print_script.js');
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        // $this->load_css[] = asset("css/button_show_invoice.css");
	        $this->load_css[] = asset("css/table.css");
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	            
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }

      public function getIndex()
	    {
	    	
	    	$myID = CRUDBooster::myId();
	    	$data = [];
	    	$data['page_title'] = "merge payment";


	    	$this->cbView('admin.merge',$data);
	    }


	    public function merge_sort_invoice(Request $request)
	    {
	    	$myID = CRUDBooster::myId();

	    	if ($request->by == "Weekly") {
			    $startDate = date("Y-m-d", strtotime('monday this week'))." 00:00:00";
				$endDate = date("Y-m-d", strtotime('sunday this week'))." 23:59:59";
							
			

			}elseif ($request->by == "Monthly" ) {
				
				$startDate = date('Y-m-01')." 00:00:00";
				$date = date('Y-m-d');
				$endDate = date("Y-m-t", strtotime($date))." 23:59:59";

			
				
			}elseif ($request->by == "Yearly") {
				$startDate = date('Y-01-01')." 00:00:00";
				$date = date('Y-m-d');
				$endDate = date("Y-12-t", strtotime($date))." 23:59:59";

				
			}elseif ($request->by == "Daily" ){
				
				$day = new \DateTime();
				$startDate = $day->format('Y-m-d')." 00:00:00";
				$endDate = $day->format('Y-m-d')." 23:59:59";

			}else{ 
				$request->date;
				$date = explode(" - ",$request->date);
				$startDate = $date[0]." 00:00:00";
				$endDate = $date[1]." 23:59:59";

				if ($startDate == $endDate) {
					$startDate = $date[0]." 00:00:00";
					$endDate = $date[1]." 23:59:59";


				}
				
			}
			

			$i=1;
			
			
			 $invoices = DB::select("SELECT * FROM invoices WHERE invoice_date >= '$startDate'
				AND invoice_date < '$endDate' AND void=0 AND invoices.Is_refund=0 AND status=1 AND created_by = $myID Order By invoice_id DESC");


		

			if(count($invoices)>=1){
				foreach ($invoices as $key_invoice => $invoice) { 
				$totalPrice = 0;
				$totalQty = 0;

				$invoice_details = DB::select("SELECT total, amount FROM orders
				LEFT JOIN orders_detail ON orders.id = orders_detail.orders_id
				INNER JOIN invoices ON invoices.orders_id = orders_detail.orders_id
				INNER JOIN menus ON orders_detail.menus_id = menus.id
				WHERE orders.id = '$invoice->orders_id'
				AND menus.created_by = $myID
				AND invoices.Is_refund=0
				AND orders_detail.Is_refund=0
				");
				

				foreach ($invoice_details as $key_invoice_detail => $invoice_detail) { 
					$totalPrice = $invoice_detail->total;
					$totalQty += $invoice_detail->amount;
					$Total_Detail += $invoice_detail->subtotal;

					 } ?>
				

					<tr>
						<td class="col-md-1"><input type="checkbox" class="check_box"> <?php echo $i++; ?></td>
						<td class="col-md-2"><?php echo $invoice->invoice_id; ?></td>
						<td><?php echo $invoice->invoice_date;?></td>
						<td><?php echo "$".$totalPrice; ?></td>
						<td><?php echo $totalQty; ?></td>e
						<td>
							<a href="#" class="merge_button_show_invoice" data-val="<?php echo $invoice->invoice_id; ?>"><?php echo "View Invoice" ?></a> 
							<input type="hidden" id="invoiceId" value="<?php echo $invoice->invoice_id; ?>">
							
						</td>
					</tr>


			    <?php }
			}

	    }
	    
	    public function merge_search_invoice_byid(Request $request)
	    {
	    	$myID = CRUDBooster::myId();
	    	$id=$request->id;

	    	$i=1;
	    	$invoices = DB::select("SELECT * FROM invoices WHERE invoice_id='$id' AND void=0 AND status=1 AND created_by = $myID Order By invoice_id DESC");

	    	if(count($invoices)>=1){
				foreach ($invoices as $key_invoice => $invoice) { 
				$totalPrice = 0;
				$totalQty = 0;

				$invoice_details = DB::select("SELECT total, amount FROM orders
				LEFT JOIN orders_detail ON orders.id = orders_detail.orders_id
				INNER JOIN invoices ON invoices.orders_id = orders_detail.orders_id
				INNER JOIN menus ON orders_detail.menus_id = menus.id
				WHERE orders.id = '$invoice->orders_id'
				AND menus.created_by = $myID
				AND invoices.Is_refund=0
				AND orders_detail.Is_refund=0
				");

				

				foreach ($invoice_details as $key_invoice_detail => $invoice_detail) { 
					$totalPrice = $invoice_detail->total;
					$totalQty += $invoice_detail->amount;
					$Total_Detail += $invoice_detail->subtotal;

					 } ?>
				

					<tr>
						<td class="col-md-1"><input type="checkbox"><?php echo $i++; ?></td>
						<td class="col-md-2"><?php echo $invoice->invoice_id; ?></td>
						<td><?php echo $invoice->invoice_date;?></td>
						<td><?php echo "$".$totalPrice; ?></td>
						<td><?php echo $totalQty; ?></td>
						<td>
							<a href="#" class="merge_button_show_invoicebyid" data-val="<?php echo $invoice->invoice_id; ?>" data-target="#order_confirm_modal"><?php echo "View Invoice" ?></a> 

							<input type="hidden" id="invoiceId" value="<?php echo $invoice->invoice_id; ?>">
							
						</td>
					</tr>

			    <?php }
			    ?>
					<button class="btn btn-default">Merge</button>
				
			    <?php

			}
	    }



	   //insert menu into table
	public function merge_insert_order(Request $request){
		$invoice_id=$request->invoice_id;
		$myID 			= CRUDBooster::myId();
		$adminID 		= DB::table('cms_users')->select('created_by')->where('id',$myID)->first();

	    $setting = DB::table('settings')
							->leftjoin('countries','countries.id','=','settings.company_country')
							->leftjoin('currencies','currencies.id','=','settings.company_currency')
							->where('settings.created_by',$adminID->created_by)
							->first();

	   $last_insert_invoice = DB::table('invoices')
						->join('orders','orders.id','=','invoices.orders_id')
						->select('invoices.*','orders.*')
						->where('invoices.created_by',$myID)
						->where('invoices.invoice_id',$invoice_id)
						->orderby('id','DESC')->limit(1)
						->first();

 		 $printing_orders = DB::table("orders")
					->join('orders_detail','orders_detail.orders_id','=','orders.id')
					->leftjoin('menus','menus.id','=','menus_id')
					->leftjoin('menu_materials','menu_materials.id','=','material_id')
					->leftjoin('menu_uom_price','menu_uom_price.id','=','orders_detail.menus_size')
					->select('menus.*','menu_materials.*','orders.*','orders_detail.*','menus.name as menu_name','orders_detail.price as order_menu_price','menu_materials.name as menu_material_name','menu_uom_price.*')
					->where('orders.created_by',$myID)
					->where('orders_detail.Is_refund',0)
					->where('orders.id',$last_insert_invoice->orders_id)
					/*->where('invoices.invoice_id',$invoice_id)*/
					->orderby('orders.id','DESC')->get();
		$exchange_rate_tokh= DB::table('exchange')
					->select('exchange.*')
					->where('currency_from_text','=','Dollar')
					->where('created_by',$adminID->created_by)
					->first();
		?>

			
<div class="main_invoice">
<div id="container">
    <section id="memo">
        <div class="logo">
          <img data-logo="company_logo" src='<?php echo $setting->company_logo; ?>' />
        </div>
        
        <div class="company-info">
          <div><?php echo $setting->company_name;?></div>

          <br/>
          
          <span><?php echo $setting->company_address;?></span>
          <span><?php echo $setting->country_name;?></span>

          <br/>
          
          <span><?php echo $setting->company_contact;?></span>
          <br/>
          <span><?php echo $setting->company_phone;?></span>
          <br/>	
          <span><?php echo $setting->company_email;?></span>

        </div>

      </section>
       <section id="invoice-title-number">
      
        <span id="title">Invoice</span>
        <span id="number"><?php echo $last_insert_invoice->order_number;?></span>
        
      </section>
      <div class="clearfix"></div>
      
      <section id="items">
        
        <table cellpadding="0" cellspacing="0">
        <thead>
		          <tr>
		            <th>No</th>
		            <th>Description</th>
		            <th>Quantity</th>
		            <th>Price</th>
		            <th>Subtotal</th>
		            
		          </tr>
          </thead>
          <tbody id="show_order">
          <?php foreach($printing_orders as $key => $order): ?>
		
	          <tr data-iterate="item" class="invoice" data-id="<?php echo $order->menus_id; ?>" data-refund="0">
	             <!-- Don't remove this column as it's needed for the row commands -->
	            <?php if($order->material_id):?>
	            <td></td>
	            <td class="material"><?php echo $order->menu_material_name; ?></td>
	            <?php else: ?>	
	            <td><?php echo $key+1; ?></td>
	            <td><?php echo $order->menu_name; ?>
	            		<br/>
	  					<?php if($order->menus_size){ ?>
	            		<span class="m_size_name">(<?php echo $order->uom; ?>)</span>
	            <?php }	 ?>
	            <?php endif;?>

	            

	            </td>
	            <td><?php echo $order->amount; ?></td>
	            <td class="price_col"><?php echo $setting->currency_symbol; echo number_format($order->order_menu_price, 2); ?></td>
	            <td class="subtotal_col"><?php echo $setting->currency_symbol; echo number_format($order->subtotal, 2);?> <i class="fa fa-close cross" data-changecurrency="<?php echo $exchange_rate_tokh->price_to; ?>" style="font-size:20px;color:red"></i></td>
	          </tr>
		
      	  <?php endforeach; ?>
      	  </tbody>
          
        </table>
        
      </section>

       <section id="sums">
      
        <table cellpadding="0" cellspacing="0">
          <tr>
            <th>SubTotal :</th>
            <td id="subtotal"><?php echo $setting->currency_symbol; echo $last_insert_invoice->total; ?></td>
          </tr>
          
          <tr data-iterate="tax">
            <th>Tax :</th>
            <td><?php echo $last_insert_invoice->tax_amount. "%"; ?></td>
            <input type="hidden" id="txt_tax_amount" value="<?php echo $last_insert_invoice->tax_amount ?>">
          </tr>

          <tr data-iterate="discount">
            <th>discount :</th>
            <td><?php 
            		if($last_insert_invoice->discount_amount):
            		echo $last_insert_invoice->discount_amount; echo $last_insert_invoice->discount_type;
            		else:
            		echo "0";
            		endif; ?></td>
          </tr>
         			
         	<!-- total group -->
	          <tr class="amount-total">
	            <th>Total ($):</th>
	            <td id="order_total_data_dollar"><?php echo $setting->currency_symbol; echo $last_insert_invoice->total_with_tax_discount;?></td>
	          </tr>
	           <tr class="amount-total">
	            <th>Total (riel) :</th>
	            <td id="order_total_data_riel"><?php echo $setting->currency_symbol; echo $last_insert_invoice->total_with_tax_discount * $exchange_rate_tokh->price_to;?></td>
	          </tr>
	           <tr class="amount-total">
	            <th>Total (bath):</th>
	            <td id="order_total_data_bath"><?php echo $setting->currency_symbol; echo $last_insert_invoice->total_with_tax_discount;?></td>
	          </tr>
          
          <!-- You can use attribute data-hide-on-quote="true" to hide specific information on quotes.
               For example Invoicebus doesn't need amount paid and amount due on quotes  -->
          <tr data-hide-on-quote="true">
            <th>Paid (<?php echo $setting->currency_symbol;?>):</th>
            <td id="paid_dollar"><?php echo $setting->currency_symbol; ?><?php echo $last_insert_invoice->paid_dollar ;?></td>
          </tr>

          <tr data-hide-on-quote="true">

            <th>Paid (៛):</th>
            <td id="paid_riel">៛<?php echo $last_insert_invoice->paid_riel ;?></td>

          </tr>
          <tr data-hide-on-quote="true">
            <th>Paid (bath):</th>
            <td id="paid_bath"><?php echo $setting->currency_symbol; ?><?php echo $last_insert_invoice->paid_dollar ;?></td>
          </tr>
       
         <!--  return -->
          <tr data-hide-on-quote="true">
            <th>Return ($):</th>
            <td id="return_dollar"><?php echo $setting->currency_symbol; ?><?php echo $last_insert_invoice->return_amount; ?></td>
          </tr>
           <tr data-hide-on-quote="true">
            <th>Return :</th>
            <td id="return_riel"><?php echo $setting->currency_symbol; ?><?php echo $last_insert_invoice->return_amount * $exchange_rate_tokh->price_to; ?></td>
          </tr>
           <tr data-hide-on-quote="true">
            <th>Return (bath) :</th>
            <td id="return_bath"><?php echo $setting->currency_symbol; ?><?php echo $last_insert_invoice->return_amount; ?></td>
          </tr>
          
        </table>

        <div class="clearfix"></div>
        
      </section>
      <div class="clearfix"></div>

      <section id="invoice-info">
        <div>
          <span>Issued On</span> <span><?php echo $last_insert_invoice->invoice_date; ?></span>
        </div>
        <!-- <div>
          <span>Due On</span> <span><?php echo $last_insert_invoice->invoice_date; ?></span>
        </div> -->

        <br />

       <!--  <div>
          <span>Currency</span> <span><?php echo $setting->currency_symbol; ?></span>
        </div> -->
        <div>

      </section>
      
      <section id="terms">

        <div class="notes"><?php echo $setting->company_policy;?></div>

        <br />


        
      </section>

      <div class="clearfix"></div>
		
      <div class="thank-you">Thank You!</div>

      <div class="clearfix"></div>
     <!--  <button type="button" class="btn btn-default void_button">Void</button> -->

     
</div>
</div>

			

		<?php
							
	}
		public function void_update(Request $request){
			$myID = CRUDBooster::myId();
			$invoiceid=$request->invoice_id;
			DB::table('invoices')
				->where('created_by',$myID)
				->where('invoice_id',$invoiceid)
				->update(['void'=>1]);

		}

	    public function show_invoice(Request $request){
	    	$invoiceId 		= $request->invoiceId;
	    	$myID 			= CRUDBooster::myId();
	    	$company		= DB::table('settings')->where('created_by',$myID)->select('company_name')->first();
	    	
	    	if($invoiceId != null):
	    		$invoice 	= DB::table('invoices')
	    					->join('orders','invoices.orders_id','=','orders.id')
	    					->leftjoin('currencies','currencies.name','=','orders.currency')
	    					->where('invoice_id',$invoiceId)->where('invoices.created_by',$myID)->where('currencies.created_by',$myID)->first();

	    		$invoice_details = DB::table('orders_detail')
	    						->select('orders_detail.*'						,
	    								 'orders_detail.price as order_price'	,
	    								 'invoices.*'							,
	    								 'menus.*'								,
	    								 'menu_materials.id as material_id'		,
	    								 'menu_materials.name as material_name' ,
	    								 'currencies.currency_symbol'			
	    								)
		    					->join('invoices','invoices.orders_id','=','orders_detail.orders_id')
		    					->leftjoin('currencies','currencies.name','=','invoices.currency')
		    					->leftjoin('menus','menus.id','=','orders_detail.menus_id')
		    					->leftjoin('menu_materials','menu_materials.id','=','orders_detail.material_id')
		    					->where('invoices.created_by',$myID)->where('invoices.invoice_id',$invoiceId)->where('currencies.created_by',$myID)->get();
	    	endif;

	    	?>

	    	<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="print_invoice">
			  <div class="modal-dialog modal-md">
			    <div class="modal-content">

			      <div class="modal-header">
			        <h4 class="modal-title" id="myLargeModalLabel" style="text-align: center;"><?php echo $company->company_name; ?></h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">×</span>
			        </button>
			      </div>
			      <div class="modal-body">
			       	<table class="invoice_data">
			       		<tr>
			       			<td>Invoice Number</td>
			       			<td><?php echo $invoice->invoice_id; ?></td>
			       		</tr>

			       		<tr>
			       			<td>Invoice Order Number</td>
			       			<td><?php echo $invoice->order_number; ?></td>
			       		</tr>

			       		<tr>
			       			<td>Invoice Date</td>
			       			<td><?php echo $invoice->created_at; ?></td>
			       		</tr>
			       		

			       	</table>

			       	<table class="order_data" style="margin-bottom: 10px;">
			       		<thead>
				       		<tr>
				       			<th>Item Name</th>
				       			<th>Item Price</th>
				       			<th>Item Quantity</th>
				       			<th>Sub total</th>
				       		</tr>
			       		</thead>

			       		<?php foreach($invoice_details as $invoice_detail): ?>
			       		
			       		<tbody>
				       		<tr class="order_data_detail">
				       			<?php if($invoice_detail->material_id != null): ?>
					       			<td style="text-indent: 20px;"><?php echo $invoice_detail->material_name; ?></td>
				       			
				       			<?php elseif($invoice_detail->broken_name != ''): ?>
					       			<td><?php echo $invoice_detail->broken_name; ?></td>

				       			<?php else: ?>
					       			<td><?php echo $invoice_detail->name; ?></td>

				       			<?php endif; ?>
				       			
				       			<td><?php echo $invoice_detail->currency_symbol; echo $invoice_detail->order_price; ?></td>
				       			<td><?php echo $invoice_detail->amount; ?></td>
				       			<td><?php echo $invoice_detail->currency_symbol; echo $invoice_detail->subtotal; ?>	</td>
				       		</tr>
			       		</tbody>

			       		<?php endforeach; ?>
			       		

			       	</table>

			       	<!-- total with no tax and no discount -->
			       	<div class="total" style="text-align: right; margin-bottom: 3px">
			       		Total : 
			       		<input type="text" name="" disabled style="text-align: center;" value="<?php echo $invoice->currency_symbol; echo $invoice->total; ?>">
			       	</div>

			       	<!-- discount -->
			       	<?php if($invoice->discount_type != NULL && $invoice->discount_amount > 0): ?>
			       	<div class="discount" style="text-align: right; margin-bottom: 3px">
			       		Discount : 
			       		<input type="text" name="" disabled style="text-align: center;" value="<?php echo $invoice->discount_amount; echo $invoice->discount_type; ?>">
			       		
			       	</div>
			       	<?php endif; ?>

			       	<!-- total with tax and discount -->
			       	<div class="total_with_tax_discount" style="text-align: right; margin-bottom: 3px">
			       		Total (Discount + Tax) : 
			       		<input type="text" name="" disabled style="text-align: center;" value="<?php echo $invoice->currency_symbol; echo $invoice->total_with_tax_discount; ?>">
			       		
			       	</div>



			      </div>


			      <div class="modal-footer">
			      	<button type="button" class="btn btn-primary" onclick="doit()">print</button>
			      </div>


			    </div>
			  </div>
			</div>
	    	
	    	<?php 

	    }

	   

}
