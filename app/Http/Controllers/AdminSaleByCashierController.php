<?php namespace App\Http\Controllers;

	use Session;
	use Illuminate\Http\Request;
	// use Request;
	use DB;
	use CRUDBooster;

	class AdminSaleByCashierController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field 				= "id";
			$this->limit 					= "20";
			$this->orderby 					= "id,desc";
			$this->global_privilege 		= false;
			$this->button_table_action 		= false;
			$this->button_bulk_action 		= false;
			$this->button_action_style 		= "button_icon";
			$this->button_add 				= false;
			$this->button_edit 				= true;
			$this->button_delete 			= true;
			$this->button_detail 			= true;
			$this->button_show 				= false;
			$this->button_filter 			= true;
			$this->button_import 			= false;
			$this->button_export 			= false;
			$this->table 					= "orders";
			# END CONFIGURATION DO NOT REMOVE THIS LINE        
	    }

	    public function getIndex()
	    {
	    	$myID 					= CRUDBooster::myId();
	    	$user					= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
	    
	    	$data 					= [];
	    	$data['page_title'] 	= "Sale by Cashier Report";
	    	$data['get_user'] 		= DB::table('cms_users')->select('name','id')->where('company_id',$user->company_id)->get();
	    	$data['invoices']		= DB::table('cms_users')
	    								->join('invoices','invoices.created_by','=','cms_users.id')
	    								->select('cms_users.name',DB::raw('sum(grand_total) as grand_total'),DB::raw('count("invoice_number") as table_sold'),'invoices.currency')
	    								->where('cms_users.company_id','=',$user->company_id)
										->groupBy('cms_users.id')->groupBy('cms_users.name')->groupBy('invoices.currency')->paginate(20);

	    	$this->cbView('report.sale_by_cashier',$data);

	    }

	    public function get_sort(Request $request){
	    	$myID 					= CRUDBooster::myId();
	    	$user					= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
	    	$data 					= [];
	    	$data['page_title'] 	= "Sale by Cashier Report";
	    	$data['get_user'] 		= DB::table('cms_users')->select('name','id')->where('company_id',$user->company_id)->get();

	    	$date 					= $request->by;
	    	$custom_date			= $request->daterange;
	    	$cashier_id 			= $request->cashier_name;

	    	if($date):
	    		if ($request->by == "weekly"):
				    $startDate 	= date("Y-m-d", strtotime('monday this week'))." 00:00:00";
					$endDate 	= date("Y-m-d", strtotime('sunday this week'))." 23:59:59";
				elseif ($request->by == "monthly"): 
					$startDate 	= date('Y-m-01')." 00:00:00";
					$date 		= date('Y-m-d');
					$endDate 	= date("Y-m-t", strtotime($date))." 23:59:59";
				elseif ($request->by == "yearly"):
					$startDate 	= date('Y-01-01')." 00:00:00";
					$date 		= date('Y-m-d');
					$endDate 	= date("Y-12-t", strtotime($date))." 23:59:59";
				elseif ($request->by == "daily" ):
					$day 		= new \DateTime();
					$startDate 	= $day->format('Y-m-d')." 00:00:00";
					$endDate 	= $day->format('Y-m-d')." 23:59:59";
				else:
					$startDate 	= null;
					$endDate 	= null;
				endif;

				$invoices = DB::table('cms_users')
							->select('cms_users.name',DB::raw('sum(grand_total) as grand_total'),DB::raw('count("invoice_number") as table_sold'),'invoices.currency')
							->join('invoices','invoices.created_by','=','cms_users.id')
							->where('cms_users.company_id','=',$user->company_id)
							->groupBy('cms_users.id')->groupBy('cms_users.name')->groupBy('invoices.currency')
							->paginate(20)->appends('by',$request->by);


	    	elseif($custom_date):
    			$date 		= explode(' - ',$custom_date);
    			$startDate	= $date[0];
    			$endDate 	= $date[1];

    			$invoices 	= DB::table('cms_users')
    						->select('cms_users.name',DB::raw('sum(grand_total) as grand_total'),DB::raw('count("invoice_number") as table_sold'),'invoices.currency')
							->join('invoices','invoices.created_by','=','cms_users.id')
							->where('cms_users.company_id','=',$user->company_id)
							->where('invoice_date','>=',$startDate)
							->where('invoice_date','<=',$endDate)
							->groupBy('cms_users.id')->groupBy('cms_users.name')->groupBy('invoices.currency')
							->paginate(20)->appends('daterange',$custom_date);

	    	elseif($cashier_id):

	    		$invoices 	= DB::table('cms_users')
	    					->select('cms_users.name',DB::raw('sum(grand_total) as grand_total'),DB::raw('count("invoice_number") as table_sold'),'invoices.currency')
							->leftjoin('invoices','invoices.created_by','=','cms_users.id')
							->where('cms_users.company_id','=',$user->company_id)
							->where('invoices.created_by','>=',$cashier_id)
							->groupBy('cms_users.id')->groupBy('cms_users.name')->groupBy('invoices.currency')
							->paginate(20)->appends('cashier_name',$cashier_id);
			else:

				$invoices = DB::table('cms_users')
	    								->join('invoices','invoices.created_by','=','cms_users.id')
	    								->select('cms_users.name',DB::raw('sum(grand_total) as grand_total'),DB::raw('count("invoice_number") as table_sold'),'invoices.currency')
	    								->where('cms_users.company_id','=',$user->company_id)
										->groupBy('cms_users.id')->groupBy('cms_users.name')->groupBy('invoices.currency')->paginate(20);

	    	endif;

	    	$data['invoices'] = $invoices;
	    	$this->cbView('report.sale_by_cashier',$data);
	    }
	    
	    


	}