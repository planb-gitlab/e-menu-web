<?php 
namespace App\Http\Controllers;

use Session;
use Illuminate\Support\Facades\Request;
use Carbon\Carbon;
use DB;
use CRUDBooster;
class CBHook extends Controller {

    public function afterLogin() {
        $myID = CRUDBooster::myId();
        $user = DB::table('cms_users')->select('role','created_by')->where('id',$myID)->first();
            
        if($adminID->role == 1 && $adminID->created_by == 1):
            $setting = DB::table('settings')->where('created_by',$adminID->id)->first(); 
        else:
            $setting = DB::table('settings')->where('created_by',$adminID->created_by)->first(); 
        endif;


        if(!isset($_COOKIE['user_lang'])):
            $user_lang          = "user_lang";
            $user_lang_value    = $setting->company_lang;
            setcookie($user_lang, $user_lang_value, time() + (10 * 365 * 24 * 60 * 60),'/'); // 86400 = 1 day
        else:
            $user_lang          = "user_lang";
            $user_lang_value    = $setting->company_lang;
            setcookie($user_lang, $user_lang_value, time() + (10 * 365 * 24 * 60 * 60),'/'); // 86400 = 1 day
        endif;
    }
}