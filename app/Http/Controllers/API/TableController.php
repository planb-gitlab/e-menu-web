<?php
namespace App\Http\Controllers\API;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Hashing\BcryptHasher;
use CRUDBooster;

class TableController extends Controller
{

    public function getTables(Request $request){

    	$company_id = $request->comid;
    	$tables = DB::table('table_numbers')
        ->select('table_numbers.*')
        ->where('company_id', $company_id)
        ->get();

        return Response::json($tables);
    }

    public function setTableStatus(Request $request){
        $tableid = $request->tableid;
        // $userid = $request->userid;
        $status =  $request->status;
        $comid =  $request->comid;

        // $user = DB::table('cms_users')->select('company_id')->where('id', $userid)->first();

        // DB::table('table_numbers')
        // ->where('id', $tableid)
        // ->where('company_id', $user->company_id)
        // ->update(['status' => $status]);

        DB::table('table_numbers')
        ->where('id', $tableid)
        ->where('company_id', $comid)
        ->update(['status' => $status]);

        $table = DB::table('table_numbers')->select('status')->where('id', $tableid)->first();
        
        return $table->status;
    }

    public function mergeTable(Request $request){
        $comid = $request->comid;
        $main_table_id = $request->main; 
        $side_table_id = $request->side;
        $merge = "";

        $explodeSide = explode(",", $side_table_id);
        for ($i = 0; $i<count($explodeSide); $i++) {
            DB::table('table_numbers')->where('table_code', $explodeSide[$i])->where('company_id', $comid)->update(['is_merge' => 1]);

            $table_id = DB::table('table_numbers')->select('id')->where('table_code', $explodeSide[$i])->first();
           
            if($merge != ""){
                $merge = implode(',', array($merge, $table_id->id));
            }else{
                $merge = $table_id->id;
            }
        }

        DB::table('table_numbers')
        ->where('id', $main_table_id)
        ->where('company_id', $comid)
        ->update(['merge_with' => $merge]);

        return "done";
    }

}