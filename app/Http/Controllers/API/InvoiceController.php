<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use CRUDBooster;
use DB;
use Response;
use Tymon\JWTAuth\Facades\JWTAuth;

class InvoiceController extends Controller
{
    public function getInvoice()
    {
        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user['id'];
        $checkUserPermission = DB::select("SELECT cms_users.id,roles.name FROM cms_users LEFT JOIN roles ON cms_users.role=roles.id WHERE cms_users.id =$userId");
        if (strtolower($checkUserPermission[0]->name) == 'cashier') {
            $last_invoice = DB::table("orders")->where('user_id', $userId)->orderBy("id", "DESC")->limit(1)->get();
            if (count($last_invoice)) {
                $last_invoice = $last_invoice[0]->order_number;
                $last_invoice = explode("-", $last_invoice);
                $end_invoice = end($last_invoice);
                $first_invoice = $last_invoice[0];
                $end_invoice = (int)$end_invoice + 1;


                $result_id = $first_invoice . "-" . $end_invoice;
                return Response::json([
                    'last_invoice' => $result_id
                ]);
            }
            return Response::json([
                'last_invoice' => date("mdy") . "-1"
            ]);

        }
    }
}
