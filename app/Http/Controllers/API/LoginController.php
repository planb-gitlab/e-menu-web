<?php
namespace App\Http\Controllers\API;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Hashing\BcryptHasher;
use CRUDBooster;

class LoginController extends Controller{

	public function loginByEmail(Request $request){
        /*
            $email = "admin@system.com"
            $password = "123456"
        */
    	$email = $request->email;
    	$password = $request->password;

    	$user = DB::table('cms_users')
        ->select('cms_users.*', 'settings.company_name as settings_name')
        ->where('email', $email)
        ->leftjoin("settings", "settings.id", "=", "cms_users.company_id")
        ->first();

        // pusher data
        $pusher = [
            'pusher_app_id'  => env('PUSHER_APP_ID'),
            'pusher_key'     => env('PUSHER_APP_KEY'),
            'pusher_secret'  => env('PUSHER_APP_SECRET'),
            'pusher_cluster' => env('PUSHER_APP_CLUSTER'),
        ];
    


    	if(\Hash::check($password,$user->password)):
    		return Response::json(['result' => 'successful', 'users' => $user, 'pusher' => $pusher]);
    	else:
    		return Response::json(['result' => 'fail']);
    	endif;
    }

    public function loginByPin(Request $request){
    	$userPin 	=$request->pin;
    	$company_id	=$request->comid;
    	/*
            $userPin="8178";
    	    $company_id="6";
        */
    	$userPin = DB::table('cms_users')
        ->select('cms_users.*',"cms_privileges.name as cms_privileges_name")
        ->where('cms_users.company_id', $company_id)
        ->where('user_pin', $userPin)
        ->leftjoin("cms_privileges","cms_privileges.id","=","cms_users.id_cms_privileges")
        ->first();

    	if($userPin != null):
    		return Response::json(['result' => 'successful','users' => $userPin]);
    	else:
    		return Response::json(['result' => 'failed']);
    	endif;	
    }
}