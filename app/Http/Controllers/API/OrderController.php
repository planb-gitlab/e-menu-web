<?php

namespace App\Http\Controllers\API;

use App\Events\OrderEvent;
use App\Http\Controllers\Controller;
use CRUDBooster;
use App\InvoiceModel;
use DB;
use Illuminate\Http\Request;
use Response;
use Tymon\JWTAuth\Facades\JWTAuth;
use Session;
use URL;
use Carbon\Carbon;
use File;

class OrderController extends Controller{

    // ========================= Save order to local json file ===============================
    public function requestOrder(Request $request){

        $order      = $request->getContent();
        $orderType = request()->order_type;
        $content    = json_decode($order, true);
        $table_id   = $content['id'];        
        $status     = $content['dataStatus'];

        if ($content['type'] == 'edit_invoice') {
            $takeOutOrder = InvoiceModel::whereInvoiceId($content['invoiceId'])
                ->whereHasPaid(0)
                ->first();
            $table_id = 'takeout-' . $takeOutOrder->file_name;
        }
        // Response data json to realtime update
        $data = [
            'status'    => 0, // Ordered
            'tableCode' => $table_id,
        ];
        event(new OrderEvent($data));
        File::put($table_id.'-ticket.json', $order);
        return Response::json(["status" => "done"]);
    }

    // ========================= Get Order to local json file =========================
    public function getOrder(Request $request){
        
        $tableID    = $request->tableid;
        
        $url        = File::get((string)$tableID."-ticket.json");
        $order      = json_decode($url, true);
        return Response::json($order);
    }

    // ========================= Save Order to DB (TAKE OUT) =========================
    public function saveTakeout(Request $request){

        $req = $request->all();
        $data = $req['data'];
        $items = $data['items'];

        $myID = CRUDBooster::myId();
        $user = DB::table('cms_users')->select('company_id')->where('id', $myID)->first();
        $invoice_number = DB::table('invoices')->select('invoice_number')->latest()->where('company_id',$user->company_id)->orderBy('invoice_id','DESC')->first();
        $invoiceNumber = $invoice_number->invoice_number + 1;
        $dataSet = [
            "invoice_date"      => Carbon::now()->toDateString(),
            'invoice_number'    => $invoiceNumber,
            'order_type'        => 'takeout',
            'company_id'        => getCompanyId($myID),
            'created_by'        => $myID,
            'created_at'        => Carbon::now()
        ];

        $saveInvoice        = DB::table('invoices')->insert($dataSet);
        $lastSaveInvoice    = DB::table('invoices')->select('invoice_id')->latest()->where('company_id',$user->company_id)->orderBy('invoice_id','DESC')->first();

        if ($saveInvoice) {
            $invoiceOrder = [];
            foreach ($items as $key => $value) {
                $invoiceOrder[] = [
                    'invoice_id' => $lastSaveInvoice->invoice_id,
                    'item_id' => $value['item_id'],
                    'item_size' => $value['item_size'],
                    'addon_id' => 0,
                    'qty' => $value['item_qty'],
                    'price' => $value['item_amount'],
                    'status' => 1,
                    'company_id' => getCompanyId($myID),
                ];
                
            }
            DB::table('invoice_orders')->insert($invoiceOrder);
        }


        // Response data json to realtime update
        $data = [
            'action'    => 'takeout',
        ];
        event(new OrderEvent($data));
    }

}
