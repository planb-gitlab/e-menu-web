<?php

namespace App\Http\Controllers\API;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Hashing\BcryptHasher;
use CRUDBooster;

class SubcategoryController extends Controller
{
    //
    public function getSubCategory(Request $request) {
        $company_id     = $request->comid;
        $category_id    = $request->categoryid;
        $subCategory    = DB::table('subcategories')->where('company_id', $company_id)->where('status', "1");
        if(!$category_id):
            $subCategory = $subCategory->get();
        else:
            $subCategory = $subCategory->where('category_id', $category_id)->get();
        endif;
        
    	return Response::json($subCategory);
    }
}
