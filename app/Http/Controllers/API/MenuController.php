<?php

namespace App\Http\Controllers\API;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Hashing\BcryptHasher;
use CRUDBooster;

class MenuController extends Controller
{


    public function getMenu(Request $request)
    {
        $company_id = $request->comid;
        $menus      = DB::table('menus')->where('company_id', $company_id)->get();
        return Response::json($menus);
        // return count($menus);
    }


    public function getMenuByCategoryIdAndSubCategoryId(Request $request)
    {
        $company_id     = $request->comid;
        $category_id    = $request->categoryid;
        $subcategory_id = $request->subcategoryid;

        $menus = DB::table('menus')->where('company_id', $company_id);
        if(!$subcategory_id):
            $menus = $menus->where('category_id', $category_id)->get();
        elseif(!$category_id && $subcategory_id):
            $menus = $menus->where('subcategory_id', $subcategory_id)->get();
        else:
            $menus = $menus->where('category_id', $category_id)->where('subcategory_id', $subcategory_id)->get();
        endif;
        return Response::json($menus);
        // return count($menus);
    }

    //by viroth
    public function getAllMenu(Request $request)
    {
        $company_id = $request->comid;
        $menus = DB::table('menus')
            ->where('company_id', $company_id)
            ->get();
        return Response::json($menus);
        // return count($menus);
    }

    public function getCategories(Request $request)
    {
        $company_id = $request->comid;
        $categories = DB::table('categories')->where('company_id', $company_id)->get();
        return Response::json($categories);
    }

    public function getMenuDetail(Request $request)
    {
        $details = DB::table('menu_detail')->get();
        return Response::json($details);
    }

    // =================== Get Addon by Menu ID =========================
    public function getMenuDetailByMenuId(Request $request)
    {
        $menuid = $request->menuid;
        $details = DB::table('menu_detail')
            ->where('menu_id', $menuid)
            ->get();
        return Response::json($details);
    }

    public function getMenuMaterial(Request $request)
    {
        $materials = DB::table('menu_materials')->get();
        return Response::json($materials);
    }

    // ================= Get Addon by ID ====================
    public function getMenuMaterialByMenuId(Request $request)
    {
        $materialid = $request->materialid;
        $menus      = DB::table('menu_materials')->where('id', $materialid)->get();
        return Response::json($menus);
    }

    public function getMenuUOMPrice(Request $request)
    {   
        $menu_id    = $request->menu_id; 
        $uom        = DB::table('menu_uom_price')->where('menu_id',$menu_id)->get();
        return Response::json($uom);
    }

    public function getAddOns(Request $request)
    {
        $comid      = $request->comid;
        $addOns = DB::table('menu_materials')->where('company_id', $comid)->get();
        return Response::json($addOns);
    }
}