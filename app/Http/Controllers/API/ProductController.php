<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use CRUDBooster;
use DB;
use Illuminate\Http\Request;
use Response;
use Tymon\JWTAuth\Facades\JWTAuth;

class ProductController extends Controller
{
    public function getSendRequest(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user['id'];
        $checkUserPermission = DB::select("SELECT cms_users.id,roles.name FROM cms_users LEFT JOIN roles ON cms_users.role=roles.id WHERE cms_users.id =$userId");
        if (strtolower($checkUserPermission[0]->name) == 'cashier') {
            if (DB::table('tmp_order')->where('user_id', $userId)->where('table_code', $request->table_name)->update(['send_to_kitchen' => 1])) {
                return Response::json([
                    'message' => 'Product has been send request to kitchen'
                ], 200);
            }
        }
    }

    public function getHasMaterial(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user['id'];
        $checkUserPermission = DB::select("SELECT cms_users.id,roles.name FROM cms_users LEFT JOIN roles ON cms_users.role=roles.id WHERE cms_users.id =$userId");
        if (strtolower($checkUserPermission[0]->name) == 'cashier') {
            $id = $request->id;
            $name = $request->name;
            $price = $request->price;
            $table_number = $request->table_number;
            $type = $request->type;
            $exist = DB::table('tmp_order')->where('user_id', $userId)->where('menu_id', $id)->where('table_code', $table_number)->where('name',$name);
            if (!$exist->count()) {
                DB::table('tmp_order')->insert(['user_id' => $userId, 'menu_id'=>$id,'name' => $name,'qty'=>1, 'price' => $price, 'type' => $type,'table_code'=>$table_number]);
                return Response::json([
                    'message' => 'Product has been added to the list'
                ]);
            } else {
                $qty = $exist->first()->qty;
                $qty = (int)$qty + 1;
                $exist->update(['qty' => $qty]);
                return Response::json([
                    'message' => 'Product has updated!'
                ]);
            }
        }

    }
}
