<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use CRUDBooster;
use DB;
use Illuminate\Http\Request;
use Response;
use Tymon\JWTAuth\Facades\JWTAuth;

class NotificationController extends Controller
{
    public function getIndex(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user['id'];
        $checkUserPermission = DB::select("SELECT cms_users.id,roles.name FROM cms_users LEFT JOIN roles ON cms_users.role=roles.id WHERE cms_users.id =$userId");
        if (strtolower($checkUserPermission[0]->name) == 'cashier') {
            $orders = DB::table('orders')->where('user_id', $userId)->where('status', 1)->get();
            $array = array();
            foreach ($orders as $key => $order) {
                $array[] = [
                    'id' => $order->id,
                    'table_id' => DB::table('table_numbers')->where('id', $order->table_id)->first()->table_code,
                    'order_number' => $order->order_number,
                    'available' => DB::table("orders_detail")->leftJoin('menus','menus.id','orders_detail.menus_id')->where('orders_id', $order->id)->where('orders_detail.status', '!=',5)->select('menus.name','orders_detail.status')->get(),
                ];
            }
            return $array;
        }
    }

    public function getCount(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user['id'];
        $checkUserPermission = DB::select("SELECT cms_users.id,roles.name FROM cms_users LEFT JOIN roles ON cms_users.role=roles.id WHERE cms_users.id =$userId");
        if (strtolower($checkUserPermission[0]->name) == 'cashier') {
            $orders = DB::table('orders')->where('user_id', $userId)->where('status', 1)->get();
            $array = array();
            foreach ($orders as $key => $order) {
                $array[] = [
                    'id' => $order->id,
                    'order_number' => $order->order_number,
                    'not_available' => DB::table("orders_detail")->where('orders_id', $order->id)->count(),
                    'available' => DB::table("orders_detail")->where('orders_id', $order->id)->count()
                ];
            }
            return Response::json([
                'notification' => $array
            ]);
        }
    }


}
