<?php
namespace App\Http\Controllers\API;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Hashing\BcryptHasher;
use CRUDBooster;

class TestingController extends Controller
{
    
    // public function getmenu(Request $request){
    // 	$mycompanyId 	=$request->companyid;
    // 	$getPhotoMenu	=DB::table('menus')->select('menus.*')->where('company_id',$mycompanyId)->get();

    // 	return Response::json(['getMenu'=>$getPhotoMenu]);
    // }

    // public function gettable(Request $request){
    // 	$mycompanyId = $request->companyId;
    // 	$getTable = DB::table('table_numbers')->select('table_numbers.*')->where('company_id',$mycompanyId)->get();

    // 	return Response::json(['tables'=>$getTable]);
    // }

    // public function update_tablestatus(Request $request){
    // 	$tablecode 		=$request->table_code;
    // 	$userid 		=$request->userid;

    // 	$get_companyid=DB::table('cms_users')->select('company_id')->where('id',$userid)->first();
    // 	DB::table('table_numbers')
    // 	->where('table_code',$tablecode)
    // 	->where('company_id',$get_companyid->company_id)
    // 	->update(['status'=>0]);
    // }


    // public function mergetable(Request $request){
    //     $userid     = $request->userid;
    //     $main       = $request->main; 
    //     $side       = $request->side;

    //     $get_companyid=DB::table('cms_users')->select('company_id')->where('id',$userid)->first();
    //     DB::table('table_numbers')
    //         ->where('id',$main)
    //         ->where('company_id',$get_companyid->company_id)
    //         ->update(['merge_with'=>$side]);
    //     $explodeSide=explode(",", $side);
    //     for ($i = 0; $i<count($explodeSide); $i++) {
    //         DB::table('table_numbers')->where('id',$explodeSide[$i])->where('company_id',$get_companyid->company_id)->update(['is_merge'=>1]);
    //     }
    // }

    public function getUserSingle(Request $request){

        // $user = DB::table('cms_users')->first();
        $users = collect(DB::select("select * from cms_users"))->where('id', 44)->first();

        // $user = $users -> first();
        return Response::json($users);
    }
}
