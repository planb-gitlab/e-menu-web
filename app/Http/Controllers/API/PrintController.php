<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use CRUDBooster;
use App\InvoiceModel;
use DB;
use Illuminate\Http\Request;
use Response;
use Session;
use URL;
use Carbon\Carbon;
use File;
use Dompdf\Dompdf;

class PrintController extends Controller{

	public function printTicket(Request $request){

		$myId 			= CRUDBooster::myId();
		$companyId 		= getCompanyId($myId);
		$req 			= $request->all();
		$company_id 	= $request->comid;
		$tableID 		= $request->table_code;
		$type 			= $request->type;
		$invoiceId 		= $request->invoiceId;
		$orderType 		= $request->order_type;
		
		if (!empty($invoiceId)) {
			$getFileName = InvoiceModel::whereInvoiceId($req['invoiceId'])->first();
			
            $tableID = 'takeout-' . $getFileName->file_name;
		}
		

		$checkOrderNo = InvoiceModel::whereDate('created_at', Carbon::today())
			->whereCompanyId($companyId);

		// Count Order Number of Take Out
		$orderCode = $checkOrderNo->whereOrderType('takeout')->count();
		// Count Invoice number	
		$invoiceNumber = $checkOrderNo->count();

		$orderCode = ($orderCode < 9) ? '0' . ($orderCode+1) : $orderCode+1;

		$invoiceNumber = ($invoiceNumber < 9) ? '0' . ($invoiceNumber+1) : $invoiceNumber+1;

		if ($req['order_type'] == 'takeout' && $req['invoiceId'] == Null) {

			$data = [
				'created_by' => $myId,
				'company_id' => $company_id,
				'order_type' => 'takeout',
				'has_paid' => 0,
				'invoice_date' => now(),
				'order_code' => $orderCode,
				'invoice_number' => $invoiceNumber,
				// 'grand_total' => 
				'file_name' => str_replace('takeout-', '', $tableID)
			];
			$takeOutOrder = InvoiceModel::create($data);
		}

		if(!empty($company_id)) :

			$order_data     = File::get((string) $tableID . "-ticket.json");
			$orders         = json_decode($order_data, true);
			$item  			= [];
			foreach ($orders['items'] as $key => $order):
				if($orders['items'][$key]['print_status'] == '0'):
					array_push($item,$orders['items'][$key]);
				endif;
			endforeach;

			if(count($item) >= 1):

				$data                   = [];
				$data['now']            = Carbon::now();
				$data['table']			= $tableID;
				$data['items']			= $item;
				$data['order_type']		= request()->order_type;
				$data['orderCode']		= $orderCode;
				$orderType 				= request()->order_type;
				$view                   = view('ticket.ticket_format_01', $data)->render();
				$dompdf                 = new Dompdf();
				$dompdf->loadHtml($view);
				$dompdf->setPaper('A7', 'portrait');
				$dompdf->render();
				$output 				= $dompdf->output();


				File::put($tableID.'-ticket.pdf', $output);

				$printer_info = DB::table('printer')->select('printer_name')->where('company_id',$company_id)->where('printer_type','invoice')->first();

		        //generate vbscript
				$print_vbs = '
				Set oShell = CreateObject ("Wscript.Shell") 
				Dim strArgs
				strArgs = "'.$tableID.'-ticket.bat"
				oShell.Run strArgs, 0, false';

				$print_batch 		= '"PDFtoPrinter.exe" "' . $tableID . '-ticket.pdf" "' . $printer_info->printer_name .'"';

				File::put($tableID.'-ticket.vbs', $print_vbs);
				File::put($tableID.'-ticket.bat', $print_batch);
				
				$print_ticket  		= $tableID."-ticket.vbs";
				echo shell_exec($print_ticket);


				//get ticket data
				$jsonString 	= File::get((string)$tableID."-ticket.json");
				$printTicket 	= json_decode($jsonString, true);


				foreach ($printTicket['items'] as $key => $entry):
					if($printTicket['items'][$key]["print_status"] == "0"):
						$qty 											= (int)$printTicket['items'][$key]["item_qty"] + (int)$printTicket['items'][$key]["addmore"];
						$printTicket['items'][$key]["item_qty"]	 		= (string)$qty;		
						$printTicket['items'][$key]["addmore"]	 		= "0";  
						$printTicket['items'][$key]["print_status"] 	= "1";
					endif;
				endforeach;
				$newJsonString = json_encode($printTicket);
				File::put($tableID.'-ticket.json', $newJsonString);
			
			endif;
			return Response::json([
				"status" => 'done',
			]);
		else:
			// return Response::json(["status" => "fail"]);
		endif;
		
		return Response::json([
			"status" => $takeOutOrder,
		]);
	}
}

