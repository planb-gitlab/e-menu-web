<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use CRUDBooster;
use DB;
use Response;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Events\MergeEvent;
use App\Events\UnmergeEvent;

class MergeController extends Controller
{
    public function MergeAndUnmerge(Request $request)
    {
    		$myId = $request->id;
			$req = $request->all(); 

			$user = DB::table('cms_users')->select('company_id')->find($myId);
			// var_dump($user->company_id); die();
			if (!empty($req['side_table_id'])) {
				
				// Get table id that going to merge to the main table
				$tableMergeId = implode(',', array_filter($req['side_table_id']));

				// Before Update new Table merge
				$mergedTable = DB::table('table_numbers')->find($req['table_id']);
				$mergeId = explode(',', $mergedTable->merge_with);
				
				// Update all data to null related to main table
				// Reset is_merge field to 0 
				if ($mergedTable) {
					foreach ($mergeId as $key => $value) {
						DB::table('table_numbers')->whereId($value)->update(['is_merge' => 0, 'created_by' => $myId]);
					}
				}

				// Re-updated is merge and merge_with field
				$merge = DB::table('table_numbers')->whereId($req['table_id'])->whereIsMerge(0)->update(['merge_with' => $tableMergeId, 'created_by' => $myId, 'company_id' => $user->company_id]);

				// Check if have any merge table inside
				if ($merge && !empty($tableMergeId)) {
					if (count($req['side_table_id']) > 0) {
						foreach ($req['side_table_id'] as $key => $value) {

							$isMerged = DB::table('table_numbers')->find($value); 

							// Check if merge with the main table already merge with sub table
							if (!empty($isMerged->merge_with)) {
								$result = $tableMergeId . ',' . $isMerged->merge_with;

								$lastExplode = explode(',', $result);
								DB::table('table_numbers')->whereId($req['table_id'])->update([
									'merge_with' => $result, 'company_id' => $user->company_id
								]);

								foreach ($lastExplode as $key => $value) {
									DB::table('table_numbers')->whereId($value)->update([
										'merge_with' => NULL, 'is_merge' => 1
									]);
								}
								
							}

							// Check if already merged
							if ($isMerged->is_merge == 1) {
								DB::table('table_numbers')->whereId($req['table_id'])->update(['merge_with' => NULL]);
								return "already_merged";
								// var_dump($isMerged->table_code . 'Is already merged');
							}

							DB::table('table_numbers')->whereId($value)->update(['is_merge' => 0, 'created_by' => $myId]);
							$merge = DB::table('table_numbers')->whereId($value)->update(['is_merge' => 1, 'created_by' => $myId, 'company_id' => $user->company_id]);
							
						}
					}

				}

				// Response data json to realtime update
		        $data = [
		        	'oldTableCode' => getTableCode($mergeId),
		            'tableCode' => getTableCode($req['side_table_id']),
		            'currentTable' => $req['table_id'],
		            'mergeWith' => true
		        ];
		        event(new MergeEvent($data));
				return "merge_and_order";
			}

			// Clear table merge code
			else {

				$findTableId = DB::table('table_numbers')->find($req['table_id']);
				// Check if it have any merged data
				if (!empty($findTableId->merge_with)) {
					$explodeId = explode(',', $findTableId->merge_with);

					foreach ($explodeId as $key => $value) {
						$updateIsMerge = DB::table('table_numbers')->whereId($value)
							->update(['is_merge' => 0, 'created_by' => $myId,  'company_id' => $user->company_id]);
					}
					DB::table('table_numbers')->whereId($req['table_id'])
						->update(['merge_with' => NULL,  'company_id' => $user->company_id]);
				}

				// Response data json to realtime update
		        $data = [
		            'oldTableCode' => !empty($explodeId) ? getTableCode($explodeId) : '',
		            'currentTable' => $req['table_id'],
		            'mergeWith' => false
		        ];
		        event(new UnmergeEvent($data));
				return "reload";

			}
	}
}