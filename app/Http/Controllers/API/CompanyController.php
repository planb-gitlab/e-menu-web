<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use CRUDBooster;
use DB;
use Response;
use Tymon\JWTAuth\Facades\JWTAuth;

class CompanyController extends Controller
{
    public function getCompanyName()
    {
        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user['id'];
        $checkUserPermission = DB::select("SELECT cms_users.id,roles.name FROM cms_users LEFT JOIN roles ON cms_users.role=roles.id WHERE cms_users.id =$userId");
        if (strtolower($checkUserPermission[0]->name) == 'cashier') {
            $owner = DB::table('cms_users')->where('id', $checkUserPermission[0]->id)->first();
            $owner_id = $owner->created_by;
            $company = DB::table("settings")->where('created_by', $owner_id)->first();
            $company_name = $company->company_name;
            return Response::json([
                'company_name' => $company_name
            ], 200);
        }
    }
}
