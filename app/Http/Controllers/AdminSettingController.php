<?php namespace App\Http\Controllers;

	use Session;
	use Illuminate\Http\Request;
	use DB;
	use CRUDBooster;
	use URL;
	use Carbon\Carbon;
	use App\Setting;
	use Image;

	class AdminSettingController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field 			= "company_name";
			$this->limit 				= "20";
			$this->orderby 				= "id,desc";
			$this->global_privilege 	= true;
			$this->button_table_action 	= true;
			$this->button_bulk_action 	= false;
			$this->button_action_style 	= "button_icon";
			$this->button_add 			= false;
			$this->button_edit 			= true;
			$this->button_delete 		= false;
			$this->button_detail 		= true;
			$this->button_show 			= false;
			$this->button_filter 		= false;
			$this->button_import 		= false;
			$this->button_export 		= false;
			$this->table 				= "settings";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Company Name","name"=>"company_name","width"=>"115"];
			$this->col[] = ["label"=>"Company Address","name"=>"company_address","width"=>"400"];
			$this->col[] = ["label"=>"Company Logo","name"=>"company_logo","image"=>true,"width"=>"110"];
			$this->col[] = ["label"=>"Company Email","name"=>"company_email","width"=>"160"];
			$this->col[] = ["label"=>"Company Phone","name"=>"company_phone","width"=>"160"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Company Name','name'=>'company_name','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Company Address','name'=>'company_address','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Company Logo','name'=>'company_logo','type'=>'upload','validation'=>'image','width'=>'col-sm-10','help'=>'File types support : JPG, JPEG, PNG, GIF, BMP'];
			$this->form[] = ['label'=>'Company Info','name'=>'company_info','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Company Email','name'=>'company_email','type'=>'email','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Company Contact (Multiple)','name'=>'company_contact','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Company Phone','name'=>'company_phone','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Company Policy','name'=>'company_policy','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Company Name','name'=>'company_name','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Company Address','name'=>'company_address','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Company Logo','name'=>'company_logo','type'=>'upload','validation'=>'image','width'=>'col-sm-10','help'=>'File types support : JPG, JPEG, PNG, GIF, BMP'];
			//$this->form[] = ['label'=>'Company Info','name'=>'company_info','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Company Email','name'=>'company_email','type'=>'email','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Company Contact (Multiple)','name'=>'company_contact','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Company Phone','name'=>'company_phone','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Company Policy','name'=>'company_policy','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			# OLD END FORM

			}

	    public function getIndex(){
	    	$myID 		= CRUDBooster::myId();
	    	$user       = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();

           	$data = [];
           	$data['url']            	= URL::to("/");
           	$data['settings'] 			= DB::table('settings')->where('id',$user->company_id)->first();
           	$data['countries']			= DB::table('countries')->get();
           	$data['languages'] 			= DB::table('languages')->get();
           	$data['currencies'] 		= DB::table('currencies')->get();
           	$data['tax_types']			= DB::table('tax_types')->get();
           	$data['outlet_session']		= $_COOKIE['outlet_session'];
          	  
          	$this->cbView('setting.outlet.outlet',$data);
	    }

	    public function update_company_information(Request $request){
	    	$myID 		= CRUDBooster::myId();
	    	$now 		= Carbon::now();
	    	$user       = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
	    	
	    	$uploaddir = '../storage/app/uploads/'.$now->year."-".$now->month;
	        if (!is_dir($uploaddir) && !mkdir($uploaddir)){
	          mkdir('../storage/app/uploads/'.$now->year."-".$now->month, 0777, true);
	        }

	        $company_id 		= $request->company_id;

	    	$company_name 		= $request->company_name;
	    	$company_address 	= $request->company_address;
	    	$company_country	= $request->company_country;
	    	
	    	$image = DB::table('settings')->where('id',$company_id)->first();
            if($image->company_logo != "" || $image->company_logo != null){
                $company_image = DB::table('settings')->select('company_logo')->where('id',$company_id)->first();
                $photo_db = $company_image->company_logo;
                
            }else{
                $photo                  = $request->file('company_logo');
	        	$photo_db               = "uploads/client_photo/". time() . '.' . $photo->getClientOriginalExtension();
	        	$photo_upload           = time() . '.' . $photo->getClientOriginalExtension();
	        	Image::make($photo)->fit(136,131)->save('../storage/app/uploads/client_photo/' . $photo_upload);
            }

            $company_info 		= $request->company_info;
            $company_email		= $request->company_email;
            $company_contact 	= $request->company_contact;
            $company_phone		= $request->company_phone;
            $company_website	= $request->company_website;
            $company_language	= $request->company_language;

            //update currency

            if($company_name == null || $company_address == null || $company_country == null || $photo_db == null || $company_email == null || $company_phone == null || $company_language == null):
            	CRUDBooster::redirect($_SERVER['HTTP_REFERER'],trans("crudbooster.Please enter valid information"),"warning");
            else:

            $company = setting::find($company_id);
            $company->company_name 		= $company_name;
            $company->company_address	= $company_address;
            $company->company_country	= $company_country; 
            $company->company_logo		= $photo_db;
            $company->company_info  	= $company_info;
            $company->company_email		= $company_email;
            $company->company_contact	= $company_contact;
            $company->company_phone		= $company_phone;
            $company->company_website	= $company_website;
            $company->company_lang 		= $company_language;
            $company->updated_at		= $now;


            $company->save();

           


            unset($_COOKIE['user_lang']);
            setcookie("user_lang", "", time() + 3600, '/');
            
            
            
	        $setting 	= DB::table('settings')->where('id',$user->company_id)->first(); 

            if(!isset($_COOKIE['user_lang'])):
                $user_lang          = "user_lang";
                $user_lang_value    = $setting->company_lang;
                setcookie($user_lang, $user_lang_value, time() + (10 * 365 * 24 * 60 * 60),'/'); // 86400 = 1 day
            else:
                $user_lang          = "user_lang";
                $user_lang_value    = $setting->company_lang;
                setcookie($user_lang, $user_lang_value, time() + (10 * 365 * 24 * 60 * 60),'/'); // 86400 = 1 day
            endif;



            setcookie("outlet_session",$outlet_session, time() + 10, '/');
            CRUDBooster::redirect($_SERVER['HTTP_REFERER'],trans("setting.The company information has been updated !"),"info");
        endif;

	    }


	    public function update_company_tax(Request $request){
	    	$now 					= Carbon::now();
	    	$company_id 			= $request->company_id;
	    	$company_include_tax 	= $request->have_tax;
	    	$tax_type 				= $request->Tax_type;
	    	$outlet_session 		= $request->outlet_session;
	    	
	    	if($company_include_tax == null):
	    		$include_tax		= 0;
	    		$company_sale_tax	= 0; 
	    		$company_tax_type_id = null;
	    	else:
	    		$include_tax		= 1;
	    		$company_sale_tax	= $request->company_sale_tax; 
	    		$company_tax_type_id = $tax_type;

	    	endif;

	    	if($include_tax == 1):
	    		if($company_sale_tax == null || $company_sale_tax == ''):
	    			CRUDBooster::redirect($_SERVER['HTTP_REFERER'],trans("crudbooster.Please enter valid information"),"warning");
	    		endif;
	    	endif;

	    	$company = setting::find($company_id);
	    	$company->include_tax 			= $include_tax;
	    	$company->company_sale_tax 		= $company_sale_tax;
	    	$company->company_tax_type_id 	= $company_tax_type_id;
	    	$company->updated_at	   		= $now;

	    	$company->save();

	    	setcookie("outlet_session",$outlet_session, time() + 10, '/');

            CRUDBooster::redirect($_SERVER['HTTP_REFERER'],trans("setting.The company information has been updated !"),"info");
	    }

	    public function update_company_currency(Request $request){
	    	$myID 				= CRUDBooster::myId();
	    	$user 				= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
	    	$now 				= Carbon::now();
	    	$outlet  			= DB::table('settings')->where('id',$user->company_id)->first();
	    	$currency_to 		= $request->company_currency;
	    	$outlet_session 		= $request->outlet_session;
	    	$exchange 			= DB::table('exchange')->where('currency_from',$outlet->company_currency)->where('currency_to',$currency_to)->where('company_id',$outlet->id)->first();


	    	//Exchange in settings
	    	if($exchange):
		    	DB::table('settings')->where('id',$user->company_id)->update(['company_old_currency' => $outlet->company_currency]);
	            $outlet  					= DB::table('settings')->where('id',$user->company_id)->first();
	            $company 					= setting::find($user->company_id);
	            $company->company_currency  = $currency_to;
	            $company->save();
	            setcookie("outlet_session",$outlet_session, time() + 10, '/');
        	endif;

        	//Exchange in [Menu,Addon,Discount,Promotion Pack]
        	$menus				= DB::table('menus')->where('company_id', $user->company_id)->get();
            $addons				= DB::table('menu_materials')->where('company_id', $user->company_id)->get();
            $discounts			= DB::table('discounts')->where('discount_type','$')->where('company_id', $user->company_id)->get();
            $promotion_packs	= DB::table('promotion_packs')->where('discount_type','$')->where('company_id', $user->company_id)->get();
            $promotion_types    = DB::table('promotion_packs')->where('company_id', $user->company_id)->get();

            $exchange 			= DB::table('exchange')->where('currency_from',$outlet->company_old_currency)->where('currency_to',$currency_to)->where('company_id',$user->company_id)->first();
            $outlet  			= DB::table('settings')->where('id',$user->company_id)->first();

            if($exchange):
            	//Menu
            	foreach($menus as $item):
            		$item_price = ($item->price * $exchange->price_to)/$exchange->price_from;
	            	DB::table('menus')->where('id',$item->id)->update(['price' => $item_price, 'currency' => $outlet->company_currency]);
            	endforeach;

            	//Addon
            	foreach($addons as $item):
            		$item_price = ($item->price * $exchange->price_to)/$exchange->price_from;
	            	DB::table('menu_materials')->where('id',$item->id)->update(['price' => $item_price, 'currency' => $outlet->company_currency]);
            	endforeach;

            	//discount
            	foreach($discounts as $item):
            		$item_price = ($item->discount_amount * $exchange->price_to)/$exchange->price_from;
	            	DB::table('discounts')->where('id',$item->id)->update(['discount' => $item_price]);
            	endforeach;

            	//promotion_pack
            	foreach($promotion_packs as $item):
            		$item_price = ($item->price * $exchange->price_to)/$exchange->price_from;
	            	DB::table('promotion_packs')->where('id',$item->id)->update(['discount_amount' => $item_price]);
            	endforeach;

            	//promotion_type
            	foreach($promotion_types as $item):
	            	if($item->promotion_price != null):
	            		$item_price = ($item->price * $exchange->price_to)/$exchange->price_from;
		            	DB::table('menus')->where('id',$item->id)->update(['price' => $promotion_price]);
		            endif;
            	endforeach;

            	DB::table('exchange')->where('company_id',$user->company_id)->delete();

            	CRUDBooster::redirect($_SERVER['HTTP_REFERER'],trans("setting.The company information has been updated !"),"info");

            	

            else:

            	CRUDBooster::redirect($_SERVER['HTTP_REFERER'],"The company currency cannot be updated because there is no exchange rate !","warning");

        	endif;

            

	    }




	}