<?php namespace App\Http\Controllers;

    use Session;
    use App\InvoiceModel;
    use App\InvoiceOrderModel;
    use Illuminate\Http\Request;
    use DB;
    use CRUDBooster;
    use URL;
    use Carbon\Carbon;
    use Response;
    use File;
    use Dompdf\Dompdf;
    use App\Events\RealTime;
    use App\Events\OrderEvent;
    use App\Events\UnmergeEvent;
    

    class CashierCreateOrderController extends \crocodicstudio\crudbooster\controllers\CBController {

    public function cbInit() {
        # START CONFIGURATION DO NOT REMOVE THIS LINE
        $this->title_field          = "id";
        $this->limit                = "20";
        $this->orderby              = "id,desc";
        $this->global_privilege     = false;
        $this->button_table_action  = false;
        $this->button_bulk_action   = true;
        $this->button_action_style  = "button_icon";
        $this->button_add           = false;
        $this->button_edit          = true;
        $this->button_delete        = true;
        $this->button_detail        = true;
        $this->button_show          = false;
        $this->button_filter        = true;
        $this->button_import        = false;
        $this->button_export        = false;
        $this->table                = "table_numbers";
        # END CONFIGURATION DO NOT REMOVE THIS LINE
    }
    
    // =================================== Create Order View ========================================
    public function getIndex()
    {

        $req = request()->all();
        
        $myID                       = CRUDBooster::myId();
        $user                       = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $data                       = [];
        $data['page_title']         = trans("order.Create Order");
        $data['invoice_id']         = DB::table('invoices')->where('company_id',$user->company_id)->max('invoice_number');
        $data['tables']             = DB::table('table_numbers')->where('company_id',$user->company_id)->where('status','1')->get();
        $data['categories']         = DB::table('categories')->where('company_id',$user->company_id)->where('status','1')->orderBy('name','ASC')->get();
        $data['subcategories']      = DB::table('subcategories')->where('company_id',$user->company_id)->where('status','1')->orderBy('name','ASC')->get();
        $data['menus']              = DB::table('menus')->where('company_id',$user->company_id)->where('status','1')->orderBy('name','ASC')->get();
        $data['user']               = DB::table('cms_users')->where('id',$myID)->first();
        $data['tax']                = DB::table('settings')->where('settings.id',$user->company_id)->select('company_sale_tax')->first();
        $data['com_tax']            = DB::table('settings')->where('settings.id',$user->company_id)->select('company_sale_tax')->get();
        
        $data['company_currency']   = DB::table('settings')->where('settings.id',$user->company_id)
                                    ->leftjoin('currencies','currencies.id','=','settings.company_currency')
                                    ->select('company_currency','currency_symbol')->first();
        $company_currency           = DB::table('settings')->where('id',$user->company_id)->select('company_currency')->first();
        $currency                   = DB::table('currencies')->get();
        $data['currencies']         = DB::table('currencies')
                                        ->leftjoin('exchange','exchange.currency_to','=','currencies.id')
                                        ->select('currencies.*','exchange.*','currencies.id as id')
                                        ->where('exchange.company_id',$user->company_id)->where('exchange.status','1')
                                        ->OrderBy('currencies.id','ASC')->get();
        $data['other_exchange']     = DB::table('exchange')
                                        ->where('company_id',$user->company_id)->where('currency_from',$company_currency->company_currency)->get();
        $data['setting']            = DB::table('settings')
                                        ->leftjoin('countries','countries.id','=','settings.company_country')
                                        ->leftjoin('currencies','currencies.id','=','settings.company_currency')
                                        ->where('settings.id',$user->company_id)->first();
        $data['order_type']         = $req['order_type']; // DO NOT CHANGE


        $this->cbView('order.create_order', $data);
    }

    // =================================== Update Order View ========================================
    public function getUpdate(Request $request)
    {   
        $req = request()->all();
        $myID                       = CRUDBooster::myId();
        $user                       = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $table_id                   = $request->table_id;
        $table_name                 = $request->table_name;

        $data                       = [];
        $data['page_title']         = trans("order.Create Order");
        $data['tables']             = DB::table('table_numbers')->where('company_id',$user->company_id)->where('status','1')->where('id',$table_id)->get();
        $data['categories']         = DB::table('categories')->where('company_id',$user->company_id)->where('status','1')->orderBy('name','ASC')->get();
        $data['subcategories']      = DB::table('subcategories')->where('company_id',$user->company_id)->where('status','1')->orderBy('name','ASC')->get();
        $data['menus']              = DB::table('menus')->where('company_id',$user->company_id)->where('status','1')->orderBy('name','ASC')->get();
        $data['user']               = DB::table('cms_users')->where('id',$myID)->first();
        $data['tax']                = DB::table('settings')->where('settings.id',$user->company_id)->select('company_sale_tax')->first();
        $data['company_currency']   = DB::table('settings')->where('settings.id',$user->company_id)
                                    ->leftjoin('currencies','currencies.id','=','settings.company_currency')
                                    ->select('company_currency','currency_symbol')->first();
        $company_currency           = DB::table('settings')->where('settings.id',$user->company_id)->select('company_currency')->first();
        $currency                   = DB::table('currencies')->get();
        $data['currencies']         = DB::table('currencies')
                                    ->leftjoin('exchange','exchange.currency_to','=','currencies.id')
                                    ->select('currencies.*','exchange.*','currencies.id as id')
                                    ->where('exchange.company_id',$user->company_id)
                                    ->where('exchange.status','1')
                                    ->OrderBy('currencies.id','ASC')->get();
        $data['other_exchange']     = DB::table('exchange')
                                    ->where('company_id',$user->company_id)
                                    ->where('currency_from',$company_currency->company_currency)->get();
        $data['setting']            = DB::table('settings')
                                        ->leftjoin('countries','countries.id','=','settings.company_country')
                                        ->leftjoin('currencies','currencies.id','=','settings.company_currency')
                                        ->where('settings.id',$user->company_id)->first();
        $data['invoice_id']         = DB::table('invoices')->where('company_id',$user->company_id)->where('table_id',$table_id)->where('has_paid','0')->first();
        $data['order_type']         = $req['order_type']; // DO NOT CHANGE
        $data['invoice_id']         = $req['invoice_id']; // DO NOT CHANGE
        $data['type']         = $req['type']; // DO NOT CHANGE
        $data['exist_order']        = true;

        $this->cbView('order.create_order',$data);
    }

    // =================================== Create Order : Dine In (View) ========================================
    public function getIndex_takeout()
    {

        $req = request()->all();
        $myID                       = CRUDBooster::myId();
        $user                       = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $data                       = [];
        $data['page_title']         = trans("order.Create Order");
        $data['invoice_id']         = DB::table('invoices')->where('company_id',$user->company_id)->max('invoice_number');
        $data['tables']             = DB::table('table_numbers')->where('company_id',$user->company_id)->where('status','1')->get();
        $data['categories']         = DB::table('categories')->where('company_id',$user->company_id)->where('status','1')->orderBy('name','ASC')->get();
        $data['subcategories']      = DB::table('subcategories')->where('company_id',$user->company_id)->where('status','1')->orderBy('name','ASC')->get();
        $data['menus']              = DB::table('menus')->where('company_id',$user->company_id)->where('status','1')->orderBy('name','ASC')->get();
        $data['user']               = DB::table('cms_users')->where('id',$myID)->first();
        $data['tax']                = DB::table('settings')->where('settings.id',$user->company_id)->select('company_sale_tax')->first();
        $data['com_tax']            = DB::table('settings')->where('settings.id',$user->company_id)->select('company_sale_tax')->get();
        
        $data['company_currency']   = DB::table('settings')->where('settings.id',$user->company_id)
                                    ->leftjoin('currencies','currencies.id','=','settings.company_currency')
                                    ->select('company_currency','currency_symbol')->first();
        $company_currency           = DB::table('settings')->where('id',$user->company_id)->select('company_currency')->first();
        $currency                   = DB::table('currencies')->get();
        $data['currencies']         = DB::table('currencies')
                                        ->leftjoin('exchange','exchange.currency_to','=','currencies.id')
                                        ->select('currencies.*','exchange.*','currencies.id as id')
                                        ->where('exchange.company_id',$user->company_id)->where('exchange.status','1')
                                        ->OrderBy('currencies.id','ASC')->get();
        $data['other_exchange']     = DB::table('exchange')
                                        ->where('company_id',$user->company_id)->where('currency_from',$company_currency->company_currency)->get();
        $data['setting']            = DB::table('settings')
                                        ->leftjoin('countries','countries.id','=','settings.company_country')
                                        ->leftjoin('currencies','currencies.id','=','settings.company_currency')
                                        ->where('settings.id',$user->company_id)->first();
        $data['time']               = time();
        $data['order_type']         = $req['order_type']; // DO NOT CHANGE

        $this->cbView('order.create_order',$data);
    }

    // =================================== Create Order : Take Out (View) ========================================
    public function getUpdate_takeout(Request $request)
    {   
        $myID                       = CRUDBooster::myId();
        $user                       = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $table_id                   = $request->table_id;
        $table_name                 = $request->table_name;

        $data                       = [];
        $data['page_title']         = trans("order.Create Order");
        $data['tables']             = DB::table('table_numbers')->where('company_id',$user->company_id)->where('status','1')->where('id',$table_id)->get();
        $data['categories']         = DB::table('categories')->where('company_id',$user->company_id)->where('status','1')->orderBy('name','ASC')->get();
        $data['subcategories']      = DB::table('subcategories')->where('company_id',$user->company_id)->where('status','1')->orderBy('name','ASC')->get();
        $data['menus']              = DB::table('menus')->where('company_id',$user->company_id)->where('status','1')->orderBy('name','ASC')->get();
        $data['user']               = DB::table('cms_users')->where('id',$myID)->first();
        $data['tax']                = DB::table('settings')->where('settings.id',$user->company_id)->select('company_sale_tax')->first();
        $data['company_currency']   = DB::table('settings')->where('settings.id',$user->company_id)
                                    ->leftjoin('currencies','currencies.id','=','settings.company_currency')
                                    ->select('company_currency','currency_symbol')->first();
        $company_currency           = DB::table('settings')->where('settings.id',$user->company_id)->select('company_currency')->first();
        $currency                   = DB::table('currencies')->get();
        $data['currencies']         = DB::table('currencies')
                                    ->leftjoin('exchange','exchange.currency_to','=','currencies.id')
                                    ->select('currencies.*','exchange.*','currencies.id as id')
                                    ->where('exchange.company_id',$user->company_id)
                                    ->where('exchange.status','1')
                                    ->OrderBy('currencies.id','ASC')->get();
        $data['other_exchange']     = DB::table('exchange')
                                    ->where('company_id',$user->company_id)
                                    ->where('currency_from',$company_currency->company_currency)->get();
        $data['setting']            = DB::table('settings')
                                        ->leftjoin('countries','countries.id','=','settings.company_country')
                                        ->leftjoin('currencies','currencies.id','=','settings.company_currency')
                                        ->where('settings.id',$user->company_id)->first();
        $data['invoice_id']         = DB::table('invoices')->where('company_id',$user->company_id)->where('table_id',$table_id)->where('has_paid','0')->first();
        $data['exist_order']        = true;
        $data['time']               = time();

        $this->cbView('order.create_order',$data);
    }


    // =================================== Get Order : Show it on the list ========================================
    public function getOrder(Request $request){

        // $order_type     = $request->order_type;
        $req = request()->all();

        $tableID        = $request->tableid;
        $invoiceId      = $request->invoiceId;
        $orderData      = $request->orderData;
        $newInvoiceId   = '';

        if ($req['order_type'] == 'takeout') {
            
            $takeOut = InvoiceModel::whereInvoiceId($req['invoiceId'])
                ->whereHasPaid(0)
                ->first();
            $tableID = 'takeout-' . $takeOut->file_name;
        }

        if ($tableID):
            $url = File::get((string)$tableID."-ticket.json");
            $order      = json_decode($url, true);



        // Foreach for List data order from json to list in order page
        foreach($order['items'] as $orders): ?>
            <tr class="menu_data_in_table">
                <td class="m_id" data-val="<?php echo $orders['item_id']; ?>" style="display:none;"><?php echo $orders['item_id']; ?></td>
                <td class="m_name" 
                    data-val="<?php echo $orders['item_id']; ?>" 
                    data-name="<?php echo $orders['item_name']; ?>" 
                    data-print="<?php echo $orders['print_status']; ?>" 
                    data-qty="<?php echo $orders['item_qty']; ?>" >
                    <?php echo $orders['item_name']; ?>
                    
                    <span class="m_size_name" style="display:block"><?php echo $orders['item_size_name'] ; ?></span>  
                </td>

                <td class="m_qty"> 
                    <span class="set_minus" data-val="1"><i class="glyphicon glyphicon-minus"></i></span>
                    <span class="m_qty_data"><?php echo $qty = $orders['item_qty']; ?></span>
                    <span class="set_plus" data-val="1"><i class="glyphicon glyphicon-plus"></i></span>
                </td>

                <td class="m_amount"><?php echo $amount = number_format($orders['item_amount'], 2, '.', ''); ?></td>
                <td class="m_subamount"><?php echo number_format($amount * $qty, 2, '.', '');?></td>
                <td><span class="item_note" style="display:none"></span>
                    <span class="remove-this" data-id="" title="Remove"><i class="glyphicon glyphicon-remove"></i></span>
                </td>
            </tr>
    
            <?php foreach(array_filter($orders['addons']) as $addon) : ?>
                <tr class="material_data_add material_with_price" data-parent="<?= $orders['item_id']; ?>" data-size="<?= $orders['item_size']; ?>">
                    <td class="menu_id" material-val="<?= $addon->addon_id; ?>" style="display:none;"></td>
                    <td class="material_id" material-val="1" style="display:none;"></td>
                    <td style="width:25vh; color:#00afe1; font-size:1.4vh;" class="material_name" data-val="1">
                        <?= $addon['addon_name']; ?>
                    </td>
                    <td class="m_qty">
                        <span class="set_minus" data-val="1"><i class="glyphicon glyphicon-minus"></i></span>
                        <span class="m_qty_data"><?= $addon['addon_qty']; ?></span>
                        <span class="set_plus" data-val="1"><i class="glyphicon glyphicon-plus"></i></span>
                    </td>
                    <td class="m_amount"><?php echo number_format($addon['addon_amount'], 2, '.', '');?></td>
                    <td class="m_subamount"><?= number_format((int) $addon['addon_qty'] * $addon['addon_amount'], 2); ?></td>
                    <td>
                        <span class="item_note" style="display:none"></span>
                        <span class="remove-this" data-id="" title="Remove">
                            <i class="glyphicon glyphicon-remove"></i>
                        </span>
                    </td>
                </tr>
            <?php endforeach; ?>
            
        <?php endforeach;
        endif;// Endif of Json reader

        

    }

    

    // =================================== Search Category ========================================
    public function show_category(Request $request){
        $myID             = CRUDBooster::myId();
        $user             = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $search_data      = $request->search_data;

        if($search_data == null):
            $categories = DB::table('categories')->where('status',1)->where('company_id',$user->company_id)->orderBy('name','ASC')->get();  
        else:
            $categories = DB::table('categories')->where('status',1)->where('company_id',$user->company_id)->where('name','LIKE','%' . $search_data . '%')->orderBy('name','ASC')->get();
        endif;
        ?>
        <a class="category_code" data-val="0">
            <div class="category_item item-0">
                <?php echo trans('order.most_sold'); ?>
            </div>
        </a>

        <!-- <a class="category_code" data-val="99999">
            <div class="category_item item-99999">
                <?php echo trans('order.addon'); ?>
            </div>
        </a> -->

        <?php foreach($categories as $category): ?>
            <a class="category_code" data-val="<?php echo $category->id; ?>">
                <div class="category_item item-<?php echo $category->id; ?>">
                   <?php echo $category->name; ?>
                </div>
            </a>
        <?php endforeach;
    }

    // =================================== Search Subcategory ========================================
    public function show_subcategory(Request $request){
        $myID             = CRUDBooster::myId();
        $user             = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $search_data      = $request->search_data;

        if($search_data == null):
            $subcategories      = DB::table('subcategories')->where('status',1)->where('company_id',$user->company_id)->orderBy('name','ASC')->get();   
        else:
            $subcategories      = DB::table('subcategories')->where('status',1)->where('company_id',$user->company_id)->orderBy('name','ASC')
                                    ->where('name','LIKE','%' . $search_data . '%')->get();
        endif;

        foreach($subcategories as $subcategory): ?>
        <a class="subcategory_code" data-val="<?php echo $subcategory->id; ?>">
            <div class="subcategory_item item-<?php echo $subcategory->id; ?>" >
                    <?php echo $subcategory->name; ?>
            </div>
        </a>
        <?php endforeach;
    }

    // =================================== Search Menu ========================================
    public function show_menu(Request $request){
        $myID               = CRUDBooster::myId();
        $user               = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $search_data        = $request->search_data;
        $url                = URL::to("/");


        if($search_data == null):
        $menus = DB::table('menus')
                ->where('status',1)
                ->where('company_id',$user->company_id)
                ->orderBy('name','ASC')->get();   
        else:
        $menus = DB::table('menus')->where('status',1)
                ->where('company_id',$user->company_id)
                ->where('name','LIKE','%' . $search_data . '%')
                ->orWhere(function($query) use ($search_data) {
                    $menuCode = str_replace('#', '', $search_data);
                    $query->where('menu_code', 'LIKE' , '%' . $menuCode . '%');
                })
                ->orderBy('name','ASC')->get();
        endif;


        foreach($menus as $menu): ?>
        <div class="menu_item"> 
            <?php if($menu->uom_size > 0): ?>
                <span class="have_size"></span>
            <?php endif;

            if($menu->material_size > 0): ?>
                <span class="have_material"></span>
            <?php endif; ?>
            
            <a class="menu_code" data-val="<?php echo $menu->id; ?>" data-name="<?php echo $menu->name; ?>" data-amount="<?php echo $menu->price; ?>" 
                data-size="<?php echo $menu->uom_size; ?>" >
                    <img class="menu_image" src="<?php echo $url .'/'. $menu->photo; ?> "/>
                    <div class="menu_code_number">
                        <?= !empty($menu->menu_code) ? '#' . $menu->menu_code : $menu->menu_code ?>
                    </div>
                    <div class="menu_name"><?php echo mb_substr($menu->name,0,14,'utf-8'); ?></div>
            </a>
        </div>
        <?php endforeach;
    }

    // =================================== Show All subcategory (on specific category click) ========================================
    public function show_subcategory_by_category(Request $request){
        $myID               = CRUDBooster::myId();
        $user               = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $category_id        = $request->category_id;
        
        if($category_id != null):
            $subcategories  = DB::table('subcategories')
                            ->where('status',1)->where('category_id',$category_id)->where('company_id',$user->company_id)
                            ->orderBy('name','ASC')->get();
        else:
            $subcategories  = DB::table('subcategories')
                            ->where('status',1)->where('company_id',$user->company_id)
                            ->orderBy('name','ASC')->get();

        endif;
        
        foreach ($subcategories as $subcategory): ?>
            <a class="subcategory_code" data-val="<?php echo $subcategory->id; ?>">
                <div class="subcategory_item item-<?php echo $subcategory->id; ?>">    
                    <?php echo $subcategory->name; ?>
                </div>
            </a>     
        <?php endforeach;
    }

    // =================================== Show All menu (on specific category click) ========================================
    public function show_menu_by_category_subcategory(Request $request){
        $myID           = CRUDBooster::myId();
        $user           = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $category_id    = $request->category_id;
        $subcategory_id = $request->subcategory_id;
        $url            = URL::to("/");

        if($category_id != null && $category_id != 0 && $category_id != 99999):
            $menus      = DB::table('menus')
                        ->where('status',1)->where('category_id',$category_id)->where('company_id',$user->company_id)
                        ->orderBy('name','ASC')->get();

        elseif($subcategory_id != null && $subcategory_id != 0):
            $menus      = DB::table('menus')
                        ->where('status',1)->where('subcategory_id',$subcategory_id)->where('company_id',$user->company_id)
                        ->orderBy('name','ASC')->get();

        elseif($category_id == 0):
            $menus      = DB::table('menus')->where('status',1)->where('company_id',$user->company_id)->orderBy('sale_amount','DESC')->get();
        
        elseif($category_id == 99999):
            $addons      = DB::table('menu_materials')->where('status','1')->where('company_id',$user->company_id)->orderBy('name','ASC')->get();

        else:
            $menus      = DB::table('menus')->where('status',1)->where('company_id',$user->company_id)->orderBy('name','ASC')->get();

        endif;
        
        if(!$addons):
            foreach($menus as $menu): ?>
                    <div class="menu_item">
                        <?php if($menu->uom_size > 0): ?>
                            <span class="have_size"></span>
                        <?php endif; ?>

                        <?php if($menu->material_size > 0): ?>
                            <span class="have_material"></span>
                        <?php endif; ?>
                        <a class="menu_code" data-val="<?php echo $menu->id; ?>" data-name="<?php echo $menu->name; ?>" data-amount="<?php echo $menu->price; ?>" data-size="<?php echo $menu->uom_size; ?>">

                        <img class="menu_image" src="<?php echo $url .'/'. $menu->photo; ?> "/>
                        <div class="menu_code_number">
                            <?= !empty($menu->menu_code) ? '#' . $menu->menu_code : $menu->menu_code ?>
                        </div>
                        <div class="menu_name"><?php echo mb_substr($menu->name,0,14,'utf-8'); ?></div>
                        </a>
                    </div> 
            <?php endforeach;
        else:
            foreach($addons as $addon): ?>
                    <div class="menu_item">     
                        <a class="menu_code" data-val="<?php echo $addon->id; ?>" data-name="<?php echo $addon->name; ?>" data-amount="<?php echo $addon->price; ?>" data-size="">

                        <img class="menu_image" src="<?php echo $url .'/'. $addon->photo; ?>">
                        <div class="menu_code_number">
                            <?= !empty($menu->menu_code) ? '#' . $menu->menu_code : $menu->menu_code ?>
                        </div>
                        <div class="menu_name"><?php echo mb_substr($addon->name,0,14,'utf-8'); ?></div>
                        </a>
                    </div>
            <?php endforeach;
        endif;
    }

    // =================================== Show All menu (on specific subcategory click) ========================================
    public function get_menu_type_with_subcategory(Request $request){
        $myID = CRUDBooster::myId();
        $user = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        
        $category_id    = $request->category_id;
        $id             = $request->id;

        $url            = URL::to("/");

        if($id != null):
        $menus          = DB::table('menus')
                        ->where('status',1)->where('subcategory_id',$id)->where('company_id',$user->company_id)
                        ->orderBy('name','ASC')->get();

        elseif($category_id != null):
            $menus      = DB::table('menus')
                        ->where('status',1)->where('category_id',$category_id)->where('company_id',$user->company_id)
                        ->orderBy('name','ASC')->get();

        else:
            $menus      = DB::table('menus')
                        ->where('status',1)->where('company_id',$user->company_id)
                        ->orderBy('name','ASC')->get();
        endif;

        foreach ($menus as $menu):  ?>
                <a class="menu_code" data-val="<?php echo $menu->id; ?>" data-name="<?php echo $menu->name; ?>" data-amount="<?php echo $menu->price; ?>" data-size="<?php echo $menu->uom_size; ?>">
                <div class="menu_item">
                        
                    <img class="menu_image" src="<?php echo $url .'/'. $menu->photo; ?> "/>
                    <div class="menu_name"><?php echo mb_substr($menu->name,0,14,'utf-8'); ?></div>
                      
                </div>
                </a>
                    
        <?php
        endforeach;
    }
   

    //insert material into menu
    public function show_material_for_menu(Request $request){
        $url            = URL::to("/");
        $myID           = CRUDBooster::myId();
        $user           = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $menu_id        = $request->menu_id;
        $menu_size_id   = $request->menu_size;
        $menu_note      = $request->menu_note;

        if($menu_id):
            $menu_data = DB::table('menus')
                        ->leftjoin('menu_uom_price','menu_uom_price.menu_id','=','menus.id')
                        ->Select('menus.*','menus.id as m_id','menu_uom_price.*','menu_uom_price.id as uom_id')
                        ->where('menus.id',$menu_id)->where('menu_uom_price.id', $menu_size_id)->first();

            $materials  = DB::table('menu_detail')
                        ->join('menu_materials','menu_materials.id','=','menu_detail.material_id')
                        ->join('menus','menus.id','=','menu_detail.menu_id')
                        ->select('menu_detail.*','menu_materials.id as menu_materials_id','menu_materials.*','menus.id as menu_id','menus.name as menu_name','menus.price as menu_price','menus.photo as menu_photo')
                        ->where('menu_materials.company_id',$user->company_id)->where('menu_detail.menu_id','=',$menu_id)->get();

            ?>


            <?php if($menu_data->uom): ?>
            <div class="item_detail">
                <span class="title" style="display: inline-block;">Size of menu : </span>
                <div class="menu_size active after_add" > 
                     <div class="menu_item_code" data-size="<?php echo $menu_data->uom_id; ?>" style="display:none;"></div>
                     <div class="menu_code_data"><?php echo $menu_data->uom; ?></div>
                </div>
            </div>

            <?php else: ?>
                <div class="item_detail">
                <span class="title" style="display: inline-block;">Size of menu : </span>
                    <div class="menu_size active after_add" > 
                             <div class="menu_item_code" data-size="0" style="display:none;"></div>
                             <div class="menu_code_data"><?php echo 'Normal'; ?></div>

                    </div>

            </div>
            <?php endif; ?>

            <!-- <?php if(count($materials) >= 1): ?>
            <input type="hidden" class="add_material_from_menu" value="1">
            <div class="material_main after_add_data">
                <span class="title">Material / More Option : </span>
                <?php foreach($materials as $material): ?>
                <div class="material_item" data-tick='0'> 
                        <input type="checkbox" style="display: none;" 
                        value="<?php echo $material->id;?>" 
                        menu-id="<?php echo $material->menu_id; ?>" 
                        data-name="<?php echo $material->name;?>" 
                        data-price="<?php echo $material->price;?>">
                        
                        <img class="material_tick hidden" src="<?php echo $url .'/' .'uploads/defualt_image/tick.png'; ?>"/>
                            <a class="material_code" data-val="<?php echo $material->id; ?>" data-name="<?php echo $material->name; ?>" data-amount="<?php echo $material->price; ?>">
                                <img class="material_image" src="<?php echo $url .'/'. $material->photo; ?>"/>
                                <div class="material_name"><?php echo $material->name; ?></div>
                            </a>
                </div>

                <?php endforeach; ?>
            </div>
            <?php endif; ?> -->
            
            
            <div class="item_note">
                <textarea name="item_note_data" class="form-control item_note_data" data-size="<?php echo $menu_id; ?>" placeholder="Customer Comment for this item"><?php echo $menu_note; ?></textarea>
            </div>
            <?php 
        endif;
    }

    //show discount to order
    public function check_discount_type(Request $request){
        $myID           = CRUDBooster::myId();
        $user           = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();

        $discount_type  = $request->discount_type;
        $discounts      = DB::table('discounts')->where('company_id',$user->company_id)->where("discount_type",$discount_type)->where('discount_user','<>','membership')->get();
        ?>
        
        <option value="0">Please choose discount</option>
        <?php foreach ($discounts as $discount): ?>
                    <option value="<?php echo $discount->id ;?>"><?php echo $discount->discount?></option>
        <?php endforeach;
    }




    public function get_promotion_pack_data(Request $request){
        $promotion_pack_id  = $request->promotion_pack_id;
        $myID           = CRUDBooster::myId();
        $user           = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();

        $promotion_pack = DB::table('promotion_packs')
                            ->leftjoin('promotion_type','promotion_type.id','=','promotion_packs.promotion_type_id')
                            ->leftjoin('menus','menus.id','=','promotion_packs.promotion_menu_id')
                            ->select('promotion_packs.*','promotion_packs.id as pro_id','promotion_type.name as promotion_type_name','menus.id as menu_id','menus.name as menu_name')
                            ->where('promotion_packs.id',$promotion_pack_id)
                            ->where('promotion_packs.status','1')->first();

        $promotion_pack_detail = DB::table('promotion_detail')
                                ->join('menus','menus.id','=','promotion_detail.menus_id')
                                ->leftjoin('currencies','currencies.name','=','menus.currency')
                                ->Select('promotion_detail.*','menus.*','menus.name as menu_name','currencies.*','menus.id as menu_id')
                                ->where('promotion_id',$promotion_pack->pro_id)->get();
        ?>

        <table class="promotion_pack_info_table">
            <tr class="promotion_packs_detail_name">
                <td>Name</td>
                <td><?php echo $promotion_pack->promotion_name; ?></td>
            </tr>

            <tr class="promotion_packs_detail_start_date">
                <td>Start Date</td>
                <td><?php echo $promotion_pack->start_date; ?></td>
            </tr>

            <tr class="promotion_packs_detail_end_date">
                <td>End Date</td>
                <td>
                    <?php if($promotion_pack->end_date == null): ?>
                    No End Date
                    <?php else: ?>
                    <?php echo $promotion_pack->end_date; ?>
                    <?php endif; ?>
                </td>
            </tr>

            <tr class="promotion_packs_detail_condition">
                <td>Condition</td>
                <td>
                    <?php echo  $promotion_pack->promotion_type_name; ?><?php $promotion_type = $promotion_pack->promotion_type_id; ?>
                    <?php 
                        if      ($promotion_type == '1'):
                                    echo '(customer whom is a member in our resturant)';
                        elseif  ($promotion_type == '2'):
                                    echo '(customer whom is spending over or equal to <b>' . $promotion_pack->promotion_price .'</b>)';
                        elseif  ($promotion_type == '3'):
                                    echo '(customer whom is ordering specific food: <b>'. $promotion_pack->menu_name .'</b> with <b>' . $promotion_pack->promotion_qty . '</b> quantity)';
                        elseif  ($promotion_type == '4'):
                                    echo '(customer whom is coming in on <b>'. $promotion_pack->promotion_event .'</b> event)';
                        endif;

                    ?>
                </td>
            </tr>

            <tr class="promotion_packs_detail_free">
                <td>Free Type</td>
                <td>
                    <?php $discount_type = $promotion_pack->discount_type;?>
                    <?php if($discount_type != null):
                        echo 'discount';
                    else:
                        echo 'Free Item';
                    endif;
                    ?>
                </td>
            </tr>

            <?php if($discount_type != null): ?>
            <tr class="promotion_packs_detail_amount">
                <td>Discount Amount</td>
                <td><?php echo $promotion_pack->discount_type;?><?php echo $promotion_pack->discount_amount; ?></td>
            </tr>
            <?php endif;?>



        </table>
        <?php if($discount_type == null):?>
        <table class="promotion_packs_detail">
            <tr class="promotion_packs_detail_title">
                <th>No</th>
                <th>Item</th>
                <th>Quantity</th>
                <th>Price</th>
            </tr>

            
            
                <?php 
                    foreach($promotion_pack_detail as $key => $pro_detail): ?>
                    <tr class="promotion_packs_detail_free_item">
                    <td><?php echo ++$key ?></td>
                    <td><?php echo $pro_detail->menu_name; ?></td>
                    <td><?php echo $pro_detail->amount;?></td>
                    <td><?php echo $pro_detail->currency_symbol;?><?php echo $pro_detail->price; ?></td>
                    </tr>
                    <?php endforeach;
                ?>
                        
            
            
        </table>
    <?php endif; ?> 

    <?php               
    }

    public function get_promotion_pack_data_change(Request $request){
        $promotion_pack_id  = $request->promotion_pack_id;
        if($promotion_pack_id == 0 || $promotion_pack_id == null){
            return;
        }

        $myID           = CRUDBooster::myId();
        $user           = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();

        $promotion_pack = DB::table('promotion_packs')
                            ->leftjoin('promotion_type','promotion_type.id','=','promotion_packs.promotion_type_id')
                            ->leftjoin('menus','menus.id','=','promotion_packs.promotion_menu_id')
                            ->select('promotion_packs.*','promotion_packs.id as pro_id','promotion_type.name as promotion_type_name','menus.id as menu_id','menus.name as menu_name')
                            ->where('promotion_packs.id',$promotion_pack_id)
                            ->where('promotion_packs.status','1')->first();

        $promotion_pack_detail = DB::table('promotion_detail')
                                ->join('menus','menus.id','=','promotion_detail.menus_id')
                                ->leftjoin('currencies','currencies.name','=','menus.currency')
                                ->Select('promotion_detail.*','menus.*','menus.name as menu_name','currencies.*','menus.id as menu_id')
                                ->where('promotion_id',$promotion_pack->pro_id)->get();
    

        $discount_type = $promotion_pack->discount_type;

        if($discount_type != null): ?>
            <input type="text" name="discount_type" id="promotion_pack_discount_type" value="<?php echo $promotion_pack->discount_type; ?>"> 
            <input type="hidden" name="discount_amount" id="promotion_pack_discount_amount" value="<?php echo $promotion_pack->discount_amount;?>" > 
        
        <?php 
        else: 
            foreach($promotion_pack_detail as $key => $pro_detail): ?>
                    <tr class="promotion_packs_detail_free_item" style="background: #559e55; color:#fff;">
                        <td class="m_id" data-val="<?php echo $pro_detail->menu_id; ?>" style="display:none;"><?php echo $pro_detail->id; ?></td>
                        <td data-val="<?php echo $pro_detail->menu_id; ?>"><?php echo $pro_detail->menu_name; ?></td>
                        <td class="m_qty">
                            <span class="m_qty_data"><?php echo $pro_detail->amount;?></span>
                        </td>
                        <td class="m_amount"><?php echo $pro_detail->currency_symbol; echo $pro_detail->price; ?></td>
                        <td class="m_subamount"><?php echo "Free"; ?></td>
                        <td></td>
                    </tr>
            <?php endforeach;

        endif;



    }



    //currency deposit
    public function currency_deposit(Request $request)
    {
        $myID           = CRUDBooster::myId();
        $user           = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();

        $company_currency           = $request->company_currency;
        $input_currency             = $request->input_currency;
        
        $currency_amount            = $request->currency_amount;
        $total                      = $request->total_with_tax;

        if($input_currency == $company_currency):
            if($currency_amount == ''):
            $convert_amount         = $total - 0;
            else:
            $convert_amount         = $total - $currency_amount;
            endif;

        else:
        $exchange               = DB::table('exchange')
                                    ->where('company_id',$user->company_id)->where('currency_from_text',$input_currency)->where('currency_to_text',$company_currency)->first();

            if($currency_amount != '' || $currency_amount !=0):
                $convert_amount     = $total - ($currency_amount * $exchange->price_to) / $exchange->price_from;
            else:
                $convert_amount     = $total;
            endif;

        endif;
        
        if($convert_amount < 0):
            echo "0";
        else:
            echo $convert_amount;
        endif;

        
    }

    //show menu size and data
    public function show_menu_data(Request $request){
        $url            = URL::to("/");
        $myID           = CRUDBooster::myId();
        $user           = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $menu_id        = $request->menu_id;

        if($menu_id):
            $menu_data = DB::table('menus')
                        ->leftjoin('menu_uom_price','menu_uom_price.menu_id','=','menus.id')
                        ->Select('menus.*','menus.id as m_id','menu_uom_price.*','menu_uom_price.id as uom_id')
                        ->where('menus.id', $menu_id)->get();

            $materials  = DB::table('menu_detail')
                        ->join('menu_materials','menu_materials.id','=','menu_detail.material_id')
                        ->join('menus','menus.id','=','menu_detail.menu_id')
                        ->select('menu_detail.*','menu_materials.id as menu_materials_id','menu_materials.*','menus.id as menu_id','menus.name as menu_name','menus.price as menu_price','menus.photo as menu_photo')
                        ->where('menu_materials.company_id',$user->company_id)->where('menu_detail.menu_id','=', $menu_id)->get();


            ?>


            <?php if(count($menu_data)):?>
            <div class="item_detail">
                
                
                <span class="title">Size of menu : </span>

                <div class="menu_detail">
                    <?php foreach($menu_data as $key => $menu): ?>
                        <?php if($menu->uom): ?>
                        <div class="menu_size <?= ($key == 0) ? 'active' : ''; ?>"> 
                                <div class="menu_item_code" data-val="<?php echo $menu->m_id; ?>" data-name="<?php echo $menu->name ;?>" data-size="<?php echo $menu->uom_id; ?>" data-size-name="<?php echo $menu->uom; ?>" data-material="<?php echo $menu->material_size;?>" data-amount="<?php echo $menu->uom_price?>">
                                    <div class="menu_code_data"><?php echo $menu->uom; ?></div>
                                </div>
                        </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
                

            </div> <!-- End of size of menu -->
            <?php endif; ?>

            <?php if(count($materials) >= 1): ?>
            

            <div class="material_main">
                <span class="title">Material / More Option : </span>
                <?php foreach($materials as $material): ?>
                <div class="material_item" data-tick='0'> 
                        <input type="checkbox" style="display: none;" 
                        value="<?php echo $material->id;?>" 
                        menu-id="<?php echo $material->menu_id; ?>" 
                        data-name="<?php echo $material->name;?>"
                        data-size="<?php echo $menu->uom_id; ?>" 
                        data-price="<?php echo $material->price;?>">
                        
                        <img class="material_tick hidden" src="<?php echo $url .'/' .'uploads/defualt_image/tick.png'; ?>"/>
                            <a class="material_code" data-val="<?php echo $material->id; ?>" data-name="<?php echo $material->name; ?>" data-amount="<?php echo $material->price; ?>">
                                <img class="material_image" src="<?php echo $url .'/'. $material->photo; ?>"/>
                                <div class="material_name"><?php echo $material->name; ?></div>
                            </a>
                </div>

                <?php endforeach; ?>
            <?php endif; ?>
            </div>
            



            <div class="item_note_main" style="display: none;">
                <textarea name="item_note_data" class="form-control item_note_data" placeholder="Customer Comment for this item"></textarea>
            </div>

            <?php 
        endif;


    }




    public function show_menu_data_with_material(Request $request){
        $url            = URL::to("/");
        $myID           = CRUDBooster::myId();
        $user           = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $menu_id        = $request->menu_id;

        if($menu_id):

            $menu_data = DB::table('menus')
                        ->where('menus.id', $menu_id)->first();

            $materials  = DB::table('menu_detail')
                        ->join('menu_materials','menu_materials.id','=','menu_detail.material_id')
                        ->join('menus','menus.id','=','menu_detail.menu_id')
                        ->select('menu_detail.*','menu_materials.id as menu_materials_id','menu_materials.*','menus.id as menu_id','menus.name as menu_name','menus.price as menu_price','menus.photo as menu_photo')
                        ->where('menu_materials.company_id',$user->company_id)->where('menu_detail.menu_id','=', $menu_id)->get();


            ?>

            <?php if(count($materials) >= 1): ?>
            

            <div class="material_main" data-material="true" data-val="<?php echo $menu_data->id; ?>" data-name="<?php echo $menu_data->name; ?>" data-amount="<?php echo $menu_data->price; ?>">
                <span class="title">Material / More Option : </span>
                <?php foreach($materials as $material): ?>
                <div class="material_item"> 
                        <input type="checkbox" style="display: none;" 
                        value="<?php echo $material->id;?>" 
                        menu-id="<?php echo $material->menu_id; ?>" 
                        data-name="<?php echo $material->name;?>"
                        data-size="<?php echo $menu->uom_id; ?>" 
                        data-price="<?php echo $material->price;?>">
                        
                        <img class="material_tick hidden" src="<?php echo $url .'/' .'uploads/defualt_image/tick.png'; ?>"/>
                            <a class="material_code" data-val="<?php echo $material->id; ?>" data-name="<?php echo $material->name; ?>" data-amount="<?php echo $material->price; ?>">
                                <img class="material_image" src="<?php echo $url .'/'. $material->photo; ?>"/>
                                <div class="material_name"><?php echo $material->name; ?></div>
                            </a>
                </div>

                <?php endforeach; ?>
            <?php endif; ?>
            </div>

            <div class="item_note_main" style="display: none;">
                <textarea name="item_note_data" class="form-control item_note_data" placeholder="Customer Comment for this item"></textarea>
            </div>

            <?php 
        endif;
    }


    // =================================== Find Membership (Discount Membership Modal) ========================================
    public function find_membership(Request $request){
        $membership_no  = $request->membership_no;
        $current_total  = $request->current_total;
        $url            = URL::to("/");
        $myID           = CRUDBooster::myId();
        $user           = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();

        $member                 = DB::table('memberships')->where('membership_no',$membership_no)->where('status','1')->first();
        $membership_promotion   = DB::table('discounts')->where('discount_user','membership')->where('company_id',$user->company_id)->where('status',1)->first(); 

            if($member): ?>
                <span class="tick_result">
                    <img src="<?php echo $url . '/' . 'uploads/defualt_image/checkmark.png'; ?>">
                    <span style="color:green">There is a member holding this card</span>
                </span>
                <div>
                    <table class="promotion_pack_info_table">
                        <tr>
                            <td>Name</td>
                            <td><?php echo $member->l_name . ' ' . $member->f_name; ?></td>
                        </tr>

                        <tr>
                            <td>Phone</td>
                            <td><?php echo $member->phone; ?></td>
                        </tr>

                        <?php if($member->expire_date != null): ?>
                        <tr> 
                            <td>Expire Date</td>
                            <td><?php echo $member->expire_date; ?></td>
                        </tr>
                        <?php endif; ?>

                        <tr>
                            <td>Customer's will get</td>
                            <td><?php echo "Discount" . '(' .$membership_promotion->discount . $membership_promotion->discount_type . ')'; ?></td>
                            <td style="display: none;">
                                <input type="hidden" class="membership_discount_type" value="<?php echo $membership_promotion->discount_type; ?>" />
                                <input type="hidden" class="membership_discount_amount" value="<?php echo $membership_promotion->discount; ?>" />
                            </td>
                        </tr>
                    </table>


                </div>  

                <div>
                    <button type="button" class="btn btn-default set_membership" data-dismiss="modal">Set Membership</button>
                </div>          
            <?php
            else: 
            ?>
                <span>
                    <img src="<?php echo $url . '/' . 'uploads/defualt_image/remove.png'; ?>">
                    <span style="color:red">There is no member holding this card</span>
                </span>   

            <?php
            endif;
    }

    // =================================== Set discount for membership (Discount Modal) ========================================
    public function set_membership(Request $request){
        $current_total  = $request->current_total;
        $myID           = CRUDBooster::myId();
        $user           = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();

        $membership_promotion   = DB::table('promotion_packs')
                                ->where('promotion_type_id',1)
                                ->where('company_id',$user->company_id)
                                ->where('promotion','discount')
                                ->where('status',1)->first();
        $after = $current_total - $membership_promotion->discount_amount;
        if($after <= 0): return 0;
        else:            return $after;
        endif;
    }

    // =================================== Show available discount (Discount Modal) ========================================
    public function order_discount_rate(Request $request){
        $discount_type  = $request->discount_type;
        $myID           = CRUDBooster::myId();
        $companyId  = getCompanyId($myID);
        
        $discounts      = DB::table('discounts')
            ->where('company_id', $companyId)
            ->where('discount_type', $discount_type)
            ->get();

        ?>

        <option value="0">Please select discount rate</option>
        <?php foreach($discounts as $discount): ?>
                <option data-value="<?= $discount->id; ?>" value="<?php echo $discount->discount;?>"><?php echo $discount->discount; ?></option>
        <?php endforeach;
    }

    // =================================== Enable Button discount (Discount Modal) ========================================
    public function enable_discount(Request $request){
        $discount = $request->discount;
        if($discount):?>
            <button type="button" class="btn btn-default set_discount" data-dismiss="modal">Set Discount</button>
        <?php else:
            return null;
        endif;  
    }

    // =================================== Automatically add promotion pack deal to order (No Complete) ========================================
    public function promotion_pack_deal(Request $request){
        $current_total  = $request->current_total;
        $m_id           = $request->m_id;

        $myID           = CRUDBooster::myId();
        $user           = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $now            = Carbon::now();
        $today_date     = $now->toDateString();
        

        $promotion_packs = DB::table('promotion_packs')->where('company_id',$user->company_id)->where('status','1')->get(); 

        foreach($promotion_packs as $pro):
            //price
            if($pro->promotion_type_id == '2'):

                    if($pro->promotion_price <= $current_total): ?>
                         <option value="<?php echo $pro->id; ?>"> <?php echo $pro->promotion_name;?> </option>
                    <?php endif;
            //event
            elseif($pro->promotion_type_id == '4'):

                    if($pro->start_date <= $today_date && ($pro->end_date > $today_date || $pro->end_date == NULL)): ?>
                        <option value="<?php echo $pro->id; ?>"> <?php echo $pro->promotion_name;?> </option>
                    <?php endif;
            endif;
        endforeach;

    }

    // =================================== Automatically add promotion pack deal to order (Not Complete) ========================================
    public function set_promotion(Request $request){
        $current_total = $request->current_total;
        $p_id = $request->promotion_id;
    }

    // ==== Generate Invoice Before Paid ======
    // This function uses when we print only before get paid
    public function generate_invoice(Request $request){

        $req = $request->all();

        $myID = CRUDBooster::myId();
        $companyId = getCompanyId($myID);
        $hundredPercent = 100;

        $originalPrice = $req['g_total'];

        // Check if had discount
        if (!empty($req['discount_id'])) {

            $discountType = checkDiscount($req['discount_id'])->discount_type;
            
            $discountValue = checkDiscount($req['discount_id'])->discount;
            
            $originalPrice = $req['g_total'] + $discountValue;
            
            $discountAmount = $discountValue;
            
            if ($discountType == '%') {
                $originalPrice = ($req['g_total'] * $hundredPercent) / ($hundredPercent - $discountValue);
            
                $discountAmount = (($originalPrice * $discountValue) / $hundredPercent);
            }

        }

        $taxAmount = ($req['g_total'] * getTaxValue($companyId) / $hundredPercent);
        $rielExchange = ($req['g_total'] + $taxAmount) * getExchangeRate($companyId, 'Riel')->price_to;
        $bahtExchange = ($req['g_total'] + $taxAmount) * getExchangeRate($companyId, 'Baht')->price_to;
    
        
        // Check and get currency withing setting
        $company_currency = DB::table('settings')
            ->select('company_currency','currency_symbol')
            ->leftjoin('currencies','currencies.id','=','settings.company_currency')
            ->where('settings.id', $companyId)->first();


        // Check order Number
        $checkOrderNo = InvoiceModel::whereDate('created_at', Carbon::today())
                ->whereCompanyId($companyId);
                
        // Count Order Number of Take Out
        $invoiceNumber = $checkOrderNo->count();
        $orderCode = $checkOrderNo->whereOrderType('dinein')->count();
        
        // Count Invoice number 

        $orderCode = ($orderCode < 9) ? '0' . ($orderCode+1) : $orderCode+1;

        $invoiceNumber = ($invoiceNumber < 9) ? '0' . ($invoiceNumber+1) : $invoiceNumber+1;
        
        $data = [
                'invoice_date' => Carbon::now()->toDateString(),
                'created_by' => $myID,
                'company_id' => $companyId,
                'table_id' => $req['table_id'],
                'order_type' => $req['order_type'],
                'order_code' => $orderCode,
                'invoice_number' => $invoiceNumber,
                'currency' => $company_currency->currency_symbol,
                'has_paid' => 0,
                'grand_total' => $originalPrice,
                'discount_type' => $discountType,
                'discount_value' => $discountValue,
                'discount' => $discountAmount,
                'tax' => getTaxValue($companyId),
                'tax_amount' => $taxAmount,
                'grand_riel_exchange' => $rielExchange,
                'grand_baht_exchange' => $bahtExchange,

            ];


        // Operation if dining in 
        if ($req['order_type'] == 'dinein') {

            $checkExisting = InvoiceModel::whereTableId($req['table_id'])
                ->whereHasPaid(0)
                ->first();

            if ($checkExisting) {
                $checkExisting->update($data);
            } else {
                InvoiceModel::create($data);
            }
        }
        
        // Operation if take away out
        if ($req['order_type'] == 'takeout') {
            InvoiceModel::whereInvoiceId($req['invoice_id'])
                ->whereHasPaid(0)->update($data);
        }
        // generate invoice
        
        $tableID = $request->table_code;
        if (!empty($req['invoice_id'])) {
            $checkInvoiceId = InvoiceModel::whereInvoiceId($req['invoice_id'])
                ->first();
            $tableID = 'takeout-' . $checkInvoiceId->file_name;
        }

        $order_data = File::get((string)$tableID."-ticket.json");
        $order = json_decode($order_data, true);

        $data = [];
        $data['now'] = Carbon::now()->format('d-M-Y g:i A');
        $data['outlet'] = DB::table('settings')->where('settings.id',$companyId)
            ->leftjoin('currencies','currencies.id','=','settings.company_currency')
            ->first();

        $data['exchanges'] = DB::table('exchange')
            ->leftjoin('currencies','currencies.id','=','exchange.currency_to')
            ->where('exchange.company_id',$companyId)->get();

        $data['invoice'] = DB::table('invoices')
            ->whereTableId($req['table_id'])
            ->whereHasPaid(0);

        if ($req['order_type'] == 'takeout') {
            
            $data['invoice'] = DB::table('invoices')
            ->whereInvoiceId($req['invoice_id'])
            ->whereHasPaid(0);
        }

        $data['invoice'] = $data['invoice']->first();
        

        $data['invoice_detail'] = $order['items'];  

        $view = view('invoice.invoice_format_01', $data)->render();
        $dompdf = new Dompdf(array('enable_remote' => true));
        
        $dompdf->loadHtml($view);

        $dompdf->setPaper('A7', 'portrait');
        $dompdf->render();
        $output                 = $dompdf->output();
        File::put($tableID.'.pdf', $output);

        $printer_info           = DB::table('printer')->select('printer_name')->where('company_id',$user->company_id)->where('printer_type','invoice')->first();

        //generate vbscript
        $print_vbs = '
        Set oShell = CreateObject ("Wscript.Shell") 
        Dim strArgs
        strArgs = "'.$tableID.'.bat"
        oShell.Run strArgs, 0, false';

        $print_batch = '"PDFtoPrinter.exe" "'. $tableID . '.pdf" "' . $printer_info->printer_name .'"';

        File::put($tableID.'.vbs', $print_vbs);
        File::put(''.$tableID.'.bat', $print_batch);

        $print_invoice = shell_exec($tableID.".vbs");
        echo $print_invoice; 
    }


    // =================================== Paid Without Print ========================================
    public function paid_order(Request $request){
        
        $req            = $request->all();
        $myID           = CRUDBooster::myId();
        $companyId      = getCompanyId($myID);
        $tableID        = $request->table_code;

        // check table id or invoice id
        if (!empty($req['invoiceId'])) {
            $getFileName = InvoiceModel::whereInvoiceId($req['invoiceId'])->first();
            $tableID = 'takeout-' . $getFileName->file_name;
        }

        $url            = File::get((string)$tableID."-ticket.json");
        $order          = json_decode($url, true);

        $company_currency = DB::table('settings')
            ->select('company_currency','currency_symbol')
            ->leftjoin('currencies','currencies.id','=','settings.company_currency')
            ->where('settings.id', $companyId)->first();


            $checkOrderNo = InvoiceModel::whereDate('created_at', Carbon::today())
                ->whereCompanyId($companyId);
                
            // Count Order Number of Take Out
            $invoiceNumber = $checkOrderNo->count();
            $orderCode = $checkOrderNo->whereOrderType('dinein')->count();
            
            // Default currency exchange
            $taxAmount = ($req['g_total'] * getTaxValue($companyId) / 100);
            $rielExchange = ($req['g_total'] + $taxAmount) * getExchangeRate($companyId, 'Riel')->price_to;
            $bahtExchange = ($req['g_total'] + $taxAmount) * getExchangeRate($companyId, 'Baht')->price_to;

            // Check when amount entered is Cambodian(Riel)
            if ($req['p_riel']) {
                $dollarExchange = ($req['p_riel'] / getExchangeRate($companyId, 'Riel')->price_to);
                // Exchage to riel to Baht
                $dollarAmount = ($req['p_riel'] / getExchangeRate($companyId, 'Riel')->price_to);
                $bahtExchange = ($dollarAmount * getExchangeRate($companyId, 'Baht')->price_to);
            }

            // Check when amount entered is Thai(Baht)
            if ($req['p_baht']) {
                $dollarExchange = ($req['p_baht'] / getExchangeRate($companyId, 'Baht')->price_to);
                $dollarAmount =  ($dollarAmount / getExchangeRate($companyId, 'Baht')->price_to);
                $exchangeRiel = ($dollarAmount *  getExchangeRate($companyId, 'Riel')->price_to);
            }

            // Count Invoice number 

            $orderCode = ($orderCode < 9) ? '0' . ($orderCode + 1) : $orderCode + 1;

            $invoiceNumber = ($invoiceNumber < 9) ? '0' . ($invoiceNumber + 1) : $invoiceNumber + 1;

            $data = [
                'invoice_date'              => Carbon::now()->toDateString(),
                'created_by'                => $myID,
                'company_id'                => $companyId,
                'table_id'                  => $req['table_id'],
                'order_type'                => $req['order_type'],
                'invoice_number'            => $invoiceNumber,
                'order_code'                => $orderCode,
                'currency'                  => $company_currency->currency_symbol,
                'grand_total'               => $req['g_total'],
                'grand_dollar_exchange'     => !empty($dollarExchange) ? $dollarExchange : 0,
                'grand_riel_exchange'       => !empty($rielExchange) ? $rielExchange : 0,
                'grand_baht_exchange'       => !empty($bahtExchange) ? $bahtExchange : 0,
                'has_paid'                  => 1,
                'paid_total'                => $req['p_total'],
                'paid_dollar'               => $req['p_dollar'],
                'paid_riel'                 => $req['p_riel'],
                'paid_baht'                 => $req['p_baht'],
                'return_amount'             => $req['return_amount'],
                'return_dollar'             => $req['r_dollar'],
                'return_riel'               => $req['r_riel'],
                'return_baht'               => $req['r_baht'],
                'discount_type'             => $req['discount_id'],
            ];

        // Check if already existing data in database
        // Check with TableId and HasPaid
        
        if($req['invoiceId'] == 0){

            $checkExisting = InvoiceModel::whereTableId($req['table_id'])
                ->whereHasPaid(0)
                ->first();
            if ($checkExisting) {
                $checkExisting->update($data);
            } else {
                InvoiceModel::create($data);
            }
        }
        
        // Check and Update paid status of invoice take out
        if ($req['invoiceId'] != 0) {
            InvoiceModel::whereInvoiceId($req['invoiceId'])
            ->whereHasPaid(0)
            ->update($data);
        }
        
        $last_invoice_id = DB::table('invoices')->select('invoice_id')->where('company_id', $companyId)->orderBy('invoice_id','DESC')->first();

        // insert items into invoice order
        foreach($order['items'] as $item):
            
            $data = [
                'invoice_id'  => $last_invoice_id->invoice_id,
                'item_id' => (int)$item['item_id'],
                'item_size' => (int) $item['item_size'],
                'addon_id' => 0,
                'qty' => (int)$item['item_qty'],
                'price' => (double) $item['item_amount'],
                'status' => 1,
                'company_id' => $companyId
            ];
            DB::table('invoice_orders')->insert($data);

            foreach(array_filter($item['addons']) as $addon):
                DB::table('invoice_orders')->insert(
                    [   
                        'invoice_id' => $last_invoice_id->invoice_id,
                        'item_id' => (int)$item['item_id'],
                        'item_size' => (int)$item['item_size'],
                        'addon_id' => (int)$addon['addon_id'],
                        'qty' => (int)$addon['addon_qty'],
                        'price' => (double)$addon['addon_amount'],
                        'status' => 1,
                        'company_id' => $companyId
                    ]
                );
            endforeach;
        endforeach; 
        
        $table_data = DB::table('table_numbers')->where('company_id', $companyId)->where('table_code',$tableID)->select('merge_with')->first();

        DB::table('table_numbers')->where('company_id', $companyId)->where('table_code',$tableID)->update(['merge_with' => null]);
        $table_merge = explode("," , $table_data->merge_with);
        foreach($table_merge as $table):
            DB::table('table_numbers')->where('company_id', $companyId)->where('id',$table)->update(['is_merge' => 0]);
        endforeach;

        $dataPaid = [
            'status'    => 1, 
            'tableCode' => $tableID,
        ];
        event(new OrderEvent($dataPaid));

        $data = [
            'mergeWith' => false
            ];
        event(new UnmergeEvent($data));

    }

    // =================================== Paid & Print ========================================
    public function generate_paid_invoice(Request $request){

        $myID                   = CRUDBooster::myId();
        $tableID                = $request->table_code;
        $user                   = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        // Invoice Imformation
        $invoice_id             = DB::table('invoices')->select('invoice_number')->where('company_id',$user->company_id)->orderBy('invoice_id','DESC')->first();
        
        if (!empty($request->invoiceId)) {
            $getFileName = InvoiceModel::whereInvoiceId($request->invoiceId)->first();
            
            $tableID = 'takeout-' . $getFileName->file_name;
        }


        $data                   = [];
        // Outlet Information
        $data['now']            = Carbon::now()->format('d-M-Y g:i A');
        $data['outlet']         = DB::table('settings')->where('settings.id',$user->company_id)
                                ->leftjoin('currencies','currencies.id','=','settings.company_currency')
                                ->first();
        $data['exchanges']      = DB::table('exchange')
                                    ->leftjoin('currencies','currencies.id','=','exchange.currency_to')
                                    ->where('exchange.company_id',$user->company_id)->get();

        $data['invoice']        = DB::table('invoices')
                                ->leftjoin('table_numbers','table_numbers.id','=','invoices.table_id')
                                ->leftjoin('cms_users','cms_users.id','=','invoices.created_by')->where('invoice_number',$invoice_id->invoice_number)
                                ->first();
        $printer_info           = DB::table('printer')->select('printer_name')->where('company_id',$user->company_id)->where('printer_type','invoice')->first();

        
        $order_data             = File::get((string)$tableID."-ticket.json");
        $order                  = json_decode($order_data, true);
        $data['invoice_detail'] = $order['items'];  


        if ($request->invoiceId != 0) {
            $invoice = InvoiceModel::whereInvoiceId($request->invoiceId)
                ->first();
            $tableID = 'takeout-' . $invoice->file_name;
        }
        
        


        
        // PDF Generate
        $view                   = view('invoice.invoice_format_01',$data)->render();
        $dompdf                 = new Dompdf(array('enable_remote' => true));
        $dompdf->loadHtml($view);
        $dompdf->setPaper('A7', 'portrait');
        $dompdf->render();
        $output = $dompdf->output();
        File::put($tableID.'.pdf', $output);

        // Generate Print Script
        $print_vbs = '
        Set oShell = CreateObject ("Wscript.Shell") 
        Dim strArgs
        strArgs = "'.$tableID.'.bat"
        oShell.Run strArgs, 0, false';
        $print_batch = '"PDFtoPrinter.exe" "' . $tableID . '.pdf" "' . $printer_info->printer_name .'"';
        File::put($tableID.'.vbs', $print_vbs);
        File::put($tableID.'.bat', $print_batch);
        $print_invoice  = $tableID.".vbs";
        echo shell_exec($print_invoice);

        $dataPaid = [
            'status'    => 1, 
            'tableCode' => $tableID,
        ];
        event(new OrderEvent($dataPaid));

        $data = [
            'mergeWith' => false
            ];
        event(new UnmergeEvent($data));



        sleep(5);
        // Delete all Ordered files
        unlinkFile($tableID . '-ticket.json');
        unlinkFile($tableID . '-ticket.vbs');
        unlinkFile($tableID . '-ticket.pdf');
        unlinkFile($tableID . '-ticket.bat');

        // Delete all print Files
        unlinkFile($tableID . '.vbs');
        // unlinkFile($tableID . '.pdf');
        unlinkFile($tableID . '.bat');


}




    // =================================== Split Invoice (NO PAID) ========================================
    public function split_invoice(Request $request){
        $myID                   = CRUDBooster::myId();
        $user                   = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $grandTotal             = $request->grandTotal;
        $mainSplit              = $request->mainSplit;
        $sideSplit              = $request->sideSplit;
        $tableID                = $request->table_code;

        $invoice_id             = DB::table('invoices')->select('invoice_number')->where('company_id',$user->company_id)->orderBy('invoice_id','DESC')->first();

        $order_data             = File::get((string)$tableID."-ticket.json");
        $order                  = json_decode($order_data, true);

        $data                   = [];
        $data['now']            = Carbon::now()->format('d-M-Y g:i A');
        $data['outlet']         = DB::table('settings')->where('settings.id',$user->company_id)
                                ->leftjoin('currencies','currencies.id','=','settings.company_currency')
                                ->first();
        $data['exchanges']       = DB::table('exchange')
                                    ->leftjoin('currencies','currencies.id','=','exchange.currency_to')
                                    ->where('exchange.company_id',$user->company_id)->get();
        $data['invoice']        = DB::table('invoices')
                                ->leftjoin('cms_users','cms_users.id','=','invoices.created_by')->where('invoice_number',$invoice_id->invoice_number)
                                ->first();

        $items                  = [];
        $items_second           = [];
        $total                  = 0;

        foreach ($order['items'] as $key => $new_va):
                $arr[$key]['total'] = $new_va['item_amount'] * $new_va['item_qty'];
        endforeach;


        foreach($order['items'] as $key => $row):
                $vc_array_value[$key] = $row['total'];
                $vc_array_name[$key] = $row['item_name'];
        endforeach;

        array_multisort($vc_array_value, SORT_ASC, $vc_array_name, SORT_DESC, $arr);


        foreach($order['items'] as $order):
            $total += (int)$order['item_qty'] * (float)$order['item_amount'];

            if($total <= $mainSplit):
                array_push($items,[
                    'item_id'       => $order['item_id'],
                    'item_name'     => $order['item_name'],
                    'item_qty'      => $order['item_qty'],
                    'item_amount'   => $order['item_amount'],
                    'item_size'     => $order['item_size'],
                    'print_status'  => $order['print_status'],
                    'addmore'       => $order['addmore'],
                    'item_size_name'=> $order['item_size_name'],
                    'addons'        => []
                ]

            );
            else:
                array_push($items_second,[
                    'item_id'       => $order['item_id'],
                    'item_name'     => $order['item_name'],
                    'item_qty'      => $order['item_qty'],
                    'item_amount'   => $order['item_amount'],
                    'item_size'     => $order['item_size'],
                    'print_status'  => $order['print_status'],
                    'addmore'       => $order['addmore'],
                    'item_size_name'=> $order['item_size_name'],
                    'addons'        => []
                ]

            );
            endif;

        endforeach;

        
        $data['invoice_detail'] = $items;  

        $view                   = view('invoice.invoice_format_01',$data)->render();
        $dompdf                 = new Dompdf(array('enable_remote' => true));
        $dompdf->loadHtml($view);
        $dompdf->setPaper('A7', 'portrait');
        $dompdf->render();
        $output                 = $dompdf->output();
        File::put($tableID.'-1.pdf', $output);

        //generate vbscript
        $print_vbs = '
        Set oShell = CreateObject ("Wscript.Shell") 
        Dim strArgs
        strArgs = "'.$tableID.'-1.bat"
        oShell.Run strArgs, 0, false';

        $print_batch = '"PDFtoPrinter.exe" "' . $tableID . '-1.pdf" "' . $printer_info->printer_name .'"';

        File::put($tableID.'-1.vbs', $print_vbs);
        File::put($tableID.'-1.bat', $print_batch);

        $print_invoice  = $tableID."-1.vbs";
        echo shell_exec($print_invoice);

        $data['invoice_detail'] = $items_second;  


        $view                   = view('invoice.invoice_format_01',$data)->render();
        $dompdf1                 = new Dompdf(array('enable_remote' => true));
        $dompdf1->loadHtml($view);
        $dompdf1->setPaper('A7', 'portrait');
        $dompdf1->render();
        $output1                 = $dompdf1->output();
        File::put($tableID.'-2.pdf', $output1);

        //generate vbscript
        $print_vbs = '
        Set oShell = CreateObject ("Wscript.Shell") 
        Dim strArgs
        strArgs = "'.$tableID.'-2.bat"
        oShell.Run strArgs, 0, false';

        $print_batch = '"PDFtoPrinter.exe" "' . $tableID . '-2.pdf" "' . $printer_info->printer_name .'"';

        File::put($tableID.'-2.vbs', $print_vbs);
        File::put($tableID.'-2.bat', $print_batch);

        $print_invoice  = $tableID."-2.vbs";
        echo shell_exec($print_invoice);


        // sleep(5);
        // unlink($tableID . '.vbs');
        // unlink($tableID . '.pdf');
        // unlink($tableID . '-1.vbs');
        // unlink($tableID . '-1.pdf');
        // unlink($tableID . '-ticket.json');
        // unlink($tableID . '-ticket.vbs');
        // unlink($tableID . '-ticket.pdf');

    }


    private function saveTakeoutOrder($companyId, $myId, $invoiceId, $data, $type) {
        // $type = new; // Add new takeOut Order
        // $type = edit; // Update takeOut Order
        
        $datas = [
            'invoice_id' => $invoiceId,
            'invoice_date' => now()->timestamp,
            'order_type' => 'takeout',
            'created_by' => $myId,
            'company_id' => $companyId,
            'invoice_number' => $data->invoice_number,
        ];
        $invoiceData = InvoiceModel::create($datas);

        // if ($invoiceData) {
            
        // }
    }

    public function requestOrderIntoDB(Request $request){
        $myID           = CRUDBooster::myId();
        $user           = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
        $order          = $request->getContent();
        $content        = json_decode($order, true);
        $tableID        = $content['id'];
        File::put($tableID.'-ticket.json', $order);

        $url            = File::get((string)$tableID."-ticket.json");
        $order          = json_decode($url, true);

        //Order data
        $g_total        = 0;
        $g_dollar       = 0;
        $g_riel         = 0;
        $g_baht         = 0; // end of grand total

        $p_total        = 0;
        $p_dollar       = 0;
        $p_riel         = 0;
        $p_baht         = 0; // end of paid amount

        $return_amount  = 0;
        $r_dollar       = 0;
        $r_riel         = 0;
        $r_baht         = 0; // end of return

        $table_id       = NULL;
        $order_type     = $content['order_type'];


        $company_currency = DB::table('settings')
                            ->select('company_currency','currency_symbol')
                            ->leftjoin('currencies','currencies.id','=','settings.company_currency')
                            ->where('settings.id',$user->company_id)->first();
        
        //insert data into invoices
       /* $invoice = DB::table('invoices')->where('company_id', $user)->where('table_id', $table_id)->where('has_paid','=','0')->first();*/
       $check_invoice = DB::table('invoices')->where('company_id', $user->company_id)->where('table_id', $table_id)->where('has_paid','=',0)->first();
       if($check_invoice->invoice_id <= 0){
       $invoice = DB::table('invoices')->first();
        if($invoice):
            $invoice_number = DB::table('invoices')->select('invoice_number')->where('company_id',$user->company_id)->orderBy('invoice_id','DESC')->first();

            
            DB::table('invoices')->insert(
                [
                    'invoice_date'          => Carbon::now()->toDateString(),
                    'created_by'            => $myID,
                    'company_id'            => $user->company_id,
                    'table_id'              => $table_id,
                    'order_type'            => $order_type,
                    'invoice_number'        => $invoice_number->invoice_number + 1,
                    'currency'              => $company_currency->currency_symbol,
                    'grand_total'           => $g_total,
                    'grand_dollar_exchange' => $g_dollar,
                    'grand_riel_exchange'   => $g_riel,
                    'grand_baht_exchange'   => $g_baht,
                    'has_paid'              => '1',
                    'paid_total'            => $p_total,
                    'paid_dollar'           => $p_dollar,
                    'paid_riel'             => $p_riel,
                    'paid_baht'             => $p_baht,
                    'return_amount'         => $return_amount,
                    'return_dollar'         => $r_dollar,
                    'return_riel'           => $r_riel,
                    'return_baht'           => $r_baht,
                    'created_at'            => Carbon::now()
                ]
            );
        else:
            $invoice_number = '1';
            DB::table('invoices')->insert(
                [
                    'invoice_date'          => Carbon::now()->toDateString(),
                    'created_by'            => $myID,
                    'company_id'            => $user->company_id,
                    'table_id'              => $table_id,
                    'order_type'            => $order_type,
                    'invoice_number'        => '1',
                    'currency'              => $company_currency->currency_symbol,
                    'grand_total'           => $g_total,
                    'grand_dollar_exchange' => $g_total,
                    'grand_riel_exchange'   => $g_riel,
                    'grand_baht_exchange'   => $g_baht,
                    'has_paid'              => '1',
                    'paid_total'            => $p_total,
                    'paid_dollar'           => $p_dollar,
                    'paid_riel'             => $p_riel,
                    'paid_baht'             => $p_baht,
                    'return_amount'         => $return_amount,
                    'return_dollar'         => $r_dollar,
                    'return_riel'           => $r_riel,
                    'return_baht'           => $r_baht,
                    'created_at'            => Carbon::now()
                ]
            );
        endif;
    }else{
        $invoice_number = DB::table('invoices')->select('invoice_number')->where('company_id',$user->company_id)->where('table_id', $table_id)->where('has_paid','=','0')->first();
            DB::table('invoices')->where('table_id', $table_id)->update(
                [
                    'invoice_date'          => Carbon::now()->toDateString(),
                    'created_by'            => $myID,
                    'company_id'            => $user->company_id,
                    'table_id'              => $table_id,
                    'invoice_number'        => $invoice_number->invoice_number + 1,
                    'currency'              => $company_currency->currency_symbol,
                    'grand_total'           => $g_total,
                    'grand_dollar_exchange' => $g_dollar,
                    'grand_riel_exchange'   => $g_riel,
                    'grand_baht_exchange'   => $g_baht,
                    'has_paid'              => '1',
                    'paid_total'            => $p_total,
                    'paid_dollar'           => $p_dollar,
                    'paid_riel'             => $p_riel,
                    'paid_baht'             => $p_baht,
                    'return_amount'         => $return_amount,
                    'return_dollar'         => $r_dollar,
                    'return_riel'           => $r_riel,
                    'return_baht'           => $r_baht,
                    'created_at'            => Carbon::now()
                ]
            );
    }
    

        $last_invoice_id = DB::table('invoices')->select('invoice_id')->where('company_id',$user->company_id)->orderBy('invoice_id','DESC')->first();

        // insert items into invoice order
        foreach($order['items'] as $item):
            DB::table('invoice_orders')->insert(
                [
                    'invoice_id'    => $last_invoice_id->invoice_id,
                    'item_id'           => (int)$item['item_id'],
                    'item_size'         => (int)$item['item_size'],
                    'addon_id'          => 0,
                    'qty'               => (int)$item['item_qty'],
                    'price'             => (double)$item['item_amount'],
                    'status'            => 1,
                    'company_id'        => $user->company_id
                ]
            );
            foreach($item['addons'] as $addon):
                DB::table('invoice_orders')->insert(
                    [   
                        'invoice_id'    => $last_invoice_id->invoice_id,
                        'item_id'           => (int)$item['item_id'],
                        'item_size'         => (int)$item['item_size'],
                        'addon_id'          => (int)$addon['addon_id'],
                        'qty'               => (int)$addon['addon_qty'],
                        'price'             => (double)$addon['addon_amount'],
                        'status'            => 1,
                        'company_id'        => $user->company_id
                    ]
                );
            endforeach;
        endforeach; 
        

        $table_data = DB::table('table_numbers')->where('company_id',$user->company_id)->where('table_code',$tableID)->select('merge_with')->first();

        DB::table('table_numbers')->where('company_id',$user->company_id)->where('table_code',$tableID)->update(['merge_with' => null]);
        $table_merge = explode("," , $table_data->merge_with);
        foreach($table_merge as $table):
            DB::table('table_numbers')->where('company_id',$user->company_id)->where('id',$table)->update(['is_merge' => 0]);
        endforeach;


        $data                   = [];
        // Outlet Information
        $data['now']            = Carbon::now()->format('d-M-Y g:i A');
        $data['outlet']         = DB::table('settings')->where('settings.id',$user->company_id)
                                ->leftjoin('currencies','currencies.id','=','settings.company_currency')
                                ->first();
        $data['exchanges']      = DB::table('exchange')
                                    ->leftjoin('currencies','currencies.id','=','exchange.currency_to')
                                    ->where('exchange.company_id',$user->company_id)->get();

        $data['invoice']        = DB::table('invoices')
                                ->leftjoin('table_numbers','table_numbers.id','=','invoices.table_id')
                                ->leftjoin('cms_users','cms_users.id','=','invoices.created_by')->where('invoice_number',$invoice_id->invoice_number)
                                ->first();
        $printer_info           = DB::table('printer')->select('printer_name')->where('company_id',$user->company_id)->where('printer_type','invoice')->first();

        // Order Information
        $tableID                = $request->table_code;
        $order_data             = File::get((string)$tableID."-ticket.json");
        $order                  = json_decode($order_data, true);
        $data['invoice_detail'] = $order['items'];  

        
        // PDF Generate
        $view                   = view('invoice.invoice_format_01',$data)->render();
        $dompdf                 = new Dompdf(array('enable_remote' => true));
        $dompdf->loadHtml($view);
        $dompdf->setPaper('A7', 'portrait');
        $dompdf->render();
        $output = $dompdf->output();
        File::put($tableID.'.pdf', $output);

        // Generate Print Script
        $print_vbs = '
        Set oShell = CreateObject ("Wscript.Shell") 
        Dim strArgs
        strArgs = "'.$tableID.'-ticket.bat"
        oShell.Run strArgs, 0, false';
        $print_batch = '"PDFtoPrinter.exe" "' . $tableID . '-ticket.pdf" "' . $printer_info->printer_name .'"';
        File::put($tableID.'-ticket.vbs', $print_vbs);
        File::put($tableID.'-ticket.bat', $print_batch);
        $print_invoice  = $tableID."-ticket.vbs";
        echo shell_exec($print_invoice);




    }
    

}