<?php 
namespace App\Http\Controllers;

	use Session;
	use Illuminate\Http\Request;
	use DB;
	use CRUDBooster;
	use Carbon\Carbon;

class AdminVoidController extends \crocodicstudio\crudbooster\controllers\CBController
{
	public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field 			= "id";
			$this->limit 				= "20";
			$this->orderby 				= "invoice_id,desc";
			$this->global_privilege 	= false;
			$this->button_table_action 	= false;
			$this->button_bulk_action 	= false;
			$this->button_action_style 	= "button_icon";
			$this->button_add 			= false;
			$this->button_edit 			= true;
			$this->button_delete 		= true;
			$this->button_detail 		= true;
			$this->button_show 			= false;
			$this->button_filter 		= false;
			$this->button_import 		= false;
			$this->button_export 		= false;
			$this->table 				= "invoices";
			# END CONFIGURATION DO NOT REMOVE THIS LINE
	    }
	    
      	public function getIndex()
	    {
	    	
	    	$myID 					= CRUDBooster::myId();
	    	$user					= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
	    	$data 					= [];
	    	$data['page_title'] 	= trans('cashmanagement.Void Invoice');
	    	$data['invoices']   	= DB::table('invoices')
	    								->leftjoin('settings','settings.id','=','invoices.company_id')
	    								->where('invoices.company_id',$user->company_id)->where('void',0)->orderBy('invoice_id','DESC')->paginate(20);

	    	$this->cbView('cash_management.void', $data);
	    }


	    public function get_sort(Request $request){
	    	$myID 					= CRUDBooster::myId();
	    	$user					= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();

	    	$data 					= [];
	    	$data['page_title']    	= "Sale by Invoice Report Search";
	    	

	    	$date 					= $request->by;
	    	$custom_date			= $request->daterange;
	    	$invoice_id 			= $request->invoice_id;

	    	if($date):
	    		if ($request->by == "weekly"):
				    $startDate 	= date("Y-m-d", strtotime('monday this week'))." 00:00:00";
					$endDate 	= date("Y-m-d", strtotime('sunday this week'))." 23:59:59";
				elseif ($request->by == "monthly"): 
					$startDate 	= date('Y-m-01')." 00:00:00";
					$date 		= date('Y-m-d');
					$endDate 	= date("Y-m-t", strtotime($date))." 23:59:59";
				elseif ($request->by == "yearly"):
					$startDate 	= date('Y-01-01')." 00:00:00";
					$date 		= date('Y-m-d');
					$endDate 	= date("Y-12-t", strtotime($date))." 23:59:59";
				elseif ($request->by == "daily" ):
					$day 		= new \DateTime();
					$startDate 	= $day->format('Y-m-d')." 00:00:00";
					$endDate 	= $day->format('Y-m-d')." 23:59:59";
				else:
					$startDate 	= null;
					$endDate 	= null;
				endif;

				$invoices = DB::table('invoices')->leftjoin('settings','settings.id','=','invoices.company_id')
							->where('invoice_date','>=',$startDate)
							->where('invoice_date','<',$endDate)
							->where('void','=','0')
							->where('invoices.status','=',1)
							->where('invoices.company_id',$user->company_id)
							->OrderBy('invoice_id','DESC')
							->paginate(20)->appends('by',$request->by);


	    	elseif($custom_date):
    			$date 		= explode(' - ',$custom_date);
    			$startDate	= $date[0];
    			$endDate 	= $date[1];

    			$invoices = DB::table('invoices')->leftjoin('settings','settings.id','=','invoices.company_id')
						->where('invoice_date','>=',$startDate)
						->where('invoice_date','<=',$endDate)
						->where('void','=','0')
						->where('invoices.status','=',1)
						->where('invoices.company_id',$user->company_id)
						->OrderBy('invoice_id','DESC')
						->paginate(20)->appends('daterange',$request->daterange);
	    	elseif($invoice_id):
	    		$invoices = DB::table('invoices')->leftjoin('settings','settings.id','=','invoices.company_id')
						->where('void','=','0')
						->where('invoices.status','=',1)
						->where('invoices.company_id',$user->company_id)
						->where('invoice_number','=',$invoice_id)
						->OrderBy('invoice_id','DESC')
						->paginate(20)->appends('invoice',$request->invoice_id);
	    	else:
	    		$invoices = DB::table('invoices')->leftjoin('settings','settings.id','=','invoices.company_id')
							->where('void','=','0')
							->where('invoices.status','=',1)
							->where('invoices.company_id',$user->company_id)
							->OrderBy('invoice_id','DESC')
							->paginate(20);
	    	endif;

	    	$data['invoices'] = $invoices;
	    	$this->cbView('cash_management.void',$data);
	    }

	    public function view_invoice(Request $request){
	    	$myID 					= CRUDBooster::myId();
	    	$user					= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
	    	$invoice_no 			= $request->invoice_no;
	    	$invoice 				= DB::table('invoices')
	    							->leftjoin('cms_users','cms_users.id','=','invoices.created_by')
	    							->where('invoices.company_id',$user->company_id)->where('invoices.invoice_id',$invoice_no)->first(); 
	    	$orders 				= DB::table('invoice_orders')
	    							->leftjoin('menus','menus.id','=','invoice_orders.item_id')
	    							->leftjoin('menu_materials','menu_materials.id','=','invoice_orders.addon_id')
	    							->select('invoice_orders.*','menus.*','invoice_orders.price as price','menu_materials.name as material_name')
	    							->where('invoice_id',$invoice_no)->get();?>

	    	<table class="invoice_info">
	    		<tr>
					<td>Invoice No</td>
					<td><?php echo $invoice->invoice_number; ?></td>
				</tr>
				<tr>
					<td>Date</td>
					<td><?php echo $invoice->created_at; ?></td>
				</tr>
				<tr>
					<td>Seller</td>
					<td><?php echo $invoice->name; ?></td>
				</tr>
	    	</table>
	    	<table class="invoice_order">
	    		<tr class="header">
	    			<td class="col-md-1">ID</td>
	    			<td class="col-md-5">Name</td>	
	    			<td>Qty</td>	
	    			<td>Price</td>	
	    			<td>Total</td>	
	    		</tr>
	    		<?php foreach($orders as $key => $order): ?>
	    		<tr>
					<?php if($order->addon_id == 0): ?>
						<td class="col-md-1"><?php echo ++$key; ?></td>
						<td class="col-md-5"><?php echo $order->name; ?></td>
					<?php else: ?>
						<td class="col-md-1">></td>
						<td class="col-md-5"><?php echo $order->material_name; ?></td>
					<?php endif; ?>
					<td><?php echo $order->qty; ?></td>
					<td><?php echo $invoice->currency . $order->price; ?></td>
					<td><?php echo $invoice->currency . $order->price* $order->qty; ?></td>
	    		</tr>
	    		<?php endforeach; ?>
	    	</table>
	    	<table class="invoice_transaction" style="text-align: right;">
	    		<tr>
	    			<td></td>	
	    			<td>Total Amount: </td>	
	    			<td><?php echo $invoice->currency . $invoice->grand_total;?></td>		
	    		</tr>

	    		<tr>
	    			<td></td>	
	    			<td>Paid Amount : </td>	
	    			<td><?php echo $invoice->currency . $invoice->paid_total;?></td>		
	    		</tr>

	    		<tr>
	    			<td></td>	
	    			<td>Return Amount: </td>	
	    			<td><?php echo $invoice->currency . $invoice->return_amount;?></td>		
	    		</tr>
	    	</table>

	    	<button type="button" class="btn btn-default void"  data-val="<?php echo $invoice->invoice_id; ?>"   data-dismiss="modal"  ><?php echo trans('report.Void'); ?></button>
	    	<button type="button" class="btn btn-primary create_void"  data-val="<?php echo $invoice->invoice_id; ?>" data-dismiss="modal">Void and Create</button>
			
			<!-- <button type="button" class="btn btn-default voidCopy void"  data-val="<?php echo $invoice->invoice_id; ?>"   data-dismiss="modal"  >Void & Copy</button> -->
	    	<?php
	    }

	    public function void_invoice(Request $request){
	    	$myID 					= CRUDBooster::myId();
	    	$companyId = getCompanyId($myID);
	    	$invoice_no 			= $request->invoice_no;
	    	if($invoice_no):
	    		$this->voidInvoice($companyId, $invoice_no);
	    	endif;
	    }
	   
	    public function createVoid() {
	    	$myId = CRUDBooster::myId();
	    	$invoiceId = request()->invoiceId;
	    	$orderData = request()->order_data;
	    	$companyId = getCompanyId($myId);
	    	
	    	
	    }

    // Private function for void Invoice
     private function voidInvoice($companyId, $invoiceId) {
        DB::table('invoices')->whereInvoiceId($invoiceId)
          ->whereCompanyId($companyId)
          ->update(['void'=>'1']);
     }


}
