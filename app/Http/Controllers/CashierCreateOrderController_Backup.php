<?php namespace App\Http\Controllers;

	use Session;
	use Illuminate\Http\Request;
	use DB;
	use CRUDBooster;
	use URL;
	use Carbon\Carbon;

	class CashierCreateOrderController extends \crocodicstudio\crudbooster\controllers\CBController {

	public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field 			= "id";
			$this->limit 				= "20";
			$this->orderby 				= "id,desc";
			$this->global_privilege 	= false;
			$this->button_table_action 	= false;
			$this->button_bulk_action 	= true;
			$this->button_action_style 	= "button_icon";
			$this->button_add 			= false;
			$this->button_edit 			= true;
			$this->button_delete 		= true;
			$this->button_detail 		= true;
			$this->button_show 			= false;
			$this->button_filter 		= true;
			$this->button_import 		= false;
			$this->button_export 		= false;
			$this->table 				= "table_numbers";
			# END CONFIGURATION DO NOT REMOVE THIS LINE


	}
	

	public function getIndex(){
			// if(!CRUDBooster::isVIew()) CRUDBooster::denyAccess();


			/* ===== Defined Admin,Legal and Everyone else's ID =====  */
	    	$myID 						= CRUDBooster::myId();
	    	$adminID 					= DB::table('cms_users')->select('created_by','company_id')->where('id',$myID)->first();
	    	$everyone  					= $adminID->created_by;

	    	/* ===== Admin,Legal Data =====  */
	    	$data 						= [];
	    	$data['page_title'] 		= trans("crudbooster.create_order");
	    	if($adminID->company_id == $myID):
	    	
	    	
	    	$data['tables'] 			= DB::table('table_numbers')->where('created_by',$myID)->get();
	    	$data['categories'] 		= DB::table('categories')->where('created_by',$myID)->orderBy('name','ASC')->get();
	    	$data['subcategories'] 		= DB::table('subcategories')->where('created_by',$myID)->orderBy('name','ASC')->get();
	    	$data['menus'] 				= DB::table('menus')->where('created_by',$myID)->orderBy('name','ASC')->get();
	    	$data['user'] 				= DB::table('cms_users')->where('id',$myID)->first();
	    	$data['tax']				= DB::table('settings')->where('created_by',$myID)->select('company_sale_tax')->first();
	    	$data['company_currency']	= DB::table('settings')->where('settings.created_by',$myID)
	    								->leftjoin('currencies','currencies.id','=','settings.company_currency')
	    								->select('company_currency','currency_symbol')->first();
	    	$company_currency			= DB::table('settings')->where('created_by',$myID)->select('company_currency')->first();
	    	$currency 					= DB::table('currencies')->get();
	    	$data['currencies']			= DB::table('currencies')
	    								->leftjoin('exchange','exchange.currency_form','=','currencies.id')
	    								->where('exchange.created_by',$myID)->get();
	    	$data['other_exchange']		= DB::table('exchange')
	    								->select('currencies.*','exchange.*','currencies.id as id')
	    								->where('created_by',$myID)->where('currency_to',$company_currency->company_currency)->get();
			$data['setting'] 			= DB::table('settings')
										->leftjoin('countries','countries.id','=','settings.company_country')
										->leftjoin('currencies','currencies.id','=','settings.company_currency')
										->where('settings.id',$adminID->company_id)->first();

	    	else:

	    	/* ===== Everyone Else Data =====  */
	    	$data['tables'] 			= DB::table('table_numbers')->where('created_by',$everyone)->get();
	    	$data['categories'] 		= DB::table('categories')->where('created_by',$everyone)->orderBy('name','ASC')->get();
	    	$data['subcategories'] 		= DB::table('subcategories')->where('created_by',$everyone)->orderBy('name','ASC')->get();
	    	$data['menus'] 				= DB::table('menus')->where('created_by',$everyone)->orderBy('name','ASC')->get();
	    	$data['user'] 				= DB::table('cms_users')->where('id',$myID)->first();
	    	$data['tax']				= DB::table('settings')->where('created_by',$everyone)->select('company_sale_tax')->first();
	    	$data['company_currency']	= DB::table('settings')->where('settings.created_by',$everyone)
	    								->leftjoin('currencies','currencies.id','=','settings.company_currency')
	    								->select('company_currency','currency_symbol')->first();
	    	$company_currency			= DB::table('settings')->where('created_by',$everyone)->select('company_currency')->first();
	    	$currency 					= DB::table('currencies')->get();
	    	$data['currencies']			= DB::table('currencies')
	    								->leftjoin('exchange','exchange.currency_form','=','currencies.id')
	    								->select('currencies.*','exchange.*','currencies.id as id')
	    								->where('exchange.created_by',$everyone)->get();
	    	$data['other_exchange']		= DB::table('exchange')
	    								->where('created_by',$everyone)->where('currency_to',$company_currency->company_currency)->get();
	    	$data['setting'] 			= DB::table('settings')
										->leftjoin('countries','countries.id','=','settings.company_country')
										->leftjoin('currencies','currencies.id','=','settings.company_currency')
										->where('settings.id',$adminID->company_id)->first();		
	    	endif;

	    	$this->cbView('cashier.create_order',$data);
	}

	public function getUpdate(Request $request){
		// if(!CRUDBooster::isVIew()) CRUDBooster::denyAccess();

			$table_id 					= $request->table_id;
			$table_name					= $request->table_name;

			$myID 						= CRUDBooster::myId();
	    	$adminID 					= DB::table('cms_users')->select('created_by','company_id')->where('id',$myID)->first();
	    	$everyone  					= $adminID->created_by;

	    	$data 						= [];
	    	$data['page_title'] 		= trans("crudbooster.update_order");

	    	if($adminID->company_id == $myID):

	    	else:
	    	$data['tables'] 			= DB::table('table_numbers')->where('created_by',$everyone)->where('id',$table_id)->get();
	    	$data['categories'] 		= DB::table('categories')->where('created_by',$everyone)->orderBy('name','ASC')->get();
	    	$data['subcategories'] 		= DB::table('subcategories')->where('created_by',$everyone)->orderBy('name','ASC')->get();
	    	$data['menus'] 				= DB::table('menus')->where('created_by',$everyone)->orderBy('name','ASC')->get();
	    	$data['user'] 				= DB::table('cms_users')->where('id',$myID)->first();
	    	$data['tax']				= DB::table('settings')->where('created_by',$everyone)->select('company_sale_tax')->first();
	    	$data['company_currency']	= DB::table('settings')->where('settings.created_by',$everyone)
	    								->leftjoin('currencies','currencies.id','=','settings.company_currency')->select('company_currency','currency_symbol')->first();
	    	
	    	$company_currency			= DB::table('settings')->where('created_by',$everyone)->select('company_currency')->first();
	    	$currency 					= DB::table('currencies')->get();
	    	$data['currencies']			= DB::table('currencies')
	    								->leftjoin('exchange','exchange.currency_form','=','currencies.id')
	    								->where('exchange.created_by',$everyone)
	    								->select('currencies.*','exchange.*','currencies.id as id')->get();
	    	$data['other_exchange']		= DB::table('exchange')->where('created_by',$everyone)->where('currency_to',$everyone)->get();

	    	$data['order_menu_payment'] = DB::table('orders')->where('table_id',$table_id)->where('status',1)->orderby('id','DESC')->first();

	    	$data['order_menu']			= DB::table('orders')
	    								  ->join('orders_detail','orders_detail.orders_id','=','orders.id')
	    								  ->leftjoin('menu_uom_price','menu_uom_price.id','=','orders_detail.menus_size')
	    								  ->leftjoin('menus','menus.id','=','menus_id')
	    								  ->leftjoin('menu_materials','menu_materials.id','=','orders_detail.material_id')
	    								  ->select('orders.*','orders_detail.price as order_price','orders_detail.*','menus.*','menu_uom_price.*','menu_materials.name as material_name')
	    								  ->where('table_id',$table_id)
	    								  ->where('orders.status',1)
	    								  ->get();
	    	endif;
	    	$this->cbView('cashier.create_order',$data);
	}

	//show all category when load
	public function show_category(Request $request){
		$myID = CRUDBooster::myId();
		$adminID = DB::table('cms_users')->select('created_by')->where('id',$myID)->first();
		$search_data = $request->search_data;

		if($search_data == null){
		$categories = DB::table('categories')->where('status',1)->where('created_by',$adminID->created_by)->orderBy('name','ASC')->get();	
		}else{
		$categories = DB::table('categories')->where('status',1)->where('created_by',$adminID->created_by)->where('name','LIKE','%' . $search_data . '%')->orderBy('name','ASC')->get();
		}

		?>
        <a class="category_code" data-val="0">
	        <div class="category_item item-0">
	            
	            <?php echo trans('crudbooster.most_sold'); ?>
	            
	        </div>
        </a>

        <?php 
		foreach($categories as $category){ ?>

		<a class="category_code" data-val="<?php echo $category->id; ?>">
            <div class="category_item item-<?php echo $category->id; ?>">
                        
                        <?php echo $category->name; ?>
                        
            </div>
        </a>

		<?php }
	}

	//show all subcategory on load
	public function show_subcategory(Request $request){
			$myID 					= CRUDBooster::myId();
			$adminID 				= DB::table('cms_users')->select('created_by')->where('id',$myID)->first();
			$search_data 			= $request->search_data;

			if($search_data == null):
				$subcategories 		= DB::table('subcategories')->where('status',1)->where('created_by',$adminID->created_by)->orderBy('name','ASC')->get();	
			else:
				$subcategories 		= DB::table('subcategories')->where('status',1)->where('created_by',$adminID->created_by)->orderBy('name','ASC')
										->where('name','LIKE','%' . $search_data . '%')->get();
			endif;


			foreach($subcategories as $subcategory): ?>
			<a class="subcategory_code" data-val="<?php echo $subcategory->id; ?>">
	            <div class="subcategory_item item-<?php echo $subcategory->id; ?>" >
	                        <?php echo $subcategory->name; ?>
	            </div>
	        </a>
			<?php endforeach;
	}

	//show all menu when load
	public function show_menu(Request $request){
		$myID 			= CRUDBooster::myId();
		$adminID 		= DB::table('cms_users')->select('created_by')->where('id',$myID)->first();
		$search_data 	= $request->search_data;
		$url 			= URL::to("/");

		if($search_data == null):
		$menus = DB::table('menus')
				->where('status',1)->where('created_by',$adminID->created_by)->orderBy('name','ASC')->get();	
		else:
		$menus = DB::table('menus')->where('status',1)
				->where('created_by',$adminID->created_by)->where('name','LIKE','%' . $search_data . '%')->orderBy('name','ASC')->get();
		endif;


		foreach($menus as $menu): ?>

		<a class="menu_code" data-val="<?php echo $menu->id; ?>" data-name="<?php echo $menu->name; ?>" data-amount="<?php echo $menu->price; ?>" data-size="<?php echo $menu->uom_size; ?>" >
            <div class="menu_item">
                        <img class="menu_image" src="<?php echo $url .'/'. $menu->photo; ?> "/>
                        <div class="menu_name"><?php echo mb_substr($menu->name,0,14,'utf-8'); ?></div>
            </div>
        </a>
		<?php endforeach;
	}

	//get all subcategory when click on specific category
	public function get_subcategory_type(Request $request){
		$myID 				= CRUDBooster::myId();
		$adminID 			= DB::table('cms_users')->select('created_by')->where('id',$myID)->first();
		$id 				= $request->id;
		
		if($id != null):
			$subcategories 	= DB::table('subcategories')
							->where('status',1)->where('category_id',$id)->where('created_by',$adminID->created_by)
							->orderBy('name','ASC')->get();
		else:
			$subcategories 	= DB::table('subcategories')
							->where('status',1)->where('created_by',$adminID->created_by)
							->orderBy('name','ASC')->get();

		endif;
		
	    foreach ($subcategories as $subcategory): ?>
			<a class="subcategory_code" data-val="<?php echo $subcategory->id; ?>">
	            <div class="subcategory_item item-<?php echo $subcategory->id; ?>">    
	                <?php echo $subcategory->name; ?>
	            </div>
            </a>     
	    <?php endforeach;
	}

	//get all menu when click on specific category
	public function get_menu_type(Request $request){
		$myID 			= CRUDBooster::myId();
		$adminID 		= DB::table('cms_users')->select('created_by')->where('id',$myID)->first();
		$id 			= $request->id;
		$url 			= URL::to("/");

		if($id != null):
			$menus 		= DB::table('menus')
						->where('status',1)->where('category_id',$id)->where('created_by',$adminID->created_by)
						->orderBy('name','ASC')->get();
		else:
			$menus 		= DB::table('menus')->where('status',1)->where('created_by',$adminID->created_by)->orderBy('name','ASC')->get();

		endif;
	    
	    foreach ($menus as $menu): ?>
			<a class="menu_code" data-val="<?php echo $menu->id; ?>" data-name="<?php echo $menu->name; ?>" data-amount="<?php echo $menu->price; ?>" data-size="<?php echo $menu->uom_size; ?>">
	            <div class="menu_item">
	                <img class="menu_image" src="<?php echo $url .'/'. $menu->photo; ?> "/>
	                <div class="menu_name"><?php echo mb_substr($menu->name,0,14,'utf-8'); ?></div>
	            </div>
            </a>       
	    <?php endforeach;
	}

	//get all menu when click on specific subcategory
	public function get_menu_type_with_subcategory(Request $request){
		$myID 			= CRUDBooster::myId();
		$adminID 		= DB::table('cms_users')->select('created_by')->where('id',$myID)->first();
		
		$category_id 	= $request->category_id;
		$id 			= $request->id;

		$url 			= URL::to("/");

		if($id != null):
		$menus 			= DB::table('menus')
						->where('status',1)->where('subcategory_id',$id)->where('created_by',$adminID->created_by)
						->orderBy('name','ASC')->get();

		elseif($category_id != null):
			$menus 		= DB::table('menus')
						->where('status',1)->where('category_id',$category_id)->where('created_by',$adminID->created_by)
						->orderBy('name','ASC')->get();

		else:
			$menus 		= DB::table('menus')
						->where('status',1)->where('created_by',$adminID->created_by)
						->orderBy('name','ASC')->get();
		endif;

	    foreach ($menus as $menu): 	?>
	    		<a class="menu_code" data-val="<?php echo $menu->id; ?>" data-name="<?php echo $menu->name; ?>" data-amount="<?php echo $menu->price; ?>" data-size="<?php echo $menu->uom_size; ?>">
                <div class="menu_item">
                        
                    <img class="menu_image" src="<?php echo $url .'/'. $menu->photo; ?> "/>
                  	<div class="menu_name"><?php echo mb_substr($menu->name,0,14,'utf-8'); ?></div>
                  	  
                </div>
                </a>
                    
	    <?php
	    endforeach;
	}


	//insert material into menu
	public function show_material_for_menu(Request $request){
		$url 			= URL::to("/");
		$myID 			= CRUDBooster::myId();
		$adminID 		= DB::table('cms_users')->select('created_by')->where('id',$myID)->first();
		$menu_id 		= $request->menu_id;
		$menu_size_id	= $request->menu_size;
		$menu_note 	    = $request->menu_note;

		if($menu_id){
			$menu_data = DB::table('menus')
						->leftjoin('menu_uom_price','menu_uom_price.menu_id','=','menus.id')
						->Select('menus.*','menus.id as m_id','menu_uom_price.*','menu_uom_price.id as uom_id')
						->where('menus.id',$menu_id)->where('menu_uom_price.id',$menu_size_id)->first();

			$materials 	= DB::table('menu_detail')
						->join('menu_materials','menu_materials.id','=','menu_detail.material_id')
						->join('menus','menus.id','=','menu_detail.menu_id')
						->select('menu_detail.*','menu_materials.id as menu_materials_id','menu_materials.*','menus.id as menu_id','menus.name as menu_name','menus.price as menu_price','menus.photo as menu_photo')
						->where('menu_materials.created_by',$adminID->created_by)->where('menu_detail.menu_id','=',$menu_id)->get();

			?>


			<?php if($menu_data->uom): ?>
			<div class="item_detail">
				<span class="title" style="display: inline-block;">Size of menu : </span>
					<div class="menu_size active after_add" > 
							 <div class="menu_item_code" data-size="<?php echo $menu_data->uom_id; ?>" style="display:none;"></div>
		                     <div class="menu_code_data"><?php echo $menu_data->uom; ?></div>

					</div>

			</div> <!-- End of size of menu -->
			<?php endif; ?>

			<?php if(count($materials) >= 1): ?>
			

			<div class="material_main after_add_data" style="display: none;">
				<span class="title">Material / More Option : </span>
				<?php foreach($materials as $material): ?>
				<div class="material_item"> 
						<input type="checkbox" style="display: none;" 
						value="<?php echo $material->id;?>" 
						menu-id="<?php echo $material->menu_id; ?>" 
						data-name="<?php echo $material->name;?>" 
						data-price="<?php echo $material->price;?>">
						
						<img class="material_tick hidden" src="<?php echo $url .'/' .'uploads/defualt_image/tick.png'; ?>"/>
	                        <a class="material_code" data-val="<?php echo $material->id; ?>" data-name="<?php echo $material->name; ?>" data-amount="<?php echo $material->price; ?>">
	                            <img class="material_image" src="<?php echo $url .'/'. $material->photo; ?>"/>
	                            <div class="material_name"><?php echo $material->name; ?></div>
	                        </a>
	            </div>

				<?php endforeach; ?>
			<?php endif; ?>
			</div>
			



			<div class="item_note">
				<textarea name="item_note_data" class="form-control item_note_data" data-size="<?php echo $menu_id; ?>" placeholder="Customer Comment for this item"><?php echo $menu_note; ?></textarea>
			</div> <!-- End of item_note -->




			<?php }
	}

	//show discount to order
	public function check_discount_type(Request $request){
		$myID 			= CRUDBooster::myId();
		$adminID 		= DB::table('cms_users')->select('created_by')->where('id',$myID)->first();

		$discount_type 	= $request->discount_type;
		$discounts 		= DB::table('discounts')->where('created_by',$adminID->created_by)->where("discount_type",$discount_type)->get();
		?>
		

		<option value="0">Please choose discount</option>
		<?php foreach ($discounts as $discount): ?>
					<option value="<?php echo $discount->id ;?>"><?php echo $discount->discount?></option>
		<?php endforeach;
	}


	//automatically add promotion pack to order
	public function promotion_pack_deal(Request $request){
		$order_price 	= $request->order_price;
		$m_id 			= $request->m_id;
		$m_qty			= $request->m_qty;

		$myID 			= CRUDBooster::myId();
		$adminID 		= DB::table('cms_users')->select('created_by')->where('id',$myID)->first();
		$now 			= Carbon::now();
	    $today_date 	= $now->toDateString();
		
		$promotion_packs = DB::table('promotion_packs')->where('created_by',$adminID->created_by)->where('status','1')->get(); ?>

		<option value="0">Please choose one promotion only</option>
		<?php
		foreach($promotion_packs as $pro):
				
				//member
				if($pro->promotion_type_id == '1'):

				//price
				elseif($pro->promotion_type_id == '2'):

					if($pro->promotion_price <= $order_price): ?>
						 <option value="<?php echo $pro->id; ?>"> <?php echo $pro->promotion_name;?> </option>
					<?php endif;

				//menu
				elseif($pro->promotion_type_id == '3'):
					
					foreach($m_id as $index => $menu_id):
					if($pro->promotion_menu_id == $menu_id && $m_qty[$index] >= $pro->promotion_qty): ?>
							<option value="<?php echo $pro->id; ?>"> <?php echo $pro->promotion_name;?> </option>
							<?php
						
					endif;
					endforeach;

				//event
				elseif($pro->promotion_type_id == '4'):

					if($pro->start_date <= $today_date && ($pro->end_date > $today_date || $pro->end_date == NULL)): ?>
						<option value="<?php echo $pro->id; ?>"> <?php echo $pro->promotion_name;?> </option>

					<?php endif;

				endif;

		endforeach;

	}


	public function order_discount_rate(Request $request){
		$discount_type 	= $request->discount_type;
		$myID 			= CRUDBooster::myId();
		$adminID 		= DB::table('cms_users')->select('created_by')->where('id',$myID)->first();
		
		$discounts 		= DB::table('discounts')->where('created_by',$adminID->created_by)->where('discount_type',$discount_type)->get();
		?>

		<option value="0">Please select discount rate</option>
		<?php foreach($discounts as $discount): ?>
				<option value="<?php echo $discount->discount;?>"><?php echo $discount->discount ?></option>
		<?php endforeach;


	}


	public function get_promotion_pack_data(Request $request){
		$promotion_pack_id 	= $request->promotion_pack_id;
		$myID 				= CRUDBooster::myId();
		$adminID			= DB::table('cms_users')->select('created_by')->where('id',$myID)->first();

		$promotion_pack	= DB::table('promotion_packs')
							->leftjoin('promotion_type','promotion_type.id','=','promotion_packs.promotion_type_id')
							->leftjoin('menus','menus.id','=','promotion_packs.promotion_menu_id')
							->select('promotion_packs.*','promotion_packs.id as pro_id','promotion_type.name as promotion_type_name','menus.id as menu_id','menus.name as menu_name')
							->where('promotion_packs.id',$promotion_pack_id)
							->where('promotion_packs.status','1')->first();

		$promotion_pack_detail = DB::table('promotion_detail')
								->join('menus','menus.id','=','promotion_detail.menus_id')
								->leftjoin('currencies','currencies.name','=','menus.currency')
								->Select('promotion_detail.*','menus.*','menus.name as menu_name','currencies.*','menus.id as menu_id')
								->where('promotion_id',$promotion_pack->pro_id)->get();
		?>

		<table class="promotion_pack_info_table">
			<tr class="promotion_packs_detail_name">
				<td>Name</td>
				<td><?php echo $promotion_pack->promotion_name; ?></td>
			</tr>

			<tr class="promotion_packs_detail_start_date">
				<td>Start Date</td>
				<td><?php echo $promotion_pack->start_date; ?></td>
			</tr>

			<tr class="promotion_packs_detail_end_date">
				<td>End Date</td>
				<td>
					<?php if($promotion_pack->end_date == null): ?>
					No End Date
					<?php else: ?>
					<?php echo $promotion_pack->end_date; ?>
					<?php endif; ?>
				</td>
			</tr>

			<tr class="promotion_packs_detail_condition">
				<td>Condition</td>
				<td>
					<?php echo  $promotion_pack->promotion_type_name; ?><?php $promotion_type = $promotion_pack->promotion_type_id; ?>
					<?php 
						if 		($promotion_type == '1'):
									echo '(customer whom is a member in our resturant)';
						elseif 	($promotion_type == '2'):
									echo '(customer whom is spending over or equal to <b>' . $promotion_pack->promotion_price .'</b>)';
						elseif 	($promotion_type == '3'):
									echo '(customer whom is ordering specific food: <b>'. $promotion_pack->menu_name .'</b> with <b>' . $promotion_pack->promotion_qty . '</b> quantity)';
						elseif  ($promotion_type == '4'):
									echo '(customer whom is coming in on <b>'. $promotion_pack->promotion_event .'</b> event)';
						endif;

					?>
				</td>
			</tr>

			<tr class="promotion_packs_detail_free">
				<td>Free Type</td>
				<td>
					<?php $discount_type = $promotion_pack->discount_type;?>
					<?php if($discount_type != null):
						echo 'discount';
					else:
						echo 'Free Item';
					endif;
					?>
				</td>
			</tr>

			<?php if($discount_type != null): ?>
			<tr class="promotion_packs_detail_amount">
				<td>Discount Amount</td>
				<td><?php echo $promotion_pack->discount_type;?><?php echo $promotion_pack->discount_amount; ?></td>
			</tr>
			<?php endif;?>



		</table>
		<?php if($discount_type == null):?>
		<table class="promotion_packs_detail">
			<tr class="promotion_packs_detail_title">
				<th>No</th>
				<th>Item</th>
				<th>Quantity</th>
				<th>Price</th>
			</tr>

			
			
				<?php 
					foreach($promotion_pack_detail as $key => $pro_detail): ?>
					<tr class="promotion_packs_detail_free_item">
					<td><?php echo ++$key ?></td>
					<td><?php echo $pro_detail->menu_name; ?></td>
					<td><?php echo $pro_detail->amount;?></td>
					<td><?php echo $pro_detail->currency_symbol;?><?php echo $pro_detail->price; ?></td>
					</tr>
					<?php endforeach;
				?>
						
			
			
		</table>
	<?php endif; ?>	

	<?php				
	}

	public function get_promotion_pack_data_change(Request $request){
		$promotion_pack_id 	= $request->promotion_pack_id;
		if($promotion_pack_id == 0 || $promotion_pack_id == null){
			return;
		}

		$myID 				= CRUDBooster::myId();
		$adminID			= DB::table('cms_users')->select('created_by')->where('id',$myID)->first();

		$promotion_pack	= DB::table('promotion_packs')
							->leftjoin('promotion_type','promotion_type.id','=','promotion_packs.promotion_type_id')
							->leftjoin('menus','menus.id','=','promotion_packs.promotion_menu_id')
							->select('promotion_packs.*','promotion_packs.id as pro_id','promotion_type.name as promotion_type_name','menus.id as menu_id','menus.name as menu_name')
							->where('promotion_packs.id',$promotion_pack_id)
							->where('promotion_packs.status','1')->first();

		$promotion_pack_detail = DB::table('promotion_detail')
								->join('menus','menus.id','=','promotion_detail.menus_id')
								->leftjoin('currencies','currencies.name','=','menus.currency')
								->Select('promotion_detail.*','menus.*','menus.name as menu_name','currencies.*','menus.id as menu_id')
								->where('promotion_id',$promotion_pack->pro_id)->get();
	

		$discount_type = $promotion_pack->discount_type;

		if($discount_type != null): ?>
			<input type="hidden" name="discount_type" id="promotion_pack_discount_type" value="<?php echo $promotion_pack->discount_type; ?>"> 
			<input type="hidden" name="discount_amount" id="promotion_pack_discount_amount" value="<?php echo $promotion_pack->discount_amount;?>" > 
		
		<?php 
		else: 
			foreach($promotion_pack_detail as $key => $pro_detail): ?>
					<tr class="promotion_packs_detail_free_item" style="background: #559e55; color:#fff;">
						<td class="m_id" data-val="<?php echo $pro_detail->menu_id; ?>" style="display:none;"><?php echo $pro_detail->id; ?></td>
						<td data-val="<?php echo $pro_detail->menu_id; ?>"><?php echo $pro_detail->menu_name; ?></td>
						<td class="m_qty">
							<span class="m_qty_data"><?php echo $pro_detail->amount;?></span>
						</td>
						<td class="m_amount"><?php echo $pro_detail->currency_symbol; echo $pro_detail->price; ?></td>
						<td class="m_subamount"><?php echo "Free"; ?></td>
						<td></td>
					</tr>
			<?php endforeach;

		endif;



	}


	//insert menu into table
	public function insert_order(Request $request){
		$myID 			= CRUDBooster::myId();
		$adminID 		= DB::table('cms_users')->select('created_by')->where('id',$myID)->first();
		$setting 		= DB::table('settings')
							->leftjoin('countries','countries.id','=','settings.company_country')
							->leftjoin('currencies','currencies.id','=','settings.company_currency')
							->where('settings.created_by',$adminID->created_by)->first();

		//button functionality
		$button_functionality = $request->button_functionality;

		//table info
		$table_id 		= $request->table_id;
		$table_data 	= $request->table_data;
		$dt 			= Carbon::now()->format('Ymd');
		$date 			= Carbon::now()->toDateString();

		//cashier info
		$cashier_id 	= $request->cashier_id;

		//menu info
		$m_id 			= $request->m_id;

		$menu_id 		= $request->menu_id;
		$m_size 		= $request->m_size;
		$material_id 	= $request->material_id;
		$m_name 		= $request->m_name;
		$m_qty 			= $request->m_qty;
		$m_amount 		= $request->m_amount;
		$m_subamount 	= $request->m_subamount;

		//broken info
		$broken_name 	= $request->broken_name;

		//order note
		$order_note 		= $request->note_info;
		$order_note_detail	= $request->order_note_detail;

		//company_currency
		$currency 		= $request->company_currency;

		//tax
		$tax_amount		= $request->tax_amount;

		//discount
		$discount_type 	= $request->discount_type;
		$discount_amount= $request->discount_amount; 

		//button functionality
		$button_name	= $request->button_name;



		

		$order_id 		= DB::table("orders")->where('created_by',$adminID->created_by)->orderby('id','DESC')->limit(1)->first();

		if($order_id == null){
			$table 		=  implode('-', array( $dt,"1"));
		}else{
			$data 		= explode("-",$order_id->order_number);
			$order_num 	= $data[count($data)-1];
			$data 		= $order_num + 1;
			$table 		=  implode('-', array( $dt,$data));
		}

		$total_no_tax 	= $request->total_no_tax;
		$total_with_tax	= $request->total_with_tax;


		$old_order_id	= $request->order_id;
		$remove_menu_id = $request->remove_menu_id;

		if($old_order_id == null):
		DB::table('orders')->insert(
			[
				'user_id' 					=> $cashier_id,
			 	'order_date' 				=> $date,
			 	'order_number' 				=> $table, 
			 	'table_id' 					=> $table_id,
			 	'created_by' 				=> $adminID->created_by, 
			 	'status' 					=> '1',
			 	'order_note'				=> $order_note,
			 	'currency'					=> $currency,
			 	'tax_amount'				=> $tax_amount,	
			 	'total' 					=> $total_no_tax,
			 	'total_with_tax_discount' 	=> $total_with_tax,
			 	'discount_type'				=> $discount_type,
			 	'discount_amount'			=> $discount_amount,
			 	'created_at' 				=> Carbon::now(),
			 	'updated_at' 				=> null 
			]
		);


		DB::table('table_numbers')->where('id',$table_id)->update(
			[
				'status'					=> 0,
			]
		);

		else:
			if($remove_menu_id != null){
				foreach($remove_menu_id as $key => $m_ids){
					$data = DB::table('orders_detail')->where('menus_id',$remove_menu_id[$key])->where('orders_id',$old_order_id)->delete();		
				}
			}
		

		// $dataUpdate 		= [];
		// 	if($m_id != null){
		// 		foreach($m_id as $key => $m_ids){
		// 			 $dataUpdate[] = [
		// 		        'orders_id'  	=> $old_order_id,
		// 		        'menus_id'    	=> $m_id[$key],
		// 		        'amount' 		=> $m_qty[$key],
		// 		        'subtotal' 		=> $m_subamount[$key],
		// 		        'price' 		=> $m_amount[$key],
		// 		        'status'		=> 1,
		// 		        'created_at' 	=> Carbon::now(),
		//     			];     		
		// 		}
		// 	}



		DB::table('orders')->where('id',$old_order_id)->update(
			[
				'updated_at' 				=> Carbon::now(),
				'total'						=> $total_no_tax,
				'total_with_tax_discount' 	=> $total_with_tax,
			 	'discount_type'				=> $discount_type,
			 	'discount_amount'			=> $discount_amount,
			]
		);

		

		endif;

		$orderID 		= DB::table("orders")->where('created_by',$adminID->created_by)->orderby('id','DESC')->limit(1)->first();
		
		$dataSet 		= [];
		$materialSet 	= [];
		$brokenSet 		= [];

		if($m_id != null){
			foreach($m_id as $key => $m_ids){
				 $dataSet[] = [
			        'orders_id'  	=> $orderID->id,
			        'menus_id'    	=> $m_id[$key],
			        'menus_size'	=> $m_size[$key],
			        'amount' 		=> $m_qty[$key],
			        'subtotal' 		=> $m_subamount[$key],
			        'price' 		=> $m_amount[$key],
			        'status'		=> 1,
			        'order_note'	=> $order_note_detail[$key],
			        'created_at' 	=> Carbon::now(),
	    			];     		
			}
		}

		

		
		DB::table('orders_detail')->insert($dataSet);

		if($material_id != null){
	    		foreach($material_id as $key => $material_ids){
				 	$materialSet[] = 
				 	[
				        'orders_id'  	=> $orderID->id,
				        'menus_id'    	=> $menu_id[$key],
				        'material_id' 	=> $material_id[$key],
				        'amount' 		=> $m_qty[$key],
				        'subtotal' 		=> $m_subamount[$key],
				        'price' 		=> $m_amount[$key],
				        'order_note'	=> $order_note_detail[$key],
				        'status'		=> 1
	    			]; 
				}

			DB::table('orders_detail')->insert($materialSet);
		}

		if($broken_name != null){
	    		foreach($broken_name as $key => $broken_names){
					$brokenSet[] = 
					[
				        'orders_id' 	=> $orderID->id,
				        'amount' 		=> $m_qty[$key],
				        'broken_name' 	=> $broken_name[$key],
				        'subtotal' 		=> $m_subamount[$key],
				        'price' 		=> $m_amount[$key],
				        'order_note'	=> $order_note_detail[$key],
				        'status'	 	=> 1
		    		]; 

				}
			DB::table('orders_detail')->insert($brokenSet);
		}

		$c_dollar 		= $request->c_dollar;
		$c_riel   		= $request->c_riel;
		$c_baht   		= $request->c_baht;
		$return_amount  = $request->return_amount;

		if($button_functionality == 'order and print'):

		if($c_dollar || $c_riel || $c_baht):
			DB::table('invoices')->insert(
			[
				'invoice_date' 		=> $date, 
				'orders_id' 		=> $orderID->id,
				'currency'			=> $currency,
				'void' 				=> 0, 
				'status' 			=> 1, 
				'has_paid'			=> 1,
				'paid_dollar'		=> $c_dollar,
				'paid_riel'			=> $c_riel,
				'paid_baht'			=> $c_baht,
				'return_amount'		=> $return_amount,
				'created_by' 		=> $adminID->created_by,
				'created_at' 		=> now()
			]
		);
		else:
			DB::table('invoices')->insert(
			[
				'invoice_date' 		=> $date, 
				'orders_id' 		=> $orderID->id,
				'currency'			=> $currency,
				'void' 				=> 0, 
				'status' 			=> 1, 
				'created_by' 		=> $adminID->created_by,
				'created_at' 		=> now()
			]
		);
		
		endif;
		endif;

		$takeout_id 		= $request->takeout_id;
		if(strtolower($button_name) == 'order_print'){
				DB::table('table_numbers')->where('id',$table_id)->update(['status'=> 1]);
				DB::table('orders')->where('id',$orderID->id)->update(['status' => 0]);

				if($takeout_id):
					DB::table('orders')->where('order_number',$takeout_id)->update(['status' => 0]);
				endif;

		}



//Create Invoice for them
$last_insert_invoice = DB::table('invoices')->join('orders','orders.id','=','invoices.orders_id')
						->where('invoices.created_by',$adminID->created_by)
						->orderby('id','DESC')->limit(1)
						->first();

$printing_orders = DB::table("orders")
					->join('orders_detail','orders_detail.orders_id','=','orders.id')
					->leftjoin('menus','menus.id','=','menus_id')
					->leftjoin('menu_materials','menu_materials.id','=','material_id')
					->leftjoin('menu_uom_price','menu_uom_price.id','=','orders_detail.menus_size')
					->select('menus.*','menu_materials.*','orders.*','orders_detail.*','menus.name as menu_name','orders_detail.price as order_menu_price','menu_materials.name as menu_material_name','menu_uom_price.*')
					->where('orders.created_by',$adminID->created_by)
					->where('orders.id',$last_insert_invoice->orders_id)
					->orderby('orders.id','DESC')->get();


$tax_amount_dollar = $request->tax_amount_dollar;
$tax_amount_all    = $request->tax_amount_all;
?>


<!-- Start Of Design Invoice -->
<div class="main_invoice">
<div id="container">
    <section id="memo">
        <div class="logo">
          <img data-logo="company_logo" src='<?php echo $setting->company_logo; ?>' />
        </div>
        
        <div class="company-info">
          <div><?php echo $setting->company_name;?></div>

          <br/>
          
          <span><?php echo $setting->company_address;?></span>
          <span><?php echo $setting->country_name;?></span>

          <br/>
          
          <span><?php echo $setting->company_contact;?></span>
          <br/>
          <span><?php echo $setting->company_phone;?></span>
          <br/>	
          <span><?php echo $setting->company_email;?></span>

        </div>

      </section>

      <section id="invoice-title-number">
      
        <span id="title">Invoice</span>
        <span id="number"><?php echo $last_insert_invoice->order_number;?></span>
        
      </section>
      
      <div class="clearfix"></div>
      
      <section id="items">
        
        <table cellpadding="0" cellspacing="0">
        
          <tr>
            <th>No</th>
            <th>Description</th>
            <th>Quantity</th>
            <th>Price</th>
            <th>Subtotal</th>
            
          </tr>
          
          <?php foreach($printing_orders as $key => $order): ?>

          <tr data-iterate="item" class="invoice">
             <!-- Don't remove this column as it's needed for the row commands -->
            <?php if($order->material_id):?>
            <td></td>
            <td class="material"><?php echo $order->menu_material_name; ?></td>
            <?php else: ?>	
            <td><?php echo $key+1; ?></td>
            <td><?php echo $order->menu_name; ?>
            		<br/>
  					<?php if($order->menus_size){ ?>
            		<span class="m_size_name">(<?php echo $order->uom; ?>)</span>
            <?php }	 ?>
            <?php endif;?>

            

            </td>
            <td><?php echo $order->amount; ?></td>
            <td><?php echo $setting->currency_symbol; echo number_format($order->order_menu_price, 2); ?></td>
            <td><?php echo $setting->currency_symbol; echo number_format($order->subtotal, 2);?></td>
          </tr>

      	  <?php endforeach; ?>
          
        </table>
        
      </section>
      
      <section id="sums">
      
        <table cellpadding="0" cellspacing="0">
          <tr>
            <th>SubTotal :</th>
            <td><?php echo $setting->currency_symbol; echo $last_insert_invoice->total; ?></td>
          </tr>
          
          <tr data-iterate="tax">
            <th>Tax(%<?php echo $last_insert_invoice->tax_amount; ?>) :</th>
            <td><?php echo $tax_amount_dollar; ?></td>
            <td><?php echo $tax_amount_all; ?></td>

          </tr>

          <tr data-iterate="discount">
            <th>discount :</th>
            <td><?php 
            		if($last_insert_invoice->discount_amount):
            		echo $last_insert_invoice->discount_amount; echo $last_insert_invoice->discount_type;
            		else:
            		echo "0%";
            		endif; ?></td>
          </tr>
          
          <tr class="amount-total">
            <th>Total :</th>
            <td><?php echo $setting->currency_symbol; echo $last_insert_invoice->total_with_tax_discount;?></td>
          </tr>
          
          <!-- You can use attribute data-hide-on-quote="true" to hide specific information on quotes.
               For example Invoicebus doesn't need amount paid and amount due on quotes  -->
          <tr data-hide-on-quote="true">
            <th>Paid (<?php echo $setting->currency_symbol;?>):</th>
            <td><?php echo $setting->currency_symbol; ?><?php echo $last_insert_invoice->paid_dollar ;?></td>
          </tr>

          <tr data-hide-on-quote="true">

            <th>Paid (៛):</th>
            <td>៛<?php echo $last_insert_invoice->paid_riel ;?></td>

          </tr>
          
          <tr data-hide-on-quote="true">
            <th>Return :</th>
            <td><?php echo $setting->currency_symbol; ?><?php echo $last_insert_invoice->return_amount; ?></td>
          </tr>
          
        </table>

        <div class="clearfix"></div>
        
      </section>
      
      <div class="clearfix"></div>

      <section id="invoice-info">
        <div>
          <span>Issued On</span> <span><?php echo $last_insert_invoice->invoice_date; ?></span>
        </div>
        <!-- <div>
          <span>Due On</span> <span><?php echo $last_insert_invoice->invoice_date; ?></span>
        </div> -->

        <br />

       <!--  <div>
          <span>Currency</span> <span><?php echo $setting->currency_symbol; ?></span>
        </div> -->
        <div>

      </section>
      
      <section id="terms">

        <div class="notes"><?php echo $setting->company_policy;?></div>

        <br />


        
      </section>

      <div class="clearfix"></div>

      <div class="thank-you">Thank You!</div>

      <div class="clearfix"></div>
</div>
</div>


<?php
	}


	//currency deposit
	public function currency_deposit(Request $request){
		$myID 						= CRUDBooster::myId();
	    $adminID 					= DB::table('cms_users')->select('created_by')->where('id',$myID)->first();

	    $company_currency			= $request->company_currency;
	    $input_currency				= $request->input_currency;
	    
	    $currency_amount			= $request->currency_amount;
	    $total 						= $request->total_with_tax;

	    if($input_currency == $company_currency):
	    	if($currency_amount == ''):
	    	$convert_amount 		= $total - 0;
	    	else:
	    	$convert_amount 		= $total - $currency_amount;
	    	endif;

	    else:
	   	$exchange				= DB::table('exchange')
	    							->where('created_by',$adminID->created_by)->where('currency_from_text',$input_currency)->where('currency_to_text',$company_currency)->first();

		    if($currency_amount != '' || $currency_amount !=0):
		    $convert_amount 		   = $total - ($currency_amount * $exchange->price_to)/$exchange->price_from;
			else:
			$convert_amount			   = $total;
			endif;

		endif;
	    
	    if($convert_amount < 0):
	    	echo "0";
	    else:
	    	echo $convert_amount;
		endif;

		
	}


	public function show_old_order(Request $request){
		$old_order_id 	= $request->old_order_id;
		$old_menu_id 	= $request->old_menu_id;


		$order_data 	= DB::table('orders')->join('orders_detail','orders_detail.orders_id','=','orders.id')
						->leftjoin('menus','menus.id','=','orders_detail.menus_id')
						->where('orders_detail.created_at','<>',null)->get();


		foreach ($order_data as $order) :
			if(in_array($order->orders_id,$old_order_id) && !in_array($order->menus_id,$old_menu_id)):

				echo '<tr>';
				echo '<td class="menu_id_remove" style="display:none;">' . $order->menus_id . '</td>';
				echo '<td>' . $order->name	. '<br/>';
				echo '<span class="m_size" data-val="' . $order->menus_id .'" style="display:none;">' . $order->menus_id . '</span>';
				echo '<span class="m_size_name">' . $order->menus_size . '</span></td>';
				echo '<td>' . $order->amount	. '</td>';
				echo '<td>' . $order->price		. '</td>';
				echo '<td>' . $order->subtotal	. '</td>';
				echo '<td style="color:#f00; font-weight:bold;"> Canceled </td>';
				echo '</tr>';
				

			endif;
		endforeach;


	}



	//Printing Ticket
	public function print_ticket(Request $request){
		$order_id = $request->order_id;
		$myID 						= CRUDBooster::myId();
	    $adminID 					= DB::table('cms_users')->select('created_by')->where('id',$myID)->first();

		if(!$order_id){
			$order_ticket = DB::table('orders')->where('created_by',$adminID->created_by)->orderby('id','DESC')->limit(1)->first();
			$order_id 	  = $order_ticket->id;
		}

		if($order_id):

			$print_menu 	= DB::table('orders')->where('orders.id',$order_id)->first();

			$print_datas 	= DB::table('orders')
							  	->join('orders_detail','orders_detail.orders_id','=','orders.id')
							  	->leftjoin('menus','menus.id','=','orders_detail.menus_id')
							  	->leftjoin('menu_uom_price','menu_uom_price.id','=','orders_detail.menus_size')
							  	->leftjoin('table_numbers','table_numbers.id','=','orders.table_id')
							  	->select( 'orders.*',
							  			'orders.order_note as overall_note',
							  			'orders_detail.*',
							  			'orders_detail.order_note as item_note',
							  			'menus.*',
							  			'menus.name as menu_name',
							  			'menu_uom_price.*',
							  			'orders.created_at as order_created_at',
							  			'table_numbers.*')
							  	->where('orders.id',$print_menu->id)
							  	->where('orders_detail.material_id',null)
							  	->get();
			

			foreach($print_datas as $key => $print_data):
			$print_data_material  = DB::table('orders_detail')
									->leftjoin('menu_materials','menu_materials.id','=','orders_detail.material_id')
									->select('menu_materials.*',
						  					 'menu_materials.name as menu_material_name')
									->where('orders_detail.orders_id',$print_menu->id)
									->where('orders_detail.material_id','<>',null)
									->where('menus_id',$print_data->menus_id)
									->get();
									?>


				<div id="menu_<?php echo $print_data->menus_id; ?>" class="printing_ticket">
						
						
						<div class="item_ticket">
							<div class="item_data">
								<span class="p_menu_date">Date : <?php echo $print_data->order_created_at; ?></span>
								<div class="p_menu_table">Table : <?php echo $print_data->table_code; ?></div>
								<div class="p_menu">
									<div class="p_menu_name">Menu 		: <?php echo $print_data->menu_name ;?> </div>
									<?php if($print_data->menus_size): ?>
										<div class="p_menu_size">Size : <?php echo $print_data->uom;?></div>
									<?php endif; ?>
									<div class="p_menu_qty">Quantity 	:<?php echo $print_data->amount; ?></div>

										
								</div>
								
							</div>

							<div class="item_number">
								<div class="p_number"><?php echo ++$key; ?></div>
							</div>

							<div class="item_note">
								<div class="p_note"><?php echo $print_data->item_note; ?></div>
							</div>

							<div class="menu_materials_main">
							<?php foreach ($print_data_material as $key => $material): ?>
								<div class="p_menu_material">Extra : <?php echo $material->menu_material_name;?></div>
							<?php endforeach; ?>
							</div>
							
						</div>
						
						
						
					<div class="overall_note">
						<?php echo $print_data->overall_note ;?>
					</div>

					

				</div>

			<?php
			endforeach;

		endif;



	}



	//show menu size and data
	public function show_menu_data(Request $request){
		$url 			= URL::to("/");
		$myID 			= CRUDBooster::myId();
		$adminID 		= DB::table('cms_users')->select('created_by')->where('id',$myID)->first();
		$menu_id = $request->menu_id;

		if($menu_id){
			$menu_data = DB::table('menus')
						->leftjoin('menu_uom_price','menu_uom_price.menu_id','=','menus.id')
						->Select('menus.*','menus.id as m_id','menu_uom_price.*','menu_uom_price.id as uom_id')
						->where('menus.id',$menu_id)->get();

			$materials 	= DB::table('menu_detail')
						->join('menu_materials','menu_materials.id','=','menu_detail.material_id')
						->join('menus','menus.id','=','menu_detail.menu_id')
						->select('menu_detail.*','menu_materials.id as menu_materials_id','menu_materials.*','menus.id as menu_id','menus.name as menu_name','menus.price as menu_price','menus.photo as menu_photo')
						->where('menu_materials.created_by',$adminID->created_by)->where('menu_detail.menu_id','=',$menu_id)->get();


			?>


			<?php if(count($menu_data)):?>
			<div class="item_detail">
				
				
				<span class="title">Size of menu : </span>

				<div class="menu_detail">
					<?php foreach($menu_data as $menu): ?>
						<?php if($menu->uom): ?>
						<div class="menu_size"> 
		                        <div class="menu_item_code" data-val="<?php echo $menu->m_id; ?>" data-name="<?php echo $menu->name ;?>" data-size="<?php echo $menu->uom_id; ?>" data-size-name="<?php echo $menu->uom; ?>" data-material="<?php echo $menu->material_size;?>" data-amount="<?php echo $menu->uom_price?>">
		                            <div class="menu_code_data"><?php echo $menu->uom; ?></div>
		                        </div>
		            	</div>
		            	<?php endif; ?>
					<?php endforeach; ?>
				</div>
				

			</div> <!-- End of size of menu -->
			<?php endif; ?>

			<?php if(count($materials) >= 1): ?>
			

			<div class="material_main" style="display: none;">
				<span class="title">Material / More Option : </span>
				<?php foreach($materials as $material): ?>
				<div class="material_item"> 
						<input type="checkbox" style="display: none;" 
						value="<?php echo $material->id;?>" 
						menu-id="<?php echo $material->menu_id; ?>" 
						data-name="<?php echo $material->name;?>"
						data-size="<?php echo $menu->uom_id; ?>" 
						data-price="<?php echo $material->price;?>">
						
						<img class="material_tick hidden" src="<?php echo $url .'/' .'uploads/defualt_image/tick.png'; ?>"/>
	                        <a class="material_code" data-val="<?php echo $material->id; ?>" data-name="<?php echo $material->name; ?>" data-amount="<?php echo $material->price; ?>">
	                            <img class="material_image" src="<?php echo $url .'/'. $material->photo; ?>"/>
	                            <div class="material_name"><?php echo $material->name; ?></div>
	                        </a>
	            </div>

				<?php endforeach; ?>
			<?php endif; ?>
			</div>
			



			<div class="item_note_main" style="display: none;">
				<textarea name="item_note_data" class="form-control item_note_data" placeholder="Customer Comment for this item"></textarea>
			</div> <!-- End of item_note -->




			<?php 
		}





	}


	
}