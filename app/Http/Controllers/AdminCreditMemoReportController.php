<?php namespace App\Http\Controllers;

	use Session;
	use Illuminate\Http\Request;
	// use Request;
	use DB;
	use CRUDBooster;

	class AdminCreditMemoReportController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field 			= "id";
			$this->limit 				= "20";
			$this->orderby 				= "id,desc";
			$this->global_privilege 	= false;
			$this->button_table_action 	= false;
			$this->button_bulk_action 	= true;
			$this->button_action_style 	= "button_icon";
			$this->button_add 			= false;
			$this->button_edit 			= true;
			$this->button_delete 		= true;
			$this->button_detail 		= true;
			$this->button_show 			= false;
			$this->button_filter 		= true;
			$this->button_import 		= false;
			$this->button_export 		= false;
			$this->table 				= "orders";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];

			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];

			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ["label"=>"User Id","name"=>"user_id","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"user,id"];
			//$this->form[] = ["label"=>"Order Number","name"=>"order_number","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Created By","name"=>"created_by","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
			//$this->form[] = ["label"=>"Status","name"=>"status","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        
	        
	    }

	    // ========================= Index of Credit Memo report (View) =======================
	    public function getIndex()
	    {
	    	if(!CRUDBooster::isVIew()) CRUDBooster::denyAccess();

	    	$myID 				= CRUDBooster::myId();
	    	$data 				= [];
	    	$data['page_title'] = "Credit Memo Report";


	    	$this->cbView('report.credit_memo',$data);

	    }

	   
	    // ========================= Sort Credit memo (function) =======================
	    public function get_sort(Request $request)
	    {
	    	$myID = CRUDBooster::myId();
	    	if ($request->result == "Weekly") {
				$startDate = date("Y-m-d", strtotime('monday this week'))." 00:00:00";
				$endDate = date("Y-m-d", strtotime('sunday this week'))." 23:59:59";
							
				

			}elseif ($request->result == "Monthly" ) {
				
				$startDate = date('Y-m-01')." 00:00:00";
				$date = date('Y-m-d');
				$endDate = date("Y-m-t", strtotime($date))." 23:59:59";

			
				
			}elseif ($request->result == "Yearly") {
				$startDate = date('Y-01-01')." 00:00:00";
				$date = date('Y-m-d');
				$endDate = date("Y-12-t", strtotime($date))." 23:59:59";

				
			}elseif ($request->result == "Daily" ){
				
				$day = new \DateTime();
				$startDate = $day->format('Y-m-d')." 00:00:00";
				$endDate = $day->format('Y-m-d')." 23:59:59";

				

			}else{

				$request->date;
				$date = explode(" - ",$request->date);
				$startDate = $date[0]." 00:00:00";
				$endDate = $date[1]." 23:59:59";

				if ($startDate == $endDate) {
					$startDate = $date[0]." 00:00:00";
					$endDate = $date[1]." 23:59:59";


				}
				
			}

			$invoices = DB::select("
				SELECT * FROM invoices 
				LEFT JOIN orders ON orders.id = invoices.orders_id
				LEFT JOIN settings ON settings.id = invoices.company_id
				WHERE invoice_date >= '$startDate'
				AND invoice_date < '$endDate' 
				AND void = 1");

			$total = 0;
			if(count($invoices)>=1):
				foreach ($invoices as $key => $invoice):
					
					$totalPrice += $invoice->grand_total;
					?>

					<tr>
						<td class="col-md-1"><?php echo ++$key; ?></td>
						<td class="col-md-1"><?php echo $invoice->invoice_date; ?></td>
						<td class="col-md-1"><?php echo $invoice->order_number; ?></td>
						<td class="col-md-1"><?php echo $invoice->currency . $invoice->grand_total;?></td>
						<?php if($invoice->discount_total_dollar != null): ?>
						<td class="col-md-1"><?php echo $invoice->currency . $invoice->discount_total_dollar; ?></td>
						<?php else: ?>
						<td class="col-md-1"><?php echo $invoice->currency . "0.00"; ?></td>
						<?php endif; ?>
						<?php $sub_total = $invoice->grand_total - $invoice->discount_total_dollar; ?>
						<td class="col-md-1"><?php echo $invoice->currency . $sub_total; ?></td>
						<td class="col-md-1"><?php echo $invoice->currency . $invoice->tax_dollar; ?></td>
						<td class="col-md-1"><?php echo $invoice->company_name;?></td>

						
					</tr>

					


			    <?php endforeach; ?>


			    <tr class="table_grand_total">
			    <td colspan="3" style="text-align: right;">Grand Total</td>
			    <td><?php echo $invoice->currency . $totalPrice; ?></td>
			    <td></td>
			    <td></td>
			    <td></td>
			    <td></td>
			    </tr>

			<?php else: ?>
				 <tr>
	                <td colspan="8" class="text-center"><h4>No Data!</h4></td>
	              </tr>
			
			<?php endif;

			
	    }




	}