<?php namespace App\Http\Controllers;

	use Session;
	// use Request;
	use Illuminate\Http\Request;
	use DB;
	use CRUDBooster;
	use Carbon\Carbon;
	use App\promotion;

	class AdminPromotionPackController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field 			= "promotion_name";
			$this->limit 				= "20";
			$this->orderby 				= "id,desc";
			$this->global_privilege 	= false;
			$this->button_table_action 	= true;
			$this->button_bulk_action 	= true;
			$this->button_action_style 	= "button_icon";
			$this->button_add 			= true;
			$this->button_edit			= true;
			$this->button_delete 		= true;
			$this->button_detail 		= true;
			$this->button_show 			= false;
			$this->button_filter 		= false;
			$this->button_import 		= false;
			$this->button_export 		= false;
			$this->table 				= "promotion_packs";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>trans('promotionpack.Promotion Name'),"name"=>"promotion_name"];
			$this->col[] = ["label"=>trans('promotionpack.Promotion Type'),"name"=>"promotion_type_id","join" => "promotion_type,name"];
			$this->col[] = ["label"=>trans('promotionpack.Start Date'),"name"=>"start_date"];
			$this->col[] = ["label"=>trans('promotionpack.End Date'),"name"=>"end_date"];
			$this->col[] = ["label"=>trans('promotionpack.Status'),"name"=>"status"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];

			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();
	        $this->addaction[] = ['icon'=>'fa fa-clone','color'=>'warning','url'=>CRUDBooster::mainpath('promotion_pack_duplicate').'/[id]' ];


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = "";

            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js 		= array();
	        $this->load_js[]    = asset("js/disable_row.js");
	        $this->load_js[]    = asset("js/end_date_null.js");
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css 	= array();
	        $this->load_css[] 	= asset('css/no_data.css');
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) 
	    {	
	        $myID           = CRUDBooster::myId();
        	$user           = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
	        
	        $now 			= Carbon::now();
	    	$today_date 	= $now->toDateString();
	            
	        $query->where('promotion_packs.company_id', $user->company_id);
            DB::table('promotion_packs')
            	->where('promotion_packs.company_id', $user->company_id)
            	->where('end_date','<',$today_date)
            	->update(['status' => '0']);
            
            DB::table('promotion_packs')
            	->where('promotion_packs.company_id', $user->company_id)
            	->where('end_date','>=',$today_date)
            	->update(['status' => '1']); 
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here
	     	
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {   
	    	

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here


	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {

	        DB::table('promotion_detail')->where('promotion_id',$id)->delete();

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }

	    // ============================= Create Promotion Pack (View) =============================
	    public function getAdd() 
	    {
		    $myID = CRUDBooster::myId();
         	$user = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
		  
			$data = [];
			$data['page_title'] 		= trans('promotionpack.Promotion Pack');
			$data['promotion_types']	= DB::table('promotion_type')->where('status','1')->get();
			$data['command']            = "add";
			$data['menu'] 				= DB::table('menus')->where('company_id',$user->company_id)->get();
		  
		  	$this->cbView('promotion_pack.promotion_packs',$data);
		}

		// ============================= Update Promotion Pack (View) =============================
		public function getEdit($id) 
		{
		  	$myID = CRUDBooster::myId();
         	$user = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();

		 	$data = [];
		  	$data['page_title'] 		= trans('promotionpack.Promotion Pack');
		  	$data['promotion_pack'] 	= DB::table('promotion_packs')->where('id',$id)->first();
		  	$data['promotion_types'] 	= DB::table('promotion_type')->where('status','1')->get();
		  	$data['promotion_detail'] 	= DB::table('promotion_detail')
		  								->where('promotion_id',$id)->leftJoin('menus','menus.id','=','promotion_detail.menus_id')->get();
			$data['menu'] 				= DB::table('menus')->where('company_id',$user->company_id)->get();
		  
		  	$this->cbView('promotion_pack.promotion_packs',$data);
		}

		// ============================= View Promotion Pack (View) =============================
		public function getDetail($id) {
		  
		  	$myID = CRUDBooster::myId();
         	$user = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
			
			$data = [];
			$data['page_title'] 		= trans('crudbooster.detail_promotion_pack');
			$data['promotion_pack'] 	= DB::table('promotion_packs')
											->join('promotion_type','promotion_packs.promotion_type_id','=','promotion_type.id')
											->where('promotion_packs.id',$id)->first();
			$data['promotion_detail'] 	= DB::table('promotion_detail')
											->where('promotion_id',$id)->leftJoin('menus','menus.id','=','promotion_detail.menus_id')->get();	
	
		  $this->cbView('promotion_pack.detail_view_promotion_packs',$data);
		}

		// ============================= Get All Menu in thier outlet (Function) =============================
	    

	    // ============================= Create Promotion Pack (Function) =============================
	    public function save_promotion_pack(Request $request){
	    	$myID 						= CRUDBooster::myId();
         	$user 						= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
	    	$now 						= Carbon::now();
	    	$promotion_name				= $request->promotion_name;
	    	$promotion_type_id			= $request->promotion_type;
	    	$start_date					= $request->start_date;
	    	$end_date					= $request->end_date;
	    	$promotion_price			= $request->promotion_price;
	    	$promotion_menu_id 			= $request->promotion_menu;	
	    	$promotion_qty				= $request->promotion_qty;
	    	$promotion_event			= $request->promotion_event;
	    	$promotion					= $request->promotion;
	    	$discount_type				= $request->discount_type;
	    	$discount_amount			= $request->discount_amount;
	    	$menu 						= $request->menu;
	    	$menu_amount				= $request->menu_amount;
	    	$created_by 				= $myID;
         	$company_id 				= $user->company_id;
         	$submit 					= $request->submit;
         	$end_date_checkbox			= $request->end_date_checkbox;


         	if($end_date_checkbox == null){
         		if($end_date==null){
         			
         		}else{
			    	DB::table('promotion_packs')->insert(
			    		[
			    			'promotion_name' 	=> $promotion_name,
			    			'promotion_type_id'	=> $promotion_type_id,
			    			'start_date'		=> $start_date,
			    			'end_date'			=> $end_date,
			    			'promotion_price'	=> $promotion_price,
			    			'promotion_menu_id'	=> $promotion_menu_id,
			    			'promotion_qty'		=> $promotion_qty,
			    			'promotion_event'	=> $promotion_event,
			    			'created_by'		=> $created_by,
			    			'company_id'		=> $company_id,
			    			'created_at'		=> $now,
			    			'promotion'			=> $promotion,
			    			'discount_type'		=> $discount_type,
			    			'discount_amount'	=> $discount_amount,
			    		]
			    	);
	    		}
	    	}else{
		    	DB::table('promotion_packs')->insert(
		    		[
		    			'promotion_name' 	=> $promotion_name,
		    			'promotion_type_id'	=> $promotion_type_id,
		    			'start_date'		=> $start_date,
		    			
		    			'promotion_price'	=> $promotion_price,
		    			'promotion_menu_id'	=> $promotion_menu_id,
		    			'promotion_qty'		=> $promotion_qty,
		    			'promotion_event'	=> $promotion_event,
		    			'created_by'		=> $created_by,
		    			'company_id'		=> $company_id,
		    			'created_at'		=> $now,
		    			'promotion'			=> $promotion,
		    			'discount_type'		=> $discount_type,
		    			'discount_amount'	=> $discount_amount,
		    		]
		    	);
	    	}

	    	$last_promotion_pack 	= DB::table('promotion_packs')->where('created_by',$created_by)->where('company_id',$user->company_id)
	    								->OrderBy('id','DESC')->first();

	    	if($last_promotion_pack->promotion == "free"):
	    		foreach ($menu as $key => $value):
                DB::table('promotion_detail')->insert(
                	[
                		'promotion_id' 	=> $last_promotion_pack->id,
                		'menus_id'		=> $value,
                		'amount'		=> $menu_amount[$key],
                	]
                );	
	        	endforeach;
	    	endif;

	    	if(strtolower($submit) == "save"){
	    		CRUDBooster::redirect(CRUDBooster::mainpath(), trans("crudbooster.alert_add_data_success"), 'success');
	    	}else{
	    		CRUDBooster::redirect($_SERVER['HTTP_REFERER'], trans("crudbooster.alert_add_data_success"), 'success');
	    	}	
	    }

	    // ============================= Update Promotion Pack (Function) =============================
	    public function update_promotion_pack(Request $request)
	    {
	    	$myID 						= CRUDBooster::myId();
         	$user 						= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
	    	$now 						= Carbon::now();

	    	$promotion_id 				=	$request->promotion_id;
	    	$promotion_name 			= 	$request->promotion_name;
	    	$promotion_type_id			= 	$request->promotion_type;
	    	$start_date 				=	$request->start_date;
	    	$end_date 					= 	$request->end_date;
	    	$promotion_price			= 	$request->promotion_price;
	    	$promotion_menu_id 			= 	$request->promotion_menu;	
	    	$promotion_qty				= 	$request->promotion_qty;
	    	$promotion_event			= 	$request->promotion_event;
	    	$promotion					= 	$request->promotion;
	    	$discount_type				= 	$request->discount_type;
	    	$discount_amount			= 	$request->discount_amount;
	    	$menus 						= 	$request->menu;
	    	$menu_amount				= 	$request->menu_amount;
	    	$created_by 				= 	$myID;
         	$company_id 				= 	$user->company_id;
         	$end_date_checkbox			=	$request->end_date_checkbox;

         	if($promotion_id == null || $promotion_type_id == null || $start_date == null):
         		CRUDBooster::redirect($_SERVER['HTTP_REFERER'],trans("crudbooster.Please enter valid information"),"warning");
         	else:

         	if($end_date_checkbox == null):
         	
	         	$pro 					= promotion::find($promotion_id);
		    	$pro->promotion_name 	= $promotion_name;
		    	$pro->promotion_type_id = $promotion_type_id;
		    	$pro->start_date 		= $start_date;
		    	$pro->end_date			= $end_date;
		    	$pro->promotion_price 	= $promotion_price;
		    	$pro->promotion_menu_id = $promotion_menu_id;
		    	$pro->promotion_qty		= $promotion_qty;
		    	$pro->promotion_event 	= $promotion_event;
		    	$pro->updated_at		= $now;
		    	$pro->promotion 		= $promotion;
			    	if($discount_type != NULL):
			    		$pro->discount_type		= $discount_type;
			    		$pro->discount_amount	= $discount_amount;
			    	else:

			    		$pro->discount_type		= NULL;
			    		$pro->discount_amount	= NULL;	
			    	endif;

		    	DB::table('promotion_detail')->where('promotion_id',$promotion_id)->delete();

		    	$last_pro = DB::table('promotion_packs')->where('id',$promotion_id)->first();
			  		if($last_pro->promotion == 'free'):
			    	foreach ($menus as $key => $menu):
				   			DB::table('promotion_detail')->insert(
				    		 [		
				    		      'promotion_id' => $promotion_id,
					    		  'menus_id' => $menu , 
					    		  'amount' => $menu_amount[$key]
					    		 ]
				    		);
				    endforeach;
					endif;
		    	$pro->save();

         	else:

         	$pro 					= promotion::find($promotion_id);
	    	$pro->promotion_name 	= $promotion_name;
	    	$pro->promotion_type_id = $promotion_type_id;
	    	$pro->start_date 		= $start_date;
	    	$pro->end_date 			= null;
	    	$pro->promotion_price 	= $promotion_price;
	    	$pro->promotion_menu_id = $promotion_menu_id;
	    	$pro->promotion_qty		= $promotion_qty;
	    	$pro->promotion_event 	= $promotion_event;
	    	$pro->updated_at		= $now;
	    	$pro->promotion 		= $promotion;
	    	if($discount_type != NULL):
	    		$pro->discount_type		= $discount_type;
	    		$pro->discount_amount	= $discount_amount;
	    	else:
	    		$pro->discount_type		= NULL;
	    		$pro->discount_amount	= NULL;	
	    	endif;

	    	DB::table('promotion_detail')->where('promotion_id',$promotion_id)->delete();

	    	$last_pro = DB::table('promotion_packs')->where('id',$promotion_id)->first();
		  		if($last_pro->promotion == 'free'):
		    	foreach ($menus as $key => $menu):
			   			DB::table('promotion_detail')->insert(
			    		 [		
			    		      'promotion_id' => $promotion_id,
				    		  'menus_id' => $menu , 
				    		  'amount' => $menu_amount[$key]
				    		 ]
			    		);
			    endforeach;
				endif;
	    	$pro->save();
         	
         	endif;

         	CRUDBooster::redirect($_SERVER['HTTP_REFERER'],trans('crudbooster.alert_update_data_success'),"info");

         	endif;
	    	
	        
	    	
	    }

	    // ============================= Duplicate Promotion Pack (Function) =============================
	    public function promotion_pack_duplicate($id){
	    	$myID 	= CRUDBooster::myId();
         	$user 	= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
	    	$now 	= Carbon::now();

	    	$promotion_pack 	= DB::table('promotion_packs')->where('id',$id)->first();
	        $promotion_details 	= DB::table('promotion_detail')->join('promotion_packs','promotion_packs.id','=','promotion_detail.promotion_id')->where('promotion_packs.id',$id)->get();

	        DB::table('promotion_packs')->insert(
	        [
                'promotion_name' 	=> $promotion_pack->promotion_name,
                'promotion_type_id'	=> $promotion_pack->promotion_type_id,
                'start_date'		=> $promotion_pack->start_date,
                'end_date'			=> $promotion_pack->end_date,
                'promotion'			=> $promotion_pack->promotion,
                'discount_type'		=> $promotion_pack->discount_type,
                'discount_amount'	=> $promotion_pack->discount_amount,
                'created_by'		=> $promotion_pack->created_by,
                'company_id' 		=> $user->company_id,
                'status'			=> $promotion_pack->status,
                'created_at'		=> $promotion_pack->now,
                'updated_at'		=> NULL,
                'promotion_price'	=> $promotion_pack->promotion_price,
                'promotion_menu_id'	=> $promotion_pack->promotion_menu_id,
                'promotion_qty'		=> $promotion_pack->promotion_qty,
                'promotion_event'	=> $promotion_pack->promotion_event,

	        ]
	        );


	        $last_menu = DB::table('promotion_packs')->orderby('id','DESC')->where('created_by',$promotion_pack->created_by)->where('company_id',$user->company_id)->first();
	        foreach($promotion_details as $promotion_detail):
	        DB::table('promotion_detail')->insert(
	            [
	                'promotion_id'  => $last_menu->id,
	                'menus_id'      => $promotion_detail->menus_id,
	                'amount'		=> $promotion_detail->amount,
	               
	            ]
	        );
	        endforeach;

	        CRUDBooster::redirect($_SERVER['HTTP_REFERER'],trans('crudbooster.data_has_duplicate'),"info");
		}

	  


	}