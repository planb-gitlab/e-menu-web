<?php namespace App\Http\Controllers;

	use Session;
	use Illuminate\Http\Request;
	use DB;
	use CRUDBooster;
	use URL;
	use Carbon\Carbon;
	use Image;

	class AdminMenuMaterialsController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

	    	$myID = CRUDBooster::myId();
			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field 			= "name";
			$this->limit 				= "20";
			$this->orderby 				= "id,desc";
			$this->global_privilege 	= false;
			$this->button_table_action 	= true;
			$this->button_bulk_action 	= true;
			$this->button_action_style 	= "button_icon";
			$this->button_add 			= true;
			$this->button_edit 			= true;
			$this->button_delete 		= true;
			$this->button_detail 		= false;
			$this->button_show 			= false;
			$this->button_filter 		= false;
			$this->button_import 		= false;
			$this->button_export 		= false;
			$this->table 				= "menu_materials";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Name","name"=>"name"];
			$this->col[] = ["label"=>"Photo","name"=>"photo","image"=>true,"width"=>"80"];
			$this->col[] = ["label"=>"Price","name"=>"price"];
			$this->col[] = ["label"=>"Status","name"=>"status"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Addon Name','name'=>'name','type'=>'text'];
			$this->form[] = ['label'=>'Addon Photo','name'=>'photo','type'=>'upload'];
			$this->form[] = ['label'=>'Addon Price','name'=>'price','type'=>'text'];

			# END FORM DO NOT REMOVE THIS LINE

	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        $this->load_js[]    = asset("js/disable_row.js");
	        
	        
	        

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css   = array();
	        $this->load_css[] = asset('css/no_data.css');
        	$this->load_css[] = asset('css/img_display_fit.css');
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }
	    
	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        $myID 		= CRUDBooster::myId();
	        $user 		= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
	        $query->where('menu_materials.company_id', $user->company_id);
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 


	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        
	        $myID           = CRUDBooster::myId();
	        $menu_material  = DB::table('menu_detail')->select('id')->where('material_id',$id)->get();

	        if(count($menu_material) >= 1):
	            CRUDBooster::redirect($_SERVER['HTTP_REFERER'], trans("crudbooster.data_is_in_use_menu"), "warning");
	            die();
	        endif;

	    }
	    
	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }


	    // ======================== Crate Menu (View) ============================
	    public function getAdd(){
	    	$myID           = CRUDBooster::myId();
	        $user           = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();

	        $data                   = [];
	        $data['command']		= 'add';
	        $data['page_title']     = trans('menumanagement.Addon');
	        $this->cbView('menu_management.addon.add-on',$data);
	    }

	    // ======================== Update Menu (View) ============================
	    public function getEdit($id){
	    	$myID           = CRUDBooster::myId();
	        $user           = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();

	        $data                   = [];
	        $data['url']            = URL::to("/");
	        $data['page_title']     = trans('menumanagement.Addon');
	        $data['id']             = $id;
	        $data['material']    	= DB::table('menu_materials')->where('id',$id)->first();
	        $this->cbView('menu_management.addon.add-on',$data);

	    }

	    // ======================== Crate Menu (Function) ============================
	    public function save(Request $request){
	        $myID           = CRUDBooster::myId();
	        $user           = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
	        $now            = Carbon::now();
	        $name           = $request->name;
	        $submit         = $request->submit;

	        $photo                  = $request->file('photo');
	        if(!empty($photo)):
	        $photo_db               = "uploads/client_photo/". time() . '.' . $photo->getClientOriginalExtension();
	        $photo_upload           = time() . '.' . $photo->getClientOriginalExtension();
	        Image::make($photo)->fit(136,131)->save('../storage/app/uploads/client_photo/' . $photo_upload);
	    	endif;

	        if($request->price):
	        $price 			= $request->price;
	        else:
			$price 			= 0;
	        endif;

	        $currency 		= DB::table('settings')->select('company_currency')
	        				->leftjoin('currencies','currencies.id','=','settings.company_currency')
	        				->where('settings.id',$user->company_id)->first();

	        

	        if($name): 
	            $checkExist     = DB::table('menu_materials')->where('name', $name)->where('company_id', $user->company_id)->get();
	            if(count($checkExist) >= 1):
	                CRUDBooster::redirect($_SERVER['HTTP_REFERER'], trans("crudbooster.exist_data"), "warning");
	            else:
	                DB::table('menu_materials')->insert(
	                    [	
	                        'name'          => $name,
	                        'photo'         => $photo_db,
	                        'price'			=> $price,
	                        'currency'		=> $currency->company_currency,
	                        'company_id'    => $user->company_id,
	                        'created_by'    => $myID,
	                        'status'		=> '1',
	                        'created_at'    => $now
	                    ]
	                );

	               if(strtolower($submit) == strtolower(trans('crudbooster.button_save'))):
	                CRUDBooster::redirect(CRUDBooster::mainpath(), trans("crudbooster.alert_add_data_success"), 'success');
	                else:
                    CRUDBooster::redirect($_SERVER['HTTP_REFERER'], trans("crudbooster.alert_add_data_success"), 'success');
                	endif;
	            endif;
	        endif;
	    }

	    // ======================== Update Menu (View) ============================
	    public function update(Request $request){
	        $myID           = CRUDBooster::myId();
	        $user           = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
	        $now            = Carbon::now();
	        $id             = $request->id;
	        $name           = $request->name;
            $image 			= DB::table('menu_materials')->where('id',$id)->first();
            if($image->photo != "" || $image->photo != null){
                $menu_image = DB::table('menu_materials')->select('photo')->where('id',$id)->first();
                $photo_db = $menu_image->photo;
                
            }else{
                $photo                  = $request->file('photo');
                $photo_db               = "uploads/client_photo/". time() . '.' . $photo->getClientOriginalExtension();
                $photo_upload           = time() . '.' . $photo->getClientOriginalExtension();
                Image::make($photo)->fit(136,131)->save('../storage/app/uploads/client_photo/' . $photo_upload);
            }

	        if($request->price):
	        $price 			= $request->price;
	        else:
			$price 			= 0;
	        endif;

	        $currency 		= DB::table('settings')->select('company_currency')
	        				->leftjoin('currencies','currencies.id','=','settings.company_currency')
	        				->where('settings.id',$user->company_id)->first();

	        if($name): 
	                DB::table('menu_materials')->where('id',$id)->update(
	                    [
	                        'name'          => $name,
	                        'photo'         => $photo_db,
	                        'price'			=> $price,
	                        'currency'		=> $currency->company_currency,
	                        'company_id'    => $user->company_id,
	                        'created_by'    => $myID,
	                        'updated_at'    => $now
	                    ]
	                );
	                CRUDBooster::redirect($_SERVER['HTTP_REFERER'], trans("crudbooster.alert_update_data_success"), "info");
	        endif;

	    } 


	}