<?php 
	namespace App\Http\Controllers;
	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use URL;
	use Carbon\Carbon;

	class AdminOrderController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field 			= "name";
			$this->limit 				= "20";
			$this->orderby 				= "id,desc";
			$this->global_privilege 	= false;
			$this->button_table_action 	= false;
			$this->button_bulk_action 	= false;
			$this->button_action_style 	= "button_icon";
			$this->button_add 			= false;
			$this->button_edit 			= false;
			$this->button_delete 		= false;
			$this->button_detail 		= false;
			$this->button_show 			= false;
			$this->button_filter 		= false;
			$this->button_import 		= false;
			$this->button_export 		= false;
			$this->table 				= "menus";
			# END CONFIGURATION DO NOT REMOVE THIS LINE
		}

		// ============================== Index of Take Out (View) ============================
	    public function getIndex(){
	    	
	    	$myID 			= CRUDBooster::myId();
        	$user 			= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
	    	$data 			= [];
	    	$data['order'] 	= DB::table('invoices')
					    		->where('company_id',$user->company_id)
					    		->whereOrderType('takeout')
					    		->whereHasPaid(0)
					    		->whereVoid(0)
					    		->get();

	    	$data['page_title'] 		= trans('order.takeout_order');
	    	$data['order_type'] 		= "takeout"; // PLEASE DO NOT CHANGE THIS
	    	$this->cbView('order.takeout',$data);
	    }

	    // ============================== Get all Take out order that haven't been paid yet (View) ============================
	    public function get_order_data(){
	    	
	    	$myID 		= CRUDBooster::myId();
        	$companyId 	= getCompanyId($myID);
	    	$order 		= DB::table('invoices')->where('company_id', $companyId)
				    		->whereOrderType('takeout')
				    		->whereHasPaid(0)->get();

		        foreach($order as $key => $view): ?>
			        <tr>
		                <td><?php echo ++$key; ?></td>
		                <td><?php echo $view->order_code; ?></td>
		                <td><?php echo $view->invoice_date; ?></td>
		                <td><?php echo Carbon::parse($view->created_at)->format('g:i A'); ?></td>
		                <td>
		                	<a class="view_order" href='<?php route("cashier_update_order");?>?table_id=0&table_name=0000&order_data=old, _blank'>
	                            <i class="fa fa-eye"></i>
	                         </a>
		                </td>
		            </tr>
	    		<?php
	    		endforeach;
	    }

	}