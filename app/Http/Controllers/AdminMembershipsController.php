<?php namespace App\Http\Controllers;

	use Session;
	use Illuminate\Http\Request;
	use DB;
	use CRUDBooster;
	use Carbon\Carbon;

	class AdminMembershipsController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field 			= "name";
			$this->limit 				= "20";
			$this->orderby 				= "id,desc";
			$this->global_privilege 	= false;
			$this->button_table_action 	= true;
			$this->button_bulk_action 	= true;
			$this->button_action_style 	= "button_icon";
			$this->button_add 			= true;
			$this->button_edit 			= true;
			$this->button_delete 		= true;
			$this->button_detail 		= true;
			$this->button_show 			= false;
			$this->button_filter 		= false;
			$this->button_import 		= false;
			$this->button_export 		= false;
			$this->table 				= "memberships";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col 	= [];
			$this->col[] = ["label"=>"First Name","name"=>"f_name"];
			$this->col[] = ["label"=>"Last Name","name"=>"l_name"];
			$this->col[] = ["label"=>"Membership No","name"=>"membership_no"];
			$this->col[] = ["label"=>"Status","name"=>"status"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>trans('setting.First Name'),'name'=>'f_name','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
			$this->form[] = ['label'=>trans('setting.Last Name'),'name'=>'l_name','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
			$this->form[] = ['label'=>trans('setting.Address'),'name'=>'address','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10','placeholder'=>'Input member current address'];
			$this->form[] = ['label'=>trans('setting.Date Of Birth'),'name'=>'dob','type'=>'datetime_without','validation'=>'required|date','width'=>'col-sm-10','placeholder'=>'Input member date of birth'];
			$this->form[] = ['label'=>trans('setting.Phone Number'),'name'=>'phone','type'=>'number','validation'=>'required|numeric','width'=>'col-sm-10','placeholder'=>'You can only enter the number only'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Name','name'=>'name','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
			//$this->form[] = ['label'=>'Address','name'=>'address','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Dob','name'=>'dob','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Phone','name'=>'phone','type'=>'number','validation'=>'required|numeric','width'=>'col-sm-10','placeholder'=>'You can only enter the number only'];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	            
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here

	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }

	    // ======================== Create Membership (View) ============================
	    public function getAdd(){
	    	$myID                       = CRUDBooster::myId();
	        $user                       = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
	           
	        $data                       = [];
	        $data['page_title']         = trans('membership.Membership');
	        $data['command']            = "add";
	        $this->cbView('setting.membership.membership',$data);
	    }

	    // ======================== Update Membership (View) ============================
	    public function getEdit($id){
	    	$myID                       = CRUDBooster::myId();
	        $user                       = DB::table('cms_users')->select('company_id')->where('id',$myID)->first();

	    	$data                       = [];
	    	$data['id']					= $id;
	    	$data['member']				= DB::table('memberships')->where('id',$id)->first();
	        $data['page_title']         = trans('membership.Membership');
	        $data['command']            = "update";
	        $this->cbView('setting.membership.membership',$data);
	    }

	    // ======================== Create Membership (Function) =========================
	    public function save(Request $request){
	    	$myID       	= CRUDBooster::myId();
	        $user       	= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
	        $f_name     	= $request->f_name;
	        $l_name     	= $request->l_name;
	        $address 		= $request->address;
	        $dob 			= $request->dob;
	        $phone 			= $request->phone;
	        $membership_no 	= $request->user_pin;


	        $client_db  = DB::table('memberships')->select('membership_no')->where('company_id',$user->company_id)->get();
	        foreach($client_db as $c):
	            if($membership_no == $c->membership_no):
	                CRUDBooster::redirect($_SERVER['HTTP_REFERER'],trans('crudbooster.pin_already_in_use'),"info"); 
	            endif;
	        endforeach;


	        $dataSet = [
	            'f_name'            => $f_name,
	            'l_name'            => $l_name,
	            'address'			=> $address,
	            'dob'				=> $dob,
	            'phone'				=> $phone,
	            'membership_no'		=> $membership_no,
	            'company_id'		=> $user->company_id,
	            'status'			=> 1,
	            'created_by'		=> $user->company_id,
	            'created_at'		=> Carbon::now()
	        ];

	        if($dataSet != null):
	            DB::table('memberships')->insert($dataSet);
	        endif;
	        CRUDBooster::redirect(CRUDBooster::mainpath(), trans("crudbooster.alert_add_data_success"), 'success');
	    }

	    // ======================== Update Membership (Function) =========================
	    public function update(Request $request){
	    	$myID       	= CRUDBooster::myId();
	        $user       	= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();
	        $id 			= $request->id;
	        $f_name     	= $request->f_name;
	        $l_name     	= $request->l_name;
	        $address 		= $request->address;
	        $dob 			= $request->dob;
	        $phone 			= $request->phone;
	        $membership_no 	= $request->user_pin;

	        $client_db  = DB::table('memberships')->select('membership_no')->where('company_id',$user->company_id)->get();
	        foreach($client_db as $c):
	            if($membership_no == $c->membership_no):
	                CRUDBooster::redirect($_SERVER['HTTP_REFERER'],trans('crudbooster.pin_already_in_use'),"info"); 
	            endif;
	        endforeach;

	        $dataSet = [
	            'f_name'            => $f_name,
	            'l_name'            => $l_name,
	            'address'			=> $address,
	            'dob'				=> $dob,
	            'phone'				=> $phone,
	            'membership_no'		=> $membership_no,
	            'company_id'		=> $user->company_id,
	            'status'			=> 1,
	            'created_by'		=> $user->company_id,
	            'created_at'		=> Carbon::now()
	        ];

	        if($dataSet != null):
	            DB::table('memberships')->where('id',$id)->update($dataSet);
	        endif;
	        CRUDBooster::redirect(CRUDBooster::mainpath(), trans("crudbooster.alert_add_data_success"), 'success');
	    }

	}