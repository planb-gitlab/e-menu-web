<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;

	class AdminRevenueController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field			 	= "id";
			$this->limit 					= "20";
			$this->orderby 					= "id,desc";
			$this->global_privilege 		= false;
			$this->button_table_action 		= false;
			$this->button_bulk_action 		= false;
			$this->button_action_style 		= "button_icon";
			$this->button_add 				= false;
			$this->button_edit 				= false;
			$this->button_delete 			= false;
			$this->button_detail 			= false;
			$this->button_show 				= false;
			$this->button_filter 			= false;
			$this->button_import 			= false;
			$this->button_export 			= false;
			$this->table 					= "orders";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			}

	    //By the way, you can still create your own method in here... :) 
		public function getIndex(){
			if(!CRUDBooster::isVIew()) CRUDBooster::denyAccess();

	    	$myID 							= CRUDBooster::myId();
	    	$data 							= [];
	    	$data['page_title'] 			= "Revenue Report";
	    	$data['setting']				= DB::table('settings')->leftjoin('currencies','currencies.id','=','settings.company_currency')
	    									->where('settings.created_by',$myID)->first();
	    	$data['overall_ravenue_first']	= DB::table('orders')->select('order_date')->where('created_by',$myID)->orderBy('order_date','asc')->first();
	    	$data['overall_ravenue_last']	= DB::table('orders')->select('order_date')->where('created_by',$myID)->orderBy('order_date','desc')->first();
	    	$data['overall_ravenue']		= DB::table('orders')->select('total_with_tax_discount','created_at')->where('created_by',$myID)->get();

	    	$this->cbView('report.revenue',$data);
		}

	}