<?php namespace App\Http\Controllers;

	use Session;
	use Illuminate\Http\Request;
	use DB;
	use CRUDBooster;
	use URL;

	class AdminOutletController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field 			= "company_name";
			$this->limit 				= "20";
			$this->orderby 				= "id,desc";
			$this->global_privilege 	= false;
			$this->button_table_action 	= true;
			$this->button_bulk_action 	= true;
			$this->button_action_style 	= "button_icon";
			$this->button_add 			= true;
			$this->button_edit 			= true;
			$this->button_delete 		= true;
			$this->button_detail 		= true;
			$this->button_show 			= false;
			$this->button_filter 		= false;
			$this->button_import 		= false;
			$this->button_export 		= false;
			$this->table 				= "settings";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Outlet Name","name"=>"company_name"];
			$this->col[] = ["label"=>"Outlet Address","name"=>"company_address"];
			$this->col[] = ["label"=>"Outlet User","name"=>"created_by","join" => "cms_users,name"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Outlet Name','name'=>'company_name','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Outlet Address','name'=>'company_address','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Outlet Country','name'=>'company_country','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Outlet Logo','name'=>'company_logo','type'=>'upload','validation'=>'','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Outlet Info','name'=>'company_info','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Outlet Email','name'=>'company_email','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Outlet Contact','name'=>'company_contact','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Outlet Phone','name'=>'company_phone','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Outlet Policy','name'=>'company_policy','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Outlet Website','name'=>'company_website','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Outlet Lang','name'=>'company_lang','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Outlet Currency','name'=>'company_currency','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Outlet Old Currency','name'=>'company_old_currency','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Include Tax','name'=>'include_tax','type'=>'checkbox','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Outlet Sale Tax','name'=>'company_sale_tax','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Outlet Tax Type Id','name'=>'company_tax_type_id','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Outlet Pin','name'=>'company_pin','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Print Setting','name'=>'print_setting','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Status','name'=>'status','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			# END FORM DO NOT REMOVE THIS LINE


			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	            
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {  
	    	$postdata['include_tax'] 			= $_POST['include_tax'] == 'on' ? '1' : '0';
	    	$postdata['company_sale_tax'] 		= $_POST['include_tax'] == 'on' ? $_POST['company_sale_tax'] : null;
	    	$postdata['company_tax_type_id'] 	= $_POST['include_tax'] == 'on' ? $_POST['company_tax_type_id'] : null;
	        $postdata['company_pin'] 			= $_POST['company_pin'];
	        $postdata['print_setting'] 			= 3;
	        $postdata['status'] 				= 1;
	        $postdata['created_by'] 			= $_POST['company_outlet_user'];
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        DB::table('cms_users')->where('id',$_POST['company_outlet_user'])->update([
	        	'company_id' 	=> $id,
	        	'company_pin' 	=> $_POST['company_pin']
	        ]);
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here
	        $postdata['company_logo']			= $_POST['company_logo'];
	        $postdata['include_tax'] 			= $_POST['include_tax'] == 'on' ? '1' : '0';
	    	$postdata['company_sale_tax'] 		= $_POST['include_tax'] == 'on' ? $_POST['company_sale_tax'] : null;
	    	$postdata['company_tax_type_id'] 	= $_POST['include_tax'] == 'on' ? $_POST['company_tax_type_id'] : null;
	        $postdata['company_pin'] 			= $_POST['company_pin'];
	        $postdata['print_setting'] 			= 3;
	        $postdata['status'] 				= 1;
	        $postdata['created_by'] 			= $_POST['company_outlet_user'];

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 
	        DB::table('cms_users')->where('id',$_POST['company_outlet_user'])->update(['company_id' => $id]);
	        if($_POST['company_pin']):
	        	DB::table('cms_users')->where('id',$_POST['company_outlet_user'])->update(['company_pin' => $_POST['company_pin']]);
	        endif;

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }


	    public function getAdd(){
	    	$myID           		= CRUDBooster::myId();
	        $user           		= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();

	        $data                   = [];
	        $data['url']            = URL::to("/");
	        $data['page_title']     = "Request Outlet";
	        $data['command']        = "add";
	        $data['users']			= DB::table('cms_users')->where('created_by','1')->get();
	        $data['countries']		= DB::table('countries')->get();
           	$data['languages'] 		= DB::table('languages')->orderBy('id','ASC')->get();
           	$data['currencies'] 	= DB::table('currencies')->get();
           	$data['tax_types']		= DB::table('tax_types')->get();
	        $this->cbView('outlet/request_outlet',$data);
	    }

	    public function getEdit($id){
	    	$myID           		= CRUDBooster::myId();
	        $user           		= DB::table('cms_users')->select('company_id')->where('id',$myID)->first();

	        $data                   = [];
	        $data['url']            = URL::to("/");
	        $data['row']       		= DB::table('settings')->where('id',$id)->first();
	        $data['page_title']     = "Request Outlet";
	        $data['command']        = "edit";
	        $data['users']			= DB::table('cms_users')->where('created_by','1')->get();
	        $data['settings']		= DB::table('settings')->where('id',$id)->first();
	        $data['countries']		= DB::table('countries')->get();
           	$data['languages'] 		= DB::table('languages')->orderBy('id','ASC')->get();
           	$data['currencies'] 	= DB::table('currencies')->get();
           	$data['tax_types']		= DB::table('tax_types')->get();
	        $this->cbView('outlet/request_outlet',$data);
	    }


	}