<?php 

function getTableMerged($tableCode) {
	
	$table 		= DB::table('table_numbers')->whereTableCode($tableCode)->first();
	$tableMerge = $table->merge_with;
	if (!empty($tableMerge)):
		$explodeTable = explode(',', $tableMerge);
	endif;
	return $explodeTable;
}


function getSingleTableCode($tableId) {

	$table 	= getTableMerged($tableId);
	$table 	= $table->id;
	$data 	= [];
	foreach ($table as $key => $value):
		$tableData = DB::table('table_numbers')->find($value);
		$data[] = [
			'tableId' => $tableData->id,
			'tableCode' => $tableData->table_code,
		];
	endforeach;
	return $table;
}

// Get table code in array from array table_id
function getTableCode(array $tableId) {
	// Get table code instead of table id
	$tableCode = [];
	foreach ($tableId as $key => $value) {
		$getTableCode = DB::table('table_numbers')->whereId($value)->first();
		$tableCode[] = $getTableCode->table_code;
	}
	return $tableCode;
}



function getCompanyId($userId) {
	$companyId = DB::table('cms_users')->select('company_id')->find($userId);
	return $companyId->company_id;
}

function dineInFolderName(){
	$dineIn = "";
	return $dineIn;
}


function getCurrency($currencyName) {
	$currency = DB::table('currencies')
		->select('name', 'currency_symbol as symbol', 'currency_letter as letter')
		->whereName($currencyName)->first();
	return $currency->symbol;
}

// Delete files within folder
function unlinkFile($fileName) {
	if (file_exists($fileName)) {
		unlink($fileName);
	}
}

// Get item size 
function getItemSize($sizeId) {
	$uom = DB::table('menu_uom_price')->find($sizeId);
	return $uom->uom;
}

function getAddonName($itemId) {
	$item = DB::table('menu_materials')->find($itemId);

	return $item->name;
}

// Show Table code
function showTableCode($tableId) {
	$table = DB::table('table_numbers')->find($tableId);
	return $table->table_code;
}

// Check and get the discount value
function checkDiscount($discountId) {
	$discount = DB::table('discounts')
		->find($discountId);
	return $discount;
}

// Get tax for each company
function getTaxValue($companyId) {
	$tax = DB::table('settings')
	->select('company_sale_tax as tax')
	->whereIncludeTax(1)
	->find($companyId);
	return $tax->tax;
}


// Exchange Rate 
function getExchangeRate($companyId, $currency) {
	$rate = DB::table('exchange')
        ->leftjoin('currencies','currencies.id', 'exchange.currency_to')
        	->whereCurrencyToText($currency)
            ->where('exchange.company_id', $companyId)->first();

    return $rate;
}