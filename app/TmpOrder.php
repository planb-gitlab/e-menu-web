<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmpOrder extends Model
{
    protected $table = "tmp_order";
}
