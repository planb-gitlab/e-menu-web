<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $table='orders_detail';
    protected $fillable=['orders_id','menus_id','material_id','discount','amount','subtotal','price'];

    public function menu(){
        return $this->belongsTo("App\Menu","menus_id","id");
    }
}
