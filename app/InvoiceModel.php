<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceModel extends Model
{
    protected $table = "invoices";

    protected $fillable = [
    	'invoice_id',
    	'invoice_date',
    	'table_id',
    	'order_type',
    	'order_code',
    	'created_by',
    	'company_id',
    	'invoice_number',
    	'promotion_pack_id',
    	'currency',
    	'void',
    	'grand_total',
    	'grand_dollar_exchange',
    	'grand_riel_exchange',
    	'grand_baht_exchange',
        'tax',
    	'tax_amount',
    	'discount_type',
        'discount_value',
        'discount',
    	'discount_total_dollar',
    	'grand_total_after_refund',
    	'refund_amount',
    	'is_refund',
    	'has_paid',
    	'paid_total',
    	'paid_dollar',
    	'paid_riel',
    	'paid_baht',
    	'return_amount',
    	'return_dollar',
    	'return_riel',
    	'return_baht',
        'file_name',
    	'status',

    ];

    protected $primaryKey = 'invoice_id';

    public function getInvoiceOrder() {
         return $this->hasMany('App\InvoiceOrderModel');
    }

}
