<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    protected $table = "menu_materials";
    protected $fillable = ['name', 'description', 'photo', 'price', 'status', 'category_id', 'created_by'];

    public function hasMenus()
    {
        return $this->belongsToMany("App\Material", "menu_detail", "material_id", "menu_id");
    }
}
