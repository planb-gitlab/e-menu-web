<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Table extends Model
{
    protected $table='table_numbers';
    protected $fillable=['table_code','type','status','size','photo','description','merge_with','created_by'];
}
