<?php

namespace App;

use App\InvoiceOrderModel;
use Illuminate\Database\Eloquent\Model;

class InvoiceOrderModel extends Model
{
    //
    protected $table = 'invoice_orders';

    protected $fillable = [
    	'invoice_number',
    	'item_id',
    	'item_size',
    	'addon_id',
    	'qty',
    	'price',
    	'status',
    	'company_id'
    ];

    public $timestamps = false;
}
