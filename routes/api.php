<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login/email','API\LoginController@loginByEmail');
Route::post('/login/pin','API\LoginController@loginByPin');

Route::post('/menu','API\MenuController@getMenu');
//by viroth
Route::post('/menu/menu-categoryid-subcategoryid','API\MenuController@getMenuByCategoryIdAndSubCategoryId');

Route::post('/menu/category','API\MenuController@getCategories');
Route::post('/menu/subcategory','API\SubcategoryController@getSubCategory');
Route::get('/menu/detail','API\MenuController@getMenuDetail');
//by viroth
Route::post('/menu/detail-menuid','API\MenuController@getMenuDetailByMenuId');

Route::get('/menu/material','API\MenuController@getMenuMaterial');
Route::post('/menu/uom','API\MenuController@getMenuUOMPrice');
Route::post('/menu/addons','API\MenuController@getAddOns');

//byviroth
Route::post('/menu/material-materialid','API\MenuController@getMenuMaterialByMenuId');

Route::post('/table','API\TableController@getTables');
Route::post('/table/status','API\TableController@setTableStatus');
Route::post('/table/merge','API\TableController@mergeTable');

Route::post('/order','API\OrderController@getOrder');
Route::post('/order/request','API\OrderController@requestOrder');
Route::post('/order/update','API\OrderController@updateOrder');
    
Route::post('/print/ticket','API\PrintController@printTicket');

Route::get('/user/single','API\TestingController@getUserSingle');
Route::post('/mergeandunmerge','API\MergeController@MergeAndUnmerge');
