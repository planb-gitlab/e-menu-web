<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Events\RealTime;

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/login',function(){
// 	return view('login/login_via_pin');
// });

Route::get('admin/outlet_user/outlet_generate_file/{id}'		, ['uses' => 'AdminStoreUserController@generate_file'	, 'as' => 'outlet_generate_file']);


// =============== Store Configuration : when login first time ============

Route::post('store_configuration'		,['uses' => 'AdminDashboardController@add_store_configuration'	,'as' 	=> 'store_configuration']);

// =============== Dashboard ============

Route::post('pin_validation'			,['uses' => 'ValidationController@pin_validation'			,'as' => 'pin_validation']);
Route::get(	'admin'						,['uses' => 'AdminDashboardController@getIndex'					,'as' 	=> 'admin']);
Route::post('insert_dashboard_setting'	,['uses' => 'AdminDashboardController@insert_dashboard_setting'	,'as' => 'insert_dashboard_setting']);


// =============== User ===============
Route::post('user_save'					,['uses' => 'AdminCmsUsers1Controller@save'			,'as' => 'save.user']);
Route::post('user_update'				,['uses' => 'AdminCmsUsers1Controller@update'		,'as' => 'update.user']);

// =============== Profile ===============
Route::post('profile_update'			,['uses' => 'AdminCmsUsersController@update'		,'as' => 'update.profile']);



Route::post('category_save'				,['uses' => 'AdminCategoriesController@save'		,'as' => 'save.category']);
Route::post('category_update'			,['uses' => 'AdminCategoriesController@update'		,'as' => 'update.category']);

Route::post('subcategory_save'			,['uses' => 'AdminSubcategoryController@save'		,'as' => 'save.subcategory']);
Route::post('subcategory_update'		,['uses' => 'AdminSubcategoryController@update'		,'as' => 'update.subcategory']);

Route::post('material_save'				,['uses' => 'AdminMenuMaterialsController@save'		,'as' => 'save.material']);
Route::post('material_update'			,['uses' => 'AdminMenuMaterialsController@update'	,'as' => 'update.material']);


Route::get('admin/{slug}/status/{id}'	,['uses' => 'StatusController@getUpdateStatus'	,'as' => 'status']);
Route::post('/menu'						,['uses' => 'AdminMenusController@getMenu'		,'as' => 'menu']);
Route::post('/uom'						,['uses' => 'AdminUomController@getUOM'			,'as' => 'uom']);
Route::get('/exchange'					,'AdminExchangeController@getCurrency');
Route::post('update.promotion_pack'		,['uses' => 'AdminPromotionPackController@UpdatePromotionPack', 'as' => 'update.promotion_pack']);
Route::get('add-to-log'					,'HomeController@myTestAddToLog');
Route::get('logActivity'				,'HomeController@logActivity');
Route::post('show_box_branch','AdminDashboardController@show_box_branch')->name('show_box_branch');
Route::post('change_admin_branch'				,['uses' => 'AdminDashboardController@change_admin_branch', 'as' => 'change_admin_branch']);

//============================= invoice mg ==================

Route::post('sort_invoice_invoicemanagement','AdminInvoiceMGController@sort_invoice_invoicemanagement')->name('sort_invoice_invoicemanagement');
Route::post('invoicemanagement_search_invoice_byid','AdminInvoiceMGController@invoicemanagement_search_invoice_byid')->name('invoicemanagement_search_invoice_byid');
Route::get('invoice_management_view_detail','AdminInvoiceMGController@invoice_management_view_detail')->name('invoice_management_view_detail');


Route::post('search_invoice_by_id','AdminSaleByInvoiceController@search_invoice_by_id')->name('search_invoice_by_id');
Route::post('sale_by_invoice_insert_order','AdminSaleByInvoiceController@insert_order')->name('sale_by_invoice_insert_order');


Route::get('print-invoice', 'AdminSaleByInvoiceController@printInvoice')->name('report.printInvoice');


//=========================== end invoice mg ===============
Route::get('refund','AdminRefundController@getIndex')->name('refund');
/*Route::post('search_invoice_to_refund',['uses' => 'AdminRefundController@sort_invoice', 'as' => 'search_invoice']);*/
Route::post('search_invoice','AdminRefundController@sort_invoice')->name('search_invoice');
Route::post('admin_show_invoice',['uses' => 'AdminRefundController@show_invoice', 'as' => 'admin_show_invoice']);
Route::post('search_invoice_byid','AdminRefundController@search_invoice_byid')->name('search_invoice_byid');
Route::get('refund_update','AdminRefundController@refund_update')->name('refund_update');
Route::get('generate_new_invoice','AdminRefundController@generate_new_invoice')->name('generate_new_invoice');
Route::get('insert_order_after_refunded','AdminRefundController@insert_order_after_refunded')->name('insert_order_after_refunded');
Route::get('update_after_refund','AdminRefundController@update_after_refund')->name('update_after_refund');
//===============================================Void================================================//
Route::get('void_sort_invoice','AdminVoidController@void_sort_invoice')->name('void_sort_invoice');
Route::get('void_search_invoice_byid','AdminVoidController@void_search_invoice_byid')->name('void_search_invoice_byid');
Route::get('void_invoice','AdminVoidController@getIndex')->name('void_invoice');
Route::get('void_update','AdminVoidController@void_update')->name('void_update');
Route::get('void_insert_order','AdminVoidController@void_insert_order')->name('void_insert_order');

// Void and Create new one
Route::get('create-void', 'AdminVoidController@createVoid')->name('createVoidInvoice');

//==================================== end void invoice =======================================//
//=========================================Merge/Splite ========================================//
 Route::get('merge_invoice','AdminMergeInvoiceController@getIndex')->name('merge_invoice');
 Route::get('merge_sort_invoice','AdminMergeInvoiceController@merge_sort_invoice')->name('merge_sort_invoice');
Route::get('merge_search_invoice_byid','AdminMergeInvoiceController@merge_search_invoice_byid')->name('merge_search_invoice_byid');
Route::get('merge_insert_order','AdminMergeInvoiceController@merge_insert_order')->name('merge_insert_order');


Route::post('add_category_menu_via_menu_page'	,['uses' => 'AdminMenusController@add_category_menu_via_menu_page'	,'as' => 'add_category_menu_via_menu_page']);
Route::post('add_subcategory_menu_via_menu_page',['uses' => 'AdminMenusController@add_subcategory_menu_via_menu_page','as' => 'add_subcategory_menu_via_menu_page']);
Route::post('add_uom_via_menu_page'				,['uses' => 'AdminMenusController@add_uom_via_menu_page'				,'as' => 'add_uom_via_menu_page']);



Route::post('get_subcategory'			,['uses' => 'AdminMenusController@get_subcategory'			,'as' => 'get_subcategory']);
Route::post('add.menu'					,['uses' => 'AdminMenusController@AddMenu'						,'as' => 'add.menu']);
Route::post('update.menu'				,['uses' => 'AdminMenusController@UpdateMenus'					,'as' => 'update.menu']);
Route::post('save.table'				,['uses' => 'AdminTableNumbersController@AddTable'				,'as' => 'save.table']);
Route::post('update.table'				,['uses' => 'AdminTableNumbersController@UpdateTable'			,'as' => 'update.table']);
Route::post('update.company_information',['uses' => 'AdminSettingController@update_company_information'	,'as' => 'update.company_information']);
Route::post('update.company_tax'		,['uses' => 'AdminSettingController@update_company_tax'			,'as' => 'update.company_tax']);
Route::post('update.company_currency'	,['uses' => 'AdminSettingController@update_company_currency'	,'as' => 'update.company_currency']);


//role
Route::post('save_role'					,['uses' => 'AdminRolesController@save_role'	, 'as' => 'save_role']);
Route::post('update_role'				,['uses' => 'AdminRolesController@update_role'	, 'as' => 'update_role']);

//menu
Route::get('admin/menus/menu_duplicate/{id}'	, ['uses' => 'AdminMenusController@menu_duplicate'		, 'as' => 'menu_duplicate']);
Route::get('admin/menus/menu_uom_index/{id}'	, ['uses' => 'AdminMenusController@menu_uom_index'		, 'as' => 'menu_uom_index']);
Route::get('admin/menus/menu_addon_index/{id}'	, ['uses' => 'AdminMenusController@menu_addon_index'	, 'as' => 'menu_addon_index']);
Route::post('add_menu_uom'						, ['uses' => 'AdminMenusController@add_menu_uom'		, 'as' => 'add.menu_uom']);
Route::post('add_menu_addon'					, ['uses' => 'AdminMenusController@add_menu_addon'		, 'as' => 'add.menu_addon']);


//promotion_pack

Route::post('save_promotion_pack'		, ['uses' => 'AdminPromotionPackController@save_promotion_pack', 'as' => 'save_promotion_pack']);
Route::post('update_promotion_pack'		, ['uses' => 'AdminPromotionPackController@update_promotion_pack', 'as' => 'update_promotion_pack']);
Route::get('admin/promotion_packs/promotion_pack_duplicate/{id}', ['uses' => 'AdminPromotionPackController@promotion_pack_duplicate', 'as' => 'promotion_pack_duplicate']);

//Branch
Route::post('save_branch'			, ['uses' => 'AdminBranchController@save_branch'		,'as' => 'save_branch']);
Route::post('update_branch'			, ['uses' => 'AdminBranchController@update_branch'		,'as' => 'update_branch']);
Route::post('save_branch_admin'		, ['uses' => 'AdminBranchController@save_branch_admin'	,'as' => 'save_branch_admin']);
Route::post('update_branch_admin'	, ['uses' => 'AdminBranchController@update_branch_admin','as' => 'update_branch_admin']);


// =================================== Cashier =======================================
Route::get('cashier/cashier_table_management', ['uses' => 'CashierTableManagementController@getIndex', 'as' => 'cashier_table_management']);
Route::get('cashier/cashier_table_management_detail', ['uses' => 'CashierTableManagementDetailController@getIndex', 'as' => 'cashier_table_management_detail']);
	

// =============== Table ============== //

Route::post('get_all_table'		, 	['uses' => 'CashierTableManagementController@get_all_table'			, 'as' => 'get_all_table']);
Route::post('/find_table'		, 	['uses' => 'CashierTableManagementController@find_table'			, 'as' => 'find_table']);
Route::post('find_merge_table'	, 	['uses' => 'CashierTableManagementController@find_merge_table'		, 'as' => 'find_merge_table']);
Route::post('merge_table', 'CashierTableManagementController@merge_table')->name('merge_table');
Route::post('unmerge_table'		, 	['uses' => 'CashierTableManagementController@unmerge_table'			, 'as' => 'unmerge_table']);





// =============== Order no table =========
Route::post('get_order_data'	,['uses' => 'AdminOrderController@get_order_data'	,'as' => 'get_order_data']);


// =============== Order ==============
Route::get('admin/cashier_order'			,['uses' => 'CashierOrderController@getIndex'				, 'as' => 'cashier_order']);

Route::get('/cashier_create_order', 'CashierCreateOrderController@getIndex')->name('cashier_create_order');

Route::get('/cashier_update_order'			,['uses' => 'CashierCreateOrderController@getUpdate'		, 'as' => 'cashier_update_order']);
Route::post('show_category'					,['uses' => 'CashierCreateOrderController@show_category' 	, 'as' => 'show_category']);
Route::post('show_subcategory'				,['uses' => 'CashierCreateOrderController@show_subcategory' , 'as' => 'show_subcategory']);
Route::post('show_menu'						,['uses' => 'CashierCreateOrderController@show_menu' 		, 'as' => 'show_menu']);
Route::post('show_subcategory_by_category'	,['uses' => 'CashierCreateOrderController@show_subcategory_by_category', 'as' => 'show_subcategory_by_category']);
Route::post('show_menu_by_category_subcategory', ['uses' => 'CashierCreateOrderController@show_menu_by_category_subcategory' ,'as' => 'show_menu_by_category_subcategory']);
Route::post('split_invoice', 				['uses' => 'CashierCreateOrderController@split_invoice' ,'as' => 'split_invoice']);


Route::post('menu_type_with_subcategory','CashierCreateOrderController@get_menu_type_with_subcategory')->name('menu_type_with_subcategory');



Route::post('show_material_for_menu','CashierCreateOrderController@show_material_for_menu')->name('show_material_for_menu');




Route::post('check_discount_type'			,['uses' => 'CashierCreateOrderController@check_discount_type'	, 'as' => 'check_discount_type']);


Route::post('promotion_pack_deal'			,['uses' => 'CashierCreateOrderController@promotion_pack_deal'		, 'as' => 'promotion_pack_deal']);
Route::post('order_discount_rate'			,['uses' => 'CashierCreateOrderController@order_discount_rate'		, 'as' => 'order_discount_rate' ]);
Route::post('get_promotion_pack_data'		,['uses' => 'CashierCreateOrderController@get_promotion_pack_data'	, 'as' => 'get_promotion_pack_data']);


Route::post('get_promotion_pack_data_change',['uses' => 'CashierCreateOrderController@get_promotion_pack_data_change', 'as' => 'get_promotion_pack_data_change']);

Route::post('currency_deposit'				,['uses' => 'CashierCreateOrderController@currency_deposit'		, 'as' => 'currency_deposit']);

Route::post('/menu_data'					,['uses' => 'CashierCreateOrderController@show_menu_data'		, 'as' => 'menu_data']);
Route::post('/menu_data_with_material'		,['uses' => 'CashierCreateOrderController@show_menu_data_with_material'		, 'as' => 'menu_data_with_material']);

// =============== End Order ==============



// Report
Route::post('view_invoice'				,['uses' => 'AdminSaleByInvoiceController@view_invoice'			, 'as' => 'report.view_invoice']);

Route::post('insert_report'				,['uses' => 'AdminReportDashboardController@insert_report'		, 'as' => 'insert_report']);
Route::post('get_sold_amount'			,['uses' => 'AdminReportDashboardController@get_sold_amount'	, 'as' => 'get_sold_amount']);
Route::post('get_table_sold'			,['uses' => 'AdminReportDashboardController@get_table_sold'		, 'as' => 'get_table_sold']);
Route::post('get_customer_count'		,['uses' => 'AdminReportDashboardController@get_customer_count'	, 'as' => 'get_customer_count']);
Route::post('get_discount'				,['uses' => 'AdminReportDashboardController@get_discount'		, 'as' => 'get_discount']);
Route::post('get_subtotal'				,['uses' => 'AdminReportDashboardController@get_subtotal'		, 'as' => 'get_subtotal']);
Route::post('get_revenue'				,['uses' => 'AdminReportDashboardController@get_revenue'		, 'as' => 'get_revenue']);
Route::post('get_top_sale_cashier'		,['uses' => 'AdminReportDashboardController@get_top_sale_cashier', 'as' => 'get_top_sale_cashier']);



Route::get('get_sort_invoice'			,['uses' => 'AdminSaleByInvoiceController@get_sort'				, 'as' => 'get_sort_invoice']);
Route::get('get_sort_item'				,['uses' => 'AdminSaleByItemController@get_sort'				, 'as' => 'get_sort_item']);
Route::post('get_uom_menu'				,['uses' => 'AdminSaleByItemController@get_uom_menu'			, 'as' => 'get_uom_menu']);
Route::get('get_sort_cashier'			,['uses' => 'AdminSaleByCashierController@get_sort'				, 'as' => 'get_sort_cashier']);
Route::get('get_sort_promotion_pack'	,['uses' => 'AdminSaleByPromotionPackController@get_sort'		, 'as' => 'get_sort_promotion_pack']);
Route::get('get_sort_void_payment'		,['uses' => 'AdminVoidPaymentController@get_sort'				, 'as' => 'get_sort_void_payment']);
Route::get('get_sort_discount_summary'	,['uses' => 'AdminDiscountSummaryController@get_sort'			, 'as' => 'get_sort_discount_summary']);



Route::post('get_sort_by_date'	,['uses' => 'AdminSaleByPromotionPackController@get_sort_by_date'			, 'as' => 'get_sort_by_date']);
Route::post('get_sort_by_custom_date'	,['uses' => 'AdminSaleByPromotionPackController@get_sort_by_custom_date'			, 'as' => 'get_sort_by_custom_date']);
Route::post('get_sort_by_promotion_name'	,['uses' => 'AdminSaleByPromotionPackController@get_sort_by_promotion_name'			, 'as' => 'get_sort_by_promotion_name']);
Route::post('get_sort_by_promotion_type'	,['uses' => 'AdminSaleByPromotionPackController@get_sort_by_promotion_type'			, 'as' => 'get_sort_by_promotion_type']);

// Cash Management
Route::get('get_sort_void_invoice'			,['uses' => 'AdminVoidController@get_sort'				, 'as' => 'get_sort_void_invoice']);
Route::post('cash_management.view_invoice'	,['uses' => 'AdminVoidController@view_invoice'			, 'as' => 'cash_management.view_invoice']);
Route::post('cash_management.void_invoice'	,['uses' => 'AdminVoidController@void_invoice'			, 'as' => 'cash_management.void_invoice']);
Route::post('cash_management.void_copy_invoice'	,['uses' => 'AdminVoidController@void_copy_invoice'			, 'as' => 'cash_management.void_copy_invoice']);

// Log
Route::post('get_sort_log_activity'		,['uses' => 'AdminUserActivitiesController@get_sort'		, 'as' => 'get_sort_log_activity']);
Route::post('get_search_log_activity'	,['uses' => 'AdminUserActivitiesController@get_search'		, 'as' => 'get_search_log_activity']);

Route::post('get_invoice','AdminRefundController@insert_order')->name('get_invoice');


// View Detail and Report
Route::get('get_sort_invoice_detail/{id}'		,['uses' => 'AdminSaleByInvoiceController@view_detail'		, 'as' =>'get_sort_invoice_detail']);
Route::get('get_sort_promotion_pack_detail/{id}',['uses' => 'AdminSaleByPromotionPackController@view_detail', 'as' =>'get_sort_promotion_pack_detail']);
Route::get('get_promotion_detail/{id}'			,['uses' => 'AdminSaleByPromotionPackController@view_promotion_pack_detail', 'as' =>'get_promotion_detail']);

Route::get('get_void_payment_detail/{id}'		,['uses' => 'AdminVoidPaymentController@view_detail'		, 'as' =>'get_void_payment_detail']);
Route::get('get_sort_discount_detail/{id}'		,['uses' => 'AdminDiscountSummaryController@view_detail'	, 'as' =>'get_sort_discount_detail']);
Route::get('get_sort_credit_memo_detail/{id}'	,['uses' => 'AdminCreditMemoReportController@view_detail'	, 'as' =>'get_sort_credit_memo_detail']);
Route::post('view_log_detail'					,['uses' => 'AdminUserActivitiesController@view_log_detail'	, 'as' =>'view_log_detail']);
Route::get('log_activity_detail/{id}'			,['uses' => 'AdminUserActivitiesController@view_detail'		, 'as' =>'log_activity_detail']);



Route::get('export_pdf/{id}'					,['uses' => 'AdminSaleByInvoiceController@export'			, 'as' =>'export_pdf']);
Route::get('export_pdf_void_payment/{id}'		,['uses' => 'AdminVoidPaymentController@export'				, 'as' =>'export_pdf_void_payment']);
Route::get('export_pdf_credit_memo/{id}	'		,['uses' => 'AdminCreditMemoReportController@export'		, 'as' =>'export_pdf_credit_memo']);


// Admin report print
Route::post('admin_view_invoice'		,['uses' => 'AdminSaleByInvoiceController@view_invoice'		, 'as' => 'admin_view_invoice']);

// Cashier invoice pirnt
Route::get('cashier_view_invoice'		,['uses' => 'CashierViewOrderController@view_invoice'				, 'as' => 'cashier_view_invoice']);
Route::get('print_invoice'				,['uses' => 'AdminRefundController@show_invoice'					, 'as' => 'print_invoice']);


Route::post('insert_role_privilege',['uses' => 'AdminRolesController@insert_role_privilege', 'as' => 'insert_role_privilege']);

  
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::post('find_membership' 				, ['uses'=> 'CashierCreateOrderController@find_membership'			, 'as' => 'find_membership']);
Route::post('set_membership' 				, ['uses'=> 'CashierCreateOrderController@set_membership'			, 'as' => 'set_membership']);
Route::post('enable_discount'				, ['uses'=> 'CashierCreateOrderController@enable_discount'			, 'as' => 'enable_discount']);
Route::post('request_order' 				, ['uses'=> 'API\OrderController@requestOrder'						, 'as' => 'request_order']);
Route::post('request_order_into_db' 		, ['uses'=> 'CashierCreateOrderController@requestOrderIntoDB'		, 'as' => 'request_order_into_db']);
Route::post('get_order'						, ['uses'=> 'CashierCreateOrderController@getOrder'					, 'as' => 'get_order']);
Route::post('paid_order'					, ['uses'=> 'CashierCreateOrderController@paid_order'				, 'as' => 'paid_order']);
Route::post('unlinkOrder'					, ['uses'=> 'API\OrderController@unlinkOrder'						, 'as' => 'unlinkOrder']);
Route::post('set_promotion' 				, ['uses'=> 'CashierCreateOrderController@set_promotion'			, 'as' => 'set_promotion']);
Route::post('set_table_status'				, ['uses'=> 'CashierTableManagementController@set_table_status'		, 'as' => 'set_table_status']);
Route::post('generate_invoice'				, ['uses'=> 'CashierCreateOrderController@generate_invoice'			, 'as' => 'generate_invoice']);
Route::post('printTicket' 					, ['uses'=> 'API\PrintController@printTicket'						, 'as' => 'printTicket']);
Route::post('generate_paid_invoice' 		, ['uses'=> 'CashierCreateOrderController@generate_paid_invoice'	, 'as' => 'generate_paid_invoice']);


//print setting
Route::post('update_discount_permission' 	, ['uses'=> 'AdminOrderSettingController@set_permit'				, 'as' => 'update_discount_permission']);
Route::post('print_setting' 				, ['uses'=> 'AdminOrderSettingController@print_setting'				, 'as' => 'print_setting']);

//pusher realtime
Route::get('sender-form', 'CashierCreateOrderController@getForm')->name('getForm');

Route::get('sender','CashierCreateOrderController@senderForm')->name('senderForm');
Route::post('sender','CashierCreateOrderController@submitForm')->name('sender');
// Route::get('')

//end realtime

Route::post('save.takeout' 					,['uses'=> 'API\OrderController@saveTakeout'						, 'as' => 'save.takeout']);
Route::get('/takeout_save_order'			,['uses' => 'CashierCreateOrderController@getIndex_takeout'			, 'as' => 'save_order.takeout']);
Route::get('/takeout_update_order'			,['uses' => 'CashierCreateOrderController@getUpdate_takeout'		, 'as' => 'update_order.takeout']);

Route::post('update_printer_name'			,['uses' => 'AdminOrderSettingController@update_printer_name'		, 'as' => 'update.printer_name']);
Route::post('find_printer_name'				,['uses' => 'AdminOrderSettingController@find_printer_name'			, 'as' => 'find_printer_name']);

Route::get('membership_discount_update'		,['uses' => 'AdminDiscountsController@membership_discount_update'	,	'as' => 'membership_discount_update']);
Route::post('update.membership_discount'	,['uses' => 'AdminDiscountsController@update_membership_discount'	,	'as' => 'update.membership_discount']);

// ================ Membership =====================
Route::post('save.membership' 				,['uses'=> 'AdminMembershipsController@save'						, 'as' => 'save.membership']);
Route::post('update.membership' 			,['uses'=> 'AdminMembershipsController@update'						, 'as' => 'update.membership']);

